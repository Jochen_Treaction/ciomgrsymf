# MARKETING-IN-ONE #
## VERSION 2.0.3.1 ##
`--------------------------------------------- `
# this file must be in mod 640
## deployement configuration
----------------------------------
start .env (DEV)
----------------------------------
APP_ENV=dev
APP_SECRET=70157ac5cffa9e969a628f2574f94c2d
# TRUSTED_PROXIES=127.0.0.0/8,10.0.0.0/8,172.16.0.0/12,192.168.0.0/16
# TRUSTED_HOSTS='^localhost|example\.com$'
### symfony/framework-bundle ###

######> doctrine/doctrine-bundle ###
###### Format described at https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
###### For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
###### For a PostgreSQL database, use: "postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11"
###### IMPORTANT: You MUST configure your db driver and server version, either here or in config/packages/doctrine.yaml
###### reverse engeneering db
###### DATABASE_URL=mysql://jo:"ruSOAq5aiabwBtGSm3QI"@127.0.0.1:3306/ciomgr?serverVersion=8.0
###### DATABASE_URL=mysql://jo:"ruSOAq5aiabwBtGSm3QI"@127.0.0.1:3306/ciomgr?serverVersion=8.0
###### work db
###### ----------- start DEAD -----------
###### DEV OLD: DATABASE_URL=mysql://ciodev_Di8che6eX:T8Z!NK828pMowyqsAOm1OCLydGYlMOVgAj@127.0.0.1:3306/cio?serverVersion=5.7
###### ----------- end DEAD -----------
###### ----------- start local  -----------
###### DATABASE_URL=mysql://homestead:secret@192.168.10.10:3306/homestead?serverVersion=5.7
###### ----------- end local  -----------
###### ----------- start dev-campaign-in-one.net  -----------
DATABASE_URL=mysql://cioaccessEeQu3Ph:ufie_N6AhvieM6quov6ied4EepHei3fae1R@127.0.0.1:3306/newcio?serverVersion=5.7
###### ----------- end dev-campaign-in-one.net  -----------
#####< doctrine/doctrine-bundle ###

######> symfony/swiftmailer-bundle ###
###### For Gmail as a transport, use: "gmail://username:password@localhost"
###### For a generic SMTP server, use: "smtp://localhost:25?encryption=&auth_mode="
###### Delivery is disabled by default via "null://localhost"
###### MAILER_URL=null://homestead:8025
###### ------------------- start old config 
###### MAILER_URL=smtp://lvps92-51-150-31.dedicated.hosteurope.de:587?encryption=tls&auth_mode=login&username=support@dev-campaign-in-one.net&password=gKsxYmr2cKB10@=rV4UPr
###### ------------------- end old config 
######------------------- start new config 
MAILER_URL=smtp://smtp.office365.com:587?encryption=tls&auth_mode=login&username=dev-maassmtp@treaction.de&password=fNc8H+3Rw@m&1x
###### ------------------- end new config 

# MIRCOSERVICE URLs
MSCIO_URL=https://mscio.dev-api-in-one.net
MSHB_URL=https://mshb.dev-api-in-one.net
MSMIO_URL=https://msmio.dev-api-in-one.net
####### MSMAIL_URL=https://msmail.dev-api-in-one.net
MSMAIL_URL=http://msmailapi.test
MSDIGISTORE_URL=https://msdigi.dev-api-in-one.net
CAMPAIGN_DOMAIN=https://campaigns.dev-campaign-in-one.net
CAMPAIGN_SERVICE=https://mscamp.dev-campaign-in-one.net

### < symfony/swiftmailer-bundle ###
----------------------------------
end .env (DEV)
----------------------------------
