-- Drop the index
ALTER TABLE `inbox_monitor`
DROP FOREIGN KEY `fk_inbox_monitor_seed_pool1`,
DROP INDEX `fk_inbox_monitor_seed_pool1_idx`;
-- Create new constrain
ALTER TABLE .`inbox_monitor`
    ADD CONSTRAINT fk_inbox_monitor_seed_pool1
    FOREIGN KEY (seed_pool_id) REFERENCES seed_pool (id)
    ON DELETE CASCADE;
-- Create new index
CREATE INDEX fk_inbox_monitor_seed_pool1_idx
    ON inbox_monitor (seed_pool_id);