-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE =
        'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema newcio
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `blacklist_domains`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blacklist_domains`;

CREATE TABLE IF NOT EXISTS `blacklist_domains`
(
    `id`          BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `domain_name` VARCHAR(255)     NOT NULL COMMENT 'Blacklisted Domain names',
    `created`     DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`  INT(11) UNSIGNED NULL,
    `updated`     DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`  INT(11) UNSIGNED NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 75
    DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `company`;

CREATE TABLE IF NOT EXISTS `company`
(
    `id`               BIGINT UNSIGNED                                                   NOT NULL AUTO_INCREMENT,
    `account_no`       VARCHAR(100)                                                      NOT NULL,
    `name`             VARCHAR(150) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `street`           VARCHAR(100)                                                      NOT NULL,
    `house_number`     VARCHAR(100)                                                      NOT NULL,
    `postal_code`      VARCHAR(100)                                                      NOT NULL,
    `city`             VARCHAR(100)                                                      NOT NULL,
    `country`          VARCHAR(100)                                                      NOT NULL,
    `website`          VARCHAR(100)                                                      NOT NULL,
    `phone`            VARCHAR(100)                                                      NOT NULL,
    `vat`              VARCHAR(100)                                                      NOT NULL,
    `login_url`        VARCHAR(100)                                                      NOT NULL,
    `delegated_domain` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'Only when URL option is delegated domain',
    `url_option`       VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT 'folder' COMMENT 'folder, subdomain, deligate domain ',
    `created`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`       INT(11) UNSIGNED                                                  NULL,
    `updated`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`       INT(11) UNSIGNED                                                  NULL,
    `modification`     VARCHAR(100)                                                      NOT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 19
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `id` ON `company` (`id` ASC);

CREATE UNIQUE INDEX `uidx_account_no` ON `company` (`account_no` ASC);


-- -----------------------------------------------------
-- Table `status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `status`;

CREATE TABLE IF NOT EXISTS `status`
(
    `id`         BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `status_txt` VARCHAR(45)      NOT NULL,
    `created_at` DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT(11) UNSIGNED NULL COMMENT 'decoupled users.id',
    `updated_at` DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by` INT(11) UNSIGNED NULL COMMENT 'decoupled users.id',
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 13
    DEFAULT CHARACTER SET = utf8mb4
    COMMENT = 'NEW. VERSION 1.1 status TABLE BY JSR. REPLACES ENUM-TYPES IN FUTURE VERSIONS';


-- -----------------------------------------------------
-- Table `survey`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `survey`;

CREATE TABLE IF NOT EXISTS `survey`
(
    `id`                 BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `layout_status_id`   BIGINT UNSIGNED  NULL     DEFAULT NULL COMMENT 'key from status table (Carousel or Single page)',
    `survey_is_online`   TINYINT(1)       NOT NULL DEFAULT 0 COMMENT 'flag to turn survey online (1)  or offline (0). default is on ( 1 )\\n',
    `survey_description` TEXT             NULL     DEFAULT NULL COMMENT 'description of survey, add customer requirements or comments of the survey-maker here. add textarea to frontend.',
    `created_at`         DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`         INT(11) UNSIGNED NULL COMMENT 'decoupled users.id',
    `updated_at`         DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`         INT(11) UNSIGNED NULL COMMENT 'decoupled users.id',
    PRIMARY KEY (`id`),
    CONSTRAINT `survey_status1`
        FOREIGN KEY (`layout_status_id`)
            REFERENCES `status` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 77
    DEFAULT CHARACTER SET = utf8mb4
    COMMENT = 'NEW. VERSION 1.1 survey TABLE BY JSR.';

CREATE INDEX `survey_status1_idx` ON `survey` (`layout_status_id` ASC);


-- -----------------------------------------------------
-- Table `campaign`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `campaign`;

CREATE TABLE IF NOT EXISTS `campaign`
(
    `id`             BIGINT UNSIGNED                     NOT NULL AUTO_INCREMENT,
    `company_id`     BIGINT UNSIGNED                     NOT NULL,
    `survey_id`      BIGINT UNSIGNED                     NULL,
    `name`           VARCHAR(255)                        NOT NULL,
    `account`        VARCHAR(255)                        NOT NULL,
    `url`            VARCHAR(255)                        NOT NULL,
    `comment`        VARCHAR(255)                        NOT NULL,
    `activationdate` DATE                                NOT NULL,
    `activationby`   INT(100)                            NOT NULL,
    `archivedate`    DATE                                NOT NULL,
    `archiveby`      INT(100)                            NOT NULL,
    `version`        VARCHAR(255)                        NOT NULL,
    `status`         ENUM ('draft', 'active', 'archive') NOT NULL,
    `created`        DATETIME                            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`     INT(11) UNSIGNED                    NULL,
    `updated`        DATETIME                            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`     INT(11) UNSIGNED                    NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `campaign_ibfk_1`
        FOREIGN KEY (`company_id`)
            REFERENCES `company` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `ff_campaign_survey`
        FOREIGN KEY (`survey_id`)
            REFERENCES `survey` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 851
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `company_id` ON `campaign` (`company_id` ASC);

CREATE INDEX `ff_campaign_survey_idx` ON `campaign` (`survey_id` ASC);


-- -----------------------------------------------------
-- Table `configuation_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `configuation_groups`;

CREATE TABLE IF NOT EXISTS `configuation_groups`
(
    `id`         BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `name`       VARCHAR(100)     NOT NULL DEFAULT '' COMMENT 'campaign_params group_name goes here',
    `desc`       TEXT             NULL,
    `created`    DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT(11) UNSIGNED NULL,
    `updated`    DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by` INT(11) UNSIGNED NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `configuration_targets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `configuration_targets`;

CREATE TABLE IF NOT EXISTS `configuration_targets`
(
    `id`                     BIGINT UNSIGNED  NOT NULL,
    `configuration_group_id` BIGINT UNSIGNED  NOT NULL,
    `name`                   VARCHAR(100)     NOT NULL DEFAULT '',
    `desc`                   TEXT             NULL,
    `created`                DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`             INT(11) UNSIGNED NULL,
    `updated`                DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`             INT(11) UNSIGNED NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_target_group`
        FOREIGN KEY (`configuration_group_id`)
            REFERENCES `configuation_groups` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;

CREATE INDEX `fk_target_group_idx` ON `configuration_targets` (`configuration_group_id` ASC);

CREATE UNIQUE INDEX `uk_target_group` ON `configuration_targets` (`id` ASC, `configuration_group_id` ASC);


-- -----------------------------------------------------
-- Table `campaign_params`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `campaign_params`;

CREATE TABLE IF NOT EXISTS `campaign_params`
(
    `id`                      BIGINT UNSIGNED                                                 NOT NULL AUTO_INCREMENT,
    `campaign_Id`             BIGINT UNSIGNED                                                 NOT NULL,
    `configuration_target_id` BIGINT UNSIGNED                                                 NOT NULL,
    `value`                   MEDIUMTEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT '	MEDIUMTEXT must be changed to JSON datatype in modern MySql Version',
    `created`                 DATETIME                                                        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`              INT(11) UNSIGNED                                                NULL,
    `updated`                 DATETIME                                                        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`              INT(11) UNSIGNED                                                NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `campaign_params_ibfk_1`
        FOREIGN KEY (`campaign_Id`)
            REFERENCES `campaign` (`id`),
    CONSTRAINT `fk_params_target`
        FOREIGN KEY (`configuration_target_id`)
            REFERENCES `configuration_targets` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 80
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `campaign_Id` ON `campaign_params` (`campaign_Id` ASC);

CREATE INDEX `fk_params_target_idx` ON `campaign_params` (`configuration_target_id` ASC);


-- -----------------------------------------------------
-- Table `leads`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `leads`;

CREATE TABLE IF NOT EXISTS `leads`
(
    `id`                     BIGINT UNSIGNED                                                   NOT NULL AUTO_INCREMENT,
    `campaign_id`            BIGINT UNSIGNED                                                   NOT NULL DEFAULT 0,
    `email`                  VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `salutation`             VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `first_name`             VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `last_name`              VARCHAR(255) CHARACTER SET 'utf8mb4'                              NOT NULL DEFAULT '',
    `firma`                  VARCHAR(255)                                                      NOT NULL DEFAULT '',
    `street`                 VARCHAR(255) CHARACTER SET 'utf8mb4'                              NOT NULL DEFAULT '',
    `postal_code`            VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `city`                   VARCHAR(255) CHARACTER SET 'utf8mb4'                              NOT NULL DEFAULT '',
    `country`                VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `phone`                  VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `account`                INT(100)                                                          NOT NULL DEFAULT 0,
    `url`                    VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `final_url`              VARCHAR(255)                                                      NOT NULL DEFAULT '',
    `trafficsource`          VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `utm_parameters`         VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `campaign`               VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `split_version`          VARCHAR(255)                                                      NOT NULL DEFAULT '',
    `lead_reference`         VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `targetgroup`            VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT '',
    `affiliateID`            VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT '' COMMENT 'Affiliate Id from campaign URL',
    `affiliateSubID`         VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT '' COMMENT 'AffiliateSubId from campaign URL',
    `status`                 ENUM ('active', 'inactive', 'storno')                             NOT NULL DEFAULT 'active',
    `ip`                     VARCHAR(100)                                                      NOT NULL DEFAULT '',
    `device_type`            VARCHAR(100)                                                      NOT NULL DEFAULT '',
    `device_browser`         VARCHAR(100)                                                      NOT NULL DEFAULT '',
    `device_os`              VARCHAR(100)                                                      NOT NULL DEFAULT '',
    `device_os_version`      VARCHAR(100)                                                      NOT NULL DEFAULT '',
    `device_browser_version` VARCHAR(100)                                                      NOT NULL DEFAULT '',
    `created`                DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`             INT(11) UNSIGNED                                                  NULL,
    `updated`                DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`             INT(11) UNSIGNED                                                  NULL,
    `modification`           INT(100)                                                          NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_leads_campaign`
        FOREIGN KEY (`campaign_id`)
            REFERENCES `campaign` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 15148
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `campaign_id` ON `leads` (`campaign_id` ASC);

CREATE INDEX `account` ON `leads` (`account` ASC);


-- -----------------------------------------------------
-- Table `customer_journey`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `customer_journey`;

CREATE TABLE IF NOT EXISTS `customer_journey`
(
    `id`               BIGINT UNSIGNED                                                   NOT NULL AUTO_INCREMENT,
    `lead_id`          BIGINT UNSIGNED                                                   NOT NULL COMMENT 'Lead id from leads',
    `customer_journey` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'Format  : \'trafficsource\' - \'datetime\'\'',
    `created`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`       INT(11) UNSIGNED                                                  NULL,
    `updated`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`       INT(11) UNSIGNED                                                  NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `cj_leads_ibfk_1`
        FOREIGN KEY (`lead_id`)
            REFERENCES `leads` (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 611
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `Index` ON `customer_journey` (`lead_id` ASC);


-- -----------------------------------------------------
-- Table `fraud`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fraud`;

CREATE TABLE IF NOT EXISTS `fraud`
(
    `id`             BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `campaign_id`    BIGINT UNSIGNED NOT NULL COMMENT 'Campaign Id ',
    `type`           VARCHAR(255)    NULL     DEFAULT NULL COMMENT 'IP Block, Blacklist Domain, Fraud Limit',
    `email`          VARCHAR(255)    NOT NULL COMMENT 'Client email id',
    `domain`         VARCHAR(255)    NULL COMMENT 'Client Domain name',
    `ip`             VARCHAR(100)    NULL COMMENT 'Client Ip address',
    `device_browser` VARCHAR(255)    NULL COMMENT 'Client Browser',
    `device_os`      VARCHAR(255)    NULL COMMENT 'Client OS name ',
    `time_stamp`     DATETIME        NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Current time stamp',
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_fraud_campaign_id`
        FOREIGN KEY (`campaign_id`)
            REFERENCES `campaign` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 163
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `fk_fraud_campaign_id` ON `fraud` (`campaign_id` ASC);


-- -----------------------------------------------------
-- Table `integrations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `integrations`;

CREATE TABLE IF NOT EXISTS `integrations`
(
    `id`                     BIGINT UNSIGNED                                                   NOT NULL AUTO_INCREMENT,
    `company_id`             BIGINT UNSIGNED                                                   NOT NULL,
    `configuration_group_id` BIGINT UNSIGNED                                                   NOT NULL,
    `type`                   VARCHAR(255)                                                      NULL     DEFAULT NULL,
    `api_key`                VARCHAR(255)                                                      NULL     DEFAULT NULL,
    `user`                   VARCHAR(255)                                                      NULL     DEFAULT NULL,
    `password`               VARCHAR(255)                                                      NULL     DEFAULT NULL,
    `url`                    VARCHAR(255)                                                      NULL     DEFAULT NULL,
    `account_no`             VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL COMMENT 'delete since it is replaced by company_id',
    `first_name`             VARCHAR(255)                                                      NULL     DEFAULT NULL,
    `last_name`              VARCHAR(255)                                                      NULL     DEFAULT NULL,
    `owner_id`               VARCHAR(255)                                                      NULL     DEFAULT NULL,
    `sync`                   VARCHAR(255)                                                      NULL     DEFAULT NULL,
    `additional_settings`    MEDIUMTEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'   NULL     DEFAULT NULL COMMENT '	MEDIUMTEXT must be changed to JSON datatype in modern MySql Version',
    `created`                DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`             INT(11) UNSIGNED                                                  NULL,
    `updated`                DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`             INT(11) UNSIGNED                                                  NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_integration_company`
        FOREIGN KEY (`company_id`)
            REFERENCES `company` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_integration_config_group`
        FOREIGN KEY (`configuration_group_id`)
            REFERENCES `configuation_groups` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `account_no` ON `integrations` (`account_no` ASC);

CREATE INDEX `fk_integration_company_idx` ON `integrations` (`company_id` ASC);

CREATE INDEX `fk_integration_config_group_idx` ON `integrations` (`configuration_group_id` ASC);


-- -----------------------------------------------------
-- Table `mapping_templates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mapping_templates`;

CREATE TABLE IF NOT EXISTS `mapping_templates`
(
    `id`               BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `Campaign-In-One`  VARCHAR(255)     NOT NULL COMMENT 'CIO Standard Fields',
    `HTML Form`        VARCHAR(255)     NOT NULL COMMENT 'html Standard fields',
    `Mail-In-One`      VARCHAR(255)     NOT NULL COMMENT 'MIO Stardard fields',
    `Hubspot`          VARCHAR(255)     NOT NULL COMMENT 'Hubspot Standard fields',
    `Email Forwarding` VARCHAR(255)     NOT NULL COMMENT 'Email Standard fields',
    `Field type`       VARCHAR(255)     NOT NULL COMMENT 'standard fields',
    `created`          DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created by User Id',
    `created_by`       INT(11) UNSIGNED NULL COMMENT 'Created Date',
    `updated`          DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated by User Id',
    `updated_by`       INT(11) UNSIGNED NULL COMMENT 'Updated Date',
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 27
    DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `users`;

CREATE TABLE IF NOT EXISTS `users`
(
    `id`            BIGINT UNSIGNED                                                   NOT NULL AUTO_INCREMENT,
    `company_id`    BIGINT UNSIGNED                                                   NOT NULL DEFAULT '0',
    `email`         VARCHAR(100)                                                      NOT NULL DEFAULT '',
    `password`      VARCHAR(60) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'  NOT NULL DEFAULT '',
    `first_name`    VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `last_name`     VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `salutation`    VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `registration`  VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `login_date`    VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `phone`         VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `mobile`        VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `failed_logins` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `super_admin`   TINYINT(1)                                                        NOT NULL DEFAULT '0',
    `created`       DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`    INT(11) UNSIGNED                                                  NULL,
    `updated`       DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`    INT(11) UNSIGNED                                                  NULL,
    `status`        ENUM ('new', 'active', 'blocked', 'inactive')                     NOT NULL DEFAULT 'new',
    PRIMARY KEY (`id`),
    CONSTRAINT `users_ibfk_1`
        FOREIGN KEY (`company_id`)
            REFERENCES `company` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 83
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `company_id` ON `users` (`company_id` ASC);


-- -----------------------------------------------------
-- Table `oauth2_tokens`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oauth2_tokens`;

CREATE TABLE IF NOT EXISTS `oauth2_tokens`
(
    `id`            BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id`       BIGINT UNSIGNED NOT NULL,
    `access_tokens` VARCHAR(255)    NOT NULL,
    `expire_time`   VARCHAR(255)    NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `oauth2_tokens_ibfk_1`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 36
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `user_id` ON `oauth2_tokens` (`user_id` ASC);


-- -----------------------------------------------------
-- Table `question_context`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `question_context`;

CREATE TABLE IF NOT EXISTS `question_context`
(
    `id`               BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `context_category` VARCHAR(60)      NOT NULL COMMENT 'knowledge check:  possible  results like [yes||no]0, [true||false] answers\\ninformation harvesting: possible results like  ckeckbox answers e.g. [{ \"survey-id\" : \"4711\", \"question-id\" : \"8654\",   \"question\" : \"blabla?\", \"answer\" : \"checked\" }, { \"survey-id\" : \"4711\", \"question-id\" : \"8654\",   \"question\" : \"blablaXY?\", \"answer\" : \"unchecked\" }] ',
    `created_at`       DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`       INT(11) UNSIGNED NULL COMMENT 'decoupled users.id',
    `updated_at`       DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`       INT(11) UNSIGNED NULL COMMENT 'users.id',
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 3
    DEFAULT CHARACTER SET = utf8mb4
    COMMENT = 'NEW. VERSION 1.1 question_context TABLE BY JSR.';


-- -----------------------------------------------------
-- Table `question_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `question_types`;

CREATE TABLE IF NOT EXISTS `question_types`
(
    `id`                          BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `question_type_html_tag`      VARCHAR(40)      NOT NULL COMMENT 'inital values for html_tag:  select,  range, textarea, checkbox',
    `question_type_html_template` MEDIUMTEXT       NULL     DEFAULT NULL COMMENT 'optional pre-defined html template for select,  checkboxes and so on',
    `question_type_frontend_hint` VARCHAR(255)     NULL     DEFAULT NULL COMMENT 'human synonmys for html tags to show in frontend\\nselect => Dropdown-list\\nrange => Slider\\ntextarea => Freitext-Antwort\\ncheckbox => Fragenkatalog',
    `question_type_description`   TEXT             NULL     DEFAULT NULL,
    `created_at`                  DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`                  INT(11) UNSIGNED NULL COMMENT 'decoupled users.id',
    `updated_at`                  DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`                  INT(11) UNSIGNED NULL COMMENT 'decoupled users.id',
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 5
    DEFAULT CHARACTER SET = utf8mb4
    COMMENT = 'NEW. VERSION 1.1 question_types TABLE BY JSR.';


-- -----------------------------------------------------
-- Table `questions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `questions`;

CREATE TABLE IF NOT EXISTS `questions`
(
    `id`                           BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `survey_id`                    BIGINT UNSIGNED  NOT NULL,
    `context_id`                   BIGINT UNSIGNED  NOT NULL,
    `question_types_id`            BIGINT UNSIGNED  NOT NULL,
    `question_question_txt`        TEXT             NOT NULL COMMENT 'the survey question',
    `question_hint_txt`            TEXT             NULL     DEFAULT NULL COMMENT 'frontend: text hint for  leads about the question and how to answer it ...',
    `question_json_representation` MEDIUMTEXT       NULL     DEFAULT NULL COMMENT 'MEDIUMTEXT must be changed to JSON datatype in modern MySql Version',
    `created_at`                   DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`                   INT(11) UNSIGNED NULL COMMENT 'decoupled users.id',
    `updated_at`                   DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`                   INT(11) UNSIGNED NULL COMMENT 'decoupled users.id',
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_questions_context1`
        FOREIGN KEY (`context_id`)
            REFERENCES `question_context` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_questions_survey1`
        FOREIGN KEY (`survey_id`)
            REFERENCES `survey` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_questions_question_types1`
        FOREIGN KEY (`question_types_id`)
            REFERENCES `question_types` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 539
    DEFAULT CHARACTER SET = utf8mb4
    COMMENT = 'ALTERED. VERSION 1.1 QUESTION TABLE BY JSR';

CREATE INDEX `fk_questions_survey1_idx` ON `questions` (`survey_id` ASC);

CREATE INDEX `fk_questions_context1_idx` ON `questions` (`context_id` ASC);

CREATE INDEX `fk_questions_question_types1_idx` ON `questions` (`question_types_id` ASC);


-- -----------------------------------------------------
-- Table `securelog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `securelog`;

CREATE TABLE IF NOT EXISTS `securelog`
(
    `id`         BIGINT UNSIGNED                                                   NOT NULL AUTO_INCREMENT,
    `user_id`    BIGINT UNSIGNED                                                   NOT NULL DEFAULT 1,
    `company_id` BIGINT UNSIGNED                                                   NULL COMMENT 'delete that col since ref tab users has company_id',
    `logindate`  DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ip`         VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT '',
    `result`     ENUM ('success', 'failure')                                       NOT NULL DEFAULT 'success',
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_securelog_users`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 418
    DEFAULT CHARACTER SET = utf8mb4;

CREATE INDEX `user_id` ON `securelog` (`user_id` ASC);

CREATE INDEX `company_id` ON `securelog` (`company_id` ASC);


-- -----------------------------------------------------
-- Table `whitelist_domains`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `whitelist_domains`;

CREATE TABLE IF NOT EXISTS `whitelist_domains`
(
    `id`          BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
    `domain_name` VARCHAR(255)     NULL     DEFAULT NULL COMMENT 'White listed domain names',
    `created`     DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`  INT(11) UNSIGNED NULL,
    `updated`     DATETIME         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`  INT(11) UNSIGNED NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 37
    DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `customfields`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `customfields`;

CREATE TABLE IF NOT EXISTS `customfields`
(
    `id`                BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `campaign_id`       BIGINT UNSIGNED  NOT NULL,
    `fieldname`         VARCHAR(45)      NULL,
    `datatype`          VARCHAR(45)      NULL,
    `regexp_validation` TEXT             NULL,
    `created`           DATETIME         NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`        INT(11) UNSIGNED NULL,
    `updated`           DATETIME         NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`        INT(11) UNSIGNED NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_cf_campaign`
        FOREIGN KEY (`campaign_id`)
            REFERENCES `campaign` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;

CREATE INDEX `fk_cf_campaign_idx` ON `customfields` (`campaign_id` ASC);


-- -----------------------------------------------------
-- Table `lead_customfield_content`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lead_customfield_content`;

CREATE TABLE IF NOT EXISTS `lead_customfield_content`
(
    `leads_id`                 BIGINT UNSIGNED NOT NULL,
    `customfields_id`          BIGINT UNSIGNED NOT NULL,
    `lead_customfield_content` MEDIUMTEXT      NULL,
    PRIMARY KEY (`leads_id`, `customfields_id`),
    CONSTRAINT `fk_lcc_leads`
        FOREIGN KEY (`leads_id`)
            REFERENCES `leads` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_lcc_customfields`
        FOREIGN KEY (`customfields_id`)
            REFERENCES `customfields` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;

CREATE INDEX `fk_lcc_customfields_idx` ON `lead_customfield_content` (`customfields_id` ASC);


-- -----------------------------------------------------
-- Table `lead_answer_content`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lead_answer_content`;

CREATE TABLE IF NOT EXISTS `lead_answer_content`
(
    `leads_id`            BIGINT UNSIGNED NOT NULL,
    `questions_id`        BIGINT UNSIGNED NOT NULL,
    `lead_answer_content` MEDIUMTEXT      NULL,
    PRIMARY KEY (`leads_id`, `questions_id`),
    CONSTRAINT `fk_lac_leads`
        FOREIGN KEY (`leads_id`)
            REFERENCES `leads` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_lac_questions`
        FOREIGN KEY (`questions_id`)
            REFERENCES `questions` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;

CREATE INDEX `fk_lac_questions_idx` ON `lead_answer_content` (`questions_id` ASC);


-- -----------------------------------------------------
-- Table `company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `company`;

CREATE TABLE IF NOT EXISTS `company`
(
    `id`               BIGINT(20) UNSIGNED                                               NOT NULL AUTO_INCREMENT,
    `account_no`       VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `name`             VARCHAR(150) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `street`           VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `house_number`     VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `postal_code`      VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `city`             VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `country`          VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `website`          VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `phone`            VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `vat`              VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `login_url`        VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `delegated_domain` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'Only when URL option is delegated domain',
    `url_option`       VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT 'folder' COMMENT 'folder, subdomain, deligate domain ',
    `created`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`       INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    `updated`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`       INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    `modification`     VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 19
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE UNIQUE INDEX `uidx_account_no` ON `company` (`account_no` ASC);

CREATE INDEX `id` ON `company` (`id` ASC);


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `users`;

CREATE TABLE IF NOT EXISTS `users`
(
    `id`            BIGINT(20) UNSIGNED                                                                                NOT NULL AUTO_INCREMENT,
    `company_id`    BIGINT(20) UNSIGNED                                                                                NOT NULL DEFAULT '0',
    `email`         VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                  NOT NULL DEFAULT '',
    `password`      VARCHAR(60) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                   NOT NULL DEFAULT '',
    `first_name`    VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                  NOT NULL DEFAULT '',
    `last_name`     VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                  NOT NULL DEFAULT '',
    `salutation`    VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                  NOT NULL DEFAULT '',
    `registration`  VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                  NOT NULL DEFAULT '',
    `login_date`    VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                  NOT NULL DEFAULT '',
    `phone`         VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                  NOT NULL DEFAULT '',
    `mobile`        VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                  NOT NULL DEFAULT '',
    `failed_logins` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                                  NOT NULL DEFAULT '',
    `super_admin`   TINYINT(1)                                                                                         NOT NULL DEFAULT '0',
    `created`       DATETIME                                                                                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`    INT(11) UNSIGNED                                                                                   NULL     DEFAULT NULL,
    `updated`       DATETIME                                                                                           NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`    INT(11) UNSIGNED                                                                                   NULL     DEFAULT NULL,
    `status`        ENUM ('new', 'active', 'blocked', 'inactive') CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT 'new',
    PRIMARY KEY (`id`),
    CONSTRAINT `users_ibfk_1`
        FOREIGN KEY (`company_id`)
            REFERENCES `company` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 83
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `company_id` ON `users` (`company_id` ASC);


-- -----------------------------------------------------
-- Table `administrates_companies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `administrates_companies`;

CREATE TABLE IF NOT EXISTS `administrates_companies`
(
    `id`         BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id`    BIGINT(20) UNSIGNED NOT NULL,
    `company_id` BIGINT(20) UNSIGNED NOT NULL,
    `created`    DATETIME            NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`    DATETIME            NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT(11) UNSIGNED    NULL DEFAULT NULL,
    `updated_by` INT(11) UNSIGNED    NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_admin_comp_company_id`
        FOREIGN KEY (`company_id`)
            REFERENCES `company` (`id`)
            ON DELETE CASCADE,
    CONSTRAINT `fk_admin_comp_user_id`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
            ON DELETE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE UNIQUE INDEX `UK_admin_comp_user_id_company_id` ON `administrates_companies` (`user_id` ASC, `company_id` ASC);

CREATE INDEX `fk_admin_comp_company_id_idx` ON `administrates_companies` (`company_id` ASC);

CREATE INDEX `fk_admin_comp_user_id_idx` ON `administrates_companies` (`user_id` ASC);


-- -----------------------------------------------------
-- Table `administrates_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `administrates_users`;

CREATE TABLE IF NOT EXISTS `administrates_users`
(
    `id`              BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id`         BIGINT(20) UNSIGNED NOT NULL,
    `invited_user_id` BIGINT(20) UNSIGNED NOT NULL,
    `to_company_id`   BIGINT(20) UNSIGNED NOT NULL,
    `with_roles`      TEXT                NOT NULL,
    `created`         DATETIME            NULL DEFAULT CURRENT_TIMESTAMP,
    `updated`         DATETIME            NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`      INT(11) UNSIGNED    NULL DEFAULT NULL,
    `updated_by`      INT(11) UNSIGNED    NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_administrates_users_invited_user_id`
        FOREIGN KEY (`invited_user_id`)
            REFERENCES `users` (`id`),
    CONSTRAINT `fk_administrates_users_to_company_id`
        FOREIGN KEY (`to_company_id`)
            REFERENCES `company` (`id`)
            ON DELETE CASCADE,
    CONSTRAINT `fk_administrates_users_user_id`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
            ON DELETE CASCADE
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE UNIQUE INDEX `uk_administrates_users` ON `administrates_users` (`user_id` ASC, `invited_user_id` ASC, `to_company_id` ASC);

CREATE INDEX `fk_administrates_users_to_company_id_idx` ON `administrates_users` (`to_company_id` ASC);

CREATE INDEX `fk_administrates_users_user_id_idx` ON `administrates_users` (`user_id` ASC);

CREATE INDEX `fk_administrates_users_invited_user_id_idx` ON `administrates_users` (`invited_user_id` ASC);


-- -----------------------------------------------------
-- Table `blacklist_domains`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blacklist_domains`;

CREATE TABLE IF NOT EXISTS `blacklist_domains`
(
    `id`          BIGINT(20) UNSIGNED                                               NOT NULL AUTO_INCREMENT,
    `domain_name` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'Blacklisted Domain names',
    `created`     DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`  INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    `updated`     DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`  INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 75
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;


-- -----------------------------------------------------
-- Table `status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `status`;

CREATE TABLE IF NOT EXISTS `status`
(
    `id`         BIGINT(20) UNSIGNED                                              NOT NULL AUTO_INCREMENT,
    `status_txt` VARCHAR(45) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `created_at` DATETIME                                                         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT(11) UNSIGNED                                                 NULL     DEFAULT NULL COMMENT 'decoupled users.id',
    `updated_at` DATETIME                                                         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by` INT(11) UNSIGNED                                                 NULL     DEFAULT NULL COMMENT 'decoupled users.id',
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 13
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci
    COMMENT = 'NEW. VERSION 1.1 status TABLE BY JSR. REPLACES ENUM-TYPES IN FUTURE VERSIONS';


-- -----------------------------------------------------
-- Table `survey`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `survey`;

CREATE TABLE IF NOT EXISTS `survey`
(
    `id`                 BIGINT(20) UNSIGNED                                       NOT NULL AUTO_INCREMENT,
    `layout_status_id`   BIGINT(20) UNSIGNED                                       NULL     DEFAULT NULL COMMENT 'key from status table (Carousel or Single page)',
    `survey_is_online`   TINYINT(1)                                                NOT NULL DEFAULT '0' COMMENT 'flag to turn survey online (1)  or offline (0). default is on ( 1 )\\\\n',
    `survey_description` TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'description of survey, add customer requirements or comments of the survey-maker here. add textarea to frontend.',
    `created_at`         DATETIME                                                  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`         INT(11) UNSIGNED                                          NULL     DEFAULT NULL COMMENT 'decoupled users.id',
    `updated_at`         DATETIME                                                  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`         INT(11) UNSIGNED                                          NULL     DEFAULT NULL COMMENT 'decoupled users.id',
    PRIMARY KEY (`id`),
    CONSTRAINT `survey_status1`
        FOREIGN KEY (`layout_status_id`)
            REFERENCES `status` (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 77
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci
    COMMENT = 'NEW. VERSION 1.1 survey TABLE BY JSR.';

CREATE INDEX `survey_status1_idx` ON `survey` (`layout_status_id` ASC);


-- -----------------------------------------------------
-- Table `campaign`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `campaign`;

CREATE TABLE IF NOT EXISTS `campaign`
(
    `id`             BIGINT(20) UNSIGNED                                                                      NOT NULL AUTO_INCREMENT,
    `company_id`     BIGINT(20) UNSIGNED                                                                      NOT NULL,
    `survey_id`      BIGINT(20) UNSIGNED                                                                      NULL     DEFAULT NULL,
    `name`           VARCHAR(80) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                         NOT NULL,
    `account_no`     VARCHAR(40) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                         NULL     DEFAULT '',
    `account`        VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                        NOT NULL,
    `url`            VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                        NOT NULL,
    `comment`        VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                        NOT NULL,
    `activationdate` DATE                                                                                     NOT NULL,
    `activationby`   INT(100)                                                                                 NOT NULL,
    `archivedate`    DATE                                                                                     NOT NULL,
    `archiveby`      INT(100)                                                                                 NOT NULL,
    `version`        VARCHAR(40) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                         NOT NULL,
    `status`         ENUM ('draft', 'active', 'archive') CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `created`        DATETIME                                                                                 NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`     INT(11) UNSIGNED                                                                         NULL     DEFAULT NULL,
    `updated`        DATETIME                                                                                 NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`     INT(11) UNSIGNED                                                                         NULL     DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `campaign_ibfk_1`
        FOREIGN KEY (`company_id`)
            REFERENCES `company` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT `ff_campaign_survey`
        FOREIGN KEY (`survey_id`)
            REFERENCES `survey` (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 851
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE UNIQUE INDEX `uk_campaign_name_version_status_compid` ON `campaign` (`name` ASC, `version` ASC, `status` ASC, `company_id` ASC);

CREATE INDEX `company_id` ON `campaign` (`company_id` ASC);

CREATE INDEX `ff_campaign_survey_idx` ON `campaign` (`survey_id` ASC);

CREATE INDEX `idx_campaign_name` ON `campaign` (`name` ASC);

CREATE INDEX `uk_campaign_account_no` ON `campaign` (`account_no` ASC);


-- -----------------------------------------------------
-- Table `configuation_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `configuation_groups`;

CREATE TABLE IF NOT EXISTS `configuation_groups`
(
    `id`         BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`       VARCHAR(100)        NOT NULL DEFAULT '' COMMENT 'campaign_params group_name goes here',
    `desc`       TEXT                NULL     DEFAULT NULL,
    `created`    DATETIME            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by` INT(11) UNSIGNED    NULL     DEFAULT NULL,
    `updated`    DATETIME            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by` INT(11) UNSIGNED    NULL     DEFAULT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;


-- -----------------------------------------------------
-- Table `configuration_targets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `configuration_targets`;

CREATE TABLE IF NOT EXISTS `configuration_targets`
(
    `id`                     BIGINT(20) UNSIGNED NOT NULL,
    `configuration_group_id` BIGINT(20) UNSIGNED NOT NULL,
    `name`                   VARCHAR(100)        NOT NULL DEFAULT '',
    `desc`                   TEXT                NULL     DEFAULT NULL,
    `created`                DATETIME            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`             INT(11) UNSIGNED    NULL     DEFAULT NULL,
    `updated`                DATETIME            NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`             INT(11) UNSIGNED    NULL     DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_target_group`
        FOREIGN KEY (`configuration_group_id`)
            REFERENCES `configuation_groups` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE UNIQUE INDEX `uk_target_group` ON `configuration_targets` (`id` ASC, `configuration_group_id` ASC);

CREATE INDEX `fk_target_group_idx` ON `configuration_targets` (`configuration_group_id` ASC);


-- -----------------------------------------------------
-- Table `campaign_params`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `campaign_params`;

CREATE TABLE IF NOT EXISTS `campaign_params`
(
    `id`                      BIGINT(20) UNSIGNED                                             NOT NULL AUTO_INCREMENT,
    `campaign_Id`             BIGINT(20) UNSIGNED                                             NOT NULL,
    `configuration_target_id` BIGINT(20) UNSIGNED                                             NOT NULL,
    `value`                   MEDIUMTEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT '  MEDIUMTEXT must be changed to JSON datatype in modern MySql Version',
    `created`                 DATETIME                                                        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`              INT(11) UNSIGNED                                                NULL     DEFAULT NULL,
    `updated`                 DATETIME                                                        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`              INT(11) UNSIGNED                                                NULL     DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `campaign_params_ibfk_1`
        FOREIGN KEY (`campaign_Id`)
            REFERENCES `campaign` (`id`),
    CONSTRAINT `fk_params_target`
        FOREIGN KEY (`configuration_target_id`)
            REFERENCES `configuration_targets` (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 80
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `campaign_Id` ON `campaign_params` (`campaign_Id` ASC);

CREATE INDEX `fk_params_target_idx` ON `campaign_params` (`configuration_target_id` ASC);


-- -----------------------------------------------------
-- Table `leads`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `leads`;

CREATE TABLE IF NOT EXISTS `leads`
(
    `id`                     BIGINT(20) UNSIGNED                                                                        NOT NULL AUTO_INCREMENT,
    `campaign_id`            BIGINT(20) UNSIGNED                                                                        NOT NULL DEFAULT '0',
    `email`                  VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `salutation`             VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `first_name`             VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `last_name`              VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `firma`                  VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `street`                 VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `postal_code`            VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `city`                   VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `country`                VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `phone`                  VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `account`                INT(100)                                                                                   NOT NULL DEFAULT '0',
    `url`                    VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `final_url`              VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `trafficsource`          VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `utm_parameters`         VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `campaign`               VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `split_version`          VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `lead_reference`         VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `targetgroup`            VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NULL     DEFAULT '',
    `affiliateID`            VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NULL     DEFAULT '' COMMENT 'Affiliate Id from campaign URL',
    `affiliateSubID`         VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NULL     DEFAULT '' COMMENT 'AffiliateSubId from campaign URL',
    `status`                 ENUM ('active', 'inactive', 'storno') CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT 'active',
    `ip`                     VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `device_type`            VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `device_browser`         VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `device_os`              VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `device_os_version`      VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `device_browser_version` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                          NOT NULL DEFAULT '',
    `created`                DATETIME                                                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`             INT(11) UNSIGNED                                                                           NULL     DEFAULT NULL,
    `updated`                DATETIME                                                                                   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`             INT(11) UNSIGNED                                                                           NULL     DEFAULT NULL,
    `modification`           INT(100)                                                                                   NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_leads_campaign`
        FOREIGN KEY (`campaign_id`)
            REFERENCES `campaign` (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 15148
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `campaign_id` ON `leads` (`campaign_id` ASC);

CREATE INDEX `account` ON `leads` (`account` ASC);


-- -----------------------------------------------------
-- Table `customer_journey`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `customer_journey`;

CREATE TABLE IF NOT EXISTS `customer_journey`
(
    `id`               BIGINT(20) UNSIGNED                                               NOT NULL AUTO_INCREMENT,
    `lead_id`          BIGINT(20) UNSIGNED                                               NOT NULL COMMENT 'Lead id from leads',
    `customer_journey` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'Format  : \'trafficsource\' - \'datetime\'\'',
    `created`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`       INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    `updated`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`       INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `cj_leads_ibfk_1`
        FOREIGN KEY (`lead_id`)
            REFERENCES `leads` (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 611
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `Index` ON `customer_journey` (`lead_id` ASC);


-- -----------------------------------------------------
-- Table `customfields`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `customfields`;

CREATE TABLE IF NOT EXISTS `customfields`
(
    `id`                BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `campaign_id`       BIGINT(20) UNSIGNED NOT NULL,
    `fieldname`         VARCHAR(45)         NULL DEFAULT NULL,
    `datatype`          VARCHAR(45)         NULL DEFAULT NULL,
    `regexp_validation` TEXT                NULL DEFAULT NULL,
    `created`           DATETIME            NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`        INT(11) UNSIGNED    NULL DEFAULT NULL,
    `updated`           DATETIME            NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`        INT(11) UNSIGNED    NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_cf_campaign`
        FOREIGN KEY (`campaign_id`)
            REFERENCES `campaign` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `fk_cf_campaign_idx` ON `customfields` (`campaign_id` ASC);


-- -----------------------------------------------------
-- Table `fraud`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fraud`;

CREATE TABLE IF NOT EXISTS `fraud`
(
    `id`             BIGINT(20) UNSIGNED                                               NOT NULL AUTO_INCREMENT,
    `campaign_id`    BIGINT(20) UNSIGNED                                               NOT NULL COMMENT 'Campaign Id ',
    `type`           VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'IP Block, Blacklist Domain, Fraud Limit',
    `email`          VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'Client email id',
    `domain`         VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'Client Domain name',
    `ip`             VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'Client Ip address',
    `device_browser` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'Client Browser',
    `device_os`      VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'Client OS name ',
    `time_stamp`     DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Current time stamp',
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_fraud_campaign_id`
        FOREIGN KEY (`campaign_id`)
            REFERENCES `campaign` (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 163
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `fk_fraud_campaign_id` ON `fraud` (`campaign_id` ASC);


-- -----------------------------------------------------
-- Table `question_context`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `question_context`;

CREATE TABLE IF NOT EXISTS `question_context`
(
    `id`               BIGINT(20) UNSIGNED                                              NOT NULL AUTO_INCREMENT,
    `context_category` VARCHAR(60) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'knowledge check:  possible  results like [yes||no]0, [true||false] answers\\\\ninformation harvesting: possible results like  ckeckbox answers e.g. [{ \"survey-id\" : \"4711\", \"question-id\" : \"8654\",   \"question\" : \"blabla?\", \"answer\" : \"checked\" }, { \"survey-id\" : \"4711\", \"question-id\" : \"8654\",   \"question\" : \"blablaXY?\", \"answer\" : \"unchecked\" }] ',
    `created_at`       DATETIME                                                         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`       INT(11) UNSIGNED                                                 NULL     DEFAULT NULL COMMENT 'decoupled users.id',
    `updated_at`       DATETIME                                                         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`       INT(11) UNSIGNED                                                 NULL     DEFAULT NULL COMMENT 'users.id',
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 3
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci
    COMMENT = 'NEW. VERSION 1.1 question_context TABLE BY JSR.';


-- -----------------------------------------------------
-- Table `question_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `question_types`;

CREATE TABLE IF NOT EXISTS `question_types`
(
    `id`                          BIGINT(20) UNSIGNED                                               NOT NULL AUTO_INCREMENT,
    `question_type_html_tag`      VARCHAR(40) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'  NOT NULL COMMENT 'inital values for html_tag:  select,  range, textarea, checkbox',
    `question_type_html_template` MEDIUMTEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'   NULL     DEFAULT NULL COMMENT 'optional pre-defined html template for select,  checkboxes and so on',
    `question_type_frontend_hint` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'human synonmys for html tags to show in frontend\\\\nselect => Dropdown-list\\\\nrange => Slider\\\\ntextarea => Freitext-Antwort\\\\ncheckbox => Fragenkatalog',
    `question_type_description`   TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'         NULL     DEFAULT NULL,
    `created_at`                  DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`                  INT(11) UNSIGNED                                                  NULL     DEFAULT NULL COMMENT 'decoupled users.id',
    `updated_at`                  DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`                  INT(11) UNSIGNED                                                  NULL     DEFAULT NULL COMMENT 'decoupled users.id',
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 5
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci
    COMMENT = 'NEW. VERSION 1.1 question_types TABLE BY JSR.';


-- -----------------------------------------------------
-- Table `questions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `questions`;

CREATE TABLE IF NOT EXISTS `questions`
(
    `id`                           BIGINT(20) UNSIGNED                                             NOT NULL AUTO_INCREMENT,
    `survey_id`                    BIGINT(20) UNSIGNED                                             NOT NULL,
    `context_id`                   BIGINT(20) UNSIGNED                                             NOT NULL,
    `question_types_id`            BIGINT(20) UNSIGNED                                             NOT NULL,
    `question_question_txt`        TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'       NOT NULL COMMENT 'the survey question',
    `question_hint_txt`            TEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'       NULL     DEFAULT NULL COMMENT 'frontend: text hint for  leads about the question and how to answer it ...',
    `question_json_representation` MEDIUMTEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'MEDIUMTEXT must be changed to JSON datatype in modern MySql Version',
    `created_at`                   DATETIME                                                        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`                   INT(11) UNSIGNED                                                NULL     DEFAULT NULL COMMENT 'decoupled users.id',
    `updated_at`                   DATETIME                                                        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`                   INT(11) UNSIGNED                                                NULL     DEFAULT NULL COMMENT 'decoupled users.id',
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_questions_context1`
        FOREIGN KEY (`context_id`)
            REFERENCES `question_context` (`id`),
    CONSTRAINT `fk_questions_question_types1`
        FOREIGN KEY (`question_types_id`)
            REFERENCES `question_types` (`id`),
    CONSTRAINT `fk_questions_survey1`
        FOREIGN KEY (`survey_id`)
            REFERENCES `survey` (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 539
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci
    COMMENT = 'ALTERED. VERSION 1.1 QUESTION TABLE BY JSR';

CREATE INDEX `fk_questions_survey1_idx` ON `questions` (`survey_id` ASC);

CREATE INDEX `fk_questions_context1_idx` ON `questions` (`context_id` ASC);

CREATE INDEX `fk_questions_question_types1_idx` ON `questions` (`question_types_id` ASC);


-- -----------------------------------------------------
-- Table `images`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `images`;

CREATE TABLE IF NOT EXISTS `images`
(
    `id`          BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `question_id` BIGINT(20) UNSIGNED NOT NULL,
    `option_id`   BIGINT(20) UNSIGNED NULL DEFAULT NULL,
    `imgtype`     VARCHAR(10)         NULL DEFAULT NULL,
    `img`         LONGTEXT            NULL DEFAULT NULL,
    `created`     DATETIME            NULL DEFAULT NULL,
    `created_by`  INT(11) UNSIGNED    NULL DEFAULT NULL,
    `updated`     DATETIME            NULL DEFAULT NULL,
    `updated_by`  INT(11) UNSIGNED    NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_images_questions`
        FOREIGN KEY (`question_id`)
            REFERENCES `questions` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `fk_images_questions_idx` ON `images` (`question_id` ASC);


-- -----------------------------------------------------
-- Table `integrations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `integrations`;

CREATE TABLE IF NOT EXISTS `integrations`
(
    `id`                     BIGINT(20) UNSIGNED                                               NOT NULL AUTO_INCREMENT,
    `company_id`             BIGINT(20) UNSIGNED                                               NOT NULL,
    `configuration_group_id` BIGINT(20) UNSIGNED                                               NOT NULL,
    `type`                   VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL,
    `api_key`                VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL,
    `user`                   VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL,
    `password`               VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL,
    `url`                    VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL,
    `account_no`             VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'delete since it is replaced by company_id',
    `first_name`             VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL,
    `last_name`              VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL,
    `owner_id`               VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL,
    `sync`                   VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL,
    `additional_settings`    MEDIUMTEXT CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'   NULL     DEFAULT NULL COMMENT '  MEDIUMTEXT must be changed to JSON datatype in modern MySql Version',
    `created`                DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`             INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    `updated`                DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`             INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_integration_company`
        FOREIGN KEY (`company_id`)
            REFERENCES `company` (`id`),
    CONSTRAINT `fk_integration_config_group`
        FOREIGN KEY (`configuration_group_id`)
            REFERENCES `configuation_groups` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `account_no` ON `integrations` (`account_no` ASC);

CREATE INDEX `fk_integration_company_idx` ON `integrations` (`company_id` ASC);

CREATE INDEX `fk_integration_config_group_idx` ON `integrations` (`configuration_group_id` ASC);


-- -----------------------------------------------------
-- Table `integrations_meta`
-- -----------------------------------------------------
CREATE TABLE `integrations_meta`
(
    `id`              bigint(20) unsigned                     NOT NULL AUTO_INCREMENT,
    `integrations_id` bigint(20) unsigned                              DEFAULT NULL,
    `meta_key`        varchar(190) COLLATE utf8mb4_german2_ci NOT NULL DEFAULT 'undefined',
    `meta_value`      mediumtext COLLATE utf8mb4_german2_ci,
    `created`         datetime                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`      int(11) unsigned                                 DEFAULT NULL,
    `updated`         datetime                                NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`      int(11) unsigned                                 DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `fk_integrations_meta_integrations_idx` (`integrations_id`),
    KEY `idx_meta_key` (`meta_key`),
    CONSTRAINT `fk_integrations_meta_integrations` FOREIGN KEY (`integrations_id`) REFERENCES `integrations` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_german2_ci;



-- -----------------------------------------------------
-- Table `lead_answer_content`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lead_answer_content`;

CREATE TABLE IF NOT EXISTS `lead_answer_content`
(
    `id`                  BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `leads_id`            BIGINT(20) UNSIGNED NOT NULL,
    `questions_id`        BIGINT(20) UNSIGNED NOT NULL,
    `lead_answer_content` MEDIUMTEXT          NULL DEFAULT NULL,
    `lead_answer_score`   DECIMAL(15, 2)      NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_lac_leads`
        FOREIGN KEY (`leads_id`)
            REFERENCES `leads` (`id`),
    CONSTRAINT `fk_lac_questions`
        FOREIGN KEY (`questions_id`)
            REFERENCES `questions` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE UNIQUE INDEX `uidx_leads_questions` ON `lead_answer_content` (`leads_id` ASC, `questions_id` ASC);

CREATE INDEX `fk_lac_questions_idx` ON `lead_answer_content` (`questions_id` ASC);


-- -----------------------------------------------------
-- Table `lead_customfield_content`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `lead_customfield_content`;

CREATE TABLE IF NOT EXISTS `lead_customfield_content`
(
    `leads_id`                 BIGINT(20) UNSIGNED NOT NULL,
    `customfields_id`          BIGINT(20) UNSIGNED NOT NULL,
    `lead_customfield_content` MEDIUMTEXT          NULL DEFAULT NULL,
    PRIMARY KEY (`leads_id`, `customfields_id`),
    CONSTRAINT `fk_lcc_customfields`
        FOREIGN KEY (`customfields_id`)
            REFERENCES `customfields` (`id`),
    CONSTRAINT `fk_lcc_leads`
        FOREIGN KEY (`leads_id`)
            REFERENCES `leads` (`id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `fk_lcc_customfields_idx` ON `lead_customfield_content` (`customfields_id` ASC);


-- -----------------------------------------------------
-- Table `mapping_templates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mapping_templates`;

CREATE TABLE IF NOT EXISTS `mapping_templates`
(
    `id`               BIGINT(20) UNSIGNED                                               NOT NULL AUTO_INCREMENT,
    `Campaign-In-One`  VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'CIO Standard Fields',
    `HTML Form`        VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'html Standard fields',
    `Mail-In-One`      VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'MIO Stardard fields',
    `Hubspot`          VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'Hubspot Standard fields',
    `Email Forwarding` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'Email Standard fields',
    `Field type`       VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL COMMENT 'standard fields',
    `created`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created by User Id',
    `created_by`       INT(11) UNSIGNED                                                  NULL     DEFAULT NULL COMMENT 'Created Date',
    `updated`          DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Updated by User Id',
    `updated_by`       INT(11) UNSIGNED                                                  NULL     DEFAULT NULL COMMENT 'Updated Date',
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 27
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;


-- -----------------------------------------------------
-- Table `oauth2_tokens`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `oauth2_tokens`;

CREATE TABLE IF NOT EXISTS `oauth2_tokens`
(
    `id`            BIGINT(20) UNSIGNED                                               NOT NULL AUTO_INCREMENT,
    `user_id`       BIGINT(20) UNSIGNED                                               NOT NULL,
    `access_tokens` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    `expire_time`   VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `oauth2_tokens_ibfk_1`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
            ON DELETE CASCADE
            ON UPDATE CASCADE
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 36
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `user_id` ON `oauth2_tokens` (`user_id` ASC);


-- -----------------------------------------------------
-- Table `securelog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `securelog`;

CREATE TABLE IF NOT EXISTS `securelog`
(
    `id`         BIGINT(20) UNSIGNED                                                              NOT NULL AUTO_INCREMENT,
    `user_id`    BIGINT(20) UNSIGNED                                                              NOT NULL DEFAULT '1',
    `company_id` BIGINT(20) UNSIGNED                                                              NULL     DEFAULT NULL COMMENT 'delete that col since ref tab users has company_id',
    `logindate`  DATETIME                                                                         NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `ip`         VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci'                NOT NULL DEFAULT '',
    `result`     ENUM ('success', 'failure') CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NOT NULL DEFAULT 'success',
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_securelog_users`
        FOREIGN KEY (`user_id`)
            REFERENCES `users` (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 418
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

CREATE INDEX `user_id` ON `securelog` (`user_id` ASC);

CREATE INDEX `company_id` ON `securelog` (`company_id` ASC);


-- -----------------------------------------------------
-- Table `sessions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sessions`;

CREATE TABLE IF NOT EXISTS `sessions`
(
    `sess_id`       VARCHAR(128) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_bin' NOT NULL,
    `sess_data`     BLOB                                                       NOT NULL,
    `sess_time`     INT(10) UNSIGNED                                           NOT NULL,
    `sess_lifetime` INT(10) UNSIGNED                                           NOT NULL,
    PRIMARY KEY (`sess_id`)
)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;


-- -----------------------------------------------------
-- Table `whitelist_domains`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `whitelist_domains`;

CREATE TABLE IF NOT EXISTS `whitelist_domains`
(
    `id`          BIGINT(20) UNSIGNED                                               NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
    `domain_name` VARCHAR(255) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_german2_ci' NULL     DEFAULT NULL COMMENT 'White listed domain names',
    `created`     DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `created_by`  INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    `updated`     DATETIME                                                          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_by`  INT(11) UNSIGNED                                                  NULL     DEFAULT NULL,
    PRIMARY KEY (`id`)
)
    ENGINE = InnoDB
    AUTO_INCREMENT = 37
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;


-- -----------------------------------------------------
-- View `v_campaign_lead_infos`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `v_campaign_lead_infos`;
CREATE OR REPLACE VIEW `v_campaign_lead_infos` AS
select `c`.`id`                         AS `CampaignId`,
       `c`.`company_id`                 AS `CompanyId`,
       `c`.`name`                       AS `Campaign Name`,
       `l`.`id`                         AS `lead_id`,
       `l`.`lead_reference`             AS `Lead Reference`,
       `l`.`created`                    AS `Created`,
       `l`.`salutation`                 AS `Salutation`,
       `l`.`first_name`                 AS `Firstname`,
       `l`.`last_name`                  AS `Lastname`,
       `l`.`email`                      AS `Email`,
       `l`.`postal_code`                AS `Postalcode`,
       `l`.`status`                     AS `Status`,
       `l`.`trafficsource`              AS `Trafficsource`,
       `l`.`utm_parameters`             AS `UTM Parameters`,
       `l`.`affiliateID`                AS `AffiliateID`,
       `l`.`affiliateSubID`             AS `AffiliateSubID`,
       `cf`.`fieldname`                 AS `name`,
       `lcf`.`lead_customfield_content` AS `value`
from (((`campaign` `c` join `leads` `l` on ((`l`.`campaign_id` = `c`.`id`))) join `customfields` `cf` on ((`cf`.`campaign_id` = `c`.`id`)))
         join `lead_customfield_content` `lcf` on (((`lcf`.`leads_id` = `l`.`id`) and (`lcf`.`leads_id` = `l`.`id`))))
union
select `c`.`id`                    AS `CampaignId`,
       `c`.`company_id`            AS `CompanyId`,
       `c`.`name`                  AS `Campaign Name`,
       `l`.`id`                    AS `lead_id`,
       `l`.`lead_reference`        AS `Lead Reference`,
       `l`.`created`               AS `Created`,
       `l`.`salutation`            AS `Salutation`,
       `l`.`first_name`            AS `Firstname`,
       `l`.`last_name`             AS `Lastname`,
       `l`.`email`                 AS `Email`,
       `l`.`postal_code`           AS `Postalcode`,
       `l`.`status`                AS `Status`,
       `l`.`trafficsource`         AS `Trafficsource`,
       `l`.`utm_parameters`        AS `UTM Parameters`,
       `l`.`affiliateID`           AS `AffiliateID`,
       `l`.`affiliateSubID`        AS `AffiliateSubID`,
       `q`.`question_question_txt` AS `name`,
       `lac`.`lead_answer_content` AS `value`
from ((((`campaign` `c` join `leads` `l` on ((`l`.`campaign_id` = `c`.`id`))) join `survey` `s` on ((`s`.`id` = `c`.`survey_id`))) join `questions` `q` on ((`q`.`survey_id` = `s`.`id`)))
         join `lead_answer_content` `lac` on (((`lac`.`leads_id` = `l`.`id`) and (`lac`.`questions_id` = `q`.`id`))));

-- -----------------------------------------------------
-- View `v_campaign_leads_customfields`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `v_campaign_leads_customfields`;
CREATE OR REPLACE VIEW `v_campaign_leads_customfields` AS
select `c`.`id`                         AS `CampaignId`,
       `c`.`company_id`                 AS `CompanyId`,
       `c`.`name`                       AS `Campaign Name`,
       `l`.`id`                         AS `lead_id`,
       `l`.`lead_reference`             AS `Lead Reference`,
       `l`.`created`                    AS `Created`,
       `l`.`salutation`                 AS `Salutation`,
       `l`.`first_name`                 AS `Firstname`,
       `l`.`last_name`                  AS `Lastname`,
       `l`.`email`                      AS `Email`,
       `l`.`postal_code`                AS `Postalcode`,
       `l`.`status`                     AS `Status`,
       `l`.`trafficsource`              AS `Trafficsource`,
       `l`.`utm_parameters`             AS `UTM Parameters`,
       `l`.`affiliateID`                AS `AffiliateID`,
       `l`.`affiliateSubID`             AS `AffiliateSubID`,
       `cf`.`fieldname`                 AS `name`,
       `lcf`.`lead_customfield_content` AS `value`
from (((`campaign` `c` join `leads` `l` on ((`l`.`campaign_id` = `c`.`id`))) join `customfields` `cf` on ((`cf`.`campaign_id` = `c`.`id`)))
         join `lead_customfield_content` `lcf` on (((`lcf`.`leads_id` = `l`.`id`) and (`lcf`.`leads_id` = `l`.`id`))));

-- -----------------------------------------------------
-- View `v_campaign_leads_survey_questions`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `v_campaign_leads_survey_questions`;
CREATE OR REPLACE VIEW `v_campaign_leads_survey_questions` AS
select `c`.`id`                    AS `CampaignId`,
       `c`.`company_id`            AS `CompanyId`,
       `c`.`name`                  AS `Campaign Name`,
       `l`.`id`                    AS `lead_id`,
       `l`.`lead_reference`        AS `Lead Reference`,
       `l`.`created`               AS `Created`,
       `l`.`salutation`            AS `Salutation`,
       `l`.`first_name`            AS `Firstname`,
       `l`.`last_name`             AS `Lastname`,
       `l`.`email`                 AS `Email`,
       `l`.`postal_code`           AS `Postalcode`,
       `l`.`status`                AS `Status`,
       `l`.`trafficsource`         AS `Trafficsource`,
       `l`.`utm_parameters`        AS `UTM Parameters`,
       `l`.`affiliateID`           AS `AffiliateID`,
       `l`.`affiliateSubID`        AS `AffiliateSubID`,
       `q`.`question_question_txt` AS `name`,
       `lac`.`lead_answer_content` AS `value`
from ((((`campaign` `c` join `leads` `l` on ((`l`.`campaign_id` = `c`.`id`))) join `survey` `s` on ((`s`.`id` = `c`.`survey_id`))) join `questions` `q` on ((`q`.`survey_id` = `s`.`id`)))
         join `lead_answer_content` `lac` on (((`lac`.`leads_id` = `l`.`id`) and (`lac`.`questions_id` = `q`.`id`))));

DELIMITER $$


DROP TRIGGER IF EXISTS `mapping_templates_BEFORE_INSERT` $$
CREATE DEFINER = CURRENT_USER TRIGGER `mapping_templates_BEFORE_INSERT`
    BEFORE INSERT
    ON `mapping_templates`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `mapping_templates_BEFORE_UPDATE` $$
CREATE DEFINER = CURRENT_USER TRIGGER `mapping_templates_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `mapping_templates`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `questions_BEFORE_UPDATE` $$
CREATE
    TRIGGER `questions_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `questions`
    FOR EACH ROW
BEGIN
    set new.updated_at = now();
END$$


DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS `company_BEFORE_INSERT` $$
CREATE
    TRIGGER `company_BEFORE_INSERT`
    BEFORE INSERT
    ON `company`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `company_BEFORE_UPDATE` $$
CREATE
    TRIGGER `company_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `company`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `users_BEFORE_INSERT` $$
CREATE
    TRIGGER `users_BEFORE_INSERT`
    BEFORE INSERT
    ON `users`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `users_BEFORE_UPDATE` $$
CREATE
    TRIGGER `users_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `users`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `administrates_companies_BEFORE_INSERT` $$
CREATE
    TRIGGER `administrates_companies_BEFORE_INSERT`
    BEFORE INSERT
    ON `administrates_companies`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `administrates_companies_BEFORE_UPDATE` $$
CREATE
    TRIGGER `administrates_companies_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `administrates_companies`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `administrates_users_BEFORE_INSERT` $$
CREATE
    TRIGGER `administrates_users_BEFORE_INSERT`
    BEFORE INSERT
    ON `administrates_users`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `administrates_users_BEFORE_UPDATE` $$
CREATE
    TRIGGER `administrates_users_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `administrates_users`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `blacklist_domains_BEFORE_INSERT` $$
CREATE
    TRIGGER `blacklist_domains_BEFORE_INSERT`
    BEFORE INSERT
    ON `blacklist_domains`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `blacklist_domains_BEFORE_UPDATE` $$
CREATE
    TRIGGER `blacklist_domains_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `blacklist_domains`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `status_BEFORE_DELETE` $$
CREATE
    TRIGGER `status_BEFORE_DELETE`
    BEFORE DELETE
    ON `status`
    FOR EACH ROW
BEGIN
    signal sqlstate '45003' set message_text =
            " A STATUS SHOULD NOT BE DELETED. IT CAN BE DELETED AFTER DISABLING THE BEFORE DELETE TRIGGER OF THIS TABLE!";
END$$


DROP TRIGGER IF EXISTS `status_BEFORE_INSERT` $$
CREATE
    TRIGGER `status_BEFORE_INSERT`
    BEFORE INSERT
    ON `status`
    FOR EACH ROW
BEGIN
    set new.created_at = now();
END$$


DROP TRIGGER IF EXISTS `status_BEFORE_UPDATE` $$
CREATE
    TRIGGER `status_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `status`
    FOR EACH ROW
BEGIN
    set new.updated_at = now();
END$$


DROP TRIGGER IF EXISTS `survey_BEFORE_INSERT` $$
CREATE
    TRIGGER `survey_BEFORE_INSERT`
    BEFORE INSERT
    ON `survey`
    FOR EACH ROW
BEGIN
    set new.created_at = now();
    set new.updated_at = now();
END$$


DROP TRIGGER IF EXISTS `survey_BEFORE_UPDATE` $$
CREATE
    TRIGGER `survey_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `survey`
    FOR EACH ROW
BEGIN
    set new.updated_at = now();
END$$


DROP TRIGGER IF EXISTS `campaign_BEFORE_INSERT` $$
CREATE
    TRIGGER `campaign_BEFORE_INSERT`
    BEFORE INSERT
    ON `campaign`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `campaign_BEFORE_UPDATE` $$
CREATE
    TRIGGER `campaign_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `campaign`
    FOR EACH ROW
BEGIN

END$$


DROP TRIGGER IF EXISTS `configuation_groups_BEFORE_INSERT` $$
CREATE
    TRIGGER `configuation_groups_BEFORE_INSERT`
    BEFORE INSERT
    ON `configuation_groups`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `configuation_groups_BEFORE_UPDATE` $$
CREATE
    TRIGGER `configuation_groups_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `configuation_groups`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `configuration_targets_BEFORE_INSERT` $$
CREATE
    TRIGGER `configuration_targets_BEFORE_INSERT`
    BEFORE INSERT
    ON `configuration_targets`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `configuration_targets_BEFORE_UPDATE` $$
CREATE
    TRIGGER `configuration_targets_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `configuration_targets`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `campaign_params_BEFORE_INSERT` $$
CREATE
    TRIGGER `campaign_params_BEFORE_INSERT`
    BEFORE INSERT
    ON `campaign_params`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `campaign_params_BEFORE_UPDATE` $$
CREATE
    TRIGGER `campaign_params_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `campaign_params`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `leads_BEFORE_INSERT` $$
CREATE
    TRIGGER `leads_BEFORE_INSERT`
    BEFORE INSERT
    ON `leads`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `leads_BEFORE_UPDATE` $$
CREATE
    TRIGGER `leads_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `leads`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `customer_journey_BEFORE_INSERT` $$
CREATE
    TRIGGER `customer_journey_BEFORE_INSERT`
    BEFORE INSERT
    ON `customer_journey`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `customer_journey_BEFORE_UPDATE` $$
CREATE
    TRIGGER `customer_journey_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `customer_journey`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `fraud_BEFORE_INSERT` $$
CREATE
    TRIGGER `fraud_BEFORE_INSERT`
    BEFORE INSERT
    ON `fraud`
    FOR EACH ROW
BEGIN
    set new.time_stamp = now();
END$$


DROP TRIGGER IF EXISTS `question_context_BEFORE_DELETE` $$
CREATE
    TRIGGER `question_context_BEFORE_DELETE`
    BEFORE DELETE
    ON `question_context`
    FOR EACH ROW
BEGIN
    signal sqlstate '45001' set message_text =
            " A QUESTION CONTEXT CAN ONLY BE DELETED AFTER DISABLING THIS TABLES BEFORE DELETE TRIGGER!";
END$$


DROP TRIGGER IF EXISTS `question_context_BEFORE_INSERT` $$
CREATE
    TRIGGER `question_context_BEFORE_INSERT`
    BEFORE INSERT
    ON `question_context`
    FOR EACH ROW
BEGIN
    set new.created_at = now();
    set new.updated_at = now();
END$$


DROP TRIGGER IF EXISTS `question_context_BEFORE_UPDATE` $$
CREATE
    TRIGGER `question_context_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `question_context`
    FOR EACH ROW
BEGIN
    if (new.id <> old.id) then
        signal sqlstate '45000' set message_text = " A QUESTION CONTEXT ID CAN NEVER BE CHANGED!";
    end if;
END$$


DROP TRIGGER IF EXISTS `question_types_BEFORE_DELETE` $$
CREATE
    TRIGGER `question_types_BEFORE_DELETE`
    BEFORE DELETE
    ON `question_types`
    FOR EACH ROW
BEGIN
    signal sqlstate '45002' set message_text =
            " A QUESTION TYPE SHOULD NOT BE DELETED. BUT CAN BE DELETED AFTER DISABLING THIS TABLES BEFORE DELETE TRIGGER!";
END$$


DROP TRIGGER IF EXISTS `question_types_BEFORE_INSERT` $$
CREATE
    TRIGGER `question_types_BEFORE_INSERT`
    BEFORE INSERT
    ON `question_types`
    FOR EACH ROW
BEGIN
    set new.created_at = now();
    set new.updated_at = now();
END$$


DROP TRIGGER IF EXISTS `question_types_BEFORE_UPDATE` $$
CREATE
    TRIGGER `question_types_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `question_types`
    FOR EACH ROW
BEGIN
    set new.updated_at = now();
END$$


DROP TRIGGER IF EXISTS `questions_BEFORE_INSERT` $$
CREATE
    TRIGGER `questions_BEFORE_INSERT`
    BEFORE INSERT
    ON `questions`
    FOR EACH ROW
BEGIN
    set new.created_at = now();
    set new.updated_at = now();
END$$



DROP TRIGGER IF EXISTS `images_BEFORE_INSERT` $$
CREATE
    TRIGGER `images_BEFORE_INSERT`
    BEFORE INSERT
    ON `images`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `images_BEFORE_UPDATE` $$
CREATE
    TRIGGER `images_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `images`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `integrations_BEFORE_INSERT` $$
CREATE
    TRIGGER `integrations_BEFORE_INSERT`
    BEFORE INSERT
    ON `integrations`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `integrations_BEFORE_UPDATE` $$
CREATE
    TRIGGER `integrations_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `integrations`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `integrations_meta_BEFORE_INSERT` $$
CREATE TRIGGER `integrations_meta_BEFORE_INSERT` BEFORE INSERT ON `integrations_meta` FOR EACH ROW BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `integrations_meta_BEFORE_UPDATE` $$
CREATE  TRIGGER `integrations_meta_BEFORE_UPDATE` BEFORE UPDATE ON `integrations_meta` FOR EACH ROW BEGIN
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `securelog_BEFORE_INSERT` $$
CREATE
    TRIGGER `securelog_BEFORE_INSERT`
    BEFORE INSERT
    ON `securelog`
    FOR EACH ROW
BEGIN
    set new.logindate = now();
END$$


DROP TRIGGER IF EXISTS `whitelist_domains_BEFORE_INSERT` $$
CREATE
    TRIGGER `whitelist_domains_BEFORE_INSERT`
    BEFORE INSERT
    ON `whitelist_domains`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$


DROP TRIGGER IF EXISTS `whitelist_domains_BEFORE_UPDATE` $$
CREATE
    TRIGGER `whitelist_domains_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `whitelist_domains`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DELIMITER ;

SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;
