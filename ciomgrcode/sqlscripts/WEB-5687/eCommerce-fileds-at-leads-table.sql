alter table leads
    add sub_shop mediumtext null after product_attributes;

alter table leads
    add shop_url text null after sub_shop;

alter table leads
    add customer_group varchar(255) null after shop_url;

alter table leads
    add total_number_of_orders int null after total_order_net_value;


/*renaming the column sub_shop to subshops. */
alter table leads
    change sub_shop sub_shops mediumtext null;


/*Update miostandardfield with the new fields.*/
insert into miostandardfield(datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
values (6, 'sub_shops', 'Contact.SubShops', 'SubShops', 'eCommerce', 0),
       (9, 'shop_url', 'Contact.ShopUrl', 'ShopUrl', 'eCommerce', 0),
       (8, 'customer_group', 'Contact.Group', 'Group', 'eCommerce', 0),
       (5, 'total_number_of_orders', 'Contact.TotalNumberOfOrders', 'TotalNumberOfOrders', 'eCommerce', 0)
