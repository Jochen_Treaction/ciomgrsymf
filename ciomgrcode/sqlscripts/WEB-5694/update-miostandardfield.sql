/*Update the datatype for the firstOrderDate */
update miostandardfield msf
set msf.datatypes_id = 2
where msf.fieldname = 'first_order_date';

/*Update the datatype for the LastOrderDate */
update miostandardfield msf
set msf.datatypes_id = 2
where msf.fieldname = 'last_order_date';