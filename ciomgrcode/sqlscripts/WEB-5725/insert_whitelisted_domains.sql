# https://treaction.atlassian.net/browse/WEB-5725 White listed domains
insert into whitelist_domains (domain_name, created_by, updated_by)
values ('meineshopdemo.de', 999999, 999999),
       ('treaction.de', 999999, 999999),
       ('treaction.net', 999999, 999999),
       ('marckresin.com', 999999, 999999),
       ('marckresin.de', 999999, 999999)