-- mysql
-- SET FOREIGN_KEY_CHECKS = 0;
-- maria
SET @@session.unique_checks = 0;
SET @@session.foreign_key_checks = 0;
START TRANSACTION;
delete from accessconfigurations WHERE id > 0;
delete from administrates_companies WHERE id > 0;
delete from administrates_users WHERE id > 0;
delete from blacklist_domains WHERE id > 0;
delete from customfields WHERE id > 0;
delete from campaign WHERE id > 0;
delete from customer_journey WHERE id > 0;
delete from fraud WHERE id > 0;
delete from images WHERE id > 0;
delete from layout_type WHERE id > 0;
delete from lead_answer_content WHERE id > 0;
delete from lead_customfield_content WHERE id > 0;
delete from leads WHERE id > 0;
delete from mailsystems WHERE id > 0;
delete from oauth2_tokens WHERE id > 0;
delete from object_register_meta WHERE id > 0;
delete from objectregister WHERE id > 0;
delete from question_context WHERE id > 0;
delete from questions WHERE id > 0;
delete from securelog WHERE id > 0;
delete from survey WHERE id > 0;
delete from users WHERE id > 0;
delete from company WHERE id > 0;
delete from whitelist_domains WHERE id > 0;
COMMIT;
-- mysql
-- SET FOREIGN_KEY_CHECKS = 1;
-- maria
SET @@session.unique_checks = 1;
SET @@session.foreign_key_checks = 1;
