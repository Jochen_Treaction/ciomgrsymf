CREATE TABLE IF NOT EXISTS `users_meta` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `users_id` BIGINT UNSIGNED NOT NULL,
  `meta_key` VARCHAR(190) NULL,
  `meta_value` MEDIUMTEXT NULL,
  `created` DATETIME NULL,
  `created_by` BIGINT UNSIGNED NULL,
  `updated` DATETIME NULL,
  `updated_by` BIGINT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_idx` (`users_id` ASC),
   CONSTRAINT `fk_users_users_meta`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

DELIMITER $$
CREATE
TRIGGER `users_meta_BEFORE_INSERT`
BEFORE INSERT ON `users_meta`
FOR EACH ROW
BEGIN
DECLARE jetzt DATETIME;
SET jetzt=NOW();
set new.created = jetzt;
set new.updated = jetzt;
END $$

CREATE TRIGGER `users_meta_BEFORE_UPDATE` BEFORE UPDATE ON `users_meta` FOR EACH ROW
BEGIN
set new.updated = NOW();
END $$
DELIMITER ;
