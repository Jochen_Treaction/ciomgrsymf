DROP TABLE IF EXISTS `fieldtypes` ;

CREATE TABLE IF NOT EXISTS `fieldtypes` (
                                            `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
                                            `fieldtype` VARCHAR(45) NOT NULL DEFAULT 'custom',
                                            PRIMARY KEY (`id`))
    ENGINE = InnoDB
    AUTO_INCREMENT = 4
    DEFAULT CHARACTER SET = utf8mb4
    COLLATE = utf8mb4_german2_ci;

/*fill the necessary data */
# create inserts hear
INSERT INTO `fieldtypes` ( `fieldtype`) VALUES ('custom');
INSERT INTO `fieldtypes` ( `fieldtype`) VALUES ('standard');

/*Adding fk relationship to fieldtypes table */
ALTER TABLE  customfields
    ADD `fieldtypes_id` BIGINT UNSIGNED NULL DEFAULT 4 AFTER campaign_id,
    ADD `required` BOOLEAN NOT NULL DEFAULT TRUE AFTER fieldtypes_id,
    ADD CONSTRAINT `fk_customfields_fieldtypes1`
        FOREIGN KEY (`fieldtypes_id`)
            REFERENCES `fieldtypes` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;

