-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema mio_processhooks
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `miostandardfield`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `miostandardfield` ;

CREATE TABLE IF NOT EXISTS `miostandardfield` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `datatypes_id` BIGINT(20) UNSIGNED NOT NULL,
  `fieldname` VARCHAR(255) NOT NULL,
  `htmllabel` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_standardfields_datatypes1_idx` (`datatypes_id` ASC) VISIBLE,
  CONSTRAINT `fk_standardfields_datatypes1`
    FOREIGN KEY (`datatypes_id`)
    REFERENCES `datatypes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `system`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `system` ;

CREATE TABLE IF NOT EXISTS `system` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `objectregister_id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL COMMENT 'hubspot, maileon, digistore, ...',
  PRIMARY KEY (`id`),
  INDEX `fk_system_objectregister1_idx` (`objectregister_id` ASC) VISIBLE,
  CONSTRAINT `fk_system_objectregister1`
    FOREIGN KEY (`objectregister_id`)
    REFERENCES `objectregister` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `standardfieldmapping`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `standardfieldmapping` ;

CREATE TABLE IF NOT EXISTS `standardfieldmapping` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `system_id` BIGINT UNSIGNED NOT NULL,
  `miostandardfield_id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `datatype` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_standardfieldmapping_system1_idx` (`system_id` ASC) VISIBLE,
  INDEX `fk_standardfieldmapping_miostandardfield1_idx` (`miostandardfield_id` ASC) VISIBLE,
  CONSTRAINT `fk_standardfieldmapping_system1`
    FOREIGN KEY (`system_id`)
    REFERENCES `system` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_standardfieldmapping_miostandardfield1`
    FOREIGN KEY (`miostandardfield_id`)
    REFERENCES `miostandardfield` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
