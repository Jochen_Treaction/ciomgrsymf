drop index uk_campaign_name_version_status_compid on campaign;

create unique index uk_campaign_name_version_status_compid
    on campaign (name, company_id, objectregister_id);