/*
-- Query: SELECT * FROM mio.miostandardfield
LIMIT 0, 1000

-- Date: 2021-04-02 13:17
*/
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (1, 4, 'email', 'Contact.Email');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (2, 8, 'salutation', 'Contact.Salutation');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (3, 8, 'first_name', 'Contact.FirstName');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (4, 8, 'last_name', 'Contact.LastName');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (5, 8, 'firma', 'Contact.Firmation');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (6, 8, 'street', 'Contact.Street');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (7, 8, 'postal_code', 'Contact.PostalCode');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (8, 8, 'city', 'Contact.City');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (9, 8, 'country', 'Contact.Country');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (10, 7, 'phone', 'Contact.Phone');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (11, 8, 'lead_reference', 'Contact.LeadReference');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (12, 9, 'url', 'Contact.Url');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (13, 9, 'final_url', 'Contact.FinalUrl');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (14, 8, 'trafficsource', 'Contact.TrafficSource');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (15, 8, 'utm_parameters', 'Contact.UTMParameters');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (16, 8, 'split_version', 'Contact.SplitVersion');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (17, 8, 'targetgroup', 'Contact.Targetgroup');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (18, 8, 'ip', 'Contact.IP');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (19, 8, 'device_type', 'Contact.DeviceType');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (20, 8, 'device_browser', 'Contact.DeviceBrowser');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (21, 8, 'device_os', 'Contact.DeviceOs');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (22, 8, 'device_os_version', 'Contact.DeviceOsVersion');
INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`)
VALUES (23, 8, 'device_browser_version', 'Contact.DeviceBrowserVersion');
