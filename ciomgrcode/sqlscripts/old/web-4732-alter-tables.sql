-- this script requires code changes in:
-- MIO entities,
-- MIO Repositories,
-- webspider database microservice,
-- AIO microservice

ALTER TABLE `lead_answer_content`
DROP FOREIGN KEY `fk_lac_leads`;
ALTER TABLE `lead_answer_content`
CHANGE COLUMN `leads_id` `leads_id` BIGINT(20) UNSIGNED NULL COMMENT 'can be null for anonymous survey answers as of web-4732' ;
ALTER TABLE `lead_answer_content`
ADD CONSTRAINT `fk_lac_leads`
  FOREIGN KEY (`leads_id`)
  REFERENCES `leads` (`id`);

-- rename survey status to what it is: layout_type
ALTER TABLE `status` RENAME TO  `layout_type`;

-- change survey column `layout_status_id` to `layout_type_id`, redefine the FK relation
ALTER TABLE `survey`
DROP FOREIGN KEY `fk_survey_status`;
ALTER TABLE `survey`
CHANGE COLUMN `layout_status_id` `layout_type_id` BIGINT(20) UNSIGNED NULL DEFAULT NULL COMMENT 'key from layout_type table (Carousel or Single page)' ;
ALTER TABLE `survey`
ADD CONSTRAINT `fk_survey_status`
  FOREIGN KEY (`layout_type_id`)
  REFERENCES `layout_type` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
