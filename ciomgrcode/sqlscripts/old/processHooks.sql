SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


-- -----------------------------------------------------
-- New status called defined for processhook
-- -----------------------------------------------------

-- CAUTION RUN THAT INSERTS ONLY ENTRIES DO NOT EXIST IN YOUR LOCAL DATABASE
-- INSERT INTO `statusdef` (`id`, `value`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES (NULL, 'defined', NULL, '1', NULL, '1');
-- INSERT INTO `unique_object_type` (`id`, `assigned_table`, `unique_object_name`, `has_versioning`, `has_encryption`, `description`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES (NULL, 'processhook', 'processhook', '0', '0', 'processhook', NULL, '1', NULL, '1');
-- INSERT INTO `status_model` (`id`, `unique_object_type_id`, `statusdef_id`, `modelname`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES (NULL, '16', '11', 'processhook', NULL, '1', NULL, '1'), (NULL, '16', '14', 'processhook', NULL, '1', NULL, '1'), (NULL, '16', '2', 'processhook', NULL, '1', NULL, '1');

-- -----------------------------------------------------
-- Table `workflow`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `workflow` ;

CREATE TABLE IF NOT EXISTS `workflow` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `objectregister_id` BIGINT UNSIGNED NOT NULL,
  `campaign_id` BIGINT(20) UNSIGNED NULL,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_objectregister_workflow`
    FOREIGN KEY (`objectregister_id`)
    REFERENCES `objectregister` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_workflow_campaign1`
    FOREIGN KEY (`campaign_id`)
    REFERENCES `campaign` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_objectregister_workflow_idx` ON `workflow` (`objectregister_id` ASC);

CREATE INDEX `fk_workflow_campaign1_idx` ON `workflow` (`campaign_id` ASC);


-- -----------------------------------------------------
-- Table `action`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `action` ;

CREATE TABLE IF NOT EXISTS `action` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `method` VARCHAR(45) NULL,
  `host` VARCHAR(255) NULL,
  `end_point` VARCHAR(255) NULL,
  `params` VARCHAR(45) NULL,
  `output_status` VARCHAR(45) NULL,
  `output_message` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `event` ;

CREATE TABLE IF NOT EXISTS `event` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ui_name` VARCHAR(255) NOT NULL,
  `attached_dom_element` VARCHAR(255) NOT NULL,
  `comment` TEXT NULL COMMENT 'add as comment what the fuck that button is: button [submit] or button[custom1], button[custom2], button[custom3]',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `switch_operators`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `switch_operators` ;

CREATE TABLE IF NOT EXISTS `switch_operators` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `switch_definition`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `switch_definition` ;

CREATE TABLE IF NOT EXISTS `switch_definition` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `switch_operators_id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `expected_value` TEXT NULL,
  `check_value` TEXT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_switch_definition_switch_operators1`
    FOREIGN KEY (`switch_operators_id`)
    REFERENCES `switch_operators` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_switch_definition_switch_operators1_idx` ON `switch_definition` (`switch_operators_id` ASC);


-- -----------------------------------------------------
-- Table `input_datainterface`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `input_datainterface` ;

CREATE TABLE IF NOT EXISTS `input_datainterface` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `data_source` VARCHAR(255) NULL,
  `comment` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `output_datainterface`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `output_datainterface` ;

CREATE TABLE IF NOT EXISTS `output_datainterface` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `data_source` VARCHAR(45) NULL,
  `comment` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `processhook`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `processhook` ;

CREATE TABLE IF NOT EXISTS `processhook` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `objectregister_id` BIGINT(20) UNSIGNED NOT NULL,
  `action_id` BIGINT(20) UNSIGNED NOT NULL,
  `input_datainterface_id` BIGINT(20) UNSIGNED NULL,
  `output_datainterface_id` BIGINT(20) UNSIGNED NULL,
  `switch_definition_id` BIGINT(20) UNSIGNED NULL,
  `acts` ENUM('serverside', 'clientside') NULL DEFAULT 'serverside' COMMENT 'possible values serverside or clientsside',
  `name` VARCHAR(45) NOT NULL,
  `description` TEXT NULL,
  `is_triggered_by_event_id` BIGINT(20) UNSIGNED NULL,
  `is_triggered_by_processhook_id` BIGINT(20) UNSIGNED NULL,
  `execution_status` VARCHAR(45) NULL,
  `on_error_instruction` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_processhook_action1`
    FOREIGN KEY (`action_id`)
    REFERENCES `action` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processhook_objectregister1`
    FOREIGN KEY (`objectregister_id`)
    REFERENCES `objectregister` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event`
    FOREIGN KEY (`is_triggered_by_event_id`)
    REFERENCES `event` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processhook`
    FOREIGN KEY (`is_triggered_by_processhook_id`)
    REFERENCES `processhook` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processhook_switch_definition1`
    FOREIGN KEY (`switch_definition_id`)
    REFERENCES `switch_definition` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processhook_input_datainterface1`
    FOREIGN KEY (`input_datainterface_id`)
    REFERENCES `input_datainterface` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processhook_output_datainterface1`
    FOREIGN KEY (`output_datainterface_id`)
    REFERENCES `output_datainterface` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_processhook_action1_idx` ON `processhook` (`action_id` ASC) ;

CREATE INDEX `fk_processhook_objectregister1_idx` ON `processhook` (`objectregister_id` ASC) ;

CREATE INDEX `fk_event_idx` ON `processhook` (`is_triggered_by_event_id` ASC) ;

CREATE INDEX `fk_processhook_idx` ON `processhook` (`is_triggered_by_processhook_id` ASC) ;

CREATE INDEX `fk_processhook_switch_definition1_idx` ON `processhook` (`switch_definition_id` ASC) ;

CREATE INDEX `fk_processhook_input_datainterface1_idx` ON `processhook` (`input_datainterface_id` ASC) ;

CREATE INDEX `fk_processhook_output_datainterface1_idx` ON `processhook` (`output_datainterface_id` ASC) ;


-- -----------------------------------------------------
-- Table `workflow_processhooks`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `workflow_processhooks` ;

CREATE TABLE IF NOT EXISTS `workflow_processhooks` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `processhook_id` BIGINT(20) UNSIGNED NOT NULL,
  `workflow_id` BIGINT(20) UNSIGNED NOT NULL,
  `predecessor_id` BIGINT(20) UNSIGNED NULL,
  `successor_id` BIGINT(20) UNSIGNED NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`processhook_id`, `workflow_id`),
  CONSTRAINT `fk_processhook_has_workflow_processhook1`
    FOREIGN KEY (`processhook_id`)
    REFERENCES `processhook` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_processhook_has_workflow_workflow1`
    FOREIGN KEY (`workflow_id`)
    REFERENCES `workflow` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_predecessor`
    FOREIGN KEY (`predecessor_id`)
    REFERENCES `processhook` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_successor`
    FOREIGN KEY (`successor_id`)
    REFERENCES `processhook` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_processhook_has_workflow_workflow1_idx` ON `workflow_processhooks` (`workflow_id` ASC) ;

CREATE INDEX `fk_processhook_has_workflow_processhook1_idx` ON `workflow_processhooks` (`processhook_id` ASC) ;

CREATE INDEX `fk_predecessor_idx` ON `workflow_processhooks` (`predecessor_id` ASC) ;

CREATE INDEX `fk_successor_idx` ON `workflow_processhooks` (`successor_id` ASC, `predecessor_id` ASC) ;


-- -----------------------------------------------------
-- Table `clientside_pagetypes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `clientside_pagetypes` ;

CREATE TABLE IF NOT EXISTS `clientside_pagetypes` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `processhook_id` BIGINT(20) UNSIGNED NOT NULL,
  `pagetype` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_clientside_pagetypes_processhook1`
    FOREIGN KEY (`processhook_id`)
    REFERENCES `processhook` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_clientside_pagetypes_processhook1_idx` ON `clientside_pagetypes` (`processhook_id` ASC) ;


-- -----------------------------------------------------
-- Table `datainterface_fields`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `datainterface_fields` ;

CREATE TABLE IF NOT EXISTS `datainterface_fields` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `source_objectregister_id` BIGINT(20) UNSIGNED NULL COMMENT 'objectregister_id of unique_object_type customfield,  lead or (currently not existing) standardfields',
  `output_datainterface_id` BIGINT(20) UNSIGNED NULL,
  `input_datainterface_id` BIGINT(20) UNSIGNED NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_datainterface_fields_output_datainterface1`
    FOREIGN KEY (`output_datainterface_id`)
    REFERENCES `output_datainterface` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_datainterface_fields_input_datainterface1`
    FOREIGN KEY (`input_datainterface_id`)
    REFERENCES `input_datainterface` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_objectregister_id`
    FOREIGN KEY (`source_objectregister_id`)
    REFERENCES `objectregister` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_datainterface_fields_output_datainterface1_idx` ON `datainterface_fields` (`output_datainterface_id` ASC) ;

CREATE INDEX `fk_datainterface_fields_input_datainterface1_idx` ON `datainterface_fields` (`input_datainterface_id` ASC) ;

CREATE INDEX `fk_objectregister_id_idx` ON `datainterface_fields` (`source_objectregister_id` ASC) ;



DELIMITER $$

DROP TRIGGER IF EXISTS `workflow_BEFORE_INSERT` $$
CREATE TRIGGER `workflow_BEFORE_INSERT` BEFORE INSERT ON `workflow` FOR EACH ROW
BEGIN
    DECLARE selected_unique_object_name VARCHAR(190);
    SET @new_campaign_id = new.campaign_id;

    SELECT lower(u.unique_object_name)
    INTO selected_unique_object_name
    FROM campaign c
    JOIN objectregister o on c.objectregister_id = o.id
    JOIN unique_object_type u ON o.unique_object_type_id = u.id
    WHERE c.id = @new_campaign_id;

    IF( selected_unique_object_name != 'page' AND selected_unique_object_name != 'webhook' )
    THEN
        signal sqlstate '45000' set message_text='campaign_id must be page or webhook';
    END IF;
END$$


DROP TRIGGER IF EXISTS `workflow_BEFORE_UPDATE` $$
CREATE TRIGGER `workflow_BEFORE_UPDATE` BEFORE UPDATE ON `workflow` FOR EACH ROW
BEGIN
    DECLARE selected_unique_object_name VARCHAR(190);
    SET @new_campaign_id = new.campaign_id;

    IF(  new.campaign_id != old.campaign_id)
    THEN

        SELECT lower(u.unique_object_name)
        INTO selected_unique_object_name
        FROM campaign c
        JOIN objectregister o on c.objectregister_id = o.id
        JOIN unique_object_type u ON o.unique_object_type_id = u.id
        WHERE c.id = @new_campaign_id;

        IF( selected_unique_object_name != 'page' AND selected_unique_object_name != 'webhook' )
        THEN
            signal sqlstate '45000' set message_text='campaign_id must be page or webhook';
        END IF;
    END IF;
END$$


DROP TRIGGER IF EXISTS `processhook_BEFORE_INSERT` $$
CREATE TRIGGER processhook_BEFORE_INSERT BEFORE INSERT ON `processhook` FOR EACH ROW
BEGIN
if( new.is_triggered_by_event_id is not null and new.is_triggered_by_processhook_id is not null)
then
signal sqlstate '45000' set message_text='either use event_id trigger or processhook trigger';
end if;
END$$


DROP TRIGGER IF EXISTS `processhook_BEFORE_UPDATE` $$
CREATE TRIGGER `processhook_BEFORE_UPDATE` BEFORE UPDATE ON `processhook` FOR EACH ROW
BEGIN
if( (new.is_triggered_by_event_id is not null and new.is_triggered_by_processhook_id is not null) or
	(new.is_triggered_by_event_id is not null and old.is_triggered_by_processhook_id is not null) or
    (new.is_triggered_by_processhook_id is not null and old.is_triggered_by_event_id is not null)
)
then
signal sqlstate '45000' set message_text='either use event_id trigger or processhook trigger';
end if;
END$$


DROP TRIGGER IF EXISTS `workflow_processhooks_BEFORE_INSERT` $$
CREATE TRIGGER `workflow_processhooks_BEFORE_INSERT` BEFORE INSERT ON `workflow_processhooks` FOR EACH ROW
BEGIN
    DECLARE   count_processhook_id INT;
    DECLARE   count_triggered_by_event INT;
    DECLARE   count_triggered_by_processhook INT;

    SET @new_wp2_workflow_id = new.workflow_id;
    SET @new_processhook_id  = new.processhook_id;

    -- stmt counts processhook-id of a workflow that is is_triggered_by_event_id-processhooks
    select count(*)
    into count_processhook_id
    from workflow_processhooks wp
    join workflow_processhooks wp2 on wp2.workflow_id = @new_wp2_workflow_id
    join processhook p on p.id = wp2.processhook_id and p.is_triggered_by_event_id is not null;

    IF(count_processhook_id = 0)
    THEN --
        select count(*) into count_triggered_by_event
        from processhook
        where id = @new_processhook_id
        and is_triggered_by_event_id is not null
        and is_triggered_by_processhook_id is null;
        IF(count_triggered_by_event =0)
        THEN
            signal sqlstate '45000' set message_text='processhook must be triggered_by_event one';
        END IF;
    ELSE
        select count(*) into count_triggered_by_processhook
        from processhook
        where id = @new_processhook_id
        and is_triggered_by_event_id is null
        and is_triggered_by_processhook_id is not null;
            IF(count_triggered_by_processhook = 0)
        THEN
            signal sqlstate '45000' set message_text='processhook must be triggered_by_processhook one';
        END IF;

    END IF;
END$$

-- -----------------------------------------------------
-- Default value for table `event`
-- -----------------------------------------------------
INSERT INTO event (ui_name, attached_dom_element, comment) VALUES ('LoadPage', 'load', null);
INSERT INTO event (ui_name, attached_dom_element, comment) VALUES ('LeavePage', 'unload', null);
INSERT INTO event (ui_name, attached_dom_element, comment) VALUES ('ChangeTab', 'tabOnChange', null);
INSERT INTO event (ui_name, attached_dom_element, comment) VALUES ('Submit', 'click', null);
INSERT INTO event (ui_name, attached_dom_element, comment) VALUES ('ClosePage', 'unload', null);

DELIMITER ;
-- fill event table | defined by aravind
INSERT INTO event (id, ui_name, attached_dom_element, comment) VALUES (1, 'LoadPage', 'load', '$(window).on("load", function () {} => triggered after all assets on the page have loaded, including images.');
INSERT INTO event (id, ui_name, attached_dom_element, comment) VALUES (2, 'DocumentReady', 'ready', '$( document ).ready( handler ) => as soon as the page''s Document Object Model (DOM) becomes safe to manipulate');
INSERT INTO event (id, ui_name, attached_dom_element, comment) VALUES (3, 'LeavePage', 'unload', '$(window).on("unload", function(){} => sent to the window element when the user navigates away from the page');
INSERT INTO event (id, ui_name, attached_dom_element, comment) VALUES (4, 'Submit', 'click', '$("button#anyid").on("click", function(){}');
--  INSERT INTO event (id, ui_name, attached_dom_element, comment) VALUES (5, 'ChangeTab', 'tabOnChange', '$(any element).on("blur", function(){} =>  to an element when it loses focus');
--  INSERT INTO event (id, ui_name, attached_dom_element, comment) VALUES (6, 'ClosePage', 'unload', 'Same as LeavePage => $(window).on("unload", function(){} => sent to the window element when the user navigates away from the page');

-- sample entry for action | defined by jochen
-- INSERT INTO action (id, name, method, host, end_point, params, output_status, output_message) VALUES (1, 'send_lead_notification', 'POST', 'AIO', '/processhook/send/leadnotification', '{key:value}', 'bool', '{''error'': ''error msg txt'', ''success'': ''success msg txt''}');



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
