ALTER TABLE lead_answer_content ADD COLUMN lead_answer_score decimal(15,2);

-- alter table lead_answer_content DROP FOREIGN KEY fk_lac_leads;

-- alter table lead_answer_content DROP FOREIGN KEY fk_lac_questions;

-- alter table lead_answer_content drop column id;

-- alter table lead_answer_content drop primary key;
SHOW ERRORS;
alter table lead_answer_content ADD UNIQUE INDEX `uidx_leads_questions` (`leads_id` ASC, `questions_id` ASC);
SHOW ERRORS;
alter table lead_answer_content ADD COLUMN `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`);
SHOW ERRORS;
alter table lead_answer_content ADD CONSTRAINT `fk_lac_leads`  FOREIGN KEY (leads_id) REFERENCES leads (id);
SHOW ERRORS;
alter table lead_answer_content ADD CONSTRAINT `fk_lac_questions` FOREIGN KEY (questions_id) REFERENCES questions (id);
SHOW ERRORS;