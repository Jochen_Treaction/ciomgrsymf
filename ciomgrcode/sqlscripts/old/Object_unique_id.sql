-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Table `unique_object_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `unique_object_type` ;

CREATE TABLE IF NOT EXISTS `unique_object_type` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `unique_object_name` VARCHAR(190) NOT NULL,
  `created_at` DATETIME NULL,
  `created_by` BIGINT UNSIGNED NULL,
  `updated_at` DATETIME NULL,
  `updated_by` BIGINT UNSIGNED NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_german2_ci;


-- -----------------------------------------------------
-- Table `status_model`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `status_model` ;

CREATE TABLE IF NOT EXISTS `status_model` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `unique_object_type_id` BIGINT UNSIGNED NOT NULL,
  `value` VARCHAR(80) NOT NULL,
  `created_at` DATETIME NULL,
  `created_by` BIGINT UNSIGNED NULL,
  `udated_at` DATETIME NULL,
  `updated_by` BIGINT UNSIGNED NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_unique_object_type_idx` (`unique_object_type_id` ASC) VISIBLE,
  CONSTRAINT `fk_unique_object_type`
    FOREIGN KEY (`unique_object_type_id`)
    REFERENCES `unique_object_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `object_register` ;

-- -----------------------------------------------------
-- Table `object_register`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `object_register` ;

CREATE TABLE IF NOT EXISTS `object_register` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `object_unique_id` VARCHAR(190) NOT NULL DEFAULT '00000000-0000-0000-00000000000000000',
  `version` INT UNSIGNED NOT NULL DEFAULT 1,
  `unique_object_type_id` BIGINT(20) UNSIGNED NOT NULL,
  `status_model_id` BIGINT UNSIGNED NULL,
  `modification_no` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  `comment` VARCHAR(255) NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `created_by` BIGINT UNSIGNED NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `updated_by` BIGINT UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_object_register_object_type1_idx` (`unique_object_type_id` ASC) VISIBLE,
  INDEX `fk_status_models_idx` (`status_model_id` ASC) VISIBLE,
  UNIQUE INDEX `uk_ouid_version` (`object_unique_id` ASC, `version` ASC) VISIBLE,
  CONSTRAINT `fk_object_register_object_type1`
    FOREIGN KEY (`unique_object_type_id`)
    REFERENCES `unique_object_type` (`id`)
    ON DELETE CASCADE,
  CONSTRAINT `fk_status_models`
    FOREIGN KEY (`status_model_id`)
    REFERENCES `status_model` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_german2_ci;


-- -----------------------------------------------------
-- Table `object_register_meta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `object_register_meta` ;

CREATE TABLE IF NOT EXISTS `object_register_meta` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `object_register_id` BIGINT(20) UNSIGNED NOT NULL,
  `meta_key` VARCHAR(190) NOT NULL,
  `meta_value` MEDIUMTEXT NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT NULL,
  `created_by` BIGINT UNSIGNED NULL DEFAULT NULL,
  `updated` DATETIME NULL DEFAULT NULL,
  `updated_by` BIGINT UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_object_register_meta_object_register1_idx` (`object_register_id` ASC) VISIBLE,
  CONSTRAINT `fk_object_register_meta_object_register1`
    FOREIGN KEY (`object_register_id`)
    REFERENCES `object_register` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_german2_ci;

USE `object_register`;

DELIMITER $$


DROP TRIGGER IF EXISTS `object_register_BEFORE_INSERT` $$

CREATE

TRIGGER `object_register_BEFORE_INSERT`
BEFORE INSERT ON `object_register`
FOR EACH ROW
BEGIN
    set new.created_at = now();
    set new.updated_at = now();
END$$



DROP TRIGGER IF EXISTS `object_register_BEFORE_UPDATE` $$

CREATE

TRIGGER `object_register_BEFORE_UPDATE`
BEFORE UPDATE ON `object_register`
FOR EACH ROW
BEGIN
	set new.modification_no = old.modification_no + 1;
    set new.updated_at = now();
END$$



DROP TRIGGER IF EXISTS `object_register_BEFORE_INSERT_1` $$

CREATE DEFINER = CURRENT_USER TRIGGER `object_register_BEFORE_INSERT_1` BEFORE INSERT ON `object_register` FOR EACH ROW
BEGIN
    set new.created_at = now();
    set new.updated_at = now();
END$$



DROP TRIGGER IF EXISTS `object_register_meta_BEFORE_INSERT` $$

CREATE

TRIGGER `object_register_meta_BEFORE_INSERT`
BEFORE INSERT ON `object_register_meta`
FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$



DROP TRIGGER IF EXISTS `object_register_meta_BEFORE_UPDATE` $$

CREATE

TRIGGER `object_register_meta_BEFORE_UPDATE`
BEFORE UPDATE ON `object_register_meta`
FOR EACH ROW
BEGIN
    set new.updated = now();
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


# create inserts hear
INSERT INTO `unique_object_type` ( `unique_object_name`) VALUES ('campaign');
INSERT INTO `unique_object_type` ( `unique_object_name`) VALUES ('webhook');
INSERT INTO `unique_object_type` ( `unique_object_name`) VALUES ('lead');

