DROP TABLE IF EXISTS `feature_toggle`;
CREATE TABLE `feature_toggle` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `feature` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `updated_by` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DELIMITER $$

CREATE TRIGGER `feature_toggle_BEFORE_INSERT` BEFORE INSERT ON `feature_toggle` FOR EACH ROW BEGIN
set new.created = now();
set new.updated = now();
END
$$

CREATE TRIGGER `feature_toggle_BEFORE_UPDATE` BEFORE UPDATE ON `feature_toggle` FOR EACH ROW BEGIN
set new.updated = now();
END
$$

DELIMITER ;
