-- create basic object register entries
insert into statusdef (value, created_by, updated_by) values ('active',1,1);
insert into statusdef (value, created_by, updated_by) values ('archived',1,1);
insert into statusdef (value, created_by, updated_by) values ('draft',1,1);
insert into statusdef (value, created_by, updated_by) values ('inactive',1,1);
insert into statusdef (value, created_by, updated_by) values ('fraud',1,1);

insert into unique_object_type (unique_object_name,assigned_table,has_versioning,description,created_by,updated_by)	VALUES ('project','campaign',0,'Project',1,1);
insert into unique_object_type (unique_object_name,assigned_table,has_versioning,description,created_by,updated_by)	VALUES ('page','campaign',1,'LandingPage',1,1);
insert into unique_object_type (unique_object_name,assigned_table,has_versioning,description,created_by,updated_by)	VALUES ('webhook','campaign',1,'Webhook',1,1);
insert into unique_object_type (unique_object_name,assigned_table,has_versioning,description,created_by,updated_by)	VALUES ('customer','leads',1,'Customer',1,1);
insert into unique_object_type (unique_object_name,assigned_table,has_versioning,description,created_by,updated_by)	VALUES ('customfields','customfields',1,'User defined customfields related to a project',1,1);
insert into unique_object_type (unique_object_name,assigned_table,has_versioning,description,created_by,updated_by)	VALUES ('smarttags','customfields',1,'User defined smarttags related to a project',1,1);
insert into unique_object_type (unique_object_name,assigned_table,has_versioning,description,created_by,updated_by)	VALUES ('survey','survey',1,'Survey with answers related to a customer',1,1);

insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (1,1,'project',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (2,1,'page',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (2,2,'page',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (2,3,'page',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (3,1,'webhook',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (3,2,'webhook',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (3,3,'webhook',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (4,1,'customer',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (4,4,'customer',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (4,5,'customer',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (7,1,'survey',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (7,2,'survey',1,1);
insert into status_model (unique_object_type_id,statusdef_id,modelname,created_by,updated_by)	 VALUES (7,3,'survey',1,1);

insert into object_type_meta (unique_object_type_id, meta_key, meta_value, created_by, updated_by) VALUES (1, 'encrypted', 1,1,1);
insert into object_type_meta (unique_object_type_id, meta_key, meta_value, created_by, updated_by) VALUES (2, 'encrypted', 0,1,1);
insert into object_type_meta (unique_object_type_id, meta_key, meta_value, created_by, updated_by) VALUES (3, 'encrypted', 0,1,1);
insert into object_type_meta (unique_object_type_id, meta_key, meta_value, created_by, updated_by) VALUES (4, 'encrypted', 0,1,1);
insert into object_type_meta (unique_object_type_id, meta_key, meta_value, created_by, updated_by) VALUES (5, 'encrypted', 0,1,1);
insert into object_type_meta (unique_object_type_id, meta_key, meta_value, created_by, updated_by) VALUES (6, 'encrypted', 0,1,1);
insert into object_type_meta (unique_object_type_id, meta_key, meta_value, created_by, updated_by) VALUES (7, 'encrypted', 0,1,1);
