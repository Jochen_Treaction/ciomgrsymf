-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Erstellungszeit: 31. Mrz 2021 um 18:14
-- Server-Version: 10.1.47-MariaDB-0ubuntu0.18.04.1
-- PHP-Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `mio`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `workflow`
--

DROP TABLE IF EXISTS `workflow`;
CREATE TABLE `workflow` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `objectregister_id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED DEFAULT NULL,
  `campaign_id` bigint(20) UNSIGNED DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Trigger `workflow`
--
DROP TRIGGER IF EXISTS `workflow_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `workflow_BEFORE_INSERT` BEFORE INSERT ON `workflow` FOR EACH ROW BEGIN
    DECLARE selected_unique_object_name VARCHAR(190);
    SET @new_campaign_id = new.campaign_id;

    SELECT lower(u.unique_object_name)
    INTO selected_unique_object_name
    FROM campaign c
    JOIN objectregister o on c.objectregister_id = o.id
    JOIN unique_object_type u ON o.unique_object_type_id = u.id
    WHERE c.id = @new_campaign_id;

    IF( selected_unique_object_name != 'page' AND selected_unique_object_name != 'webhook' )
    THEN
        signal sqlstate '45000' set message_text='campaign_id must be page or webhook';
    END IF;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `workflow_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `workflow_BEFORE_UPDATE` BEFORE UPDATE ON `workflow` FOR EACH ROW BEGIN
    DECLARE selected_unique_object_name VARCHAR(190);
    SET @new_campaign_id = new.campaign_id;

    IF(  new.campaign_id != old.campaign_id)
    THEN

        SELECT lower(u.unique_object_name)
        INTO selected_unique_object_name
        FROM campaign c
        JOIN objectregister o on c.objectregister_id = o.id
        JOIN unique_object_type u ON o.unique_object_type_id = u.id
        WHERE c.id = @new_campaign_id;

        IF( selected_unique_object_name != 'page' AND selected_unique_object_name != 'webhook' )
        THEN
            signal sqlstate '45000' set message_text='campaign_id must be page or webhook';
        END IF;
    END IF;
END
$$
DELIMITER ;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `workflow`
--
ALTER TABLE `workflow`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_objectregister_workflow_idx` (`objectregister_id`),
  ADD KEY `fk_workflow_campaign1_idx` (`campaign_id`),
  ADD KEY `workflow___fk_company_id` (`company_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `workflow`
--
ALTER TABLE `workflow`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `workflow`
--
ALTER TABLE `workflow`
  ADD CONSTRAINT `fk_objectregister_workflow` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_workflow_campaign1` FOREIGN KEY (`campaign_id`) REFERENCES `campaign` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `workflow___fk_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
