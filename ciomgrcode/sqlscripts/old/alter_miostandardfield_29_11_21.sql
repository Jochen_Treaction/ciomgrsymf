use mio;

# Add new column 'fieldtype'
alter table miostandardfield
    add column fieldtype varchar(50) after htmllabel;

# Update all the columns accordingly to the standard fields.
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '1');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '2');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '3');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '4');
UPDATE `mio`.`miostandardfield`
SET `fieldname` = 'firma',
    `htmllabel` = 'Contact.Company',
    `fieldtype` = 'standard'
WHERE (`id` = '5');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '6');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '7');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '8');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '9');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '10');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '11');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '12');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '13');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '14');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '15');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '16');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '17');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '18');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '19');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '20');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '21');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '22');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '23');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '24');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '25');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '26');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '27');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '28');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'technical'
WHERE (`id` = '29');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'standard'
WHERE (`id` = '30');


# Update Correct HTML Labels.
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.SOIIP'
WHERE (`id` = '27');
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.DOIIP'
WHERE (`id` = '28');
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.SOITimestamp'
WHERE (`id` = '26');
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.DeviceOS'
WHERE (`id` = '21');
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.DeviceOSVersion'
WHERE (`id` = '22');
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.FinalURL'
WHERE (`id` = '13');
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.URL'
WHERE (`id` = '12');
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.Reference'
WHERE (`id` = '11');

# Add new Standard field 'birthday'
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('2', 'birthday', 'Contact.Birthday ', 'standard', '0');

# Inserted new field type 'eCommerce'
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('8', 'last_order_no', 'Contact.LastOrderNo', 'eCommerce', '0');
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('1', 'last_order_date', 'Contact.LastOrderDate', 'eCommerce', '0');
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('3', 'total_order_net_value', 'Contact.TotalOrderNetValue', 'eCommerce', '0');
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('3', 'last_year_order_net_value', 'Contact.LastYearOrderNetValue', 'eCommerce', '0');
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('3', 'last_order_net_value', 'Contact.LastOrderNetValue', 'eCommerce', '0');
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('6', 'smart_tags', 'Contact.eCommerceTags', 'eCommerce', '0');
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('8', 'referrer_id', 'Contact.ReferrerID', 'Technical', '1');
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('1', 'first_order_date', 'Contact.FirstOrderDate', 'eCommerce', '0');
INSERT INTO `mio`.`miostandardfield` (`datatypes_id`, `fieldname`, `htmllabel`, `fieldtype`, `hidden_field`)
VALUES ('8', 'product_attributes', 'Contact.ProductAttributes', 'eCommerce', '0');


# updated 'standard' => 'Standard' and 'technical' => 'Technical'
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '31');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '30');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '25');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '12');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '10');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '9');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '8');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '7');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '6');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '5');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '4');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '3');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '2');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Standard'
WHERE (`id` = '1');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '11');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '13');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '14');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '15');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '16');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '17');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '18');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '19');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '20');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '21');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '22');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '23');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '24');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '26');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '27');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '28');
UPDATE `mio`.`miostandardfield`
SET `fieldtype` = 'Technical'
WHERE (`id` = '29');
