CREATE TABLE `images`
(
    `id`          BIGINT UNSIGNED  NOT NULL AUTO_INCREMENT,
    `question_id` BIGINT UNSIGNED  NOT NULL,
    `option_id`   BIGINT UNSIGNED  NULL,
    `imgtype`     VARCHAR(10)      NULL,
    `img`         LONGTEXT         NULL,
    `created`     DATETIME         NULL,
    `created_by`  INT(11) UNSIGNED NULL,
    `updated`     DATETIME         NULL,
    `updated_by`  INT(11) UNSIGNED NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_images_questions_idx` (`question_id` ASC),
    CONSTRAINT `fk_images_questions`
        FOREIGN KEY (`question_id`)
            REFERENCES `questions` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
);
DROP TRIGGER IF EXISTS `images_BEFORE_INSERT`;

DELIMITER $$

CREATE TRIGGER `images_BEFORE_INSERT`
    BEFORE INSERT
    ON `images`
    FOR EACH ROW
BEGIN
    set new.created = now();
    set new.updated = now();
END$$
DELIMITER ;
DROP TRIGGER IF EXISTS `images_BEFORE_UPDATE`;

DELIMITER $$

CREATE TRIGGER `images_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `images`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$
DELIMITER ;


