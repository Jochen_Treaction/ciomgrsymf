DROP TRIGGER IF EXISTS administrates_users_BEFORE_INSERT;

DELIMITER $$
CREATE TRIGGER administrates_users_BEFORE_INSERT BEFORE INSERT ON administrates_users FOR EACH ROW
BEGIN
set new.created = now();
set new.updated = now();
END$$
DELIMITER ;
DROP TRIGGER IF EXISTS administrates_users_BEFORE_UPDATE;

DELIMITER $$
CREATE TRIGGER administrates_users_BEFORE_UPDATE BEFORE UPDATE ON administrates_users FOR EACH ROW
BEGIN
set new.updated = now();
END$$
DELIMITER ;
ALTER TABLE administrates_users DROP FOREIGN KEY fk_administrates_users_invited_user_id;
ALTER TABLE administrates_users
ADD CONSTRAINT fk_administrates_users_invited_user_id
  FOREIGN KEY (invited_user_id)
  REFERENCES users (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
