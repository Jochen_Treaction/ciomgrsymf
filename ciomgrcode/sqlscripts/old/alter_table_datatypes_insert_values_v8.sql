ALTER TABLE `datatypes`
ADD COLUMN `phptype` VARCHAR(80) NULL AFTER `name`,
ADD COLUMN `base_regex` TEXT NULL AFTER `phptype`;

insert into datatypes (id,name,phptype,base_regex) values (1,'Date ','date','^\d{2}(\.|-)\d{2}(\.|-)\d{4}$');
insert into datatypes (id,name,phptype,base_regex) values (2,'DateTime','date','^\d{2}(\.|-)\d{2}(\.|-)\d{4} \d{2}:\d{2}:\d{2}$');
insert into datatypes (id,name,phptype,base_regex) values (3,'Decimal','double','^[\d]*[\.]?[\d]*$');
insert into datatypes (id,name,phptype,base_regex) values (4,'Email','string','^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-z]{2,9})$');
insert into datatypes (id,name,phptype,base_regex) values (5,'Integer','integer','^[0-9]+$');
insert into datatypes (id,name,phptype,base_regex) values (6,'List','string','([\w\-\.]+,?)+');
insert into datatypes (id,name,phptype,base_regex) values (7,'Phone','string','[0-9\-\/ \(\)+]*');
insert into datatypes (id,name,phptype,base_regex) values (8,'Text','string','.*');
insert into datatypes (id,name,phptype,base_regex) values (9,'URL','string','^(?:(?:https?)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6}))(?::\d+)?(?:[^\s]*)?$');

-- add encryption flag to unique_object_type
ALTER TABLE `unique_object_type`
ADD COLUMN `has_encryption` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `has_versioning`,
CHANGE COLUMN `has_versioning` `has_versioning` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'object per default have versioning. currently ther is only project, which does not have versioning. if versioning is set to false, then the instance in objectregister will have only version = 1' ;
