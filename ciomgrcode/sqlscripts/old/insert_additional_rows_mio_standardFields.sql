INSERT INTO mio.miostandardfield (datatypes_id, fieldname, htmllabel, hidden_field)
VALUES (5, 'permission', 'Contact.Permission', 1);

INSERT INTO mio.miostandardfield (datatypes_id, fieldname, htmllabel, hidden_field)
VALUES (9, 'webpage', 'Contact.Webpage', 0);

INSERT INTO mio.miostandardfield (datatypes_id, fieldname, htmllabel, hidden_field)
VALUES (2, 'optin_timestamp', 'Contact.OptinTimestamp', 1);

INSERT INTO mio.miostandardfield (datatypes_id, fieldname, htmllabel, hidden_field)
VALUES (8, 'optin_ip', 'Contact.OptinIp', 1);

INSERT INTO mio.miostandardfield (datatypes_id, fieldname, htmllabel, hidden_field)
VALUES (8, 'doi_ip', 'Contact.DoiIp', 1);

INSERT INTO mio.miostandardfield (datatypes_id, fieldname, htmllabel, hidden_field)
VALUES (2, 'doi_timestamp', 'Contact.DoiTimestamp', 1);