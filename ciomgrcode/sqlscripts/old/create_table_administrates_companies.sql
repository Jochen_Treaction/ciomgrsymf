-- WEB-3850
CREATE TABLE `administrates_companies` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `company_id` BIGINT UNSIGNED NOT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` INT(11) UNSIGNED NULL,
  `updated_by` INT(11) UNSIGNED NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `UK_admin_comp_user_id_company_id` (`user_id` ASC, `company_id` ASC),
  INDEX `fk_admin_comp_company_id_idx` (`company_id` ASC),
  INDEX `fk_admin_comp_user_id_idx` (`user_id` ASC),
  CONSTRAINT `fk_admin_comp_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_admin_comp_company_id`
    FOREIGN KEY (`company_id`)
    REFERENCES `company` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
DROP TRIGGER IF EXISTS `administrates_companies_BEFORE_INSERT`;

DELIMITER $$
CREATE DEFINER = CURRENT_USER TRIGGER `administrates_companies_BEFORE_INSERT` BEFORE INSERT ON `administrates_companies` FOR EACH ROW
BEGIN
set new.created = now();
set new.updated = now();
END$$
DELIMITER ;
DROP TRIGGER IF EXISTS `administrates_companies_BEFORE_UPDATE`;

DELIMITER $$
CREATE DEFINER = CURRENT_USER TRIGGER `administrates_companies_BEFORE_UPDATE` BEFORE UPDATE ON `administrates_companies` FOR EACH ROW
BEGIN
set new.updated = now();
END$$
DELIMITER ;