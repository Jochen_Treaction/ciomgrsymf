ALTER TABLE administrates_users DROP FOREIGN KEY fk_administrates_users_invited_user_id;
ALTER TABLE administrates_users
ADD CONSTRAINT fk_administrates_users_invited_user_id
  FOREIGN KEY (invited_user_id)
  REFERENCES users (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
