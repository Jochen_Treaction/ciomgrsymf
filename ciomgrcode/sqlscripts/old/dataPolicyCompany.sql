alter table company
    add url_gtc varchar(255) null after url_imprint;

alter table company
    add hash_url_gtc varchar(255) null after hash_url_imprint;
