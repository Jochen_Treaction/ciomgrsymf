SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `status_model`;
CREATE TABLE `status_model` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unique_object_type_id` bigint(20) UNSIGNED NOT NULL,
  `statusdef_id` bigint(20) UNSIGNED NOT NULL,
  `modelname` varchar(80) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `status_model` (`id`, `unique_object_type_id`, `statusdef_id`, `modelname`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 1, 'project', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(2, 2, 1, 'page', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(3, 2, 2, 'page', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(4, 2, 3, 'page', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(5, 3, 1, 'webhook', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(6, 3, 2, 'webhook', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(7, 3, 3, 'webhook', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(8, 4, 1, 'customer', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(9, 4, 4, 'customer', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(10, 4, 5, 'customer', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(11, 7, 1, 'survey', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(12, 7, 2, 'survey', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(13, 7, 3, 'survey', '2020-10-23 19:19:11', 1, '2020-10-23 19:19:11', 1),
(14, 9, 6, 'account', '2020-11-09 18:32:28', 1, '2020-11-09 18:32:28', 1),
(15, 9, 7, 'account', '2020-11-09 18:32:44', 1, '2020-11-09 18:34:17', 1),
(16, 9, 1, 'account', '2020-11-09 18:34:32', 1, '2020-11-09 18:34:32', 1),
(17, 9, 8, 'account', '2020-11-09 18:34:52', 1, '2020-11-09 18:34:52', 1),
(18, 4, 9, 'customer', '2020-11-11 10:38:46', 1, '2020-11-11 10:38:46', 1),
(19, 4, 10, 'customer', '2020-11-11 10:39:02', 1, '2020-11-11 10:39:02', 1),
(20, 9, 13, 'account', '2021-03-01 10:40:41', 1, '2021-03-01 11:02:48', 1);

DROP TRIGGER IF EXISTS `status_model_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `status_model_BEFORE_INSERT` BEFORE INSERT ON `status_model` FOR EACH ROW BEGIN
DECLARE jetzt DATETIME; SET jetzt=NOW();
set new.created_at = jetzt;
set new.updated_at = jetzt;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `status_model_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `status_model_BEFORE_UPDATE` BEFORE UPDATE ON `status_model` FOR EACH ROW BEGIN
set new.updated_at = NOW();
END
$$
DELIMITER ;

ALTER TABLE `status_model`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_unique_object_type_id__statusdef_id` (`unique_object_type_id`,`statusdef_id`),
  ADD KEY `fk_status_model_statusdef1_idx` (`statusdef_id`),
  ADD KEY `fk_unique_object_type_id_idx` (`unique_object_type_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `status_model`
--
ALTER TABLE `status_model`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `status_model`
--
ALTER TABLE `status_model`
  ADD CONSTRAINT `fk_statusdef` FOREIGN KEY (`statusdef_id`) REFERENCES `statusdef` (`id`),
  ADD CONSTRAINT `fk_unique_object_type_id` FOREIGN KEY (`unique_object_type_id`) REFERENCES `unique_object_type` (`id`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;
