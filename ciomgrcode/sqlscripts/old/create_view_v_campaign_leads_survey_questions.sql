create view v_campaign_leads_survey_questions as select
     c.id as `CampaignId`,
     c.company_id as `CompanyId`,
     c.name as `Campaign Name`,
     l.id as lead_id,
     l.lead_reference  as `Lead Reference`,
     l.created as Created,
     l.salutation as Salutation,
     l.first_name as Firstname,
     l.last_name as Lastname,
     l.email as Email,
     l.postal_code as Postalcode,
     l.status as Status,
     l.trafficsource as Trafficsource,
     l.utm_parameters `UTM Parameters`,
     l.`affiliateID` as `AffiliateID`,
     l.`affiliateSubID`  as `AffiliateSubID`,
     q.question_question_txt as `name`,
     lac.lead_answer_content AS `value`
 from campaign c
          join leads l on l.campaign_id = c.id
          join survey s on s.id = c.survey_id
          join questions q on q.survey_id = s.id
          join lead_answer_content lac on lac.leads_id = l.id and lac.questions_id = q.id;
