
/*change the data type of campaign.object_register_id to match original table*/
alter table campaign drop column object_register_id;

ALTER TABLE campaign
    ADD object_register_id BIGINT(20) not null  default 0 after survey_id;


/* Modify lead table and add unique id */

ALTER TABLE leads
    ADD object_register_campaign_id bigint(20) not null default 0 after campaign_id;

ALTER TABLE leads
    ADD object_register_lead_id bigint(20) not null default 0 after campaign_id;

