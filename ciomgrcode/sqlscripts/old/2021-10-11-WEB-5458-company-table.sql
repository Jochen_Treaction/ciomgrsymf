-- WEB-5458 add further fields (url_privacy_policy, url_imprint) to company table
ALTER TABLE `company`
ADD COLUMN `url_privacy_policy` VARCHAR(255) NULL DEFAULT NULL AFTER `website`,
ADD COLUMN `url_imprint` VARCHAR(255) NULL DEFAULT NULL AFTER `url_privacy_policy`,
ADD COLUMN `hash_url_privacy_policy` VARCHAR(128) NULL DEFAULT NULL AFTER `hash_website`,
ADD COLUMN `hash_url_imprint` VARCHAR(128) NULL DEFAULT NULL AFTER `hash_url_privacy_policy`;
