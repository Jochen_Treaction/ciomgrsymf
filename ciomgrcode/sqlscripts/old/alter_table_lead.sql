alter table leads
    add doi_ip          varchar(255) default null null after device_browser_version,
    add doi_timestamp   datetime     default null null after doi_ip,
    add optin_timestamp datetime     default null null after doi_timestamp,
    add optin_ip        varchar(255) default null null after optin_timestamp,
    add permission      int          default 0    null after optin_ip,
    add webpage         text         default null null after permission;

alter table leads
    add hash_doi_ip          varchar(128) default null null after hash_device_browser_version,
    add hash_doi_timestamp   varchar(128) default null null after hash_doi_ip,
    add hash_optin_timestamp varchar(128) default null null after hash_doi_timestamp,
    add hash_optin_ip        varchar(128) default null null after hash_optin_timestamp,
    add hash_permission      varchar(128) default null null after hash_optin_ip,
    add hash_webpage         varchar(128) default null null after hash_permission;


