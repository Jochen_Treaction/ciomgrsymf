INSERT INTO `mio`.`miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`, `hidden_field`)
VALUES ('30', '8', 'house_number', 'Contact.HouseNumber', '0');
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.DOIIp'
WHERE (`id` = '28');
UPDATE `mio`.`miostandardfield`
SET `htmllabel` = 'Contact.DOITimestamp'
WHERE (`id` = '29');
