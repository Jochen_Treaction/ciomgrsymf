DROP TRIGGER IF EXISTS `objectregister_BEFORE_UPDATE`;

DELIMITER $$
CREATE TRIGGER `objectregister_BEFORE_UPDATE`
    BEFORE UPDATE ON `objectregister`
    FOR EACH ROW
BEGIN
    set new.updated_at = NOW();
    if( new.object_unique_id <> old.object_unique_id ) then
        set new.object_unique_id = old.object_unique_id;
    end if;
END$$
DELIMITER ;


DROP TRIGGER IF EXISTS `objectregister_BEFORE_INSERT`;

DELIMITER $$
USE `mio`$$
CREATE  TRIGGER `objectregister_BEFORE_INSERT` BEFORE INSERT ON `objectregister` FOR EACH ROW
BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt=NOW();
    set new.created_at = jetzt;
    set new.updated_at = jetzt;
    if( new.object_unique_id IS NULL or  new.object_unique_id = '' ) then
        set new.object_unique_id = UUID();
    end if;
END$$
DELIMITER ;