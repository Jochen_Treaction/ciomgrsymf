INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (1, 4, 'email', 'Contact.Email', 'Email', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (2, 8, 'salutation', 'Contact.Salutation', 'Salutation', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (3, 8, 'first_name', 'Contact.FirstName', 'FirstName', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (4, 8, 'last_name', 'Contact.LastName', 'LastName', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (5, 8, 'firma', 'Contact.Company', 'Company', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (6, 8, 'street', 'Contact.Street', 'Street', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (7, 8, 'postal_code', 'Contact.PostalCode', 'PostalCode', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (8, 8, 'city', 'Contact.City', 'City', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (9, 8, 'country', 'Contact.Country', 'Country', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (10, 7, 'phone', 'Contact.Phone', 'Phone', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (11, 8, 'lead_reference', 'Contact.Reference', 'Reference', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (12, 9, 'url', 'Contact.URL', 'URL', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (13, 9, 'final_url', 'Contact.FinalURL', 'FinalURL', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (14, 8, 'traffic_source', 'Contact.TrafficSource', 'TrafficSource', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (15, 8, 'utm_parameters', 'Contact.UTMParameters', 'UTMParameters', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (16, 8, 'split_version', 'Contact.SplitVersion', 'SplitVersion', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (17, 8, 'target_group', 'Contact.TargetGroup', 'TargetGroup', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (18, 8, 'ip', 'Contact.IP', 'IP', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (19, 8, 'device_type', 'Contact.DeviceType', 'DeviceType', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (20, 8, 'device_browser', 'Contact.DeviceBrowser', 'DeviceBrowser', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (21, 8, 'device_os', 'Contact.DeviceOS', 'DeviceOS', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (22, 8, 'device_os_version', 'Contact.DeviceOSVersion', 'DeviceOSVersion', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (23, 8, 'device_browser_version', 'Contact.DeviceBrowserVersion', 'DeviceBrowserVersion', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (24, 5, 'permission', 'Contact.Permission', 'Permission', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (25, 9, 'webpage', 'Contact.Webpage', 'Webpage', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (26, 2, 'optin_timestamp', 'Contact.SOITimestamp', 'SOITimestamp', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (27, 8, 'optin_ip', 'Contact.SOIIP', 'SOIIP', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (28, 8, 'doi_ip', 'Contact.DOIIP', 'DOIIP', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (29, 2, 'doi_timestamp', 'Contact.DOITimestamp', 'DOITimestamp', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (30, 8, 'house_number', 'Contact.HouseNumber', 'HouseNumber', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (31, 2, 'birthday', 'Contact.Birthday ', 'Birthday', 'Standard', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (32, 8, 'last_order_no', 'Contact.LastOrderNo', 'LastOrderNo', 'eCommerce', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (33, 1, 'last_order_date', 'Contact.LastOrderDate', 'LastOrderDate', 'eCommerce', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (34, 3, 'total_order_net_value', 'Contact.TotalOrderNetValue', 'TotalOrderNetValue', 'eCommerce', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (35, 3, 'last_year_order_net_value', 'Contact.LastYearOrderNetValue', 'LastYearOrderNetValue', 'eCommerce', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (36, 3, 'last_order_net_value', 'Contact.LastOrderNetValue', 'LastOrderNetValue', 'eCommerce', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (37, 6, 'smart_tags', 'Contact.eCommerceTags', 'eCommerceTags', 'eCommerce', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (38, 8, 'referrer_id', 'Contact.ReferrerID', 'ReferrerID', 'Technical', 1);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (39, 1, 'first_order_date', 'Contact.FirstOrderDate', 'FirstOrderDate', 'eCommerce', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (40, 8, 'product_attributes', 'Contact.ProductAttributes', 'ProductAttributes', 'eCommerce', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (42, 8, 'affiliate_id', 'Contact.AffiliateID', 'AffiliateID', 'Technical', 0);
INSERT INTO mio.miostandardfield (id, datatypes_id, fieldname, htmllabel, html_placeholder, fieldtype, hidden_field)
VALUES (43, 8, 'affiliate_sub_id', 'Contact.AffiliateSubID', 'AffiliateSubID', 'Technical', 0);