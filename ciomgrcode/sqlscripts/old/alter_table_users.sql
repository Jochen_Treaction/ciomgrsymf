-- MYSQL
ALTER TABLE `users`
ADD COLUMN `roles` LONGTEXT NULL AFTER `password`,
    CHANGE COLUMN `email` `email` VARCHAR(180) NOT NULL ,
    CHANGE COLUMN `password` `password` VARCHAR(255) NOT NULL ,
    ADD UNIQUE INDEX `uk_users_email` (`email` ASC);

UPDATE `users` SET `first_name` = 'Aravind' , `last_name` = 'Karri'  WHERE `users`.`id` = 79;
-- MARIA
-- ALTER TABLE `users` ADD COLUMN `roles` LONGTEXT, CHANGE COLUMN `email` `email` VARCHAR(180) NOT NULL , CHANGE COLUMN `password` `password` VARCHAR(255) NOT NULL , ADD UNIQUE INDEX `uk_users_email` (`email` ASC)
