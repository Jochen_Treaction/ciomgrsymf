-- view to receive the lastest (in sense of objectregister highest version number) active lead-entry from leads
-- used in AIO for lead handling
DROP VIEW IF EXISTS v_aio_get_lastest_lead_entry;
CREATE VIEW v_aio_get_lastest_lead_entry AS
    SELECT
        l.*,
        o.version AS version,
        o.id AS objreg_objectregister_id
    FROM
        leads l
        JOIN objectregister o  ON (o.id = l.objectregister_id)
        JOIN statusdef s ON (o.statusdef_id = s.id AND s.value = 'active')
        JOIN unique_object_type uot on (o.unique_object_type_id = uot.id AND uot.assigned_table='leads');

