SET sql_mode = '';
-- clean messed up data
-- UPDATE `campaign` SET `version` = 'V1', `status` = 'archive' WHERE (`id` = '34');
-- UPDATE `campaign` SET `version` = 'V2' WHERE (`id` = '41');
-- UPDATE `campaign` SET `version` = 'V3', `status` = 'active' WHERE (`id` = '59');
-- UPDATE `campaign` SET `version` = 'V2' WHERE (`id` = '57');
-- UPDATE `campaign` SET `version` = 'V1', `status` = 'draft' WHERE (`id` = '846');
-- UPDATE `campaign` SET `version` = 'V2', `status` = 'draft' WHERE (`id` = '848');
-- UPDATE `campaign` SET `status` = 'draft' WHERE (`id` = '51');
-- UPDATE `campaign` SET `status` = 'archive' WHERE (`id` = '55');
-- UPDATE `campaign` SET `status` = 'archive' WHERE (`id` = '56');
-- commit;
-- do necessary changes
ALTER TABLE `campaign`
    CHANGE COLUMN `name` `name` VARCHAR(80) NOT NULL ,
    CHANGE COLUMN `version` `version` VARCHAR(40) NOT NULL ;
ALTER TABLE `campaign`
    ADD UNIQUE INDEX `uk_campaign_name_version_status_compid` (`name` ASC, `version` ASC, `status` ASC, `company_id` ASC);
ALTER TABLE `campaign`
    ADD INDEX `idx_campaign_name` (`name` ASC);
ALTER TABLE `campaign`
ADD COLUMN `account_no` VARCHAR(40) NULL DEFAULT '' AFTER `name`,
ADD INDEX `uk_campaign_account_no` (`account_no` ASC);
