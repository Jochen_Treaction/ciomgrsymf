-- WEB-3850 UPDATE 2020-04-21
CREATE TABLE `administrates_users` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `invited_user_id` BIGINT(20) UNSIGNED NOT NULL,
  `to_company_id` BIGINT UNSIGNED NOT NULL,
  `with_roles` TEXT NOT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` INT(11) UNSIGNED NULL,
  `updated_by` INT(11) UNSIGNED NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uk_administrates_users` (`user_id` ASC, `invited_user_id` ASC, `to_company_id` ASC),
  INDEX `fk_administrates_users_to_company_id_idx` (`to_company_id` ASC),
  INDEX `fk_administrates_users_user_id_idx` (`user_id` ASC),
  INDEX `fk_administrates_users_invited_user_id_idx` (`invited_user_id` ASC),
  CONSTRAINT `fk_administrates_users_to_company_id`
    FOREIGN KEY (`to_company_id`)
    REFERENCES `company` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_administrates_users_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_administrates_users_invited_user_id`
    FOREIGN KEY (`invited_user_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
DROP TRIGGER IF EXISTS `administrates_users_BEFORE_INSERT`;

DELIMITER $$
CREATE DEFINER = CURRENT_USER TRIGGER `administrates_users_BEFORE_INSERT` BEFORE INSERT ON `administrates_users` FOR EACH ROW
BEGIN
set new.created = now();
set new.updated = now();
END$$
DELIMITER ;
DROP TRIGGER IF EXISTS `administrates_users_BEFORE_UPDATE`;

DELIMITER $$
CREATE DEFINER = CURRENT_USER TRIGGER `administrates_users_BEFORE_UPDATE` BEFORE UPDATE ON `administrates_users` FOR EACH ROW
BEGIN
set new.updated = now();
END$$
DELIMITER ;
