create or replace view v_campaign_lead_infos as
    select
        c.id as `CampaignId`,
        c.company_id as `CompanyId`,
        c.name as `Campaign Name`,
        l.id as lead_id,
        l.lead_reference  as `Lead Reference`,
        l.created as Created,
        l.salutation as Salutation,
        l.first_name as Firstname,
        l.last_name as Lastname,
        l.email as Email,
        l.postal_code as Postalcode,
        l.status as Status,
        l.trafficsource as Trafficsource,
        l.utm_parameters `UTM Parameters`,
        l.`affiliateID` as `AffiliateID`,
        l.`affiliateSubID`  as `AffiliateSubID`,
        cf.fieldname as `name`,
        lcf.lead_customfield_content as `value`
    from campaign c
             join leads l on l.campaign_id = c.id
             join customfields cf on cf.campaign_id = c.id
             join lead_customfield_content lcf on lcf.leads_id = l.id and lcf.leads_id = l.id
    union
    select
        c.id as `CampaignId`,
        c.company_id as `CompanyId`,
        c.name as `Campaign Name`,
        l.id as lead_id,
        l.lead_reference  as `Lead Reference`,
        l.created as Created,
        l.salutation as Salutation,
        l.first_name as Firstname,
        l.last_name as Lastname,
        l.email as Email,
        l.postal_code as Postalcode,
        l.status as Status,
        l.trafficsource as Trafficsource,
        l.utm_parameters `UTM Parameters`,
        l.`affiliateID` as `AffiliateID`,
        l.`affiliateSubID`  as `AffiliateSubID`,
        q.question_question_txt as `name`,
        lac.lead_answer_content AS `value`
    from campaign c
             join leads l on l.campaign_id = c.id
             join survey s on s.id = c.survey_id
             join questions q on q.survey_id = s.id
             join lead_answer_content lac on lac.leads_id = l.id and lac.questions_id = q.id;
