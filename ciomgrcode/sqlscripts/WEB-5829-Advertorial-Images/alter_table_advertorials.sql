ALTER TABLE `advertorials`
ADD COLUMN `advertorials_surrogate_id` VARCHAR(170) NULL COMMENT 'an artifical unique id based on advertorials hashed filename, date(U) and uniqid to provide image url with an advertorial reference while the advertorial record does not exist at that moment. references advertiorial folders of file system for images' AFTER `id`;

-- add id's to existing (up to now: dummy-) records, to enable alter table with not null constraint and unique index in next statement
UPDATE `advertorials` SET advertorials_surrogate_id = id WHERE id > 0;

-- now add not null constraint and unique index
ALTER TABLE `advertorials`
CHANGE COLUMN `advertorials_surrogate_id` `advertorials_surrogate_id` VARCHAR(170) NOT NULL COMMENT 'an artifical unique  id based on advertorials hashed filename, date(U) and uniqid to provide image url with an advertorial reference while the advertorial record does not exist at that moment. references advertiorial folders of file system for images' ,
ADD UNIQUE INDEX `advertorials_surrogate_id_UNIQUE` (`advertorials_surrogate_id` ASC);

