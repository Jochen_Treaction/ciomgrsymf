CREATE TABLE IF NOT EXISTS `advertorial_images` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `advertorials_id` BIGINT UNSIGNED NOT NULL COMMENT 'reference to advertorials',
    `original_url` TEXT NOT NULL COMMENT 'img src attribute url entry found in an source HTML advertorial',
    `url` TEXT NOT NULL COMMENT 'new img src attribute url entry that replaces original_url in HTML advertorial',
    `original_image_name` VARCHAR(255) NOT NULL COMMENT 'original name of img file found in original_url',
    `image_hash_name` VARCHAR(255) NOT NULL COMMENT 'new img file name of original_image_name, used in target advertorial',
    `image_content` MEDIUMBLOB NOT NULL COMMENT 'the image file itself',
    `mime_type` VARCHAR(255) NULL DEFAULT NULL COMMENT 'mime type of the image',
    scan_line_number INT(11) UNSIGNED NULL DEFAULT NULL COMMENT 'line number where the img was found in the HTML advertorial',
    scan_status tinyint NULL COMMENT 'status if the found src attribute of img tag pointed to a real image or not',
    scan_status_reason VARCHAR(255) NULL DEFAULT NULL COMMENT 'reason of scan_status',
    transfer_status TEXT NULL DEFAULT NULL COMMENT 'status of an img transfer to serveral servers in JSON format', -- JSON => [{"server_ip_address" => string, "transfer_status" => bool }, ..]
    `created` DATETIME NULL DEFAULT current_timestamp(),
    `updated` DATETIME NULL DEFAULT current_timestamp(),
    `created_by` INT(11) UNSIGNED NULL DEFAULT NULL,
    `updated_by` INT(11) UNSIGNED NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_advertorial_images_advertorials1_idx` (`advertorials_id` ASC),
    CONSTRAINT `fk_advertorial_images_advertorials1`
    FOREIGN KEY (`advertorials_id`)
    REFERENCES `advertorials` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;



DELIMITER $$

CREATE  TRIGGER `advertorial_images_BEFORE_INSERT` BEFORE INSERT ON `advertorial_images` FOR EACH ROW BEGIN
    declare jetzt datetime;
SET jetzt = now();
set new.created = jetzt;
set new.updated = jetzt;
END$$

CREATE TRIGGER `advertorial_images_BEFORE_UPDATE` BEFORE UPDATE ON `advertorial_images` FOR EACH ROW BEGIN
        set new.updated = now();
END$$

DELIMITER ;
