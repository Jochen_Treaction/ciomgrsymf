DROP TABLE IF EXISTS `advertorials`;
CREATE TABLE `advertorials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `objectregister_id` bigint(20) unsigned NOT NULL,
  `company_id` bigint(20) unsigned NOT NULL,
  `seed_pool_group_id` bigint(20) unsigned NOT NULL,
  `advertorial_name` varchar(170) DEFAULT NULL,
  `advertorial_description` varchar(170) DEFAULT NULL,
  `advertorial_begin_date` datetime DEFAULT NULL,
  `advertorial_end_date` datetime DEFAULT NULL,
  `file_name_html` varchar(170) DEFAULT NULL COMMENT 'html file name',
  `content_uploaded_html_file` longtext COMMENT 'zipped and base64 encoded html file content which was uploaded step 1',
  `content_extract_images` longtext COMMENT 'zipped and base64 encoded html file content with  changed image pathes step 2',
  `content_inline_css` longtext COMMENT 'zipped and base64 encoded html file content with inline css step 3 (can be left out)',
  `content_clean_html` longtext COMMENT 'zipped and base64 encoded with cleanup html code step 4',
  `file_name_zip` varchar(170) DEFAULT NULL COMMENT 'zip file name (might be empty)',
  `content_uploaded_zip_file` longtext COMMENT 'base64 encoded zip file (might be empty)',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `updated_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_advertorial_company_id_idx` (`company_id`),
  KEY `fk_advertorial_seed_pool_group_id_idx` (`seed_pool_group_id`),
  KEY `fk_objectregister_id_idx` (`objectregister_id`),
  CONSTRAINT `fk_advertorial_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  CONSTRAINT `fk_advertorial_objectregister_id` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`),
  CONSTRAINT `fk_advertorial_seed_pool_group_id` FOREIGN KEY (`seed_pool_group_id`) REFERENCES `seed_pool_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;





DROP TRIGGER IF EXISTS `advertorials_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `advertorials_BEFORE_INSERT` BEFORE INSERT ON `advertorials` FOR EACH ROW
BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt = NOW();
    set new.created = jetzt;
    set new.updated = jetzt;
END$$
DELIMITER ;
DROP TRIGGER IF EXISTS `advertorials_BEFORE_UPDATE`;

DELIMITER $$
CREATE TRIGGER `advertorials_BEFORE_UPDATE` BEFORE UPDATE ON `advertorials` FOR EACH ROW
BEGIN
set new.updated = NOW();
END$$
DELIMITER ;
