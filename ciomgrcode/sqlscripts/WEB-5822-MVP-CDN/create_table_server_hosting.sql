CREATE TABLE `server_hosting` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `host_name` VARCHAR(255) NOT NULL DEFAULT 'CHANGE HOST NAME',
  `created` DATETIME NULL DEFAULT current_timestamp(),
  `updated` DATETIME NULL DEFAULT current_timestamp(),
  `created_by` INT(11) NULL DEFAULT NULL,
  `updated_by` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4 COMMENT='!!! BE CAREFUL WHEN DELETING RECORDS HERE !!! => deletes cascading records in server_configuration';


DELIMITER $$
CREATE TRIGGER `server_hosting_BEFORE_INSERT` BEFORE INSERT ON `server_hosting` FOR EACH ROW
BEGIN
DECLARE jetzt DATETIME;
set jetzt = NOW();
set new.created = jetzt;
set new.updated = jetzt;
END$$


CREATE TRIGGER `server_hosting_BEFORE_UPDATE` BEFORE UPDATE ON `server_hosting` FOR EACH ROW
BEGIN
set new.updated = now();
END$$


CREATE TRIGGER `server_hosting_BEFORE_DELETE` BEFORE DELETE ON `server_hosting` FOR EACH ROW
BEGIN
    SIGNAL SQLSTATE '45000'
    SET MESSAGE_TEXT = 'Turn off this trigger server_hosting_BEFORE_DELETE first before you delete rows. be aware that on delete corresponding rows in server_configuration are deleted as well for data consistency';
END$$

DELIMITER ;

INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('CLOUDZY',22,22);
INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('DomainFactory',22,22);
INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('Host Europe',22,22);
INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('HOSTINGER',22,22);
INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('IONOS',22,22);
INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('MONOVM',22,22);
INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('OVH',22,22);
INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('Strato',22,22);
INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('webtropia',22,22);
INSERT INTO server_hosting (host_name, created_by, updated_by) VALUES ('NO HOSTING PROVIDER ASSIGNED',22,22);



