INSERT INTO `unique_object_type`
(
`assigned_table`,
`unique_object_name`,
`has_versioning`,
`has_encryption`,
`description`,
`created_by`,
`updated_by`)
VALUES
(
'server_configuration',
'server',
0,
0,
'Server configuration with plesk and sftp access for (servers host email domains)',
1,
1);


INSERT INTO statusdef (value, created_by, updated_by) VALUES ('deleted', 1,1);

INSERT INTO status_model (unique_object_type_id, statusdef_id, modelname, created_by, updated_by)
VALUES ((select id from unique_object_type where assigned_table='server_configuration'), 1, 'server', 1,1); -- active

INSERT INTO status_model (unique_object_type_id, statusdef_id, modelname, created_by, updated_by)
VALUES ((select id from unique_object_type where assigned_table='server_configuration'), 2, 'server', 1,1); -- draft

INSERT INTO status_model (unique_object_type_id, statusdef_id, modelname, created_by, updated_by)
VALUES ((select id from unique_object_type where assigned_table='server_configuration'), 4, 'server', 1,1); -- inactive

INSERT INTO status_model (unique_object_type_id, statusdef_id, modelname, created_by, updated_by)
VALUES ((select id from unique_object_type where assigned_table='server_configuration'), (select id from statusdef where value='deleted'), 'server', 1,1); -- inactive

DROP TABLE IF EXISTS `server_configuration`;
CREATE TABLE `server_configuration` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `objectregister_id` bigint(20) unsigned NOT NULL COMMENT 'objectregister id of server_configuration, reference to table objectregister',
  `server_hosting_id` bigint(20) unsigned NOT NULL COMMENT 'reference to table server_hosting',
  `company_id` bigint(20) unsigned NOT NULL COMMENT 'referenece to table company',
  `seed_pool_group_id` bigint(20) unsigned NOT NULL COMMENT 'id of assigned see pool group, reference to table seed_pool_group',
  `ip_address` varchar(40) DEFAULT NULL COMMENT 'IPv4 or IPv6 of server',
  `internal_name` varchar(255) NOT NULL COMMENT 'treaction internal name to indentify the server and its relation to a company',
  `description` text DEFAULT NULL COMMENT 'further descriptions and comments',
  `default_domain` varchar(255) DEFAULT NULL COMMENT 'the main domain on that server, where domain-aliases are attached to',
  `segment` varchar(255) DEFAULT NULL COMMENT 'internal segment,  assignment to an ESP',
  `cloudflare` tinyint(4) DEFAULT NULL COMMENT 'flag if server is protected by cloudflare',
  `blacklist` varchar(255) DEFAULT NULL COMMENT 'removed from UI do to PO requirement',
  `plesk_url` varchar(255) DEFAULT NULL COMMENT 'url for plesk access',
  `plesk_user` varchar(255) DEFAULT NULL COMMENT 'plesk user',
  `plesk_pw` varchar(255) DEFAULT NULL COMMENT 'plesk password',
  `created` datetime DEFAULT current_timestamp(),
  `updated` datetime DEFAULT current_timestamp(),
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated_by` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_srvcfg_objectregister_id_idx` (`objectregister_id`),
  KEY `fk_srvcfg_seedpoolgroup_idx` (`seed_pool_group_id`),
  KEY `fk_srvgrp_company_idx` (`company_id`),
  KEY `fk_srvcfg_srv_hosting_idx` (`server_hosting_id`),
  CONSTRAINT `fk_srvcfg_objectregister_id` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_srvcfg_seedpoolgroup` FOREIGN KEY (`seed_pool_group_id`) REFERENCES `seed_pool_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_srvcfg_srv_hosting` FOREIGN KEY (`server_hosting_id`) REFERENCES `server_hosting` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_srvgrp_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='configuration of plesk access for servers that are used for email marketing';

DELIMITER $$
CREATE TRIGGER `server_configuration_BEFORE_INSERT` BEFORE INSERT ON `server_configuration` FOR EACH ROW
BEGIN
set new.created = now();
set new.updated = now();
END$$

CREATE TRIGGER `server_configuration_BEFORE_UPDATE` BEFORE UPDATE ON `server_configuration` FOR EACH ROW
BEGIN
set new.updated = now();
END$$

DELIMITER ;

CREATE TABLE `server_sftp_configuration` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `server_configuration_id` bigint(20) unsigned NOT NULL COMMENT 'reference to server_configuration',
  `sftp_user` varchar(255) NOT NULL COMMENT 'ftp user',
  `sftp_pw` varchar(255) NOT NULL COMMENT 'ftp password',
  `comment` text DEFAULT NULL COMMENT 'optional comments',
  `created` datetime DEFAULT current_timestamp(),
  `updated` datetime DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_server_configuration_idx` (`server_configuration_id`),
  CONSTRAINT `fk_server_configuration` FOREIGN KEY (`server_configuration_id`) REFERENCES `server_configuration` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='assigned sftp connections to server_configuration for servers that are used for email marketing';

DELIMITER $$
CREATE TRIGGER `server_sftp_configuration_BEFORE_INSERT` BEFORE INSERT ON `server_sftp_configuration` FOR EACH ROW
BEGIN
set new.created = now();
set new.updated = now();
END$$

CREATE TRIGGER `server_sftp_configuration_BEFORE_UPDATE` BEFORE UPDATE ON `server_sftp_configuration` FOR EACH ROW
BEGIN
set new.updated = now();
END$$

DELIMITER ;


