alter table leads
    add language varchar(255) null after customer_group;

INSERT INTO `miostandardfield`
(
 `datatypes_id`,
 `fieldname`,
 `htmllabel`,
 `html_placeholder`,
 `fieldtype`,
 `hidden_field`
 )
VALUES
    (
      (select id from datatypes where name = 'Text'),
     'language',
     'Contact.Language',
     'Language',
     'Technical',
     '1'
     );

