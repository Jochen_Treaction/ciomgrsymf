-- advertorials
ALTER TABLE `advertorials` DROP FOREIGN KEY `fk_advertorial_objectregister_id`;
ALTER TABLE `advertorials` ADD CONSTRAINT `fk_advertorial_objectregister_id`  FOREIGN KEY (`objectregister_id`)  REFERENCES `objectregister` (`id`)  ON DELETE CASCADE;

-- seed_pool
ALTER TABLE `seed_pool` DROP FOREIGN KEY `fk_seed_pool_objectregister1`;
ALTER TABLE `seed_pool` ADD CONSTRAINT `fk_seed_pool_objectregister1`   FOREIGN KEY (`objectregister_id`)  REFERENCES `objectregister` (`id`)  ON DELETE CASCADE;

-- inbox_monitor
ALTER TABLE `inbox_monitor` DROP FOREIGN KEY `fk_inbox_monitor_objectregister1`;
ALTER TABLE `inbox_monitor` ADD CONSTRAINT `fk_inbox_monitor_objectregister1`   FOREIGN KEY (`objectregister_id`)  REFERENCES `objectregister` (`id`)  ON DELETE CASCADE;
