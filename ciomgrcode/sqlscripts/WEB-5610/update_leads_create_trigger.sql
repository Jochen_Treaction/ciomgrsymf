BEGIN if new.created is null then
set new.created = now();
end if;
set new.updated = now();
END