-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 10, 2021 at 04:07 PM
-- Server version: 10.1.48-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mio`
--

-- --------------------------------------------------------
--
-- Dumping data for table `standardfieldmapping`
--

INSERT INTO `standardfieldmapping` (`id`, `system_id`, `miostandardfield_id`, `name`, `datatype`)
VALUES (35, 2, 1, 'email', 'string'),
       (36, 2, 2, 'salutation', 'string'),
       (37, 2, 3, 'firstname', 'string'),
       (38, 2, 4, 'lastname', 'string'),
       (39, 2, 5, 'company', 'string'),
       (40, 2, 6, 'address', 'string'),
       (41, 2, 7, 'zip', 'string'),
       (42, 2, 8, 'city', 'string'),
       (43, 2, 9, 'country', 'string'),
       (44, 2, 10, 'phone', 'string'),
       (45, 2, 11, 'MIO_lead_reference', 'string'),
       (46, 2, 12, 'MIO_url', 'string'),
       (47, 2, 14, 'MIO_trafficsource', 'string'),
       (48, 2, 15, 'MIO_utm_parameters', 'string'),
       (49, 2, 17, 'MIO_targetgroup', 'string'),
       (50, 2, 18, 'MIO_ip', 'string'),
       (51, 2, 19, 'MIO_device_type', 'string'),
       (52, 2, 20, 'MIO_device_browser', 'string'),
       (53, 2, 21, 'MIO_device_os', 'string'),
       (54, 2, 22, 'MIO_device_os_version', 'string'),
       (55, 2, 23, 'MIO_device_browser_version', 'string'),
       (130, 1, 31, 'MIO_birthday', 'string'),
       (131, 1, 8, 'CITY', 'string'),
       (132, 1, 9, 'COUNTRY', 'string'),
       (133, 1, 1, 'EMAIL', 'string'),
       (134, 1, 5, 'ORGANIZATION', 'string'),
       (135, 1, 3, 'FIRSTNAME', 'string'),
       (136, 1, 30, 'HNR', 'string'),
       (137, 1, 4, 'LASTNAME', 'string'),
       (138, 1, 10, 'MIO_phone', 'string'),
       (139, 1, 7, 'ZIP', 'string'),
       (140, 1, 2, 'SALUTATION', 'string'),
       (141, 1, 6, 'ADDRESS', 'string'),
       (142, 1, 12, 'MIO_url', 'string'),
       (143, 1, 25, 'MIO_webpage', 'string'),
       (144, 1, 42, 'MIO_affiliate_id', 'string'),
       (145, 1, 43, 'MIO_affiliate_sub_id', 'string'),
       (146, 1, 20, 'MIO_device_browser', 'string'),
       (147, 1, 23, 'MIO_device_browser_version', 'string'),
       (148, 1, 21, 'MIO_device_os', 'string'),
       (149, 1, 22, 'MIO_device_os_version', 'string'),
       (150, 1, 19, 'MIO_device_type', 'string'),
       (151, 1, 28, 'MIO_doi_ip', 'string'),
       (152, 1, 29, 'MIO_doi_timestamp', 'string'),
       (153, 1, 13, 'MIO_final_url', 'string'),
       (154, 1, 18, 'MIO_ip', 'string'),
       (155, 1, 11, 'MIO_lead_reference', 'string'),
       (156, 1, 27, 'MIO_optin_ip', 'string'),
       (157, 1, 26, 'MIO_optin_timestamp', 'string'),
       (158, 1, 16, 'MIO_split_version', 'string'),
       (159, 1, 24, 'MIO_permission', 'string'),
       (160, 1, 38, 'MIO_referrer_id', 'string'),
       (161, 1, 17, 'MIO_target_group', 'string'),
       (162, 1, 14, 'MIO_traffic_source', 'string'),
       (163, 1, 15, 'MIO_utm_parameters', 'string'),
       (164, 1, 39, 'MIO_first_order_date', 'string'),
       (165, 1, 33, 'MIO_last_order_date', 'string'),
       (166, 1, 36, 'MIO_last_order_net_value', 'string'),
       (167, 1, 32, 'MIO_last_order_no', 'string'),
       (168, 1, 35, 'MIO_last_year_order_net_value', 'string'),
       (169, 1, 40, 'MIO_product_attributes', 'string'),
       (170, 1, 37, 'MIO_smart_tags', 'string'),
       (171, 1, 34, 'MIO_total_order_net_value', 'string');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `standardfieldmapping`
--
ALTER TABLE `standardfieldmapping`
    ADD PRIMARY KEY (`id`),
    ADD KEY `fk_standardfieldmapping_system1_idx` (`system_id`),
    ADD KEY `fk_standardfieldmapping_miostandardfield1_idx` (`miostandardfield_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `standardfieldmapping`
--
ALTER TABLE `standardfieldmapping`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 172;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `standardfieldmapping`
--
ALTER TABLE `standardfieldmapping`
    ADD CONSTRAINT `fk_standardfieldmapping_miostandardfield1` FOREIGN KEY (`miostandardfield_id`) REFERENCES `miostandardfield` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_standardfieldmapping_system1` FOREIGN KEY (`system_id`) REFERENCES `system` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
