-- mariadb
SET GLOBAL foreign_key_checks=OFF;

-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 10, 2021 at 03:54 PM
-- Server version: 10.1.48-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mio`
--

-- --------------------------------------------------------

--
-- Table structure for table `miostandardfield`
--

DROP TABLE IF EXISTS `miostandardfield`;

CREATE TABLE `miostandardfield`
(
    `id`               bigint(20) UNSIGNED NOT NULL,
    `datatypes_id`     bigint(20) UNSIGNED NOT NULL,
    `fieldname`        varchar(255)        NOT NULL,
    `htmllabel`        varchar(255)        NOT NULL,
    `html_placeholder` varchar(255) DEFAULT NULL,
    `fieldtype`        varchar(50)  DEFAULT NULL,
    `hidden_field`     tinyint(1)   DEFAULT '1'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

--
-- Indexes for dumped tables
--


INSERT INTO `miostandardfield` (`id`, `datatypes_id`, `fieldname`, `htmllabel`, `html_placeholder`, `fieldtype`,
                                `hidden_field`)
VALUES (1, 4, 'email', 'Contact.Email', 'Email', 'Standard', 0),
       (2, 8, 'salutation', 'Contact.Salutation', 'Salutation', 'Standard', 0),
       (3, 8, 'first_name', 'Contact.FirstName', 'FirstName', 'Standard', 0),
       (4, 8, 'last_name', 'Contact.LastName', 'LastName', 'Standard', 0),
       (5, 8, 'firma', 'Contact.Company', 'Company', 'Standard', 0),
       (6, 8, 'street', 'Contact.Street', 'Street', 'Standard', 0),
       (7, 8, 'postal_code', 'Contact.PostalCode', 'PostalCode', 'Standard', 0),
       (8, 8, 'city', 'Contact.City', 'City', 'Standard', 0),
       (9, 8, 'country', 'Contact.Country', 'Country', 'Standard', 0),
       (10, 7, 'phone', 'Contact.Phone', 'Phone', 'Standard', 0),
       (11, 8, 'lead_reference', 'Contact.Reference', 'Reference', 'Technical', 1),
       (12, 9, 'url', 'Contact.URL', 'URL', 'Standard', 0),
       (13, 9, 'final_url', 'Contact.FinalURL', 'FinalURL', 'Technical', 1),
       (14, 8, 'traffic_source', 'Contact.TrafficSource', 'TrafficSource', 'Technical', 1),
       (15, 8, 'utm_parameters', 'Contact.UTMParameters', 'UTMParameters', 'Technical', 1),
       (16, 8, 'split_version', 'Contact.SplitVersion', 'SplitVersion', 'Technical', 1),
       (17, 8, 'target_group', 'Contact.TargetGroup', 'TargetGroup', 'Technical', 1),
       (18, 8, 'ip', 'Contact.IP', 'IP', 'Technical', 1),
       (19, 8, 'device_type', 'Contact.DeviceType', 'DeviceType', 'Technical', 1),
       (20, 8, 'device_browser', 'Contact.DeviceBrowser', 'DeviceBrowser', 'Technical', 1),
       (21, 8, 'device_os', 'Contact.DeviceOS', 'DeviceOS', 'Technical', 1),
       (22, 8, 'device_os_version', 'Contact.DeviceOSVersion', 'DeviceOSVersion', 'Technical', 1),
       (23, 8, 'device_browser_version', 'Contact.DeviceBrowserVersion', 'DeviceBrowserVersion', 'Technical', 1),
       (24, 5, 'permission', 'Contact.Permission', 'Permission', 'Technical', 1),
       (25, 9, 'webpage', 'Contact.Webpage', 'Webpage', 'Standard', 0),
       (26, 2, 'optin_timestamp', 'Contact.SOITimestamp', 'SOITimestamp', 'Technical', 1),
       (27, 8, 'optin_ip', 'Contact.SOIIP', 'SOIIP', 'Technical', 1),
       (28, 8, 'doi_ip', 'Contact.DOIIP', 'DOIIP', 'Technical', 1),
       (29, 2, 'doi_timestamp', 'Contact.DOITimestamp', 'DOITimestamp', 'Technical', 1),
       (30, 8, 'house_number', 'Contact.HouseNumber', 'HouseNumber', 'Standard', 0),
       (31, 2, 'birthday', 'Contact.Birthday ', 'Birthday', 'Standard', 0),
       (32, 8, 'last_order_no', 'Contact.LastOrderNo', 'LastOrderNo', 'eCommerce', 0),
       (33, 8, 'last_order_date', 'Contact.LastOrderDate', 'LastOrderDate', 'eCommerce', 0),
       (34, 3, 'total_order_net_value', 'Contact.TotalOrderNetValue', 'TotalOrderNetValue', 'eCommerce', 0),
       (35, 3, 'last_year_order_net_value', 'Contact.LastYearOrderNetValue', 'LastYearOrderNetValue', 'eCommerce', 0),
       (36, 3, 'last_order_net_value', 'Contact.LastOrderNetValue', 'LastOrderNetValue', 'eCommerce', 0),
       (37, 6, 'smart_tags', 'Contact.eCommerceTags', 'eCommerceTags', 'eCommerce', 0),
       (38, 8, 'referrer_id', 'Contact.ReferrerID', 'ReferrerID', 'Technical', 1),
       (39, 8, 'first_order_date', 'Contact.FirstOrderDate', 'FirstOrderDate', 'eCommerce', 0),
       (40, 8, 'product_attributes', 'Contact.ProductAttributes', 'ProductAttributes', 'eCommerce', 0),
       (42, 8, 'affiliate_id', 'Contact.AffiliateID', 'AffiliateID', 'Technical', 0),
       (43, 8, 'affiliate_sub_id', 'Contact.AffiliateSubID', 'AffiliateSubID', 'Technical', 0);


--
-- Indexes for table `miostandardfield`
--
ALTER TABLE `miostandardfield`
    ADD PRIMARY KEY (`id`),
    ADD KEY `fk_standardfields_datatypes1_idx` (`datatypes_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `miostandardfield`
--
ALTER TABLE `miostandardfield`
    MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 44;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `miostandardfield`
--
ALTER TABLE `miostandardfield`
    ADD CONSTRAINT `fk_standardfields_datatypes1` FOREIGN KEY (`datatypes_id`) REFERENCES `datatypes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


--
-- Dumping data for table `miostandardfield`
--
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
-- mariadb
SET GLOBAL foreign_key_checks=ON;
