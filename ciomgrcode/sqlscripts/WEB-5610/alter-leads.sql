use mio;


/*Alter column names*/

alter table leads
    change targetgroup target_group text null,
    change trafficsource traffic_source text null,
    change affiliateID affiliate_id text null comment 'Affiliate Id from campaign URL',
    change affiliateSubID affiliate_sub_id text null comment 'AffiliateSubId from campaign URL';

alter table leads
    add birthday     varchar(50)  null after last_name,
    add referrer_id  varchar(255) null after affiliate_sub_id,
    add house_number varchar(50)  null after street;


/* Delete Hash Keys and Columns */

alter table leads
    drop column hash_street;

alter table leads
    drop column hash_phone;

alter table leads
    drop column hash_url;

alter table leads
    drop column hash_final_url;

alter table leads
    drop column hash_trafficsource;

alter table leads
    drop column hash_utm_parameters;

alter table leads
    drop column hash_split_version;

alter table leads
    drop column hash_targetgroup;

alter table leads
    drop column hash_affiliateID;

alter table leads
    drop column hash_affiliateSubID;

alter table leads
    drop column hash_ip;

alter table leads
    drop column hash_device_type;

alter table leads
    drop column hash_device_browser;

alter table leads
    drop column hash_device_os;

alter table leads
    drop column hash_device_os_version;

alter table leads
    drop column hash_device_browser_version;

alter table leads
    drop column hash_doi_ip;

alter table leads
    drop column hash_doi_timestamp;

alter table leads
    drop column hash_optin_timestamp;

alter table leads
    drop column hash_optin_ip;

alter table leads
    drop column hash_permission;

alter table leads
    drop column hash_webpage;

drop index ix_hash_campaign on leads;

alter table leads
    drop column hash_campaign;

drop index ix_hash_city on leads;

alter table leads
    drop column hash_city;

drop index ix_hash_country on leads;

alter table leads
    drop column hash_country;

drop index ix_hash_email on leads;

alter table leads
    drop column hash_email;

drop index ix_hash_firma on leads;

alter table leads
    drop column hash_firma;

drop index ix_hash_first_name on leads;

alter table leads
    drop column hash_first_name;

drop index ix_hash_last_name on leads;

alter table leads
    drop column hash_last_name;

drop index ix_hash_lead_reference on leads;

alter table leads
    drop column hash_lead_reference;

drop index ix_hash_postal_code on leads;

alter table leads
    drop column hash_postal_code;

drop index ix_hash_salutation on leads;

alter table leads
    drop column hash_salutation;

