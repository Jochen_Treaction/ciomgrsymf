use mio;

alter table leads
    add last_order_no             varchar(100) null after webpage,
    add last_order_date           varchar(100) null after last_order_no,
    add first_order_date          varchar(100) null after last_order_date,
    add total_order_net_value     double       null after first_order_date,
    add last_year_order_net_value double       null after total_order_net_value,
    add last_order_net_value      double       null after last_year_order_net_value,
    add smart_tags                mediumtext   null after last_order_net_value,
    add product_attributes        mediumtext   null after smart_tags;