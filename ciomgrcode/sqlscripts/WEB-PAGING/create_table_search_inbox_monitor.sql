-- IF TBALE IS NOT CREATED
-- CREATE TABLE `search_inbox_monitor` (
--   `id` bigint(20) unsigned NOT NULL,
--   `searchtext` text NOT NULL,
--   PRIMARY KEY (`id`),
--   FULLTEXT KEY `searchtext` (`searchtext`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='contains search text fragments of inbox_monitor and domains';



DELIMITER $$
CREATE TRIGGER after_insert_inbox_monitor AFTER INSERT ON inbox_monitor FOR EACH ROW
BEGIN
    DECLARE addtext TEXT;
    DECLARE seedpool_name TEXT;
    DECLARE seedpool_type TEXT;
    DECLARE seedpool_group_name TEXT;
    DECLARE domainname TEXT;
    DECLARE subdomain TEXT;

    SET seedpool_name = (SELECT `name` FROM seed_pool WHERE id = new.seed_pool_id);
    SET seedpool_type = (SELECT `type` FROM seed_pool WHERE id = new.seed_pool_id);
    SET seedpool_group_name = (SELECT spg.name FROM seed_pool sp JOIN seed_pool_group spg ON spg.id = sp.seed_pool_group_id WHERE sp.id = new.seed_pool_id);
    SET domainname = (SELECT `domain` FROM domains WHERE id = new.domain_id);
    SET subdomain = (SELECT subdomain FROM domains WHERE id = new.domain_id);

    SET addtext = CONCAT(
        IF( seedpool_name IS NULL OR seedpool_name='', '', CONCAT( seedpool_name, ' ')),
        IF( seedpool_type IS NULL OR seedpool_type='', '', CONCAT( seedpool_type, ' ')),
        IF( seedpool_group_name IS NULL OR seedpool_group_name='', '', CONCAT( seedpool_group_name, ' ')),
        IF( domainname IS NULL OR domainname='', '', CONCAT( domainname, ' ')),
        IF( domainname IS NULL OR domainname='', '', CONCAT( REGEXP_REPLACE(domainname, '\\.', ' '), ' ')),
        IF( subdomain IS NULL OR subdomain='', '', CONCAT( subdomain, ' ')),
        IF( subdomain IS NULL OR subdomain='', '', CONCAT( REGEXP_REPLACE(subdomain, '\\.', ' '), ' ')),
        IF( new.name IS NULL OR new.name='', '', CONCAT( new.name, ' ')),
        IF( new.sender_alias IS NULL OR new.sender_alias='', '', CONCAT( new.sender_alias, ' ')),
        IF( new.sender_address IS NULL OR new.sender_address='', '', CONCAT( new.sender_address, ' ')),
        IF( new.spam_reason IS NULL OR new.spam_reason='', '', CONCAT( new.spam_reason, ' ')),
        IF( new.created IS NULL OR new.created='', '', CONCAT( new.created, ' '))
    );

    INSERT INTO search_inbox_monitor (id, searchtext) VALUES (NEW.id, addtext);
END$$



-- RUN THIS AFTER INSTALLATION
DELIMITER $$
CREATE TRIGGER after_update_inbox_monitor AFTER UPDATE ON inbox_monitor FOR EACH ROW
BEGIN
    DECLARE addtext TEXT;
    DECLARE seedpool_name TEXT;
    DECLARE seedpool_type TEXT;
    DECLARE seedpool_group_name TEXT;
    DECLARE domainname TEXT;
    DECLARE subdomain TEXT;

    SET seedpool_name = (SELECT `name` FROM seed_pool WHERE id = new.seed_pool_id);
    SET seedpool_type = (SELECT `type` FROM seed_pool WHERE id = new.seed_pool_id);
    SET seedpool_group_name = (SELECT spg.name FROM seed_pool sp JOIN seed_pool_group spg ON spg.id = sp.seed_pool_group_id WHERE sp.id = new.seed_pool_id);
    SET domainname = (SELECT `domain` FROM domains WHERE id = new.domain_id);
    SET subdomain = (SELECT subdomain FROM domains WHERE id = new.domain_id);

    SET addtext = CONCAT(
        IF( seedpool_name IS NULL OR seedpool_name='', '', CONCAT( seedpool_name, ' ')),
        IF( seedpool_type IS NULL OR seedpool_type='', '', CONCAT( seedpool_type, ' ')),
        IF( seedpool_group_name IS NULL OR seedpool_group_name='', '', CONCAT( seedpool_group_name, ' ')),
        IF( domainname IS NULL OR domainname='', '', CONCAT( domainname, ' ')),
        IF( domainname IS NULL OR domainname='', '', CONCAT( REGEXP_REPLACE(domainname, '\\.', ' '), ' ')),
        IF( subdomain IS NULL OR subdomain='', '', CONCAT( subdomain, ' ')),
        IF( subdomain IS NULL OR subdomain='', '', CONCAT( REGEXP_REPLACE(subdomain, '\\.', ' '), ' ')),
        IF( OLD.name IS NULL OR OLD.name='', '', CONCAT( OLD.name, ' ')),
        IF( OLD.sender_alias IS NULL OR OLD.sender_alias='', '', CONCAT( OLD.sender_alias, ' ')),
        IF( OLD.sender_address IS NULL OR OLD.sender_address='', '', CONCAT( OLD.sender_address, ' ')),
        IF( OLD.spam_reason IS NULL OR OLD.spam_reason='', '', CONCAT( new.spam_reason, ' ')),
        IF( OLD.created IS NULL OR OLD.created='', '', CONCAT( OLD.created, ' '))
    );

    INSERT INTO search_inbox_monitor (id, searchtext) VALUES (OLD.id, addtext);
END$$
DELIMITER ;
update inbox_monitor set updated_by = 99999 where id > 0;
DROP TRIGGER after_update_inbox_monitor;
SELECT COUNT(*) FROM search_inbox_monitor;
-- END RUN THIS AFTER INSTALLATION
