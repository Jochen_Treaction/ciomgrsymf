-- create_table_searchleads_and_triggers_for_leads_and_lead_customfield_content.sql
DELIMITER ;
CREATE TABLE `searchleads` (
  `id` bigint(20) unsigned NOT NULL,
  `searchtext` text NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `searchtext` (`searchtext`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------------- leads -----------------------------------

DELIMITER $$
CREATE TRIGGER after_insert_leads AFTER INSERT ON leads FOR EACH ROW
BEGIN
    DECLARE addtext TEXT;
    DECLARE txt_permisson TEXT;
    CASE new.permission
        WHEN 1 THEN SET txt_permisson='No Permission NoPermission';
        WHEN 2 THEN SET txt_permisson='Single Opt-In';
        WHEN 3 THEN SET txt_permisson='Confirmed Opt-In';
        WHEN 4 THEN SET txt_permisson='Double Opt-In';
        WHEN 5 THEN SET txt_permisson='Double Opt-In Single User Tracking';
        ELSE BEGIN SET txt_permisson='No Permission NoPermission'; END;
    END CASE;


    SET addtext = CONCAT(
        IF( new.email IS NULL OR new.email = '', '', CONCAT( new.email,' ')),
        IF( new.email IS NULL OR new.email = '', '', CONCAT( REGEXP_REPLACE(new.email, '[[.@[-[_]+', ' ' ),' ')),
        IF( new.salutation IS NULL OR new.salutation = '', '', CONCAT( new.salutation,' ')),
        IF( new.first_name IS NULL OR new.first_name = '', '', CONCAT( new.first_name,' ')),
        IF( new.last_name IS NULL OR new.last_name = '', '', CONCAT( new.last_name,' ')),
        IF( new.birthday IS NULL OR new.birthday = '', '', CONCAT( new.birthday,' ')),
        IF( new.firma IS NULL OR new.firma = '', '', CONCAT( new.firma,' ')),
        IF( new.street IS NULL OR new.street = '', '', CONCAT( new.street,' ')),
        IF( new.house_number IS NULL OR new.house_number = '', '', CONCAT( new.house_number,' ')),
        IF( new.postal_code IS NULL OR new.postal_code = '', '', CONCAT( new.postal_code,' ')),
        IF( new.city IS NULL OR new.city = '', '', CONCAT( new.city,' ')),
        IF( new.country IS NULL OR new.country = '', '', CONCAT( new.country,' ')),
        IF( new.phone IS NULL OR new.phone = '', '', CONCAT( new.phone,' ')),
        IF( new.campaign IS NULL OR new.campaign = '', '', CONCAT( new.campaign,' ')),
        IF( new.lead_reference IS NULL OR new.lead_reference = '', '', CONCAT( new.lead_reference,' ')),
        IF( new.url IS NULL OR new.url = '', '', CONCAT( new.url,' ')),
        IF( new.final_url IS NULL OR new.final_url = '', '', CONCAT( new.final_url,' ')),
        IF( new.traffic_source IS NULL OR new.traffic_source = '', '', CONCAT( new.traffic_source,' ')),
        IF( new.utm_parameters IS NULL OR new.utm_parameters = '', '', CONCAT( new.utm_parameters,' ')),
        IF( new.split_version IS NULL OR new.split_version = '', '', CONCAT( new.split_version,' ')),
        IF( new.target_group IS NULL OR new.target_group = '', '', CONCAT( new.target_group,' ')),
        IF( new.affiliate_id IS NULL OR new.affiliate_id = '', '', CONCAT( new.affiliate_id,' ')),
        IF( new.affiliate_sub_id IS NULL OR new.affiliate_sub_id = '', '', CONCAT( new.affiliate_sub_id,' ')),
        IF( new.referrer_id IS NULL OR new.referrer_id = '', '', CONCAT( new.referrer_id,' ')),
        IF( new.ip IS NULL OR new.ip = '', '', CONCAT( new.ip,' ')),
        IF( new.device_type IS NULL OR new.device_type = '', '', CONCAT( new.device_type,' ')),
        IF( new.device_browser IS NULL OR new.device_browser = '', '', CONCAT( new.device_browser,' ')),
        IF( new.device_os IS NULL OR new.device_os = '', '', CONCAT( new.device_os,' ')),
        IF( new.device_os_version IS NULL OR new.device_os_version = '', '', CONCAT( new.device_os_version,' ')),
        IF( new.device_browser_version IS NULL OR new.device_browser_version = '', '', CONCAT( new.device_browser_version,' ')),
        IF( new.doi_ip IS NULL OR new.doi_ip = '', '', CONCAT( new.doi_ip,' ')),
        IF( new.doi_timestamp IS NULL OR new.doi_timestamp = '', '', CONCAT( new.doi_timestamp,' ')),
        IF( new.optin_timestamp IS NULL OR new.optin_timestamp = '', '', CONCAT( new.optin_timestamp,' ')),
        IF( new.optin_ip IS NULL OR new.optin_ip = '', '', CONCAT( new.optin_ip,' ')),
        IF( new.permission IS NULL OR new.permission = '', '', CONCAT( new.permission,' ', txt_permisson, ' ')),
        IF( new.webpage IS NULL OR new.webpage = '', '', CONCAT( new.webpage,' ')),
        IF( new.smart_tags IS NULL OR new.smart_tags = '', '', CONCAT( new.smart_tags,' ')),
        IF( new.product_attributes IS NULL OR new.product_attributes = '', '', CONCAT( new.product_attributes,' ')),
        IF( new.created IS NULL OR new.created = '', '', CONCAT( new.created,' '))
    );

    INSERT INTO searchleads (id, searchtext) VALUES (NEW.id, addtext);
END$$

DELIMITER $$
CREATE TRIGGER leads_AFTER_UPDATE AFTER UPDATE ON leads FOR EACH ROW
BEGIN
    DECLARE addtext TEXT;
    DECLARE txt_permisson TEXT;
    DECLARE id_permission INT;

    SET id_permission = IF( new.permission IS NULL OR new.permission ='', IF(old.permission IS NULL OR old.permission='', '', old.permission), new.permission);

    CASE id_permission
        WHEN 1 THEN SET txt_permisson='No Permission NoPermission';
        WHEN 2 THEN SET txt_permisson='Single Opt-In';
        WHEN 3 THEN SET txt_permisson='Confirmed Opt-In';
        WHEN 4 THEN SET txt_permisson='Double Opt-In';
        WHEN 5 THEN SET txt_permisson='Double Opt-In Single User Tracking';
        ELSE BEGIN SET txt_permisson='No Permission NoPermission'; END;
    END CASE;

    SET addtext = CONCAT(
        IF( new.email IS NULL OR new.email ='', IF(old.email IS NULL OR old.email='', '', CONCAT(old.email, ' ')), CONCAT( new.email,' ')),
        IF( new.email IS NULL OR new.email ='', IF(old.email IS NULL OR old.email='', '', CONCAT(REGEXP_REPLACE(old.email, '[[.@[-[_]+', ' '), ' ')), CONCAT( REGEXP_REPLACE(new.email, '[[.@[-[_]+', ' '),' ')),
        IF( new.salutation IS NULL OR new.salutation ='', IF(old.salutation IS NULL OR old.salutation='', '', CONCAT(old.salutation, ' ')), CONCAT( new.salutation,' ')),
        IF( new.first_name IS NULL OR new.first_name ='', IF(old.first_name IS NULL OR old.first_name='', '', CONCAT(old.first_name, ' ')), CONCAT( new.first_name,' ')),
        IF( new.last_name IS NULL OR new.last_name ='', IF(old.last_name IS NULL OR old.last_name='', '', CONCAT(old.last_name, ' ')), CONCAT( new.last_name,' ')),
        IF( new.birthday IS NULL OR new.birthday ='', IF(old.birthday IS NULL OR old.birthday='', '', CONCAT(old.birthday, ' ')), CONCAT( new.birthday,' ')),
        IF( new.firma IS NULL OR new.firma ='', IF(old.firma IS NULL OR old.firma='', '', CONCAT(old.firma, ' ')), CONCAT( new.firma,' ')),
        IF( new.street IS NULL OR new.street ='', IF(old.street IS NULL OR old.street='', '', CONCAT(old.street, ' ')), CONCAT( new.street,' ')),
        IF( new.house_number IS NULL OR new.house_number ='', IF(old.house_number IS NULL OR old.house_number='', '', CONCAT(old.house_number, ' ')), CONCAT( new.house_number,' ')),
        IF( new.postal_code IS NULL OR new.postal_code ='', IF(old.postal_code IS NULL OR old.postal_code='', '', CONCAT(old.postal_code, ' ')), CONCAT( new.postal_code,' ')),
        IF( new.city IS NULL OR new.city ='', IF(old.city IS NULL OR old.city='', '', CONCAT(old.city, ' ')), CONCAT( new.city,' ')),
        IF( new.country IS NULL OR new.country ='', IF(old.country IS NULL OR old.country='', '', CONCAT(old.country, ' ')), CONCAT( new.country,' ')),
        IF( new.phone IS NULL OR new.phone ='', IF(old.phone IS NULL OR old.phone='', '', CONCAT(old.phone, ' ')), CONCAT( new.phone,' ')),
        IF( new.campaign IS NULL OR new.campaign ='', IF(old.campaign IS NULL OR old.campaign='', '', CONCAT(old.campaign, ' ')), CONCAT( new.campaign,' ')),
        IF( new.lead_reference IS NULL OR new.lead_reference ='', IF(old.lead_reference IS NULL OR old.lead_reference='', '', CONCAT(old.lead_reference, ' ')), CONCAT( new.lead_reference,' ')),
        IF( new.url IS NULL OR new.url ='', IF(old.url IS NULL OR old.url='', '', CONCAT(old.url, ' ')), CONCAT( new.url,' ')),
        IF( new.final_url IS NULL OR new.final_url ='', IF(old.final_url IS NULL OR old.final_url='', '', CONCAT(old.final_url, ' ')), CONCAT( new.final_url,' ')),
        IF( new.traffic_source IS NULL OR new.traffic_source ='', IF(old.traffic_source IS NULL OR old.traffic_source='', '', CONCAT(old.traffic_source, ' ')), CONCAT( new.traffic_source,' ')),
        IF( new.utm_parameters IS NULL OR new.utm_parameters ='', IF(old.utm_parameters IS NULL OR old.utm_parameters='', '', CONCAT(old.utm_parameters, ' ')), CONCAT( new.utm_parameters,' ')),
        IF( new.split_version IS NULL OR new.split_version ='', IF(old.split_version IS NULL OR old.split_version='', '', CONCAT(old.split_version, ' ')), CONCAT( new.split_version,' ')),
        IF( new.target_group IS NULL OR new.target_group ='', IF(old.target_group IS NULL OR old.target_group='', '', CONCAT(old.target_group, ' ')), CONCAT( new.target_group,' ')),
        IF( new.affiliate_id IS NULL OR new.affiliate_id ='', IF(old.affiliate_id IS NULL OR old.affiliate_id='', '', CONCAT(old.affiliate_id, ' ')), CONCAT( new.affiliate_id,' ')),
        IF( new.affiliate_sub_id IS NULL OR new.affiliate_sub_id ='', IF(old.affiliate_sub_id IS NULL OR old.affiliate_sub_id='', '', CONCAT(old.affiliate_sub_id, ' ')), CONCAT( new.affiliate_sub_id,' ')),
        IF( new.referrer_id IS NULL OR new.referrer_id ='', IF(old.referrer_id IS NULL OR old.referrer_id='', '', CONCAT(old.referrer_id, ' ')), CONCAT( new.referrer_id,' ')),
        IF( new.ip IS NULL OR new.ip ='', IF(old.ip IS NULL OR old.ip='', '', CONCAT(old.ip, ' ')), CONCAT( new.ip,' ')),
        IF( new.device_type IS NULL OR new.device_type ='', IF(old.device_type IS NULL OR old.device_type='', '', CONCAT(old.device_type, ' ')), CONCAT( new.device_type,' ')),
        IF( new.device_browser IS NULL OR new.device_browser ='', IF(old.device_browser IS NULL OR old.device_browser='', '', CONCAT(old.device_browser, ' ')), CONCAT( new.device_browser,' ')),
        IF( new.device_os IS NULL OR new.device_os ='', IF(old.device_os IS NULL OR old.device_os='', '', CONCAT(old.device_os, ' ')), CONCAT( new.device_os,' ')),
        IF( new.device_os_version IS NULL OR new.device_os_version ='', IF(old.device_os_version IS NULL OR old.device_os_version='', '', CONCAT(old.device_os_version, ' ')), CONCAT( new.device_os_version,' ')),
        IF( new.device_browser_version IS NULL OR new.device_browser_version ='', IF(old.device_browser_version IS NULL OR old.device_browser_version='', '', CONCAT(old.device_browser_version, ' ')), CONCAT( new.device_browser_version,' ')),
        IF( new.doi_ip IS NULL OR new.doi_ip ='', IF(old.doi_ip IS NULL OR old.doi_ip='', '', CONCAT(old.doi_ip, ' ')), CONCAT( new.doi_ip,' ')),
        IF( new.doi_timestamp IS NULL OR new.doi_timestamp ='', IF(old.doi_timestamp IS NULL OR old.doi_timestamp='', '', CONCAT(old.doi_timestamp, ' ')), CONCAT( new.doi_timestamp,' ')),
        IF( new.optin_timestamp IS NULL OR new.optin_timestamp ='', IF(old.optin_timestamp IS NULL OR old.optin_timestamp='', '', CONCAT(old.optin_timestamp, ' ')), CONCAT( new.optin_timestamp,' ')),
        IF( new.optin_ip IS NULL OR new.optin_ip ='', IF(old.optin_ip IS NULL OR old.optin_ip='', '', CONCAT(old.optin_ip, ' ')), CONCAT( new.optin_ip,' ')),
        IF( new.permission IS NULL OR new.permission ='', IF(old.permission IS NULL OR old.permission='', '', CONCAT(old.permission, ' ', txt_permisson, ' ')), CONCAT( new.permission,' ', txt_permisson, ' ')),
        IF( new.webpage IS NULL OR new.webpage ='', IF(old.webpage IS NULL OR old.webpage='', '', CONCAT(old.webpage, ' ')), CONCAT( new.webpage,' ')),
        IF( new.smart_tags IS NULL OR new.smart_tags ='', IF(old.smart_tags IS NULL OR old.smart_tags='', '', CONCAT(old.smart_tags, ' ')), CONCAT( new.smart_tags,' ')),
        IF( new.product_attributes IS NULL OR new.product_attributes ='', IF(old.product_attributes IS NULL OR old.product_attributes='', '', CONCAT(old.product_attributes, ' ')), CONCAT( new.product_attributes,' ')),
        IF( new.created IS NULL OR new.created ='', IF(old.created IS NULL OR old.created='', '', CONCAT(old.created, ' ')), CONCAT( new.created,' '))
    );
    UPDATE searchleads SET searchtext = addtext WHERE id = old.id;
    -- run the INSERT on the initial update of leads: UPDATE leads SET device_type='' WHERE id > 0
    -- INSERT INTO searchleads (id, searchtext) VALUES (NEW.id, addtext);
END$$


-- ----------------------------------- lead_customfield_content -----------------------------------
DELIMITER $$
CREATE TRIGGER lead_customfield_content_AFTER_INSERT AFTER INSERT ON lead_customfield_content FOR EACH ROW
BEGIN
    DECLARE addtext TEXT;
    DECLARE customfieldname VARCHAR(255);
    SET customfieldname = (SELECT fieldname FROM customfields WHERE id = new.customfields_id);
    SET addtext = CONCAT(
        new.lead_customfield_content,
        ' ',
        CONCAT(customfieldname, ':', NEW.lead_customfield_content, ' ')
    );
    UPDATE searchleads SET searchtext = CONCAT(searchtext, ' ', addtext) WHERE id = new.leads_id;
END$$

-- DROP THIS TRIGGER AFTER FIRST UPDATE as UPDATE lead_customfield_content SET updated = NOW() WHERE id >0;
CREATE TRIGGER lead_customfield_content_AFTER_UPDATE AFTER UPDATE ON lead_customfield_content FOR EACH ROW
BEGIN
    DECLARE addtext TEXT;
    DECLARE customfieldname VARCHAR(255);
    SET customfieldname = (SELECT fieldname FROM customfields WHERE id = new.customfields_id);
    SET addtext = CONCAT(
        new.lead_customfield_content,
        ' ',
        CONCAT(customfieldname, ':', NEW.lead_customfield_content, ' ')
    );
    UPDATE searchleads SET searchtext = CONCAT(searchtext, ' ', addtext) WHERE id = new.leads_id;
END$$


CREATE TRIGGER lead_customfield_content_BEFORE_INSERT BEFORE INSERT ON lead_customfield_content FOR EACH ROW
BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt = NOW();
    SET new.created = jetzt;
    SET new.updated = jetzt;
END$$

CREATE TRIGGER lead_customfield_content_BEFORE_UPDATE BEFORE UPDATE ON lead_customfield_content FOR EACH ROW
BEGIN
    SET new.updated = NOW();
END$$




DELIMITER ;
