create trigger domain_blacklists_after_insert_add_to_domains_search
    after insert
    on domain_blacklists
    for each row
BEGIN
    DECLARE addtext TEXT;
    DECLARE seedpool_name TEXT;
    DECLARE seedpool_group_name TEXT;
    DECLARE domainname TEXT;
    DECLARE sub_domain TEXT;
    DECLARE ip TEXT ;


    SET seedpool_group_name = (SELECT DISTINCT spg.name
                                      FROM seed_pool_group spg
                                      JOIN seed_pool sp on spg.id = sp.seed_pool_group_id
                                      JOIN inbox_monitor im on sp.id = im.seed_pool_id
                                      WHERE im.domain_id = new.domains_id);
    SET ip = (SELECT server_ip_address FROM domains WHERE id = new.domains_id);
    SET seedpool_name = (SELECT `seed_pool` FROM domains WHERE id = new.domains_id);
    SET domainname = (SELECT `domain` FROM domains WHERE id = new.domains_id);
    SET sub_domain = (SELECT `subdomain` FROM domains WHERE id = new.domains_id);


    SET addtext = CONCAT(
        IF( seedpool_name IS NULL OR seedpool_name='', '', CONCAT( seedpool_name, ' ')),
        IF( seedpool_group_name IS NULL OR seedpool_group_name='', '', CONCAT( seedpool_group_name, ' ')),
        IF( ip IS NULL OR ip ='', '', CONCAT( ip, ' ')),
        IF( domainname IS NULL OR domainname='', '', CONCAT( domainname, ' ')),
        IF( domainname IS NULL OR domainname='', '', CONCAT( REGEXP_REPLACE(domainname, '[[.@[-[_]+', ' '), ' ')),
        IF( sub_domain IS NULL OR sub_domain='', '', CONCAT( sub_domain, ' ')),
        IF( sub_domain IS NULL OR sub_domain='', '', CONCAT( REGEXP_REPLACE(sub_domain, '[[.@[-[_]+', ' '), ' ')),
        IF( new.cyren_url_categories IS NULL OR new.cyren_url_categories='', '', CONCAT( new.cyren_url_categories, ' ')),
        IF( new.cyren_url_categories IS NULL OR new.cyren_url_categories='', '', CONCAT( REGEXP_REPLACE(new.cyren_url_categories, '[[,.@[-[_]+', ' '), ' '))
    );

INSERT INTO search_domains (id, searchtext) VALUES (NEW.id, addtext);
END


-- BEGIN
-- SIGNAL SQLSTATE '45000'
--     SET MESSAGE_TEXT = server_ip_address;
-- END$$

