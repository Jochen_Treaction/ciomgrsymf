CREATE TABLE `domains` (
                           `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
                           `objectregister_id` BIGINT(20) UNSIGNED NOT NULL,
                           `company_id` BIGINT(20) UNSIGNED NOT NULL,
                           `domain` VARCHAR(170) NOT NULL,
                           `subdomain` VARCHAR(170) DEFAULT NULL,
                           `received` DATETIME NULL ,
                           `seed_pool` VARCHAR(70) NULL DEFAULT NULL,
                           `server_ip_address` VARCHAR(40) NULL,
                           `created` DATETIME DEFAULT NULL,
                           `updated` DATETIME DEFAULT NULL,
                           `created_by` INT(11) UNSIGNED DEFAULT NULL,
                           `updated_by` INT(11) UNSIGNED DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `unique_seed_pool_domain_name` (`seed_pool`, `domain`),
                           KEY `fk_domains_objectregister_idx` (`objectregister_id`),
                           CONSTRAINT `fk_domains_objectregister` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE CASCADE,
                           KEY `fk_domains_company1_idx` (`company_id` ),
                           CONSTRAINT `fk_domains_company1` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='related to inbox_monitor';



DELIMITER $$
CREATE  TRIGGER `domains_BEFORE_INSERT` BEFORE INSERT ON `domains` FOR EACH ROW BEGIN
declare jetzt datetime;
SET jetzt = now();
set new.created = jetzt;
set new.updated = jetzt;
END$$

CREATE TRIGGER `domains_BEFORE_UPDATE` BEFORE UPDATE ON `domains` FOR EACH ROW BEGIN
set new.updated = now();
END$$

DELIMITER ;
