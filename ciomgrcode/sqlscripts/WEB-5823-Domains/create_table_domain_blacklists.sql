SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';


-- -----------------------------------------------------
-- Table `domain_blacklists`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `domain_blacklists` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `domains_id` BIGINT UNSIGNED NOT NULL,
  `cyren_blacklisted` TINYINT NULL DEFAULT 0,
  `cyren_last_check` DATETIME NULL,
  `cyren_url_categories` TEXT NULL,
  `public_bls_blacklisted` TINYINT NULL DEFAULT 0,
  `public_bls_last_check` DATETIME NULL,
  `public_bls_listed` TEXT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) UNSIGNED DEFAULT NULL,
  `updated_by` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_domain_blacklists_domains1_idx` (`domains_id` ASC),
  CONSTRAINT `fk_domain_blacklists_domains1`
    FOREIGN KEY (`domains_id`)
    REFERENCES `domains` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


DELIMITER $$
CREATE  TRIGGER `domain_blacklists_BEFORE_INSERT` BEFORE INSERT ON `domain_blacklists` FOR EACH ROW BEGIN
    declare jetzt datetime;
SET jetzt = now();
set new.created = jetzt;
set new.updated = jetzt;
END$$

    CREATE TRIGGER `domain_blacklists_BEFORE_UPDATE` BEFORE UPDATE ON `domain_blacklists` FOR EACH ROW BEGIN
        set new.updated = now();
END$$

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
