-- -----------------------------------------------------
-- Alter Table `domains`
-- -----------------------------------------------------

ALTER TABLE `domains`
    ADD COLUMN `received` DATETIME NULL AFTER `subdomain`,
    ADD COLUMN `hosting_ipv4` VARCHAR(128) NULL AFTER `received`,
    ADD COLUMN `hosting_ipv6` VARCHAR(128) NULL AFTER `hosting_ipv4`;


