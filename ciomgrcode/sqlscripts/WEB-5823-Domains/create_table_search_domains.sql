CREATE TABLE `search_domains` (
                                        `id` bigint(20) unsigned NOT NULL,
                                        `searchtext` text COLLATE utf8mb4_general_ci NOT NULL,
                                        PRIMARY KEY (`id`),
                                        FULLTEXT KEY `searchtext` (`searchtext`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='contains search text fragments of domains';