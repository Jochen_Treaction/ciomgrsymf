
SET FOREIGN_KEY_CHECKS = 0;

-- -----------------------------------------------------
-- Table `seed_pool_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `seed_pool_group`;

CREATE TABLE IF NOT EXISTS `seed_pool_group`
(
    `id`         BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`       VARCHAR(170)    NULL,
    `created`    DATETIME        NULL,
    `updated`    DATETIME        NULL,
    `created_by` INT UNSIGNED    NULL,
    `updated_by` INT UNSIGNED    NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),
    UNIQUE INDEX `name_UNIQUE` (`name` ASC)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `seed_pool`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `seed_pool`;

CREATE TABLE IF NOT EXISTS `seed_pool`
(
    `id`                   BIGINT(20) UNSIGNED                                  NOT NULL,
    `seed_pool_group_id`   BIGINT(20) UNSIGNED                                  NULL,
    `company_id`           BIGINT(20) UNSIGNED                                  NOT NULL,
    `objectregister_id`    BIGINT(20) UNSIGNED                                  NOT NULL,
    `mosento_seed_pool_id` VARCHAR(170)                                         NOT NULL,
    `name`                 VARCHAR(255)                                         NULL,
    `type`                 ENUM ('monitoring', 'testing', 'monitoring&testing') NULL DEFAULT 'monitoring',
    `created`              DATETIME                                             NULL,
    `updated`              DATETIME                                             NULL,
    `created_by`           INT UNSIGNED                                         NULL,
    `updated_by`           INT UNSIGNED                                         NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),
    INDEX `fk_seed_pool_company1_idx` (`company_id` ASC),
    INDEX `fk_seed_pool_objectregister1_idx` (`objectregister_id` ASC),
    INDEX `fk_seed_pool_seed_pool_group1_idx` (`seed_pool_group_id` ASC),
    INDEX `fk_mosento_seed_pool_id_idx` (`mosento_seed_pool_id` ASC),
    CONSTRAINT `fk_seed_pool_company1`
        FOREIGN KEY (`company_id`)
            REFERENCES `company` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_seed_pool_objectregister1`
        FOREIGN KEY (`objectregister_id`)
            REFERENCES `objectregister` (`id`)
            ON DELETE RESTRICT
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_seed_pool_seed_pool_group1`
        FOREIGN KEY (`seed_pool_group_id`)
            REFERENCES `seed_pool_group` (`id`)
            ON DELETE RESTRICT
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inbox_monitor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `inbox_monitor`;

CREATE TABLE IF NOT EXISTS `inbox_monitor`
(
    `id`                       BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `seed_pool_id`             BIGINT(20) UNSIGNED NOT NULL,
    `objectregister_id`        BIGINT(20) UNSIGNED NOT NULL,
    `mosento_inbox_monitor_id` VARCHAR(170)        NOT NULL,
    `name`                     VARCHAR(255)        NULL,
    `sender_alias`             VARCHAR(255)        NULL,
    `sender_address`           VARCHAR(255)        NULL,
    `received_emails`          INT                 NULL,
    `inbox_emails`             INT                 NULL,
    `span_emails`              INT                 NULL,
    `spam_reason`              VARCHAR(255)        NULL,
    `created`                  DATETIME            NULL,
    `updated`                  DATETIME            NULL,
    `created_by`               INT UNSIGNED        NULL,
    `updated_by`               INT UNSIGNED        NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),
    INDEX `fk_inbox_monitor_seed_pool1_idx` (`seed_pool_id` ASC),
    INDEX `fk_inbox_monitor_objectregister1_idx` (`objectregister_id` ASC),
    INDEX `fk_mosento_inbox_monitor_id_idx` (`mosento_inbox_monitor_id` ASC),
    CONSTRAINT `fk_inbox_monitor_seed_pool1`
        FOREIGN KEY (`seed_pool_id`)
            REFERENCES `seed_pool` (`id`)
            ON DELETE RESTRICT
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_inbox_monitor_objectregister1`
        FOREIGN KEY (`objectregister_id`)
            REFERENCES `objectregister` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


DELIMITER $$


DROP TRIGGER IF EXISTS `seed_pool_group_BEFORE_INSERT` $$

CREATE TRIGGER `seed_pool_group_BEFORE_INSERT`
    BEFORE INSERT
    ON `seed_pool_group`
    FOR EACH ROW
BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt = NOW();
    set new.created = jetzt;
    set new.updated = jetzt;
END$$



DROP TRIGGER IF EXISTS `seed_pool_group_BEFORE_UPDATE` $$

CREATE TRIGGER `seed_pool_group_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `seed_pool_group`
    FOR EACH ROW
BEGIN
    set new.updated = NOW();
END$$



DROP TRIGGER IF EXISTS `seed_pool_BEFORE_INSERT` $$

CREATE TRIGGER `seed_pool_BEFORE_INSERT`
    BEFORE INSERT
    ON `seed_pool`
    FOR EACH ROW
BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt = NOW();
    set new.created = jetzt;
    set new.updated = jetzt;
END$$



DROP TRIGGER IF EXISTS `seed_pool_BEFORE_UPDATE` $$

CREATE TRIGGER `seed_pool_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `seed_pool`
    FOR EACH ROW
BEGIN
    set new.updated = NOW();
END$$



DROP TRIGGER IF EXISTS `inbox_monitor_BEFORE_INSERT` $$

create trigger inbox_monitor_BEFORE_INSERT
    before insert
    on inbox_monitor
    for each row
BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt = NOW();
    set new.created = jetzt;
    set new.updated = jetzt;
END;
$$



DROP TRIGGER IF EXISTS `inbox_monitor_BEFORE_UPDATE` $$

create trigger inbox_monitor_BEFORE_UPDATE
    before update
    on inbox_monitor
    for each rowd
BEGIN
    set new.updated = NOW();
END;
$$

DELIMITER ;

SET FOREIGN_KEY_CHECKS = 1;

-- -----------------------------------------------------
-- "Use this script for Mariya DB for local use the above one
-- -----------------------------------------------------

SET GLOBAL foreign_key_checks=OFF;



-- -----------------------------------------------------
-- Table `seed_pool_group`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `seed_pool_group`
(
    `id`         BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`       VARCHAR(170)    NULL,
    `created`    DATETIME        NULL,
    `updated`    DATETIME        NULL,
    `created_by` INT UNSIGNED    NULL,
    `updated_by` INT UNSIGNED    NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),
    UNIQUE INDEX `name_UNIQUE` (`name` ASC)
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `seed_pool`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `seed_pool`
(
    `id`                   BIGINT(20) UNSIGNED                                  NOT NULL AUTO_INCREMENT,
    `seed_pool_group_id`   BIGINT(20) UNSIGNED                                  NULL,
    `company_id`           BIGINT(20) UNSIGNED                                  NOT NULL,
    `objectregister_id`    BIGINT(20) UNSIGNED                                  NOT NULL,
    `mosento_seed_pool_id` VARCHAR(170)                                         NOT NULL,
    `name`                 VARCHAR(255)                                         NULL,
    `type`                 ENUM ('monitoring', 'testing', 'monitoring&testing') NULL DEFAULT 'monitoring',
    `created`              DATETIME                                             NULL,
    `updated`              DATETIME                                             NULL,
    `created_by`           INT UNSIGNED                                         NULL,
    `updated_by`           INT UNSIGNED                                         NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),
    INDEX `fk_seed_pool_company1_idx` (`company_id` ASC),
    INDEX `fk_seed_pool_objectregister1_idx` (`objectregister_id` ASC),
    INDEX `fk_seed_pool_seed_pool_group1_idx` (`seed_pool_group_id` ASC),
    INDEX `fk_mosento_seed_pool_id_idx` (`mosento_seed_pool_id` ASC),
    CONSTRAINT `fk_seed_pool_company1`
        FOREIGN KEY (`company_id`)
            REFERENCES `company` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_seed_pool_objectregister1`
        FOREIGN KEY (`objectregister_id`)
            REFERENCES `objectregister` (`id`)
            ON DELETE RESTRICT
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_seed_pool_seed_pool_group1`
        FOREIGN KEY (`seed_pool_group_id`)
            REFERENCES `seed_pool_group` (`id`)
            ON DELETE RESTRICT
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inbox_monitor`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `inbox_monitor`
(
    `id`                       BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `seed_pool_id`             BIGINT(20) UNSIGNED NOT NULL,
    `objectregister_id`        BIGINT(20) UNSIGNED NOT NULL,
    `mosento_inbox_monitor_id` VARCHAR(170)        NOT NULL,
    `name`                     VARCHAR(255)        NULL,
    `sender_alias`             VARCHAR(255)        NULL,
    `sender_address`           VARCHAR(255)        NULL,
    `received_emails`          INT                 NULL,
    `inbox_emails`             INT                 NULL,
    `span_emails`              INT                 NULL,
    `spam_reason`              VARCHAR(255)        NULL,
    `created`                  DATETIME            NULL,
    `updated`                  DATETIME            NULL,
    `created_by`               INT UNSIGNED        NULL,
    `updated_by`               INT UNSIGNED        NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id` ASC),
    INDEX `fk_inbox_monitor_seed_pool1_idx` (`seed_pool_id` ASC),
    INDEX `fk_inbox_monitor_objectregister1_idx` (`objectregister_id` ASC),
    INDEX `fk_mosento_inbox_monitor_id_idx` (`mosento_inbox_monitor_id` ASC),
    CONSTRAINT `fk_inbox_monitor_seed_pool1`
        FOREIGN KEY (`seed_pool_id`)
            REFERENCES `seed_pool` (`id`)
            ON DELETE RESTRICT
            ON UPDATE NO ACTION,
    CONSTRAINT `fk_inbox_monitor_objectregister1`
        FOREIGN KEY (`objectregister_id`)
            REFERENCES `objectregister` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
)
    ENGINE = InnoDB;



DELIMITER $$


DROP TRIGGER IF EXISTS `seed_pool_group_BEFORE_INSERT` $$

CREATE TRIGGER `seed_pool_group_BEFORE_INSERT`
    BEFORE INSERT
    ON `seed_pool_group`
    FOR EACH ROW
BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt = NOW();
    set new.created = jetzt;
    set new.updated = jetzt;
END$$



DROP TRIGGER IF EXISTS `seed_pool_group_BEFORE_UPDATE` $$

CREATE TRIGGER `seed_pool_group_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `seed_pool_group`
    FOR EACH ROW
BEGIN
    set new.updated = NOW();
END$$



DROP TRIGGER IF EXISTS `seed_pool_BEFORE_INSERT` $$

CREATE TRIGGER `seed_pool_BEFORE_INSERT`
    BEFORE INSERT
    ON `seed_pool`
    FOR EACH ROW
BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt = NOW();
    set new.created = jetzt;
    set new.updated = jetzt;
END$$



DROP TRIGGER IF EXISTS `seed_pool_BEFORE_UPDATE` $$

CREATE TRIGGER `seed_pool_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `seed_pool`
    FOR EACH ROW
BEGIN
    set new.updated = NOW();
END$$



DROP TRIGGER IF EXISTS `inbox_monitor_BEFORE_INSERT` $$

create trigger inbox_monitor_BEFORE_INSERT
    before insert
    on inbox_monitor
    for each row
BEGIN
    DECLARE jetzt DATETIME;
    SET jetzt = NOW();
    set new.created = jetzt;
    set new.updated = jetzt;
END;
$$



DROP TRIGGER IF EXISTS `inbox_monitor_BEFORE_UPDATE` $$

create trigger inbox_monitor_BEFORE_UPDATE
    before update
    on inbox_monitor
    for each row
BEGIN
    set new.updated = NOW();
END;
$$

DELIMITER ;

INSERT INTO unique_object_type (assigned_table, unique_object_name, has_versioning, has_encryption, description,
                                    created_at, created_by, updated_at, updated_by)
VALUES ('seed_pool', 'seed_pool', 0, 0, 'Seed pool meta data from mosento', null, 1, null, 1);


SET GLOBAL foreign_key_checks=ON;














