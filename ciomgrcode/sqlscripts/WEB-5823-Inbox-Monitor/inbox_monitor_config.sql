INSERT INTO unique_object_type (assigned_table, unique_object_name, has_versioning, has_encryption, description, created_by, updated_by)
VALUES ('inbox_monitor', 'inbox_monitor_user_config', 0, 0, 'Config for mosento API used for inbox monitor ', 1, 1);

INSERT INTO unique_object_type (assigned_table, unique_object_name, has_versioning, has_encryption, description, created_by, updated_by)
VALUES ('inbox_monitor', 'inbox_monitor', 0, 0, 'Inbox-box monitor object from mosento ', 1, 1);


INSERT INTO unique_object_type (assigned_table, unique_object_name, has_versioning, has_encryption, description, created_by, updated_by)
VALUES ('domains', 'domain', 0, 0, 'Domains for emails ', 1, 1);

ALTER TABLE inbox_monitor RENAME COLUMN spam_emails TO spam_emails;