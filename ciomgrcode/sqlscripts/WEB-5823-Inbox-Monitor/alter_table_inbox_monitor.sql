-- mysql
-- set foreign_key_checks = OFF;
-- mariaDb
set foreign_key_checks = 0;

ALTER TABLE `inbox_monitor`
ADD COLUMN `domain_id` BIGINT UNSIGNED NOT NULL AFTER `id`,
ADD INDEX `fk_inbox_monitor_domain_idx` (`domain_id` ASC);

ALTER TABLE `inbox_monitor`
ADD CONSTRAINT `fk_inbox_monitor_domain`
  FOREIGN KEY (`domain_id`)
  REFERENCES `domains` (`id`)
  ON DELETE CASCADE
  ON UPDATE NO ACTION;
-- mysql
-- set foreign_key_checks = ON;
-- mariaDb
set foreign_key_checks = 1;
