/*Table creation*/
CREATE TABLE `Languages`
(
    `id`         INT UNSIGNED                         NOT NULL AUTO_INCREMENT,
    `name`       VARCHAR(100)                         NOT NULL COMMENT 'Name of the language',
    `iso_code`   VARCHAR(45)                          NOT NULL COMMENT 'ISC_CODE of the language',
    `created`    DATETIME default current_timestamp() NULL COMMENT 'DateTime stamp of language created ',
    `updated`    DATETIME default current_timestamp() NULL COMMENT 'DateTime stamp of language last updated',
    `created_by` INT UNSIGNED                         NULL COMMENT 'User Id by whom the language is created.',
    `updated_by` INT UNSIGNED                         NULL COMMENT 'UserId by whoem the last updated by.',
    PRIMARY KEY (`id`)
)
    COMMENT = 'Languages supported for the localization of the MIO.';

/*Rename table name*/
ALTER TABLE `Languages`
    CHANGE COLUMN `id` `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, RENAME TO `languages`;



INSERT INTO `languages` (`id`, `name`, `iso_code`, `created`, `updated`, `created_by`, `updated_by`)
VALUES (NULL, 'German', 'de', '2022-05-12 18:15:34', '2022-05-12 18:15:34', 1, 1),
       (NULL, 'English', 'en', '2022-05-12 18:15:34', '2022-05-12 18:15:34', 1, 1),
       (NULL, 'French', 'fr', '2022-05-12 18:17:21', '2022-05-12 18:17:21', 1, 1),
       (NULL, 'Polish', 'pl', '2022-05-12 18:17:21', '2022-05-12 18:17:21', 1, 1)