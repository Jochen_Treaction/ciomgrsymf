/*
https://docs.oracle.com/cd/E13214_01/wli/docs92/xref/xqisocodes.html
*/

INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Abkhazian', 'ab');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Afar', 'aa');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Afrikaans', 'af');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Albanian', 'sq');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Amharic', 'am');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Arabic', 'ar');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Armenian', 'hy');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Assamese', 'as');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Aymara', 'ay');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Azerbaijani', 'az');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Bashkir', 'ba');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Basque', 'eu');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Bengali (Bangla)', 'bn');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Bhutani', 'dz');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Bihari', 'bh');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Bislama', 'bi');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Breton', 'br');


INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Bulgarian', 'bg');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Burmese', 'my');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Byelorussian (Belarusian)', 'be');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Cambodian', 'km');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Catalan', 'ca');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Chinese (Simplified)', 'zh');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Corsican', 'co');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Croatian', 'hr');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Czech', 'cs');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Danish', 'da');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Dutch', 'nl');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Esperanto', 'eo');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Estonian', 'et');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Faeroese', 'fo');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Farsi', 'fa');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Fiji', 'fj');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Finnish', 'fi');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Frisian', 'fy');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Galician', 'gl');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Gaelic (Scottish)', 'gd');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Gaelic (Manx)', 'gv');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Georgian', 'ka');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Greek', 'el');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Greenlandic', 'kl');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Guarani', 'gn');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Gujarati', 'gu');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Hausa', 'ha');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Hebrew', 'he');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Hindi', 'hi');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Hungarian', 'hu');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Icelandic', 'is');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Indonesian', 'id');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Interlingua', 'ia');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Interlingue', 'ie');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Inuktitut', 'iu');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Inupiak', 'ik');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Irish', 'ga');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Italian', 'it');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Japanese', 'ja');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Kannada', 'kn');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Kashmiri', 'ks');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Kazakh', 'kk');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Kinyarwanda (Ruanda)', 'rw');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Kirghiz', 'ky');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Kirundi (Rundi)', 'rn');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Korean', 'ko');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Kurdish', 'ku');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Laothian', 'lo');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Latin', 'la');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Latvian (Lettish)', 'lv');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Limburgish ( Limburger)', 'li');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Lingala', 'ln');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Lithuanian', 'lt');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Macedonian', 'mk');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Malagasy', 'mg');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Malay', 'ms');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Malayalam', 'ml');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Maltese', 'mt');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Maori', 'mi');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Marathi', 'mr');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Moldavian', 'mo');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Mongolian', 'mn');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Nauru', 'na');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Nepali', 'ne');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Norwegian', 'no');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Occitan', 'oc');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Oriya', 'or');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Oromo (Afan, Galla)', 'om');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Pashto (Pushto)', 'ps');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Punjabi', 'pa');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Portuguese', 'pt');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Quechua', 'qu');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Rhaeto-Romance', 'rm');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Romanian', 'ro');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Russian', 'ru');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Samoan', 'sm');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Sangro', 'sg');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Sanskrit', 'sa');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Serbian', 'sr');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Serbo-Croatian', 'sh');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Sesotho', 'st');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Setswana', 'tn');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Shona', 'sn');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Sindhi', 'sd');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Sinhalese', 'si');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Siswati', 'ss');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Slovak', 'sk');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Slovenian', 'sl');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Somali', 'so');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Spanish', 'es');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Sundanese', 'su');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Swahili (Kiswahili)', 'sw');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Swedish', 'sv');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Tagalog', 'tl');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Tajik', 'tg');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Tamil', 'ta');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Tatar', 'tt');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Telugu', 'te');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Thai', 'th');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Tibetan', 'bo');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Tigrinya', 'ti');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Tonga', 'to');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Tsonga', 'ts');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Turkish', 'tr');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Turkmen', 'tk');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Twi', 'tw');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Uighur', 'ug');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Ukrainian', 'uk');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Urdu', 'ur');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Uzbek', 'uz');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Vietnamese', 'vi');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Volapük', 'vo');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Welsh', 'cy');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Wolof', 'wo');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Xhosa', 'xh');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Yiddish', 'yi');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Yoruba', 'yo');
INSERT INTO `languages` (`name`, `iso_code`)
VALUES ('Zulu', 'zu');

