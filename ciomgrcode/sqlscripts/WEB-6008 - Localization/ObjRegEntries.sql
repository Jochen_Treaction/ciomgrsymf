/*INSERT INTO Unique Object Type*/
insert into unique_object_type (assigned_table, unique_object_name, has_versioning, has_encryption, description,
                                created_by, updated_by)
values ('language_localization', 'language_localization', 0, 0, 'Language for the localization ', 1, 1);

/*
    When a language is set as default then ObjStatus is set to Active.
    All other languages are set to inactive.
    Only one language for a company is set as active.
*/


INSERT INTO `status_model` (`unique_object_type_id`, `statusdef_id`, `modelname`)
VALUES ((SELECT id FROM `unique_object_type` WHERE unique_object_name = 'language_localization'), '1',
        'language_localization');
INSERT INTO `status_model` (`unique_object_type_id`, `statusdef_id`, `modelname`)
VALUES ((SELECT id FROM `unique_object_type` WHERE unique_object_name = 'language_localization'), '4',
        'language_localization');
