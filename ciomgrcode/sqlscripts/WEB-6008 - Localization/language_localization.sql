DROP TABLE IF EXISTS language_localization;
CREATE TABLE `language_localization`
(
    `id`                INT UNSIGNED                         NOT NULL AUTO_INCREMENT,
    `objectregister_id` INT UNSIGNED                         NOT NULL COMMENT 'ObjectRegiterId of the language.\n- Settings of each language configured for the company is stored in object_reg_meta.\n- Status of the language is determined by the status of the objectRegister_status.',
    `company_id`        INT UNSIGNED                         NOT NULL COMMENT 'CompanyId to which the language is configured.',
    `language_id`       INT UNSIGNED                         NOT NULL COMMENT 'Id of the languages',
    `created`           DATETIME default current_timestamp() NULL COMMENT 'DateTime stamp of language created ',
    `updated`           DATETIME default current_timestamp() NULL COMMENT 'DateTime stamp of language last updated',
    `created_by`        INT UNSIGNED                         NULL COMMENT 'User Id by whom the row is created',
    `updated_by`        INT UNSIGNED                         NULL COMMENT 'UserId by whom the last updated is done',
    PRIMARY KEY (`id`)
)
    COMMENT = 'List of languages supported by different MIO accounts for localization';



ALTER TABLE `language_localization`
    CHANGE COLUMN `objectregister_id` `objectregister_id` BIGINT UNSIGNED NOT NULL COMMENT 'ObjectRegiterId of the language.\\n- Settings of each language configured for the company is stored in object_reg_meta.\\n- Status of the language is determined by the status of the objectRegister_status.',
    CHANGE COLUMN `company_id` `company_id` BIGINT UNSIGNED NOT NULL COMMENT 'CompanyId to which the language is configured.',
    CHANGE COLUMN `language_id` `language_id` BIGINT UNSIGNED NOT NULL COMMENT 'Id of the languages';


alter table language_localization
    add is_default bool null comment 'Only language for the given company is default.
Objectregister status has to be active when the language is set to default.
';

alter table language_localization
    modify created datetime null comment 'DateTime stamp of language created ' after updated_by;

alter table language_localization
    modify is_default tinyint(1) null comment 'Only language for the given company is default.
Objectregister status has to be active when the language is set to default.
' after language_id;


ALTER TABLE `language_localization`
    ADD CONSTRAINT `fk_langlocal_company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE `language_localization`
    ADD CONSTRAINT `fk_langlocal_language` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE `language_localization`
    ADD CONSTRAINT `fk_langlocal_objreg` FOREIGN KEY (`objectregister_id`) REFERENCES `objectregister` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


/*FORIENGN KEYS*/

;
/*ALTER TABLE `language_localization`
    ADD CONSTRAINT `fk_langlocal_objreg`
        FOREIGN KEY (`objectregister_id`)
            REFERENCES `objectregister` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_langlocal_company`
        FOREIGN KEY (`company_id`)
            REFERENCES `campaign` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;



ALTER TABLE `language_localization`
    DROP FOREIGN KEY `fk_langlocal_company`,
    DROP FOREIGN KEY `fk_langlocal_objreg`;*/
ALTER TABLE `language_localization`
    ADD INDEX `fk_langlocal_objreg_idx` (`objectregister_id` ASC),
    ADD INDEX `fk_langlocal_language_idx` (`language_id` ASC),
    ADD INDEX `fk_langlocal_company_idx` (`company_id` ASC);
;



DELIMITER $$
CREATE TRIGGER `language_localization_BEFORE_INSERT`
    BEFORE INSERT
    ON `language_localization`
    FOR EACH ROW
BEGIN
    declare jetzt datetime;
    SET jetzt = now();
    set new.created = jetzt;
    set new.updated = jetzt;
END$$

CREATE TRIGGER `language_localization_BEFORE_UPDATE`
    BEFORE UPDATE
    ON `language_localization`
    FOR EACH ROW
BEGIN
    set new.updated = now();
END$$

DELIMITER ;
