<?php

namespace App\PagingServices;

use App\Types\SelectionKeyOperatorValueTypes;

interface GenericStatementInterface
{

    /**
     * <b>optional:</b><pre>inject Controller Dependency injected Classes, that shall be used within GenericStatement|VIEW|Service</pre>
     * @param array $injectObjects
     */
    public function inject(array $injectObjects):void;

    /**
     * get the limit of records that should be returned after each paging request
     * @return int
     */
    public function getLimit():int;


    /**
     * set the limit of records that should be returned after each paging request
     * @param int $limit
     */
    public function setLimit(int $limit):void;


    /**
     * get the total number of rows that
     * @param SelectionKeyOperatorValueTypes $selections
     * @param array $currentUser
     * @return int
     */
    public function getTotalNumberOfRows(SelectionKeyOperatorValueTypes $selections, array $currentUser):int;


    /**
     * execute selection on some repository => returns repository records
     * @param SelectionKeyOperatorValueTypes $selections
     * @return array
     */
    public function runSelection(SelectionKeyOperatorValueTypes $selections): array;


    /**
     * @return SelectionKeyOperatorValueTypes
     */
    public function getSelection():SelectionKeyOperatorValueTypes;


    /**
     * @param string $tableFieldName
     * @param string $operator
     * @param string $selectionValue
     * @return self
     */
    public function setSelection(string $tableFieldName, string $operator, string $selectionValue):self;
}
