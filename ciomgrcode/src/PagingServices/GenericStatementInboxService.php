<?php

namespace App\PagingServices;

use App\Types\SelectionKeyOperatorValueTypes;
use App\Types\UniqueObjectTypes;
use App\Services\AppCacheService;
use App\Repository\InboxMonitorRepository;

class GenericStatementInboxService implements GenericStatementInterface
{

    private SelectionKeyOperatorValueTypes $selection;
    private int $limit;
    private AppCacheService $apcuCacheService;
    private InboxMonitorRepository $inboxMonitorRepository;

    public function __construct()
    {
        $this->selection = new SelectionKeyOperatorValueTypes();
        $this->limit = 100; // default maximum number of records to return
    }


    /**
     * @inheritDoc
     */
    public function inject(array $injectObjects): void
    {
        $this->apcuCacheService = $injectObjects['$apcuCacheService'];
        $this->inboxMonitorRepository = $injectObjects['$inboxMonitorRepository'];
    }


    /**
     * @inheritDoc
     */
    public function getLimit(): int
    {
        return $this->limit;
    }


    /**
     * @inheritDoc
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }


    /**
     * @inheritDoc
     */
    public function getTotalNumberOfRows(SelectionKeyOperatorValueTypes $selections, array $currentUser): int
    {
        return $this->inboxMonitorRepository->getTotalNumberOfRecordsForView(
            $selections->getSelectionValue('company_id'),
            $selections->getSelectionValue('seed_pool_group_id'),
            $selections->getSelectionValue('seed_pool_name'),
            $selections->getSelectionValue('domain'),
            $selections->getSelectionValue('spam_reason'),
            $selections->getSelectionValue('inboxmonitor_type'),
            $selections->getSelectionValue('inboxmonitor_search_string')
        );
    }


    /**
     * @inheritDoc
     */
    public function runSelection(SelectionKeyOperatorValueTypes $selections): array
    {
        $records = $this->inboxMonitorRepository->getInboxMonitorForView(
            $selections->getSelectionValue('company_id'),
            $selections->getSelectionValue('seed_pool_group_id'),
            $selections->getSelectionValue('seed_pool_name'),
            $selections->getSelectionValue('domain'),
            $selections->getSelectionValue('spam_reason'),
            $selections->getSelectionValue('inboxmonitor_type'),
            $selections->getSelectionValue('inboxmonitor_search_string'),
            $selections->getSelectionValue('inboxmonitor.offset'),
            $this->limit
        );

        if($_ENV['APP_ENV'] === 'dev') {
            try {
                file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . '../../var/log/selectInboxMonitorRecords.csv', print_r($records, true));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [ __METHOD__, __LINE__]);
            }
        }

        if(empty($records)) {
            $msg = "no records found for selection";
            $status = false;
        } else {
            $msg = count($records). " records found for selection";
            $status = true;
        }

        return ['status' => $status, 'message' => $msg, 'records' => $records ];
    }


    public function setSelection(string $tableFieldName, string $operator, string $selectionValue):self
    {
        // just to be save
        if(!($this->selection instanceof SelectionKeyOperatorValueTypes)) {
            $this->selection = new SelectionKeyOperatorValueTypes();
        }
        $this->selection->addKeyOpValue($tableFieldName, $operator, $selectionValue);
        return $this;
    }

    public function getSelection(): SelectionKeyOperatorValueTypes
    {
        return $this->selection;
    }
}
