<?php

namespace App\PagingServices;

use App\PagingServices\GenericStatementInterface;
use App\PagingServices\GenericStatementContactService;
use App\PagingServices\GenericStatementInboxService;
use App\Types\PagingViewType;
use Psr\Log\LoggerInterface;

class GenericStatementFactory
{
    public static function createGenericStatementService(PagingViewType $pagingViewType): ?GenericStatementInterface
    {
        switch (strtolower($pagingViewType->getPagingView())) {
            case 'contacthistory':
            case 'contact' :
                $instance = new GenericStatementContactService();
                break;
            case 'inboxmonitor' :
                $instance = new GenericStatementInboxService();
                break;
            case 'externaldataqueue' :
                $instance = new GenericStatementExternalDataQueueService();
                break;
            case 'domains':
                $instance = new GenericStatementDomainService();
                break;
            default:
                $instance = null;
                break;
        }
        return $instance;
    }
}
