<?php

namespace App\PagingServices;

use App\Repository\ContactRepository;
use App\Repository\CustomfieldsRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Types\ContactDataTableViewTypes;
use App\Types\SelectionKeyOperatorValueTypes;
use App\Types\UniqueObjectTypes;
use App\Services\AppCacheService;

class GenericStatementContactService implements GenericStatementInterface
{
    private SelectionKeyOperatorValueTypes $selection;
    private int $limit;

    private ContactRepository           $contactRepository;
    private CustomfieldsRepository      $customFieldsRepository;
    private MIOStandardFieldRepository  $mioStandardFieldRepository;
    private ContactDataTableViewTypes   $contactDataTableViewTypes;
    private UniqueObjectTypes           $uniqueObjectTypes;
    private AppCacheService            $apcuCacheService;


    public function __construct()
    {
        $this->selection = new SelectionKeyOperatorValueTypes();
        $this->limit = 100; // default maximum number of records to return
    }


    /**
     * expects [ '$contactRepository' => ContactRepository, '$customFieldsRepository' => CustomfieldsRepository, '$mioStandardFieldRepository' => MIOStandardFieldRepository, '$contactDataTableViewTypes' => ContactDataTableViewTypes, '$uniqueObjectTypes' => UniqueObjectTypes]
     * @param array $injectObjects
     */
    public function inject( array $injectObjects ):void
    {
        /**
         * This objects are necessary to use Pradeep initial "getContactOverview" - Algorithms => see LeadsController::getContactOverview (if not removed in the meanwhile)
         */
        $this->contactRepository = $injectObjects['$contactRepository'];
        $this->customFieldsRepository = $injectObjects['$customFieldsRepository'];
        $this->mioStandardFieldRepository = $injectObjects['$mioStandardFieldRepository'];
        $this->contactDataTableViewTypes = $injectObjects['$contactDataTableViewTypes'];
        $this->uniqueObjectTypes = $injectObjects['$uniqueObjectTypes'];
        $this->apcuCacheService = $injectObjects['$apcuCacheService'];
    }


    /**
     * @inheritDoc
     */
    public function getLimit(): int
    {
        return $this->limit;
    }


    /**
     * @inheritDoc
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;

    }


    /**
     * uses cache for all subsequent call on same selection
     * @inheritDoc
     */
    public function getTotalNumberOfRows(SelectionKeyOperatorValueTypes $selections, array $currentUser): int
    {
        $cacheKeyPrefix = hash('md5',$currentUser['user_id'].':'. $currentUser['company_id'].':'.serialize($selections));

        if($selections->getSelectionValue('useRepositoryMethod'  ) === 'getPagingLeadsForContactOverview' ) {
            if (!$this->apcuCacheService->entryExists($cacheKeyPrefix . ':TotalNumberOfRows')) {
                $count = $this->contactRepository->getCountPagingLeadsForContactOverview(
                    (int)$selections->getSelectionValue('leads.account'),
                    $selections->getSelectionValue('searchleads.searchtext'),
                    $selections->getSelectionValue('leads.created_from'),
                    $selections->getSelectionValue('leads.created_to'),
                    $selections->getSelectionValue('objectregister.statusdef_value')
                );
                $this->apcuCacheService->addOrOverwrite($cacheKeyPrefix . ':TotalNumberOfRows', $count, AppCacheService::SEVEN_MINUTES);
            } else {
                $count = $this->apcuCacheService->get($cacheKeyPrefix . ':TotalNumberOfRows');
            }
        } else { // $selections->getSelectionValue('useRepositoryMethod'  ) === 'getPagingLeadsForContactHistory'
            if (!$this->apcuCacheService->entryExists($cacheKeyPrefix . ':TotalNumberOfRowsHistory')) {
                $count = $this->contactRepository->getCountPagingLeadsForContactHistory(
                    (int)$selections->getSelectionValue('leads.account'),
                    $selections->getSelectionValue('searchleads.searchtext'),
                    $selections->getSelectionValue('leads.created_from'),
                    $selections->getSelectionValue('leads.created_to'),
                    $selections->getSelectionValue('objectregister.statusdef_value')
                );
                $this->apcuCacheService->addOrOverwrite($cacheKeyPrefix . ':TotalNumberOfRowsHistory', $count, AppCacheService::SEVEN_MINUTES);
            } else {
                $count = $this->apcuCacheService->get($cacheKeyPrefix . ':TotalNumberOfRowsHistory');
            }

        }
        return $count;
    }


    /**
     * returns <pre>['status' => true, 'message' => 'some message', 'minId' => $leads['minId'], 'maxId' => $leads['maxId'], 'c_leads' => $c_leads ]</pre>
     * @inheritDoc
     */
    public function runSelection(SelectionKeyOperatorValueTypes $selections): array
    {
        $minLeadId = 0;
        $maxLeadId = 0;

        $companyId = $selections->getSelectionValue('leads.account');

        if($selections->getSelectionValue('useRepositoryMethod'  ) === 'getPagingLeadsForContactOverview' ) {
            $leads = $this->contactRepository->getPagingLeadsForContactOverview(
                $companyId,
                $selections->getSelectionValue('searchleads.searchtext'),
                $selections->getSelectionValue('leads.created_from'),
                $selections->getSelectionValue('leads.created_to'),
                $selections->getSelectionValue('objectregister.statusdef_value'),
                $selections->getSelectionValue('leads.id'),
                $selections->getOperator('leads.id'),
                $this->limit
            );
        } else {  // $selections->getSelectionValue('useRepositoryMethod'  ) === 'getPagingLeadsForContactHistory'
            $leads = $this->contactRepository->getPagingLeadsForContactHistory(
                $companyId,
                $selections->getSelectionValue('searchleads.searchtext'),
                $selections->getSelectionValue('leads.created_from'),
                $selections->getSelectionValue('leads.created_to'),
                $selections->getSelectionValue('objectregister.statusdef_value'),
                $selections->getSelectionValue('leads.id'),
                $selections->getOperator('leads.id'),
                $this->limit
            );
        }

        // TODO: check if param  $contacts is set correctly according to LeadsController::getContactOverview
        // Get all supported custom fields for the company.
        $customFields = $this->customFieldsRepository->getProjectCustomfields($companyId, UniqueObjectTypes::CUSTOMFIELDS);
        // Removes the duplicate EMail and appends the custom fields.
        if(!isset($leads['contacts']) || empty($leads['contacts'])) {
            return ['status' => false, 'message' => 'no records found for selection', 'minId' => 0, 'maxId' => 0, 'c_leads' => [] ];
        }

        $contacts = $this->contactRepository->appendContactWithCustomFields($leads['contacts'], $customFields);
        // fetches the supported contact status
        $response[ 'c_status' ] = $this->contactRepository->getContactStatus();
        // assign Placeholder for the contacts to show in UserInterface.
        $contacts = $this->mioStandardFieldRepository->assignPlaceHoldersForLeadTwigView($contacts);
        // reorder all the fields as per the requirement to show in the UserInterface.
        $c_leads = $this->contactDataTableViewTypes->reOrderFieldsForDataTableView($contacts, $customFields);

        return ['status' => true, 'message' => 'some message', 'minId' => $leads['minId'], 'maxId' => $leads['maxId'], 'c_leads' => $c_leads ];

    }


    /**
     * @inheritDoc
     */
    public function getSelection(): SelectionKeyOperatorValueTypes
    {
        return $this->selection;
    }


    /**
     * @inheritDoc
     */
    public function setSelection(string $tableFieldName, string $operator, string $selectionValue):self
    {
        // just to be save
        if(!($this->selection instanceof SelectionKeyOperatorValueTypes)) {
            $this->selection = new SelectionKeyOperatorValueTypes();
        }
        $this->selection->addKeyOpValue($tableFieldName, $operator, $selectionValue);
        return $this;
    }


}
