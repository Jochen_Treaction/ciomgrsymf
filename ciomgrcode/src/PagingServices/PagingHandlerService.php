<?php

namespace App\PagingServices;

use App\Services\AppCacheService;
use App\Types\SelectionKeyOperatorValueTypes;
use Psr\Log\LoggerInterface;

/**
 * caches or keeps in mind the current paging status
 */
class PagingHandlerService
{
    /**
     * @const INITIAL_MAXID=999999999 ==> 1G-1
     */
    const INITIAL_MAXID = 999999999;
    const PAGE_ONE = 1;
    const GT = '>';
    const LT = '<';

    /**
     * @var AppCacheService
     */
    private AppCacheService $cacheService;


    private int $lastPageIndex;
    private int $currentPageIndex;
    private int $minId;
    private int $maxId;
    private LoggerInterface $logger;


    public function __construct(AppCacheService $cacheService,LoggerInterface $logger)
    {
        $this->cacheService = $cacheService;
        $this->lastPageIndex = 1;
        $this->currentPageIndex = 1;
        $this->minId = 0;
        $this->maxId = 0;
        $this->logger = $logger;
    }


    /**
     * @param array                          $currentUser
     * @param array                          $postBody
     * @param SelectionKeyOperatorValueTypes $keyPartBaseSelection
     * @return array <pre>[ 'operator' => string {'&lt', '&gt'},  'id' => int ];</pre>
     */
    public function managePaging(array $currentUser, array $postBody, SelectionKeyOperatorValueTypes $keyPartBaseSelection):array
    {
        $indexSelectionId = 0;
        $indexSelectionOperator = ''; //  { '>', '<' } ==> see const

        $this->currentPageIndex = $postBody['page_index'];
        $cacheKeyPrefix = hash('md5',$currentUser['user_id'].':'. $currentUser['company_id'].':'.serialize($keyPartBaseSelection));

        //if no entry in cache is available or change of page number
        if( !$this->cacheService->entryExists($cacheKeyPrefix) || self::PAGE_ONE === $this->currentPageIndex ) {
            $d = date('Y-m-d H:i:s');
            $this->cacheService->addOrOverwrite($cacheKeyPrefix, "cache $cacheKeyPrefix active from $d for ".AppCacheService::THIRTY_MINUTES.'m', AppCacheService::THIRTY_MINUTES);
            $this->cacheService->addOrOverwrite($cacheKeyPrefix.':lastPageIndex', $this->currentPageIndex, AppCacheService::THIRTY_MINUTES);
            $this->lastPageIndex = $this->currentPageIndex;
            $this->minId = 0;
            $this->maxId = self::INITIAL_MAXID; // 1G - 1
            $this->setIds($this->minId, $this->maxId, $currentUser, $keyPartBaseSelection);
        } else {
            $this->lastPageIndex = $this->cacheService->get($cacheKeyPrefix.':lastPageIndex');
            $this->minId = $this->cacheService->get($cacheKeyPrefix.':minId');
            $this->maxId = $this->cacheService->get($cacheKeyPrefix.':maxId');
            $this->cacheService->addOrOverwrite($cacheKeyPrefix.':lastPageIndex', $this->currentPageIndex, AppCacheService::THIRTY_MINUTES);
        }

        if(self::PAGE_ONE === $this->currentPageIndex) { // usually at the first call
            return [ 'operator' => self::GT,  'id' => $this->minId];
        }
        $x = [
            'min' => $this->minId,
            'max' => $this->maxId,
            'previousPage' =>  $this->lastPageIndex,
            'currentPage' => $this->currentPageIndex
        ];

        $this->logger->info('$Paging$', [$x,__METHOD__, __LINE__]);

        if($this->currentPageIndex > $this->lastPageIndex) { //  calls for next page
            return [ 'operator' => self::LT,  'id' => $this->minId];
        }

        return [ 'operator' => self::GT,  'id' => $this->maxId]; //Going to first page selection

        //todo:calls for random pages
    }


    /**
     * set min and max id (primary key) of the main tables (e.g. leads) record result set based on the current selection
     * @param int $minId
     * @param int $maxId
     * @param array $currentUser
     * @param SelectionKeyOperatorValueTypes $keyPartBaseSelection
     */
    public function setIds(int $minId, int $maxId, array $currentUser, SelectionKeyOperatorValueTypes $keyPartBaseSelection)
    {
        $cacheKeyPrefix = $cacheKeyPrefix = hash('md5',$currentUser['user_id'].':'. $currentUser['company_id'].':'.serialize($keyPartBaseSelection));

        $this->cacheService->addOrOverwrite($cacheKeyPrefix.':minId', $minId);
        $this->cacheService->addOrOverwrite($cacheKeyPrefix.':maxId', $maxId);
    }


    /**
     * @param int $recordsPerPage
     * @param int $pageNumber
     * @return int
     * @internal returns the offset value for the page - used in sql for paging
     */
    public function managePagingOffSetValueForPaginationBar(int $recordsPerPage, int $pageNumber):int
    {
        return (($pageNumber*$recordsPerPage)-$recordsPerPage);
    }

}
