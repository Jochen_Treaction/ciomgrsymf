<?php

namespace App\PagingServices;

use App\Types\SelectionKeyOperatorValueTypes;
use App\Types\UniqueObjectTypes;
use App\Services\AppCacheService;
use App\Repository\ExternalDataQueueRepository;

class GenericStatementExternalDataQueueService implements GenericStatementInterface
{
    private SelectionKeyOperatorValueTypes $selection;
    private int $limit;

    private AppCacheService $apcuCacheService;
    private ExternalDataQueueRepository $externalDataQueueRepository;


    public function __construct()
    {
        $this->selection = new SelectionKeyOperatorValueTypes();
        $this->limit = 100; // default maximum number of records to return
    }


    /**
     * @inheritDoc
     */
    public function inject(array $injectObjects): void
    {
        $this->apcuCacheService = $injectObjects['$apcuCacheService'];
        $this->externalDataQueueRepository = $injectObjects['$externalDataQueueRepository'];
    }


    /**
     * @inheritDoc
     */
    public function getLimit(): int
    {
        return $this->limit;
    }


    /**
     * @inheritDoc
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }


    /**
     * @inheritDoc
     */
    public function getTotalNumberOfRows(SelectionKeyOperatorValueTypes $selections, array $currentUser): int
    {
        $cacheKeyPrefix = hash('sha256',$currentUser['user_id'].':'. $currentUser['company_id'].':'.serialize($selections));

        if (!$this->apcuCacheService->entryExists($cacheKeyPrefix . ':TotalNumberOfRows')) {
            $count = $this->externalDataQueueRepository->getCountPagingForExternalDataQueueView(
                $selections->getSelectionValue('externaldataqueue.companyId'),
                $selections->getSelectionValue('objectregister.webhookname'),
                $selections->getSelectionValue('externaldataqueue.status')
            );

            $this->apcuCacheService->addOrOverwrite($cacheKeyPrefix . ':TotalNumberOfRows', $count, AppCacheService::THIRTY_SEC);
        } else {
            $count = $this->apcuCacheService->get($cacheKeyPrefix . ':TotalNumberOfRows');
        }
        return $count;
    }


    public function runSelection(SelectionKeyOperatorValueTypes $selections):array
    {
        $records = $this->externalDataQueueRepository->getPagingExternalDataQueueView(
            $selections->getSelectionValue('externaldataqueue.companyId'),
            $selections->getSelectionValue('externaldataqueue.status'),
            $selections->getSelectionValue('objectregister.webhookname'),
            $selections->getSelectionValue('externaldataqueue.offset'),
            $this->limit
        );

        if(empty($records['records'])) {
            $msg = "no records found for selection";
            $status = false;
        } else {
            $msg = count($records['records']). " records found for selection";
            $status = true;
        }

        return ['status' => $status, 'message' => $msg, 'minId' => $records['minId'], 'maxId' => $records['maxId'], 'records' => $records['records'] ];
    }



    /**
     * @inheritDoc
     */
    public function getSelection(): SelectionKeyOperatorValueTypes
    {
        return $this->selection;
    }


    /**
     * @inheritDoc
     */
    public function setSelection(string $tableFieldName, string $operator, string $selectionValue):self
    {
        // just to be save
        if(!($this->selection instanceof SelectionKeyOperatorValueTypes)) {
            $this->selection = new SelectionKeyOperatorValueTypes();
        }
        $this->selection->addKeyOpValue($tableFieldName, $operator, $selectionValue);
        return $this;
    }
}
