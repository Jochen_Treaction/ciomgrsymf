<?php

namespace App\Security;

use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use GeoIp2\WebService\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use App\Repository\UsersRepository;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class AppCioAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private $entityManager;
    private $urlGenerator;
    private $csrfTokenManager;
    private $passwordEncoder;
    private $logger;
    private $usersRepository;


	/**
	 * AppCioAuthenticator constructor.
	 * @param UsersRepository              $usersRepository
	 * @param EntityManagerInterface       $entityManager
	 * @param UrlGeneratorInterface        $urlGenerator
	 * @param CsrfTokenManagerInterface    $csrfTokenManager
	 * @param UserPasswordEncoderInterface $passwordEncoder
	 * @param LoggerInterface              $logger
	 */
    public function __construct(
        UsersRepository $usersRepository,
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
        CsrfTokenManagerInterface $csrfTokenManager,
        UserPasswordEncoderInterface $passwordEncoder,
        LoggerInterface $logger
    ) {
        $this->usersRepository = $usersRepository;
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->logger = $logger;
        $this->geoIpClient = new Client(346418, 'NrHslBO4wNzcA53C');
    }


	/**
	 * @param Request $request
	 * @return bool
	 */
    public function supports(Request $request)
    {
        return 'auth.signin' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }


	/**
	 * @param Request $request
	 * @return array
	 */
    public function getCredentials(Request $request)
    {

        $this->logger->info('AppCioAuthenticator EMAIL = ' . $request->request->get('email'), [__METHOD__, __LINE__]);
        // $this->logger->info('PW = ' . $request->request->get('password'), [__METHOD__, __LINE__]);

        $credentials = [
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
            'ip'=>$request->getClientIp()
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

         $this->logger->info('$credentials = ' . json_encode($credentials), [__METHOD__, __LINE__]);

        return $credentials;
    }


	/**
	 * @param mixed                 $credentials
	 * @param UserProviderInterface $userProvider
	 * @return object|UserInterface|null
	 */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        // COMMENT: TODO: login stops here sometimes ... solve that
        $this->logger->info('AppCioAuthenticator getUser', [__METHOD__, __LINE__]);
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        $user = $this->entityManager->getRepository(Users::class)->findOneBy(['email' => $credentials['email']]);
        // apcu_store('user', $user, 3600);
        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('your login was rejected. check for typos.');
        }

        return $user;
    }


	/**
	 * @param mixed         $credentials
	 * @param UserInterface $user
	 * @return bool
	 * @throws \Doctrine\Common\Persistence\Mapping\MappingException
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $this->logger->info('AppCioAuthenticator checkCredentials', [__METHOD__, __LINE__]);
        $ret = true;
        $setSecureLogEntry = false;
        $this->logger->info('USER = ' . json_encode($user), [__METHOD__, __LINE__]);
        $this->logger->info('CREDENTIALS = ' . json_encode($credentials), [__METHOD__, __LINE__]);

        $userArray = $this->usersRepository->getUserAndCompanyForApcu($credentials[ 'email' ]);
        $isUserActive = $this->usersRepository->isUserActive($credentials[ 'email' ]);
        $failedLogins = $this->usersRepository->getFailedLogins($credentials[ 'email' ]);

        $getUserGeoLocationStatus = $this->usersRepository->ValidateUserGeoLocation($credentials[ 'ip' ]);
        $this->logger->info('FAILED LOGINS = ', ['failedLogins' => $failedLogins], [__METHOD__, __LINE__]);

        $isPasswordValid = $this->passwordEncoder->isPasswordValid($user, $credentials[ 'password' ]);

        $this->logger->info('IS PW VALID = ' . json_encode(['$isPasswordValid' => $isPasswordValid]),
            [__METHOD__, __LINE__]);

        if (!$getUserGeoLocationStatus) {
            $this->usersRepository->setSecureLogEntry(array_merge(['ip' => $credentials[ 'ip' ], 'result' => 'failure'],
                $userArray));
            $setSecureLogEntry = true;
        }

        if (!$isPasswordValid) {
            $this->usersRepository->incrementFailedLogins($credentials['email']);

            $this->usersRepository->setSecureLogEntry( array_merge(['ip' => '', 'result' => 'failure'],$userArray));
            $setSecureLogEntry = true;
            $failedLogins++;
            $this->logger->warning('INVALID PASSWORD FROM USER ' . base64_encode($credentials['email']));
        }

        if ($failedLogins >= 3) {
            $this->logger->warning('THREE OR MORE LOGIN ATEMPTS BY USER. BLOCKING USER ' . base64_encode($credentials['email']));
            $this->usersRepository->blockUser($credentials['email']);
            if (!$setSecureLogEntry) {
                $this->usersRepository->setSecureLogEntry(array_merge(['ip' => '', 'result' => 'failure'], $userArray));
                $setSecureLogEntry = true;
            }
            $isUserActive = false;
        }

        if (!$isUserActive) {
            $this->logger->warning('USER INACTIVE CANNOT LOGIN' . base64_encode($credentials['email']));
            if (!$setSecureLogEntry) {
                $this->usersRepository->setSecureLogEntry(array_merge(['ip' => '', 'result' => 'failure'], $userArray));
            }
        }

        if (!$getUserGeoLocationStatus || !$isUserActive || !$isPasswordValid || $failedLogins >= 3) {
            $ret = false;
        }

        return $ret;
    }


	/**
	 * @param Request        $request
	 * @param TokenInterface $token
	 * @param string         $providerKey
	 * @return null
	 */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $this->logger->info('AppCioAuthenticator onAuthenticationSuccess', [__METHOD__, __LINE__]);
        return null;

//        $this->logger->info(
//            'ZZZ = ' . json_encode(['token' => $token, 'providerkey' => $providerKey]),
//            [__METHOD__, __LINE__]
//        );
//
//        if ($targetPath = $this->getTargetPath($request->getSession(), $providerKey)) {
//            return $this->urlGenerator->generate('auth.home'); // dashboard
//        }
//        // $token->setUser($request->get(''));
//
//        return new RedirectResponse($this->urlGenerator->generate('auth.home'), Response::HTTP_FOUND, []);

    }


	/**
	 * @return string
	 */
    protected function getLoginUrl()
    {
        $this->logger->info('AppCioAuthenticator checkCredentials', [__METHOD__, __LINE__]);
        return $this->urlGenerator->generate('auth.signin');
    }
}
