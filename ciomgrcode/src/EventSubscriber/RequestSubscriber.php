<?php


namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface as EventSubscriberInterfaceAlias;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\KernelEvent;

class RequestSubscriber implements EventSubscriberInterfaceAlias, LoggerAwareInterface
{

    private $logger;

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * ['eventName' => 'methodName']
     *  * ['eventName' => ['methodName', $priority]]
     *  * ['eventName' => [['methodName1', $priority], ['methodName2']]]
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        // listen on kennel.request
        return [
            KernelEvents::REQUEST => [
                ['processRequest', 10],
                // add more of your methods here (see processRequest) and define their priority (e.g. 10 for processRequest)
            ],
        ];
    }

    public function processRequest(KernelEvent $events)
    {
        // apcu_fetch('user')
        // your code goes here
        // this example simply logs the caller IP to var/log/dev.log
        $req = $events->getRequest();
        $ip = $req->getClientIp();
        // $this->logger->info("THE CALLER-IP WAS $ip", [__METHOD__, __LINE__]);

        // todo: may be add some code here to do something with every incomming request e.g. user request tracking

    }

    /**
     * Sets a logger instance on the object.
     *
     * @param LoggerInterface $logger
     *
     * @return void
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}
