<?php

namespace App\Services;

use App\Repository\CompanyRepository;
use App\Repository\LanguageLocalizationRepository;
use App\Repository\LanguageRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\StatusDefRepository;
use App\Repository\UsersRepository;
use App\Types\LocalizationLanguageWizardTypes;
use App\Types\StatusdefTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Driver\Exception;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class LanguageOptInTextServices
{

    /**
     * @var LanguageLocalizationRepository
     * @author Pradeep
     */
    private LanguageLocalizationRepository $languageLocalizationRepository;
    /**
     * @var LanguageRepository
     * @author Pradeep
     */
    private LanguageRepository $languageRepository;
    /**
     * @var LocalizationLanguageWizardTypes
     * @author Pradeep
     */
    private LocalizationLanguageWizardTypes $localizationLanguageWizardTypes;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private ObjectRegisterRepository $objectRegisterRepository;
    /**
     * @var ObjectRegisterMetaRepository
     * @author Pradeep
     */
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $connection;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(
        LanguageLocalizationRepository $languageLocalizationRepository,
        LocalizationLanguageWizardTypes $localizationLanguageWizardTypes,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        LanguageRepository $languageRepository,
        CompanyRepository $companyRepository,
        UsersRepository $usersRepository,
        Connection $connection,
        LoggerInterface $logger
    ) {
        $this->languageLocalizationRepository = $languageLocalizationRepository;
        $this->localizationLanguageWizardTypes = $localizationLanguageWizardTypes;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->languageRepository = $languageRepository;
        $this->companyRepository = $companyRepository;
        $this->usersRepository = $usersRepository;
        $this->connection = $connection;
        $this->logger = $logger;
    }

    /**
     * addLanguage -> adds new language from the list of available languages.
     * @param array $localLangValue
     * @param int $userId
     * @param int $companyId
     * @return bool
     * @throws ConnectionException
     * @author Pradeep
     */
    public function addLanguage(array $localLangValue, int $userId, int $companyId): bool
    {
        try {
            $this->connection->beginTransaction();
            $this->logger->info('addLang - LocallangValue : ' . json_Encode($localLangValue));
            // fetch the language details.
            $language = $this->languageRepository->get((int)$localLangValue[ 'lang_id' ]);
            if (empty($language) || !isset($language[ 'id' ], $language[ 'name' ])) {
                throw new \RuntimeException('invalid language_id or language_name provided');
            }

            // create object register for the language
            $objRegId = $this->objectRegisterRepository->createInitialObjectRegisterEntry($companyId,
                UniqueObjectTypes::LANGUAGE_LOCALIZATION);
            if ($objRegId === null || $objRegId <= 0) {
                throw new \RuntimeException('Failed to create Object Register for language localization');
            }
            $isLangMarkedDefault = $this->localizationLanguageWizardTypes->isLangDefault($localLangValue);

            // insert language to localization table.
            $langLocalId = $this->languageLocalizationRepository->insert(
                $language[ 'id' ], $objRegId, $companyId, $userId, $isLangMarkedDefault
            );

            if (!$isLangMarkedDefault) {
                $this->updateLanguageStatus($langLocalId, $companyId, $isLangMarkedDefault);
            } else {
                $this->makeLanguageAsDefault($langLocalId, $companyId);
            }

            if (!$isLangMarkedDefault && !$this->updateLanguageStatus($langLocalId, $companyId, false)) {
                throw new \RuntimeException('failed to make initial language as not default language');
            }
            $this->logger->info('insert languageLocRepo : ' . json_Encode($langLocalId));
            if ($langLocalId === null || $langLocalId <= 0) {
                throw new \RuntimeException('Failed to create Object Register for language localization');
            }

            // insert the json to object_register_meta.
            $json = $this->localizationLanguageWizardTypes->buildJsonBody(
                $userId, $companyId, $objRegId, $langLocalId, $language, $localLangValue
            );
            $this->logger->info('payload ObjRegMeta : ' . json_Encode($json));
            $objRegMetaId = $this->objectRegisterMetaRepository->insert(
                $objRegId, $this->localizationLanguageWizardTypes::OBJ_REG_META_KEY, $json);
            $this->logger->info('insert ObjRegMeta : ' . json_Encode($objRegMetaId));
            if ($objRegMetaId === null || $objRegMetaId <= 0) {
                throw new \RuntimeException('Failed to insert to objectRegisterMeta ' . $json);
            }

            $this->connection->commit();

            return true;
        } catch (\RuntimeException|\Exception|ConnectionException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $this->connection->rollBack();
            return false;
        }
    }

    /**
     * updateLanguageStatus - helper function to update the status of the
     * objectregister to active or inactive.
     * @param int $id
     * @param int $companyId
     * @param bool $isDefault
     * @return bool
     * @author Pradeep
     */
    public function updateLanguageStatus(int $id, int $companyId, bool $isDefault = true): ?bool
    {
        try {
            if ($id <= 0 || $companyId <= 0) {
                throw new \RuntimeException("invalid Language Id or company Id provided");
            }
            $details = $this->languageLocalizationRepository->get($id);
            if (empty($details) || !isset($details[ 'id' ], $details[ 'objectregister_id' ])) {
                throw new \RuntimeException('invalid langLocalization details provided');
            }
            $objRegStatus = $isDefault ? StatusDefRepository::ACTIVE : StatusDefRepository::INACTIVE;
            if (is_null($this->objectRegisterRepository->updateStatus($details[ 'objectregister_id' ],
                $objRegStatus))) {
                throw new \RuntimeException('failed to update object register status to active');
            }
            return $this->languageLocalizationRepository->update(
                $details[ 'id' ], $isDefault);
            //return true;
        } catch (\RuntimeException|\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, $e->getCode()]);
            return false;
        }
    }

    /**
     * makeLanguageAsDefault - update the language as default for the company.
     * - language can be made default.
     * - objectregister status will also be changed to active or inactive.
     * - Only one language for the company can be set to default.
     * - When a new language is marked as default, then previously default language should be set to false.
     * - Accordingly update the objectregister status to 'inactive'
     * @param int $id
     * @param int $companyId
     * @return bool|null
     * @throws ConnectionException
     * @author Pradeep
     */
    public function makeLanguageAsDefault(int $id, int $companyId): ?bool
    {
        $status = true;
        try {
            $this->connection->beginTransaction();
            // fetch old default language details
            $oldDefaultLang = $this->languageLocalizationRepository->getDefaultLanguage($companyId);
            if (!empty($oldDefaultLang) && isset($oldDefaultLang[ 'id' ])) {
                // make the old default language as inactive.
                if (!$this->updateLanguageStatus($oldDefaultLang[ 'id' ], $companyId, false)) {
                    throw new \RuntimeException('Failed to remove old default language');
                }
            }
            // make the new language a default.
            if (!$this->updateLanguageStatus($id, $companyId)) {
                throw new \RuntimeException('Failed to updated new language as default');
            }
            $this->connection->commit();
        } catch (\RuntimeException|Exception|ConnectionException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $this->connection->rollBack();
            $status = false;
        }
        return $status;
    }

    /**
     * removeLanguage deletes the language completely for the company.
     * @param int $langId
     * @return bool
     * @throws ConnectionException
     * @author Pradeep
     */
    public function removeLanguage(int $langId): bool
    {
        $details = $this->languageLocalizationRepository->get($langId);
        try {
            if (empty($details) || !isset($details[ 'id' ], $details[ 'objectregister_id' ])) {
                throw new \RuntimeException('invalid langLocalization details provided');
            }
            $this->connection->beginTransaction();// delete localization Lang row.
            if (!$this->languageLocalizationRepository->delete($details[ 'id' ]) ||
                !$this->objectRegisterMetaRepository->delete($details[ 'objectregister_id' ]) ||
                !$this->objectRegisterRepository->delete($details[ 'objectregister_id' ])) {
                throw new \RuntimeException('Failed to delete language');
            }
            $this->connection->commit();
            return true;
        } catch (\RuntimeException|ConnectionException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            $this->connection->rollBack();
            return false;
        }
    }

    /**
     * modify updates the language settings/ optin Text for newsletter or ecommerce text
     * for the specified language
     * @param array $localLangValue
     * @param int $userId
     * @param int $companyId
     * @return bool
     * @throws ConnectionException
     * @author Pradeep
     */
    public function modify(array $localLangValue, int $userId, int $companyId): bool
    {
        try {
            $this->connection->beginTransaction();
            $language = $this->languageRepository->get((int)$localLangValue[ 'lang_id' ]);
            if (empty($language) || !isset($language[ 'id' ], $language[ 'name' ])) {
                throw new \RuntimeException('invalid language_id or language_name provided');
            }
            $objRegId = $localLangValue[ 'objectregister_id' ];
            $langLocalId = $localLangValue[ 'local_lang_id' ];
            $json = $this->localizationLanguageWizardTypes->buildJsonBody($userId, $companyId, $objRegId, $langLocalId,
                $language, $localLangValue);

            $isLangMarkedDefault = $this->localizationLanguageWizardTypes->isLangDefault($localLangValue);

            if (!$isLangMarkedDefault) {
                $this->updateLanguageStatus($langLocalId, $companyId, $isLangMarkedDefault);
            } else {
                $this->makeLanguageAsDefault($langLocalId, $companyId);
            }

            if (!$this->objectRegisterMetaRepository->updateSettings($objRegId, $json,
                $this->localizationLanguageWizardTypes::OBJ_REG_META_KEY)) {
                throw new \RuntimeException('failed to update language settings');
            }
            $this->connection->commit();
            return true;
        } catch (Exception|\Doctrine\DBAL\Exception|ConnectionException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $this->connection->rollback();
            return false;
        }
    }

    /**
     * getWizardSettings - gets the settings or values from the object_register_meta
     * to show in wizard.
     * @param int $id
     * @return array|null
     * @author Pradeep
     */
    public function getWizardSettings(int $id): ?array
    {
        try {
            $locLangDetails = $this->languageLocalizationRepository->get($id);
            if (empty($locLangDetails) || !isset($locLangDetails[ 'id' ], $locLangDetails[ 'objectregister_id' ])) {
                throw new \RuntimeException('invalid langLocalization details provided');
            }
            $details = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails(
                $locLangDetails[ 'objectregister_id' ],
                $this->localizationLanguageWizardTypes::OBJ_REG_META_KEY
            );

            if (empty($details) || !isset($details[ 'id' ], $details[ 'meta_value' ])) {
                throw new \RuntimeException('failed to fetch object register meta value for lang settings');
            }
            if (empty($details[ 'meta_value' ]) || !is_string($details[ 'meta_value' ])) {
                throw new \RuntimeException('invalid wizard settings for loclanguage settings.');
                //  $details = json_decode($details[ 'meta_value' ], false, 512, JSON_THROW_ON_ERROR);
            }
            return json_decode($details[ 'meta_value' ], true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException|\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }


    /**
     * - setDefaultLangForCreatingAccount for settings a default language (currently 'German')
     *  immediately after creating a new account.
     * - Default json is loaded from 'LocalizationLanguageWizardTypes'.
     * - This method is called while user accepting DPA.
     * @param int $userId
     * @param int $companyId
     * @param string $email
     * @param string $gtcUrl
     * @param string $urlDpp
     * @param string $imprintUrl
     * @return bool
     * @author Pradeep
     */
    public function setDefaultLangForCreatingAccount(
        int $userId,
        int $companyId,
        string $email,
        string $companyName,
        string $gtcUrl,
        string $urlDpp,
        string $imprintUrl
    ): bool {
        try {
            if ($userId <= 0 || $companyId <= 0 || empty($companyName) || empty($gtcUrl) || empty($urlDpp) || empty($imprintUrl)) {
                throw new \RuntimeException('Failed to set up default language while creating new account');
            }
            // get the default language details.
            $langDetails = $this->languageRepository->getLanguageByName($this->localizationLanguageWizardTypes::DFLT_LANG);
            // build Json For default language selection while creating company.
            $defLangConfig = $this->localizationLanguageWizardTypes->buldCnfgFrDfltLang(
                $companyId, $userId, $companyName, $gtcUrl, $urlDpp, $imprintUrl, $email, $langDetails
            );
            return $this->addLanguage($defLangConfig, $userId, $companyId);
        } catch (ConnectionException|\RuntimeException  $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }
}