<?php


namespace App\Services;


use App\Types\EmailTemplatePlaceholderTypes as EmailTypes;
use Psr\Log\LoggerInterface;
// --------------------------------
use App\Types\MsMailInterfaceTypes;
use App\Types\EmailContentTypes;
use App\Types\MessageParameterTypes;
use App\Services\MsMailService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

use const MaxMind\Db\Reader\_MM_MAX_INT_BYTES;


/**
 * QUICK AND DIRTY -> must be replaced by TWIG Templates TODO: reimplement, since concept is bad ...
 * Class EmailService
 * @package App\Services
 */
class EmailService
{
	private  $logger;
	private  $msMailInterfaceTypes;
	private  $msMail;
	private  $emailContentTypes;
	private  $templating;

	//public function __construct(LoggerInterface $logger, MsMailInterfaceTypes $msMailInterfaceTypes)


	/**
	 * EmailService constructor.
	 * @param LoggerInterface             $logger
	 * @param \App\Services\MsMailService $msMail
	 * @param MsMailInterfaceTypes        $msMailInterfaceTypes
	 * @param EmailContentTypes           $emailContentTypes
	 */
	public function __construct(LoggerInterface $logger,
                                MsMailService $msMail,
                                MsMailInterfaceTypes $msMailInterfaceTypes,
                                EmailContentTypes $emailContentTypes,
                                \Twig\Environment $templating
    )
	{
		$this->msMail = $msMail;
		$this->msMailInterfaceTypes = $msMailInterfaceTypes;
		$this->emailContentTypes = $emailContentTypes;
		$this->logger = $logger;
        $this->templating = $templating;

	}


	/**
	 * @param array                 $recipientEmailList
	 * @param string                $subject
	 * @param string                $emailContentType
	 * @param MessageParameterTypes $messageParameterTypes
	 * @return bool
	 * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
	 * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
	 * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
	 * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
	 */
	public function sendByMsMail(array $recipientEmailList, string $subject, string $emailContentType, MessageParameterTypes $messageParameterTypes, string &$returnMessage=null, MsMailInterfaceTypes $paramMsMailInterfaceTypes=null):bool
	{
		$this->logger->info('PARAMS',[$recipientEmailList,$subject, $emailContentType, $messageParameterTypes->getMessageParameterTypesArray(), __LINE__, __FUNCTION__]);
		// if is set $paramMsMailInterfaceTypes then overwrite $this->msMailInterfaceTypes by $paramMsMailInterfaceTypes,
		if(null !== $paramMsMailInterfaceTypes) {
			foreach($paramMsMailInterfaceTypes as $type => $value) {
				$this->msMailInterfaceTypes->{$type} = $value;
			}
		}

		$checkSendByMsMailParams = $this->checkSendByMsMailParams($recipientEmailList, $subject, $emailContentType);
		$this->logger->info('$checkSendByMsMailParams', [$checkSendByMsMailParams, __LINE__, __FUNCTION__]);
		if(!empty($checkSendByMsMailParams)) {
			throw new \Exception("parameter error: {$checkSendByMsMailParams}", __LINE__);
			return false;
		}

		$this->msMailInterfaceTypes->recipientEmailList = $recipientEmailList;
		$this->msMailInterfaceTypes->messageSubject = $subject;
		$propertyName = $this->emailContentTypes->getPropertyName($emailContentType);

		try {
			$responseContent = $this->msMail->sendStandardMail(
				$this->emailContentTypes->{$propertyName},
				$messageParameterTypes->getMessageParameterTypesArray(),
				$this->msMailInterfaceTypes->getConfigurationArray()
			);
			$this->logger->info('$responseContent', [$responseContent, __LINE__, __FUNCTION__]);
		} catch (ClientExceptionInterface $e) {
			$returnMessage = 'msg:'. $e->getMessage() .', code: '.$e->getCode();
			$this->logger->info('$returnMessage, status', [$returnMessage, 'false', __METHOD__, __LINE__ ]);
			return false;

		} catch (RedirectionExceptionInterface $e) {
			$returnMessage = 'msg:'. $e->getMessage() .', code: '.$e->getCode();
			$this->logger->info('$returnMessage, status', [$returnMessage, 'false', __METHOD__, __LINE__ ]);
			return false;
		} catch (ServerExceptionInterface $e) {
			$returnMessage = 'msg:'. $e->getMessage() .', code: '.$e->getCode();
			$this->logger->info('$returnMessage, status', [$returnMessage, 'false', __METHOD__, __LINE__ ]);
			return false;
		} catch (TransportExceptionInterface $e) {
			$returnMessage = 'msg:'. $e->getMessage() .', code: '.$e->getCode();
			$this->logger->info('$returnMessage, status', [$returnMessage, 'false', __METHOD__, __LINE__ ]);
			return false;
		}

        // Todo  analyze the responseContent and return the corresponding boolean value.
		$returnMessage = $responseContent;
		$this->logger->info('$returnMessage, status', [$returnMessage, 'true', __METHOD__, __LINE__ ]);
		return true;

	}


	/**
	 * @param array  $recipientEmailList
	 * @param string $subject
	 * @param string $emailContentType
	 * @return string if empty, no error where found, else returned string contains error messages
	 */
	private function checkSendByMsMailParams(array $recipientEmailList, string $subject, string $emailContentType) :string
	{
		$msg = '';
		if(empty($subject)) {
			$msg .= "error: empty \$subject, ";
		}

		if(empty($recipientEmailList)) {
			$msg .=  "error: empty \$recipientEmailList, ";
		}

		if(null === $this->emailContentTypes->getPropertyName($emailContentType) ) {
			$msg .= "error: undefined \$emailContentType {$emailContentType}, ";
		}

		return $msg;
	}



	/**
	 * @deprecated
	 * @param string $activationLink
	 * @param string $firstName
	 * @param string $lastName
	 * @return string
	 * @author jsr
	 *
	 */
	public function getActivationLinkContent(string $activationLink, string $firstName, string $lastName, string $accountNo=null): string
	{
//		$this->logger->info('>>> PARAMS: ', [$activationLink, $firstName, $lastName, __FUNCTION__,__LINE__]);
//		$this->logger->info('>>> BASEURL: ', [EmailTypes::getBaseUrl(), __FUNCTION__,__LINE__]);
//		$this->logger->info('>>> FILE:', [EmailTypes::getActivationlinkHtmlFilename(), __FUNCTION__,__LINE__]);
//		$this->logger->info('>>> PATH:', [EmailTypes::getActivationLinkPath(), __FUNCTION__,__LINE__]);
//
//		$this->logger->info('>>> FILENAME:', [EmailTypes::getActivationLinkPath() . '/' . EmailTypes::getActivationlinkHtmlFilename(), __FUNCTION__,__LINE__]);
		//$content = file_get_contents(EmailTypes::getActivationLinkPath() . '/' . EmailTypes::getActivationlinkHtmlFilename());
        $url = EmailTypes::getActivationLinkPath() . '/' . EmailTypes::getActivationlinkHtmlFilename();
        $content = $this->templating->render($url);
//		$this->logger->info('CONTENT BEFORE', [$content, __FUNCTION__, __LINE__]);

		foreach (EmailTypes::getActivationLinkPlaceholders() as $placeholder) {
			switch ($placeholder) {
				case EmailTypes::PLACEHOLDER_LASTNAME:
//					$this->logger->info('PLACEHOLDER', [$placeholder, $lastName, __FUNCTION__, __LINE__]);
					$content = str_replace($placeholder, $lastName, $content);
					break;
				case EmailTypes::PLACEHOLDER_FIRSTNAME:
//					$this->logger->info('PLACEHOLDER', [$placeholder, $firstName, __FUNCTION__, __LINE__]);
					$content = str_replace($placeholder, $firstName, $content);
					break;
				case EmailTypes::PLACEHOLDER_IMGURL:
//					$this->logger->info('PLACEHOLDER', [$placeholder, EmailTypes::getActivationLinkImgUrl(), __FUNCTION__, __LINE__]);
					$content = str_replace($placeholder, EmailTypes::getActivationLinkImgUrl(), $content);
					break;
				case EmailTypes::PLACEHOLDER_ACTIVATIONLINK:
//					$this->logger->info('PLACEHOLDER', [$placeholder, $activationLink, __FUNCTION__, __LINE__]);
					$content = str_replace($placeholder, $activationLink, $content);
					break;
				case EmailTypes::PLACEHOLDER_ACCOUNTNO:
//					$this->logger->info('PLACEHOLDER', [$placeholder, $activationLink, __FUNCTION__, __LINE__]);
					if (null !== $accountNo) {
						$content = str_replace($placeholder, '#'.$accountNo, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				default:
					; // do nothing
					break;
			}
		}
//		$this->logger->info('CONTENT AFTER', [$content, __FUNCTION__, __LINE__]);
		return $content;
	}


	/**
	 * @deprecated
	 * @param string      $activationLink
	 * @param string      $apiKey
	 * @param string      $firstName
	 * @param string      $lastName
	 * @param string|null $accountNo
	 * @return string
	 */
	public function getVerifyApikeyContent(string $activationLink, string $apiKey, string $firstName, string $lastName, string $accountNo=null): string
	{
        $url = EmailTypes::getVerifyApikeyPath() . '/' . EmailTypes::getVerifyApikeyHtmlFilename();
		//$content = file_get_contents(EmailTypes::getVerifyApikeyPath() . '/' . EmailTypes::getVerifyApikeyHtmlFilename());
		$content = $this->templating->render($url);

		foreach (EmailTypes::getVerifyApikeyPlaceholders() as $placeholder) {
			switch ($placeholder) {
				case EmailTypes::PLACEHOLDER_APIKEY:
					$content = str_replace($placeholder, $apiKey, $content);
					break;
				case EmailTypes::PLACEHOLDER_LASTNAME:
					$content = str_replace($placeholder, $lastName, $content);
					break;
				case EmailTypes::PLACEHOLDER_FIRSTNAME:
					$content = str_replace($placeholder, $firstName, $content);
					break;
				case EmailTypes::PLACEHOLDER_IMGURL:
					$content = str_replace($placeholder, EmailTypes::getVerifyApikeyImgUrl(), $content);
					break;
				case EmailTypes::PLACEHOLDER_ACTIVATIONLINK:
					$content = str_replace($placeholder, $activationLink, $content);
					break;
				case EmailTypes::PLACEHOLDER_ACCOUNTNO:
					if (null !== $accountNo) {
						$content = str_replace($placeholder, '#'.$accountNo, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				default:
					; // do nothing
					break;
			}
		}
		return $content;
	}


	/**
	 * @deprecated
	 * @param string      $firstName
	 * @param string      $lastName
	 * @param string      $email
	 * @param string      $accountNo
	 * @param string|null $furtherDetails optional, currently not implemented
	 * @return false|string|string[]
	 */
	public function getNewAccountNotificationContent(string $firstName, string $lastName, string $email, string $accountNo, string $furtherDetails=null)
	{
       $url = EmailTypes::getRegistrationNotificationPath() . '/' . EmailTypes::getRegistrationNotificationHtmlFilename();
		//$content = file_get_contents(EmailTypes::getRegistrationNotificationPath() . '/' . EmailTypes::getRegistrationNotificationHtmlFilename());
		$content = $this->templating->render($url);

		foreach (EmailTypes::getRegistrationNotificationPlaceholders() as $placeholder) {
			switch ($placeholder) {
				case EmailTypes::PLACEHOLDER_LASTNAME:
					$content = str_replace($placeholder, $lastName, $content);
					break;
				case EmailTypes::PLACEHOLDER_FIRSTNAME:
					$content = str_replace($placeholder, $firstName, $content);
					break;
				case EmailTypes::PLACEHOLDER_IMGURL:
					$content = str_replace($placeholder, EmailTypes::getRegistrationNotificationImgUrl(), $content);
					break;
				case EmailTypes::PLACEHOLDER_EMAIL:
					$content = str_replace($placeholder, $email, $content);
					break;
				case EmailTypes::PLACEHOLDER_ACCOUNTNO:
					if (null !== $accountNo) {
						$content = str_replace($placeholder, '#'.$accountNo, $content);
					} else {
						$content = str_replace($placeholder, '', $content);
					}
					break;
				default:
					; // do nothing
					break;
			}
		}
		return $content;
	}




}
