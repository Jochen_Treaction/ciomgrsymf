<?php


namespace App\Services;


use Redis;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


/**
 * removed all APCu stuff for usage with PHP 8.x
 * <br><b>works with REDIS only!!!</b>
 *
 */
class AppCacheService
{

    /**
     * @const USE_REDIS
     * @deprecated since 2022-08-02
     */
    const USE_REDIS = true; // switch between redis and apcu
    const SECRET = 'f#`R5oWw1\e>Lt\20RcN&>jC%a>5QZ';
    const USERACTIVATION = 'Iigei7na_user_activation';
    const USERPWRESET = 'Unsd24oK_user_pw_reset';
    const USERINVITATION= 'Quaik8ai_user_invitation';
    const FIVE_MINUTES = 300;
    const SEVEN_MINUTES = 420;
    const FIFTEEN_MINUTES = 900;
    const THIRTY_MINUTES = 1800;
    const TWO_HOURS = 7200;
    const FOUR_HOURS = 14400;
    const EIGHT_HOURS = 28800;
    const ONE_DAY = 86400;
    const THIRTY_SEC = 30;
    const SELECTED_COMPANY_ID_OF_USER ='SwitchedToCurrentCompId:';



    private bool $isApcu = false;
    private LoggerInterface $logger;
	private AuthenticationUtils $authenticationUtils;
    private string $prefix;
    private string $userName;



    public function __construct(
        LoggerInterface $logger,
        AuthenticationUtils $authenticationUtils,
        string $redisHost,
        string $redisPort,
        string $redisUser,
        string $redisPw
    ) {
        $this->prefix = $_ENV['APP_ENV'] . ':';
        $this->logger = $logger;

        $this->authenticationUtils = $authenticationUtils;
        $this->userName = $authenticationUtils->getLastUsername();
        $logger->info('$this->userName', [$this->userName, __METHOD__, __LINE__]);

        //if(self::USE_REDIS && $redisService->isAuthenticated()) {
        $this->redis = new Redis();
        $connectionStatus = $this->redis->connect($redisHost, $redisPort);
        $this->authSuccessful = $this->redis->auth([$redisUser, $redisPw]);
        // $this->redis = $redisService->getRedisInstance();
        $this->logger->info('using REDIS as cache', [$this->authSuccessful, $this->prefix, __METHOD__, __LINE__]);
    }


    /**
     * Retrieves cached information from APCu's data store
     * @return array
     */
    public function getCacheInfo():array
    {
        if($this->isApcu) {
            // return apcu_cache_info();
            return ['apcu' => false, 'cache' => true];
        } else {
            return ['redis' => true, 'cache' => true]; // return a dummy array, since redis php does not provide cache info
        }
    }


    /**
     * Clears the APCu cache
     * @deprecated since 2022-08-02
     */
    public function clearApcuCache()
    {
        $this->clearCache();
    }


    /**
     * checks if Apcu is available
     * @return bool
     * @author jsr
     * @deprecated since 2022-08-02
     */
    public function hasApcu(): bool
    {
        // return $this->isApcu;
        return false;
    }


    /**
     * checks if redis is accessible
     * @return bool
     */
    public function hasRedis():bool
    {
        $ret = false;

        if(!empty($this->redis->getAuth())) {
            $ret = true;
        }
        return $ret;
    }


    /**
     * Caches a variable in the data store, only if it's not already stored.
     * @param string $key
     * @param mixed $value
     * @return bool
     * @author jsr
     */
    public function addIfNotExists(string $key, $value, $ttl = self::ONE_DAY): bool
    {
        $ret = false;
        // REDIS - all references to keys have to be stored in a hash, which can be deleted completely later (e.g. user logout) without having knowledge of the keys that where used during a user session
        $existsKey = $this->redis->exists($this->getUserKey($key));
        if (!$existsKey) {
            $retSet = $this->redis->set($this->getUserKey($key), serialize($value), $ttl); // add if not exists!!! dies after timeout
            $retHSet = $this->redis->hSetNx($this->getUserKey(), $key, $this->getUserKey($key)); // set a reference to the timed out $this->getUserKey($key) of $this->redis->set, since hashes do not provide expiring data
            $ret = ($retSet && $retHSet);
        }
        return (is_array($ret)) ? false : $ret;
    }


    /**
     * Caches a variable in the data store. CAUTION: If it already exists, it is overwritten!!!
     * @param string $key
     * @param mixed $value
     * @return bool
     * @author jsr
     */
    public function addOrOverwrite(string $key, $value, $ttl = self::ONE_DAY): bool
    {
        $ret = false;

        // REDIS
        $retSet = $this->redis->set($this->getUserKey($key), serialize($value), $ttl);
        $retHSet = $this->redis->hSet($this->getUserKey(), $key, $this->getUserKey($key)); // set a reference to the timed out $this->getUserKey($key) of $this->redis->set, since hashes do not provide expiring data
        $ret = ($retSet && $retHSet);
        return (is_array($ret)) ? false : $ret;
    }


    /**
     * Get a value by key from the cache
     * @param string $key
     * @return bool|mixed <b>false</b>, if key not found, else <br><b>mixed (object, array, string, int, float, boolean, ...)</b> => value
     * @author jsr
     */
    public function get(string $key)
    {
        // $this->logger->info('GET KEY', [$key, __METHOD__, __LINE__]);
        $ret = false;

        // REDIS
        $redisTimeoutKey = $this->redis->hGet($this->getUserKey(), $key);
        $serializedValue = $this->redis->get($redisTimeoutKey); // might be timed out !!!

        if (false !== $serializedValue) {
            $ret = unserialize($serializedValue, ['']);
        } else {
            try {
                $this->redis->hDel($this->getUserKey(), $key);// delete the hash in case key does not exist anymore or was timed out
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
            $ret = false; // key is not existing => might be expired
        }
        // $this->logger->info("RET KEY $redisTimeoutKey", [$ret, __METHOD__, __LINE__]);
        return $ret;
    }


    /**
     * check if an entry for key exists in cache
     * @param string $key
     * @return bool
     * @author jsr
     */
    public function entryExists(string $key): bool
    {
        // REDIS
        $ret = (false === $this->redis->get($this->getUserKey($key))) ? false : true;
        return $ret;
    }


    /**
     * delete an entry from apcu cache by key
     * @param string $key
     * @author jsr
     */
    public function del(string $key): void
    {
        // REDIS
        $redisTimeoutKey = $this->redis->hGet($this->getUserKey(), $key);
        $this->redis->del($redisTimeoutKey); // delete the timeout-key: fire and forget
        $this->redis->hDel($this->getUserKey(), $key); // delete the hash key: fire and forget
    }


    /**
     * clear the cache completely
     */
    public function clearCache()
    {
        try {
            // REDIS
            $section = 'redis';
            $this->logger->info('$this->getUserKey() for $allHashKeysValues', [$this->getUserKey(), __METHOD__, __LINE__]);
            $allHashKeysValues = $this->redis->hGetAll($this->getUserKey());

            $userKey = $this->getUserKey();
            $this->logger->info('DELETING KEYS FOR', [$this->getUserKey(), __METHOD__, __LINE__]);
            $this->logger->info('$allHashKeysValues', [$allHashKeysValues, __METHOD__, __LINE__]);

            foreach ($allHashKeysValues as $hashKey => $redisTimeoutKey) {
                $this->logger->info('DEL KEY', ['$hashKey' => $hashKey, '$redisTimeoutKey' => $redisTimeoutKey, __METHOD__, __LINE__]);
                $this->redis->del($redisTimeoutKey); // delete the timeout-key: fire and forget
                $this->redis->hDel($userKey, $hashKey); // delete the reference hash key: fire and forget
            }
        } catch (\Exception $e) {
            $this->logger->error("cache delete failed in $section", [__METHOD__, __LINE__]);
        }
    }


	/**
	 * @param string $key default: empty string (for use in REDIS hash)
	 * @return string md5(logged in user)-$key
	 */
    protected function getUserKey(string $key=''):string
	{
        // $this->logger->info('key parts',[$this->prefix, $this->userName, hash("md5", $this->userName), $key, __METHOD__, __LINE__]);

        $userKey = $this->prefix . hash("md5", $this->userName);
        $userKey .= (!empty($key)) ? ':'.$key : '';

        // $this->logger->info('userKey', [$userKey, __METHOD__, __LINE__]);
		return $userKey;
	}

}
