<?php

namespace App\Services;

use Psr\Log\LoggerInterface;

class FormFieldValidationService
{

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function isValidSalutation(string $salutation): bool
    {
        $string = trim($salutation);
        return !(empty(trim($string)) || !in_array($string, $this->validSalutation()));
    }

    private function validSalutation(): array
    {
        return ['Mr', 'Mrs', 'Mx'];
    }

    public function isValidEmail(string $email): bool
    {
        $email = trim($email);
        return !(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    public function isValidName(string $name): bool
    {
        $name = trim($name);
        return !(empty($name) || strlen($name) < 3);
    }

    public function isValidString(
        string $string,
        int $minLength = 0,
        int $maxLength = 0,
        bool $umlautsAllowed = false
    ): bool {
        $string = trim($string);
        $strLength = strlen($string);
        $isMinLengthSpecified = $minLength > 0;
        $isMaxLengthSpecified = $maxLength > 0;
        if (empty($string)) {
            return false;
        }
        if ($isMinLengthSpecified && $strLength < $minLength) {
            return false;
        }
        if ($isMaxLengthSpecified && $strLength > $maxLength) {
            return false;
        }
        return true;
    }

    public function isValidPhone(string $phone): bool
    {
        // Allow +, - and . in phone number
        $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
        // Remove "-" from number
        $phone_to_check = str_replace("-", "", $filtered_phone_number);
        // Check the lenght of number
        // This can be customized if you want phone number from a specific country
        return !(strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14);
    }

    public function isValidInt(int $integer): bool
    {

    }
}