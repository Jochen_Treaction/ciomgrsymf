<?php

namespace App\Services;
use App\Repository\FeatureToggleRepository;

class FeatureToggleService
{

    private FeatureToggleRepository $featureToggleRepository;

    public function __construct(FeatureToggleRepository $featureToggleRepository)
    {
        $this->featureToggleRepository = $featureToggleRepository;
    }


    /**
     * @internal <b style="color:green">FeatureToggleRepository::getFeatureToggleStatusByName WAS MADE FOR USE IN PHP CODE<br>to turn features ON and OFF</b><br><i>returns null in case of error or if $featureName was not found</i>
     * @param string $feature
     * @return bool|null
     */
    public function getFeatureToggleStatus(string $featureName ):?bool
    {
        return $this->featureToggleRepository->getFeatureToggleStatusByName($featureName);
    }
}
