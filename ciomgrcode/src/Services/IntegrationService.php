<?php


namespace App\Services;


use App\Repository\CustomfieldsRepository;
use App\Repository\DatatypesRepository;
use App\Repository\StandardFieldMappingRepository;
use App\Types\UniqueObjectTypes;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * @property AioServices aioServices
 */
class IntegrationService
{
    // flag is obtained from frontend, either in integration-emo-twig.js Or emailinone.twig.
    public const auto_create_flag = 'Auto-Create';
    /**
     * @var StandardFieldMappingRepository
     * @author Pradeep
     */
    private  $standardFieldMappingRepository;
    /**
     * @var CustomfieldsRepository
     * @author Pradeep
     */
    private  $customfieldsRepository;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    /**
     * @param LoggerInterface $logger
     * @param AioServices $aioServices
     * @param StandardFieldMappingRepository $standardFieldMappingRepository
     * @param CustomfieldsRepository $customfieldsRepository
     * @author Pradeep
     */

    public function __construct(
        LoggerInterface $logger,
        AioServices $aioServices,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        CustomfieldsRepository $customfieldsRepository
    ) {
        $this->logger = $logger;
        $this->aioServices = $aioServices;
        $this->standardFieldMappingRepository = $standardFieldMappingRepository;
        $this->customfieldsRepository = $customfieldsRepository;
    }

    /**
     * @param array $eMIOCustomFields
     * @return array|null
     * @author Pradeep
     */
    public function autoCreateEMIOCustomField(array $eMIOCustomFields): ?array
    {
        //$this->logger->info('AutocreateEMIOCustomField');
        try {
            if (empty($eMIOCustomFields) ||
                empty($eMIOCustomFields[ 'settings' ][ 'apikey' ]) ||
                empty($eMIOCustomFields[ 'settings' ][ 'mapping' ])) {
                throw new RuntimeException('Invalid eMIOCustomFields provided');
            }

            // Create CustomFields for MIO StandardFields.
            $mioStandardMappingFields = $this->standardFieldMappingRepository->getAutoCreateFields(UniqueObjectTypes::EMAILINONE);
            //$this->logger->info('mioStdfields ' . json_encode($mioStandardMappingFields, JSON_THROW_ON_ERROR));
            foreach ($mioStandardMappingFields as $mioStandardMappingField) {
                //$this->logger->info('Single mioStdfield ' . json_encode($mioStandardMappingField, JSON_THROW_ON_ERROR));
                if (empty($mioStandardMappingField) || empty($mioStandardMappingField[ 'fieldname' ]) || empty($mioStandardMappingField[ 'id' ])) {
                    continue;
                }
                $dataType = $this->standardFieldMappingRepository->getDataType($mioStandardMappingField[ 'mio_id' ]);// Get the datatype of the mioCustomField
                $eMIOFieldName = $this->createCustomFieldIneMIO($mioStandardMappingField[ 'fieldname' ],
                    $eMIOCustomFields[ 'settings' ][ 'apikey' ], $dataType);
                // $this->logger->info('after created field ' . json_encode($eMIOFieldName, JSON_THROW_ON_ERROR));
                if (!$this->standardFieldMappingRepository->updateAutoCreateField($eMIOFieldName,
                    $mioStandardMappingField[ 'id' ])) {
                    $this->logger->error('Failed to update eMIO AutoCreated StandardField.');
                }
            }

            // Create CustomFields for MIO CustomFields.
            $eMIOFields = json_decode(base64_decode($eMIOCustomFields[ 'settings' ][ 'mapping' ]), true, 512,
                JSON_THROW_ON_ERROR);
            $this->logger->info('eMIOFields ' . json_encode($eMIOFields, JSON_THROW_ON_ERROR));
            foreach ($eMIOFields as &$eMIOField) {
                if (empty($eMIOField[ 'email-in-one-custom-field' ]) ||
                    empty($eMIOField[ 'marketing-in-one-custom-field' ]) ||
                    $eMIOField[ 'email-in-one-custom-field' ] !== self::auto_create_flag
                ) {
                    continue;
                }
                // Get the datatype of the mioCustomField
                $dataType = $this->customfieldsRepository->getDataType($eMIOField[ 'marketing-in-one-custom-field-id' ]);
                $this->logger->info('MIO CustomField Datatype: ' . $dataType);
                $field = $this->createCustomFieldIneMIO($eMIOField[ 'marketing-in-one-custom-field' ],
                    $eMIOCustomFields[ 'settings' ][ 'apikey' ], $dataType);
                if (empty($field)) {
                    $this->logger->error('Failed to send request or create customfield in eMIO MicroService');
                    continue;
                }
                $eMIOField[ 'email-in-one-custom-field' ] = $field;
            }
            $this->logger->info('eMIO-custom-fields ' . json_encode($eMIOFields, JSON_THROW_ON_ERROR));
            unset($eMIOField);
            $eMIOCustomFields[ 'settings' ][ 'mapping' ] = base64_encode(json_encode($eMIOFields, JSON_THROW_ON_ERROR));
            return $eMIOCustomFields;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $fieldName
     * @param string $dataType
     * @param string $eMIOAPIKey
     * @return string|null
     * @author Pradeep
     */
    public function createCustomFieldIneMIO(
        string $fieldName,
        string $eMIOAPIKey,
        string $dataType = DatatypesRepository::PHP_STRING
    ): ?string {
        try {
            if (empty($fieldName) || empty($eMIOAPIKey)) {
                throw new RuntimeException('Invalid $fieldName Or APIKey provided');
            }
            $eMIOSanitizedCustomField = $this->sanitizeTextForeMIOCustomFields($fieldName);
            if ($eMIOSanitizedCustomField === null) {
                throw new RuntimeException('Failed to Sanitize CustomField for eMIO');
            }
            if (!$this->aioServices->createCustomFieldIneMIO(
                $eMIOSanitizedCustomField, $dataType, $eMIOAPIKey)) {
                throw new RuntimeException('Failed to send request or create custom field in eMIO MicroService');
            }
            return $eMIOSanitizedCustomField;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return '';
        }
    }

    /**
     * @param string $text
     * @return string|null
     * @author Pradeep
     */
    private function sanitizeTextForeMIOCustomFields(string $text): ?string
    {
        if (empty($text)) {
            return null;
        }
        // Sanitize string
        $sanitized_string = filter_var($text, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        // Replace German Characters
        $sanitized_string = preg_replace(['/Ä/', '/ä/', '/Ö/', '/ö/', '/ß/', '/Ü/', '/ü/', '/\s+/'],
            ['AE', 'ae', 'OE', 'oe', 'ss', 'UE', 'ue', '_'], $sanitized_string);
        // Replace '&' character
        $sanitized_string = str_replace(array(
            '&',
            '/',
            '!',
            '"',
            '§',
            '$',
            '%',
            '#',
            '*',
            '+',
            ';',
            ',',
            '>',
            '<',
            '|',
        ),
            array('-', '-', '', '', '', '', '', '', '', '', '', '', '', '', ''), $sanitized_string);

        return 'MIO_' . $sanitized_string;
    }

    /**
     * @param array $hbConfig
     * @return array|null
     * @author Pradeep
     */
    public function autoCreateHubspotCustomField(array $hbConfig): ?array
    {
        try {
            if (empty($hbConfig) ||
                empty($hbConfig[ 'settings' ][ 'apikey' ]) ||
                empty($hbConfig[ 'settings' ][ 'mapping' ])) {
                throw new RuntimeException('Invalid eMIOCustomFields provided');
            }

            // CREATE CUSTOMFIELD FOR MIO STANDARD FIELDS
            $mioStandardMappingFields = $this->standardFieldMappingRepository->getAutoCreateFields(UniqueObjectTypes::HUBSPOT);
            //$this->logger->info('mioStdfields ' . json_encode($mioStandardMappingFields, JSON_THROW_ON_ERROR));
            foreach ($mioStandardMappingFields as $mioStandardMappingField) {
                //$this->logger->info('Single mioStdfield ' . json_encode($mioStandardMappingField, JSON_THROW_ON_ERROR));
                // VALIDATING CONFIGURATION
                if (empty($mioStandardMappingField) || empty($mioStandardMappingField[ 'fieldname' ]) || empty($mioStandardMappingField[ 'id' ])) {
                    continue;
                }
                // GET THE DATATYPE
                $type = $this->standardFieldMappingRepository->getDataType($mioStandardMappingField[ 'mio_id' ]);
                // CREATE CUSTOMFIELD IN HUBSPOT:
                $hbCustomField = $this->createHubspotCustomField($mioStandardMappingField[ 'fieldname' ],
                    $mioStandardMappingField[ 'fieldname' ],
                    'MIO CustomField', $type, 'text', [], $hbConfig[ 'settings' ][ 'apikey' ]);
                // UPDATE CUSTOMFIELD IN DATABASE
                if (!$this->standardFieldMappingRepository->updateAutoCreateField($hbCustomField,
                    $mioStandardMappingField[ 'id' ])) {
                    $this->logger->error('Failed to update eMIO AutoCreated StandardField.');
                }
            }

            // CREATE CUSTOMFIELD FOR MIO CUSTOMFIELDS
            $hbFields = json_decode(base64_decode($hbConfig[ 'settings' ][ 'mapping' ]), true, 512,
                JSON_THROW_ON_ERROR);

            foreach ($hbFields as &$hbField) {
                // VALIDATING CONFIGURATION
                if (empty($hbField[ 'hb-custom-field' ]) ||
                    empty($hbField[ 'mio-custom-field' ]) ||
                    $hbField[ 'hb-custom-field' ] !== self::auto_create_flag
                ) {
                    continue;
                }
                // GET DATATYPE
                $type = $this->customfieldsRepository->getDataType($hbField[ 'mio-custom-field-id' ]);
                // CREATE CUSTOMFIELD IN HUBSPOT
                $hbCustomField = $this->createHubspotCustomField($hbField[ 'mio-custom-field' ],
                    $hbField[ 'mio-custom-field' ],
                    'MIO CustomField', $type, 'text', [], $hbConfig[ 'settings' ][ 'apikey' ]);
                if (empty($hbCustomField)) {
                    $this->logger->error('Failed to send request or create customfield in Hubspot MicroService');
                    continue;
                }
                $hbField[ 'hb-custom-field' ] = $hbCustomField;
            }
            unset($hbField);
            $hbConfig[ 'settings' ][ 'mapping' ] = base64_encode(json_encode($hbFields, JSON_THROW_ON_ERROR));
            return $hbConfig;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $fieldName
     * @param string $label
     * @param string $fieldDescription
     * @param string $type
     * @param string $fieldType
     * @param array $options
     * @param string $apikey
     * @return string|null
     * @author Pradeep
     */
    public function createHubspotCustomField(
        string $fieldName,
        string $label,
        string $fieldDescription,
        string $type,
        string $fieldType,
        array $options,
        string $apikey
    ): ?string {
        $hbSanitizedField = '';
        try {
            if (empty($fieldName) || empty($apikey)) {
                throw new RuntimeException('Invalid Configuration for HubspotCustomField');
            }// SANITIZE CUSTOM FIELD
            $hbSanitizedField = $this->sanitizeTextForeMIOCustomFields($fieldName);
            if ($hbSanitizedField === null) {
                throw new RuntimeException('Failed to Sanitize CustomField ' . $hbSanitizedField . ' for Hubspot');
            }
            $this->logger->info('createHubspotCustomField ' .
                                json_encode([
                                    $hbSanitizedField,
                                    $hbSanitizedField,
                                    $fieldDescription,
                                    $type,
                                    $fieldType,
                                    $options,
                                    $apikey,
                                ], JSON_THROW_ON_ERROR));
            // CREATE HUBSPOT FIELD
            if (!$this->aioServices->createHubspotCustomField($hbSanitizedField, $hbSanitizedField, $fieldDescription,
                $type,
                $fieldType, $options, $apikey, true)) {
                throw new RuntimeException('Failed to send request or create customfield ' . $hbSanitizedField . ' in Hubspot MicroService');
            }
            $this->logger->info('create CustomField in HUbspot ' . $hbSanitizedField);
            return $hbSanitizedField;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $hbSanitizedField;
        }
    }
}
