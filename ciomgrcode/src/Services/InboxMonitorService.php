<?php


namespace App\Services;

use App\Repository\InboxMonitorRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\SeedPoolRepository;
use App\Repository\UsersRepository;
use App\Types\SeedPoolTypes;
use App\Types\UniqueObjectTypes;
use JsonException;
use Psr\Log\LoggerInterface;
use App\Services\AppCacheService;
use RangeException;
use Exception;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class InboxMonitorService
{

    protected LoggerInterface $logger;
    protected \App\Services\AppCacheService $cacheService;
    private \Symfony\Contracts\HttpClient\HttpClientInterface $httpClient;
    private SeedPoolRepository $seedPoolRepository;
    private InboxMonitorRepository $inboxMonitorRepository;
    private SeedPoolTypes $seedPoolTypes;
    private ObjectRegisterRepository $objectRegisterRepository;
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    private UsersRepository $usersRepository;

    /**
     * @return mixed
     */
    public function __construct(LoggerInterface              $logger,
                                AppCacheService              $cacheService,
                                SeedPoolRepository           $seedPoolRepository,
                                InboxMonitorRepository       $inboxMonitorRepository,
                                SeedPoolTypes                $seedPoolTypes,
                                ObjectRegisterRepository     $objectRegisterRepository,
                                ObjectRegisterMetaRepository $objectRegisterMetaRepository,
                                UsersRepository              $usersRepository
    )
    {
        $this->logger = $logger;
        $this->cacheService = $cacheService;
        $this->httpClient = HttpClient::create(['http_version' => '2.0']);
        $this->seedPoolRepository = $seedPoolRepository;
        $this->inboxMonitorRepository = $inboxMonitorRepository;
        $this->seedPoolTypes = $seedPoolTypes;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->usersRepository = $usersRepository;
    }

    /**
     * @param string $mosentoUserId
     * @param string $mosentoApikey
     * @param string $mosentoAPi
     * @return array
     * @author Aki
     * @internal Calls mosento and gets data
     */
    public function getSeedPoolsFromMosento(string $mosentoUserId,
                                            string $mosentoApikey,
                                            string $mosentoAPi
    ): array
    {
        $seedPools = [];
        $method = 'POST';
        $endPoint = $mosentoAPi;
        try {
            $body = array(
                'api_key' => $mosentoApikey,
                'user_id' => $mosentoUserId,
                'endpoint' => 'get_seed_pools'
            );
            $response = $this->httpClient->request($method, $endPoint, ['body' => $body]);
            if ($response->getStatusCode() === 200) {
                $seedPools = $response->toArray();
            }
        } catch (TransportExceptionInterface|JsonException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $seedPools;
        }
        // validate the response and return the status;
        return $seedPools;

    }

    /**
     * @param array $mosentoConfig
     * @param string $apiEndPoint
     * @param int $companyId
     * @return array|null
     * @author Aki
     */
    public function getSeedPoolDataForAssignSeedPool(int    $superAdminObjectRegisterId,
                                                     string $apiEndPoint,
                                                     int    $companyId
    ): ?array
    {
        $mosentoConfig = $this->getMosentoConfig($superAdminObjectRegisterId);
        //Get seed pool from mosento
        $seedPoolsFromMosento = $this->getSeedPoolsFromMosento($mosentoConfig['settings']['user'],
            $mosentoConfig['settings']['apikey'],
            $apiEndPoint)['data'];
        if (empty($seedPoolsFromMosento)) {
            return null;
        }
        //Get the data from the Database
        $allAvailableSeedPoolGroups = $this->seedPoolRepository->getSeedPoolGroups();
        $seedPoolMappingForAccount = $this->structureSeedPoolMapping($this->seedPoolRepository->getSeedPoolsForAccount($companyId));
        //Get the data from the types
        $allAvailableSeedPoolTypes = $this->seedPoolTypes::getUITypes();
        //process the data and show the available seed pools(available_seed_pools = allSeedPools- seedPoolsAssigned)
        $allAvailableSeedPools = $this->getAvailableSeedPools($seedPoolsFromMosento['seed_pools']);
        //return the array
        return [
            'allAvailableSeedPools' => $allAvailableSeedPools,
            'seedPoolMappingForAccount' => $seedPoolMappingForAccount,
            'allAvailableSeedPoolGroups' => $allAvailableSeedPoolGroups,
            'allAvailableSeedPoolTypes' => $allAvailableSeedPoolTypes
        ];

    }

    /**
     * @param $superAdminObjectRegisterId
     * @return array
     * @author Aki
     */
    public function getMosentoConfig($superAdminObjectRegisterId): array
    {
        // Get the mosento config from database
        $mosentoConfig = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails($superAdminObjectRegisterId,
            UniqueObjectTypes::INBOX_MONITOR_CONFIG)['meta_value'];
        //extract the required params
        try {
            $mosentoConfig = json_decode($mosentoConfig, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage());
        }
        return $mosentoConfig;

    }

    /**
     * @param array $seedPoolsFromMosento
     * @return array
     * @internal removes assigned seed pools from the list of seed pools sent from mosento
     * @author Aki
     */
    private function getAvailableSeedPools(array $seedPoolsFromMosento): array
    {
        //assigned seed pools from database
        $allAssignedSeedPools = $this->seedPoolRepository->getAllSeedPools();
        //loop through seed pools from mosento and remove the assigned seed pools
        foreach ($seedPoolsFromMosento as $key => $seedPool) {
            foreach ($allAssignedSeedPools as $assignedSeedPool) {
                if ($assignedSeedPool['mosento_seed_pool_id'] === $seedPool['id']) {
                    unset($seedPoolsFromMosento[$key]);
                }
            }
            //remove seed pools which does not have a name - WEB-8523
            if (empty($seedPool['name'])) {
                unset($seedPoolsFromMosento[$key]);
            }
            //remove seed pool if the type is not monitoring
            if ($seedPool['type'] !== 'monitoring') {
                unset($seedPoolsFromMosento[$key]);
            }
        }
        return $seedPoolsFromMosento;
    }

    /**
     * @param array $seedPoolsForAccount
     * @return array
     * @internal structures into UI mapping table - adds UI seed pool type and seed pool group name
     * @author Aki
     */
    private function structureSeedPoolMapping(array $seedPoolsForAccount): array
    {
        //return empty if no records are found
        if (empty($seedPoolsForAccount)) {
            return [];
        }
        //Add the UI seed pool type and seed pool group name
        foreach ($seedPoolsForAccount as $key => $seepPool) {
            $seedPoolGroupId = $seepPool['seed_pool_group_id'];
            $seedPoolsForAccount[$key]['group'] = $this->seedPoolRepository->getSeedPoolGroupNameWithId($seedPoolGroupId);
            $seedPoolsForAccount[$key]['type'] = $this->seedPoolTypes::convertDatabaseTypesToUITypes($seedPoolsForAccount[$key]['type']);
            //remove some unnecessary params for UI
            unset($seedPoolsForAccount[$key]['created']);
            unset($seedPoolsForAccount[$key]['updated']);
            unset($seedPoolsForAccount[$key]['created_by']);
            unset($seedPoolsForAccount[$key]['updated_by']);
        }

        return $seedPoolsForAccount;
    }

    /**
     * @param array $seedPoolDataFormMappingTable
     * @param int $companyId
     * @param int $superAdminObjectRegisterId
     * @param string $apiEndPoint
     * @param int $userId
     * @return bool
     * @author Aki
     * @internal Takes mapping table from UI and fill the database respectively
     */
    public function saveSeedPoolData(array  $seedPoolDataFormMappingTable,
                                     int    $companyId,
                                     int    $superAdminObjectRegisterId,
                                     string $apiEndPoint,
                                     int    $userId
    ): bool
    {
        //set the current user
        $this->seedPoolRepository->setRepostitoriesCurrentUserId($userId);
        //create new seed pool groups
        if (!$this->createNewSeedPoolGroups($seedPoolDataFormMappingTable)) {
            return false;
        }
        //update seed pool table
        if (!$this->createOrUpdateSeedPoolTable($seedPoolDataFormMappingTable, $companyId, $superAdminObjectRegisterId, $apiEndPoint)) {
            return false;
        }
        return true;
    }

    /**
     * @param array $seedPoolDataFormMappingTable
     * @return bool
     * @author Aki
     * @internal - compares the seed pool table and UI data, if seed pool group does not exist creates one
     */
    private function createNewSeedPoolGroups(array $seedPoolDataFormMappingTable): bool
    {
        try {
            //loop through data
            foreach ($seedPoolDataFormMappingTable as $seedPoolRow) {
                if ($this->checkIfNewSeedPoolGroupExists($seedPoolRow['group'])) {
                    $this->seedPoolRepository->setSeedPoolGroup($seedPoolRow['group']);
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
        return true;
    }

    /**
     * @param array $seedPoolDataForMapping
     * @param int $companyId
     * @param int $superAdminObjectRegisterId
     * @param string $apiEndPoint
     * @return bool
     * @author Aki
     * @internal update seed pool table with the data from UI
     */
    private function createOrUpdateSeedPoolTable(array  $seedPoolDataForMapping,
                                                 int    $companyId,
                                                 int    $superAdminObjectRegisterId,
                                                 string $apiEndPoint): bool
    {
        try {
            foreach ($seedPoolDataForMapping as $seedPool) {
                //check if seed pool id exists if not create a seed pool entry
                if (!isset($seedPool['seed_pool_id'])) {
                    //Create new seed pool
                    //create object register entry
                    $objectRegisterForSeedPoolId = $this->objectRegisterRepository->
                    createInitialObjectRegisterEntry($companyId, UniqueObjectTypes::SEEDPOOL);
                    $this->logger->info('Object register id for seed pool', [$objectRegisterForSeedPoolId, __METHOD__, __LINE__]);
                    //get seed pool info from mosento
                    $mosnetoSeedPoolId = $this->getMosentoSeedPoolIdByName($seedPool['seed_pool'],
                        $superAdminObjectRegisterId,
                        $apiEndPoint);
                    $this->logger->info('mosnetoSeedPoolId', [$mosnetoSeedPoolId, __METHOD__, __LINE__]);
                    if (!is_null($objectRegisterForSeedPoolId)) {
                        //get seed pool group id
                        $seedPoolGroupId = $this->seedPoolRepository->getSeedPoolGroupIdWithName($seedPool['group']);
                        //insert new row in database
                        $this->seedPoolRepository->createSeedPool(
                            $seedPoolGroupId,
                            $companyId,
                            $objectRegisterForSeedPoolId,
                            $mosnetoSeedPoolId,
                            $seedPool['seed_pool'],
                            $this->seedPoolTypes::convertUITypesToDataBaseTypes($seedPool['type'])
                        );
                    }
                }

            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
        return true;
    }

    /**
     * @param string $group
     * @return bool
     * @author Aki
     * @internal check if new seed pool group exists
     */
    private function checkIfNewSeedPoolGroupExists(string $group): bool
    {
        //Get all available seed pool groups
        $seedPoolGroupsFromDatabase = $this->seedPoolRepository->getSeedPoolGroups();
        $seedPoolGroups = [];
        //collect all seed pool groups
        foreach ($seedPoolGroupsFromDatabase as $seedPool) {
            $seedPoolGroups[] = $seedPool['name'];
        }
        //check if seed pool group from UI exists in database
        if (in_array($group, $seedPoolGroups, true)) {
            return false;
        }
        return true;
    }

    /**
     * @param string $seedPoolName
     * @param $superAdminObjectRegisterId
     * @param $apiEndPoint
     * @return string|null
     * @author Aki
     */
    private function getMosentoSeedPoolIdByName(string $seedPoolName, $superAdminObjectRegisterId, $apiEndPoint): ?string
    {
        $mosentoSeedPoolId = null;
        $mosentoConfig = $this->getMosentoConfig($superAdminObjectRegisterId);
        //Get seed pool from mosento
        $seedPoolsFromMosento = $this->getSeedPoolsFromMosento($mosentoConfig['settings']['user'],
            $mosentoConfig['settings']['apikey'],
            $apiEndPoint)['data'];
        if (empty($seedPoolsFromMosento)) {
            return null;
        }
        //loop seed pools from mosento and take the required data
        foreach ($seedPoolsFromMosento['seed_pools'] as $seedPool) {
            if ($seedPool['name'] === $seedPoolName) {
                $mosentoSeedPoolId = $seedPool['id'];
            }
        }
        return $mosentoSeedPoolId;
    }

    /**
     * @param string $mosentoSeedPoolId
     * @param array $mosentoConfig
     * @param string $mosentoAPi
     * @return array|null
     * @author Aki
     * @internal call mosento get inbox monitor route and get inbox-monitor for a single seed pool
     */
    public function getInboxMonitorFromMosento(string $mosentoSeedPoolId,
                                               array  $mosentoConfig,
                                               string $mosentoAPi
    ): ?array
    {
        $inboxMonitorData = [];
        $method = 'POST';
        $endPoint = $mosentoAPi;

        $lastThreeDays= Date('Y-m-d', strtotime('-3 days'));

        $today = date('Y-m-d');

        try {
            $body = array(
                'api_key' => $mosentoConfig['apikey'],
                'user_id' => $mosentoConfig['user'],
                'endpoint' => 'get_inbox_monitor',
                'seed_pool_id' => $mosentoSeedPoolId,
                'date_from' 	=> $lastThreeDays,
                'date_to' 		=> $today,
            );
            $response = $this->httpClient->request($method, $endPoint, ['body' => $body]);
            if ($response->getStatusCode() === 200) {
                $inboxMonitorData = $response->toArray();
            }
        } catch (TransportExceptionInterface|JsonException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $inboxMonitorData;
        }
        // validate the response and return the status;
        return $inboxMonitorData;
    }

    public function getBlacklistDomainInfoFromMosento(string $domain,
                                                      array  $mosentoConfig,
                                                      string $mosentoAPi
    ): ?array
    {
        $domainBlacklistData = [];
        $method = 'POST';
        $endPoint = $mosentoAPi;

        try {
            $body = array(
                'api_key' => $mosentoConfig['apikey'],
                'user_id' => $mosentoConfig['user'],
                'endpoint' => 'get_blacklist_monitor',
                'domain' => $domain
            );
            $response = $this->httpClient->request($method, $endPoint, ['body' => $body]);
            if ($response->getStatusCode() === 200) {
                $domainBlacklistData = $response->toArray();
            }
        } catch (TransportExceptionInterface|JsonException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $domainBlacklistData;
        }
        // validate the response and return the status;
        return $domainBlacklistData;
    }

    /**
     * @param array $inboxMonitorResponse
     * @param string $mosentoSeedPoolId
     * @param array $mosentoConfig
     * @param string $apiEndpoint
     * @return bool
     * @author Aki
     * @internal sync the data between mosento and MIO DB - on table's inbox_monitor,domains makes CURD operations
     */
    public function syncInboxMonitor(array  $inboxMonitorResponse,
                                     string $mosentoSeedPoolId,
                                     array  $mosentoConfig,
                                     string $apiEndpoint
    ): bool
    {
        //$this->logger->info('PARAMS', [$inboxMonitorResponse, $mosentoSeedPoolId, __METHOD__, __LINE__]);
        $inboxMonitorData = $inboxMonitorResponse['data']['inbox_monitor'];

        if (empty($inboxMonitorData)) {
            $this->logger->warning('Inbox monitor data is empty for seedpool id ' . $mosentoSeedPoolId, [__METHOD__, __LINE__]);
            return true;
        }

        //Get all mosento_seed_pool_id 's in MIO database as an array
        $mosentoInboxMonitorIdArrayInMIODatabase = $this->
        inboxMonitorRepository->getMosentoInboxMonitorIdsArray($mosentoSeedPoolId);

        //get seedpool
        $seedPool = $this->seedPoolRepository->getSeedPoolFromMosentoSeedPoolId($mosentoSeedPoolId);
        //$this->logger->info('$seedPool', [$seedPool, __METHOD__, __LINE__]);
        //loop through all inbox monitors
        foreach ($inboxMonitorData as $inboxMonitor) {

            /* Inbox monitor form mosento
             * array (
                        'id' => '#IM231677',
                        'seed_pool_id' => '71',
                        'from_address' => '202204ta@unsere.weltweitlesen.de',
                        'from_domain' => 'weltweitlesen.de',
                        'from_subdomain' => 'unsere.weltweitlesen.de',
                        'from_name' => 'Horoskop 2022',
                        'subject' => 'Ihre Sterne für 2022 stehen bereit',
                        'first_mail' => '2022-04-08 12:18:10',
                        'received_total' => '103',
                        'received_inbox' => '103',
                        'received_spam' => '0',
                        'header_info' =>
                        array (
                        ),
             */

            $mosentoInboxMonitorId = $inboxMonitor['id'];
            //$this->logger->info('$mosentoInboxMonitorId', [$mosentoInboxMonitorId, __METHOD__, __LINE__]);
            //check for the DB entry for inbox monitor and if exists update the entry
            if (in_array($mosentoInboxMonitorId, $mosentoInboxMonitorIdArrayInMIODatabase, true)) {
                //update the inbox monitor entry
                if (!$this->inboxMonitorRepository->updateInboxMonitorWithMosentoInboxMonitorId($mosentoInboxMonitorId, $inboxMonitor)) {
                    $this->logger->info('Inbox monitor can\'t be updated', [__METHOD__, __LINE__]);
                    return false;
                }
                //remove it form the $mosentoInboxMonitorIdInMIODatabase
                if (($key = array_search($mosentoInboxMonitorId, $mosentoInboxMonitorIdArrayInMIODatabase, true)) !== false) {
                    unset($mosentoInboxMonitorIdArrayInMIODatabase[$key]);
                }
                continue;
            }
            //Domains
            $domain = $inboxMonitor['from_domain'];
            $subDomain = $inboxMonitor['from_subdomain'];
            $firstMailReceived = $inboxMonitor['first_mail'];

            if ($this->inboxMonitorRepository->checkIfDomainExists($domain)) {
                //Get domain id with domain
                $domainId = $this->inboxMonitorRepository->getDomainIdWithDomain($domain);
            } else {
                //create an entry in domains
                $domainId = $this->createEntryForDomain($domain,
                    $subDomain, $firstMailReceived, $mosentoConfig, $apiEndpoint, $seedPool['company_id'],$seedPool['name']);
            }

            //Create new inbox monitor table entry
            if (!$this->createInboxMonitorEntry($inboxMonitor, $seedPool, $domainId)) {
                $this->logger->error('createInboxMonitorEntry: Inbox monitor can\'t be created', [__METHOD__, __LINE__]);
                return false;
            }

            //create entry for domain_blacklist table
            if(!$this->inboxMonitorRepository->checkIfDomainBlacklistEntryExists($domainId) &&
                !$this->createDomainBlacklistEntry($domainId,$domain,$mosentoConfig, $apiEndpoint)){
                $this->logger->warning('Domain blacklist table entry: Entry in domain_blacklist can\'t be created',
                    [__METHOD__, __LINE__]);
            }
            /*
             * @internal: domains can't be updated in mosento i.e no updates in MIO database
            else if(!$this->updateDomainBlacklist($domainId,$domain,$mosentoConfig, $apiEndpoint)){
                $this->logger->warning('Domain blacklist table entry: Entry in domain_blacklist can\'t be updated',
                    [__METHOD__, __LINE__]);
            }
            */
        }

        return true;

        /*
         * @internal:web-5823 we don't need 'delete' operation for sync only create, update, read are done for sync
        if (empty($mosentoInboxMonitorIdArrayInMIODatabase)) {
            return true;
        }

        //Delete the MIO DB entry with not present in mosento
        foreach ($mosentoInboxMonitorIdArrayInMIODatabase as $mosentoInboxMonitorId) {
            if (!$this->inboxMonitorRepository->deleteInboxMonitorEntryWithMosentoInboxMonitorId($mosentoInboxMonitorId)) {
                $this->logger->info('Inbox monitor can\'t be deleted', [__METHOD__, __LINE__]);
                return false;
            }
        }
        */

    }

    /**
     * @param array $inboxMonitor
     * @param array $seedPool
     * @param int $domainId
     * @return bool
     * @author Aki
     * @internal This method creates new inbox-monitor entry along with object register entry and domains_blacklist table and returns true on success
     */
    private function createInboxMonitorEntry(array $inboxMonitor, array $seedPool, int $domainId): bool
    {
        $mosentoInboxMonitorId = $inboxMonitor['id'];
        //Get super admin user id
        $userId = $this->usersRepository->getSuperAdminUserId();
        //set user id for inserts
        $this->inboxMonitorRepository->setRepostitoriesCurrentUserId($userId);
        //create object register entry for inbox monitor
        $objectRegisterIdForInboxMonitor = $this->objectRegisterRepository->
        createInitialObjectRegisterEntry($seedPool['company_id'], UniqueObjectTypes::INBOX_MONITOR);
        //$this->logger->info('Object register id for inbox monitor', [$objectRegisterIdForInboxMonitor, __METHOD__, __LINE__]);
        //Get the span reason
        $spamReason = '';
        if (count($inboxMonitor['header_info']) > 0) {
            $spamReason = implode(",", ($inboxMonitor['header_info']));
        }
        //create the inbox monitor entry in DB
        try {
            $inboxMonitorId = $this->inboxMonitorRepository->createInboxMonitor(
                $seedPool['id'],
                $domainId,
                $objectRegisterIdForInboxMonitor,
                $mosentoInboxMonitorId,
                $inboxMonitor['from_name'],
                $inboxMonitor['first_mail'],
                $inboxMonitor['subject'],
                $inboxMonitor['from_address'],
                $inboxMonitor['received_total'],
                $inboxMonitor['received_inbox'],
                $inboxMonitor['received_spam'],
                $spamReason
            );
            if (is_null($inboxMonitorId)) {
                $this->logger->info('Inbox monitor can\'t be Created', [__METHOD__, __LINE__]);
                return false;
            }
            return true;
        } catch (Exception $e) {
            $this->logger->info('Inbox monitor can\'t be Created', [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param string $domain
     * @param string $subDomain
     * @param string $firstMailReceived
     * @param array $mosentoConfig
     * @param string $apiEndpoint
     * @param int $companyId
     * @param string $seedPool
     * @return int|null
     * @author Aki
     * @internal Create entry in MIO_domains and MIO_domains_blacklist tables
     */
    private function createEntryForDomain(string $domain,
                                          string $subDomain,
                                          string $firstMailReceived,
                                          array  $mosentoConfig,
                                          string $apiEndpoint,
                                          int    $companyId,
                                          string $seedPool
    ): ?int
    {
        //create object register entry for inbox monitor
        $objectRegisterIdForDomain = $this->objectRegisterRepository->
        createInitialObjectRegisterEntry($companyId, UniqueObjectTypes::DOMAIN);
        //$this->logger->info('Object register id for domain '.$domain,[$objectRegisterIdForDomain,__METHOD__,__LINE__]);
        //Get super admin user id
        $userId = $this->usersRepository->getSuperAdminUserId();
        //set user id for inserts
        $this->inboxMonitorRepository->setRepostitoriesCurrentUserId($userId);
        //Make no entry in subdomain if domain and sub_domain are same
        if ($domain === $subDomain) {
            $subDomain = '';
        }

        //Get black list data from mosento
        $domainBlacklistData = [];
        $blacklistDomainResponse = $this->getBlacklistDomainInfoFromMosento($domain, $mosentoConfig, $apiEndpoint);
        if(isset($blacklistDomainResponse['data'])){
            $domainBlacklistData = $blacklistDomainResponse['data'];
        }
        /* example of blacklist monitor data form mosento
             array (
            'blacklist_monitor' =>
                    array (
                      0 =>
                      array (
                        'domain' => '4champion.ru',
                        'hosting_ip' => '46.17.104.167',
                        'cyren_url_categories' => NULL,
                        'cyren_blacklisted' => false,
                        'cyren_last_check' => '0000-00-00 00:00:00',
                        'public_bls_blacklisted' => false,
                        'public_bls_listed' => NULL,
                        'public_bls_last_check' => '0000-00-00 00:00:00',
                      ),
                    ),
                  ),
                  'error_message' => NULL,
                );
            ]*/

        $hostingIp = null;
        if (isset($domainBlacklistData['blacklist_monitor'][0]['hosting_ip'])) {
            $hostingIp = $domainBlacklistData['blacklist_monitor'][0]['hosting_ip'];
        }

        //create domain in DB
        $domainId = $this->inboxMonitorRepository->createDomainEntry(
            $objectRegisterIdForDomain,
            $domain,
            $subDomain,
            $firstMailReceived,
            $hostingIp,
            $companyId,
            $seedPool
        );

        if (is_null($domainId)) {
            $this->logger->info('Domains can\'t be Created in DB', [__METHOD__, __LINE__]);
            return null;
        }

        return $domainId;
    }

    /**
     * @param int $domainId
     * @param string $domain
     * @param array $mosentoConfig
     * @param string $apiEndpoint
     * @return bool
     * @internal Get blacklist data from mosento and create entry in domain_blacklists
     */
    private function createDomainBlacklistEntry(int    $domainId,
                                                string $domain,
                                                array  $mosentoConfig,
                                                string $apiEndpoint)
    :bool
    {
        $blacklistDomainResponse = $this->getBlacklistDomainInfoFromMosento($domain, $mosentoConfig, $apiEndpoint);
        if(isset($blacklistDomainResponse['data'])){
            $domainBlacklistData = $blacklistDomainResponse['data'];
        }

        /* example of blacklist monitor data form mosento
             array (
            'blacklist_monitor' =>
                    array (
                      0 =>
                      array (
                        'domain' => '4champion.ru',
                        'hosting_ip' => '46.17.104.167',
                        'cyren_url_categories' => NULL,
                        'cyren_blacklisted' => false,
                        'cyren_last_check' => '0000-00-00 00:00:00',
                        'public_bls_blacklisted' => false,
                        'public_bls_listed' => NULL,
                        'public_bls_last_check' => '0000-00-00 00:00:00',
                      ),
                    ),
                  ),
                  'error_message' => NULL,
                );
            ]*/

        if (isset($domainBlacklistData['blacklist_monitor'][0])) {
            // create domain_blacklist entry in DB
            $blacklistDomainId = $this->inboxMonitorRepository->createDomainBlacklistEntry($domainId,
                $domainBlacklistData['blacklist_monitor'][0]);

            if (is_null($blacklistDomainId)) {
                $this->logger->info('Blacklist domains can\'t be Created', [__METHOD__, __LINE__]);
                return false;
            }
        }
        return true;
    }

    /**
     * @param int $domainId
     * @param $domain
     * @param array $mosentoConfig
     * @param string $apiEndpoint
     * @return bool
     * @deprecated
     * @internal update the domain_blacklist entry
     */
    private function updateDomainBlacklist(int $domainId, $domain, array $mosentoConfig, string $apiEndpoint):bool
    {
        $domainBlacklistId = $this->inboxMonitorRepository->getDomainBlacklistIdWithDomainId($domainId);
        if($domainBlacklistId > 0){
            $blacklistDomainResponse = $this->getBlacklistDomainInfoFromMosento($domain, $mosentoConfig, $apiEndpoint);
            if(isset($blacklistDomainResponse['data'])){
                $domainBlacklistData = $blacklistDomainResponse['data'];
            }

            if (isset($domainBlacklistData['blacklist_monitor'][0])) {
                // create domain_blacklist entry in DB
                 if(!$this->inboxMonitorRepository->updateDomainBlacklistEntry($domainBlacklistId,
                    $domainBlacklistData['blacklist_monitor'][0])){
                     $this->logger->warning('Blacklist domains can\'t be Updated', [__METHOD__, __LINE__]);
                 }
            }
            return true;
        }
        return false;
    }

}
