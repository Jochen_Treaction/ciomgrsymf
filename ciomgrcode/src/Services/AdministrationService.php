<?php


namespace App\Services;

use App\Entity\Company;
use App\Entity\Users;
use App\Repository\AdministratesUsersRepository;
use App\Services\AuthenticationService;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Repository\AdministrationRepository;
use App\Repository\UsersRepository;
use App\Repository\CompanyRepository;
use App\Services\RolesService;
use App\Services\SwiftMailerService;
use App\Services\CryptoService;
use Psr\Log\LoggerInterface;
use App\Services\CampaignService;

class AdministrationService
{
    protected $logger;
    protected $usersRepository;
    protected $administrationRepository;
    protected $authenticationUtils;
    protected $authenticationService;
    protected $rolesService;
    protected $administratesUsersRepository;
    protected $swiftMailerService;
    protected $cryptoService;
    protected $cacheService;
    protected $companyRepository;
    protected $campaignServcie;


    public function __construct(
        LoggerInterface $logger,
        UsersRepository $usersRepository,
        CompanyRepository $companyRepository,
        AdministrationRepository $administrationRepository,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService,
        AdministratesUsersRepository $administratesUsersRepository,
        RolesService $rolesService,
        SwiftMailerService $swiftMailerService,
        CryptoService $cryptoService,
        AppCacheService $cacheService,
        CampaignService $campaignServcie,
        AioServices $aioServices
    ) {
        $this->logger = $logger;
        $this->usersRepository = $usersRepository;
        $this->administrationRepository = $administrationRepository;
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->rolesService = $rolesService;
        $this->administratesUsersRepository = $administratesUsersRepository;
        $this->swiftMailerService = $swiftMailerService;
        $this->cryptoService = $cryptoService;
        $this->cacheService = $cacheService;
        $this->companyRepository = $companyRepository;
        $this->campaignServcie = $campaignServcie;
        $this->aioServices = $aioServices;
    }


    /**
     * @return array [user_id,user_1stletter,first_name,last_name,user_roles,email,company_id,company_name,company_account_no,is_user,is_admin,is_master_admin,is_super_admin,login_ts,login_expires_ts]  for app.twig
     */
    public function getCurrentUser(): array
    {
        return $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
    }


    /**
     * @return bool
     * @author   jsr
     * @since    WEB-3850
     */
    public function isSuperAdmin(): bool
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        return $user['is_super_admin'];
    }


    /**
     * @return bool
     * @author   jsr
     * @since    WEB-3850
     */
    public function isMasterAdmin(): bool
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        return $user['is_master_admin'];
    }


    /**
     * @return bool
     * @author   jsr
     * @since    WEB-3850
     */
    public function isAdmin(): bool
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        return $user['is_admin'];
    }


    /**
     * @param int $company_id
     * @return array
     */
    public function getUserIdsOfUsersCompany(int $company_id):array
    {
        return $this->administratesUsersRepository->getUserIdsOfUsersCompany($company_id);
    }


    /**
     * @param int $user_id
     * @param int $company_id
     * @return bool
     */
    public function doesUserManageCompany(int $user_id, int $company_id):bool
    {
        return $this->administratesUsersRepository->doesUserManageCompany($user_id,  $company_id);
    }


    /**
     * deleteAccountByUserEmail used by route (/admintool)/super/useroverview from UI Administration ->Account Overview -> Actions -> Delete Account
     * @param string $email
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @internal SuperAdmin only
     * @author   jsr
     */
    public function deleteAccountByUserEmail(string $email): array
    {
        $this->logger->info('emailaddress to delete', [$email, __METHOD__, __LINE__]);
        $hostUser = $this->getCurrentUser(); // user that invited others to one or mor companies
        $hostUserId = $hostUser['user_id'];
        $userToDelete = $this->usersRepository->getUserByEmail($email);

        $account_no = null;
		try {
			$account_no = $userToDelete->getCompany()->getAccountNo();// to delete account on file system
		} catch (\Exception $e) {
			// seems that $email is a zombie user record (user, who's company does not exist) that came in with Shop MVP
			;
		}

		if(null === $account_no) {
			// seems that $email is a zombie user record (user, who's company does not exist) that came in with Shop MVP
			if($this->usersRepository->isZombieUserRecord($email)) {
				$this->administrationRepository->deleteUser($userToDelete->getId());
				return ['ret' => true, 'msg' => "account {$email} was deleted"];
			}
		}

		$userToDeleteCompanyId = $userToDelete->getCompany()->getId();

        $this->logger->info('$userToDelete' , [$userToDelete, __METHOD__, __LINE__]);
        $this->logger->info('$this->isSuperAdmin()' , [$this->isSuperAdmin(), __METHOD__, __LINE__]);
        $data = [];

        // early exit
        if (empty($userToDelete) || !$this->isSuperAdmin()) { // see also security.yaml
            return ['ret' => false, 'msg' => ['denied' => 'you are not permitted to delete this account. e' . __LINE__]];
        }

//        if($this->isMasterAdmin()) { // check if own record is deleted
//            // check if masterAdmin has invited this user => if not, exit!
//            $currentUser = $this->getCurrentUser();
//            $toBeDeletedUserId = $users->getId();
//
//            if($currentUser['user_id'] === $toBeDeletedUserId) {
//                return ['ret' => false, 'msg' => ['denied'=> 'you cannot delete users by account, that you have not invited' ]];
//            }
//        }


		$allUserIdsOfSameCompany = $this->usersRepository->getAllUserIdsByCompanyId($userToDeleteCompanyId);

        $data['user_id'] = (int)$userToDelete->getId();
        $data['company_id'] = $this->administrationRepository->getCompanyIdOfUser($email);

        // users that belong only to that company => will be deleted completely
        $data['users_id_list'] = array_merge(
            $this->administrationRepository->getUsersByCompanyIdDelAll($data['company_id']),
            [ $data['user_id'] ]
        );
        $data['objectregister_id_list'] = $this->administrationRepository->getObjectRegisterIdsFromAllTablesByCompanyId($data['company_id']);
        $data['advertorials_id_list']   = $this->administrationRepository->getAdvertorialIdsByCompanyId($data['company_id']);
        $data['seedpool_id_list']       = $this->administrationRepository->getSeedPoolIdsByCompanyId($data['company_id']);
        $data['inboxmonitor_id_list']   = $this->administrationRepository->getInboxMonitorIdsBySeedPoolIds($data['seedpool_id_list']);

        $this->logger->info('$data[\'users_id_list\']', [$data['users_id_list'], __FUNCTION__,__LINE__]);

        // users that belong to that company and others companies => change  their main company to the first one of the other companies they belong to
        $data['list_of_user_ids_to_be_updated'] = $this->administrationRepository->getUsersAndCompanyByCompanyIdForUpdate($data['company_id']);
        $data['updated_users_new_comp'] =  $this->administrationRepository->updateUsersCompany($data['list_of_user_ids_to_be_updated']); // assing new "main" company in users-table, since the old one will be deleted

        $data['campaign_id_list'] = $this->administrationRepository->getCompanyCampaignIds($data['company_id']);

        $data['leads_id_list'] = $this->administrationRepository->getLeadsByCampaignIds($data['campaign_id_list']);

		$companyIntegrationsIds = $this->administrationRepository->getCompanyIntegrationsIds($data['company_id']);
		$campainIntegrationsIds = $this->administrationRepository->getCampaignIntegrationsIds($data['campaign_id_list']);
		$data['integrations_id_list'] = array_unique(array_merge($companyIntegrationsIds, $campainIntegrationsIds));
		$this->logger->info('$data[\'integrations_id_list\']', [$data['integrations_id_list'], __METHOD__, __LINE__]);

        $data['survey_id_list'] = $this->administrationRepository->getSurveyIds($data['company_id']);
        $data['questions_id_list'] = $this->administrationRepository->getQuestionsIdsBySurveyIds($data['survey_id_list']);

        // company
        // $data['deleteIntegrationsMeta'] = (!empty($data['integrations_id_list'])) ? $this->administrationRepository->deleteIntegrationsMeta($data['integrations_id_list']) : 0;
        $data['deleteIntegrations'] = (!empty($data['company_id'])) ? $this->administrationRepository->deleteIntegrationsByIdList($data['integrations_id_list']) : 0; // list

        // campaign
        $data['deleteFraud'] = (!empty($data['campaign_id_list'])) ? $this->administrationRepository->deleteFraud($data['campaign_id_list']) : 0;
        //$data['deleteCampaignParams'] = (!empty($data['campaign_id_list'])) ? $this->administrationRepository->deleteCampaignParams($data['campaign_id_list']) : 0;
        $data['deleteObjectRegisterMeta'] = (!empty($data['campaign_id_list'])) ? $this->administrationRepository->deleteObjectRegisterMeta($data['objectregister_id_list']) : 0;

        // leads
        $data['deleteLeadCustomfieldContent'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteLeadCustomfieldContent($data['leads_id_list']) : 0;
        $data['deleteCustomfields'] = (!empty($data['campaign_id_list'])) ? $this->administrationRepository->deleteCustomfields($data['campaign_id_list']) : 0;
        $data['deleteCustomerJourney'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteCustomerJourney($data['leads_id_list']) : 0;
        $data['deleteLeadAnswerContent'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteLeadAnswerContent($data['leads_id_list']) : 0;

        // survey
        $data['deleteImages'] = (!empty($data['questions_id_list'])) ? $this->administrationRepository->deleteImages($data['questions_id_list']) : 0;
        $data['deleteQuestions'] = (!empty($data['survey_id_list'])) ? $this->administrationRepository->deleteQuestions($data['survey_id_list']) : 0;

        // user
        $data['deleteSecurelog'] = (!empty($data['user_id'])) ? $this->administrationRepository->deleteSecurelog($data['user_id']) : 0;

        // adminstrates-tables
        $data['deleteAdministratesCompanies'] = $this->administrationRepository->deleteAdministratesCompaniesByCompanyId($data['company_id']);
        //$data['deleteAdministratesUsers'] = $this->administrationRepository->deleteAdministratesUsersByCompanyId($data['company_id']);
        $data['deleteAdministratesUsers'] = $this->administrationRepository->deleteAdministratesUsersByInvitedUserIds($data['users_id_list'], $data['company_id']);

        // advertorials, seedpool, inboxmonitor
        $data['deleteAdvertorialsIdList'] = (!empty($data['advertorials_id_list'])) ? $this->administrationRepository->deleteAdvertorials($data['advertorials_id_list']) :0;
        $data['deleteSeedpoolIdList'] = (!empty($data['advertorials_id_list'])) ?     $this->administrationRepository->deleteSeedpools($data['seedpool_id_list']) :0;
        $data['deleteInboxmonitorIdList'] = (!empty($data['advertorials_id_list'])) ? $this->administrationRepository->deleteInboxMonitors($data['inboxmonitor_id_list']):0;

        // main tables
        $data['deleteLeads'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteLeads($data['leads_id_list']) : 0;
        $data['deleteSearchLeads'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteSearchLeads($data['leads_id_list']) : 0; // added 2022-03-09
        $data['deleteSurvey'] = (!empty($data['survey_id_list'])) ? $this->administrationRepository->deleteSurvey($data['survey_id_list']) : 0;
        $data['deleteCampaign'] = (!empty($data['campaign_id_list'])) ? $this->administrationRepository->deleteCampaign($data['campaign_id_list']) : 0;
        $data['deleteExternalDataQueue'] = (!empty($data['company_id'])) ? $this->administrationRepository->deleteExternalDataQueue($data['company_id']) : 0; // added 2022-03-09
        $this->logger->info('delete user', [$data['users_id_list'], __METHOD__,__LINE__]);
        $data['deleteUser'] = (!empty($data['users_id_list'])) ? $this->administrationRepository->deleteUsersByIdList($data['users_id_list']) : 0;
        $this->logger->info('delete company', [$data['company_id'], __METHOD__,__LINE__]);
        $data['deleteCompany'] = (!empty($data['company_id'])) ? $this->administrationRepository->deleteCompany($data['company_id']) : 0;
        $data['deleteObjectregister']= (!empty($data['objectregister_id_list'])) ? $this->administrationRepository->deleteObjectRegister($data['objectregister_id_list']) : 0;

        // added
		foreach ($allUserIdsOfSameCompany as $record) {
			$this->administrationRepository->deleteUser($record['id']);
		}
		$this->administrationRepository->deleteUser($data['user_id']); // just to be save: kill the user that was provided as param

        unset($data['users_id_list'], $data['user_id'], $data['company_id'], $data['campaign_id_list'], $data['leads_id_list'], $data['integrations_id_list'], $data['survey_id_list'], $data['questions_id_list'], $data['objectregister_id_list']);
        $this->logger->info('$data', [$data, __METHOD__,__LINE__]);

        if(!empty($account_no)) {
			$this->campaignServcie->deleteCampaignOnFilesystem($account_no);
		}

		$this->administrationRepository->enableForeignKeyContraintsExplicitly();

        return ['ret' => true, 'msg' => $data];
    }


    /**
     * deleteCompanyById
     * @param int $company_id
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @internal SuperAdmin, MasterAdmin only
     * @author   jsr
     */
    public function deleteCompanyById(int $company_id): array
    {
        $data = [];
        $currentUser = $this->getCurrentUser();
        if((int)$currentUser['company_id'] == $company_id) {
            $numberOf = $this->administratesUsersRepository->getNoUsersCampaignsByCompanyId($company_id);
            if (!empty($numberOf) && ($numberOf['count_users'] > 0 || $numberOf['count_campaigns'] > 0)) {
                return ['ret' => false, 'msg' => 'Deletion denied: You would delete your own record. To delete this company account, delete first all user and all campaigns.'];
            }
        }
        // todo: delete data from tables administrates_companies & administrates_users
        // search $data['user_id']
        $data['users_id_list'] = $this->administrationRepository->getUsersByCompanyId($company_id);
        $data['company_id'] = $company_id;
        $data['campaign_id_list'] = $this->administrationRepository->getCompanyCampaignIds($data['company_id']);
        $data['leads_id_list'] = $this->administrationRepository->getLeadsByCampaignIds($data['campaign_id_list']);
        $data['integrations_id_list'] = $this->administrationRepository->getCompanyIntegrationsIds($data['company_id']);
        $data['survey_id_list'] = $this->administrationRepository->getSurveyIds($data['company_id']);
        $data['questions_id_list'] = $this->administrationRepository->getQuestionsIdsBySurveyIds($data['survey_id_list']);
        $data['advertorials_id_list']   = $this->administrationRepository->getAdvertorialIdsByCompanyId($data['company_id']);
        $data['seedpool_id_list']       = $this->administrationRepository->getSeedPoolIdsByCompanyId($data['company_id']);
        $data['inboxmonitor_id_list']   = $this->administrationRepository->getInboxMonitorIdsBySeedPoolIds($data['seedpool_id_list']);

        // company
        // $data['deleteIntegrationsMeta'] = (!empty($data['integrations_id_list'])) ? $this->administrationRepository->deleteIntegrationsMeta($data['integrations_id_list']) : 0;
        $data['deleteIntegrations'] = (!empty($data['company_id'])) ? $this->administrationRepository->deleteIntegrations($data['company_id']) : 0;

        // campaign
        $data['deleteFraud'] = (!empty($data['campaign_id_list'])) ? $this->administrationRepository->deleteFraud($data['campaign_id_list']) : 0;
        // $data['deleteCampaignParams'] = (!empty($data['campaign_id_list'])) ? $this->administrationRepository->deleteCampaignParams($data['campaign_id_list']) : 0;

        // leads
        $data['deleteLeadCustomfieldContent'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteLeadCustomfieldContent($data['leads_id_list']) : 0;
        $data['deleteCustomfields'] = (!empty($data['campaign_id_list'])) ? $this->administrationRepository->deleteCustomfields($data['campaign_id_list']) : 0;
        $data['deleteCustomerJourney'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteCustomerJourney($data['leads_id_list']) : 0;
        $data['deleteLeadAnswerContent'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteLeadAnswerContent($data['leads_id_list']) : 0;

        // survey
        $data['deleteImages'] = (!empty($data['questions_id_list'])) ? $this->administrationRepository->deleteImages($data['questions_id_list']) : 0;
        $data['deleteQuestions'] = (!empty($data['survey_id_list'])) ? $this->administrationRepository->deleteQuestions($data['survey_id_list']) : 0;

        // user
        $data['deleteSecurelog'] = (!empty($data['users_id_list'])) ? $this->administrationRepository->deleteSecurelogByUsersIdList($data['users_id_list']) : 0;

        // adminstrates-tables
        $data['deleteAdministratesCompanies'] = $this->administrationRepository->deleteAdministratesCompaniesByCompanyId($company_id);
        $data['deleteAdministratesUsers'] = $this->administrationRepository->deleteAdministratesUsersByCompanyId($company_id);

        // advertorials, seedpool, inboxmonitor
        $data['deleteAdvertorialsIdList'] = (!empty($data['advertorials_id_list'])) ? $this->administrationRepository->deleteAdvertorials($data['advertorials_id_list']) :0;
        $data['deleteSeedpoolIdList'] = (!empty($data['advertorials_id_list'])) ?     $this->administrationRepository->deleteSeedpools($data['seedpool_id_list']) :0;
        $data['deleteInboxmonitorIdList'] = (!empty($data['advertorials_id_list'])) ? $this->administrationRepository->deleteInboxMonitors($data['inboxmonitor_id_list']):0;

        // main tables
        $data['deleteLeads'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteLeads($data['leads_id_list']) : 0;
        $data['deleteSearchLeads'] = (!empty($data['leads_id_list'])) ? $this->administrationRepository->deleteSearchLeads($data['leads_id_list']) : 0; // added 2022-03-09
        $data['deleteSurvey'] = (!empty($data['survey_id_list'])) ? $this->administrationRepository->deleteSurvey($data['survey_id_list']) : 0;
        $data['deleteCampaign'] = (!empty($data['campaign_id_list'])) ? $this->administrationRepository->deleteCampaign($data['campaign_id_list']) : 0;
        // $data['deleteUser'] = (!empty($data['users_id_list'])) ? $this->administrationRepository->deleteUsersByIdList($data['users_id_list']) : 0;
        $data['deleteExternalDataQueue'] = (!empty($data['company_id'])) ? $this->administrationRepository->deleteExternalDataQueue($data['company_id']) : 0; // added 2022-03-09
        $data['deleteCompany'] = (!empty($data['company_id'])) ? $this->administrationRepository->deleteCompany($data['company_id']) : 0;
        $data['deleteUser'] = (!empty($data['users_id_list'])) ? $this->administrationRepository->deleteUsersByIdListAndCompanyId($data['users_id_list'], $company_id ) : 0;
        $this->logger->info('$data', [$data, __METHOD__,__LINE__]);
        unset($data['users_id_list'], $data['company_id'], $data['campaign_id_list'], $data['leads_id_list'], $data['integrations_id_list'], $data['survey_id_list'], $data['questions_id_list']);

        return ['ret' => true, 'msg' => $data];
    }


    /**
     * getCountUsersOfEmailsCompanyId
     * @param string $email
     * @return int count total users of emails's CompanyId
     */
    public function getCountUsersOfEmailsCompanyId(string $email): int
    {
        return $this->administrationRepository->getCountUsersOfCompanyId($email);
    }


    /**
     * @internal currently not used
     * @param string $userToDeleteEmail
     * @return bool
     */
    public function checkIfCurrentUserBelongsToEmailsCompany(string $userToDeleteEmail)
    {
        return $this->administrationRepository->currentUserBelongsToEmailsCompany($userToDeleteEmail);
    }


    /**
     * @internal currently not used
     * @return string
     * @internal get UUID for initial company entry after user registration (but not inivitation)
     * @since    WEB-3850
     * @author   jsr
     */
    public function getUUID()
    {
        mt_srand((double)microtime() * 10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            . substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16,
                4) . $hyphen . substr($charid, 20, 12) . chr(125);// "}"
        return $uuid;
    }


    /**
     * @param string $serverProtocol
     * @param string $salutation ,
     * @param string $first_name ,
     * @param string $last_name  ,
     * @param string $invitedUserEmail
     * @param int    $company_id
     * @param string $role
     * @return bool
     * @internal SuperAdmin and MasterAdmin only - a fat piece code => todo: refactor when there is time for
     * @author   jsr
     * @since    WEB-3850
     */
    public function inviteUser(
        string $url,
        string $salutation,
        string $first_name,
        string $last_name,
        string $invitedUserEmail,
        int $to_company_id,
        string $role
    ): array {
        switch ($role) {
            case 'ADMIN':
                $roles = $this->rolesService->getRoleAdmin();
                break;
            case 'USER':
            default:
                $roles = $this->rolesService->getRoleUser();
                break;
        }

        // check if invited user exists and has role on higher or same level as inviting users role
        $invitingUserIsSuperAdmin = $this->isSuperAdmin();
        $invitingUserIsMasterAdmin = $this->isMasterAdmin();

        // early exit here
        if (!$invitingUserIsSuperAdmin && !$invitingUserIsMasterAdmin) {
            return ['status' => false, 'msg' => 'You are not permitted for this action. e' . __LINE__, 'content' => ''];
        }

        $currentUser = $this->getCurrentUser();
        $sanitizedEmail = filter_var(trim($invitedUserEmail), FILTER_SANITIZE_EMAIL);
        $sanitizedFirstName = filter_var(trim($first_name), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $sanitizedLastName = filter_var(trim($last_name), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $sanitizedSalutation = filter_var(trim($salutation), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        $userExists = $this->usersRepository->getUserByEmail($invitedUserEmail);
        $userWasInvitedToCompany = false;

        if (null !== $userExists) { // set $userWasInvitedToCompany
            $invitedUser = $this->administratesUsersRepository->getRecordByInvitedUserId($userExists->getId(), $to_company_id);
            $this->logger->info('$invitedUser', [$invitedUser, $userExists->getId(), __METHOD__,__LINE__]);
            $userWasInvitedToCompany = (null !== $invitedUser) ? ($to_company_id == (int)$invitedUser->getToCompany()->getId()) : false;
        }

        // CASE 1: invited user in users and in administrates_users with same $to_company_id ($userWasInvitedToCompany == true)
        // CASE 2: invited user neither in users nor in administrates_users with $to_company_id ($userWasInvitedToCompany == false)
        // CASE 3: invited user in users but not in administrates_users with different $to_company_id ($userWasInvitedToCompany == false)
        // CASE 4  invited user not in users, but in administrates_users with $to_company_id ($userWasInvitedToCompany == false) ==>  situation not possible!

        if ($userExists !== null && $userWasInvitedToCompany) {
            // CASE 1: invited user in users and in administrates_users
            // simply send invitation link again todo: this code is partially duplicate, refactor when there is time for!
            $newInitalPw = $this->cryptoService->simplePwGen(25);

            $emailEncrypted = $this->cryptoService->simpleEncrypt($sanitizedEmail);
            $this->logger->info('$sanitizedEmail', [$sanitizedEmail, $emailEncrypted,  __METHOD__,__LINE__]);
            $activationUrl = $url . $emailEncrypted;

            $this->logger->info('REINVITATION-ACTIVATION-URL: ', [$activationUrl, __METHOD__, __LINE__]);

            $this->usersRepository->updateUserPassword($newInitalPw, $userExists->getId());

            $this->cacheService->addOrOverwrite($this->cacheService::USERINVITATION . $sanitizedEmail, $userExists->toArray(),
                $this->cacheService::EIGHT_HOURS);
			// SENDMAIL
            $swiftMailerReturned = $this->swiftMailerService->sendInvitationLink($userExists->getEmail(), $newInitalPw, $userExists->getSalutation(),
                $userExists->getLastName(), $activationUrl, $currentUser);

            if ($swiftMailerReturned['ret'] > 0) {
                $administratesUsers = $this->administratesUsersRepository->getRecordByInvitedUserId($userExists->getId(), $to_company_id);

                return [
                    'status' => true,
                    'msg' => "Success. User {$sanitizedEmail} invited again. i" . __LINE__,
                    'content' => ($administratesUsers !== null) ? $administratesUsers : [],
                ];
            } else {
                $this->cacheService->del($this->cacheService::USERINVITATION . $sanitizedEmail);
                return [
                    'status' => true,
                    'msg' => "Error. Reinvitation of user {$sanitizedEmail} falied. e" . __LINE__,
                    'content' => [],
                ];
            }

        } elseif ($userExists === null && !$userWasInvitedToCompany) {
            // CASE 2: invited user neither in users nor in administrates_users
            $initalPassword = $this->cryptoService->simplePwGen(25);
            $company = $this->companyRepository->getCompanyById($to_company_id);

            $newUser = $this->usersRepository->createNewUser([
                'salutation' => $sanitizedSalutation,
                'f_name' => $sanitizedFirstName,
                'l_name' => $sanitizedLastName,
                'email' => $sanitizedEmail,
                'pass' => $initalPassword,
				'permission' => 1,
            ], $company, $roles);

            if ($newUser instanceof Users) {

                $administratesUsers = $this->administratesUsersRepository->addRecord($currentUser['user_id'], $newUser->getId(), $newUser->getCompany()->getId(),
                    $newUser->getRolesAsString());

                if ($administratesUsers !== null) {
                    $emailEncrypted = $this->cryptoService->simpleEncrypt($sanitizedEmail);
                    $this->logger->info('$sanitizedEmail', [$sanitizedEmail, $emailEncrypted, __METHOD__,__LINE__]);
                    $activationUrl = $url . $emailEncrypted;

                    $this->logger->info('INVITATION-ACTIVATION-URL: ', [$activationUrl, __METHOD__, __LINE__]);

                    $this->cacheService->addOrOverwrite($this->cacheService::USERINVITATION . $sanitizedEmail, $newUser->toArray(),
                        $this->cacheService::EIGHT_HOURS);

                    $swiftMailerReturned = $this->swiftMailerService->sendInvitationLink($newUser->getEmail(), $initalPassword, $newUser->getSalutation(),
                        $newUser->getLastName(), $activationUrl, $currentUser);

                    if ($swiftMailerReturned['ret'] > 0) {
                        return [
                            'status' => true,
                            'msg' => "Success. User {$sanitizedEmail} created and invited. i" . __LINE__,
                            'content' => $administratesUsers,
                        ];
                    } else {
                        $this->cacheService->del($this->cacheService::USERINVITATION . $sanitizedEmail);
                        return [
                            'status' => false,
                            'msg' => "Error. User {$sanitizedEmail} was created, but the sending of invitation email failed. Please contact support. e" . __LINE__,
                            'content' => $administratesUsers,
                        ];
                    }
                } else {
                    return ['status' => false, 'msg' => 'Failed to create user. e' . __LINE__, 'content' => ''];
                }
            } else {
                return ['status' => false, 'msg' => 'Failed to create user. e' . __LINE__, 'content' => ''];
            }
        } elseif($userExists !== null && !$userWasInvitedToCompany) {
            // CASE 3: invited user in users but not in administrates_users with that $to_company_id ($userWasInvitedToCompany == false)
            // simply add user to administrates_users and don't send invitation email but notification email without activation link and so on, since user is registered already
            $administratesUsers = $this->administratesUsersRepository->addRecord($currentUser['user_id'], $userExists->getId(), $to_company_id,
                $this->rolesService->toStringForDatabase($roles));
            $company = $this->companyRepository->getCompanyById($to_company_id);

            // what 's the users main (login/cache) company? => by default it is the one he was assigned to in users table => see users.company_id
            if ($administratesUsers !== null) {
                // send notification email
				// SENDMAIL
                $swiftMailerReturned = $this->swiftMailerService->sendInvitationNotification($userExists->getEmail(), $userExists->getSalutation(),
                    $userExists->getLastName(), $company->getName(), $currentUser);
                if ($swiftMailerReturned['ret'] > 0) {
                    return [
                        'status' => true,
                        'msg' => "Success. User {$sanitizedEmail} added as member of {$company->getName()} and notified. i" . __LINE__,
                        'content' => $administratesUsers,
                    ];
                } else {
                    $this->cacheService->del($this->cacheService::USERINVITATION . $sanitizedEmail);
                    return [
                        'status' => false,
                        'msg' => "Error. User {$sanitizedEmail} was added, but the sending of notification email failed. e" . __LINE__,
                        'content' => $administratesUsers,
                    ];
                }
            } else {
                return ['status' => false, 'msg' => "Failed to add user to {$company->getName()}. e" . __LINE__, 'content' => ''];
            }
        }
        else { // CASE 4: user not in users, but as invited user in administrates_users? Impossible!
            return [
                'status' => false,
                'msg' => "Error. This case should not occur. Please inform our support. e" . __LINE__,
                'content' => '',
            ];
        }
    }


    /**
     * @param int      $selectedCompany
     * @param int|null $administratedByUserId
     * @return array
     * @internal determines if user is superAdmin to provide superAdmins ID to AdministratesUsersRepository::getAdministratedUsersForAnyAdmin
     */
    public function getAdministratedUsersForJson(int $selectedCompany, int $administratedByUserId=null):array
    {
        $record = [];

        $currentUser = $this->getCurrentUser();
        if($currentUser['is_super_admin']) {
            $superAdminUserId = $currentUser['user_id'];
        } else {
            $superAdminUserId = null;
        }

        $administratedUsers = $this->administratesUsersRepository->getAdministratedUsersForAdmin($selectedCompany, $administratedByUserId, $superAdminUserId);
        return $administratedUsers;

        //todo: remove block
        /*
        $this->logger->info('$administratedUsers', [$administratedUsers, __METHOD__,__LINE__]);


        if (!empty($administratedUsers)) {
            $invited_user = ['invited_user_id', 'rolesAsString', 'email', 'firstName', 'lastName', 'status', 'failedLogins'];
            $to_company_id = ['to_company_id', 'company_name'];

            foreach ($administratedUsers as $key => $value) {
                if ($key === 'id') {
                    $record['id'] = $value;
                } elseif (in_array($key, $invited_user)) {
                    $record['invited_user_id'][($key==='invited_user_id') ? 'id' : $key] = $value;
                } elseif (in_array($key, $to_company_id)) {
                    $record['to_company_id'][$key] = $value;
                } else {
                    ;// do nothing
                }
            }
            $this->logger->info('$administratedUsers $record', [$record, __METHOD__,__LINE__]);
            return $record;
        }
        return $record;*/
    }


    /**
     * @param $user_id
     * @param $invited_user_id
     * @param $to_company_id
     * @return array
     */
    public function getAdministratedUsersSingleRecord($user_id, $invited_user_id, $to_company_id):array
    {
        return $this->administratesUsersRepository->getAdministratedUsersSingleRecord($user_id, $invited_user_id, $to_company_id);
    }


    /**
     * @param string $email
     * @param int    $company_id
     * @param array  $roles
     * @return array  [to_company_id.'-'.invited_user_id] [id,user_id,invited_user_id,to_company_id,invited_user_roles,invited_user_email,invited_user_first_name,invited_user_last_name,invited_user_status,invited_user_failed_logins,invited_user_company_name]
     * @internal SuperAdmin and MasterAdmin only
     * @author   jsr
     * @since    WEB-3850
     */
    public function updateUser(array $userData): array
    {
        /*
        0: Object { name: "manageusers_action", value: "edit_user" }
        1: Object { name: "selected_company", value: "45" }
        2: Object { name: "selected_user", value: "MTMz" }
        3: Object { name: "salutation", value: "Mr." }
        4: Object { name: "first_name", value: "Öttö" }
        5: Object { name: "last_name", value: "Wälkes" }
        6: Object { name: "email", value: "otto.walkes@emden.de" }
        7: Object { name: "role", value: "USER" }
        8: Object { name: "to_company", value: "NTI=" }
        9: Object { name: "token", value: "MtKVN9gDSk7LlDq7e0AXXEbeYPq5NPDlbXNX7ZEJSto" }
        */

        $currentUser = $this->getCurrentUser();
        $userData['user_id'] = $currentUser['user_id']; // add
        $userData['selected_user'] = base64_decode($userData['selected_user']);
        $userData['to_company'] = base64_decode($userData['to_company']);

        switch ($userData['role']){
            case 'SUPER':
                $userData['role'] = $this->rolesService->getRoleSuperAdmin();
                break;

            case 'MASTER':
                $userData['role'] = $this->rolesService->getRoleMasterAdmin();
                break;

            case 'ADMIN':
                $userData['role'] = $this->rolesService->getRoleAdmin();
                break;

            case 'USER':
            default:
                $userData['role'] = $this->rolesService->getRoleUser();
        }

        if ($this->isSuperAdmin() || $this->isMasterAdmin()) {
            $user = $this->usersRepository->updateUserById($userData['selected_user'], $userData);
            if($user instanceof Users) {

                $userData['user_id'] = ($currentUser['is_super_admin'] === true) ? -1 : $userData['user_id']; // -1 for super admin
                $ret = $this->administratesUsersRepository->updateUserRole($userData);

                return $ret;
            }
        }

        return [];
    }


    /**
     * @param string $email
     * @param int    $company_id
     * @return bool
     * @internal SuperAdmin and MasterAdmin only
     * @author   jsr
     * @since    WEB-3850
     */
    public function deleteUserByIdAndCompanyInUsersAndAdministratesUsers(string $email, int $company_id): array
    {
    	$error = [
			'status' => false,
			'msg' => "Error. User {$email} was not deleted from company {$company_id}." ,
			'content' => [],
		];

		$this->logger->info('PARAMS', [$email, $company_id, __METHOD__, __LINE__]);
        $user = $this->usersRepository->getUserByEmail($email);

        if($user === null ) {
			$this->logger->info('$user is null', [$email, __METHOD__, __LINE__]);
			return $error;
		}
		$this->logger->info('$user => ', [$user->toString(), $company_id, __METHOD__, __LINE__]);

        $administratesUsers = $this->administratesUsersRepository->getRecordByInvitedUserId($user->getId(), $company_id);
        if ($this->isSuperAdmin() || $this->isMasterAdmin()) {
            $this->usersRepository->deleteUserByIdAndCompanyInUsersAndAdministratesUsers($user->getId(), $company_id);

            return [
                'status' => true,
                'msg' => "Success. User {$email} was deleted from company {$user->getCompany()->getName()}.",
                'content' => ($administratesUsers !== null) ? $administratesUsers : [],
            ];
        } else {
            return $error;
        }
    }


    /**
     * @param string $encodedUrl
     * @return string|null
     * @author Pradeep
     */
    public function decodeRedirectParamFromUrl(string $encodedUrl): ?string
    {
        if (!isset($encodedUrl) || empty($encodedUrl)) {
            return null;
        }
        return base64_decode($encodedUrl);
    }

    /**
     * @param string $mioField
     * @param string $mioDataType
     * @return string[]
     * @author Pradeep
     * @internal Generate eMIO fieldname and datatype for a given MIOfieldname and datatype.
     * Function does not auto create the field in eMIO,
     * Function only returns a compatible string by appending tag 'MIO_' to MIOFieldName.
     */
    public function generateEMIOFieldForMIOField(string $mioField, string $mioDataType): array
    {
        $eMIOField = [
            'name' => "",
            'datatype' => "",
        ];
        if (empty($mioField) || trim($mioField) === '') {
            return $eMIOField;
        }
        $eMIOField[ "name" ] = "MIO_" . strtolower(trim($mioField));
        $eMIOField[ "datatype" ] = $this->aioServices->getSupportedEMIODataType($mioDataType, $mioField);
        return $eMIOField;
    }
}
