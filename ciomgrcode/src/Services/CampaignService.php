<?php


namespace App\Services;

use App\Repository\CampaignRepository;
use App\Repository\ContactRepository;
use App\Repository\CompanyRepository;
use App\Repository\CustomfieldsRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Types\PageWizardTypes;
use App\Types\ProcesshookTypes;
use App\Types\StatusdefTypes;
use DOMDocument;
use DOMXPath;
use Exception;
use FilesystemIterator;
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\Exception\AuthenticationException;
use GeoIp2\Exception\GeoIp2Exception;
use GeoIp2\Exception\HttpException;
use GeoIp2\Exception\InvalidRequestException;
use GeoIp2\Exception\OutOfQueriesException;
use http\Client\Response;
use JsonException;
use Psr\Log\LoggerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Mime\MimeTypes;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use \GeoIp2\WebService\Client as GeoIp2Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ZipArchive;

/**
 * @property ObjectRegisterMetaRepository objectRegisterMetaRepository
 * @property PageWizardTypes pageWizardTypes
 * @property AuthenticationUtils authenticationUtils
 * @property AuthenticationService authenticationService
 * @property CampaignRepository campaignRepository
 * @property ObjectRegisterRepository objectRegisterRepository
 * @property ContactRepository contactRepository
 * @property CompanyRepository companyRepository
 * @property CustomfieldsRepository customFieldsRepository
 */
class CampaignService
{

    private $logger;
    private $geoIpClient;
    private const accountnumber = "account_no";
    private $leadsRepository;
    protected $campaignOrganizer;
    protected $webhookOrganizer;
    /**
     * @var MIOStandardFieldRepository
     * @author Pradeep
     */
    private MIOStandardFieldRepository $mioStandardFieldRepository;
    private ProcesshookService $processhookService;


    public function __construct(
        LoggerInterface              $logger,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        ObjectRegisterRepository     $objectRegisterRepository,
        PageWizardTypes              $pageWizardTypes,
        AuthenticationUtils          $authenticationUtils,
        AuthenticationService        $authenticationService,
        ContactRepository            $contactRepository,
        CampaignRepository           $campaignRepository,
        CompanyRepository            $companyRepository,
        CustomfieldsRepository       $customfieldsRepository,
        MIOStandardFieldRepository   $mioStandardFieldRepository,
        ProcesshookService           $processhookService
    )
    {
        $this->logger = $logger;
        $this->geoIpClient = new GeoIp2Client($_ENV['GeoIpAccountId'], $_ENV['GeoIpLicenceKey'], ['en']);
        $this->campaignOrganizer = $_ENV['CAMPAIGN_SERVICE'];
        $this->webhookOrganizer = $_ENV['WEBHOOK_SERVICE'];
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->pageWizardTypes = $pageWizardTypes;
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->campaignRepository = $campaignRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->contactRepository = $contactRepository;
        $this->companyRepository = $companyRepository;
        $this->customFieldsRepository = $customfieldsRepository;
        $this->mioStandardFieldRepository = $mioStandardFieldRepository;
        $this->processhookService = $processhookService;
    }


    /**
     * appendLinksForCampaigns function
     *
     * @param array $campaigns
     * @param string $useCaseType
     * @param string|null $pageStatus
     * @return array
     * @author aki
     */
    public function appendLinksForCampaigns(
        array   $campaigns,
        string  $useCaseType,
        ?string $pageStatus
    ): array
    {
        $result = array();

        if (empty($campaigns)) {
            return $result;
        }


        foreach ($campaigns as $campaign) {
            if ((int)$campaign['id'] <= 0) {
                continue;
            }
            $campaign['preview_url'] = $this->campaignRepository->getCampaignDetailsById($campaign['id'])['url'];
            $campaign['delete_url'] = $this->generateCampaignLinks($campaign['id'], 'delete', $useCaseType);
            $campaign['copy_url'] = $this->generateCampaignLinks($campaign['id'], 'copy', $useCaseType);
            $campaign['update_url'] = $this->generateCampaignLinks($campaign['id'], 'update', $useCaseType);
            $campaign['revise_url'] = $this->generateCampaignLinks($campaign['id'], 'revise', $useCaseType);
            $campaign['activate_url'] = $this->generateCampaignLinks($campaign['id'], 'activate', $useCaseType);
            $campaign['archive_url'] = $this->generateCampaignLinks($campaign['id'], 'archive', $useCaseType);
            $campaign['reactivate_url'] = $this->generateCampaignLinks($campaign['id'], 'reactivate', $useCaseType);
            $campaign['start_wizard'] = $this->generateCampaignLinks($campaign['id'], 'startwizard', $useCaseType);
            $campaign['move_url'] = $this->generateCampaignLinks($campaign['id'], 'move', $useCaseType);
            $campaign['viewSettings_url'] = $this->generateCampaignLinks($campaign['id'], 'viewSettings', $useCaseType);
            $campaign['downloadCode_url'] = $this->generateCampaignLinks($campaign['id'], 'downloadCode', $useCaseType);
            $campaign['copywebhook'] = $this->generateCampaignLinks($campaign['id'], 'copy', $useCaseType);
            $campaign['draft_url'] = $campaign['preview_url'] . "/versions/" . $campaign['version'];
            if (!is_null($pageStatus)) {
                $campaign['download_url'] = $this->generateDownloadUrl($campaign['id'], $pageStatus);
                $campaign['upload_url'] = "page/upload/" . $campaign['id'] . "/" . $pageStatus;
            }
            if (!empty($campaign)) {
                $result[] = $campaign;
            }
        }
        return $result;
    }


    /**
     * generateCampaignLinks function
     *
     * @param integer $campaign_id
     * @param string $operation
     * @param string $useCaseType
     * @return string
     * @author aki
     */
    private function generateCampaignLinks(int $campaign_id, string $operation, string $useCaseType): string
    {
        $url = '';

        if ($campaign_id <= 0) {
            return $url;
        }
        // Generate URL for given Campaign Operation.
        if ($useCaseType === 'webhooks') {
            if ($operation === 'settings') {
                $str_url = '/account/' . $useCaseType . '/' . $campaign_id . '/' . $operation . '/' . 'general/edit';
            } elseif ($operation === 'viewSettings') {
                $str_url = '/account/' . $useCaseType . '/' . $campaign_id . '/settings/' . 'general/view';
            } else {
                $str_url = '/account/' . $useCaseType . '/' . $campaign_id . '/' . $operation;
            }
        } else {
            if ($operation === 'settings') {
                $str_url = '/account/' . $useCaseType . '/' . $campaign_id . '/pageflow/edit';
            } elseif ($operation === 'viewSettings') {
                $str_url = '/account/' . $useCaseType . '/' . $campaign_id . '/pageflow/view';
            } else {
                $str_url = '/account/' . $useCaseType . '/' . $campaign_id . '/' . $operation;
            }
        }
        // Validate the generated URL.
        if (filter_var($str_url)) {
            $url = $str_url;
        }

        return $url;
    }


    /**
     * @param string $request_as_str
     * @return array
     * @author pva
     */
    public function convertRequestToArray(string $request_as_str): array
    {
        $req_arr = [];
        if (empty($request_as_str)) {
            return $req_arr;
        }
        $content_arr = explode('&', $request_as_str);
        foreach ($content_arr as $c) {
            if (empty($c)) {
                continue;
            }
            $c_arr = explode('=', urldecode($c));
            if ($c_arr[0] !== 'token') {
                $req_arr[$c_arr[0]] = $c_arr[1];
            }
        }

        return $req_arr;
    }


    /**
     *
     *setMappingTable
     * @param $c_set
     * @return array
     * @author aki
     */
    public function setMappingTable($c_set): array
    {
        if (empty($c_set['mappingTable'])) {
            $c_set['mappingTable'] = '';
        }
        return $c_set;
    }


    /**
     * @param string $microService
     * @param string $camp_name
     * @param int $version
     * @param string $company
     * @param string $campaignSettings
     * @return bool
     */
    public function activateCampaignInServer(
        string $microService,
        string $camp_name,
        int    $version,
        string $company,
        string $campaignSettings
    ): bool
    {
        $status = true;
        $http_client = HttpClient::create(['http_version' => '2.0']);
        if (empty($camp_name) || empty($company) || $version <= 0) {
            return $status;
        }
        try {
            $http_client_resp = $http_client->request('POST', $microService . '/activate', [
                'body' => [
                    'campaign_name' => $camp_name,
                    'company' => $company,
                    'version' => $version,
                    'campaignSettings' => $campaignSettings,
                ],
            ]);
            $status = $http_client_resp->getContent();
        } catch (TransportExceptionInterface|ClientExceptionInterface|
        RedirectionExceptionInterface|ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $microService
     * @param string $camp_name
     * @param string $company
     * @param string $version_old
     * @param string $version_new
     * @param int $camp_id
     * @param int $company_id
     * @return bool
     */
    public function reviseCampaignInServer(
        string $microService,
        string $camp_name,
        string $version_old,
        string $version_new,
               $camp_id,
               $company_id
    ): bool
    {
        $status = true;
        $http_client = HttpClient::create(['http_version' => '2.0']);
        //make new token for revised campaign

        if (empty($camp_name) || $version_old <= 0 || $version_new <= 0) {
            return $status;
        }
        $accountName = $this->companyRepository->getCompanyDetailById($company_id)[self::accountnumber];
        $token = $this->createCampaignToken($camp_id, $company_id);
        try {
            $http_client_resp = $http_client->request('POST', $microService . '/revise', [
                'body' => [
                    'campaign_name' => $camp_name,
                    'company' => $accountName,
                    'version_old' => $version_old,
                    'version_new' => $version_new,
                    'token' => $token,
                ],
            ]);
            $status = $http_client_resp->getContent();
        } catch (TransportExceptionInterface|ClientExceptionInterface|
        RedirectionExceptionInterface|ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param int $campaign_id
     * @param int $company_id
     * @return string
     * @author Pradeep
     */
    private function createCampaignToken(int $campaign_id, int $company_id): string
    {
        $salt = str_rot13(base64_encode('cio'));
        $pepper = str_rot13(base64_encode('treaction'));
        if ($company_id <= 0 || $campaign_id <= 0) {
            return '';
        }
        $encrypt_campaign_id = str_rot13(base64_encode('campaign_id=' . decoct($campaign_id)));
        $encrypt_company_id = str_rot13(base64_encode('company_id=' . decoct($company_id)));

        $encrypt_str = str_rot13($salt . '|' . $encrypt_campaign_id . '|' . $encrypt_company_id . '|' . $pepper);
        $encrypt_md5 = md5($encrypt_str);

        return $encrypt_str . '|' . $encrypt_md5;
    }


    /**
     * @param string $microService
     * @param string $campaign_name
     * @param int $campaign_id
     * @param int $company_id
     * @param string $useCaseType
     * @param string $company_name
     * @return bool
     * @author pva
     * @comment company name is now account number
     */
    public function createCampaignInServer(
        string $microService,
        string $campaign_name,
        int    $campaign_id,
        int    $company_id,
        string $useCaseType,
        string $company_name
    ): bool
    {
        $status = false;
        $accountNumber = $this->companyRepository->getCompanyDetailById($company_id)[self::accountnumber];
        $http_client = HttpClient::create(['http_version' => '2.0']);
        $token = $this->createCampaignToken($campaign_id, $company_id);
        $this->logger->info("createCampaignInServer ", [
            ['$campaign_name' => $campaign_name,
                '$campaign_id' => $campaign_id,
                '$company_id' => $company_id,
                '$company_name' => $accountNumber,
                '$token' => $token,]
            , __METHOD__, __LINE__]);
        if (empty($campaign_name) || $company_id <= 0 || empty($token)) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return $status;
        }

        try {
            $http_client_resp = $http_client->request('POST', $microService . '/create', [
                'body' => [
                    'campaign_name' => $campaign_name,
                    'company' => $accountNumber,
                    'token' => $token,
                    'useCaseType' => $useCaseType,
                ],
            ]);
            $this->logger->info('http status code ' . $http_client_resp->getStatusCode());
            $this->logger->info($http_client_resp->getContent(), [__METHOD__, __LINE__]);
            $status = $http_client_resp->getContent();
            $this->logger->info('crate campaign status ' . $status, [__METHOD__, __LINE__]);
        } catch (TransportExceptionInterface|ClientExceptionInterface|
        RedirectionExceptionInterface|ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $microService
     * @param string $campaign_name
     * @param string $company
     * @param string $version
     * @param string $splittest_name
     * @return bool
     * @author Pradeep
     */
    public function createSplittestFolder(
        string $microService,
        string $campaign_name,
        string $company,
        string $version,
        string $splittest_name
    ): bool
    {
        $status = false;
        $http_client = HttpClient::create(['http_version' => '2.0']);
        if (empty($campaign_name) || empty($company) || empty($splittest_name) || empty($version)) {
            return $status;
        }
        try {
            $http_client_resp = $http_client->request('POST', $microService . '/create_splittest', [
                'body' => [
                    'campaign_name' => $campaign_name,
                    'company' => $company,
                    'version' => $version,
                    'splittest_name' => $splittest_name,
                ],
            ]);
            $status = $http_client_resp->getContent();
        } catch (TransportExceptionInterface|ClientExceptionInterface|
        RedirectionExceptionInterface|ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $microService
     * @param string $old_campaign_name
     * @param string $new_campaign_name
     * @param string $old_campaign_version
     * @param int $campaign_id
     * @param int $company_id
     * @param string $company_name
     * @return bool
     * @author pva
     */
    public function copyCampaignInServer(
        string $microService,
        string $old_campaign_name,
        string $new_campaign_name,
        string $old_campaign_version,
        int    $campaign_id,
        int    $company_id,
        string $company_name
    ): bool
    {
        $status = false;
        $http_client = HttpClient::create(['http_version' => '2.0']);
        $token = $this->createCampaignToken($campaign_id, $company_id);
        if (empty($old_campaign_name) || empty($token) || empty($new_campaign_name) || empty($old_campaign_version) || empty($company_name)) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return $status;
        }
        try {
            $http_client_resp = $http_client->request('POST', $microService . '/copy', [
                'body' => [
                    'campaign_old' => $old_campaign_name,
                    'version' => $old_campaign_version,
                    'campaign_new' => $new_campaign_name,
                    'company' => $company_name,
                    'token' => $token,
                ],
            ]);
            $status = $http_client_resp->getContent();
        } catch (TransportExceptionInterface|
        ClientExceptionInterface|
        RedirectionExceptionInterface|
        ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    /**
     * @param string $campaign_name
     * @return bool
     * @author Pradeep
     */
    public function isCampaignNameValid(string $campaign_name): bool
    {
        $status = false;
        if (empty($campaign_name)) {
            $status = false;
        }
        if ((false !== strpos($campaign_name, '-')) || ($campaign_name === trim($campaign_name) && strpos($campaign_name, ' ') !== false)) {
            return $status;
        }
        $status = true;
        return $status;
    }


    /**
     * @param string $microService
     * @param string $campaign_name
     * @param string $company_name
     * @param string $version
     * @param array $fc_pages
     * @return bool
     * @author Pradeep
     */
    public function createFlowControlPagesInServer(
        string $microService,
        string $campaign_name,
        string $company_name,
        string $version,
        array  $fc_pages
    ): bool
    {
        $http_client = HttpClient::create(['http_version' => '2.0']);
        if (empty($campaign_name) || empty($company_name) || empty($fc_pages) || empty($version)) {
            return false;
        }
        try {
            $http_client_resp = $http_client->request('POST', $microService . '/createFlowControl', [
                'body' => [
                    'campaign_name' => $campaign_name,
                    'company' => $company_name,
                    'version' => $version,
                    'pages' => $fc_pages,
                ],
            ]);
            $status = $http_client_resp->getContent();
        } catch (TransportExceptionInterface|
        ClientExceptionInterface|
        RedirectionExceptionInterface|
        ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param array $mio_details
     * @param array $survey_questions
     * @return bool
     * @author Pradeep
     */
    public function createSurveyAsMIOCustomFields(array $mio_details, array $survey_questions)
    {
        $status = false;
        $http_client = HttpClient::create(['http_version' => '2.0']);
        if (empty($mio_details) || empty($survey_questions)) {
            return false;
        }
        // base 64 text
        $base64_text = base64_encode(json_encode([
            'custom_fields' => $survey_questions,
            'api_key' => $mio_details['apikey'],
        ], JSON_THROW_ON_ERROR, 512));

        try {
            $http_client_resp = $http_client->request('POST', $_ENV['MSMIO'] . 'create_custom_fields', [
                'body' => [
                    'text' => $base64_text,
                ],
            ]);
            $status = $http_client_resp->getContent();
            $this->logger->info('status of Survey ' . $status, [__METHOD__, __LINE__]);
        } catch (TransportExceptionInterface|
        ClientExceptionInterface|
        RedirectionExceptionInterface|
        ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param int $campaignId
     * @param int $projectObjectRegisterID
     * @param string $metaKey
     * @return array
     * @throws \Doctrine\DBAL\Exception
     * @author  Aki
     * @comment This method is to take settings from database and restructure the data
     *
     */
    public function getCampaignSettings(
        int    $campaignId,
        int    $projectObjectRegisterID,
        string $metaKey
    ): array
    {
        $settings = [];
        $campStructuredSettings = [];
        $survey = [];
        if ($campaignId <= 0) {
            return $campStructuredSettings;
        }
        $objectRegisterId = $this->campaignRepository->getobjectregisterIdWithCampaignId($campaignId);

        try {
            $settings = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($objectRegisterId, $metaKey);
            $projectSettings = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($projectObjectRegisterID,
                ObjectRegisterMetaRepository::MetaKeyProject);
            $settings['tracker'] = $projectSettings['tracker'];
            //add matomo auth token to settings
            $settings['tracker']['matomo_auth_token'] = $_ENV['MatomoAuthToken'];
            $settings['redirect'] = $projectSettings['advancedsettings']['redirect_after_registration'];
            // TODO: find new the method and replace calls to getCampaignSettings
            // jsr: added 2021-03-10, since baby sends data without apikey to AIO

            if (($companyObjectRegisterId = $this->objectRegisterRepository->getCompanyObjectRegisterIdByProjectObjectRegisterID($projectObjectRegisterID)) > 0) {
                $apikey = $this->objectRegisterMetaRepository->getAccountAPIKey($companyObjectRegisterId);
            } else {
                $apikey = "ERROR FETCHTING APIKEY " . __FUNCTION__ . ", " . __LINE__;
            }
            $settings['apikey'] = $apikey;
            $this->logger->info('GET BABIES APIKEY', [$projectObjectRegisterID, $companyObjectRegisterId, $apikey, __METHOD__, __LINE__]);

            #check for survey
            $surveyDetails = $this->campaignRepository->fetchSurveyForCampaign($campaignId);
            if (!empty($surveyDetails) && isset($surveyDetails['id']) && (int)$surveyDetails['id'] > 0) {
                $survey['survey'] = $surveyDetails;
                $survey['questions'] = $this->campaignRepository->fetchSurveyQuestionsForCampaign($campaignId);
                $settings['survey'] = $survey;
            }
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $settings;
    }


    /**@param $siteName
     * @param $siteUrl
     * @return bool
     * @author  Aki
     * @comment This method is to take settings from database and restructure the data
     */
    public function addCampaignToMatomo(string $siteName, string $siteUrl, string $groupName): bool
    {
        $status = false;
        $http_client = HttpClient::create(['http_version' => '2.0']);
        if (empty($siteName) || empty($siteUrl)) {
            return $status;
        }
        $matomoAuthToken = $_ENV['MatomoAuthToken'];
        try {
            $http_client_resp = $http_client->request('POST',
                $_ENV['MATOMO_URL'] . '/' . '?module=API' . '&method=SitesManager.addSite' . '&siteName="' . $siteName . '"&urls=' . $siteUrl . '&group="' . $groupName . '"&format=JSON' . '&token_auth=' . $matomoAuthToken);
            $matomo_id = $http_client_resp->getContent();
            if (!empty($matomo_id)) {
                $status = true;
            }
        } catch (TransportExceptionInterface|
        ClientExceptionInterface|
        RedirectionExceptionInterface|
        ServerExceptionInterface $e) {
            return $status;
        }
        return $status;
    }


    /**
     * @param array $campaigns ,
     * @param string $campaignType
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @author Aki
     * @comment
     */
    public function addConversionsValueToCampaigns(
        array  $campaigns,
        string $campaignType
    ): array
    {
        $result = array();

        if (empty($campaigns)) {
            return $result;
        }

        foreach ($campaigns as $campaign) {
            if ((int)$campaign['id'] <= 0) {
                continue;
            }
            $objRegDetails = $this->objectRegisterRepository->getObjectregisterById($campaign['objectregister_id']);

            if ($objRegDetails === null) {
                continue;
            }
            $objectUniqueId = $objRegDetails->getObjectUniqueId();
            /*
            if ($campaignType === ObjectRegisterMetaRepository::MetaKeyPage) {
                $objectRegisterId = $objRegDetails->getId();
                $campaign['splittest'] = $this->getSplittestStatus($objectRegisterId);

            }*/

            if (isset($campaign['objectregister_id'])) {
                $campaign['conversions'] = $this->contactRepository->getCountOfLeadsInCampaign($objectUniqueId);
            } else {
                $campaign['conversions'] = 0;
            }
            if (!empty($campaign)) {
                $result[] = $campaign;
            }
        }
        return $result;
    }


    /**
     * @param int $objectRegisterId
     * @return array|null
     * @author aki
     */
    private function getSplittestStatus(int $objectRegisterId): ?array
    {
        $pageSettings = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($objectRegisterId,
            ObjectRegisterMetaRepository::MetaKeyPage);
        if (array_key_exists('status', $pageSettings['splittest']) && $pageSettings['splittest']['status'] == 'on') {
            return $pageSettings['splittest'];
        }
        return null;
    }


    /**
     *
     * @param array $campaigns
     * @return array
     * @author aki
     */
    public function appendLinksForWebhooks(array $campaigns): array
    {
        $result = array();

        if (empty($campaigns)) {
            return $result;
        }

        foreach ($campaigns as $campaign) {
            if ((int)$campaign['id'] <= 0) {
                continue;
            }
            $campaign['preview_url'] = $this->campaignRepository->getCampaignDetailsById($campaign['id'])['url'];
            $campaign['delete_url'] = $this->generateCampaignLinks($campaign['id'], 'delete');
            $campaign['copy_url'] = $this->generateCampaignLinks($campaign['id'], 'copy');
            $campaign['update_url'] = $this->generateCampaignLinks($campaign['id'], 'update');
            $campaign['revise_url'] = $this->generateCampaignLinks($campaign['id'], 'revise');
            $campaign['activate_url'] = $this->generateCampaignLinks($campaign['id'], 'activate');
            $campaign['archive_url'] = $this->generateCampaignLinks($campaign['id'], 'archive');
            $campaign['reactivate_url'] = $this->generateCampaignLinks($campaign['id'], 'reactivate');
            $campaign['edit_settings'] = $this->generateCampaignLinks($campaign['id'], 'settings');
            $campaign['move_url'] = $this->generateCampaignLinks($campaign['id'], 'move');
            $campaign['viewSettings_url'] = $this->generateCampaignLinks($campaign['id'], 'viewSettings');
            if (!empty($campaign)) {
                $result[] = $campaign;
            }
        }
        return $result;
    }


    /**
     * @param array $campaigns ,
     * @return array
     * @author Aki
     * @comment deprecated
     */
    public function addObjectUniqueValueToCampaigns(array $campaigns): array
    {
        $result = array();

        if (empty($campaigns)) {
            return $result;
        }

        foreach ($campaigns as $campaign) {
            if (isset($campaign['objectregister_id']) && $campaign['objectregister_id'] !== '0') {
                $object_unique_id = $this->campaignRepository->getUniqueObjectForCampaign($campaign['objectregister_id']);
                $campaign['object_unique_id'] = $object_unique_id;
            } else {
                $campaign['object_unique_id'] = 'UOIDxxxxxxxxxx';
            }
            //PradeepComment: remove while pushing
            if (!empty($campaign)) {
                $result[] = $campaign;
            }
        }
        return $result;
    }


    /**
     * @param int $campaignID
     * @param int $companyId
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @author Aki
     * @comment this method adds base payload and adds tracking from project to settings.txt file of client
     */
    public function addBasePayLoad(
        int $campaignID,
        int $companyId
    ): array
    {
        $additionalSettings = [];
        $apiKey = "";
        if (empty($campaignID) || empty($companyId)) {
            return $additionalSettings;
        }
        $company = $this->companyRepository->getCompanyDetailById($companyId);
        //get additional required params
        $objectRegisterId = $this->campaignRepository->getUniqueObjectIdWithcampaignID($campaignID);
        $accountNumber = $this->campaignRepository->getAccountNumberWithAccountID($companyId);
        $apiKey = $this->objectRegisterMetaRepository->getAccountAPIKey($company['objectregister_id']); // TODO take this one
        $campaignName = $this->campaignRepository->getCampaignNameWithId($campaignID);
        $pageWebhookUUID = $this->objectRegisterRepository->getObjectregisterById($objectRegisterId)->getObjectUniqueId();

        //todo: take values from types(hint: emum support form php 8.1) aki
        $additionalSettings['additionalSettings']['objectregister_id'] = $objectRegisterId;
        $additionalSettings['additionalSettings']['accountNumber'] = $accountNumber;
        $additionalSettings['additionalSettings']['apiKey'] = $apiKey;
        $additionalSettings['additionalSettings']['CampaignName'] = $campaignName;
        $additionalSettings['additionalSettings']['AIOUrl'] = $_ENV['APIINONE_URL'];
        $additionalSettings['additionalSettings']['matomoUrl'] = $_ENV['MATOMO_URL'];
        $additionalSettings['additionalSettings']['uuid'] = $pageWebhookUUID;

        return $additionalSettings;
    }


    /**
     * @param int $camp_id
     * @return bool
     * @throws JsonException
     * @author Aki
     * @comment
     */
    public function addFlowControlSettings(int $camp_id): bool
    {
        $return = false;
        $pageFlowSettings = $this->campaignRepository->fetchCampaignParams($camp_id, 'pageflow');
        //if flow control is settings are available
        if (!empty($pageFlowSettings)) {
            $customPages = $pageFlowSettings['c_set']['pages']['custom'];
            //checks is survey is already present
            foreach ($customPages as $index) {
                foreach ($index as $key => $value) {
                    if ($value == 'survey.html') {
                        $return = true;
                        return $return;
                    }
                }
            }
            //add survey page to flow control
            $pageFlowSettings['c_set']['pages']['custom'][]['name'] = 'survey.html';
            $pageFlowSettingsValue = $pageFlowSettings['c_set'];
            $pageFlowSettingsValue['id'] = $pageFlowSettings['id'];
            if ($this->campaignRepository->storeCampaignParams($camp_id, 'pageflow', $pageFlowSettingsValue)) {
                $return = true;
            }
        }
        return $return;
    }


    /**
     * @param                    $campaign_id
     * @param CampaignRepository $campaignRepository
     * @return array
     * @author Aki
     * @comment
     * @deprecated
     */
    public function fetchCampaignParamsForCopyCampaign($campaign_id, $campaignRepository): array
    {
        $return = [];
        //get params from database
        $campaign_params = $campaignRepository->fetchCampaignParamsForCopyCampaign($campaign_id);
        if (empty($campaign_params)) {
            return $return;
        }
        //remove unnecessary params for mapping
        $unnecessaryFieldsForMapping = [
            'campaign_id',
            'object_register_lead_id',
            'object_register_campaign_id',
            'url',
            'final_url',
            'trafficsource',
            'utm_parameters',
            'campaign',
            'split_version',
            'lead_reference',
            'targetgroup',
            'status',
            'ip',
            'device_type',
            'device_browser',
            'device_os',
            'device_os_version',
            'device_browser_version',
            'status_reason',
            'account',
            'affiliateID',
            'affiliateSubID',
        ];
        foreach ($campaign_params['standard_fields'] as $key => $value) {
            if (in_array($key, $unnecessaryFieldsForMapping)) {
                unset($campaign_params['standard_fields'][$key]);
            }
        }
        return $campaign_params;
    }


    /**
     * @return string
     * @author Aki
     * @comment
     */
    public function createUniqueObjectGUID()
    {
        $ouid = '';
        try {
            mt_srand((double)microtime() * 10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen = chr(45);// "-"
            $ouid = substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16,
                    4) . $hyphen . substr($charid, 20, 12);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $ouid;
    }


    /**
     * @param       $campaign
     * @param array $oldCampaignSettings
     * @return array
     * @author Aki
     * @comment
     */
    public function mergeUndefinedSettingsFromOldCampaign($campaign, array $oldCampaignSettings): array
    {
        $return = [];
        if (!empty($campaign) || !empty($oldCampaignSettings)) {
            foreach ($oldCampaignSettings as $key => $value) {
                if (!(array_key_exists($key, $campaign))) {
                    if (array_key_exists('id', $value)) {
                        unset($value['id']);
                    }
                    $campaign[$key] = $value;
                }
            }
            return $campaign;
        }
        return $return;
    }

    /**
     * @param                    $user
     * @param                    $campaignId
     * @param CampaignRepository $campaignRepository
     * @param CompanyRepository $companyRepository
     * @return string
     * @throws \JsonException
     * @author Aki
     * @comment
     */
    public function generateScriptFileData($user, $campaignId, $templateFileContent, $campaignRepository, $companyRepository): string
    {
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
        $campaignSettings = $this->getCampaignSettings($campaignId, $projectObjectRegisterId,
            ObjectRegisterMetaRepository::MetaKeyWebHook);
        //add additional parameters for camapaign settings
        //todo: need to add script file with all settings included!!
        $additionalSettings = $this->addBasePayLoad($campaignId, $user['company_id'], $user['company_id'],
            $companyRepository);
        $campaignSettings = array_merge($campaignSettings, $additionalSettings);
        $campaignSettings = base64_encode(json_encode($campaignSettings));
        $scriptString = "
        let settingsValue = \"$campaignSettings\" \n
        $templateFileContent
        ";

        return $scriptString;
    }


    /**
     * @param string $ip
     * @return string default = Germany
     */
    public function getCountryByIp(string $ip): string
    {
        $defaultCountry = 'Germany';
        try {
            $country = $this->geoIpClient->country($ip);
        } catch (AddressNotFoundException $e) {
            $this->logger->warning($e->getMessage(), [__FUNCTION__, __LINE__]);
            return $defaultCountry;
        } catch (AuthenticationException $e) {
            $this->logger->warning($e->getMessage(), [__FUNCTION__, __LINE__]);
            return $defaultCountry;
        } catch (InvalidRequestException $e) {
            $this->logger->warning($e->getMessage(), [__FUNCTION__, __LINE__]);
            return $defaultCountry;
        } catch (HttpException $e) {
            $this->logger->warning($e->getMessage(), [__FUNCTION__, __LINE__]);
            return $defaultCountry;
        } catch (OutOfQueriesException $e) {
            $this->logger->warning($e->getMessage(), [__FUNCTION__, __LINE__]);
            return $defaultCountry;
        } catch (GeoIp2Exception $e) {
            $this->logger->warning($e->getMessage(), [__FUNCTION__, __LINE__]);
            return $defaultCountry;
        }
        $this->logger->info('$country->country', [$country->country, __FUNCTION__, __LINE__]);
        $this->logger->info('$country->country', [$country->country->name, __FUNCTION__, __LINE__]);
        return (!empty($country->country) && !empty($country->country->name)) ? $country->country->name : $defaultCountry;
    }

    /**
     * @param string $campaignType
     * @param array $pageWizardData
     * @param int $pageObjectRegisterId
     * @param int $projectObjectRegisterId
     * @return bool
     * @author Aki
     */
    public function addCampaignSettings(string $campaignType,
                                        array  $pageWizardData,
                                        int    $pageObjectRegisterId,
                                        int    $projectObjectRegisterId): bool
    {
        // basic check
        if ($pageObjectRegisterId <= 0 || empty($pageWizardData) || empty($projectObjectRegisterId)) {
            $this->logger->critical('Invalid params passed');
            return false;
        }
        //encode the settings
        $pageSettingsString = json_encode($pageWizardData);
        $objectRegisterMetaId = $this->objectRegisterMetaRepository->insert($pageObjectRegisterId,
            $campaignType, $pageSettingsString);
        if (empty($objectRegisterMetaId)) {
            return false;
        }
        return true;
    }

    /**
     * @param int $campaignId
     * @param string $metaKey
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Aki
     */
    public function deleteCampaign(int $campaignId, string $metaKey): bool
    {
        // basic check
        if ($campaignId <= 0) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return false;
        }
        //todo:delete campaign in server
        $objectRegisterId = $this->campaignRepository->getobjectregisterIdWithCampaignId($campaignId);
        $objectRegisterMetaId = $this->objectRegisterMetaRepository->getIdWithObjectRegisterId($objectRegisterId, $metaKey);
        //delete campaign form objectRegisterMeta
        $boolCheck['objectRegisterMeta'] = $this->objectRegisterMetaRepository->delete($objectRegisterMetaId);
        //delete from campaign
        $boolCheck['campaign'] = $this->campaignRepository->deleteCampaign($campaignId);
        //delete from object register
        $boolCheck['objectRegister'] = $this->objectRegisterRepository->delete($objectRegisterId);
        if (in_array(true, $boolCheck, true) === false) {
            return false;
        }
        return true;
    }


    /**
     * COMMENT: method MUST be moved to MSCAMP later
     * @param string $account_no
     * @return bool
     * @expectedDeprecation
     * @internal 2021-11-11 added loop for $_ENV['CAMPAIGN_DOMAIN'], $_ENV['WEBHOOK_DOMAIN'], since they can be hosted below different domains
     * @todo IMPLEMENT => METHOD MUST BE MOVED TO MSCAMP
     */
    public function deleteCampaignOnFilesystem(string $account_no): bool
    {
        if ($_ENV['CAMPAIGN_DOMAIN'] === $_ENV['WEBHOOK_DOMAIN']) {
            $domains = [$_ENV['CAMPAIGN_DOMAIN']];
        } else {
            $domains = [$_ENV['CAMPAIGN_DOMAIN'], $_ENV['WEBHOOK_DOMAIN']];
        }

        foreach ($domains as $domain) {
            try {
                $mscamp = array_reverse(explode('/', $domain))[0];
                $dir = explode(DIRECTORY_SEPARATOR, $_SERVER['DOCUMENT_ROOT']);
                $unsetKey = count($dir) - 1;
                unset($dir[$unsetKey]);
                $rootPath = implode(DIRECTORY_SEPARATOR, $dir) . DIRECTORY_SEPARATOR . "{$mscamp}/";// TODO add to .env
                $this->logger->info('$rootPath', [$rootPath]);
                $this->logger->info('$rootPath . $account_no', [$rootPath . $account_no]);
                $chmod = chmod($rootPath . $account_no, 0777);
                $this->logger->info('$chmod', [$chmod]);

                $this->rrmdir($rootPath . $account_no);
            } catch (Exception $e) {
                $this->logger->error('# EXCEPTION #', [$e->getMessage(), $e->getCode(), __FUNCTION__, __LINE__]);
                return false;
            }
        }

        return true;
    }


    /**
     * recursively delete directory
     * @param string $directory
     */
    protected function rrmdir(string $directory)
    {
        $this->logger->info('$directory', [$directory, __FUNCTION__, __LINE__]);

        $files = array_diff(scandir($directory), array('.', '..'));

        foreach ($files as $file) {
            if (is_dir("$directory/$file")) {
                $this->rrmdir("$directory/$file");
            } else {
                chmod("$directory/$file", 0777);
                unlink("$directory/$file");
            }
        }
        chmod($directory, 0777);
        return rmdir($directory);
    }


    /**
     * @param int $campaignId
     * @param string $campaignSettings
     * @param string $metaKey
     * @param array $oldSettings
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @autho AKI
     * @internal 1.Create new page/webhook in database
     *           2.Updates work flow
     *           3.Saves new settings
     *           4. Archives previous versions of draft web-5710
     *
     */
    public function reviseCampaign(int $campaignId, string $campaignSettings, string $metaKey, array $oldSettings): ?array
    {
        $newCampaignDetails = [];
        // basic check
        if ($campaignId <= 0 || empty($metaKey)) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return null;
        }
        //get user details
        $userEmail = $this->authenticationUtils->getLastUsername();
        $user = $this->authenticationService->getUserParametersFromCache($userEmail);
        $companyId = $user['company_id'];
        //get old campaign settings from objectRegister, objectRegisterMeta and campaign tables
        $oldCampaign = $this->campaignRepository->get($campaignId);
        $oldObjectRegister = $this->objectRegisterRepository->getObjectregisterById($oldCampaign['objectregister_id']);
        $objectUniqueId = $oldObjectRegister->getObjectUniqueId();
        $oldVersion = $this->objectRegisterRepository->getLatestVersion($objectUniqueId);

        //All old versions of the pages/webhooks are  archived web- 5710
        if(!$this->archivePreviousVersionsOfDraftCampaign($objectUniqueId)){
            return null;
        }

        //create new campaign
        $newVersion = $oldVersion + 1;
        //New Object register
        $newObjectRegisterId = $this->objectRegisterRepository->createObjectRegisterEntryForRevisedCampaign($metaKey,
            'draft',
            $newVersion,
            $objectUniqueId,
            $companyId);
        //New campaign - create new page in DB
        $newCampaignId = $this->campaignRepository->createPageInDB($newObjectRegisterId,
            $oldCampaign['name'],
            $companyId,
            $oldCampaign['url'],
            $user['user_id']);

        //update workflow
        $campaignSettings = json_decode($campaignSettings, true, 512, JSON_THROW_ON_ERROR);
        $campaignSettings['processhookStatus'] = $this->createNewWorkflowVersion($campaignSettings,
            $newCampaignId,
            $newVersion
        );
        $campaignSettings = json_encode($campaignSettings, JSON_THROW_ON_ERROR | true, 512);
        //New Settings
        $objectRegisterMetaId = $this->objectRegisterMetaRepository->insert($newObjectRegisterId,
            $metaKey,
            $campaignSettings);
        if (empty($newObjectRegisterId) or empty($newCampaignId) or empty($objectRegisterMetaId)) {
            return null;
        }
        //put required fields into array
        $newCampaignDetails['ObjectRegisterId'] = $newObjectRegisterId;
        $newCampaignDetails['CampaignId'] = $newCampaignId;
        $newCampaignDetails['objectRegisterMetaId'] = $objectRegisterMetaId;
        $newCampaignDetails['oldVersion'] = $oldVersion;
        $newCampaignDetails['newVersion'] = $newVersion;
        $newCampaignDetails['name'] = $oldCampaign['name'];

        return $newCampaignDetails;

    }

    /**
     * @param int $campaignId
     * @return bool
     * @Author Aki
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function archivePreviousVersionsOfActiveCampaign(int $campaignId): bool
    {
        // basic check
        if ($campaignId <= 0) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return false;
        }
        $Campaign = $this->campaignRepository->get($campaignId);
        $oldObjectRegister = $this->objectRegisterRepository->getObjectregisterById($Campaign['objectregister_id']);
        $objectUniqueId = $oldObjectRegister->getObjectUniqueId();

        return $this->objectRegisterRepository->archiveCampaignWithObjectUniqueID($objectUniqueId,
            StatusdefTypes::ACTIVE);
    }

    /**
     * @param int $campaignId
     * @param int $projectObjectRegisterId
     * @return array|null
     * @author Aki
     * @deprecated - use fetchSettingsForStartWizard
     */
    public function fetchSettingsForCopyCampaign(int $campaignId, int $projectObjectRegisterId): ?array
    {
        // basic check
        if ($campaignId <= 0) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return null;
        }

        //get data
        $objectRegisterId = $this->campaignRepository->getobjectregisterIdWithCampaignId($campaignId);
        $settings = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($objectRegisterId, ObjectRegisterMetaRepository::MetaKeyPage);
        //required field for build page flow
        $settings['page_flow']['campaign']['url'] = '';
        $campaignName = $this->campaignRepository->getCampaignNameWithId($campaignId);
        $settings['settings']['name'] = $campaignName . 'copy';
        $settings['objectRegisterId'] = $objectRegisterId;
        $settings['standard_fields'] = $this->contactRepository->getStandardFieldsFromDB();
        $settings['customFieldList'] = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($projectObjectRegisterId,
            ObjectRegisterMetaRepository::MetaKeyProject)['customfieldlist'];
        return $settings;
    }

    /**
     * @param int $camp_id
     * @param string $cnf_group
     * @param string $cnf_tgt
     * @param string $metaKey
     * @return array|null
     * @author Aki
     */
    public function fetchCampaignSettings(int $camp_id, string $cnf_group, string $cnf_tgt, string $metaKey): ?array
    {
        // basic check
        if ($camp_id <= 0 || empty($cnf_tgt)) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return Null;
        }
        $objectRegisterId = $this->campaignRepository->getobjectregisterIdWithCampaignId($camp_id);
        $settings = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($objectRegisterId, $metaKey);
        $cnf_group = ucfirst($cnf_group);
        $cnf_tgt = ucfirst($cnf_tgt);
        return $settings[$cnf_group][$cnf_tgt];

    }

    /**
     * @param int $campaignId
     * @param string $campaignType
     * @return array|null
     * @author Aki
     */
    public function fetchSettingsForStartWizard(int $campaignId, string $campaignType): ?array
    {
        // basic check
        if ($campaignId <= 0) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return null;
        }
        //get data
        $objectRegisterId = $this->campaignRepository->getobjectregisterIdWithCampaignId($campaignId);
        if ($campaignType === "pages") {
            $metaKey = ObjectRegisterMetaRepository::MetaKeyPage;
        } elseif ($campaignType === "webhooks") {
            $metaKey = ObjectRegisterMetaRepository::MetaKeyWebHook;
        }
        $settings = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($objectRegisterId, $metaKey);
        //required field for build page flow
        if ($campaignType === "pages") {
            $settings['page_flow']['campaign']['url'] = '';
        }
        $settings['objectRegisterId'] = $objectRegisterId;
        return $settings;

    }

    /**
     * @param string $metaKey
     * @param array $campaign
     * @param int $objectRegisterId
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function updateSettings(string $metaKey,
                                   array  $campaign,
                                   int    $objectRegisterId): bool
    {
        // basic check
        if (empty($metaKey) || empty($campaign) || empty($objectRegisterId)) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return false;
        }
        $settings = json_encode($campaign);
        return $this->objectRegisterMetaRepository->updateSettings($objectRegisterId, $settings, $metaKey);

    }


    /**
     * @param array $newSettings
     * @param array $settings
     * @return array|false|null
     * @author  Aki
     */
    public function checkNewSettings(array $newSettings,
                                     array $settings): ?array
    {
        $response = null;
        // basic check
        if (empty($newSettings) || empty($settings)) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return null;
        }
        if (!isset ($settings['splittest']['status']) && isset($newSettings['splittest']['status'])) {
            $response['difference_splittest'] = true;
        };
        $difference_page_flow = $this->compare_multi_Arrays($newSettings['page_flow']['custom_pages'], $settings['page_flow']['custom_pages']);
        if (count($difference_page_flow['more']) > 0) {
            $response['newPages'] = $difference_page_flow['more'];
        }
        return $response;
    }


    /**
     * @param $array1
     * @param $array2
     * @return array
     * @author Aki
     */
    public function compare_multi_Arrays($array1, $array2): array
    {
        $result = array("more" => array(), "less" => array(), "diff" => array());
        foreach ($array1 as $k => $v) {
            if (is_array($v) && isset($array2[$k]) && is_array($array2[$k])) {
                $sub_result = $this->compare_multi_Arrays($v, $array2[$k]);
                //merge results
                foreach (array_keys($sub_result) as $key) {
                    if (!empty($sub_result[$key])) {
                        $result[$key] = array_merge_recursive($result[$key], array($k => $sub_result[$key]));
                    }
                }
            } else {
                if (isset($array2[$k])) {
                    if ($v !== $array2[$k]) {
                        $result["diff"][$k] = array("from" => $v, "to" => $array2[$k]);
                    }
                } else {
                    $result["more"][$k] = $v;
                }
            }
        }
        foreach ($array2 as $k => $v) {
            if (!isset($array1[$k])) {
                $result["less"][$k] = $v;
            }
        }
        return $result;
    }

    /**
     * @param int $companyId
     * @param int $campaignObjectRegisterId
     * @param array $mapping
     * @return bool|null
     * @throws JsonException
     * @author Pradeep
     */
    public function addCampaignCustomFields(int $companyId, int $campaignObjectRegisterId, array $mapping): ?bool
    {
        $campaignCustomFieldList = [];
        $metaKey = 'customfield_reference';
        if (empty($mapping) || !isset($mapping['mappingTable'])) {
            return false;
        }
        $decodedJson = base64_decode($mapping['mappingTable']);
        $mappingArr = json_decode($decodedJson, true, 512, JSON_THROW_ON_ERROR);

        if (!isset($mappingArr['contact']['custom'])) {
            return true;
        }
        $customFields = $mappingArr['contact']['custom'];
        $projectCustomFields = $this->customFieldsRepository->getProjectCustomfields($companyId);
        // search the defined campaign customfield in project customfield list
        foreach ($customFields as $fields) {
            foreach ($fields as $key => $value) {
                if (in_array($key, ['required', 'datatype', 'regex'])) {
                    continue;
                }
                foreach ($projectCustomFields as $projectCustomField) {
                    if ($projectCustomField['fieldname'] !== $key) {
                        continue;
                    }
                    $campaignCustomFieldList[$projectCustomField['fieldname']] = $projectCustomField;
                }
            }
        }
        if (empty($campaignCustomFieldList)) {
            return true;
        }
        $customfieldlist = json_encode($campaignCustomFieldList, JSON_THROW_ON_ERROR);
        $objRegMetaDetails = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails($campaignObjectRegisterId,
            $metaKey);
        if (empty($objRegMetaDetails)) {
            //insert
            if (is_null($this->objectRegisterMetaRepository->insert($campaignObjectRegisterId, $metaKey,
                $customfieldlist))) {
                // var_dump('failed');
                return false;
            }
        } else {
            //update
            if (is_null($this->objectRegisterMetaRepository->update($objRegMetaDetails['id'],
                $campaignObjectRegisterId, $metaKey,
                $customfieldlist))) {
                // var_dump('failed');
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $settings
     * @return array|null
     * @author AKI
     * @internal Add company id to settings structure
     */
    public function chageDataStructure(array $settings): ?array
    {
        $settings['mapping'] = (array)$settings['mapping'];
        $settings['mapping']['mappingTable'] = $settings['mapping'];
        unset($settings['mapping'][0]);
        unset($settings['company_name']);
        $settings['company_id'] = $settings['relation']['company_id'];
        $settings['settings']['name'] = $settings['settings']['name']['value'];
        $settings['settings']['description'] = $settings['settings']['description']['value'];
        if (isset($settings['settings']['beginDate']['value'])) {
            $settings['settings']['beginDate'] = $settings['settings']['beginDate']['value'];
        }
        if (isset($settings['settings']['expireDate']['value'])) {
            $settings['settings']['expireDate'] = $settings['settings']['expireDate']['value'];
        }
        $settings['processhooks'] = "";
        return $settings;
    }

    /**
     * @param array $params
     * @param string|null $processhooks
     * @return array|null
     * @throws JsonException
     * @author aki
     */
    public function formatProcesshooksForWizard(array $params, ?string $processhooks): ?array
    {
        //Basic check
        if (empty($params)) {
            $this->logger->error(" processhook settings are empty", [__METHOD__, __LINE__]);
            return null;
        }
        $usedProcesshooks = [];
        $formattedProcesshooks = [];
        //for new wizard
        if (is_null($processhooks) || empty($processhooks)) {
            $tempParams = [];
            foreach ($params as $key => $value) {
                if (isset($value['active_details'])) {
                    //remove unwanted params
                    unset($params[$key]['active_details']);
                }
                //divide processhooks into active and inactive
                if ($value['name'] === ProcesshookTypes::DEFAULT_PROCESSHOOK_SEND_LEAD_NOTIFICATION) {
                    $tempParams['active'][] = $value;
                } else {
                    $tempParams['inactive'][] = $value;
                }
            }
            //return all params as inactive
            return $tempParams;
        }
        //structure into active and inactive
        $processhooks = json_decode($processhooks, true, 512, JSON_THROW_ON_ERROR);
        //todo:get all events and loop for each event
        foreach ($processhooks['index.html']['data_received'] as $processhook) {
            $usedProcesshooks[] = $processhook['processhookName'];
        }
        foreach ($params as $param) {
            if (isset($param['active_details'])) {
                //remove unwanted params
                unset($param['active_details']);
            }
            //divide processhooks into active and inactive
            if (in_array($param['name'], $usedProcesshooks, false)) {
                $formattedProcesshooks['active'][] = $param;
            } else {
                $formattedProcesshooks['inactive'][] = $param;
            }
        }
        return $formattedProcesshooks;
    }

    /**
     * @param array $campaign
     * @param array $settings
     * @return array|null
     * @author aki
     */
    public function addMissingParametersToCampaign(array $campaign, array $settings): ?array
    {
        //Basic check
        if (empty($campaign) || empty($settings)) {
            $this->logger->error(" processhook settings are empty", [__METHOD__, __LINE__]);
            return null;
        }
        if (isset($settings['objectRegisterId'])) {
            $campaign['objectRegisterId'] = $settings['objectRegisterId'];
        }
        if (isset($settings['version'])) {
            $campaign['version'] = $settings['version'];
        }

        if (isset($settings['url'])) {
            $campaign['url'] = $settings['url'];
        }

        if (isset($settings['processhookStatus'])) {
            $campaign['processhookStatus'] = $settings['processhookStatus'];
        }

        return $campaign;
    }

    /**
     * @param array $availableProcesshooks
     * @param string|null $mappedProcesshooks
     * @return array|null
     */
    public function formatProcesshooksForPageWizard(array $availableProcesshooks, ?string $mappedProcesshooks): ?array
    {
        //Basic check
        if (empty($availableProcesshooks)) {
            $this->logger->error(" processhook settings are empty", [__METHOD__, __LINE__]);
            return null;
        }
        //remove unnecessary elements for wizard
        foreach ($availableProcesshooks as $key => $value) {
            unset($availableProcesshooks[$key]['active_details']);
        }
        //add existing processhooks
        if ($mappedProcesshooks) {
            $tempParams['defined'] = json_decode($mappedProcesshooks, true, 512, JSON_THROW_ON_ERROR);
        }
        //for new wizard
        $tempParams['available'] = $availableProcesshooks;
        //return all params as inactive
        return $tempParams;
    }

    /**
     * @param array $pageFlowSettings
     * @return array|null
     * @auther Aki
     */
    public function formatPageflowButtons(array $pageFlowSettings): ?array
    {
        //Basic check
        if (empty($pageFlowSettings)) {
            $this->logger->error(" processhook settings are empty", [__METHOD__, __LINE__]);
            return null;
        }

        $buttons = json_decode($pageFlowSettings['buttons'], true, 512, JSON_THROW_ON_ERROR);
        //hardcode submit button as this is default button for index
        $buttons['index.html'][] = "Submit";
        // format into structure
        foreach ($pageFlowSettings['custom_pages'] as $key => $value) {

            if (array_key_exists($value['name'], $buttons)) {
                foreach ($buttons[$value['name']] as $index => $name) {
                    //see structure for understanding the assignment
                    $pageFlowSettings['custom_pages'][$key]['buttons'][$index]['name'] = $name;
                }
            }
        }
        return $pageFlowSettings;

    }

    /**
     * @param $id
     * @param string $pageStatus
     * @return string
     * @author Aki
     */
    private function generateDownloadUrl($id, string $pageStatus): string
    {
        return "page/download/$id/$pageStatus";
    }

    /**
     * @param $microService
     * @param array $pageDetails
     * @param string $downloadDir
     * @return MimeTypes|null
     * @throws JsonException
     * @author Aki
     */
    public function downloadPage(
        $microService,
        array $pageSettings,
        array $pageDetails,
        string $downloadDir
    ): ?string
    {

        $http_client = HttpClient::create(['http_version' => '2.0']);

        //get the zip file
        try {
            $http_client_resp = $http_client->request('POST', $microService . '/page/download', [
                'body' => [
                    'download_details' => base64_encode(json_encode($pageDetails, JSON_THROW_ON_ERROR | true, 512)),
                    'page_settings' => base64_encode(json_encode($pageSettings, JSON_THROW_ON_ERROR | true, 512))
                ],
            ]);

            $zipFile = $http_client_resp->getContent();
            $fileContentDisposition = $http_client_resp->getHeaders()['content-disposition'];
            preg_match('/"([^"]+)"/', $fileContentDisposition[0], $fileName);
        } catch (TransportExceptionInterface|ClientExceptionInterface|
        RedirectionExceptionInterface|ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        $destination = $downloadDir . DIRECTORY_SEPARATOR . $fileName[1];

        $this->logger->info('$destination', [$destination, __METHOD__, __LINE__]);

        if (!file_exists($downloadDir)) {
            if (!mkdir($downloadDir, 0755, true) && !is_dir($downloadDir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $downloadDir));
            }
        }


        $file = fopen($destination, 'w+');
        fwrite($file, $zipFile);
        fclose($file);
        return $destination;
    }

    /**
     * @param string $pageType
     * @param int $pageId
     * @return array
     * author Aki
     * @throws \Doctrine\DBAL\Exception
     */
    public function getDetailsForPageDownload(string $pageType, int $pageId): ?array
    {
        //Basic check
        if (empty($pageType) || $pageId <= 0) {
            $this->logger->error(" page information not available", [__METHOD__, __LINE__]);
            return null;
        }
        //get company name
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        $version = $this->campaignRepository->getVersionFromCampaignId($pageId);
        //get page settings
        $projectObjectRegisterId = $this->campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
        $pageSettings = $this->getCampaignSettings($pageId, $projectObjectRegisterId,
            ObjectRegisterMetaRepository::MetaKeyPage);
        //get page name from settings
        $pageName = $pageSettings['settings']['name'];
        //details needed for download
        $downloadDetails['folder_name'] = $pageName;
        $downloadDetails['company'] = $user['company_account_no'];
        $downloadDetails['version'] = $version;
        $downloadDetails['page_type'] = $pageType;

        return $downloadDetails;

    }

    /**
     * @param array $pageDetails
     * @param string $fileLocation
     * @return array|null
     * @throws JsonException
     * @author Aki
     * @deprecated
     */
    public function uploadPageFiles(array $pageDetails, string $fileLocation): ?array
    {
        $status = [];
        //Basic check
        if (empty($pageDetails) || empty($fileLocation)) {
            $this->logger->error("", [__METHOD__, __LINE__]);
            $status['error'] = "Please check the uploaded file";
            return $status;
        }

        //format data
        $data = [
            'upload_details' => base64_encode(json_encode($pageDetails, JSON_THROW_ON_ERROR | true, 512)),
            'file' => DataPart::fromPath($fileLocation),
        ];
        $formData = new FormDataPart($data);
        //send the file to mscamp
        $http_client = HttpClient::create(['http_version' => '2.0']);
        //get the zip file
        try {
            $http_client_resp = $http_client->request('POST', $this->campaignOrganizer . '/page/upload', [
                'headers' => $formData->getPreparedHeaders()->toArray(),
                'body' => $formData->bodyToIterable(),
            ]);
            $status['success'] = $http_client_resp->getContent();
        } catch (TransportExceptionInterface|ClientExceptionInterface|
        RedirectionExceptionInterface|ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $status['error'] = $e->getMessage();
        }
        //todo:delete the file
        //return status
        return $status;
    }


    /**
     * @param string $camp_name
     * @param int $version
     * @param string $company
     * @param string $campaignSettings
     * @param string $urlImprint
     * @param string $urlPrivacyPolicy
     * @return bool
     * @author aki, jsr
     */
    public function addOrUpdateClientSettings(
        string $camp_name,
        int    $version,
        string $company,
        string $campaignSettings,
        string $urlImprint,
        string $urlPrivacyPolicy,
        string $organizerServiceUrl
    ): bool
    {
        $microService = $organizerServiceUrl;
        $status = true;
        $http_client = HttpClient::create(['http_version' => '2.0']);
        if (empty($camp_name) || empty($company) || $version <= 0) {
            return $status;
        }
        try {
            $http_client_resp = $http_client->request('POST', $microService . '/settings/update', [
                'body' => [
                    'campaign_name' => $camp_name,
                    'company' => $company,
                    'version' => $version,
                    'campaignSettings' => $campaignSettings,
                    'urlImprint' => $urlImprint,
                    'urlPrivacyPolicy' => $urlPrivacyPolicy
                    // TODO: add link to datenschutzerklärung, may be imprint as well - take data from account settings (add fields datanscvhutz..., impressum-link)
                ],
            ]);
            $status = $http_client_resp->getContent();
        } catch (TransportExceptionInterface|ClientExceptionInterface|
        RedirectionExceptionInterface|ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param $campaignId
     * @param $companyId
     * @param $projectObjectRegisterId
     * @param $pageName
     * @param $companyAccountNumber
     * @param string $domain
     * @return bool
     * @internal <b style="color:red">TODO: @Aravind => add types to parameters!!!</b>
     * @author Aki
     * @internal Add or updates settings.txt file of client using mscamp microservice
     */
    public function addOrUpdateSettingsInServer(
        $campaignId,
        $companyId,
        $projectObjectRegisterId,
        $pageName,
        $companyAccountNumber,
        string $organizerServiceUrl
    ): bool
    {
        //take settings for database
        try {
            $campaignSettings = $this->getCampaignSettings($campaignId, $projectObjectRegisterId,
                ObjectRegisterMetaRepository::MetaKeyPage);
        } catch (\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
        $this->logger->info("New settings for DB", [$campaignSettings, __METHOD__, __LINE__]);
        //add additional parameters for campaign settings
        $additionalSettings = $this->addBasePayLoad($campaignId, $companyId);
        $companyData = $this->companyRepository->getCompanyById($companyId);

        $campaignSettings = array_merge($campaignSettings, $additionalSettings);
        //make settings into a string
        $campaignSettings = json_encode($campaignSettings);
        //some other needed parameters for the moving files in ms-camp
        $campaignVersion = $this->campaignRepository->getVersionFromCampaignId($campaignId);
        $this->logger->info("New version adding settings", [$campaignVersion, __METHOD__, __LINE__]);
        //This is calling Ms camp
        if (!$this->addOrUpdateClientSettings(
            $pageName,
            (int)$campaignVersion,
            $companyAccountNumber,
            $campaignSettings,
            (empty($companyData->getUrlImprint())) ? '#' : $companyData->getUrlImprint(),
            (empty ($companyData->getUrlPrivacyPolicy())) ? '#' : $companyData->getUrlPrivacyPolicy(),
            $organizerServiceUrl
        )) {
            $this->logger->critical('Something went wrong, could not create in Server', [__METHOD__, __LINE__]);
            return false;
        }
        return true;
    }

    /**
     * @param string $sourceFolder
     * @param string $zipFIleName
     * @return bool
     * @author Aki
     */
    public function unZipFile(string $sourceFolder, string $zipFileName): bool
    {
        $zipFile = $sourceFolder . DIRECTORY_SEPARATOR . $zipFileName;
        //extract the files
        try {
            $zip = new ZipArchive;
            if ($zip->open($zipFile, ZipArchive::CREATE) === true) {
                $unzipStatus = $zip->extractTo($sourceFolder);
                $zip->close();
            }
        } catch (Exception $e) {
            $this->logger->error("Could not unzip $zipFileName", [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }
        return true;

    }

    public function deleteUploadFolder(string $sourceFolder): bool
    {
        //Delete the content of the folder
        try {
            $directory = new RecursiveDirectoryIterator($sourceFolder, FilesystemIterator::SKIP_DOTS);
            $files = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ($files as $file) {
                $file->isDir() ? rmdir($file) : unlink($file);
            }
        } catch (Exception $e) {
            $this->logger->error('could not able to clear the upload pages folder', [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }
        //remove the folder
        rmdir($sourceFolder);
        return true;
    }

    /**
     * @param string $sourceFolder
     * @param string $pageId
     * @return bool
     * @author Aki
     */
    public function makeHtmlFieldAnalysis(string $sourceFolder, string $pageId): bool
    {
        //todo: add new fields to custom fields of the project
        try {
            $formHtmlFolder = $this->getAbsolutePath('form.html', $sourceFolder);
            $this->logger->info('path for form.html', [$formHtmlFolder, __METHOD__, __LINE__]);

            $htmlFieldsFromUploadedZip = $this->getHtmlFields($formHtmlFolder);
            $this->logger->info('html fields from upload zip', [$htmlFieldsFromUploadedZip, __METHOD__, __LINE__]);

            //format to the structure to check against the upload fields
            $htmlFieldsFromDatabase = $this->formatHtmlFieldsForFieldCheck($pageId);
            $this->logger->info('html fields form Database', [$htmlFieldsFromDatabase, __METHOD__, __LINE__]);

            //check for the changes in the html fields of the upload form.html
            if (!($htmlFieldsFromDatabase === $htmlFieldsFromUploadedZip)) {
                //add new settings to page object register meta
                return $this->addNewFieldsToMapping($htmlFieldsFromUploadedZip, $htmlFieldsFromDatabase, $pageId);
            }
        } catch (Exception $e) {
            $this->logger->error('uploaded page analysis failed', [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }

        return true;

    }

    /**
     * @param string $fileName e.g. form.html
     * @param string $startSearchFromDirectory e.g. /upload/temp
     * @return string absolute path where file was found or null
     * @throws Exception
     * @author JSR
     */
    private function getAbsolutePath(string $fileName, string $startSearchFromDirectory): ?string
    {
        try {
            $directoryIterator = new RecursiveDirectoryIterator($startSearchFromDirectory);
        } catch (Exception $e) {
            $expectedFileNameArr = explode(DIRECTORY_SEPARATOR, $startSearchFromDirectory);
            $expectedFileName = $expectedFileNameArr[count($expectedFileNameArr) - 1];
            throw new Exception("The uploaded zip file does not contain a root folder '{$expectedFileName}'.  Please check your zip acrhive.");
        }

        foreach (new RecursiveIteratorIterator($directoryIterator) as $pathAndFile) {
            if (stristr($pathAndFile, $fileName) !== false) {
                return rtrim(preg_replace("/{$fileName}/", '', $pathAndFile), DIRECTORY_SEPARATOR);
            }
        }
        return null;
    }

    /**
     * @param string|null $formHtmlFolder
     * @return array
     * @author Aki
     */
    private function getHtmlFields(?string $formHtmlFolder): array
    {
        //location of the form.html in the upload folder
        $formFileLocation = $formHtmlFolder . '/form.html';
        //get contents into string
        $form = file_get_contents($formFileLocation);
        $this->logger->info('form', [$form, __METHOD__, __LINE__]);
        //move the matches into array
        $matches = array();
        preg_match_all('~marketingInOneField="(.*?)"~', $form, $matches);
        $this->logger->info('matches', [$matches, __METHOD__, __LINE__]);
        //return the matches
        return $matches[1];
    }


    /**
     * @param array $htmlFieldsFromUploadedZip
     * @param array $htmlFieldsFromDatabase
     * @param string $pageId
     * @return bool
     * @throws JsonException
     * @author Aki
     */
    private function addNewFieldsToMapping(array  $htmlFieldsFromUploadedZip,
                                           array  $htmlFieldsFromDatabase,
                                           string $pageId
    ): bool
    {
        //Get the difference in html fields
        $newlyAddedHtmlFields = array_diff($htmlFieldsFromUploadedZip, $htmlFieldsFromDatabase);
        //get Mapping table from the Database
        $mapping = $this->getMappingTable($pageId);
        //if new fields are empty there is difference in order of newly uploaded zip file
        if (empty($newlyAddedHtmlFields)) {
            //the difference is only the oder of the array
            //todo:arrange the order and get mapping
            return true;
        }
        //add each new html field to mapping table
        foreach ($newlyAddedHtmlFields as $field) {
            $fieldStructure = $this->pageWizardTypes->getMappingHtmlFieldType();
            //add new field in custom fields and at the beginning of the array
            $fieldStructure = array($field => "") + $fieldStructure;
            //needed to move to beginning as required by view(UI)
            $mapping["contact"]['custom'][] = $fieldStructure;
        }
        //save format and save the new page settings
        try {
            //get the page settings
            $pageSettings = $this->fetchSettingsForStartWizard((int)$pageId, CampaignRepository::PAGES);
            $pageSettings['mapping']['mappingTable'] = base64_encode(json_encode($mapping, JSON_THROW_ON_ERROR));
            //Save the new settings into database
            $this->logger->info('$pageSettings', [$pageSettings, __METHOD__, __LINE__]);
            $this->objectRegisterMetaRepository->updateSettings($pageSettings['objectRegisterId'],
                json_encode($pageSettings, JSON_THROW_ON_ERROR),
                ObjectRegisterMetaRepository::MetaKeyPage);
            return true;
        } catch (Exception $e) {
            $this->logger->error('Error in saving new settings', [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }

    }

    /**
     * @param string $pageId
     * @return array
     * @throws JsonException
     */
    private function formatHtmlFieldsForFieldCheck(string $pageId): array
    {
        //get mapping of the page
        $mapping = $this->getMappingTable($pageId);
        $this->logger->info(' $mapping', [$mapping, __METHOD__, __LINE__]);
        //combine custom fields and standard fields
        $standardFields = $mapping['contact']['standard'];
        $customFields = $mapping['contact']['custom'];
        $htmlFields = array_merge($standardFields, $customFields);

        //loop and get formatted fields
        $formattedFields = [];
        foreach ($htmlFields as $key => $field) {
            $formattedFields[$key] = array_key_first($field);
        }

        return $formattedFields;
    }

    /**
     * @throws JsonException
     */
    public function getMappingTable(string $pageId)
    {
        //get settings of the page
        $pageSettings = $this->fetchSettingsForStartWizard((int)$pageId, CampaignRepository::PAGES);
        return json_decode(base64_decode($pageSettings['mapping']['mappingTable']),
            true, 512, JSON_THROW_ON_ERROR);
    }


    /**
     * @param int $pageId
     * @param string $metaKey
     * @return array
     * @author Aki
     * @internal This method replaces many deprecated methods
     */
    public function getPageSettingsWithId(int $pageId, string $metaKey): array
    {
        $settings = [];
        //basic check
        if (empty($pageId)) {
            return $settings;
        }
        //get the settings
        try {
            $objectRegisterId = $this->campaignRepository->getobjectregisterIdWithCampaignId($pageId);
            $settings = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($objectRegisterId,
                $metaKey);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        //return
        return $settings;

    }

    /**
     * @param int $campaignId
     * @return array
     * @throws JsonException
     * @author Aki
     */
    public function generateDataForTemplate(int $campaignId): array
    {
        $data = [];
        //basic check
        if (empty($campaignId)) {
            return $data;
        }
        //get the webhook settings
        $settings = $this->getPageSettingsWithId($campaignId, ObjectRegisterMetaRepository::MetaKeyWebHook);
        //Decoding base64 encoded mapping

        if (empty($settings['mapping']['mappingTable'])) {
            //automatically generated webhooks from api
            $data['mapping'] = json_decode(base64_decode($settings['mapping']),
                true, 512, JSON_THROW_ON_ERROR);
            $companyId = $settings['relation']['company_id'];
        } else {
            $data['mapping'] = json_decode(base64_decode($settings['mapping']['mappingTable']),
                true, 512, JSON_THROW_ON_ERROR);
            $companyId = $settings['company_id'];
        }
        //add additional parameters for campaign settings
        $data['base'] = $this->addBasePayLoad($campaignId, $companyId)['additionalSettings'];
        //add jquery selector for the data
        $data = $this->addJquerySelectorForData($data);
        // return the value
        return $data;
    }

    public function formatTemplateData(array $templateData): array
    {
        $data = [];
        //basic check
        if (empty($templateData)) {
            return $data;
        }
        $customFields = [];

        //take standard Fields
        $standardFields = $this->appendPlaceHolder($templateData['mapping']['contact']['standard']);

        if (!empty($templateData['mapping']['contact']['custom'])) {
            $customFields = $this->appendPlaceHolder($templateData['mapping']['contact']['custom']);
            // $customFields = $templateData['mapping']['contact']['custom'];
        }

        //merge them into single data
        $data['mapping']['contact'] = array_merge($standardFields, $customFields);
        $data['base'] = $templateData['base'];
        // return the value
        return $data;
    }


    /**
     * Helper function for 'formatTemplateData' function.
     * Appends Placeholder for HTML to all the generated fields(Standard, Technical, Custom, ECommerce)
     * @param array $fieldArray
     * @return array|null
     * @author Pradeep
     */
    public function appendPlaceHolder(array $fieldArray): ?array
    {
        $fieldsFromDB = $this->mioStandardFieldRepository->getAll();
        if (empty($fieldArray)) {
            return [];
        }

        if (empty($fieldsFromDB)) {
            return $fieldArray;
        }

        foreach ($fieldArray as &$sdField) {
            foreach ($fieldsFromDB as $fields) {
                if (!isset($sdField[$fields['fieldname']], $fields['htmllabel']) ||
                    empty($fields['htmllabel'])) {
                    continue;
                }
                $htmllabel = $fields['htmllabel'];
                $htmlPlaceHolder = explode('.', $htmllabel)[1];
                $sdField['placeholder'] = $htmlPlaceHolder;
                $sdField['category'] = $fields['fieldtype'] ?? '';
            }
        }
        unset($sdField);
        return $fieldArray;
    }

    /**
     * @param int $objectRegisterId
     * @return int
     * @author Aki
     */
    public function getLaterVersionAndNameOfCampaign(int $objectRegisterId):?array
    {
        try{
            $settings = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($objectRegisterId,
                ObjectRegisterMetaRepository::MetaKeyPage);

            $objectRegister = $this->objectRegisterRepository->getObjectregisterById($objectRegisterId);
            return [
                'name' => $settings['settings']['name'],
                'version' => (int)$this->objectRegisterRepository->getLatestVersion($objectRegister->getObjectUniqueId())
            ];
        }catch(Exception $e){
            $this->logger->error($e->getMessage());
            return null;
        }
    }

    /**
     * @param array $campaignSettings
     * @param array $oldSettings
     * @param int $campaignId
     * @param int $campaignVersion
     * @return array|null
     * @throws JsonException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @internal If null in json structure the wizard
     */
    private function createNewWorkflowVersion(array $campaignSettings,
                                              int   $campaignId,
                                              int   $campaignVersion
    ): ?array
    {
        $updateStatus = [];
        //update workflow
        if (!isset($campaignSettings['processhooks'])) {
            return null;
        }
        if (!empty($campaignSettings['processhooks'])) {
            $updateStatus = $this->processhookService->saveWorkflow($campaignId,
                $campaignSettings['processhooks'], $campaignVersion);
        }

        return $updateStatus;
    }

    private function archivePreviousVersionsOfDraftCampaign(string $objectUniqueId):bool
    {
        $status = false;
        if(empty($objectUniqueId)){
            return $status;
        }
        $this->objectRegisterRepository->archiveCampaignWithObjectUniqueID($objectUniqueId,
            StatusdefTypes::DRAFT);
        return true;
    }


    /**
     * @param array $data
     * @return array
     * @author Aki
     */
    private function addJquerySelectorForData(array $data):array
    {
        foreach ($data['mapping']['contact']['standard'] as $key => $value) {
            $fieldName = $value['field_name'];
            if($fieldName === 'salutation'){
                $jquerySelector = "$('#salutation input:radio').attr('checked', 'checked').val()";
            }else{
                $jquerySelector =  "$('#$fieldName').prop('value')";
            }
            $data['mapping']['contact']['standard'][$key]['jquerySelector'] = stripslashes($jquerySelector);
        }
        return $data;
    }
}
