<?php

namespace App\Services;

use App\Repository\ObjectRegisterMetaRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;

class JotFormService
{

    public const META_KEY_JOTFORM_ID = 'jotform_form_id';
    public const META_KEY_JOTFORM_USERNAME = 'jotform_user_name';
    public const META_KEY_JOTFORM_APIKEY = 'jotform_apikey';
    public const META_KEY_JOTFORM_REQUEST_URI = 'jotform_request_uri';
    public const META_KEY_JOTFORM_CONFIG = 'jotform_config';

    public function __construct(
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        LoggerInterface $logger,
        Connection $conn
    ) {
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->logger = $logger;
        $this->conn = $conn;
    }

    /**
     * @param int $oldObjRegId
     * @param int $newObjRegId
     * @return bool
     * @throws ConnectionException
     * @author Pradeep
     */
    public function addJotFormConfig(int $oldObjRegId, int $newObjRegId): bool
    {
        if ($oldObjRegId <= 0 || $newObjRegId <= 0) {
            return false;
        }
        $this->conn->beginTransaction();
        try {
            $jtMetaKeys = $this->getJotFormMetaKeys();
            foreach ($jtMetaKeys as $key) {
                if (empty($key)) {
                    continue;
                }
                // fetch meta_value from old Webhook.
                $ObjRegMetaDetails = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails($oldObjRegId,
                    $key);
                // check for metaValue.
                if (empty($ObjRegMetaDetails) || !isset($ObjRegMetaDetails[ 'meta_value' ]) ||
                    empty($ObjRegMetaDetails[ 'meta_value' ])) {
                    continue;
                }
                // insert meta_value to new webhook.
                if ($this->objectRegisterMetaRepository->insert($newObjRegId, $key,
                        $ObjRegMetaDetails[ 'meta_value' ]) === null) {
                    throw new Exception("Failed to insert jotform config to newly revised webhook.");
                }
            }
            $this->conn->commit();
            return true;
        } catch (JsonException | Exception | ConnectionException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $this->conn->rollBack();
            return false;
        }
    }

    /**
     * @return string[]
     * @author Pradeep
     */
    private function getJotFormMetaKeys()
    {
        return [
            self::META_KEY_JOTFORM_APIKEY,
            self::META_KEY_JOTFORM_CONFIG,
            self::META_KEY_JOTFORM_ID,
            self::META_KEY_JOTFORM_REQUEST_URI,
            self::META_KEY_JOTFORM_USERNAME,
        ];
    }
}