<?php


namespace App\Services;

use Psr\Log\LoggerInterface;
use App\Services\AppCacheService;
use RangeException;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CryptoService
{

    const SODIUM_KEY = "vF8hIURboMwry8OIbz_sdmky";

    private HttpClientInterface $httpClient;
    protected LoggerInterface $logger;
    protected AppCacheService $cacheService;
    private string $msSecureInOneUrl;

    /**
     * @param LoggerInterface               $logger
     * @param \App\Services\AppCacheService $cacheService
     * @param HttpClientInterface           $httpClient
     * @param string                        $msSecureInOneUrl see services*.yaml
     */
    public function __construct(
        LoggerInterface $logger,
        AppCacheService $cacheService,
        HttpClientInterface $httpClient,
        string $msSecureInOneUrl // see services*.yaml
    ) {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
        $this->cacheService = $cacheService;
        $this->msSecureInOneUrl = $msSecureInOneUrl;
    }


    /**
     * uses secure-in-one
     * @param string $string
     * @param bool   $returnb64 default=false
     * @return string either encoded string or encoded string base64-encoded
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function secureInOneEncryptString(string $string, bool $returnb64=false):string
    {
        $encString='';
        try {
            $response = $this->httpClient->request(
                'POST',
                $this->msSecureInOneUrl .'/secure/encstring',
                ['json' => ['string' => $string],]);
            $content = json_decode($response->getContent(), true, 10, JSON_OBJECT_AS_ARRAY);
            $encString = $content['string'];
        } catch (Exception $e) {
            $this->logger->error('Could not encode string', [__METHOD__,__LINE__]);
        }
        return ($returnb64) ? base64_encode($encString) : $encString;
    }


    /**
     * uses secure-in-one
     * @param string $string
     * @param bool   $returnb64  default=false
     * @return string either decoded string or decoded string base64-encoded
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function secureInOneDecryptString(string $string, bool $returnb64=false):string
    {
        $decString='';
        try {
            $response = $this->httpClient->request(
                'POST',
                $this->msSecureInOneUrl.'/secure/decstring',
                ['json' => ['string' => $string],]
            );
            $content = json_decode($response->getContent(), true, 10, JSON_OBJECT_AS_ARRAY);
            $decString = $content['string'];
        } catch (Exception $e) {
            $this->logger->error('Could not decode string', [__METHOD__,__LINE__]);
        }
        return ($returnb64) ? base64_encode($decString) : $decString;
    }


    /**
     * uses secure-in-one
     * @param string $string
     * @return string base64 encoded encrypted string or $string, if something went wrong
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function enCryptB64(string $string):string{
        $enc = $string;
        try {
            $enc = base64_encode($this->secureInOneEncryptString($string));
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $enc;
    }


    /**
     * uses secure-in-one
     * @param string $string base64 encoded encrypted string
     * @return string base64 decoded, decrypted string (input of ServerConfigurationRepository::enCryptB64) or $string, if something went wrong
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @see ServerConfigurationRepository::enCryptB64
     */
    public function deCryptB64(string $string):string{
        $dec = $string;
        try {
            $dec = $this->secureInOneDecryptString(base64_decode($string));
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $dec;
    }



    /**
     * Encrypt a message
     *
     * @param string $message - message to encrypt
     * @param string $key - encryption key
     * @return string
     * @throws RangeException
     */
    public function encrypt(string $message): string
    {
        $key = random_bytes(SODIUM_CRYPTO_SECRETBOX_KEYBYTES);

        if (mb_strlen($key, '8bit') !== SODIUM_CRYPTO_SECRETBOX_KEYBYTES) {
            throw new RangeException('Key is not the correct size (must be 32 bytes).');
        }
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);

        // apcu_store(md5(self::SODIUM_KEY), $key);
        $this->cacheService->addOrOverwrite(self::SODIUM_KEY, $key);

        $cipher = base64_encode(
            $nonce .
            sodium_crypto_secretbox(
                $message,
                $nonce,
                $key
            )
        );
        sodium_memzero($message);
        sodium_memzero($key);

        return $cipher;
    }

    /**
     * Decrypt a message
     *
     * @param string $encrypted - message encrypted with safeEncrypt()
     * @param string $key - encryption key
     * @return string
     * @throws Exception
     */
    public function decrypt(string $encrypted): ?string
    {
        if(empty($encrypted)) {
            return null;
        }

        // $key = apcu_fetch(md5(self::SODIUM_KEY));
        $key = $this->cacheService->get(self::SODIUM_KEY);
        $decoded = base64_decode($encrypted);
        $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
        $ciphertext = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');

        $plain = sodium_crypto_secretbox_open(
            $ciphertext,
            $nonce,
            $key
        );
        if (!is_string($plain)) {
            throw new Exception('Invalid MAC');
        }
        sodium_memzero($ciphertext);
        sodium_memzero($key);
        return $plain;
    }


    public function simpleEncrypt(string $string):string
    {
        // return base64_encode(gzcompress(str_rot13($string)));
        return base64_encode(str_rot13($string));
    }


    public function simpleDecrypt(string $string):string
    {
        //return str_rot13(gzuncompress(base64_decode($string)));
        return str_rot13(base64_decode($string));
    }

    /**
     *
     */
    public function deleteSodiumKey()
    {
        $this->cacheService->del(self::SODIUM_KEY);
    }


    public function simplePwGen(int $len): string
    {
        $chars =  'abcdefghijklmnopqrstuvwxyz#*!$%:;1234567890';

        $pw = '';
        for($i=0; $i<$len;$i++) {
            $c = $chars[random_int(0, strlen($chars)-1)];
            $pw .= (random_int(0,1) === 0 ) ? $c : strtoupper($c);
        }

        return $pw;
    }



}
