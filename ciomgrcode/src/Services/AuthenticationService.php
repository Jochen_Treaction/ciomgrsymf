<?php

namespace App\Services;

use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Users;
use Psr\Log\LoggerInterface;
use App\Entity\Securelog; // might not be used
use App\Entity\Oauth2Tokens; // might not be used
use App\Repository\FeatureToggleRepository;
use App\Repository\SeedPoolRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\CompanyRepository;

/**
 * Class AuthenticationService
 * CURRENTLY A SPOOF => TODO: Implement
 * @package App\Services
 */
class AuthenticationService extends AbstractController
{
    public $db;
    private $apcuCache;
    private $usersRepository;
    private $logger;
    private array $features;
    private SeedPoolRepository $seedPoolRepository;
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    private CompanyRepository $companyRepository;


    public function __construct(
        AppCacheService $apcuCache,
        UsersRepository $usersRepository,
        FeatureToggleRepository $featureToggleRepository,
        SeedPoolRepository $seedPoolRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        CompanyRepository $companyRepository,
        LoggerInterface $logger
    ) {
        $this->objectRegisterMetaRepository =$objectRegisterMetaRepository;
        $this->companyRepository = $companyRepository;
        $this->seedPoolRepository =$seedPoolRepository;
        $this->features = $featureToggleRepository->getFeatureToggles();
        $this->apcuCache = $apcuCache;
        $this->usersRepository = $usersRepository;
        $this->logger = $logger;
    }


    /**
     * @param int $company_id
     * @return bool
     */
    private function getAdvertorialStatus(int $company_id ):bool
    {
        $companyObjectRegisterId = $this->companyRepository->getCompanyById($company_id)->getObjectregister()->getId();
        $objRegMetaAdvertorial = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails($companyObjectRegisterId, $this->objectRegisterMetaRepository::METAKEY_ADVERTORIAL_STATUS);
        if(empty($objRegMetaAdvertorial)) {
            $status = false;
        } else {
            $status = ($objRegMetaAdvertorial['meta_value'] === '1') ? true : false;
        }
        $this->logger->info('status getAdvertorialStatus ', [$status, $company_id,__METHOD__, __LINE__]);
        return (bool) $status;
    }


    /**
     * check function
     *
     * @return bool
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function check():bool
    {
        return (!empty($_SESSION['user_id']) || !empty($_SESSION['acc_id']));
    }


    /**
     * getAuthIds function
     *
     * @return array
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function getAuthIds():array
    {
        // TODO: remove comment
//        if (!($_SESSION['user_id']) && !($_SESSION['acc_id'])) {
//            return false;
//        }

        return [
            'user_id' => $_SESSION['user_id'],
            'acc_id' => $_SESSION['acc_id'],
            'acc_name' => $_SESSION['acc_name'],
            'super_admin' => $_SESSION['super_admin'],
        ];
    }


    /**
     * @return string|null
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function user():?string
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }

        return null;
    }


    /**
     * @return string|null
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function accName():?string
    {
        if (isset($_SESSION['acc_name'])) {
            return $_SESSION['acc_name'];
        }

        return null;
    }


    /**
     * @return string|null
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function checkSuperAdmin():?string
    {
        if (isset($_SESSION['super_admin'])) {
            return $_SESSION['super_admin'];
        }

        return null;
    }


    /**
     * validateUserSignIn function
     * 1. Validates User Email with Password.
     * 2. Checks if the User is Blocked
     * 3. Checks if the User is Activated or Not activated.
     *
     * @param array $params
     * @return boolean
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function validateUserSignIn(array $params): bool
    {
        // TODO: uncomment
        /*
        $status = false;
        if (empty($params) || !filter_var($params['email'], FILTER_VALIDATE_EMAIL) || empty($params['password'])) {
            return $status;
        }

        $user_details = $this->getUserDetailsByEmail($params['email']);
        if (!is_array($user_details) || empty($user_details['id'])) {
            $_SESSION['error'] = "Email address is not registered";
            return false;
        }

        // Check for Blocked User
        if ($user_details["status"] === "blocked") {
            $_SESSION['error'] = "Your is currently blocked";
            return false;
        }
        // Check for Non Activated User
        if ($user_details["status"] === "new") {
            $_SESSION['error'] = "Your account is not activated, Please check your inbox for Activation link";
            return false;
        }

        //code added for salt and pepper
        //checking for date created
        if ($user_details['created'] > "2019-08-15") {
            $UserPassword = $params["password"];
            $pepper = '2015';
            $salt = 'treactionA.G';
            $peperPassword = ($pepper . $UserPassword . $salt);
        } else {
            $peperPassword = $params["password"];
        }

        // Validate Password
        if (!password_verify($peperPassword, $user_details["password"])) {
            $this->userLoginFailed($user_details);
            $_SESSION['error'] = "Email address and/or password do not match";
            return false;
        }

        $_SESSION['user'] = $user_details['first_name'] . ' ' . $user_details['last_name'];
        $_SESSION['user_id'] = $user_details['id'];
        $_SESSION['super_admin'] = $user_details['super_admin'];
        $_SESSION['comp_id'] = (int)$user_details['company_id'];
        $_SESSION['acc_id'] = $this->getAccountNumberByCompanyId((int)$user_details['company_id']);
        $_SESSION['acc_name'] = $this->getCompanyDetailsByAccountId($_SESSION['acc_id'])['name'];

        $status = true;

        return status;
        */
        return true;
    }


    /**
     * userLoginFailed function
     *
     * @param array $user_details
     * @return boolean
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function userLoginFailed(array $user_details): bool
    {
        if (empty($user_details)) {
            return false;
        }

        $failed_logins = $user_details['failed_logins'];

        if ((int)$failed_logins >= 5) {
            // User is blocked
            $this->blockUser($user_details);
            return false;
        }

        // $id = $user_details['id'];
        $users = $this->getDoctrine()->getRepository(Users::class)->find($user_details['id']);
        return $users->setFailedLogins($failed_logins);


//        (int)$failed_logins = (int)$failed_logins + 1;
//        $qry = "UPDATE `cio.users` set `failed_logins` = $failed_logins where `id` = $id";
//        $result = $this->excuteQuerySET($qry);


//        return $result;
    }


    /**
     * blockUser function
     *
     * @param array $params
     * @return boolean
     */
    public function blockUser(array $params): bool
    {
        // $qry = "UPDATE `cio.users` set `status` = 'blocked' where `id` = ";

        try {
            if (empty($params) || empty($params['id'])) {
                throw new Exception('Empty array sent to query');
            }

            $users = $this->getDoctrine()->getRepository(Users::class)->find($params['id']);
            $result = $users->setStatus('blocked');
            // todo: persist???

            // $qry .= $params['id'];
            // $result = $this->excuteQuerySET($qry);
            $_SESSION['blocked'] = "User has been blocked";
        } catch (\Exception $ex) {
            // echo "Caught Exception:" . $ex->getMessage();
            die($ex->getCode());
        }
        return $result;
    }


    /**
     * unblockUser function
     *
     * @param array $params
     * @return boolean
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function unblockUser(array $params): bool
    {
        // $qry = "UPDATE `cio.users` set `status` = 'active' where `id` = ";

        try {
            if (empty($params) || empty($params['id'])) {
                throw new Exception('Empty array sent to query');
            }


            $users = $this->getDoctrine()->getRepository(Users::class)->find($params['id']);
            $result = $users->setStatus('active');
            // todo: persist???


//            $qry .= $params['id'];
//            $result = $this->excuteQuerySET($qry);
        } catch (Exception $ex) {
            // echo "Caught Exception:" . $e->getMessage();
            die($ex->getCode());
        }
        return $result;
    }


    /**
     * blockAccount function
     *
     * @param array $params
     * @return boolean
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function blockAccount(array $params): bool
    {
        // TODO: leave for possible later use
        /*
        $update = $this->getUpdateDetials();
        $qry = "UPDATE `cio.users` set `status` = 'blocked', `update` = '" . $update['date'] . "' where `company_id` = ";
        try {
            if (empty($params) || empty($params['account_no'])) {
                throw new Exception('Empty array sent to query');
            }

            $qry .= $params['account_no'];
            $result = $this->excuteQuerySET($qry);
            $_SESSION['Company'] = "All the Users under this company has been blocked";
        } catch (Exception $ex) {
            //  echo "Caught Exception:" . $ex->getMessage();
            die($ex->getCode());
        }
        return $result;
        */
        return true;
    }


    /**
     * unBlockAccount function
     *
     * @param array $params
     * @return boolean
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function unBlockAccount(array $params): bool
    {
        // TODO: leave for possible later use
        /*
        $update = $this->getUpdateDetials;
        $qry = "UPDATE `cio.users` set `status` = 'active', `update` = '" . $update['date'] . "'  where `company_id` = ";

        try {
            if (empty($params) || empty($params['account_no'])) {
                throw new Exception('Empty array sent to query');
            }

            $qry .= $params['account_no'];
            $result = $this->excuteQuerySET($qry);
        } catch (Exception $ex) {
            echo "Caught Exception:" . $e->getMessage();
            die();
        }
        return $result;
        */
        return true;
    }


    /**
     * signOut function
     *
     * @return bool
     * @deprecated <b style="color:red">since 2021-11-17 (jsr), used nowhere</b>
     */
    public function signOut()
    {
        unset($_SESSION['user']);
        unset($_SESSION['user_id']);
        unset($_SESSION['super_admin']);
        unset($_SESSION['acc_id']);
        unset($_SESSION['acc_name']);

        return true;
    }


    /**
     * <b style="color:red">Heavy usage in all Controllers !!!</b><br>
     * tries to get user data from cache. if not found, tries to get user data from db, set's it to cache and returns the user data
     * @param string $user_email
     * @return array <code>[user_id, user_1stletter, first_name, last_name, user_roles (string [\"ROLE_MASTER_ADMIN\", \"ROLE_ADMIN\", \"ROLE_USER\"]), email, company_id, company_name, company_account_no, is_user, is_admin, is_master_admin, is_super_admin, login_ts, login_expires_ts, account_objectregister_id, project_objectregister_id, has_seedpool]</code>  for app.twig
     * @author jsr
     *
     */
    public function getUserParametersFromCache(string $user_email = ''): array
    {
        // early exit
        if (empty($user_email)) {
            return [];
        }

        $user_data = $this->apcuCache->get($user_email);

        if ($user_data) {
            $this->setRoles($user_data);
            return $user_data;
        } else {
            // try to get data from database
            $user_data = $this->usersRepository->getUserAndCompanyForApcu($user_email);

            if (!empty($user_data)) {
                $roles = json_decode($user_data['user_roles']);
                $is_user = (in_array('ROLE_USER', $roles)) ? true : false;
                $is_admin = (in_array('ROLE_ADMIN', $roles)) ? true : false;
                $is_master_admin = (in_array('ROLE_MASTER_ADMIN', $roles)) ? true : false;
                $is_super_admin = (in_array('ROLE_SUPER_ADMIN', $roles)) ? true : false;

                $user_data['is_user'] = ($is_user && !$is_admin && !$is_master_admin && !$is_super_admin);
                $user_data['is_admin'] = ($is_user && $is_admin && !$is_master_admin && !$is_super_admin);
                $user_data['is_master_admin'] = ($is_user && $is_admin && $is_master_admin && !$is_super_admin);
                $user_data['is_super_admin'] = ($is_user && $is_admin && $is_master_admin && $is_super_admin);

                $current_time = new \DateTime('now');
                $user_data['login_ts'] = $current_time->format('Y-m-d H:i:s');
                $user_data['login_expires_ts'] = $current_time->modify('+ 2 hour')->format('Y-m-d H:i:s');
                $user_data['feature'] = $this->features; // new from 2021-11-17 on: add featureToggles to current_user.feature.<feature name> for use in TWIG
                $user_data['app_env'] = $_ENV['APP_ENV']; //substr($_ENV['APP_ENV'], 0, 3);
                $user_data['has_seedpool_permission'] = $this->seedPoolRepository->hasSeedPool($user_data['company_id']);

                try {
                    $user_data['has_advertorial_permission'] = $this->getAdvertorialStatus($user_data['company_id']);
                } catch (\Exception $e) {
                    $user_data['has_advertorial_permission'] = false;
                }

                $this->logger->info('$user_data = ', [$user_data, __METHOD__, __LINE__]);
                $this->apcuCache->addOrOverwrite($user_email, $user_data);
            }
            $this->logger->info('$user_data = ', [$user_data, __METHOD__, __LINE__]);
            return $user_data;
        }
    }


    /**
     * @internal WEB-3850
     * @param array $user_data_in
     * @return array, [] in case of error, else updated record from getUserAndCompanyForApcu
     * @author jsr
     */
    public function updateUserParametersInCache(array $user_data_in): array
    {
        if (empty($user_data_in['email'])) {
            $this->logger->error('call to ' . __METHOD__ . ' without user_data_in[email]', [__METHOD__, __LINE__]);
            return [];
        }

        $user_data_out = $this->usersRepository->getUserAndCompanyForApcu($user_data_in['email']);

        if (!empty($user_data_in) && !empty($user_data_out)) {
            foreach ($user_data_out as $key => $value) {
                $user_data_out[$key] = (!empty($user_data_in[$key])) ? $user_data_in[$key] : $user_data_out[$key];
            }
            $this->setRoles($user_data_out);
            $user_data_out['feature'] = $this->features;
            $this->apcuCache->addOrOverwrite($user_data_in['email'], $user_data_out);
        }
        // return $this->apcuCache->get($user_data_in['email']);
        return $this->getUserParametersFromCache($user_data_in['email']);
    }


    /**
     * @internal WEB-3850
     * @param array $user_data
     * @author jsr
     */
    private function setRoles(array &$user_data)
    {
        if (!empty($user_data) && !empty($user_data['user_roles'])) {
            $roles = json_decode($user_data['user_roles']);

            $is_user = (in_array('ROLE_USER', $roles)) ? true : false;
            $is_admin = (in_array('ROLE_ADMIN', $roles)) ? true : false;
            $is_master_admin = (in_array('ROLE_MASTER_ADMIN', $roles)) ? true : false;
            $is_super_admin = (in_array('ROLE_SUPER_ADMIN', $roles)) ? true : false;

            $user_data['is_user'] = ($is_user && !$is_admin && !$is_master_admin && !$is_super_admin);
            $user_data['is_admin'] = ($is_user && $is_admin && !$is_master_admin && !$is_super_admin);
            $user_data['is_master_admin'] = ($is_user && $is_admin && $is_master_admin && !$is_super_admin);
            $user_data['is_super_admin'] = ($is_user && $is_admin && $is_master_admin && $is_super_admin);
        }
    }
}
