<?php


namespace App\Services;

use Exception;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Swift_Mailer;
use Swift_Message;
use App\Types\EmailContentTypes;
use App\Types\MessageParameterTypes;
use App\Types\MsMailInterfaceTypes;
use App\Services\EmailService;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


class SwiftMailerService implements LoggerAwareInterface
{

    use LoggerAwareTrait;
    protected const ClientSuccess = 'client-success@treaction.net';
    protected const SupportMIO ='support@dev-campaign-in-one.net';


    /**
     * @var Swift_Mailer
     */
    private $mail;
    private $emailService;
	private  $emailContentTypes;
	private  $messageParameterTypes;
	private  $msMailInterfaceTypes;


    /**
     * @var LoggerInterface
     */


    public function __construct(Swift_Mailer $mail, EmailService $emailService, EmailContentTypes $emailContentTypes, MessageParameterTypes $messageParameterTypes, MsMailInterfaceTypes $msMailInterfaceTypes)
    {
        $this->mail = $mail;
        $this->emailService = $emailService;
        $this->emailContentTypes = $emailContentTypes;
        $this->messageParameterTypes = $messageParameterTypes;
        $this->msMailInterfaceTypes = $msMailInterfaceTypes;
    }


    /**
     * @param string $email
     * @param string $last_name
     * @return bool
     * @author jsr
     */ // SENDMAIL
    public function sendActivationLink(string $email, string $first_name, string $last_name, string $activation_url, string $accountNo=null): array
    {
        if (empty($email) || empty($activation_url)) {
            return ['ret' => false, 'msg' => 'missing email or missing activation url'];
        }

		// ----------------------------------------------------
		// $this->emailContentTypes->accountSendActivationlink;
		// ----------------------------------------------------
		$this->messageParameterTypes->email = $email;
		$this->messageParameterTypes->accountNo = '#'.$accountNo;
		$this->messageParameterTypes->firstName = $first_name;
		$this->messageParameterTypes->lastName = $last_name;
		$this->messageParameterTypes->activationLink = $activation_url;

		try {
			$success = $this->emailService->sendByMsMail(
				[$email],
				'Please activate your Marketing-In-One account',
				$this->emailContentTypes->accountSendActivationlink,
				$this->messageParameterTypes,
				$retMsg
			);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
    }


    /**
     * @param string $emailto
     * @param string $emailFrom
     * @param string $messageSubject
     * @param string $messageBody
     * @return int
     * @author jsr
     */
    public function sendEmail(string $emailto, string $emailFrom, string $messageSubject, string $messageBody): array
    {
        $this->logger->info('TRY TO SEND EMAIL ', [$emailto, $emailFrom, $messageSubject, $messageBody],
            [__METHOD__, __LINE__]);
        try {
            $message = (new Swift_Message($messageSubject))
                ->setFrom($emailFrom)
                ->setTo($emailto)
                ->setBody($messageBody, 'text/html');
            $failure = '';
            $ret = $this->mail->send($message, $failure);
            $this->logger->info('SWITMAILER RETURNED ', [$ret], [__METHOD__, __LINE__]);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }


        return ['ret' => $ret, 'failure' => $failure];
    }


    /**
	 * TODO: add template and missing placeholders in MSMAIL
	 * @internal COMMENT: currently not used (2021-03), since "manage users is deactivated"
     * @param string $email
     * @param string $initalPassword
     * @param string $salutation
     * @param string $last_name
     * @param string $activation_url
     * @param array $invitedBy
     * @return array
     * @internal WEB-3850
     */ // SENDMAIL
    public function sendInvitationLink(
        string $email,
        string $initalPassword,
        string $salutation,
        string $last_name,
        string $activation_url,
        array $invitedBy
    ): array {
        if (empty($email) || empty($activation_url)) {
            return ['ret' => 0, 'failure' => ['missing email or', 'missing activation url']];
        }

        $date = date("Y-m-d H:i:s", strtotime('+8 hours'));
        $messageSubject = 'Campaign-In-One Invitation Link';
        $messageBody = "Hello {$salutation} {$last_name},\n"
                       . "{$invitedBy['first_name']} {$invitedBy['last_name']} from {$invitedBy['company_name']} invited you as member to Campaign-In-One.\n"
                       . "To enable your membership please click or copy the following activation link to your browsers address field:\n$activation_url\n\n"
                       . "The activation link is valid until {$date} and expires afterwards.\n"
                       . "Please change your inital password as soon as possible! Your inital password is:\n{$initalPassword}";

        return $this->sendEmail($email, self::SupportMIO, $messageSubject, $messageBody);
    }


    /**
	 * TODO: add template and missing placeholders in MSMAIL
	 * @internal COMMENT: currently not used (2021-03), since "manage users is deactivated"
     * sendInvitationNotification
     * @param string $toEmail
     * @param string $toEmailSalutation
     * @param string $toEmailLastName
     * @param string $invitedToCompany
     * @param array $invitedByUser
     * @return array
     * @internal WEB-3850
     */ // SENDMAIL
    public function sendInvitationNotification(
        string $toEmail,
        string $toEmailSalutation,
        string $toEmailLastName,
        string $invitedToCompany,
        array $invitedByUser
    ): array {
        if (empty($toEmail)) {
            return ['ret' => 0, 'failure' => 'missing email'];
        }

        $messageSubject = 'Campaign-In-One Invitation Notification';
        $messageBody = "Hello {$toEmailSalutation} {$toEmailLastName},\n"
                       . "{$invitedByUser['first_name']} {$invitedByUser['last_name']} from {$invitedByUser['company_name']} invited you as member of company {$invitedToCompany} to Campaign-In-One.\n"
                       . "To access {$invitedToCompany}'s data, simply login with your current credentials.\n";

        return $this->sendEmail($toEmail, self::SupportMIO, $messageSubject, $messageBody);
    }


    /**
     * @param string $email
     * @param string $fullname
     * @param string $pw_reset_url
     * @return bool|int
     * @author jsr
     */ // SENDMAIL
    public function sendPWResetLink(string $email, string $firstName, string $lastName, string $pw_reset_url): array
    {
        if (empty($email) || empty($pw_reset_url)) {
            return ['ret' => 0, 'msg' => 'missing email or missing reset url'];
        }

		// -------------------------------------------------------
		// $this->emailContentTypes->accountSendPasswordResetLink;
		// -------------------------------------------------------
		$this->messageParameterTypes->firstName = $firstName;
		$this->messageParameterTypes->lastName = $lastName;
		$this->messageParameterTypes->activationLink = $pw_reset_url;

		try {
			$success = $this->emailService->sendByMsMail(
				[$email],
				'Please change your Marketing-In-One password',
				$this->emailContentTypes->accountSendPasswordResetLink,
				$this->messageParameterTypes,
				$retMsg
			);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
    }


    /**
     * @param array $userInformation
     * @param array $companyInformation
     * @return array
     * @author Pradeep
     */ // SENDMAIL
    public function sendNewAccountNotification(array $userInformation, array $companyInformation): array // SENDMAIL
    {
        if (empty($userInformation) || empty($companyInformation)) {
			return ['ret' => false, 'msg' => 'missing user information or missing company information'];
        }

		// ----------------------------------------------------------
		// $this->emailContentTypes->accountRegistrationNotification;
		// ----------------------------------------------------------
		$this->messageParameterTypes->email = $userInformation['email'];
		$this->messageParameterTypes->firstName = $userInformation['first_name'];
		$this->messageParameterTypes->lastName = $userInformation['last_name'];
		$this->messageParameterTypes->accountNo = '#'.$userInformation['company_account_no'] . ', ' . $companyInformation['company_name'];

		try {
			$success = $this->emailService->sendByMsMail(
				['client-success@treaction.net'],
				'New Registration in Marketing-In-One',
				$this->emailContentTypes->accountRegistrationNotification,
				$this->messageParameterTypes,
				$retMsg
			);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
    }

    /**
     * @param array $company
     * @param array $user
     * @param string $apikey
     * @return array
     * @author Pradeep,jsr
     */ // SENDMAIL
    public function sendAPIKeyToClient(array $company, array $user, string $apikey): array
    {
        $status = false;

        if (empty($company) || empty($user) || empty($apikey)) {
            $this->logger->error('Invalid configuration', [__CLASS__, __METHOD__, __LINE__]);
            return ['ret' => false, 'msg' => 'missing user, missing company or missing apikey information'];
        }

		// ----------------------------------------------
		// $this->emailContentTypes->accountVerifyApikey;
		// ----------------------------------------------
		$this->messageParameterTypes->email = $user[ 'email' ];
		$this->messageParameterTypes->firstName = $user[ 'first_name' ];
		$this->messageParameterTypes->lastName = $user[ 'last_name' ];
		$this->messageParameterTypes->apiKey = "XXXXX-XXXX-XXXX-" . substr($apikey, - 4);
		$this->messageParameterTypes->activationLink = $this->getServerUrl() . "?redirect=" . base64_encode('acc.getSettingsGeneral');
		$this->messageParameterTypes->accountNo = '#'.$company['account_no'];

		try {
			$success = $this->emailService->sendByMsMail(
				[$user[ 'email' ]],
				'Please get your Marketing-In-One ApiKey',
				$this->emailContentTypes->accountVerifyApikey,
				$this->messageParameterTypes,
				$retMsg
			);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
    }


	/**
	 * @param array  must contain accountNocat
	 * @param array $user must contain first_name, last_name, email
	 * @return array
	 * @author jsr
	 */ // SENDMAIL
	public function sendAccountDeletionNotification(array $company, array $user): array
	{
		$status = false;

		if (empty($company) || empty($user)) {
			$this->logger->error('Invalid configuration', [__CLASS__, __METHOD__, __LINE__]);
			return ['ret' => false, 'msg' => 'missing user or missing company information'];
		}

		$this->logger->info('$company', [$company, __FUNCTION__,__LINE__]);
		$this->logger->info('$user', [$user, __FUNCTION__,__LINE__]);

		// -----------------------------------------------------
		// $this->emailContentTypes->accountDeletionNotification
		// -----------------------------------------------------
		$this->messageParameterTypes->firstName = $user['first_name'];
		$this->messageParameterTypes->lastName = $user['last_name'];
		$this->messageParameterTypes->accountNo = '#' . $company['account_no'];
		$this->messageParameterTypes->customContent = $company['delete_date'];
		$this->messageParameterTypes->activationLink = $this->getServerUrl(). '?redirect='.base64_encode('user.unblockaccount');

		try {
			$success = $this->emailService->sendByMsMail(
				[$user['email']],
				'Your Marketing-In-One account has been deleted',
				$this->emailContentTypes->accountDeletionNotification,
				$this->messageParameterTypes,
				$retMsg
			);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
	}

	/**
	 * @param array  must contain accountNo
	 * @param array $user must contain first_name, last_name, email
	 * @param string $activationLink
	 * @return array
	 * @author jsr
	 */ // SENDMAIL
	public function sendAccountActivationReminder(array $company, array $user): array
	{
		$status = false;


		if (empty($company) || empty($user)) {
			$this->logger->error('Invalid configuration', [__CLASS__, __METHOD__, __LINE__]);
			return ['ret' => false, 'msg' => 'missing user or missing company information'];
		}

		// -------------------------------------------------------
		// $this->emailContentTypes->accountSendActivationReminder
		// -------------------------------------------------------
		$this->messageParameterTypes->firstName = $user[ 'first_name' ];
		$this->messageParameterTypes->lastName = $user[ 'last_name' ];
		$this->messageParameterTypes->accountNo = '#'.$company['account_no'];
		$this->messageParameterTypes->activationLink = $this->getServerUrl(). '?redirect='.base64_encode('acc_dpa_accept');

		try {
			$success = $this->emailService->sendByMsMail(
				[$user[ 'email' ]],
				'Please finish setting up your Marketing-In-One Account',
				$this->emailContentTypes->accountFinishSetupNotification,
				$this->messageParameterTypes,
				$retMsg
			);

			$this->logger->info('$success', [$success, __METHOD__, __LINE__]);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
	}


	/**
	 * @param array  must contain accountNo
	 * @param array $user must contain first_name, last_name, email
	 * @param string $activationLink
	 * @return array
	 * @author jsr
	 */ // SENDMAIL
	public function sendUserSetPasswordReminder(array $company, array $user): array
	{
		$status = false;


		if (empty($company) || empty($user)) {
			$this->logger->error('Invalid configuration', [__CLASS__, __METHOD__, __LINE__]);
			return ['ret' => false, 'msg' => 'missing user or missing company information'];
		}

		// -------------------------------------------------------
		// $this->emailContentTypes->accountSendActivationReminder
		// -------------------------------------------------------
		$this->messageParameterTypes->firstName = $user[ 'first_name' ];
		$this->messageParameterTypes->lastName = $user[ 'last_name' ];
		$this->messageParameterTypes->accountNo = '#'.$company['account_no'];
		$this->messageParameterTypes->activationLink = $user['activation_link']; //  	acc.getupdate 	/accountupdate

		try {
			$success = $this->emailService->sendByMsMail(
				[$user[ 'email' ]],
				'Please activate your Marketing-In-One account',
				$this->emailContentTypes->accountSendActivationReminder,
				$this->messageParameterTypes,
				$retMsg
			);

			$this->logger->info('$success', [$success, __METHOD__, __LINE__]);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
	}





	/**
	 * @param array                $user must contain first_name, last_name, email
	 * @param MsMailInterfaceTypes $msMailInterfaceTypes configuration settings of mail server in UI form of routes <pre>/account/integration/emailserver</pre> or <pre>/admintool/super/emailserver</pre>
	 * @return array
	 * @throws Exception
	 * @author jsr
	 */ // SENDMAIL
	public function sendAccountTestMailserverNotification(array $user, MsMailInterfaceTypes $msMailInterfaceTypes=null): array
	{
		$status = false;

		if (empty($user)) {
			$this->logger->error('Invalid configuration', [__CLASS__, __METHOD__, __LINE__]);
			return ['ret' => false, 'msg' => 'missing user information'];
		}

		$myMsMailInterfaceTypes = (null === $msMailInterfaceTypes ) ? $this->msMailInterfaceTypes:  $msMailInterfaceTypes;

		// -----------------------------------------------------------
		// $this->emailContentTypes->accountTestMailserverNotification
		// -----------------------------------------------------------
		$this->messageParameterTypes->firstName = $user[ 'first_name' ];
		$this->messageParameterTypes->lastName = $user[ 'last_name' ];

		try {
			$success = $this->emailService->sendByMsMail(
				[$user[ 'email' ]],
				'Your Marketing-In-One account has been deleted',
				$this->emailContentTypes->accountTestMailserverNotification,
				$this->messageParameterTypes,
				$retMsg,
				$myMsMailInterfaceTypes
			);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
	}


	/**
	 * @param array  $user must contain first_name, last_name, email, company_account_no
	 * @param string $customContent
	 * @return array
	 * @throws Exception
	 * @internal COMMENT: account is marked for deletion. User can still logon within 30 days.
	 *
	 * @author   jsr
	 */ // SENDMAIL
	public function sendAccountReactivationNotification(array $user, string $customContent): array
	{
		$status = false;

		if (empty($user)) {
			$this->logger->error('Invalid configuration', [__CLASS__, __METHOD__, __LINE__]);
			return ['ret' => false, 'msg' => 'missing user information'];
		}

		// ---------------------------------------------------------
		// $this->emailContentTypes->accountReactivationNotification
		// ---------------------------------------------------------
		$this->messageParameterTypes->firstName = $user[ 'first_name' ];
		$this->messageParameterTypes->lastName = $user[ 'last_name' ];
		$this->messageParameterTypes->accountNo = '#'.$user['company_account_no'];
		$this->messageParameterTypes->customContent = $customContent;

/*
[user_id, user_1stletter, first_name, last_name, user_roles (string [\"ROLE_MASTER_ADMIN\", \"ROLE_ADMIN\", \"ROLE_USER\"]), email, company_id, company_name, company_account_no, is_user, is_admin, is_master_admin, is_super_admin, login_ts, login_expires_ts, account_objectregister_id, project_objectregister_id]
 */

		try {
			$success = $this->emailService->sendByMsMail(
				[$user['email']],
				'Request for Reactivation of User Account',
				$this->emailContentTypes->accountReactivationNotification,
				$this->messageParameterTypes,
				$retMsg
			);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
	}


	/**
	 * sendAccountFinishSetupNotification
	 * @param array  $user must contain first_name, last_name, email, company_account_no
	 * @param string $customContent
	 * @return array
	 * @throws Exception
	 *
	 * @author   jsr
	 */ // SENDMAIL
	public function sendAccountFinishSetupNotification(array $user): array
	{
		$status = false;

		if (empty($user)) {
			$this->logger->error('Invalid configuration', [__CLASS__, __METHOD__, __LINE__]);
			return ['ret' => false, 'msg' => 'missing user information'];
		}

		// --------------------------------------------------------
		// $this->emailContentTypes->accountFinishSetupNotification
		// --------------------------------------------------------
		$this->messageParameterTypes->firstName = $user[ 'first_name' ];
		$this->messageParameterTypes->lastName = $user[ 'last_name' ];
		$this->messageParameterTypes->accountNo = '#'.$user['company_account_no'];
		$this->messageParameterTypes->activationLink = $this->getServerUrl(). '?redirect='.base64_encode('acc_dpa_accept'); //  	acc.getupdate 	/accountupdate

/*
[user_id, user_1stletter, first_name, last_name, user_roles (string [\"ROLE_MASTER_ADMIN\", \"ROLE_ADMIN\", \"ROLE_USER\"]), email, company_id, company_name, company_account_no, is_user, is_admin, is_master_admin, is_super_admin, login_ts, login_expires_ts, account_objectregister_id, project_objectregister_id]
 */

		try {
			$success = $this->emailService->sendByMsMail(
				[$user['email']],
				'Please finish setting up your Marketing-In-One Account',
				$this->emailContentTypes->accountFinishSetupNotification,
				$this->messageParameterTypes,
				$retMsg
			);
		} catch (ClientExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (RedirectionExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (ServerExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		} catch (TransportExceptionInterface $e) {
			throw new Exception("error: ".$e->getMessage()."/{$retMsg}, code: ".$e->getCode());
		}

		return ['ret' => $success, 'msg' => $retMsg];
	}


	private function getServerUrl():string
	{
		return "https://".$_SERVER['HTTP_HOST'];
	}
}
