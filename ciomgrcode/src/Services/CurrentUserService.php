<?php

namespace App\Services;

use Psr\Log\LoggerInterface;
use App\Services\AppCacheService;
use App\Services\AuthenticationService;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class CurrentUserService
{

    protected AuthenticationUtils $authenticationUtils;
    protected AuthenticationService $authenticationService;
    protected AppCacheService $apcuCacheService;
    protected LoggerInterface $logger;


    public function __construct(AuthenticationUtils $authenticationUtils, AuthenticationService $authenticationService, AppCacheService $apcuCacheService, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->apcuCacheService = $apcuCacheService;
    }


    /**
     * returns array with current user data:<br><code>[user_id, user_1stletter, first_name, last_name, user_roles (string [\"ROLE_MASTER_ADMIN\", \"ROLE_ADMIN\", \"ROLE_USER\"]), email, company_id, company_name, company_account_no, is_user, is_admin, is_master_admin, is_super_admin, login_ts, login_expires_ts, account_objectregister_id, project_objectregister_id]</code>
     * @return array
     */
    public function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {

            $company_id = $this->apcuCacheService->get($this->apcuCacheService::SELECTED_COMPANY_ID_OF_USER.$user['email']);
            if(false !== $company_id) {
                $user['company_id'] = $company_id;
            }

            return $user;
        }
        return [];
    }
}
