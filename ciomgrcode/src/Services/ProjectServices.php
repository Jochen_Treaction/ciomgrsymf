<?php


namespace App\Services;

use App\Entity\ObjectRegisterMeta;
use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ObjectRegisterRepository;
use App\Types\UniqueObjectTypes;
use Psr\Log\LoggerInterface;
use App\Repository\ObjectRegisterMetaRepository;

class ProjectServices
{
    /**
     * @var App\Repository\CampaignRepository
     */
    private $campaignRepository;

    /**
     * @var App\Repository\ObjectRegisterRepository
     */
    private $objectRegisterRepository;

    /**
     * @var Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var App\Repository\ObjectRegisterMetaRepository
     */
    private $objectRegisterMetaRepository;
    private $companyRepository;


    public function __construct(CampaignRepository $campaignRepository,
                                ObjectRegisterRepository $objectRegisterRepository,
                                LoggerInterface $logger,
                                ObjectRegisterMetaRepository $objectRegisterMetaRepository,
                                CompanyRepository $companyRepository
    )
    {
        $this->logger = $logger;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->campaignRepository = $campaignRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->companyRepository = $companyRepository;
    }


    /**
     * create ObjectRegister entry for a new project and create project entry in campaign table for the submitted account => $company_id
     * @param int $company_id
     * @return int|null campaign.id of the project |null
     */


    /**
     * create ObjectRegister entry for a new project, create project entry in campaign table for the submitted account => $company_id, create default settings in table campaign_params for project
     * @param int $company_id
     * @param string $client_ipv4_address
     * @return int|null campaign.id of the project |null
     */
    public function createProject(int $company_id, string $client_ipv4_address = null): ?int
    {

        // create project
        $objectregister_id = $this->objectRegisterRepository->createInitialObjectRegisterEntry($company_id, UniqueObjectTypes::PROJECT);
        $this->logger->info('$objectregister_id', [$objectregister_id, __METHOD__, __LINE__]);
        //create new campaign
        $campaignId = $this->campaignRepository->createProjectEntry($company_id, $objectregister_id);
        $this->logger->info('$campaignId', [$campaignId, __METHOD__, __LINE__]);
        //insert default settings
        //$this->campaignRepository->insertProjectDefaultCampaignParams($company_id, $campaignId, $client_ipv4_address);
        $this->insertProjectDefaultSettings($company_id, $client_ipv4_address);

        /*
        COMMENT: !!! do not create a objectRegister entry for project related customfields here, since customfields must be defined during project setup) !!!
        COMMENT: !!! do not create a objectRegister entry for project related survey here, since kind of survey (standalone or lead related) must be defined during survey setup) !!!
        */

        // return the campaign_id of account's ($company_id)  project
        return $campaignId;
    }

    /**
     * @param int $company_id
     * @param string|null $client_ipv4_address
     * @return int|null
     * @author Aki
     */
    public function insertProjectDefaultSettings(int $company_id,string $client_ipv4_address = null): ?int
    {
        $company = $this->companyRepository->getCompanyDetailById($company_id);
        $projectObjectRegister = $this->campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($company_id);
        $isCorrectIp = ($client_ipv4_address === filter_var($client_ipv4_address, FILTER_VALIDATE_IP));
        //$countryByIp = ($isCorrectIp) ? $this->campaignService->getCountryByIp($client_ipv4_address) : 'Germany';
        $countryByIp = 'Germany';
        $country = (empty($countryByIp)) ? 'Germany' : $countryByIp;
        $defaultProject = $this->objectRegisterRepository->getProjectTypeDefaultSettingsAsArray();//takes data from Object layer
        $defaultProject['advancedsettings']['country'] = $country;
        $defaultProject['relation']['company_name'] = $company['name'];
        $defaultProject['relation']['company_id'] = $company['id'];
        $defaultProject['relation']['project_object_unique_id'] = $projectObjectRegister['object_unique_id'];
        $defaultProject['relation']['project_objectregister_id'] = $projectObjectRegister['id'];
        $cp_value = json_encode($defaultProject);
        //insert value
        $this->logger->info('Default settings for project', [$cp_value,__METHOD__, __LINE__]);
        return $this->objectRegisterMetaRepository->insert($projectObjectRegister['id'],ObjectRegisterMetaRepository::MetaKeyProject,$cp_value);

    }


}
