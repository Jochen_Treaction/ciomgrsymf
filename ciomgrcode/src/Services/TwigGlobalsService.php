<?php


namespace App\Services;

// use Symfony\Bundle\TwigBundle\DependencyInjection\TwigExtension;
// use Twig\Extension\GlobalsInterface; // $globals->getGlobals();
// use Twig\Extension\CoreExtension´;
// use App\Repository\CampaignRepository;

use Twig\Environment;
use Psr\Log\LoggerInterface;
use Twig\TwigFilter;
use App\Repository\AdministratesCompaniesRepository;

class TwigGlobalsService
{
    private $env, $logger;
    private $administrationService;
    private $administratesCompaniesRepository;


    public function __construct(
        Environment $environment,
        LoggerInterface $logger,
        AdministrationService $administrationService,
        AdministratesCompaniesRepository $administratesCompaniesRepository
    ) {
        $this->logger = $logger;
        $this->env = $environment;
        $this->administrationService = $administrationService;
        $this->administratesCompaniesRepository = $administratesCompaniesRepository;
        $this->env->addFilter(new TwigFilter('strrot13', 'str_rot13'));
        $this->env->addFilter(new TwigFilter('base64encode', 'base64_encode'));
        $this->env->addFilter(new TwigFilter('base64decode', 'base64_decode'));
        $this->env->addFilter(new TwigFilter('jsonEncode', 'json_encode'));
        $this->env->addFilter(new TwigFilter('jsonDecode', 'json_decode'));
        $this->env->addFilter(new TwigFilter('trim', 'trim'));
        $this->env->addFilter(new TwigFilter('removeSpaces', function($string){return preg_replace('/\s+/', '', $string);})); //
    }

    private function removeAllSpaces($string)
    {
        return preg_replace('/\s+/', '', $string);
    }


    public function setTwigGlobals(string $key, array $arr): void
    {
        try {
            $this->env->addGlobal($key, $arr);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__], [__LINE__]);
        }
    }


    /**
     * Used to display account details on twig, WEB-4088: depending on users role
     * @return array
     * @internal enhanced due to WEB-4088 -> added if-elseif-else switch
     */
    public function getSwitchAccoutDetails(): array
    {
        // TODO: jochen: does not work on reset password

        $user = $this->administrationService->getCurrentUser();

        if (isset($user['is_super_admin']) && $user['is_super_admin']) {
            return $this->administratesCompaniesRepository->getAssignedCompanies();
        } elseif ((isset($user['is_master_admin'])  && $user['is_master_admin']) || (isset($user['is_admin']) && $user['is_admin'])) {
            return $this->administratesCompaniesRepository->getAssignedCompanies($user['user_id']);
        } else { // $user['is_user']
            return ['id' => (!empty($user['company_id'])) ? $user['company_id'] : 'NOCOMPANYID', 'name' => (!empty($user['company_name'])) ? $user['company_name'] : 'NOCOMPANYNAME'];
        }
    }


    public function getUserForTwig(): string
    {
        $user = $this->administrationService->getCurrentUser();
        if (!empty($user['first_name']) || !empty($user['last_name'])) {
            return $user['first_name'] . ' ' . $user['last_name'];
        } else {
            return '';
        }
    }


    public function getCompanyNameForTwig(): string
    {
        $user = $this->administrationService->getCurrentUser();
        return (!empty($user['company_name'])) ? $user['company_name'] : '';
    }


}
