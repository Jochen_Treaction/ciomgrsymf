<?php


namespace App\Services;

use App\Entity\Datatypes;
use App\Entity\Users;
use App\Repository\CompanyRepository;
use App\Repository\DatatypesRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\StandardFieldMappingRepository;
use App\Repository\UsersRepository;
use App\Types\IntegrationTypes;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use App\Controller\AccountIntegrationController;
use App\Repository\CampaignRepository;
use RuntimeException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AioServices
{
    private const ENDPOINT_EMIO_UNSUBSCIBE = "/contact/unsubscribe";
    private $logger;
    private $httpClient;
    /**
     * @var CompanyRepository
     * @author Pradeep
     */
    private $companyRepository;

    public function __construct(
        LoggerInterface $logger,
        CompanyRepository $companyRepository,
        IntegrationsRepository $integrationsRepository
    ) {
        $this->logger = $logger;
        $this->httpClient = HttpClient::create(['http_version' => '2.0']);
        $this->companyRepository = $companyRepository;
        $this->integrationsRepository = $integrationsRepository;
    }

    /**
     * additionalParameters for twigs
     *
     * @param $cnf_tgt
     * @param CompanyRepository $company
     * @param AccountIntegrationController $accountSettings
     * @return string
     * @author aki
     */
    public function getApikey(
        $cnf_tgt,
        CompanyRepository $company,
        AccountIntegrationController $accountSettings
    ): string {
        $api_key = '';
        //request to get parameters
        $user = $accountSettings->getCurrentUser();
        $id = $user[ 'company_id' ];
        $integrations = $company->getIntegrationsForCompany($id, $cnf_tgt);
        if (!empty($integrations) && isset($integrations[ 'apikey' ]) && !empty($integrations[ 'apikey' ])) {
            $api_key = $integrations[ 'apikey' ];
        }
        return $api_key;
    }

    /**
     * CioParameters for twigs
     *
     * @param CampaignRepository $campaignRepository
     * @param $camp_id
     * @return string
     * @author aki
     */
    public function CioParameters(CampaignRepository $campaignRepository, $camp_id): string
    {
        $params = $campaignRepository->fetchCampaignParams($camp_id, 'mapping');
        if ($params) {
            $mappingParams = json_decode(base64_decode($params[ 'c_set' ][ 'mappingTable' ]));
        }
        $cioParams = array();
        if (!empty($mappingParams)) {
            foreach ($mappingParams as $key => $value) {
                $value = get_object_vars($value);
                array_push($cioParams, $value[ 'marketing-in-one' ]);
            }
        }
        return base64_encode(json_encode($cioParams));
    }

    /**
     * @param Users $user
     * @return bool
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @author Pradeep
     */
    public function createContactToeMIO(Users $user): bool
    {
        $status = false;
        $apikey = $this->integrationsRepository->getEMailInOneAPIkeySyncUserForNewsletter();
        if ($user->getEmail() === null || $user->getCompany() === null || empty($apikey)) {
            return $status;
        }
        $method = 'POST';
        $endPoint = $_ENV[ 'MSMIO_URL' ] . '/integratetomio';
        try {
            $company = $user->getCompany();
            if ($company === null) {
                throw new RuntimeException('Failed to get Company Information');
            }
            $mio_body[ 'instructions' ] = [
                'apikey' => $apikey,
                'doiPermission' => 5,
                'doiMailingKey' => true,
                'contactEventId' => '',
                'marketingAutomationId' => '',
                'campaignName' => '',
                'typeOfIntegration' => 'DOI',
                'unsubscriberStatus' => true,
            ];
            $mio_body[ 'customFields' ] = [];
            $mio_body[ 'contactEventFields' ] = [];
            $mio_body[ 'standardFields' ] = [
                'FIRSTNAME' => $user->getFirstName(),
                'LASTNAME' => $user->getLastName(),
                'SALUTATION' => $user->getSalutation(),
                'EMAIL' => $user->getEmail(),
                'ADDRESS' => $company->getStreet() ?? '',
                'CITY' => $company->getCity() ?? '',
                'COUNTRY' => $company->getCountry() ?? '',
                'ZIP' => $company->getPostalCode(),
                'ORGANIZATION' => $company->getName(),
            ];

            $mio_body[ 'checksum' ] = hash('sha512', str_rot13(json_encode($mio_body, JSON_THROW_ON_ERROR)));
            $body = base64_encode(json_encode($mio_body, JSON_THROW_ON_ERROR));
            $response = $this->httpClient->request($method, $endPoint, ['body' => $body]);
            if ($response->getStatusCode() === 200) {
                $status = $response->toArray()[ 'status' ];
            }
        } catch (TransportExceptionInterface | JsonException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
        // validate the response and return the status;
        return $status;
    }

    /**
     * @param string $customField
     * @param string $dataType
     * @param string $apikey
     * @param bool $appendMIOTag
     * @return bool
     * @author Pradeep
     */
    public function createCustomFieldIneMIO(
        string $customField,
        string $dataType,
        string $apikey,
        bool $appendMIOTag = false
    ): bool {
        try {
            if (empty($customField) || (empty($apikey))) {
                throw new RuntimeException('Invalid Custom Field or APIKey provided.');
            }
            $body = [
                'apikey' => $apikey,
                'customfield' => [
                    'name' => $customField,
                    'datatype' => $this->getSupportedEMIODataType($dataType, $customField),
                ],
                'append_mio_tag' => $appendMIOTag,
            ];
            $endPoint = $_ENV[ 'MSMIO_URL' ] . '/customfield/create';
            $base64Text = base64_encode(json_encode($body, JSON_THROW_ON_ERROR));
            //$this->logger->info('createCustomFieldIneMIO ' . $base64Text);
            $response = $this->httpClient->request('POST', $endPoint, ['body' => $base64Text]);
            //$this->logger->info('createCustomFieldIneMIO ' . json_encode($response, JSON_THROW_ON_ERROR));
            if ($response->getStatusCode() === 200) {
                $status = true;
            }
            return true;
        } catch (JsonException | TransportExceptionInterface | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * Fetches the supported eMIODataType for the given MIODataType.
     * - Used while integrating or synchronizing the contact fields between MIO and eMIO.
     * - There are some exceptional cases for fields like DOI_TIMESTAMP & SOI_TIMESTAMP
     * @param string $dataType
     * @param string $mioFieldWithMIOTag
     * $mioFieldWithMIOTag is MIOFieldName appended with 'MIO_' tag in front.
     * @return string supported eMIODataType.
     * @internal eMIO Supported DataTypes 'string', 'integer', 'date', 'float', 'boolean'
     * @author Pradeep
     */
    public function getSupportedEMIODataType(string $dataType, string $mioFieldWithMIOTag = ''): string
    {
        // Exceptional case.
        $exceptionalFields = [
            MIOStandardFieldRepository::DOI_TIMESTAMP,
            MIOStandardFieldRepository::SOI_TIMESTAMP,
        ];
        // DOITimeStamp and SOITimeStamp is a Date in MIO but has to be string in eMIO.
        // reference : https://treaction.atlassian.net/browse/WEB-5694
        if (str_contains($mioFieldWithMIOTag, MIOStandardFieldRepository::DOI_TIMESTAMP) ||
            str_contains($mioFieldWithMIOTag, MIOStandardFieldRepository::SOI_TIMESTAMP)) {
            return 'string';
        }
        // If no DataType is specified
        if (empty($dataType)) {
            return DatatypesRepository::PHP_STRING;
        }
        // String
        if (in_array($dataType, [
            DatatypesRepository::MIO_EMAIL,
            DatatypesRepository::MIO_LIST,
            DatatypesRepository::MIO_PHONE,
            DatatypesRepository::MIO_TEXT,
            DatatypesRepository::MIO_URL,
        ], true)) {
            return 'string';
        }
        //Date
        if (in_array($dataType, [
            DatatypesRepository::MIO_DATE,
            DatatypesRepository::MIO_DATETIME,
        ], true)) {
            return 'date';
        }
        //Integer
        if ($dataType === DatatypesRepository::MIO_INTEGER) {
            return 'integer';
        }
        // Float
        if ($dataType === DatatypesRepository::MIO_DECIMAL) {
            return 'float';
        }
        // Boolean
        if ($dataType === DatatypesRepository::MIO_BOOLEAN) {
            return 'boolean';
        }

        return DatatypesRepository::PHP_STRING;
    }

    /**
     * @param array $eMIOCustomFields
     * @return bool|null
     * @author Pradeep
     */
    public function updateCustomFieldsToeMIO(array $eMIOCustomFields, string $apikey): bool
    {
        try {
            if (empty($eMIOCustomFields[ 'relation' ]) || empty($eMIOCustomFields[ 'customfieldlist' ]) || empty($apikey)) {
                return false;
            }

            $eMIOCustomFields[ 'integrations' ][] = [
                'emio' => [
                    'status' => true,
                    'apikey' => $apikey,
                ],
            ];
            $endPoint = $_ENV[ 'MSAIO_URL' ] . '/contact/customfield/emio/sync';
            $base64Text = base64_encode(json_encode($eMIOCustomFields, JSON_THROW_ON_ERROR));
            $this->logger->info(json_encode(['updateCustomFieldsToeMIO' => $base64Text], JSON_THROW_ON_ERROR));
            $response = $this->httpClient->request('POST', $endPoint, ['body' => $base64Text]);
            if ($response->getStatusCode() === 200) {
                $status = true;
            }
            return $status;
        } catch (JsonException | ServerExceptionInterface | ClientExceptionInterface |
        RedirectionExceptionInterface | DecodingExceptionInterface |
        TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param int $accountNo
     * @return bool
     * @author Pradeep
     */
    public function generateMIOAPIKey(int $accountNo, &$aioApiKey = null): bool
    {
        $status = false;

        $body = [
            'name' => 'accountNo',
            'value' => $accountNo,
        ];
        try {
            $base64Text = base64_encode(json_encode([$body], JSON_THROW_ON_ERROR));
            $endPoint = $_ENV[ 'MSAIO_URL' ] . '/general/account/apikey/create';
            $response = $this->httpClient->request('POST', $endPoint, ['body' => $base64Text]);

            $this->logger->info('AIO $response /general/account/apikey/create', [$response, __METHOD__, __LINE__]);

            if ($response->getStatusCode() === 200) {
                $status = true;
            }
            return $status;
        } catch (JsonException | TransportExceptionInterface | RuntimeException | Exception $e) {
            // var_dump($e->getMessage());
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param string $name
     * @param string $label
     * @param string $description
     * @param string $type
     * @param string $fieldType
     * @param array $options
     * @param string $apikey
     * @param bool $appendMIOTag
     * @return bool
     * @author Pradeep
     */
    public function createHubspotCustomField(
        string $name,
        string $label,
        string $description,
        string $type,
        string $fieldType,
        array $options,
        string $apikey,
        bool $appendMIOTag = false
    ): bool {
        try {
            if (empty($name) || empty($label) || empty($description) || empty($type) || empty($fieldType) || (empty($apikey))) {
                throw new RuntimeException('Invalid Custom Field or APIKey provided. ' . json_encode([
                        $name,
                        $label,
                        $description,
                        $type,
                        $fieldType,
                        $options,
                        $apikey,
                    ], JSON_THROW_ON_ERROR));
            }
            $body = [
                'apikey' => $apikey,
                'property' => [
                    'name' => $name,
                    'label' => $label,
                    'description' => $description,
                    'type' => $this->getSupportedHubspotDataType($type),
                    'fieldType' => $fieldType,
                    'options' => $options,
                ],
            ];
            $endPoint = $_ENV[ 'MSHB_URL' ] . '/contact/property/create';
            $base64Text = base64_encode(json_encode($body, JSON_THROW_ON_ERROR));
            //$this->logger->info('createCustomFieldIneMIO ' . $base64Text);
            $response = $this->httpClient->request('POST', $endPoint, ['body' => $base64Text]);
            //$this->logger->info('createCustomFieldIneMIO ' . json_encode($response, JSON_THROW_ON_ERROR));
            if ($response->getStatusCode() === 200) {
                $status = true;
            }
            return true;
        } catch (JsonException | TransportExceptionInterface | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param string $dataType
     * @return string
     * @author Pradeep
     * @internal Hubspot Supported DataTypes 'string', 'number', 'date', 'datetime', 'enumeration'
     */
    public function getSupportedHubspotDataType(string $dataType): string
    {
        // If no DataType is specified
        if (empty($dataType)) {
            return DatatypesRepository::PHP_STRING;
        }
        // String
        if (in_array($dataType, [
            DatatypesRepository::MIO_EMAIL,
            DatatypesRepository::MIO_LIST,
            DatatypesRepository::MIO_PHONE,
            DatatypesRepository::MIO_TEXT,
            DatatypesRepository::MIO_URL,
            DatatypesRepository::MIO_BOOLEAN,
        ], true)) {
            return 'string';
        }
        // Date
        if ($dataType === DatatypesRepository::MIO_DATE) {
            return 'date';
        }
        // DateTime
        if ($dataType === DatatypesRepository::MIO_DATETIME) {
            return 'datetime';
        }
        // Integer
        if (in_array($dataType, [DatatypesRepository::MIO_INTEGER, DatatypesRepository::MIO_DECIMAL], true)) {
            return 'number';
        }

        return DatatypesRepository::PHP_STRING;
    }

    /**
     * @param string $email
     * @param string $apikey
     * @param int $blacklistId
     * @param string $blacklistFlagName
     * @return bool|null
     * @author Pradeep
     * @internal Blacklists the E-Mail address in eMIO.
     */
    public function blacklistContactIneMIO(
        string $email,
        string $apikey,
        int $blacklistId,
        string $blacklistFlagName = null
    ): ?bool {
        $status = false;
        try {
            if (empty($email) || empty($apikey) || empty($blacklistId) || $blacklistId <= 0) {
                throw new RuntimeException('Invalid configuration to blacklist Contact in eMIO ' . json_encode([
                        'email' => $email,
                        'apikey' => $apikey,
                        'blacklistId' => $blacklistId,
                    ], JSON_THROW_ON_ERROR));
            }
            $endPoint = $_ENV[ 'MSMIO_URL' ] . '/blacklist/emails';
            $body = json_encode([
                'blk_email_list' => $email,
                'blk_id' => $blacklistId,
                'api_key' => $apikey,
                'blk_imp_name' => $blacklistFlagName,
            ], JSON_THROW_ON_ERROR);

            $base64Text = base64_encode($body);
            //$this->logger->info('createCustomFieldIneMIO ' . $base64Text);
            $response = $this->httpClient->request('POST', $endPoint, ['body' => $base64Text]);
            $this->logger->info('blacklistEmail ' . json_encode($response, JSON_THROW_ON_ERROR));
            if ($response->getStatusCode() === 200) {
                $status = true;
            }
            $this->logger->info('status ' . $status);
            return $status;
        } catch (JsonException | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param int $companyId
     * @param array $contacts
     * @param string $integrationName
     * @return bool
     * @throws JsonException
     * @throws TransportExceptionInterface
     * @author Pradeep
     */
    public function syncContacts(int $companyId, array $contacts, string $integrationName): bool
    {
        $status = false;
        if (empty($contacts) || empty($integrationName)) {
            return false;
        }
        $payload[ 'base' ][ 'company_id' ] = $companyId;
        $payload[ 'contacts' ] = $contacts;
        $payload[ 'base' ][ 'integration_name' ] = $integrationName;

        $endPoint = $_ENV[ 'MSAIO_URL' ] . '/contact/sync';
        $payload = base64_encode(json_encode($payload, JSON_THROW_ON_ERROR));
        $this->logger->info('syncContacts before ' . $payload);
        $response = $this->httpClient->request('POST', $endPoint, ['body' => $payload]);
        $this->logger->info('syncContacts before ' . json_encode($response->getStatusCode()));
        $rs = $response->getContent();
        $this->logger->info('ResponseCode ' . json_encode($rs, JSON_THROW_ON_ERROR));
        if ($response->getStatusCode() === 200) {
            $status = true;
        }
        return $status;
    }

    /**
     * @param string $email
     * @param string $eMIOAPIKey
     * @author Pradeep
     */
    public function unsubscribeContactIneMIO(string $email, string $eMIOAPIKey): bool
    {
        $status = false;
        if (empty($email) || empty($eMIOAPIKey) || empty($_ENV[ 'MSMIO_URL' ]) ||
            empty(self::ENDPOINT_EMIO_UNSUBSCIBE)) {
            return $status;
        }
        $endPoint = $_ENV[ 'MSMIO_URL' ] . self::ENDPOINT_EMIO_UNSUBSCIBE;
        $payload[ 'apikey' ] = $eMIOAPIKey;
        $payload[ 'email' ] = $email;

        $this->logger->info('payload ' . json_encode($payload));

        // Generate Payload.
        $payload = base64_encode(json_encode($payload, JSON_THROW_ON_ERROR));
        $this->logger->info('unsubscribe payload ' . $payload);
        // Http Client Request to eMIO.
        $response = $this->httpClient->request('POST', $endPoint, ['body' => $payload]);

        $statusCode = $response->getStatusCode();
        if ($statusCode === Response::HTTP_NO_CONTENT) {
            $status = true;
        }
        return $status;
    }
}
