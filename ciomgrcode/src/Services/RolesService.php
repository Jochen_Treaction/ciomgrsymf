<?php


namespace App\Services;

/**
 * @internal WEB-3850 define roles that are needed by application
 * Class GlobalConstantsService
 * @package  App\Services
 * @author   jsr
 */
class RolesService
{

    const SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    const MASTER_ADMIN = 'ROLE_MASTER_ADMIN';
    const ADMIN = 'ROLE_ADMIN';
    const USER = 'ROLE_USER';

    protected $roleSuperAdmin; // has access to everything
    protected $roleMasterAdmin; // can maintain multiple companies and invite / maintain users, campaigns of his companies
    protected $roleAdmin; // can invite / maintain users of one (the assigend) company, this companys campaigns
    protected $roleUser; // supposed read only role of leads, campaigns of the company user is assigned to


    public function __construct()
    {
        $this->roleSuperAdmin = ['ROLE_SUPER_ADMIN', 'ROLE_MASTER_ADMIN', 'ROLE_ADMIN', 'ROLE_USER'];
        $this->roleMasterAdmin = ['ROLE_MASTER_ADMIN', 'ROLE_ADMIN', 'ROLE_USER'];
        $this->roleAdmin = ['ROLE_ADMIN', 'ROLE_USER'];
        $this->roleUser = ['ROLE_USER'];
    }


    private function getRoleLevel(array $role)
    {

        if( $role == $this->roleSuperAdmin) {
            return 4;
        }

        if( $role == $this->roleMasterAdmin) {
            return 3;
        }

        if( $role == $this->roleAdmin) {
            return 2;
        }

        if( $role == $this->roleUser) {
            return 1;
        }

        return 1;

    }


    /**
     * @return array
     */
    public function getRoleSuperAdmin(): array
    {
        return $this->roleSuperAdmin;
    }


    /**
     * @return array
     */
    public function getRoleMasterAdmin(): array
    {
        return $this->roleMasterAdmin;
    }


    /**
     * @return array
     */
    public function getRoleAdmin(): array
    {
        return $this->roleAdmin;
    }


    /**
     * @return array
     */
    public function getRoleUser(): array
    {
        return $this->roleUser;
    }


    public function hasRole(array $roles, string $checkForRole): bool
    {
        return in_array($roles, $checkForRole);
    }


    /**
     * toStringForDatabase
     * @param array $roles
     * @return string '["ROLE1","ROLE2",...]'
     */
    public function toStringForDatabase(array $roles):string
    {
        return json_encode($roles);
    }

    /**
     * toString
     * @param array $roles
     * @return string "ROLE1","ROLE2",...
     */
    public function toString(array $roles):string
    {
        return '"'.implode('","',$roles).'"';
    }


    public function roleGreaterEqual( array $roleGreaterEqual, array $thanRole):bool
    {
       $levelRoleGreaterEqual = $this->getRoleLevel($roleGreaterEqual);
       $levelThanRole = $this->getRoleLevel($thanRole);

       return ($levelRoleGreaterEqual >= $levelThanRole);
    }


    public function roleGreater( array $roleGreater, array $thanRole):bool
    {
       $levelRoleGreater = $this->getRoleLevel($roleGreater);
       $levelThanRole = $this->getRoleLevel($thanRole);

       return ($levelRoleGreater > $levelThanRole);
    }


}
