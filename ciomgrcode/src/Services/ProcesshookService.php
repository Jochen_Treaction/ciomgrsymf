<?php


namespace App\Services;


use App\Entity\Users;
use App\Repository\CampaignRepository;
use App\Repository\ContactRepository;
use App\Repository\CustomfieldsRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\ObjectTypeMetaRepository;
use App\Repository\ProcesshookRepository;
use App\Repository\StandardFieldMappingRepository;
use App\Repository\StatusDefRepository;
use App\Repository\WorkflowRepository;
use App\Services\AuthenticationService;
use App\Services\UtilsService;
use App\Types\ProcesshookTypes;
use App\Types\StandardfieldTypes;
use App\Types\StatusdefTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * @property ProcesshookRepository processhookRepository
 * @property LoggerInterface logger
 * @property Connection conn
 * @property \App\Services\UtilsService utilService
 * @property ObjectTypeMetaRepository objectTypeMetaRepository
 * @property ObjectRegisterRepository objectRegisterRepository
 * @property ObjectRegisterMetaRepository objectRegisterMetaRepository
 * @property StatusDefRepository statusDefRepository
 * @property StatusDefRepository statusdefRepository
 * @property CampaignRepository campaignRepository
 * @property WorkflowRepository workflowRepository
 * @property \App\Services\AuthenticationService authenticationService
 * @property AuthenticationUtils authenticationUtils
 * @property IntegrationsRepository integrationsRepository
 * @property \Symfony\Contracts\HttpClient\HttpClientInterface httpClient
 * @property CustomfieldsRepository customfieldsRepository
 * @property StandardFieldMappingRepository standardFieldMappingRepository
 * @auther aki
 */
class ProcesshookService
{
    public function __construct(
        AuthenticationService          $authenticationService,
        AuthenticationUtils            $authenticationUtils,
        CampaignRepository             $campaignRepository,
        Connection                     $conn,
        IntegrationsRepository $integrationsRepository,
        LoggerInterface $logger,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectTypeMetaRepository $objectTypeMetaRepository,
        ProcesshookRepository $processhookRepository,
        StatusDefRepository $statusDefRepository,
        UtilsService $utilsService,
        CustomfieldsRepository $customfieldsRepository,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        WorkflowRepository $workflowRepository,
        ContactRepository $contactRepository
    )
    {
        $this->authenticationService = $authenticationService;
        $this->authenticationUtils = $authenticationUtils;
        $this->campaignRepository = $campaignRepository;
        $this->conn = $conn;
        $this->integrationsRepository = $integrationsRepository;
        $this->logger = $logger;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->objectTypeMetaRepository = $objectTypeMetaRepository;
        $this->processhookRepository = $processhookRepository;
        $this->statusdefRepository = $statusDefRepository;
        $this->utilService = $utilsService;
        $this->httpClient = HttpClient::create(['http_version' => '2.0']);
        $this->customfieldsRepository = $customfieldsRepository;
        $this->standardFieldMappingRepository = $standardFieldMappingRepository;
        $this->workflowRepository = $workflowRepository;
        $this->contactRepository = $contactRepository;
    }

    /**
     * @param string $status
     * @param int $companyId
     * @return array
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     * @auther aki
     */
    public function fetchProcesshooks(string $status, int $companyId): array
    {
        $processhookViewDetails = [];
        if ($status === StatusdefTypes::AVAILABLE) {
            $processhookViewDetails = [];
            $processhooks = $this->processhookRepository->getAvailableProcesshooks();
            foreach ($processhooks as $key => $value) {
                $processhookViewDetails[$key]['view_name'] = $this->utilService->stringSnakeCaseToUiLabel($value['unique_object_name']);
                $processhookViewDetails[$key]['processhook_id'] = $value['id'];
                $processhookViewDetails[$key]['name'] = $value['unique_object_name'];
                $processhookViewDetails[$key]['description'] = $value['description'];
            }
        } else {
            $project_campaign_id = $this->campaignRepository->getProjectDetails($companyId)['id'];
            $user_processhooks = $this->processhookRepository->getProcesshooksByStatus($status, $project_campaign_id);
            foreach ($user_processhooks as $key => $value) {
                $processhookViewDetails[$key]['active_details'] = $this->formatDate($this->workflowRepository->getActiveProcesshooksInfo($value['id']));
                $processhookViewDetails[$key]['view_name'] = $this->utilService->stringSnakeCaseToUiLabel($value['unique_object_name']);
                $processhookViewDetails[$key]['user_processhook_name'] = $value['unique_object_name'];
                $processhookViewDetails[$key]['user_processhook_id'] = $value['id'];
                $processhookViewDetails[$key]['name'] = $value['processhook_name'];
                $processhookViewDetails[$key]['description'] = $value['description'];
                $processhookViewDetails[$key]['id'] = $value['id'];
            }
        }
        return $processhookViewDetails;
    }


    /**
     * @param string $processhookType
     * @return array|null
     * @auther aki,jsr
     * @throws \Exception|\Doctrine\DBAL\Driver\Exception
     */
    public function getSettings(string $processhookType): ?array
    {

        $this->logger->info('$processhookType', [$processhookType, __FUNCTION__, __LINE__]);
        $settings = null;
        $defaultSettings = $this->objectTypeMetaRepository->getDefaultSettingsFromObjectTypeMeta($processhookType);

        try {
            $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
            $this->logger->info('$user', [$user]);
            $projectCampaignId = $this->campaignRepository->getSelectedProjectCampaignId($user['company_id']);
            $processhookNames = ['processhooknames' => $this->processhookRepository->getProcesshookNames($projectCampaignId)];
        } catch (\Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            return null;
        }
        $this->logger->info('$defaultSettings', [$defaultSettings, __FUNCTION__, __LINE__]);
        if (!empty($defaultSettings)) {
            try {
                $settings = json_decode($defaultSettings, true, 512, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
                $this->logger->info('$settings', [$settings, __FUNCTION__, __LINE__]);

            } catch (JsonException $e) {
                throw new \Exception('Exception in ' . __METHOD__ . ': ' . $e->getMessage(), __LINE__);
            }
        } else {
            throw new \Exception('Exception in ' . __METHOD__ . ', LINE ' . __LINE__ . ': $this->objectTypeMetaRepository->getDefaultSettingsFromObjectTypeMeta delivered null : cannot json_decode(null) to array => missing database configuration', __LINE__);
        }

        // add special settings
        switch ($processhookType) {
            case ProcesshookTypes::SEND_DOI_EMAIL_OR_CALL_CONTACT_EVENT || ProcesshookTypes::CALL_CONTACT_EVENT:
                $params = $this->getEMioContactEventsConfigurationParams();
                $this->logger->info('$params', [$params, __FUNCTION__, __LINE__]);
                $settings = array_merge($settings, ['additional' => $params]);
                $this->logger->info('$settings', [$settings, __METHOD__, __LINE__]);
                break;
            default:
                $settings = array_merge($settings, ['additional' => $processhookNames]);
        }

        $this->logger->info('zzz$settings', [$settings, __METHOD__, __LINE__]);
        return $settings;
    }


    /**
     * @param int $company_id
     * @param string $processhook
     * @param string $processhookSettings
     * @param int $objectRegisterId
     * @param string $operation
     * @return array|null
     * @throws JsonException
     * @author aki
     * @internal this function save the processhook settings to DB(object_register_meta),
     *           Does both insert and update operations.
     *           update function can be enabled by providing objectRegisterId and operation = 'update'
     */
    public function saveProcesshookSettings(
        int    $company_id,
        string $processhook,
        string $processhookSettings,
        int    $objectRegisterId = 0,
        string $operation = 'insert'
    ): ?array
    {
        $status = null;
        if (empty($company_id) || empty($processhookSettings)) {
            $this->logger->error("company id or processhook settings are empty", [__METHOD__, __LINE__]);
            return null;
        }
        if ($objectRegisterId === 0) {
            // CREATE A NEW OBJECT_REGISTER_ID FOR INSERT OPERATION OR WHEN OBJECT_REGISTER_ID IS PRESENT FOR PROCESSHOOK
            $status["objectRegisterId"] = $this->objectRegisterRepository->createInitialObjectRegisterEntryProcesshook($processhook);
        } else {
            $status["objectRegisterId"] = $objectRegisterId;
        }

        if ($status["objectRegisterId"] === null) {
            $this->logger->error("Object register entry not done", [__METHOD__, __LINE__]);
            return null;
        }
        //add object register id to settings
        $processhookSettings = json_decode($processhookSettings, true, 512, JSON_THROW_ON_ERROR);
        if (isset($processhookSettings['relation'])) {
            $processhookSettings['relation']['company_id'] = $company_id;
            $processhookSettings['relation']['object_register_id'] = $status["objectRegisterId"];
        }
        $metaValue = json_encode($processhookSettings, JSON_THROW_ON_ERROR);
        $metaKey = $processhook;

        if ($operation === 'insert') {
            //INSERT NEW PROCESSHOOK SETTINGS TO OBJECT_REGISTER_META
            $status["objectRegisterMetaId"] = $this->objectRegisterMetaRepository->insert($status["objectRegisterId"],
                $metaKey, $metaValue);
        } else {
            //UPDATE NEW PROCESSHOOK SETTINGS TO OBJECT_REGISTER_META
            $objectRegMetaId = $this->objectRegisterMetaRepository->getIdWithObjectRegisterId($status["objectRegisterId"],
                $processhook);
            $status["objectRegisterMetaId"] = $this->objectRegisterMetaRepository->update($objectRegMetaId,
                $status["objectRegisterId"], $metaKey, $metaValue);
        }

        if ($status["objectRegisterMetaId"] === null) {
            $this->logger->error("processhook settings not properly saved in object register meta",
                [__METHOD__, __LINE__]);
            return null;
        }
        return $status;
    }

    /**
     * @param int $objectRegisterId
     * @param string $processhookType
     * @param int $companyId
     * @param string $processhookSettings <b style="color:red">json</b>
     * @return null|int  $processhookId|null
     * @throws JsonException
     * @throws Exception
     * @author aki
     */
    public function saveProcesshook(
        int    $objectRegisterId,
        string $processhookType,
        int    $companyId,
        string $processhookSettings
    ): ?int
    {
        if (empty($objectRegisterId) || empty($processhookSettings)) {
            $this->logger->error("company id or processhook settings are empty", [__METHOD__, __LINE__]);
            return null;
        }
        // get settings
        $phSettings = json_decode($processhookSettings, true, 512, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
        if (isset($phSettings['settings']['name']['value'])) {
            $name = $phSettings['settings']['name']['value'];
        } else {
            $name = $phSettings['settings']['name'];
        }

        //get processhook_id
        //get campaign_id
        $processhook_id = $this->processhookRepository->getProcesshookIdByType($processhookType);
        $project_campaign_id = $this->campaignRepository->getProjectDetails($companyId)['id'];
        $processhookId = $this->processhookRepository->insertUserProcesshook(
            $objectRegisterId,
            $processhook_id,
            $project_campaign_id,
            $name
        );

        if (is_null($processhookId)) {
            $this->logger->error("company id or processhook settings are empty", [__METHOD__, __LINE__]);
            return null;
        }
        return $processhookId;

    }

    /**
     * @param int $page_webhook_id
     * @param string $workflowSettings
     * @param int $pageWebhookVersion
     * @return array|null
     * @throws JsonException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     * @internal 1.This method formats the settings into its structure
     *           2.Create object register DB entry
     *           3.Save settings into object register Meta
     *           4.Insert data into workflow,User_has_workflow DB tables
     */
    public function saveWorkflow(int $page_webhook_id, string $workflowSettings ,int $pageWebhookVersion = 1 ): ?array
    {
        $status = null;

        if (empty($page_webhook_id)) {
            $this->logger->error("company id or processhook settings are empty", [__METHOD__, __LINE__]);
            return null;
        }

        $processhookSettings = json_decode($workflowSettings, true, 512, JSON_THROW_ON_ERROR);
        foreach ($processhookSettings as $pageName => $buttons) {

            foreach ($buttons as $buttonName => $settings) {
                //format settings
                $buttonSettings = json_encode(array($buttonName => $settings), JSON_THROW_ON_ERROR | true);

                //create object registry entry
                $status[$pageName][$buttonName]["objectRegisterId"] =
                    $this->objectRegisterRepository->createInitialObjectRegisterEntryWorkflow($pageWebhookVersion);

                if ($status[$pageName][$buttonName]["objectRegisterId"] === null) {
                    $this->logger->error("Object register entry not done", [__METHOD__, __LINE__]);
                    return null;
                }

                //save settings in object register meta
                $status[$pageName][$buttonName]["objectRegisterMetaId"] = $this->objectRegisterMetaRepository->insert($status[$pageName][$buttonName]["objectRegisterId"],
                    UniqueObjectTypes::WORKFLOW,
                    $buttonSettings);

                if ($status[$pageName][$buttonName]["objectRegisterMetaId"] === null) {
                    $this->logger->error("processhook settings not properly saved in object register meta",
                        [__METHOD__, __LINE__]);
                    return null;
                }

                //insert into workflow
                $status[$pageName][$buttonName]['workflowId'] = $this->workflowRepository->insert(
                    $status[$pageName][$buttonName]['objectRegisterId'],
                    $page_webhook_id,
                    $buttonName);

                if ($status[$pageName][$buttonName]['workflowId'] === null) {
                    $this->logger->error("work flow not saved correctly",
                        [__METHOD__, __LINE__]);
                    return null;
                }

                //insert into user_processhook_has_workflow
                if (!$this->saveWorkFlowInUserProcesshookHasWorkflow($settings, $status[$pageName][$buttonName]['workflowId'])) {
                    return null;
                }
            }
        }
        return $status;
    }

    /**
     * @param string $processhookName
     * @param int $company_id
     * @return bool
     * @author Aki
     */
    public function checkForProcesshookName(string $processhookName, int $company_id): bool
    {
        if (empty($processhookName) || $company_id < 0) {
            return false;
        }
        $project_campaign_id = $this->objectRegisterRepository->getProjectCampaignId($company_id);
        $processhookNames = $this->processhookRepository->getProcesshookNames($project_campaign_id);
        foreach ($processhookNames as $processhook) {
            if ($processhookName === $processhook['processhook_name']) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param array $processhookSettings
     * @param array $defaultSettings
     * @return string|null
     * @author Aki
     */
    public function formatProcesshookSettings(array $processhookSettings, array $defaultSettings): ?string
    {
        if (empty($processhookSettings) || empty($defaultSettings)) {
            return null;
        }
        //remove additional settings
        if (isset($defaultSettings['additional'])) {
            unset($defaultSettings['additional']);
        }
        foreach ($processhookSettings as $key => $value) {
            if (isset($defaultSettings['settings'][$key], $defaultSettings['settings'][$key]['value'])) {
                $defaultSettings['settings'][$key]['value'] = $value;
            } else {
                $defaultSettings['settings'][$key] = $value;
            }
        }
        try {
            return json_encode($defaultSettings, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param array $campaign
     * @return array|null
     * @throws JsonException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function updateWorkflowForStartWizard(array $campaign): ?array
    {
        $this->logger->info('updateWorkflow : campaign ' . json_encode($campaign, JSON_THROW_ON_ERROR | true));
        if (empty($campaign)) {
            $this->logger->error("company id or processhook settings are empty", [__METHOD__, __LINE__]);
            return null;
        }
        $processhookSettings = json_decode($campaign['processhooks'], true, 512, JSON_THROW_ON_ERROR);
        $this->logger->info('updateWorkflow : $processhookSettings ' . json_encode($processhookSettings, true));
        $status = array();

        foreach ($processhookSettings as $pageName => $workflows) {
            foreach ($workflows as $buttonName => $workflow) {
                //if available update the workflow
                if (isset($campaign['processhookStatus'][$pageName][$buttonName])) {
                    $this->logger->info('updateWorkflow : Inside If ' .
                        json_encode($campaign['processhookStatus'][$pageName][$buttonName],
                            JSON_THROW_ON_ERROR | true));
                    $workflowObjectRegisterMetaId = $campaign['processhookStatus'][$pageName][$buttonName]['objectRegisterMetaId'];
                    $workflowId = $campaign['processhookStatus'][$pageName][$buttonName]['workflowId'];
                    $settings = json_encode(array($buttonName => $workflow), JSON_THROW_ON_ERROR | true, 512);
                    //update work flow object register meta
                    if (!$this->processhookRepository->updateWorkflow($settings, $workflowObjectRegisterMetaId)) {
                        return null;
                    }
                    //delete all user_processhook_has_workflow
                    if (!$this->processhookRepository->deleteOldRelationsInUserProcesshookHasWorkflow($workflowId)) {
                        return null;
                    }
                    //insert into user_processhook_has_workflow
                    if (!$this->saveWorkFlowInUserProcesshookHasWorkflow($workflow, $workflowId)) {
                        return null;
                    }
                } else {
                    //create new workflow
                    $newWorkflowSettings[$pageName][$buttonName] = $workflow;
                    $pageId = $campaign['id'];
                    $settingsParsed = json_encode($newWorkflowSettings, JSON_THROW_ON_ERROR | true, 512);
                    $status[$pageName][$buttonName] = $this->saveWorkflow($pageId,
                        $settingsParsed)[$pageName][$buttonName];
                }
            }
        }
        //merge old processhook status
        if (isset($campaign['processhookStatus'])) {
            return array_merge_recursive($status, $campaign['processhookStatus']);
        }
        $this->logger->info('updateWorkflow : after merge processhookStatus ' . json_encode($status, JSON_THROW_ON_ERROR));
        return $status;
    }

    /**
     * @param string $processhookType
     * @return string|null
     */
    private function getActsFromType(string $processhookType): ?string
    {
        if (empty($processhookType)) {
            return null;
        }
        //todo:take this values from DB table
        if ($processhookType === "show_popup") {
            return "client_side";
        }
        return "serverside";

    }

    /**
     * @param $company_id
     * @param $processhookId
     * @param string $status
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function changeStatusOfProcesshook($company_id, $processhookId, string $status): bool
    {
        $statusDefId = null;
        if (empty($company_id) || empty($processhookId)) {
            $this->logger->error("company id or processhook settings are empty", [__METHOD__, __LINE__]);
            return false;
        }
        if ($status === statusdefRepository::ARCHIVED) {
            $statusDefId = $this->statusdefRepository->getId(statusdefRepository::ARCHIVED);
        } elseif ($status === $this->statusdefRepository::DEFINED) {
            $statusDefId = $this->statusdefRepository->getId(statusdefRepository::DEFINED);
        }
        $objectRegisterId = $this->processhookRepository->get($processhookId)['objectregister_id'];
        $changeStatus = $this->processhookRepository->changeStatusOfProcesshook($statusDefId, $objectRegisterId);
        if ($changeStatus === false) {
            $this->logger->error("company id or processhook settings are empty", [__METHOD__, __LINE__]);
            return false;
        }
        return true;
    }

    /**
     * @param string $message
     * @return array|null
     * @auther aki
     */
    public function formatMessage(string $message): ?array
    {
        if (empty($message)) {
            $this->logger->error("empty message", [__METHOD__, __LINE__]);
            return null;
        }
        $explodedString = explode(":", $message);
        $formattedString[$explodedString[0]] = $explodedString[1];
        return $formattedString;
    }


    /**
     * returns json content of https://api.maileon.com/1.0/transactions/types, if current users projects eMio ApiKey is set in integrations, else null
     * @return array|null
     * @author jsr
     */
    public function getEMioContactEventsConfigurationParams(): ?array
    {
        try {
            $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
            $this->logger->info('$user', [$user]);
            $projectCampaignId = $this->campaignRepository->getSelectedProjectCampaignId($user['company_id']);
            $eMioApikey = $this->integrationsRepository->getIntegrationsEMioApiKey($user['company_id'], $projectCampaignId);
        } catch (\Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            return null;
        };


        $this->logger->info('$eMioApikey', [$eMioApikey, __METHOD__, __LINE__]);

        if (!empty($eMioApikey)) {
            try {
                $jsonResponse = $this->httpClient->request('GET', $_ENV['MSMIO_URL'] . '/transaction/types?apikey=' . $eMioApikey);
            } catch (TransportExceptionInterface $e) {
                $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
                return null;
            }

            $this->logger->info('$jsonResponse', [print_r($jsonResponse->getContent(), 1), __METHOD__, __LINE__]);

            if (200 === $jsonResponse->getStatusCode()) {
                $transactionTypeArray = json_decode($jsonResponse->getContent(), true, 512, JSON_OBJECT_AS_ARRAY);

                if (!empty($transactionTypeArray)) {
                    $contactEvents = $this->getContactEventLists($transactionTypeArray);
                    $mioStandardAndCustomFields = ['mioFields' => $this->getMioStandardAndCustomFields($user['company_id'])];
                    $project_campaign_id = $this->objectRegisterRepository->getProjectCampaignId($user['company_id']);
                    $processhookNames = ['processhooknames' => $this->processhookRepository->getProcesshookNames($project_campaign_id)];

                    $this->logger->info('$processhookNames', [$processhookNames, __METHOD__, __LINE__]);
                    $this->logger->info('$mioStandardAndCustomFields', [$mioStandardAndCustomFields, __METHOD__, __LINE__]);
                    return array_merge($mioStandardAndCustomFields, $contactEvents, $processhookNames);
                }
            }
        }

        return null;
    }


    /**
     * returns an array [ 'sourcetable' => 'customfields|standardfields', 'id' => int, 'fieldname' => string]
     * @return array
     */
    private function getMioStandardAndCustomFields(int $company_id): array
    {

        try {
            $cfArray = [];
            $sfArray = [];
            $tfArray = [];

            // $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername()); // to get $user['company_id']
            $projectCustomfields = $this->customfieldsRepository->getProjectCustomfields($company_id,
                UniqueObjectTypes::CUSTOMFIELDS);
            //$standardCustomFields = $this->contactRepository->getStandardCustomFields();

            $customFields = $projectCustomfields;
            //$this->logger->info('Standard Custom Fields ' . json_encode($standardCustomFields));
            $projectStandardCustomFields = "";
            // $standardFields = $this->standardFieldMappingRepository->getAll(StandardfieldTypes::SYSTEM_EMAIL_IN_ONE);

            $standardFields = $this->standardFieldMappingRepository->getMIOStandardFields();

            $this->logger->info('ProjectCustomFields ' . json_encode($customFields));
            $this->logger->info('StandardFields ' . json_encode($standardFields));// Get the s*/

            // format CustomFields for the twig.
            foreach ($customFields as $cf) {
                if (trim($cf[ 'fieldname' ]) !== '') {

                    if (empty($cf[ 'htmllabel' ]) || trim($cf[ 'htmllabel' ]) === '') {
                        $fieldViewName = "Contact." . $cf[ 'fieldname' ];
                    } else {
                        $fieldViewName = $cf[ 'htmllabel' ];
                    }

                    // $fieldViewName = "Contact." . $cf[ 'fieldname' ];
                    $cfArray[] = [
                        'sourcetable' => 'customfields',
                        'id' => $cf[ 'id' ] ?? 0,
                        'fieldname' => $cf[ 'fieldname' ],
                        'fieldViewName' => $fieldViewName,
                    ];
                }
            }

            // Format Standard and CustomFields for the Twig.
            foreach ($standardFields as $sf) {
                // tracking parameters
                $this->logger->info('Before check for HiddenField ' . json_encode([
                        'array' => $sf,
                        'htmllabel' => $sf[ 'hidden_field' ],
                    ]));
                if ($sf[ 'hidden_field' ] === "1") {
                    $this->logger->info('TrackingField ' . json_encode([
                            'array' => $sf,
                            'htmllabel' => $sf[ 'htmllabel' ],
                        ]));
                    // Update the HTML label(How the html field should look like)
                    if (empty($sf[ 'htmllabel' ]) || trim($sf[ 'htmllabel' ]) === '') {
                        $fieldViewName = "Contact." . ucfirst($sf[ 'fieldname' ]);
                    } else {
                        $fieldViewName = $sf[ 'htmllabel' ];
                    }
                    // format to an array expected by the front end.
                    $tfArray[] =
                        [
                            'id' => $sf[ 'id' ],
                            'fieldname' => $sf[ 'fieldname' ],
                            'sourcetable' => 'trackingfields',
                            'fieldViewName' => $fieldViewName,
                        ];

                } else {
                    // standard parameters
                    // Update the HTML label(How the html field should look like)
                    $this->logger->info('StandardField ' . json_encode([
                            'array' => $sf,
                            'htmllabel' => $sf[ 'htmllabel' ],
                        ]));
                    if (empty($sf[ 'htmllabel' ]) || trim($sf[ 'htmllabel' ]) === '') {
                        $fieldViewName = "Contact." . ucfirst($sf[ 'fieldname' ]);
                    } else {
                        $fieldViewName = $sf[ 'htmllabel' ];
                    }
                    // format to an array expected by the front end.
                    $sfArray[] =
                        [
                            'id' => $sf[ 'id' ],
                            'fieldname' => $sf[ 'fieldname' ],
                            'sourcetable' => 'standardfields',
                            'fieldViewName' => $fieldViewName,
                        ];
                }
            }

            $this->logger->info('ProjectCustomFields after parsing ' . json_encode($cfArray));
            $this->logger->info('StandardFields after parsing ' . json_encode($sfArray));
            $this->logger->info('TrackingFields after parsing ' . json_encode($tfArray));

            return array_merge($sfArray, $tfArray, $cfArray);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }


    /**
     * @param array $transactionTypesArray
     * @return array
     */
    private function getContactEventLists(array $transactionTypesArray): array
    {
        $contactEvents = [];
        $contactEventsAttributes = [];
        $attr = [];
        $this->logger->info('getContactEventLists' . json_encode($transactionTypesArray));
        if (empty($transactionTypesArray)) {
            return ['contactEvents' => $contactEvents, 'contactEventAttributes' => $contactEventsAttributes];
        }
        foreach ($transactionTypesArray[ 'transaction_type' ] as $t) {
            if (!isset($t[ 'id' ], $t[ 'name' ])) {
                continue;
            }
            $contactEvents[ $t[ 'id' ] ] = $t[ 'name' ];

            foreach ($t[ 'attributes' ][ 'attribute' ] as $key => $a) {
                $contactEventsAttributes[ $t[ 'id' ] ][] = json_decode(json_encode($a), true, 512,
                    JSON_OBJECT_AS_ARRAY);
            }
        }

        $attributeList = [];
        foreach ($contactEventsAttributes as $k => $attributes) {
            foreach ($attributes as $aKey => $attribute) {
                if (isset($attribute['id']) && isset($attribute['name'])) {
                    $attributeList[$k][$attribute['id']] = $attribute['name'];
                }
            }
        }

        return ['contactEvents' => $contactEvents, 'contactEventAttributes' => $attributeList];
    }


    /**
     * @param Users $user
     * @return bool
     * @throws JsonException
     */
    public function createPhLeadNotificationOnAccountCreation(Users $user): bool
    {
        $status = false;
        // early exit
        if (null === $user) {
            $this->logger->error('param $user is null', [__METHOD__, __LINE__]);
            return false;
        }

        $companyId = $user->getCompany()->getId();
        // as defined in WEB-5238
        $phName = ProcesshookTypes::DEFAULT_PROCESSHOOK_SEND_LEAD_NOTIFICATION;
        $phSubject = 'Neuer Interessent';
        $emailAddress = $user->getEmail();
        $defaultProcesshookSettings = $this->getSettings(ProcesshookTypes::SEND_LEAD_NOTIFICATION);

        $defaultProcesshookSettings['settings'][ProcesshookTypes::SEND_LEAD_NOTIFICATION_PROP['NAME']]['value'] = $phName;
        $defaultProcesshookSettings['settings'][ProcesshookTypes::SEND_LEAD_NOTIFICATION_PROP['LIST_OF_RECIPIENTS']]['value'] = $emailAddress;
        $defaultProcesshookSettings['settings'][ProcesshookTypes::SEND_LEAD_NOTIFICATION_PROP['SUBJECT_LINE']]['value'] = $phSubject; // TODO: change SUBJECT_LINE value from kebab-case to snake_case in ProcesshookTypes.php!!!!
        $processhookSettings = json_encode($defaultProcesshookSettings);

        $projectCampaignId = $this->campaignRepository->getProjectDetails($companyId)['id'];
        // Check if the process Hook is already created.
        // ShopSystem will create the processhook with empty setting from AIO.
        // In that case we should only update Processhook settings to objectRegisterMeta.
        $configuredPHooks = $this->processhookRepository->getUserProcessHookByUniqueIndex($phName, $projectCampaignId);
        // if PHook is already created by AIO -> $isPHookCreated = true,
        // if PHook is not present -> $isPHookCreated = false
        $isPHookCreated = !empty($configuredPHooks);
        if ($isPHookCreated) {
            $objectRegisterId = $configuredPHooks['objectregister_id'];
            $status = true;
        } else {
            $objectRegisterId = 0;
        }

        $savedPh = $this->saveProcesshookSettings($companyId, ProcesshookTypes::SEND_LEAD_NOTIFICATION,
            $processhookSettings, $objectRegisterId);
        $this->logger->info('savedPh ' . json_encode($savedPh));

        if (!empty($savedPh) && !$isPHookCreated) {
            $processhookId = $this->saveProcesshook($savedPh["objectRegisterId"],
                ProcesshookTypes::SEND_LEAD_NOTIFICATION, $companyId,
                $processhookSettings);
            $status = (null === $processhookId) ? false : true;
        }
        return $status;
    }


    /**
     * @param array $workflow
     * @param int $workflowId
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author AKi
     */
    private function saveWorkFlowInUserProcesshookHasWorkflow(array $workflow, int $workflowId): bool
    {
        // early exit
        if (empty($workflow)) {
            $this->logger->error('param $user is null', [__METHOD__, __LINE__]);
            return false;
        }
        foreach ($workflow as $key => $value) {
            $userProcesshookId = $value['userProcesshookId'];
            $successor_uph_id = null;
            //first element
            if ($key === 0) {
                $predecessor_uph_id = null;
                if (count($workflow) !== 1) {
                    $successor_uph_id = $workflow[$key + 1]['userProcesshookId'];
                }
            } else if ($key === count($workflow) - 1) {
                $predecessor_uph_id = $workflow[$key - 1]['userProcesshookId'];
                $successor_uph_id = null;
            } else {
                //last element
                $predecessor_uph_id = $workflow[$key - 1]['userProcesshookId'];
                $successor_uph_id = $workflow[$key + 1]['userProcesshookId'];
            }
            $errorCheck = $this->workflowRepository->insertUserProcesshookHasWorkflow($userProcesshookId,
                $workflowId,
                $predecessor_uph_id,
                $successor_uph_id);
            if ($errorCheck === null) {
                $this->logger->error("user_processhook has workflow is not saved properly",
                    [__METHOD__, __LINE__]);
                return false;
            }
        }
        return true;

    }

    /**
     * @param array|null $activeProcesshooksInfo
     * @return array
     * @autor Aki
     */
    private function formatDate(?array $activeProcesshooksInfo): ?array
    {
        if (empty($activeProcesshooksInfo)) {
            return null;
        }
        foreach ($activeProcesshooksInfo as $key => $processhookInfo) {
            $activeProcesshooksInfo[$key]['date'] = strtok($processhookInfo['date'], ' ');
        }
        return $activeProcesshooksInfo;

    }

    /**
     * @param string $processHookName
     * @param int $projectCampaignId
     * @return int|null
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Pradeep
     */
    public function isProcessHookPresent(string $processHookName, int $projectCampaignId): ?int
    {
        $hooks = $this->processhookRepository->getUserProcessHookByUniqueIndex($processHookName, $projectCampaignId);
        if (!empty($hooks)) {
            return true;
        }
        return false;
    }

}
