<?php

namespace App\Services;

use App\Services\CryptoService as CryptService;
use App\Repository\AdvertorialRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\ServerConfigurationRepository;
use App\Types\AdvertorialImageTypes;
use App\Types\StatusdefTypes;
use App\Types\UniqueObjectTypes;
use Exception;
use JsonException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Psr\Log\LoggerInterface;
use ZipArchive;


/**
 * Service for Advertorials and AdvertorialImages
 */
class AdvertorialService
{
    public const MIME_TEXT_PLAIN = 'text/plain';

    //ftp connection const's
    private const FTP_SSL_CONNECT_FAILED = 'FTP connection failed please check your ip address';
    private const FTP_LOGIN_FAILED = 'Please check user id and password';
    private const FTP_LOGIN_SUCCESS = 'SFTP Connection successful';
    private const NETWORK_PING_FAILED = 'Network IP can\'t be pinged';

    //payload const's
    private const SFTPCONNECTIONS = 'sftpConnections';
    private const SFTP_USER = 'sftp_user';
    private const SFTP_PASSWORD = 'sftp_pw';
    private const IMAGECONTENT = 'image_content';
    private const IMAGEHASHNAME = 'image_hash_name';
    private const COMPANY_ID = 'company_id';
    private const ADVERTORIAL_ID = 'advertorial_id';
    private const IP_ADDRESS = 'ip_address';
    private const DEFAULT_DOMAIN = 'default_domain';
    private const ID = 'id';
    private const IMAGES = 'images';
    private const SERVER = 'servers';
    private const SCAN_STATUS = 'scan_status';
    private const IMAGE_MINE_TYPE = 'mime_type';
    private const PATH = 'path';

    //response const's
    private const RESPONSE_TRANSFER_STATUS = 'transfer_status';
    private const RESPONSE_STATUS_REASON = 'transfer_status_reason';
    private const SFTP_ID = 'sftpId';
    private const SERVERID = 'serverId';

    private AdvertorialRepository $advertorialRepository;
    private LoggerInterface $logger;
    private ServerConfigurationRepository $serverConfigurationRepository;
    private string $msImageDispatcherUrl;
    private string $picturesInOne;
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    private ObjectRegisterRepository $objectRegisterRepository;
    private CryptService $cryptoService;
    private StatusdefTypes $statusdefTypes;

    //support filetype
    private const ZIP = 'zip';
    private const HTML = 'html';
    private string $uploadFileType;
    private string $htmlFileLocation;
    private string $htmlFileName;

    //directory const's
    public const DIRECTORY = 'directory';
    public const FILENAME = 'fileName';
    private string $picturesInOneUrl;

    public function __construct(
        AdvertorialRepository         $advertorialRepository,
        ServerConfigurationRepository $serverConfigurationRepository,
        string                        $msImageDispatcherUrl,
        string                        $picturesInOne,
        ObjectRegisterMetaRepository  $objectRegisterMetaRepository,
        ObjectRegisterRepository      $objectRegisterRepository,
        CryptService                  $cryptoService,
        StatusdefTypes                $statusdefTypes,
        LoggerInterface               $logger,
        string                        $uploadFileType = '',
        string                        $htmlFileLocation = '',
        string                        $htmlFileName = ''
    )
    {
        $this->statusdefTypes = $statusdefTypes;
        $this->cryptoService = $cryptoService;
        $this->serverConfigurationRepository = $serverConfigurationRepository;
        $this->advertorialRepository = $advertorialRepository;
        $this->logger = $logger;
        $this->msImageDispatcherUrl = $msImageDispatcherUrl;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->uploadFileType = $uploadFileType;
        $this->htmlFileLocation = $htmlFileLocation;
        $this->htmlFileName = $htmlFileName;
        $this->picturesInOneUrl = $picturesInOne;
    }

    /**
     * @inheritDoc
     */
    public function injectFileType(bool $isFileTypeZip): void
    {
        if ($isFileTypeZip) {
            $this->uploadFileType = self::ZIP;
        } else {
            $this->uploadFileType = self::HTML;
        }
    }

    /**
     * @inheritDoc
     */
    private function getFileType(): string
    {
        return $this->uploadFileType;
    }


    /**
     * @inheritDoc
     */
    public function injectHTMLFilePathInLocalDirectory(string $htmlFileLocation): void
    {
        $this->htmlFileLocation = $htmlFileLocation;
    }

    /**
     * @inheritDoc
     */
    public function injectHTMLFileName(string $htmlFileName): void
    {
        $this->htmlFileName = $htmlFileName;
    }

    /**
     * @inheritDoc
     */
    private function getHtmlFileName(): string
    {
        return $this->htmlFileName;
    }

    /**
     * @inheritDoc
     */
    private function getFilePath(): string
    {
        return $this->htmlFileLocation;
    }


    /**
     * @param int $company_id
     * @param int $seedPoolGroupId
     * @param string $advertorialSurrogateId
     * @param string $htmlFileContent
     * @param array $images <i>call by reference</i>:  all image data along with scan and transfer status is provided in this param
     * @return string base64 encoded html
     */
    public function extractAndTransferImagesFromHtml(HttpClientInterface $httpClient,
                                                     int                 $company_id,
                                                     int                 $seedPoolGroupId,
                                                     string              $advertorialSurrogateId,
                                                     string              $htmlFileContent,
                                                     array               &$images,
                                                     bool                $isFileTypeZip,
                                                     ?string             $filePath,
                                                     ?string             $htmlFileName
    ): string
    {
        //inject file type and location
        $this->injectFileType($isFileTypeZip);
        if ($isFileTypeZip === true && !is_null($filePath)) {
            $this->injectHTMLFilePathInLocalDirectory($filePath);
            $this->injectHTMLFileName($htmlFileName);
        }
        // param &$images is changed by $this->getHmtlImages !!!
        $newImgHtml = $this->getHmtlImages($httpClient, $company_id, $seedPoolGroupId, $htmlFileContent, $advertorialSurrogateId, $images);

        return $newImgHtml;
    }


    /**
     * @param int $company_id
     * @param string $htmlFileContent
     * @param string $advertorialSurrogateId
     * @param array $images <i>call by reference</i>
     * @return string base64 encoded html with new img src urls
     */
    private function getHmtlImages(HttpClientInterface $httpClient, int $company_id, int $seedPoolGroupId, string $htmlFileContent, string $advertorialSurrogateId, array &$images): string
    {
        // start the DOM parser
        $doc = new \DOMDocument();
        // load HTML content
        @$doc->loadHTML($htmlFileContent);

        // search for all img tags
        $imageElements = $doc->getElementsByTagName('img');
        foreach ($imageElements as $imageElement) {
            /**
             * @var \DOMNode $imageElement
             */
            $fileExtension = '';
            $image = new AdvertorialImageTypes();
            $this->logger->info('$image new', [$image, __METHOD__, __LINE__]);
            $image->setAdvertorialSurrogateId($advertorialSurrogateId); // not stored in table advertorial_images, but in advertorials
            $image->setOriginalUrl($imageElement->getAttribute('src')); // get content of img src attribut
            $image->setMimeType($this->getMimeTypeOfOriginalUrl($image->getOriginalUrl(), $fileExtension));

            $image->setOriginalImageName($this->getImageFileName($image->getOriginalUrl()));
            $image->setScanLineNumber($imageElement->getLineNo());
            if ($this->getFileType() === self::ZIP) {
                $imageDirectory = $this->getImageDirectory($this->getFilePath(), $this->getHtmlFileName());
                $imageLocalPath = $image->getOriginalUrl();
                //Get content locally
                $imageContent = base64_encode(@file_get_contents($imageDirectory . DIRECTORY_SEPARATOR . $imageLocalPath));
                if (empty($imageContent)) {
                    //some images are in server
                    $imageContent = base64_encode(@file_get_contents($image->getOriginalUrl()));
                }
            } else {
                $imageContent = base64_encode(@file_get_contents($image->getOriginalUrl())); // returns "" in case file_get_contents returns false
            }
            $image->setImageContent((string)$imageContent);
            $this->logger->info('$image prefilled', [$image, __METHOD__, __LINE__]);

            if (false != $imageContent) { // use weak comparison here to be abel to interpret "" as false
                try {
                    $this->logger->info('got image content. size  ', [strlen($imageContent), __METHOD__, __LINE__]);
                } catch (\Exception $e) {
                    $this->logger->warning($e->getMessage(), [__METHOD__, __LINE__]);
                }

                if ($image->getMimeType() !== self::MIME_TEXT_PLAIN) {
                    $this->logger->info('identified mime type', [$image->getMimeType(), __METHOD__, __LINE__]);

                    $image->setImageHashName(hash('md5', $image->getImageContent() . $image->getOriginalUrl()) . '.' . $fileExtension);

                    // add to advertorial_images table
                    // keep all images in mind (e.g. redis)
                    $newSrcUrl = $this->picturesInOneUrl . "/$company_id/$advertorialSurrogateId/" . $image->getImageHashName();
                    $imageElement->setAttribute('src', $newSrcUrl);

                    $image->setUrl($newSrcUrl);
                    $image->setScanStatus(true);
                    $image->setScanStatusReason('image url replaced by ' . $newSrcUrl);
                } else {
                    $this->logger->warning('did not identified mime type', [$image->getMimeType(), __METHOD__, __LINE__]);
                    $image->setScanStatus(false);
                    $image->setScanStatusReason('cannot replace tracking pixel ' . $image->getOriginalUrl());
                }
            } else {
                $this->logger->warning('could not get image content from url ', [$image->getOriginalUrl(), __METHOD__, __LINE__]);
                $image->setScanStatus(false);
                $image->setScanStatusReason('could not load image content of ' . $image->getOriginalUrl());
            }

            $images[] = (clone $image)->toArray();
            unset($image);
        }


        // TRANSFER IMAGES TO msImageDispatcher
        $response = $this->transferImagesToServers($httpClient, $company_id, $seedPoolGroupId, $advertorialSurrogateId, $images);
        $this->logger->info('$response( transferImagesToServers )', [$response, __METHOD__, __LINE__]);

        // extract transfer_status from msImageDispatcher response for UI for each image transferred to servers
        try {
            foreach ($images as &$image) { // &$image => overwrite value in $image['transfer_status']
                $statusTxtForImage = [];

                foreach ($response as $transferStatus) {
                    $debugLine = __LINE__;

                    $serverName = 'unknown Server';

                    if (isset($transferStatus[self::SERVERID])) {
                        if ($transferStatus[self::SERVERID] != 0) {
                            $serverIpFtpUser = $this->serverConfigurationRepository->getServerIpFtpUserById((int)$transferStatus['sftpId']);
                            $serverName = "SFTP {$serverIpFtpUser} / " . $this->serverConfigurationRepository->getServerNameById((int)$transferStatus['serverId']);
                        } else {
                            $serverName = "SFTP #{$transferStatus[self::SERVERID]} / $this->picturesInOneUrl";
                        }
                    }

                    $this->logger->info("\$serverName {$transferStatus[self::SERVERID]}", [$serverName, __METHOD__, __LINE__]);
                    $this->logger->info('img image_hash_name === transferStatus image_hash_name', [$image['image_hash_name'], $transferStatus['image_hash_name'], __METHOD__, __LINE__]);

                    if ($image[self::SCAN_STATUS] === true && ($image[self::IMAGEHASHNAME] === $transferStatus[self::IMAGEHASHNAME])) {
                        $debugLine = __LINE__;
                        if (!empty($transferStatus[self::RESPONSE_STATUS_REASON])) {
                            $debugLine = __LINE__;

                            if ($transferStatus[self::RESPONSE_TRANSFER_STATUS] === true) {
                                $debugLine = __LINE__;
                                $statusTxtForImage[] = "<span class='okay'>{$serverName}: {$transferStatus['transfer_status_reason']}</span>";
                            } else {
                                $debugLine = __LINE__;
                                $statusTxtForImage[] = "<span class='bad'>{$serverName}: {$transferStatus['transfer_status_reason']}</span>";
                            }
                        } else {
                            $debugLine = __LINE__;
                            $statusTxtForImage[] = "<span class='bad'>no status provided for {$serverName} / {$transferStatus['image_hash_name']}</span>";
                        }
                    } else {
                        $image[self::RESPONSE_TRANSFER_STATUS] = "<span class='bad'>no status provided</span>";
                    }

                    $this->logger->info('$debugLine', [$debugLine, __METHOD__, __LINE__]);
                }
                $debugLine = __LINE__;

                $statusTxtForImage = array_unique($statusTxtForImage);
                $image['transfer_status'] = implode('<br>', $statusTxtForImage);
            }
            /*} else {
                foreach ($images as &$image) {
                    $image['transfer_status'] = "<span class='bad'>no status provided</span>";
                }
            }*/

            // TODO: REMOVE
            if ($_ENV['APP_ENV'] === 'dev') {
                try {
                    file_put_contents(__DIR__ . '/../../var/log/imagesNewStatus.json', json_encode($images, JSON_PRETTY_PRINT));
                } catch (\Exception $e) {
                    $this->logger->warning($e->getMessage(), [__METHOD__,__LINE__]);
                }
            }

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), ['debugLine' => $debugLine, __METHOD__, __LINE__]);
        }

        return base64_encode($doc->saveHTML());
    }


    /**
     * @param string $url
     * @param string $fileExtension <i>!!! call by reference !!!</i> the file extension e.g. <b>png</b>
     * @return string the mime type e.g. <b>image/png</b>
     */
    private function getMimeTypeOfOriginalUrl(string $url, string &$fileExtension): string
    {
        $mime = self::MIME_TEXT_PLAIN;
        $mimeArray = explode('.', $url);
        $fileExtension = $mimeArray[count($mimeArray) - 1];

        if (in_array(strtolower($fileExtension), ['png', 'gif', 'jpg', 'jpeg', 'tiff', 'webp', 'bmp', 'svg'])) { // added svg and bmp
            $mime = "image/$fileExtension";
        } else {
            $fileExtension = '';
        }

        return $mime;
    }


    /**
     * @param string $url
     * @return string
     */
    private function getImageFileName(string $scrUrl): string
    {
        $slashParts = explode('/', $scrUrl);
        $fileName = $slashParts[count($slashParts) - 1];
        return $fileName;
    }


    /**
     * @param HttpClientInterface $httpClient
     * @param int $company_id
     * @param string $advertorialSurrogateId
     * @param array $images
     * @return array
     * @throws TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     */
    private function transferImagesToServers(
        HttpClientInterface $httpClient,
        int                 $company_id,
        int                 $seedPoolGroupId,
        string              $advertorialSurrogateId,
        array               &$images
    ): array
    {
        $this->logger->info('PARAMS', [$company_id, $seedPoolGroupId, $advertorialSurrogateId, 'no of Images' => count($images), __METHOD__, __LINE__]);
        $serverConfiguration = $this->serverConfigurationRepository->getServerConfiguration($company_id, $seedPoolGroupId, $this->statusdefTypes::ACTIVE);
        $this->logger->info('$serverConfiguration', [$serverConfiguration, __METHOD__, __LINE__]);
        $companySftpConnections = $this->serverConfigurationRepository->getServerSftpConfigurationByCompanyId($company_id, $seedPoolGroupId, $this->statusdefTypes::ACTIVE);
        $this->logger->info('$companySftpConnections', [$companySftpConnections, __METHOD__, __LINE__]);

        //Get default server config from superuser
        $defaultPagesInOneConfig = $this->getDefaultPagesInOneConfig(
            $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin()
        )['settings'];

        $server = [];
        //Add path and images to server
        $server[self::PATH] = ['company_id' => $company_id, 'advertorial_id' => $advertorialSurrogateId];
        $server[self::IMAGES] = $images;

        // transfer images to default ftp(d.h pictures-in-one.net)
        $t1 = microtime(true);
        $payload = $this->generatePayloadForImagesDispatchDefaultServer($defaultPagesInOneConfig, $server);

        // dispatch images to default server ftp(d.h pictures-in-one.net)
        $imageDispatchResponsePicturesInOne = $this->dispatchImages($httpClient, $payload);
        $t2 = microtime(true);

        $this->logger->info('$imageDispatchResponsePicturesInOne', [$imageDispatchResponsePicturesInOne, __METHOD__, __LINE__]);
        $this->logger->info('dispatchtime for pictures-in-one', [($t2 - $t1), __METHOD__, __LINE__]);


        if (empty($serverConfiguration)) {
            //only return the PIO response
            return $imageDispatchResponsePicturesInOne['transferStatus'];
        }

        //Transfer images to each server
        foreach ($serverConfiguration as $key => $srv) {
            $defaultDomain = "";
            if(!empty($srv[self::DEFAULT_DOMAIN])){
                $defaultDomain = explode('/', $srv[self::DEFAULT_DOMAIN])[2];
            }
            $this->logger->info('$defaultDomain', [$defaultDomain, __METHOD__,__LINE__]);

            $server[self::SERVER][$key][self::ID] = $srv['id'];
            $server[self::SERVER][$key][self::IP_ADDRESS] = $srv['ip_address'];
            $server[self::SERVER][$key][self::DEFAULT_DOMAIN] = $defaultDomain;

            $sftpConnections = [];
            //Map all the ftp configuration to server
            foreach ($companySftpConnections as $ftp) {
                $this->logger->info('000SFTP_PW', [$ftp['sftp_pw'], __METHOD__, __LINE__]);
                if (!empty($ftp['sftp_pw'])) {
                    try {
                        $ftp['sftp_pw'] = $this->cryptoService->secureInOneDecryptString(base64_decode($ftp['sftp_pw']), true);
                        $this->logger->info('000SFTP_PW DECODED', [$ftp['sftp_pw'], __METHOD__, __LINE__]);
                    } catch (Exception $e) {
                        $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    } catch (ClientExceptionInterface $e) {
                        $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    } catch (RedirectionExceptionInterface $e) {
                        $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    } catch (ServerExceptionInterface $e) {
                        $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    } catch (TransportExceptionInterface $e) {
                        $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    }
                }
                if ($ftp['server_configuration_id'] === $srv[self::ID]) {
                    $sftpConnections[] = $ftp;
                }
            }

            if($_ENV['APP_ENV']==='dev') {
                @file_put_contents(__DIR__.'/../../var/log/ftpconnections.json', json_encode($sftpConnections, JSON_PRETTY_PRINT), FILE_APPEND);
            }

            $server['servers'][$key]['sftpConnections'] = $sftpConnections;
            $this->logger->info('XXX $servers', [print_r($server,true), __METHOD__, __LINE__]);

            if( $_ENV['APP_ENV'] === 'dev') {
                try {
                    file_put_contents(__DIR__ . '/../../var/log/servers.json', json_encode($server, JSON_PRETTY_PRINT));
                    file_put_contents(__DIR__ . '/../../var/log/serversb64.json', base64_encode(json_encode($server)));
                } catch (\Exception $e) {
                    $this->logger->warning('file_put_contents failed', [__METHOD__, __LINE__]);
                }
            }
        }
        $t1 = microtime(true);
        $payload = $this->generatePayloadForImagesDispatchServers($server);

        // dispatch images to all other (except pictures-in-one) servers configured in server_sftp_configuration
        $imageDispatchResponse = $this->dispatchImages($httpClient, $payload);
        $t2 = microtime(true);


        $this->logger->info('dispatchtime for other servers', [($t2-$t1), __METHOD__, __LINE__]);
        $this->logger->info('$imageDispatchResponsePicturesInOne', [$imageDispatchResponsePicturesInOne, __METHOD__, __LINE__]);
        $this->logger->info('$imageDispatchResponse', [$imageDispatchResponse, __METHOD__, __LINE__]);
        $this->logger->info('merge', [array_merge($imageDispatchResponse['transferStatus'], $imageDispatchResponsePicturesInOne['transferStatus']), __METHOD__, __LINE__]);

        return array_merge($imageDispatchResponse['transferStatus'], $imageDispatchResponsePicturesInOne['transferStatus']);
    }

    /**
     * @param HttpClientInterface $httpClient
     * @param string $payload
     * @return array
     * @throws TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     */
    private function dispatchImages(HttpClientInterface $httpClient, string $payload): array
    {
        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ],
            'body' => $payload,
        ];

        try {
            $response = $httpClient->request('POST', $this->msImageDispatcherUrl . '/advertorial/images/dispatch', $options);
            $this->logger->info('$response', [$response->getStatusCode(), $response->getInfo(), __METHOD__, __LINE__]);
            $reponseContent = $response->getContent();
            $this->logger->info('$reponseContent', [$reponseContent, __METHOD__, __LINE__]);

            $transferStatus = json_decode($reponseContent, true, 10, JSON_OBJECT_AS_ARRAY | JSON_THROW_ON_ERROR);

            $this->logger->info('$transferStatus', [$transferStatus, __METHOD__, __LINE__]);
        } catch (\Exception $e) {

            try {
                $statusCode = $response->getStatusCode();
            } catch (TransportExceptionInterface $e) {
                $this->logger->warning($e->getMessage(), [__METHOD__, __LINE__]);
                $statusCode = $e->getMessage();
            }

            return ['status' => false, 'msg' => $e->getMessage(), 'transferStatus' => [], 'statusCode' => $statusCode];
        }

        if ($response->getStatusCode() === 200 && !empty($transferStatus)) {
            return ['status' => true, 'msg' => 'received transfer status', 'transferStatus' => $transferStatus, 'statusCode' => $response->getStatusCode()];
        }

        return ['status' => false, 'msg' => 'something went wrong', 'transferStatus' => $transferStatus, 'statusCode' => $response->getStatusCode()];

    }

    /**
     * @param array $servers
     * @return string
     */
    private function generatePayloadForImagesDispatchServers(array $server): string
    {
        if ($_ENV['APP_ENV'] === 'dev') {
            file_put_contents(__DIR__ . '/../../var/log/ms_dispatcher_payload_b64.json', json_encode($server, JSON_PRETTY_PRINT));
        }
        return (base64_encode(json_encode($server)));
    }

    /**
     * @param array $defaultConfig
     * @param array $server
     * @return string
     */
    private function generatePayloadForImagesDispatchDefaultServer(array $defaultConfig, array $server): string
    {
        $payload['path'] = $server[self::PATH];
        $payload[self::SERVER][0][self::IP_ADDRESS] = $defaultConfig['ip_address'];
        $payload[self::SERVER][0][self::ID] = '0';
        $payload[self::SERVER][0][self::SFTPCONNECTIONS][0][self::SFTP_USER] = $defaultConfig['sftp_user'];
        $payload[self::SERVER][0][self::SFTPCONNECTIONS][0][self::SFTP_PASSWORD] = base64_encode($defaultConfig['sftp_password']);
        $payload[self::SERVER][0][self::SFTPCONNECTIONS][0][self::ID] = '0';
        $payload[self::IMAGES] = $server['images'];

        if ($_ENV['APP_ENV'] === 'dev') {
            file_put_contents(__DIR__ . '/../../var/log/payloadJsonPIO.json', json_encode($payload, JSON_PRETTY_PRINT));
        }

        return (base64_encode(json_encode($payload)));
    }

    /**
     * @param $superAdminObjectRegisterId
     * @return array
     */
    public function getDefaultPagesInOneConfig($superAdminObjectRegisterId): array
    {
        // Get the mosento config from database
        $config = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails($superAdminObjectRegisterId,
            UniqueObjectTypes::SUPER_ADMIN_DEFAULT_FTP)['meta_value'];
        //extract the required params
        try {
            $config = json_decode($config, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage());
        }
        return $config;

    }

    /**
     * @param string $ipAddress
     * @param string $sftpUserId
     * @param string $sftpPassword
     * @return array
     * @author Aki
     */
    public function checkFtpConnection(string $ipAddress, string $sftpUserId, string $sftpPassword): array
    {
        $this->logger->info('params' , [$ipAddress, $sftpUserId, $sftpPassword, __METHOD__, __LINE__]);
        //first ping the ip
        $output = [];
        $result = [];

        try{
            //todo:try some other method, tried with fsocket open but not working !!
            $execRes = exec("ping -c 3 " . $ipAddress, $output, $result);
        }catch(Exception $e){
            $response['status'] = $e->getMessage();
            $response['success'] = false;
            return $response;
        }

        //execute only if ping is successful
        if ($result !== 0) {
            $response['status'] = self::NETWORK_PING_FAILED;
            $response['success'] = false;
            return $response;
        }
        $ftpSslConnect = ftp_ssl_connect($ipAddress);
        //check ftp connection
        if (false === $ftpSslConnect) {
            $this->logger->warning('SFTP Connection failed');
            $response['status'] = self::FTP_SSL_CONNECT_FAILED;
            $response['success'] = false;
            return $response;
        }
        //login to ftp connection
        if (false === ftp_login($ftpSslConnect, $sftpUserId, $sftpPassword)) {
            $this->logger->warning('SFTP Login failed');
            $response['status'] = self::FTP_LOGIN_FAILED;
            $response['success'] = false;
            return $response;
        }
        $response['status'] = self::FTP_LOGIN_SUCCESS;
        $response['success'] = true;
        //return success message
        return $response;
    }

    /**
     * @param string $sourceFolder
     * @param string $zipFileName
     * @return bool
     * @author Aki
     */
    public function unZipFile(string $sourceFolder, string $zipFileName): bool
    {
        $unzipStatus = false;
        $zipFile = $sourceFolder . DIRECTORY_SEPARATOR . $zipFileName;
        //extract the files
        try {
            $zip = new ZipArchive;
            if ($zip->open($zipFile, ZipArchive::CREATE) === true) {
                $unzipStatus = $zip->extractTo($sourceFolder);
                $zip->close();
            }
            $this->logger->info("$zipFile unziping status", [$unzipStatus, __METHOD__, __LINE__]);
        } catch (Exception $e) {
            $this->logger->error("Could not unzip $zipFile", [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }
        return $unzipStatus;
    }

    /**
     * @param string $destination
     * @return array|null
     * @author Aki
     */
    public function getHtmlFileInfoFromUploadedZip(string $destination): ?array
    {
        //scan the initial directory z.B $destination = /199/07a1d3420b904d5a34da52aaca000ff4/
        $htmlFileLocation = $this->scanDirectoryForHTMLFile($destination);
        if (!is_null($htmlFileLocation)) {
            return $htmlFileLocation;
        }
        //loop through all the directory's for html file
        $directoryIterator = new RecursiveDirectoryIterator($destination);
        foreach (new RecursiveIteratorIterator($directoryIterator) as $directory) {
            $this->logger->info('Search directory',[$directory,__METHOD__,__LINE__]);
            $htmlFileLocation = $this->scanDirectoryForHTMLFile($directory);
            if (!is_null($htmlFileLocation)) {
                return $htmlFileLocation;
            }
        }

        return null;
    }

    /**
     * @param string $directory
     * @return array|null
     * author Aki
     */
    private function scanDirectoryForHTMLFile(string $directory): ?array
    {
        if (1 === preg_match('/__MACOSX/', $directory)) {
            return null;
        }

        try {
            $folderContents = scandir($directory);
            $this->logger->info('scan directory '.$directory,[$folderContents,__LINE__,__METHOD__]);
            // check if $folderContents is a directory and actually has items
            if (is_array($folderContents) && count($folderContents)) {
                foreach ($folderContents as $item) { // loop through directory folderContents
                    if (substr(strtolower($item), -5) === ".html") { // checking if a file ends with .html
                        $this->logger->info('$html folder and item',[$directory,$item,__METHOD__,__LINE__]);
                        return
                            [self::DIRECTORY => $directory,
                                self::FILENAME => $item
                            ];
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return null;

    }

    /**
     * @param string $filePath
     * @param string $fileName
     * @return string
     * @author Aki
     */
    private function getImageDirectory(string $filePath, string $fileName): string
    {
        return str_replace($fileName, '', $filePath);
    }

    /**
     * @param string $dir
     * @return bool
     * @author Aki
     */
    public function removeAllFilesAndDirectoryFromFolder(string $dir): bool
    {
        try {
            if (is_dir($dir)) {
                $objects = scandir($dir);
                foreach ($objects as $object) {
                    if ($object !== "." && $object !== "..") {
                        if (filetype($dir . "/" . $object) === "dir") {
                            $this->removeAllFilesAndDirectoryFromFolder($dir . "/" . $object);
                        } else {
                            unlink($dir . "/" . $object);
                        }
                    }
                }
                rmdir($dir);
            }
        } catch (Exception $e) {
            $this->logger->error('Files removal from folder', [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }
        return true;
    }
}
