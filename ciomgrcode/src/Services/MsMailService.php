<?php


namespace App\Services;


use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Psr\Log\LoggerInterface;

use const MaxMind\Db\Reader\_MM_MAX_INT_BYTES;


class MsMailService
{

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->httpClient = HttpClient::create(['http_version' => '2.0']);
    }


    public function getMailParameters(): array
    {
        try {
            $response = $this->httpClient->request('GET', $_ENV[ 'MSMAIL_URL' ] . '/email/configuration/params');
            $jsonResponse = $response->getContent();
            $content = json_decode($jsonResponse, true, 512, JSON_OBJECT_AS_ARRAY);
        } catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage());
            return [];
        }
        return $content;
    }

    /**
     * @param string $emailConfigData
     * @return array
     * @author aki
     * @internal check the configuration of the super admin
     */
    public function sendTestEmail(string $emailConfigData): array
    {
        try {
            $response = $this->httpClient->request('POST',
                $_ENV[ 'MSMAIL_URL' ] . '/email/test_cio_config',
                ['body' => $emailConfigData]
            );
            $jsonResponse = $response->getContent();
            $content = json_decode($jsonResponse, true, 512, JSON_OBJECT_AS_ARRAY);
        } catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface $e) {
            $this->logger->error($e->getMessage());
            return [];
        }
        return $content;
    }


    /**
     * @param string $emailContentType
     * @param array $messageParameterTypes
     * @param array $mailInterface
     * @return mixed|string
     * @throws TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     */
    public function sendStandardMail(string $emailContentType, array $messageParameterTypes, array $mailInterface)
    {
        /*
            MSMAIL endpoint expects body content:
            $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);
            $data['emailContentTypes']; // one string of EmailContentTypes->{key} e.g. "account-deletion-notification", where key is "accountDeletionNotification"
            $data['messageParameterTypes']; // firstName, lastName, email, activationLink, ..., filled with values (then processed) or empty '' (not processed)
            $data['mailInterface']; // configuration settings of email => senderName, senderPw, smtpserver, port, and so on
        */

        $bodyContent = base64_encode(json_encode([
            'emailContentTypes' => $emailContentType,
            'messageParameterTypes' => $messageParameterTypes,
            'mailInterface' => $mailInterface,
        ], JSON_THROW_ON_ERROR));

        $this->logger->info('$bodyContent', [$bodyContent, __METHOD__, __LINE__]);

        try {
            $response = $this->httpClient->request('POST', $_ENV[ 'MSMAIL_URL' ] . '/email/sendstandardmail',
                ['body' => $bodyContent]);
        } catch (TransportExceptionInterface $e) {
            throw new \Exception($e->getMessage(), __LINE__);
            return 'error: ' . $e->getMessage() . ', line: ' . __LINE__;
        } catch (ServerExceptionInterface $e) {
            throw new \Exception($e->getMessage(), __LINE__);
            return 'error: ' . $e->getMessage() . ', line: ' . __LINE__;
        }

        $this->logger->info('statuscode', [$response->getStatusCode(), __METHOD__, __LINE__]);

        $responseContent = json_decode($response->getContent(), true, 512, JSON_OBJECT_AS_ARRAY);
        return $responseContent[ 'msg' ];
    }

}
