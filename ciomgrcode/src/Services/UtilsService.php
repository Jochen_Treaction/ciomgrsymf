<?php


namespace App\Services;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class UtilsService
{
    private $logger;


    public function __construct()
    {
    }


    /**
     * converts a snakecase string (e.g. i_am_snakecase) to string to use as label in HTML form elements (e.g. i am snakecase).
     * <b>string</b> conversion methods: <b>string</b>SnakeCaseToUiLabel
     * @param string $snakeCaseString
     * @return string
     * @author jsr
     */
    public function stringSnakeCaseToUiLabel(string $snakeCaseString): string
    {
        $uiString = '';
        foreach (explode('_', $snakeCaseString) as $string) {
            $uiString .= ucfirst($string) . ' ';
        };
        return trim($uiString);
    }


    /**
     * converts a snakecase string (e.g. i_am_snakecase) to string to CamelCase (e.g. IAmSnakecase).
     * <b>string</b> conversion methods: <b>string</b>SnakeCaseToCamelCase
     * @param string $snakeCaseString
     * @return string
     * @author jsr
     */
    public function stringSnakeCaseToCamelCase(string $snakeCaseString): string
    {
        $camelCaseString = '';
        foreach (explode('_', $snakeCaseString) as $string) {
            $camelCaseString .= ucfirst($string);
        };
        return trim($camelCaseString);
    }

    /**
     * @param $sting
     * @return string
     * @author Aki
     */
    public function stringToSnakeCase($normalString): string
    {
        $snakeCaseString = '';
        $normalStringInArray = preg_split('/\s+/', $normalString);
        foreach ($normalStringInArray as $key => $string) {
            if ($key !== count($normalStringInArray) - 1) {
                $snakeCaseString .= strtolower($string) . '_';
            } else {
                $snakeCaseString .= strtolower($string);
            }
        }
        return trim($snakeCaseString);
    }


    /**
     * copied from AIO to do doi encoding for activationlink in passwort reset when registration source was "AIO" in users_meta-table
     * @param array $data
     * @return string
     * @throws JsonException
     * @author Pradeep
     */
    public function encryptDataToNameValuePair(array $data): string
    {
        $encrypt_str = '';
        if (empty($data)) {
            return $encrypt_str;
        }
        try {
            foreach ($data as $key => $value) {
                if (empty($key)) {
                    continue;
                }
                $encrypt_arr[] = [
                    'name' => $key,
                    'value' => $value
                ];
                /*                $encrypt_arr[ 'name' ] = $key;
                                $encrypt_arr[ 'value' ] = $value;*/
            }
            if (empty($encrypt_arr)) {
                return $encrypt_str;
            }
            $encrypt_json = json_encode($encrypt_arr, JSON_THROW_ON_ERROR);
            $encrypt_str = base64_encode($encrypt_json);
            return $encrypt_str;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $encrypt_str;
        }
    }

    /**
     * @param string $userProcesshookName
     * @return string|null
     * @author Pradeep
     */
    public function getProcesshookTwigTemplateName(string $userProcesshookName): ?string
    {
        if (empty($userProcesshookName)) {
            return '';
        }
        if ($userProcesshookName === 'send_lead_notification') {
            return 'wizard_send_lead_notification.html.twig';
        }
        if ($userProcesshookName === 'start_marketing_automation') {
            return 'wizard_start_marketing_automation.html.twig';
        }
        if ($userProcesshookName === 'send_double_opt_in_mail') {
            return 'wizard_send_double_opt_in_mail.html.twig';
        }
        if ($userProcesshookName === 'call_contact_event') {
            return 'wizard_call_contact_event.html.twig';
        }
        if ($userProcesshookName === 'send_double_opt_in_mail_or_call_contact_event') {
            return 'wizard_send_double_opt_in_mail_or_call_contact_event.html.twig';
        }
        if ($userProcesshookName === 'set_smart_tag') {
            return 'wizard_set_smart_tag.html.twig';
        }
        if ($userProcesshookName === 'remove_smart_tag') {
            return 'wizard_remove_smart_tag.html.twig';
        }
        if ($userProcesshookName === 'show_popup') {
            return 'wizard_show_popup.html.twig';
        }
        if ($userProcesshookName === 'hide_or_unhide_section') {
            return '';
        }
    }

    /**
     * @param JsonResponse $response
     * @param string $message
     * @param bool $status
     * @param array|null $response_body
     * @return JsonResponse
     * @throws \JsonException
     * @author Aki
     */
    public function setResponseWithMessage(JsonResponse $response, string $message, bool $status, array $response_body = null): Response
    {

        return $response->setContent(json_encode([
            'status' => $status,
            'message' => $message,
            'data' => $response_body,
        ], JSON_THROW_ON_ERROR));
    }
}
