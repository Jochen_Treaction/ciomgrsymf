<?php

namespace App\Repository;

use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Psr\Log\LoggerInterface;
use Exception;

class SeedPoolRepository
{

    const DEFAULT_USER_ID = 99999;
    protected int $currentUserId;
    protected array $currentUser;
    protected Connection $conn;
    protected LoggerInterface $logger;


    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->conn = $connection;
        $this->logger =$logger;
        $this->currentUserId = self::DEFAULT_USER_ID;
    }


    /**
     * set class <b>global current user id</b>, if before you run <b style="color:blue"><u>inserts</u> or <u>updates</u></b>, otherwise a default user id (99999) is used
     * @param int $user_id users.id
     */
    public function setRepostitoriesCurrentUserId(int $user_id)
    {
        $this->currentUserId = $user_id;
    }


    /**
     * @return array <code>[]['id' => int, 'name' => string]</code>
     */
    public function getSeedPoolGroups(): array
    {
        $select = 'SELECT id, name FROM seed_pool_group ORDER BY name';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if ($stmt->rowCount() > 0) {
                return $records;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return [];
    }


    /**
     * @param int $seedPoolGroupId
     * @return string|null seed_pool_group.name or null
     */
    public function getSeedPoolGroupNameById(int $seedPoolGroupId):?string
    {
        $return = null;
        $select = 'SELECT name FROM seed_pool_group WHERE id=:seed_pool_group_id';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':seed_pool_group_id', $seedPoolGroupId, \PDO::PARAM_INT);
            $stmt->execute();
            $name = $stmt->fetchColumn();

            if ($stmt->rowCount() > 0) {
                $return = $name;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), ['seedPoolGroupId'=>$seedPoolGroupId,__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * @return array <code>[]['seed_pool_group_id' => int, 'seed_pool_group_name' => string, 'seed_pool_company_id' => int, 'seed_pool_id' => int]</code>
     */
    public function getSeedPoolGroupsByCompanyId(int $company_id):array
    {
        $select = 'SELECT DISTINCT 
            spg.id, 
            spg.name
            -- sp.company_id as seed_pool_company_id
            -- sp.id as seed_pool_id
        FROM seed_pool_group spg 
        JOIN seed_pool sp on sp.seed_pool_group_id = spg.id AND sp.company_id = :company_id
        ORDER BY spg.name';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if ($stmt->rowCount() > 0) {
                return $records;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return [];
    }

    /**
     * returns all seedpool-group records of that company as indexed array or empty array []
     * @return array <code>[]['id' => int, 'name' => string, 'company_id' => int, 'company_name' => string]</code>
     */
    public function getServerWizardSeedPoolGroupsByCompanyId(int $company_id):array
    {
        $records = [];
        $select =
            'SELECT DISTINCT 
                spg.id, 
                spg.name,
                sp.company_id,
                c.name as company_name
            FROM seed_pool_group spg 
            JOIN seed_pool sp on sp.seed_pool_group_id = spg.id AND sp.company_id = :company_id
            JOIN company c on c.id = sp.company_id
            ORDER BY spg.name';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $records;
    }


    /**
     * returns all seedpool-group records or empty array, if something went wrong
     * @return array <code>[][id' => int, 'name' => string, 'company_id' => int, 'company_name' => int]</code>
     */
    public function getServerWizardSeedPoolGroupsAll():array
    {
        $records = [];
        $select =
            'SELECT DISTINCT 
                spg.id, 
                spg.name,
                sp.company_id,
                c.name as company_name
            FROM seed_pool_group spg 
            JOIN seed_pool sp on sp.seed_pool_group_id = spg.id
            JOIN company c on c.id = sp.company_id
            ORDER BY spg.name';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $records;
    }




    /**
     * @return array <code>[]['seed_pool_group_id' => int, 'seed_pool_group_name' => string, 'seed_pool_company_id' => int]</code>
     */
    public function getSeedPoolGroupsByCompanyIdForAdvertorial(int $company_id):array
    {
        $select = 'SELECT DISTINCT 
            spg.id, 
            spg.name,
            sp.company_id as seed_pool_company_id,
            c.name as company_name
        FROM seed_pool_group spg 
        JOIN seed_pool sp on sp.seed_pool_group_id = spg.id AND sp.company_id = :company_id
        JOIN company c ON c.id = sp.company_id 
        ORDER BY spg.name';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if ($stmt->rowCount() > 0) {
                return $records;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return [];
    }


    /**
     * add a new Seed Pool Group
     * @param string $newSeedPoolGroupName
     * @return int 1, if record was inserted, 0, if record was present or an error occurred
     */
    public function setSeedPoolGroup(string $newSeedPoolGroupName): int
    {
        $insert = 'INSERT INTO seed_pool_group (name, created_by, updated_by) VALUES (:seed_pool_group_name, :created_by, :updated_by)';

        try {
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':seed_pool_group_name', $newSeedPoolGroupName);
            $stmt->bindParam(':created_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->execute();
            $return = $stmt->rowCount();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $return = 0;
        }
        return $return;
    }


    /**
     * delete a  Seed Pool Group
     * @param string $seedPoolGroupName
     * @return int 1, if record was inserted, 0, if record was present or an error occurred
     */
    public function deleteSeedPoolGroup(string $seedPoolGroupName): int
    {
        $insert = 'DELETE FROM seed_pool_group WHERE name=:seed_pool_group_name';

        try {
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':seed_pool_group_name', $seedPoolGroupName);
            $stmt->execute();
            $return = $stmt->rowCount();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $return = 0;
        }
        return $return;
    }


    /**
     * update a Seed Pool Group
     * @param string $seedPoolGroupName
     * @return int 1, if record was inserted, 0, if record was present or an error occurred
     */
    public function updateSeedPoolGroup(string $seedPoolGroupName): int
    {
        $insert = 'UPDATE seed_pool_group SET name=:seed_pool_group_name, updated_by=:updated_by WHERE name=:seed_pool_group_name AND id >0'; // seed_pool_group.name is unique !!!

        try {
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':seed_pool_group_name', $seedPoolGroupName);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->execute();
            $return = $stmt->rowCount();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $return = 0;
        }
        return $return;
    }


    /**
     * update a Seed Pool Group by id
     * @param int $id
     * @param string $seedPoolGroupName
     * @return int 1, if record was inserted, 0, if record was present or an error occurred
     */
    public function updateSeedPoolGroupById(int $id, string $seedPoolGroupName): int
    {
        $insert = 'UPDATE seed_pool_group SET name=:seed_pool_group_name, updated_by=:updated_by WHERE id=:id'; // seed_pool_group.name is unique !!!

        try {
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->bindParam(':seed_pool_group_name', $seedPoolGroupName);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->execute();
            $return = $stmt->rowCount();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $return = 0;
        }
        return $return;
    }


    /**
     * @param int $company_id
     * @return bool
     */
    public function hasSeedPool(int $company_id):bool
    {

        $this->logger->info('hasSeedPool $company_id', [$company_id, __METHOD__, __LINE__]);
        $select = 'SELECT id FROM seed_pool WHERE company_id=:company_id';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            if ($stmt->rowCount() > 0) {
                return true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return false;
    }


    /**
     * returns array with current user data:<br><code>[user_id, user_1stletter, first_name, last_name, user_roles (string [\"ROLE_MASTER_ADMIN\", \"ROLE_ADMIN\", \"ROLE_USER\"]), email, company_id, company_name, company_account_no, is_user, is_admin, is_master_admin, is_super_admin, login_ts, login_expires_ts, account_objectregister_id, project_objectregister_id]</code>
     * @return array
     */
    public function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {

            $company_id = $this->apcuCacheService->get($this->apcuCacheService::SELECTED_COMPANY_ID_OF_USER.$user['email']);
            if(false !== $company_id) {
                $user['company_id'] = $company_id;
            }

            return $user;
        }
        return [];
    }

    /**
     * @param int $companyId
     * @return array
     * @internal - get seed pools for account
     * @author Aki
     */
    public function getSeedPoolsForAccount(int $companyId): array
    {
        $select = 'SELECT * FROM seed_pool WHERE company_id=:company_id';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $companyId);
            $stmt->execute();
            $records = $stmt->fetchAll();

            if ($stmt->rowCount() > 0) {
                return $records;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return [];
    }

    /**
     * @return array
     * @internal - Get all seed pools
     * @author Aki
     */
    public function getAllSeedPools(): array
    {
        $select = 'SELECT * FROM seed_pool';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->execute();
            $records = $stmt->fetchAll();

            if ($stmt->rowCount() > 0) {
                return $records;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return [];

    }

    /**
     * @return array
     * @internal - Get all seed pool names belong to a company with company_id
     * @author Aki
     */
    public function getSeedPoolNamesForCompanyInArray($company_id): array
    {
//        $select = 'select s.name from seed_pool s
//                            join company c on c.id = s.company_id
//                            where c.id =:company_id';

        $select = 'SELECT sp.id, sp.seed_pool_group_id, sp.name, sg.name as seed_pool_group_name
            FROM seed_pool sp 
            JOIN seed_pool_group sg ON sg.id = sp.seed_pool_group_id 
            WHERE sp.company_id =:company_id
            ORDER BY sp.name';

        $seedPoolNamesForCompany = [];

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->execute();
            $records = $stmt->fetchAll();
            foreach ($records as $item) {
                $seedPoolNamesForCompany[$item['id']] = [
                    'seed_pool_group_id' => $item['seed_pool_group_id'],
                    'name' => $item['name'],
                    'seed_pool_group_name' => $item['seed_pool_group_name']
                ];
            }

            if ($stmt->rowCount() > 0) {
                return $seedPoolNamesForCompany;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $seedPoolNamesForCompany;

    }






    /**
     * @param $seedPoolGroupId
     * @return string
     * @internal Get seed pool name with id
     * @author Aki
     */
    public function getSeedPoolGroupNameWithId($seedPoolGroupId): string
    {
        $select = 'SELECT name FROM seed_pool_group WHERE id=:seed_pool_group_id';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':seed_pool_group_id', $seedPoolGroupId);
            $stmt->execute();
            $record = $stmt->fetch();

            if ($stmt->rowCount() > 0) {
                return $record['name'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return '';

    }

    public function createSeedPool($seed_pool_group_id,
                                   $company_id,
                                   $objectregister_id,
                                   $mosento_seed_pool_id,
                                   $name,
                                   $type
    ): ?int
    {
        $insertedId = null;
        $select = 'INSERT INTO seed_pool (
                       seed_pool_group_id,
                       company_id,
                       objectregister_id,
                       mosento_seed_pool_id,
                       name,
                       type,
                       created_by,
                       updated_by)
                    VALUES (:seed_pool_group_id,
                            :company_id,
                            :objectregister_id,
                            :mosento_seed_pool_id,
                            :name,
                            :type,
                            :created_by,
                            :updated_by);';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':seed_pool_group_id', $seed_pool_group_id);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->bindParam(':objectregister_id', $objectregister_id);
            $stmt->bindParam(':mosento_seed_pool_id', $mosento_seed_pool_id);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':type', $type);
            $stmt->bindParam(':created_by', $this->currentUserId);
            $stmt->bindParam(':updated_by', $this->currentUserId);
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                return null;
            }
            $insertedId = $this->conn->lastInsertId();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $insertedId;

    }

    /**
     * @param $seedPoolGroupName
     * @return mixed|string
     * @author Aki
     * @internal Get seed pool id with the group name
     */
    public function getSeedPoolGroupIdWithName($seedPoolGroupName)
    {
        $select = 'SELECT id FROM seed_pool_group WHERE name=:name';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':name', $seedPoolGroupName);
            $stmt->execute();
            $record = $stmt->fetch();

            if ($stmt->rowCount() > 0) {
                return $record['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return '';

    }

    /**
     * @param int $param
     * @return bool
     * @author Aki
     * @internal Delete seed pool with id
     * todo:add inbox monitor table in sql
     */
    public function deleteSeedPool(int $seedPoolId):bool
    {

        // if not remove the error !!
        $sql = 'DELETE FROM seed_pool WHERE id=:seed_pool_id';

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':seed_pool_id', $seedPoolId);
            $stmt->execute();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
        return true;

    }

    /**
     * @param string $mosentoSeedPoolId
     * @return array
     * @author Aki
     * @internal Get seed pool with mosento seed pool id
     */
    public function getSeedPoolFromMosentoSeedPoolId(string $mosentoSeedPoolId):array
    {
        $select = 'SELECT * FROM seed_pool WHERE mosento_seed_pool_id=:mosento_seed_pool_id';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':mosento_seed_pool_id', $mosentoSeedPoolId);
            $stmt->execute();
            $record = $stmt->fetchAll();

            if ($stmt->rowCount() > 0) {
                return $record[0];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return [];

    }
}
