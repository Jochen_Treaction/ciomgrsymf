<?php


namespace App\Repository;


// different from miointerim to mio
use App\Entity\Objectregister;
use App\Types\AccountStatusTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use RuntimeException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AuthenticationService;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\ParameterType;
use App\Types\ProjectWizardTypes;
use Psr\Log\LoggerInterface;


/**
 * @property StatusDefRepository statusDefRepository
 */
class ObjectRegisterRepository extends ServiceEntityRepository
{


    /**
     * @const MARKED_FOR_DELETION ="markedfordeletion"
     */
    private const MARKED_FOR_DELETION = "markedfordeletion";
    /**
     * @const META_KEY_COMPANY_ID = "company_id" in object_register_meta
     */
    private const META_KEY_COMPANY_ID = "company_id";
    /**
     * @const META_KEY_APIKEY='account_apikey';
     */
    private const META_KEY_APIKEY='account_apikey';
    /**
     * @var CompanyRepository
     */
    private $companyRepository;
    /**
     * @var Connection database connection
     */
    private $conn;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var array|null set in the constructor
     */
    private $uniqueObjectNames;
    /**
     * @var int $userId
     * taken from the logged in user of App\Services\AuthenticationService
     */
    private $userId;
    /**
     * @var App\Types\ProjectWizardTypes
     */
    private $projectWizardTypes;


    /**
     * ObjectRegisterRepository constructor.
     * @param ProjectWizardTypes $projectWizardTypes
     * @param CompanyRepository $companyRepository
     * @param Connection $connection
     * @param LoggerInterface $logger
     * @param ManagerRegistry $managerRegistry
     * @param AuthenticationUtils $authenticationUtils
     * @param StatusDefRepository $statusDefRepository
     * @param AuthenticationService $authenticationService
     * @author jsr
     */
    public function __construct(
        ProjectWizardTypes $projectWizardTypes,
        CompanyRepository $companyRepository,
        Connection $connection,
        LoggerInterface $logger,
        ManagerRegistry $managerRegistry,
        AuthenticationUtils $authenticationUtils,
        StatusDefRepository $statusDefRepository,
        AuthenticationService $authenticationService
    )
    {
        parent::__construct($managerRegistry, Objectregister::class);
        $this->projectWizardTypes = $projectWizardTypes;
        $this->conn = $connection;
        $this->logger = $logger;
        $this->statusDefRepository = $statusDefRepository;
        $this->companyRepository = $companyRepository;
        $this->uniqueObjectNames = $this->getDefinedUniqueObjectNames();

        // set useerId
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);

        $this->userId = (!empty($user) && !empty($user['user_id'])) ? $user['user_id'] : 1;
    }

    /**
     * moves all unique_object_type.unique_object_name in an private array $this->uniqueObjectNames for use in different methods
     * @return array|null
     * @TODO   : query result could be stored in cache
     * @author jsr
     */
    protected function getDefinedUniqueObjectNames(): ?array
    {
        $uniqueObjectNames = [];
        $selectDefinedUniqueObjectNames = "SELECT DISTINCT unique_object_name FROM unique_object_type ORDER BY 1";

        $stmt = $this->conn->prepare($selectDefinedUniqueObjectNames);

        try {
            $stmt->execute();
            $records = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in unique_object_type', [__METHOD__, __LINE__]);
                return null;
            } else {
                foreach ($records as $r) {
                    $uniqueObjectNames[] = $r['unique_object_name'];
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $uniqueObjectNames;
    }


    /**
     * <b>!!! used only to provide apikey in deprecated function CampaignService::getCampaignSettings !!!</b>
     * @param int $projectObjectRegisterId
     * @return int
     * @see ciomgrcode/src/Services/CampaignService.php getCampaignSettings
     */
    public function getCompanyObjectRegisterIdByProjectObjectRegisterID(int $projectObjectRegisterId):int
    {
        $companyObjectRegisterId = 0;
        $select = "
			SELECT c.objectregister_id 
			FROM company c  
			JOIN campaign cp ON cp.company_id = c.id AND cp.objectregister_id = :projectObjectRegisterId";

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':projectObjectRegisterId', $projectObjectRegisterId, ParameterType::INTEGER);
            $stmt->execute();
            $companyObjectRegisterId = $stmt->fetchColumn();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
            return 0;
        }
        return $companyObjectRegisterId;
    }



    /**
     * @param int $id
     * @return Objectregister|null
     * @throws NonUniqueResultException
     */
    public function getObjectregisterById(int $id): ?Objectregister
    {
        $result = null;

        try {
            $em = $this->getEntityManager();
            $query = $em->createQuery('SELECT o
                    FROM App\Entity\Objectregister o
                    WHERE o.id = :id')->setParameter('id', $id);
            $result = $query->getOneOrNullResult();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
            return null;
        }
        return $result;
    }

    /**
     * @param int $id
     * @param string|\App\Types\StatusdefTypes $newStatus
     * @return bool|null
     * @author Pradeep
     */
    public function updateStatus(
        int $id,
        string $newStatus // @Pradeep TODO: $newStatus should be of type StatusdefTypes
    ): ?bool {
        $status = false;
        try {
            $statusdefId = $this->statusDefRepository->getId($newStatus);
            $this->logger->info('$statusdefId', [$statusdefId, $newStatus, __METHOD__, __LINE__]);
            if ($id <= 0 || $statusdefId === null || $statusdefId <= 0) {
                throw new RuntimeException('Invalid Id or uniqueObjectTypeId or statusdefId provided');
            }
            $sql = 'UPDATE objectregister
                    SET statusdef_id=:statusdef_id
                    WHERE id = :id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $id, ParameterType::INTEGER);
            $stmt->bindParam(':statusdef_id', $statusdefId, ParameterType::INTEGER);
            $result = $stmt->execute();
            if (is_bool($result) && $result) {
                $status = $result;
            }
            return $status;
        } catch (Exception | RuntimeException  $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * use this method to create an initial entry for a unique object type (unique_object_type.unique_object_name) in objectregister table
     * @param int $company_id
     * @param string $unique_object_name ('customer' | 'customfields' | 'page' | 'project' | 'smarttags' | 'standalone survey' | 'survey' | 'webhook')
     * @return int|null objectregister.id | null
     * @author jsr
     */
    public function createInitialObjectRegisterEntry(int $company_id, string $unique_object_name): ?int
    {
        if (!in_array($unique_object_name, $this->uniqueObjectNames)) {
            return null;
        }
        $this->logger->info('$company_id', [$company_id, __METHOD__, __LINE__]);
        $this->logger->info('$unique_object_name', [$unique_object_name, __METHOD__, __LINE__]);

        $company = $this->companyRepository->getCompanyDetailById($company_id);
        $commentEntry = ucfirst($unique_object_name) . ' for account ' . $company['name'] . ' / ' . $company['account_no'];

        $unique_object_type_id = $this->getUniqueObjectTypeIdByName($unique_object_name);

        $insertIntoObjectRegister =
                "INSERT INTO objectregister  
                (object_unique_id, unique_object_type_id, statusdef_id, version, comment, created_by, updated_by) 
                VALUES 
                ('',:unique_object_type_id, 1, 1, :commentEntry, :userId, :userId)";

        try {
            $stmt = $this->conn->prepare($insertIntoObjectRegister);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, ParameterType::INTEGER);
            $stmt->bindValue(':commentEntry', $commentEntry, ParameterType::STRING);
            $stmt->bindValue(':userId', $this->userId, ParameterType::INTEGER);
            $stmt->execute();
            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert ' . $unique_object_name . ' entry in objectregister for company_id=' . $company_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error('could not insert ' . $unique_object_name . ' Entry in objectregister for company_id=' . $company_id,
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        if ($unique_object_name === UniqueObjectTypes::CUSTOMFIELDS) {
            $this->createObjectRegisterMetaEntryForCustomfieldOR($insertedId, $company_id);
        }
        return $insertedId;
    }

    /**
     * @param string $unique_object_name
     * @return int|null unique_object_type.id | null
     * @author jsr
     */
    public function getUniqueObjectTypeIdByName(string $unique_object_name): ?int
    {
        $this->logger->info('$this->uniqueObjectNames', [$this->uniqueObjectNames, __FUNCTION__, __LINE__]);

        $selectUniqueObjectType = "SELECT id FROM unique_object_type WHERE unique_object_name = :unique_object_name";

        $stmt = $this->conn->prepare($selectUniqueObjectType);
        $stmt->bindValue(':unique_object_name', $unique_object_name, ParameterType::STRING);

        try {
            $stmt->execute();
            $uniqueObjectTypeId = $stmt->fetchColumn();
            $this->logger->info('$uniqueObjectTypeId', [$uniqueObjectTypeId, __FUNCTION__, __LINE__]);

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in unique_object_type for unique_object_name=' . $unique_object_name,
                    [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $uniqueObjectTypeId;
    }

    private function createObjectRegisterMetaEntryForCustomfieldOR(int $object_register_id, int $company_id): ?int
    {
        $insertedId = null;
        $insertObjectRegisterMeta = "INSERT INTO object_register_meta 
            (object_register_id, meta_key, meta_value, created_by, updated_by)  
            VALUES 
            (:object_register_id, :meta_key, :meta_value, :created_by, :updated_by)";

        try {
            $stmt = $this->conn->prepare($insertObjectRegisterMeta);
            $stmt->bindValue(':object_register_id', $object_register_id, ParameterType::INTEGER);
            $meta_key = self::META_KEY_COMPANY_ID;
            $stmt->bindValue(':meta_key', $meta_key, ParameterType::STRING);
            $stringCompanyId = (string)$company_id;
            $stmt->bindValue(':meta_value', $stringCompanyId, ParameterType::STRING);
            $stmt->bindValue(':created_by', $this->userId, ParameterType::INTEGER);
            $stmt->bindValue(':updated_by', $this->userId, ParameterType::INTEGER);
            $stmt->execute();
            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert company_id' . $stringCompanyId . ' entry in object register meta for object_register_id=' . $object_register_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error('could not insert company_id' . $stringCompanyId . ' entry in object register meta for object_register_id=' . $object_register_id,
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $insertedId;
    }

    /**
     * @param string $unique_object_name
     * @return int|null objectregister.id
     */
    public function createInitialObjectRegisterEntryForNewAccount(): ?int
    {
        $insertedId = null;
        $unique_object_name = UniqueObjectTypes::ACCOUNT;
        $commentEntry = 'Account Object Register Entry';
        $unique_object_type_id = $this->getUniqueObjectTypeIdByName($unique_object_name);
        $insertIntoObjectRegister = "INSERT INTO objectregister  
                (unique_object_type_id, object_unique_id ,statusdef_id, version, comment, created_by, updated_by) 
                VALUES 
                (:unique_object_type_id, '' ,1, 1, :commentEntry, :created_by, :updated_by)";

        try {
            $stmt = $this->conn->prepare($insertIntoObjectRegister);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, ParameterType::INTEGER);
            $stmt->bindValue(':commentEntry', $commentEntry, ParameterType::STRING);
            $stmt->bindValue(':created_by', $this->userId, ParameterType::INTEGER);
            $stmt->bindValue(':updated_by', $this->userId, ParameterType::INTEGER);
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert ' . $unique_object_name . ' entry in objectregister for company_id=',
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error('could not insert ' . $unique_object_name . ' Entry in objectregister for company_id=',
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        $this->logger->debug('$insertedId', [$insertedId, __METHOD__, __LINE__]);
        return $insertedId;
    }

    /**
     * @param int $objectregister_id
     * @param string $name
     * @param string $account_no
     * @return bool
     */
    public function updateAccountObjectRegisterEntry(int $objectregister_id, string $name, string $account_no, string $apikey=null): bool
    {
        $comment = "Account for Company $name / $account_no";
        $updateObjectRegister = "UPDATE objectregister SET objectregister.comment= :accountcomment WHERE id = :id";
        try {
            $stmt = $this->conn->prepare($updateObjectRegister);
            $stmt->bindValue(':accountcomment', $comment, ParameterType::STRING);
            $stmt->bindValue(':id', $objectregister_id, ParameterType::INTEGER);
            $stmt->execute();
            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not update entry in objectregister for id=' . $objectregister_id,
                    [__METHOD__, __LINE__]);
                return false;
            } elseif(null !== $apikey) {

                // added 2021-03-08, since value is missing in AccountControler::acceptDPAForNewUser POST-Part
                $insertInitoObjectRegisterMeta =
                    "INSERT INTO object_register_meta 
					(object_register_id, meta_key, meta_value, updated_by, created_by) 
					VALUES(:objectregister_id, :meta_key, :meta_value, 999999, 999999)";
                $stmt = $this->conn->prepare($insertInitoObjectRegisterMeta);
                $stmt->bindValue(':objectregister_id', $objectregister_id, ParameterType::INTEGER);
                $meta_key = self::META_KEY_APIKEY;
                $stmt->bindValue(':meta_key', $meta_key);
                $stmt->bindValue(':meta_value', $apikey);
                $stmt->execute(); // fire & forget

                $this->logger->info('APIKEY IN OBJECT_REGISTER_META', [$stmt->rowCount()]);
            }
        } catch (Exception $e) {
            $this->logger->error('could not update entry in objectregister for id=' . $objectregister_id,
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }

        return true;
    }

    /**
     * check if a customfield entry exists in objectregister for a companies (param $company_id) project in campaigns
     * @param int $company_id
     * @return int|null (int) 0 if no record was found, null in case of error, objectregister.id in case of success
     * @author jsr
     */
    public function hasCustomfieldObjectRegisterEntryForProject(int $company_id): ?int
    {
        $useMioInterim = (bool)$_ENV['USE_MIOINTERIM'];

        if ($useMioInterim) {
            $selectCustomfieldEntry = "SELECT DISTINCT o.id
                FROM campaign cn
                JOIN objectregister prjor ON cn.objectregister_id = prjor.id
                JOIN unique_object_type uot ON prjor.unique_object_type_id = uot.id AND uot.unique_object_name = :project
                -- join project assigned customfields
                JOIN customfields cf ON cn.id = cf.campaign_id
                JOIN objectregister o ON cf.objectregister_id = o.id
                JOIN unique_object_type uot2 ON o.unique_object_type_id = uot2.id AND uot2.unique_object_name = :customfields
                WHERE cn.company_id = :company_id";
        } else {

            $selectCustomfieldEntry = "SELECT DISTINCT orm.object_register_id
                FROM object_register_meta orm
                JOIN objectregister o on orm.object_register_id = o.id
                JOIN unique_object_type u on o.unique_object_type_id = u.id AND u.unique_object_name = :customfields
                WHERE orm.meta_key = :meta_key
                AND orm.meta_value = :company_id";

            /*          this code does not work, since there is no initail entyr in customfields tables =>  line "JOIN customfields cf ON cn.id = cf.campaign_id" wil cause the statement to return null
                        $selectCustomfieldEntry = "SELECT DISTINCT o.id
                            FROM campaign cn
                            JOIN objectregister prjor ON cn.objectregister_id = prjor.id
                            JOIN unique_object_type uot ON prjor.unique_object_type_id = uot.id AND uot.unique_object_name = :project
                            -- join project assigned customfields
                            JOIN customfields cf ON cn.id = cf.campaign_id -- this does not work, there mus be a company_id in object_register_meta for customfields
                            JOIN objectregister o ON cf.objectregister_id = o.id
                            JOIN unique_object_type uot2 ON o.unique_object_type_id = uot2.id AND uot2.unique_object_name = :customfields
                            WHERE cn.company_id = :company_id";
            */
        }

        $stmt = $this->conn->prepare($selectCustomfieldEntry);
        $project = UniqueObjectTypes::PROJECT;
        $customfields = UniqueObjectTypes::CUSTOMFIELDS;

        $stringCompanyId = (string)$company_id;
        $stmt->bindParam(':company_id', $stringCompanyId, ParameterType::STRING);
        $customfields = UniqueObjectTypes::CUSTOMFIELDS;
        $stmt->bindParam(':customfields', $customfields, ParameterType::STRING);
        $meta_key = self::META_KEY_COMPANY_ID;
        $stmt->bindParam(':meta_key', $meta_key, ParameterType::STRING);

        try {
            $stmt->execute();
            $objectregisterEntry = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no corresponding row found for project customfields in objectregister for company_id=' . $company_id,
                    [__METHOD__, __LINE__]);
                return 0;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $objectregisterEntry;
    }


    /**
     * returns array of [campaign_id, campaign_name, unique_object_name, customfield_id] of page-/webhook-campaigns where an id of param [$customfield_ids] is used
     * @param int $company_id
     * @param array $customfield_ids
     * @return array|null empty array, if no entries where found, else array of [campaign_id, campaign_name, unique_object_name, customfield_id] or null in case of error
     * @author jsr
     */
    public function getCustomfieldUsedInPageOrWebhook(int $company_id, array $customfield_ids): ?array
    {
        // $list = "(" . implode(',', $customfield_ids) . ")";
        $this->logger->info('$customfield_ids', [$customfield_ids, __METHOD__, __LINE__]);
        $returnlist = [];


        // does not check status (e.g. active, ...) of webhook or page !!!
        /*        $selectCustomfieldEntry = "SELECT DISTINCT cn.id campaign_id, cn.name campaign_name, uot.unique_object_name, cf.ref_customfields_id ref_customfields_id
                        FROM campaign cn
                        JOIN objectregister prjor ON cn.objectregister_id = prjor.id
                        JOIN unique_object_type uot ON prjor.unique_object_type_id = uot.id AND uot.unique_object_name in ('page', 'webhook')
                        -- join project assigned customfields
                        JOIN customfields cf ON cn.id = cf.campaign_id AND cf.ref_customfields_id IN {$list}
                        JOIN objectregister o ON cf.object_register_id = o.id
                        JOIN unique_object_type uot2 ON o.unique_object_type_id = uot2.id AND uot2.unique_object_name = :customfields
                        WHERE cn.company_id = :company_id";
        */

        $selectCustomfieldEntry = "SELECT  c.id campaign_id, c.name campaign_name, uotp.unique_object_name, orm.meta_value json_customfields
			FROM campaign c
			JOIN objectregister o on c.objectregister_id = o.id
			JOIN statusdef sd ON o.statusdef_id = sd.id AND sd.value != 'archived'
			JOIN unique_object_type uotp on o.unique_object_type_id = uotp.id and uotp.unique_object_name = :pagepage
			JOIN object_register_meta orm on o.id = orm.object_register_id and orm.meta_key = 'customfield_reference'
			WHERE c.company_id = :company_id
			UNION
			SELECT  c.id, c.name, uotp.unique_object_name, orm.meta_value json_customfields
			FROM campaign c
			JOIN objectregister o on c.objectregister_id = o.id
			JOIN statusdef sd ON o.statusdef_id = sd.id AND sd.value != 'archived'
			JOIN unique_object_type uotp on o.unique_object_type_id = uotp.id and uotp.unique_object_name = :webhook
			JOIN object_register_meta orm on o.id = orm.object_register_id and orm.meta_key = 'customfield_reference'
			WHERE c.company_id = :company_id";

        $stmt = $this->conn->prepare($selectCustomfieldEntry);
        $stmt->bindParam(':company_id', $company_id, ParameterType::INTEGER);
        $page = UniqueObjectTypes::PAGE;
        $webhook = UniqueObjectTypes::WEBHOOK;
        $stmt->bindParam(':pagepage', $page, ParameterType::STRING);
        $stmt->bindParam(':webhook', $webhook, ParameterType::STRING);

        // take out cf-ids from json, update the  array before return it to the calling method

        try {
            $stmt->execute();
            $records = $stmt->fetchAll();

            if ($stmt->rowCount() == 0) {
                $this->logger->info('no corresponding row found in page or webhook setup for customfield_ids=' . implode(',', $customfield_ids),
                    [__METHOD__, __LINE__]);
                return [];
            } else {
                foreach ($records as $record) {

                    $customfields = json_decode($record['json_customfields'], true, 512, JSON_OBJECT_AS_ARRAY);

                    foreach ($customfields as $cf) {
                        if( in_array($cf['id'], $customfield_ids)) {
                            $returnlist[] = ['campaign_id'=>$record['campaign_id'], 'campaign_name'=>$record['campaign_name'], 'unique_object_name'=>$record['unique_object_name'], 'ref_customfields_id' => $cf['id']];
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $returnlist;
    }


    /**
     * @param int   $company_id
     * @param array $customfield_ids
     * @return array|null
     * @deprecated since 2021-02-01 use ObjectRegisterRepository::getNewCustomfieldUsedInPageOrWebhook
     */
    public function oldGetCustomfieldUsedInPageOrWebhook(int $company_id, array $customfield_ids): ?array
    {
        $list = "(" . implode(',', $customfield_ids) . ")";
        $this->logger->info('$list', [$list, __METHOD__, __LINE__]);

        // does not check status (e.g. active, ...) of webhook or page !!!
        $selectCustomfieldEntry = "SELECT DISTINCT cn.id campaign_id, cn.name campaign_name, uot.unique_object_name, cf.ref_customfields_id ref_customfields_id
                FROM campaign cn
                JOIN objectregister prjor ON cn.objectregister_id = prjor.id
                JOIN unique_object_type uot ON prjor.unique_object_type_id = uot.id AND uot.unique_object_name in ('page', 'webhook')
                -- join project assigned customfields
                JOIN customfields cf ON cn.id = cf.campaign_id AND cf.ref_customfields_id IN {$list}
                JOIN objectregister o ON cf.objectregister_id = o.id
                JOIN unique_object_type uot2 ON o.unique_object_type_id = uot2.id AND uot2.unique_object_name = :customfields
                WHERE cn.company_id = :company_id";

        $stmt = $this->conn->prepare($selectCustomfieldEntry);
        $stmt->bindParam(':company_id', $company_id, ParameterType::INTEGER);
        $customfields = UniqueObjectTypes::CUSTOMFIELDS;
        $stmt->bindParam(':customfields', $customfields, ParameterType::STRING);

        try {
            $stmt->execute();
            $records = $stmt->fetchAll();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no corresponding row found in page or webhook setup for customfield_ids=' . $list,
                    [__METHOD__, __LINE__]);
                return [];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $records;
    }



    /**
     * get project description json along with default settings
     * @return string|null json string from object_type_meta.meta_value or null in case of error or not found
     * @see    App\Types\ProjectWizardTypes
     * @author jsr
     */
    public function getProjectTypeDefaultSettingsAsJson(): ?string
    {
        $metaValue = $this->getProjectTypeDefaultSettingsAsArray();
        return ($metaValue === null) ? json_encode([]) : json_encode($metaValue);
    }


    /**
     * get project description json along with default settings
     * @return array|null array from object_type_meta.meta_value or null in case of error or not found
     * @see    App\Types\ProjectWizardTypes
     * @author jsr
     */
    public function getProjectTypeDefaultSettingsAsArray(): ?array
    {
        $selectObjectTypeMeta = "SELECT otm.meta_value 
            FROM object_type_meta otm
            JOIN unique_object_type uot on otm.unique_object_type_id = uot.id AND uot.unique_object_name = :unique_object_name
            WHERE otm.meta_key = 'type_default_settings'";

        $stmt = $this->conn->prepare($selectObjectTypeMeta);
        $uniqueObjectName = $this->projectWizardTypes->uniqueObjectName;
        $stmt->bindParam(":unique_object_name", $uniqueObjectName, ParameterType::STRING);
        try {
            $stmt->execute();
            $metaValue = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in unique_object_type for unique_object_name=' . $uniqueObjectName,
                    [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return json_decode($metaValue, true, 512, JSON_OBJECT_AS_ARRAY);
    }


    /**
     * @param int $company_id
     * @return int|null company.objectregister_id
     * @author jsr
     */
    public function getCompanyObjectRegisterId(int $company_id): ?int
    {
        $unique_object_name = UniqueObjectTypes::ACCOUNT;

        $selectObjectregisterId = "SELECT c.objectregister_id 
            FROM company c
            JOIN objectregister o ON c.objectregister_id = o.id
            JOIN unique_object_type uot ON (o.unique_object_type_id = uot.id AND uot.unique_object_name = :unique_object_name) 
            WHERE c.id = :company_id";

        $stmt = $this->conn->prepare($selectObjectregisterId);
        $stmt->bindParam(':company_id', $company_id, ParameterType::INTEGER);
        $stmt->bindParam(':unique_object_name', $unique_object_name, ParameterType::INTEGER);

        try {
            $stmt->execute();
            $objectregisterId = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no objectregister_id found for account  company_id=' . $company_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $objectregisterId;
    }


    /**
     * @param int $objectregister_id
     * @param string $deletion_date
     * @return int|null object_register_meta.id in case of success
     * @author jsr
     */
    public function addCompanyDeletionDate(int $objectregister_id, string $deletion_date): ?int
    {
        $objectRegisterMetaId = null;
        $insertIntoObjectRegisterMeta = "INSERT INTO object_register_meta (object_register_id, meta_key, meta_value) VALUES (:objectregister_id, :marked_for_deletion, :deletion_date)";
        $stmt = $this->conn->prepare($insertIntoObjectRegisterMeta);
        $stmt->bindParam(':objectregister_id', $objectregister_id, ParameterType::INTEGER);
        $marked_for_deletion = self::MARKED_FOR_DELETION;
        $stmt->bindParam(':marked_for_deletion', $marked_for_deletion, ParameterType::STRING);
        $stmt->bindParam(':deletion_date', $deletion_date, ParameterType::STRING);

        try {
            $stmt->execute();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('could not insert markedfordeletion row in object_register_meta for objectregister_id' . $objectregister_id,
                    [__METHOD__, __LINE__]);
                return null;
            } else {
                $objectRegisterMetaId = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $objectRegisterMetaId;
    }


    /**
     * @param int $objectregister_id
     * @return int|null $objectregister_id of deleted record (same as param $objectregister_id)
     * @author jsr
     */
    public function removeCompanyDeletionDate(int $objectregister_id): ?int
    {
        $deleteObjectRegisterMeta = "DELETE FROM object_register_meta WHERE object_register_id = :objectregister_id AND meta_key =:marked_for_deletion";
        $stmt = $this->conn->prepare($deleteObjectRegisterMeta);
        $objectRegisterMetaId = null;
        $stmt->bindParam(':objectregister_id', $objectregister_id, ParameterType::INTEGER);
        $marked_for_deletion = self::MARKED_FOR_DELETION;
        $stmt->bindParam(':marked_for_deletion', $marked_for_deletion, ParameterType::STRING);

        try {
            $stmt->execute();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('could not delete markedfordeletion row in object_register_meta for objectregister_id' . $objectregister_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $objectregister_id;
    }


    /**
     * @param int $company_objectregister_id
     * @return int|null statusdef.id, if objectRegister was updated successfully
     * @author jsr
     */
    public function setCompanyObjectRegisterStatusToDeleted(int $company_objectregister_id): ?int
    {
        $accountStatusId = $this->getAccountStatusIdByName(AccountStatusTypes::MARKEDFORDELETION);
        if (null === $accountStatusId) {
            $this->logger->error('no accountStatusId found for ' . AccountStatusTypes::MARKEDFORDELETION,
                [__METHOD__, __LINE__]);
            return null;
        }

        $updateCompanyObjectRegister = "UPDATE objectregister SET statusdef_id = :statusdef_id WHERE id = :company_objectregister_id";

        $stmt = $this->conn->prepare($updateCompanyObjectRegister);
        $stmt->bindParam(':statusdef_id', $accountStatusId, ParameterType::INTEGER);
        $stmt->bindParam(':company_objectregister_id', $company_objectregister_id, ParameterType::INTEGER);

        try {
            $stmt->execute();

            if ($stmt->rowCount() == 0) {
                $this->logger->error('could not update objectregister with markedfordeletion for objectregister_id' . $company_objectregister_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $accountStatusId;
    }

    /**
     * @param string $statusName
     * @return int|null
     * @author jsr
     */
    public function getAccountStatusIdByName(string $statusName): ?int
    {
        $selectAccountStatusId = "SELECT s.id 
            FROM status_model sm
            JOIN statusdef s ON sm.statusdef_id = s.id AND s.value = :status_name
            WHERE sm.modelname = :account";

        $stmt = $this->conn->prepare($selectAccountStatusId);
        $stmt->bindParam(':status_name', $statusName, ParameterType::STRING);
        $account = UniqueObjectTypes::ACCOUNT;
        $stmt->bindParam(':account', $account, ParameterType::STRING);

        try {
            $stmt->execute();
            $accountStatusId = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no statusdef.id found for status=' . $statusName, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $accountStatusId;
    }

    /**
     * @param int $company_objectregister_id
     * @param string $statusType AccountStatusTypes::<const>
     * @return int|null status_def.id of submitted $statusType in case of successful update, null if $statusType does not exist or update failed
     * @see    App\Types\AccountStatusTypes
     * @author jsr
     */
    public function setCompanyObjectRegisterStatusToAccountStatusType(
        int $company_objectregister_id,
        string $statusType
    ): ?int
    {
        if (!AccountStatusTypes::isCorrectType($statusType)) {
            return null;
        }

        $accountStatusId = $this->getAccountStatusIdByName($statusType);
        if (null === $accountStatusId) {
            $this->logger->error('no accountStatusId found for ' . AccountStatusTypes::MARKEDFORDELETION,
                [__METHOD__, __LINE__]);
            return null;
        }

        $updateCompanyObjectRegister = "UPDATE objectregister SET statusdef_id = :statusdef_id WHERE id = :company_objectregister_id";

        $stmt = $this->conn->prepare($updateCompanyObjectRegister);
        $stmt->bindParam(':statusdef_id', $accountStatusId, ParameterType::INTEGER);
        $stmt->bindParam(':company_objectregister_id', $company_objectregister_id, ParameterType::INTEGER);

        try {
            $stmt->execute();

            if ($stmt->rowCount() == 0) {
                $this->logger->error('could not update objectregister with markedfordeletion for objectregister_id' . $company_objectregister_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $accountStatusId;
    }

    /**
     * @param int $company_id
     * @return string|null string date "YYYY-MM-DD" if a "marked for deletion" entry is present in corresponding object_register_meta
     * @author jsr
     */
    public function getCompanyMarkedForDeletionDate(int $company_id): ?string
    {
        $selectObjectRegisterMetaValue = "SELECT orm.meta_value FROM company c
        JOIN objectregister o ON c.objectregister_id = o.id
        JOIN object_register_meta orm ON o.id = orm.object_register_id AND orm.meta_key = :markedfordeletion      
        WHERE c.id = :company_id";

        $stmt = $this->conn->prepare($selectObjectRegisterMetaValue);
        $stmt->bindParam(':company_id', $company_id, ParameterType::STRING);
        $markedfordeletion = AccountStatusTypes::MARKEDFORDELETION;
        $stmt->bindParam(':markedfordeletion', $markedfordeletion, ParameterType::STRING);

        try {
            $stmt->execute();
            $metaValue = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no markedForDeletion entry found for company_id=' . $company_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $metaValue;
    }

    /**
     * use this method to create an initial entry for a unique object type (unique_object_type.unique_object_name) in objectregister table
     * @param int $company_id
     * @param string $unique_object_name ('customer' | 'customfields' | 'page' | 'project' | 'smarttags' | 'standalone survey' | 'survey' | 'webhook')
     * @param string $statusdef ('active' | 'archived' | 'draft')
     * @return int|null objectregister.id | null
     * @author Aki
     */
    public function createInitialObjectRegisterEntryforCampaign(
        int $company_id,
        string $unique_object_name,
        string $statusdef
    ): ?int
    {
        if (!in_array($unique_object_name, $this->uniqueObjectNames)) {
            return null;
        }
        $this->logger->info('$company_id', [$company_id, __METHOD__, __LINE__]);
        $this->logger->info('$unique_object_name', [$unique_object_name, __METHOD__, __LINE__]);

        $company = $this->companyRepository->getCompanyDetailById($company_id);
        $commentEntry = ucfirst($unique_object_name) . ' for account ' . $company['name'] . ' / ' . $company['account_no'];

        $unique_object_type_id = $this->getUniqueObjectTypeIdByName($unique_object_name);
        $statusdef_id = $this->getStatusdefIdByName($statusdef);

        $insertIntoObjectRegister = "INSERT INTO objectregister  
                (unique_object_type_id, statusdef_id, version,object_unique_id, comment, created_by, updated_by) 
                VALUES 
                (:unique_object_type_id, :statusdef_id, 1,'', :commentEntry, :userId, :userId)";

        try {
            $stmt = $this->conn->prepare($insertIntoObjectRegister);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, ParameterType::INTEGER);
            $stmt->bindValue(':statusdef_id', $statusdef_id, ParameterType::INTEGER);
            $stmt->bindValue(':commentEntry', $commentEntry, ParameterType::STRING);
            $stmt->bindValue(':userId', $this->userId, ParameterType::INTEGER);
            $stmt->execute();
            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert ' . $unique_object_name . ' entry in objectregister for company_id=' . $company_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error('could not insert ' . $unique_object_name . ' Entry in objectregister for company_id=' . $company_id,
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        if ($unique_object_name === UniqueObjectTypes::CUSTOMFIELDS) {
            $this->createObjectRegisterMetaEntryForCustomfieldOR($insertedId, $company_id);
        }
        return $insertedId;
    }


    /**
     * returns objectregister_id of created workflow
     * @param int    $company_id
     * @param string $unique_object_name
     * @param string $statusdef
     * @return int|null
     */
    public function createObjectRegisterEntryForWorkflow(
        int $company_id,
        string $unique_object_name = UniqueObjectTypes::WORKFLOW,
        string $statusdef = StatusDefRepository::ACTIVE
    ): ?int
    {
        $this->logger->info('$this->uniqueObjectNames' . print_r($this->uniqueObjectNames, true), [__LINE__]);

        if (!in_array($unique_object_name, $this->uniqueObjectNames)) {
            return null;
        }

        $this->logger->info('----------------', [__LINE__]);


        $company = $this->companyRepository->getCompanyDetailById($company_id);
        $commentEntry = ucfirst($unique_object_name) . ' for account ' . $company['name'] . ' / ' . $company['account_no'];
        $this->logger->info('----------------', [__LINE__]);

        $unique_object_type_id = $this->getUniqueObjectTypeIdByName($unique_object_name);
        $statusdef_id = $this->getStatusdefIdByName($statusdef);
        $this->logger->info('----------------', [__LINE__]);

        $insertIntoObjectRegister = "INSERT INTO objectregister  
                (unique_object_type_id, statusdef_id, version,object_unique_id, comment, created_by, updated_by) 
                VALUES 
                (:unique_object_type_id, :statusdef_id, 1,'', :commentEntry, :userId, :userId)";

        try {
            $this->logger->info('----------------', [__LINE__]);
            $stmt = $this->conn->prepare($insertIntoObjectRegister);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, ParameterType::INTEGER);
            $stmt->bindValue(':statusdef_id', $statusdef_id, ParameterType::INTEGER);
            $stmt->bindValue(':commentEntry', $commentEntry);
            $stmt->bindValue(':userId', $this->userId, ParameterType::INTEGER);
            $stmt->execute();
            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert ' . $unique_object_name . ' entry in objectregister for company_id=' . $company_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
            $this->logger->info('$insertedId', [$insertedId, __METHOD__, __LINE__]);

        } catch (Exception $e) {
            $this->logger->error('could not insert ' . $unique_object_name . ' Entry in objectregister for company_id=' . $company_id,
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $insertedId;
    }


    public function createObjectRegisterEntryForProcesshook(
        int $company_id,
        string $unique_object_name = UniqueObjectTypes::PROCESSHOOK,
        string $statusdef = StatusDefRepository::ACTIVE
    ): ?int
    {
        if (!in_array($unique_object_name, $this->uniqueObjectNames)) {
            return null;
        }


        $company = $this->companyRepository->getCompanyDetailById($company_id);
        $commentEntry = ucfirst($unique_object_name) . ' for account ' . $company['name'] . ' / ' . $company['account_no'];

        $unique_object_type_id = $this->getUniqueObjectTypeIdByName($unique_object_name);
        $statusdef_id = $this->getStatusdefIdByName($statusdef);

        $insertIntoObjectRegister = "INSERT INTO objectregister  
                (unique_object_type_id, statusdef_id, version,object_unique_id, comment, created_by, updated_by) 
                VALUES 
                (:unique_object_type_id, :statusdef_id, 1,'', :commentEntry, :userId, :userId)";

        try {
            $stmt = $this->conn->prepare($insertIntoObjectRegister);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, ParameterType::INTEGER);
            $stmt->bindValue(':statusdef_id', $statusdef_id, ParameterType::INTEGER);
            $stmt->bindValue(':commentEntry', $commentEntry);
            $stmt->bindValue(':userId', $this->userId, ParameterType::INTEGER);
            $stmt->execute();
            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert ' . $unique_object_name . ' entry in objectregister for company_id=' . $company_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error('could not insert ' . $unique_object_name . ' Entry in objectregister for company_id=' . $company_id,
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $insertedId;
    }


    /**
     * @param string $name
     * @return int|null
     * @author aki
     */
    public function getStatusdefIdByName(string $name): ?int
    {
        $name = strtolower($name);
        $selectStatusDef = "select id from statusdef where value = :name";
        $stmt = $this->conn->prepare($selectStatusDef);
        $stmt->bindParam(':name', $name, ParameterType::STRING);

        try {
            $stmt->execute();
            $record = $stmt->fetch();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }

        return $record['id'];

    }

    /**
     *
     * @param int $company_id
     * @param string $unique_object_name ('customer' | 'customfields' | 'page' | 'project' | 'smarttags' | 'standalone survey' | 'survey' | 'webhook')
     * @param string $statusdefName ('active' | 'archived' | 'draft' )
     * @return array|null
     * @author aki
     */
    public function getCampaignsWithStatus(int $company_id, string $unique_object_name, string $statusdefName): ?array
    {
        $campaigns = [];
        switch ($statusdefName) {
            case StatusDefRepository::ACTIVE:
                $selectCampaignsByStatus = "select c.*,u.email,  o.object_unique_id, o.version from objectregister o
                                            join campaign c on o.id = c.objectregister_id
                                            join users u on c.created_by = u.id
                                            JOIN statusdef s on o.statusdef_id = s.id and s.value = 'active'
                                            JOIN unique_object_type uot on o.unique_object_type_id = uot.id and uot.unique_object_name = :unique_object_name
                                            where c.company_id= :company_id order by updated desc";
                break;
            case StatusDefRepository::ARCHIVED:
                $selectCampaignsByStatus = "select c.*,u.email,  o.object_unique_id, o.version from objectregister o
                                            join campaign c on o.id = c.objectregister_id
                                            join users u on c.created_by = u.id
                                            JOIN statusdef s on o.statusdef_id = s.id and s.value = 'archived'
                                            JOIN unique_object_type uot on o.unique_object_type_id = uot.id and uot.unique_object_name = :unique_object_name
                                            where c.company_id= :company_id order by updated desc";
                break;
            case StatusDefRepository::DRAFT:
                $selectCampaignsByStatus = "select c.*,u.email,  o.object_unique_id, o.version from objectregister o
                                            join campaign c on o.id = c.objectregister_id
                                            join users u on c.created_by = u.id
                                            JOIN statusdef s on o.statusdef_id = s.id and s.value = 'draft'
                                            JOIN unique_object_type uot on o.unique_object_type_id = uot.id and uot.unique_object_name = :unique_object_name
                                            where c.company_id= :company_id order by updated desc";
                break;
        }
        $stmt = $this->conn->prepare($selectCampaignsByStatus);
        $stmt->bindValue(':unique_object_name', $unique_object_name, ParameterType::STRING);
        $stmt->bindValue(':company_id', $company_id, ParameterType::STRING);
        try {
            $stmt->execute();
            $res = $stmt->fetchAll();
            $campaigns = !empty($res) ? $res : [];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return $campaigns;

    }

    /**
     * @param $id
     * @return bool
     * @author Aki
     */
    public function delete(int $id): bool
    {
        if ($id <= 0) {
            throw new RuntimeException('Invalid Parameters provided');
        }
        $deleteSql = "delete from objectregister where id = :id";
        try {
            $stmt = $this->conn->prepare($deleteSql);
            $stmt->bindParam(':id', $id, ParameterType::INTEGER);
            $result = $stmt->execute();
            return (is_bool($result) && $result) ? $result : false;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
        return $result;
    }

    public function createObjectRegisterEntryForRevisedCampaign(
        string $unique_object_name,
        string $statusdef,
        int $version,
        string $objectUniqueId,
        int $companyId
    ): ?int
    {
        if (!in_array($unique_object_name, $this->uniqueObjectNames)) {
            return null;
        }
        $company = $this->companyRepository->getCompanyDetailById($companyId);
        $commentEntry = ucfirst($unique_object_name) . ' for account ' . $company['name'] . ' / ' . $company['account_no'];
        $statusdef_id = $this->getStatusdefIdByName($statusdef);
        $unique_object_type_id = $this->getUniqueObjectTypeIdByName($unique_object_name);
        $insertIntoObjectRegister = "INSERT INTO objectregister  
                (unique_object_type_id, statusdef_id, version,object_unique_id, comment, created_by, updated_by) 
                VALUES 
                (:unique_object_type_id, :statusdef_id, :version,:objectUniqueId, :commentEntry, :userId, :userId)";

        try {
            $stmt = $this->conn->prepare($insertIntoObjectRegister);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, ParameterType::INTEGER);
            $stmt->bindValue(':statusdef_id', $statusdef_id, ParameterType::INTEGER);
            $stmt->bindValue(':commentEntry', $commentEntry, ParameterType::STRING);
            $stmt->bindValue(':userId', $this->userId, ParameterType::INTEGER);
            $stmt->bindValue(':version', $version, ParameterType::INTEGER);
            $stmt->bindValue(':objectUniqueId', $objectUniqueId, ParameterType::STRING);
            $stmt->execute();
            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert ' . $unique_object_name . ' entry in objectregister for company_id=' . $companyId,
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error('could not insert ' . $unique_object_name . ' Entry in objectregister for company_id=' . $companyId,
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        if ($unique_object_name === UniqueObjectTypes::CUSTOMFIELDS) {
            $this->createObjectRegisterMetaEntryForCustomfieldOR($insertedId, $companyId);
        }
        return $insertedId;
    }

    /**
     * @param string $object_unique_id
     * @return int|null
     * @author AKi
     */
    public function getLatestVersion(string $object_unique_id): ?int
    {
        // basic check
        if (empty($object_unique_id)) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return null;
        }
        $sql = "select max(version) from objectregister where object_unique_id = :uniqueObjectId";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':uniqueObjectId', $object_unique_id, ParameterType::STRING);

        try {
            $stmt->execute();
            $record = $stmt->fetch();
            $version = $stmt->rowCount();
            if ($version == 0) {
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return (int)$record['max(version)'];

    }

    public function archiveCampaignWithObjectUniqueID(string $ObjectUniqueId,string $oldStatus): bool
    {
        // basic check
        if (empty($ObjectUniqueId)) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return false;
        }
        $statusDefId = $this->statusDefRepository->getId($oldStatus);

        $updateObjectRegister = "UPDATE objectregister SET statusdef_id= 2 WHERE statusdef_id = :oldStatusId and object_unique_id = :objectUniqueId";
        try {
            $stmt = $this->conn->prepare($updateObjectRegister);
            $stmt->bindValue(':objectUniqueId', $ObjectUniqueId, ParameterType::STRING);
            $stmt->bindValue(':oldStatusId', $statusDefId, ParameterType::INTEGER);
            $stmt->execute();
            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not update entry in objectregister', [__METHOD__, __LINE__]);
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error('could not update entry in objectregister', [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }

        return true;
    }

    /**
     * @return int|null
     * @author Pradeep
     */
    public function getObjectRegisterIdForSuperAdmin(): ?int
    {
        $objectRegisterId = null;
        try {
            $uniqueObjectName = UniqueObjectTypes::SUPERADMIN;
            $sql = 'SELECT o.*
                    FROM objectregister o
                        JOIN unique_object_type uot on uot.id = o.unique_object_type_id
                    WHERE uot.unique_object_name = :unique_object_name';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':unique_object_name', $uniqueObjectName);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result)) {
                $objectRegisterId = $result[0]['id'];
            }
            return $objectRegisterId;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $objectRegisterId;
        }
    }

    /**
     * @param int $company_id
     * @param string $processhook
     * @return int|null
     * @auther aki
     */
    public function createInitialObjectRegisterEntryProcesshook(string $processhook):?int
    {
        $commentEntry = 'Processhook Object Register Entry';
        $unique_object_type_id = $this->getUniqueObjectTypeIdByName($processhook);
        $sql = "INSERT INTO objectregister  
                (unique_object_type_id, object_unique_id ,statusdef_id, version, comment, created_by, updated_by) 
                VALUES 
                (:unique_object_type_id, '' ,14, 1, :commentEntry, :created_by, :updated_by)";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, ParameterType::INTEGER);
            $stmt->bindValue(':commentEntry', $commentEntry, ParameterType::STRING);
            $stmt->bindValue(':created_by', $this->userId, ParameterType::INTEGER);
            $stmt->bindValue(':updated_by', $this->userId, ParameterType::INTEGER);
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert ' . $processhook . ' entry in objectregister for company_id=',
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error('could not insert ' . $processhook . ' Entry in objectregister for company_id=',
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        $this->logger->debug('$insertedId', [$insertedId, __METHOD__, __LINE__]);
        return $insertedId;

    }

    /**
     * @param $pageWebhookVersion
     * @return int|null
     * @auther aki
     */
    public function createInitialObjectRegisterEntryWorkflow($pageWebhookVersion):?int
    {
        $commentEntry = 'Workflow Object Register Entry';
        $unique_object_type_id = $this->getUniqueObjectTypeIdByName(UniqueObjectTypes::WORKFLOW);
        $sql = "INSERT INTO objectregister  
                (unique_object_type_id, object_unique_id ,statusdef_id, version, comment, created_by, updated_by) 
                VALUES 
                (:unique_object_type_id, '' ,1, :pageWebhookVersion, :commentEntry, :created_by, :updated_by)";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, ParameterType::INTEGER);
            $stmt->bindValue(':commentEntry', $commentEntry, ParameterType::STRING);
            $stmt->bindValue(':created_by', $this->userId, ParameterType::INTEGER);
            $stmt->bindValue(':updated_by', $this->userId, ParameterType::INTEGER);
            $stmt->bindValue(':pageWebhookVersion', $pageWebhookVersion, ParameterType::INTEGER);
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert workflow entry in objectregister for company_id=',
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error('could not insert workflow Entry in objectregister for company_id=',
                [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        $this->logger->debug('$insertedId', [$insertedId, __METHOD__, __LINE__]);
        return $insertedId;

    }


    /**
     * @param $pageWebhookVersion
     * @return int|null
     * @auther aki
     */
    public function createObjectRegisterEntryAdvertorial(string $advertorialName, string $advertorialDescription, int $company_id, string $seedPoolGroupName):?int
    {
        $return = null;
        $commentEntry = substr("Ad: $advertorialName, CmpnyId: $company_id, SdPlGrp: $seedPoolGroupName, Desc: $advertorialDescription", 0 , 254);
        $unique_object_type_id = $this->getUniqueObjectTypeIdByName(UniqueObjectTypes::ADVERTORIAL);
        $sql = "INSERT INTO objectregister  
                (unique_object_type_id, object_unique_id ,statusdef_id, version, comment, created_by, updated_by) 
                VALUES 
                (:unique_object_type_id, '' ,1, :version, :commentEntry, :created_by, :updated_by)";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, \PDO::PARAM_INT);
            $stmt->bindValue(':commentEntry', $commentEntry, \PDO::PARAM_STR);
            $stmt->bindValue(':created_by', $this->userId, \PDO::PARAM_INT);
            $stmt->bindValue(':updated_by', $this->userId, \PDO::PARAM_INT);
            $version = 0;
            $stmt->bindValue(':version', $version, \PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert '.UniqueObjectTypes::ADVERTORIAL. "  entry in objectregister for company_id=$company_id",[__METHOD__, __LINE__]);
             } else {
                $return = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            $this->logger->error('could not insert '.UniqueObjectTypes::ADVERTORIAL. "  entry in objectregister for company_id=$company_id",[$e->getMessage(), __METHOD__, __LINE__]);
        }
        $this->logger->info("lastInsertedId |$return|", [__METHOD__, __LINE__]);
        return $return;
    }


    /**
     * @param string $internal_name
     * @param string $ip_address
     * @param string $default_domain
     * @return int|null new (int) objectregister.id or null if something went wrong
     */
    public function createObjectRegisterEntryServer(string $internal_name, string $ip_address, string $default_domain):?int
    {
        $return = null;
        $commentEntry = substr("Server $internal_name, IP $ip_address, default Domain: $default_domain", 0 , 254);
        $unique_object_type_id = $this->getUniqueObjectTypeIdByName(UniqueObjectTypes::SERVER);
        $sql = "INSERT INTO objectregister  
                (unique_object_type_id, object_unique_id ,statusdef_id, version, comment, created_by, updated_by) 
                VALUES 
                (:unique_object_type_id, '' ,1, :version, :commentEntry, :created_by, :updated_by)";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':unique_object_type_id', $unique_object_type_id, \PDO::PARAM_INT);
            $stmt->bindValue(':commentEntry', $commentEntry, \PDO::PARAM_STR);
            $stmt->bindValue(':created_by', $this->userId, \PDO::PARAM_INT);
            $stmt->bindValue(':updated_by', $this->userId, \PDO::PARAM_INT);
            $version = 0;
            $stmt->bindValue(':version', $version, \PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                $this->logger->error("could not insert entry in objectregister for unique_object_type_id=$unique_object_type_id",[__METHOD__, __LINE__]);
             } else {
                $return = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            $this->logger->error("could not insert entry in objectregister for unique_object_type_id=$unique_object_type_id",[$e->getMessage(), __METHOD__, __LINE__]);
        }
        $this->logger->info("lastInsertedId |$return|", [__METHOD__, __LINE__]);
        return $return;
    }



	/**
	 * get project's campaign id by company_id
	 * @param int $company_id
	 * @return int|null
	 * @internal <b style="color:red">works only if there is only one project per company</b>
	 */
	public function getProjectCampaignId(int $company_id):?int
	{
		$unique_object_name = UniqueObjectTypes::PROJECT;

		$selectObjectregisterId = "SELECT cp.id 
            FROM company c
            JOIN campaign cp ON cp.company_id = c.id 
            JOIN objectregister o ON cp.objectregister_id = o.id
            JOIN unique_object_type uot ON (o.unique_object_type_id = uot.id AND uot.unique_object_name = :unique_object_name) 
            WHERE c.id = :company_id";

		$stmt = $this->conn->prepare($selectObjectregisterId);
		$stmt->bindParam(':company_id', $company_id, ParameterType::INTEGER);
		$stmt->bindParam(':unique_object_name', $unique_object_name, ParameterType::STRING);

		try {
			$stmt->execute();
			$objectregisterId = $stmt->fetchColumn(); // TODO: if there are more than one projects possible for one company => fetchAll and return array

			if ($stmt->rowCount() == 0) {
				$this->logger->warning("no objectregister_id found for {$unique_object_name} company_id=" . $company_id, [__METHOD__, __LINE__]);
				return null;
			}
		} catch (Exception $e) {
			$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
			return null;
		}
		return $objectregisterId;

	}


    /**
     * update a comment in objectregister for a given objectregister.id
     * @param int    $objectRegisterId
     * @param string $comment
     * @return int 0, if update failed, else updated objectregister.id
     */
    public function updateComment(int $objectRegisterId, string $comment=''):int
    {
        $return = 0;
        $update = 'UPDATE objectregister SET comment =:new_comment WHERE id = :objectregister_id';
        try {
            $stmt = $this->conn->prepare($update);
            $stmt->bindParam(':objectregister_id', $objectRegisterId, \PDO::PARAM_INT);
            $stmt->bindParam(':new_comment', $comment, \PDO::PARAM_STR);
            $stmt->execute();
            $return = ($stmt->rowCount() > 0) ? $objectRegisterId : 0;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), ['objectRegisterId'=> $objectRegisterId, __METHOD__, __LINE__]);
        }
        return $return;
    }

}
