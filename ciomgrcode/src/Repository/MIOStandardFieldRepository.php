<?php

namespace App\Repository;

use App\Types\ContactPermissionTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * Class MIOStandardFieldRepository
 * @package App\Repository
 * @author Pradeep
 * @internal Repository for 'miostandardfield' table
 */
class MIOStandardFieldRepository
{

    public const STANDARD_FIELDS = "Standard";
    public const TECHNICAL_FIELDS = "Technical";
    public const ECOMMERCE_FIELDS = "eCommerce";

    /** Technical Fields **/
    public const SOI_TIMESTAMP = "optin_timestamp";
    public const DOI_TIMESTAMP = "doi_timestamp";

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $conn;
    /**
     * @var ContactPermissionTypes
     * @author Pradeep
     */
    private ContactPermissionTypes $contactPermissionTypes;


    public function __construct(
        LoggerInterface        $logger,
        Connection             $conn,
        ContactPermissionTypes $contactPermissionTypes
    )
    {
        $this->logger = $logger;
        $this->conn = $conn;
        $this->contactPermissionTypes = $contactPermissionTypes;
    }

    /**
     * Fetches all the supported standard fields (from table 'miostandardfield') as an array.
     * @return array|null
     * @author Pradeep
     * @internal Field type is 'Standard'
     */
    public function getStandardFields(): ?array
    {
        return $this->getAllFieldsByType(self::STANDARD_FIELDS);
    }

    /**
     * Fetches all the MIO fields related to the fieldtype.
     * @param string $fieldType
     * @return array
     * @author Pradeep
     * @internal Field type should be one of ['Standard','Technical', 'eCommerce']
     */
    public function getAllFieldsByType(string $fieldType): ?array
    {
        try {
            if (!$this->isValidFieldType($fieldType)) {
                throw new RuntimeException("Invalid field type: " . $fieldType);
            }

            $sql = '
                select m.*, d.name as dtype_label, d.phptype as dtype_name, d.base_regex as regex ,m.hidden_field
                from miostandardfield m
                join datatypes d on m.datatypes_id = d.id
                where fieldtype=? order by m.fieldname';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $fieldType);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (empty($result) || !isset($result[0])) {
                throw new RuntimeException("Failed to get fields of type " . $fieldType);
            }
            return $result;
        } catch (Exception|RuntimeException|\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return null;
        }
    }

    /**
     * Check whether fieldtype is valid or not
     * @param $fieldType
     * @return bool
     * @author Pradeep
     * @internal Field type should be one of ['Standard','Technical', 'eCommerce']
     */
    public function isValidFieldType($fieldType): bool
    {
        if (in_array($fieldType, $this->getAllFieldTypes(), true)) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     * @author Pradeep
     * @internal returns all the supported field types for MIO
     * i,e ['Standard','Technical', 'eCommerce'] as present in DB.
     */
    public function getAllFieldTypes(): array
    {
        return [
            self::STANDARD_FIELDS,
            self::TECHNICAL_FIELDS,
            self::ECOMMERCE_FIELDS,
        ];
    }

    /**
     * Fetches all the fields present in miostandardfields table.
     * @return array|null
     * @author Pradeep
     */
    public function getAll(): ?array
    {
        try {

            $sql = '
                select m.*, d.name as dtype_label, d.phptype as dtype_name
                from miostandardfield m
                join datatypes d on m.datatypes_id = d.id
                order by m.fieldname';
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (empty($result) || !isset($result[0])) {
                throw new RuntimeException("Failed to get fields  ");
            }
            return $result;
        } catch (Exception|RuntimeException|\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return null;
        }
    }

    /**
     * Fetches all the supported technical fields (from table 'miostandardfield') as an array.
     * @return array|null
     * @author Pradeep
     * @internal Field type is 'Technical'
     */
    public function getTechnicalFields(): ?array
    {
        return $this->getAllFieldsByType(self::TECHNICAL_FIELDS);
    }

    /**
     * Fetches all the supported eCommerce fields (from table 'miostandardfield') as an array.
     * @return array|null
     * @author Pradeep
     * @internal Field type is 'eCommerce'
     */
    public function getECommerceFields(): ?array
    {
        return $this->getAllFieldsByType(self::ECOMMERCE_FIELDS);
    }

    /**
     * Helper function for LeadController.
     * Filters the array of lead details based on category
     * Updates the database field key to placeholder mentioned in miostandardfield.
     * eg => first_name (column in leads table) => FirstName (placeholder in miostandardfield)
     * @param array $contactDetails
     * @param string $fieldCategoryType
     * @return array
     * @author Pradeep
     */
    public function filterContactDetails(
        array  $contactDetails,
        string $fieldCategoryType = self::STANDARD_FIELDS
    ): array
    {
        $details = [];
        if (empty($contactDetails)) {
            return [];
        }
        foreach ($contactDetails as $cntField => $cntValue) { // TODO: prevent that getFieldType is called with an $cntField that's not member of standard-, custom-, whatever-category ==> leads to multiple  log entries ==>   [2022-04-08T08:24:33.400489+00:00] app.ERROR: no category found for the given fieldname = id ["App\\Repository\\MIOStandardFieldRepository::getFieldType",237] []
            $fieldType = $this->getFieldType($cntField);
            if ($fieldType !== $fieldCategoryType) {
                continue;
            }
            $displayField = $this->getPlaceHolderName($cntField);
            if ($cntField === 'permission') {
                $details[$displayField] = $this->contactPermissionTypes->getPermission((int)$cntValue);
            } else {
                $details[$displayField] = $cntValue;
            }
        }
        return $this->addMissingFieldNames($details, $fieldCategoryType);
    }

    /**
     * Fetches the FieldType for the given fieldname from table 'miostandardfield'
     * Field type should be one of ['Standard','Technical', 'eCommerce']
     * @param string $fieldName
     * @return string|null
     * @author Pradeep
     */
    public function getFieldType(string $fieldName): ?string
    {
        try {
            if (empty($fieldName)) {
                throw new RuntimeException("empty fieldName provided to fetch fieldtype ");
            }
            $sql = 'select fieldtype from miostandardfield where fieldname = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $fieldName);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (empty($result) || !isset($result[0]['fieldtype'])) {
                throw new RuntimeException("no category found for the given fieldname = " . $fieldName);
            }
            return $result[0]['fieldtype'];
        } catch (RuntimeException|\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * For a given fieldName,fetches the PlaceHolder column from miostandardfield table
     * @param string $fieldName
     * @return string|null
     * @author Pradeep
     */
    public function getPlaceHolderName(string $fieldName): ?string
    {
        try {
            if (empty($fieldName)) {
                throw new RuntimeException("empty fieldname provided to fetch placeholder ");
            }
            $sql = 'select html_placeholder from miostandardfield where fieldname = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $fieldName);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (empty($result) || !isset($result[0]['html_placeholder'])) {
                throw new RuntimeException("no placeholder found for the given fieldname = " . $fieldName);
            }

            return $result[0]['html_placeholder'];
        } catch (RuntimeException|\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

    }

    /**
     * Helper function for 'filterContactDetails'
     * Adds missing standard or eCommerce or technical fields to empty value.
     * Function only used for service => 'leads.detailLeadViewById'
     * @param array $contactDetails
     * @param $fieldCategoryType
     * @return array
     * @author Pradeep
     */
    private function addMissingFieldNames(array $contactDetails, string $fieldCategoryType)
    {
        // field type has to be one of ['Standard','Technical', 'eCommerce']
        if (!$this->isValidFieldType($fieldCategoryType)) {
            return $contactDetails;
        }
        $fields = $this->getAllFieldsByType($fieldCategoryType);
        foreach ($fields as $field) {
            // ignore if the field name is already defined in contactDetails.
            if (!isset($field['html_placeholder']) || isset($contactDetails[$field['html_placeholder']])) {
                continue;
            }
            // add missing field name or placeholder to empty string.
            // Just to display for contact details view.
            $contactDetails[$field['html_placeholder']] = '';
        }
        return $contactDetails;
    }

    /**
     * Helper function for Assigning the HTML Placeholder for all the lead fields
     * @param array $leads
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    public function assignPlaceHoldersForLeadTwigView(array $leads): array
    {
        if (empty($leads)) {
            return [];
        }
        $htmlPlaceHolders = $this->getAllPlaceHolderForTwig();
        foreach ($leads as &$lead) {
            foreach ($htmlPlaceHolders as $fieldName => $htmlPlaceHolder) {
                /*                $this->logger->info('Lead' .
                                                    json_encode(['lead' => $lead], JSON_THROW_ON_ERROR));*/
                if (!array_key_exists($fieldName, $lead)) {
                    $this->logger->info('missing Key' .
                        json_encode(['fieldName' => $fieldName, 'lead' => $lead], JSON_THROW_ON_ERROR));
                    continue;
                }
                $lead[$htmlPlaceHolder] = $lead[$fieldName];
                unset($lead[$fieldName]);
            }
        }
        unset($lead);
        return $leads;
    }


    /**
     * Fetches the field name and placeholder as an array.
     * placeholder is used to display as label in html.
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    private function getAllPlaceHolderForTwig()
    {
        $placeHolderAsArr = [];
        $sql = "select fieldname, html_placeholder from miostandardfield";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $fields = $stmt->fetchAll();
        if (empty($fields)) {
            return [];
        }
        //$this->logger->info('getAllPlaceHolderForTwig '.json_encode($fields));
        foreach ($fields as $field) {
            $placeHolderAsArr[$field['fieldname']] = $field['html_placeholder'];
        }
        $this->logger->info('getAllPlaceHolderForTwig ' . json_encode($placeHolderAsArr));
        return $placeHolderAsArr;
    }


}
