<?php


namespace App\Repository;
use Exception;
use JsonException;
use RuntimeException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\BastelService;
use App\Services\AuthenticationService;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;

class CustomfieldsRepository
{
	private $conn;
	private $logger;
	private $userId;
	private $authenticationUtils;
	private $authenticationService;

	public function __construct(
		Connection $connection,
		AuthenticationUtils $authenticationUtils,
		AuthenticationService $authenticationService,
		LoggerInterface $logger
	) {
		$this->authenticationService = $authenticationService;
		$this->authenticationUtils = $authenticationUtils;

		$this->conn = $connection;
		$this->logger = $logger;

		// set userId
		$this->setUserId();
	}

	private function setUserId()
	{
        // set userId
        $userEmail = $this->authenticationUtils->getLastUsername();
        $user = $this->authenticationService->getUserParametersFromCache($userEmail);
        if (isset($user[ 'user_id' ])) {
            $this->userId = $user[ 'user_id' ];
        } else {
            $this->userId = 1;
        }
    }

    /**
     * get all customfields of a project
     * @param int $company_id
     * @param string|null $uniqueObjectName
     * @return array|null if no rows where found [], else [customfields.id, customfields.object_register_id objectregister_id, customfields.campaign_id, customfields.ref_customfields_id, customfields.fieldname, customfields.datatypes_id, datatypes.name datatypes_name, customfields.mandatory, customfields.hide_in_ui_selection, customfields.regexp_validation]
     */
    public function getProjectCustomfields(int $company_id, string $uniqueObjectName = null): ?array
    {
        if (!empty($uniqueObjectName)) {
            $sql = "SELECT cf.id, cf.objectregister_id, cf.fieldname, cf.datatypes_id, dt.name datatypes_name, cf.mandatory, cf.mandatory required, cf.hide_in_ui_selection ,dt.name datatype , dt.base_regex regex ,dt.phptype php_type
                    FROM customfields cf
                    JOIN datatypes dt ON cf.datatypes_id = dt.id
                    JOIN campaign cn ON cf.campaign_id = cn.id
                    JOIN objectregister o ON cf.objectregister_id = o.id
                    JOIN unique_object_type uot ON uot.id = o.unique_object_type_id
                    WHERE cn.company_id = :company_id and uot.unique_object_name = :unique_object_name 
                    ORDER BY cf.fieldname";

        } else {
            $sql = "SELECT cf.id, cf.objectregister_id, cf.fieldname, cf.datatypes_id, dt.name datatypes_name, cf.mandatory, cf.hide_in_ui_selection, cf.regexp_validation 
                    FROM customfields cf
                    JOIN datatypes dt ON cf.datatypes_id = dt.id
                    JOIN campaign cn ON cf.campaign_id = cn.id
                    JOIN objectregister o ON cf.objectregister_id = o.id
                    WHERE cn.company_id = :company_id and cf.fieldname != ''
                    ORDER BY cf.fieldname";
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':company_id', $company_id, ParameterType::INTEGER);
        if (!empty($uniqueObjectName)) {
            $stmt->bindParam(':unique_object_name', $uniqueObjectName);
        }

        try {
            $stmt->execute();
            $customfields = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in customfields for (project) company_id=' . $company_id,
                    [__METHOD__, __LINE__]);
                return [];
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $customfields;
    }

    public function getCustomFieldsNames(int $companyId)
    {
        $fields = $this->getProjectCustomfields($companyId);
        $keys = array_column($fields, 'fieldname');
        $trimmedArray = array_map('trim', $keys);
        return array_unique($trimmedArray);
    }

    /**
     * insert or update project wizard customfields
     * @param array $customfieldlist
     * @return array|int[] ['inserted' => int, 'updated'=> int]
     * @author jsr
     */
    public function insertUpdateProjectCustomfields(array $customfieldlist): array
    {
        $useMioInterim = (bool)$_ENV[ 'USE_MIOINTERIM' ];

		if ($useMioInterim) {
			$updateCustomfields = "UPDATE customfields 
            SET fieldname=:fieldname, datatypes_id = :datatypes_id, mandatory =:mandatory, hide_in_ui_selection = :hide_in_ui_selection, updated_by=:updated_by   
            WHERE id = :id
            AND object_register_id = :objectregister_id
            AND campaign_id = :campaign_id";

			$insertCustomfields = "INSERT INTO customfields 
            ( object_register_id, campaign_id, datatypes_id, fieldname, hide_in_ui_selection, mandatory, created_by, updated_by) 
            VALUES
            (:objectregister_id, :campaign_id, :datatypes_id, :fieldname, :hide_in_ui_selection, :mandatory, :created_by, :updated_by )";
		} else {
			$updateCustomfields = "UPDATE customfields 
            SET fieldname=:fieldname, datatypes_id = :datatypes_id, mandatory =:mandatory, hide_in_ui_selection = :hide_in_ui_selection, updated_by=:updated_by   
            WHERE id = :id
            AND objectregister_id = :objectregister_id
            AND campaign_id = :campaign_id";

			$insertCustomfields = "INSERT INTO customfields 
            ( objectregister_id, campaign_id, datatypes_id, fieldname, hide_in_ui_selection, mandatory, created_by, updated_by) 
            VALUES
            (:objectregister_id, :campaign_id, :datatypes_id, :fieldname, :hide_in_ui_selection, :mandatory, :created_by, :updated_by )";
		}

		$inserted = 0;
		$updated = 0;

		try {
			foreach ($customfieldlist as $cf) {
				if ($cf['instruction'] === 'update') { // fire and forget
					$stmt = $this->conn->prepare($updateCustomfields);
					$stmt->bindParam(':id', $cf['id'], ParameterType::INTEGER);
					$stmt->bindParam(':objectregister_id', $cf['objectregister_id'], ParameterType::INTEGER);
					$stmt->bindParam(':campaign_id', $cf['campaign_id'], ParameterType::INTEGER);
					$stmt->bindParam(':datatypes_id', $cf['datatype'], ParameterType::INTEGER);
					$stmt->bindParam(':fieldname', $cf['fieldname'], ParameterType::STRING);
					$mandatory = (int)$cf['mandatory'];
					$hide_in_ui_selection = (int)$cf['hideinuiselection']; // no underscores here
					$stmt->bindParam(':mandatory', $mandatory, ParameterType::INTEGER);
					$stmt->bindParam(':hide_in_ui_selection', $hide_in_ui_selection, ParameterType::INTEGER);
					$stmt->bindParam(':updated_by', $this->userId, ParameterType::INTEGER);
					$stmt->execute();
					$updated += $stmt->rowCount();
				}

				if ($cf['instruction'] === 'insert') { // fire and forget
					$stmt = $this->conn->prepare($insertCustomfields);
					$stmt->bindParam(':objectregister_id', $cf['objectregister_id'], ParameterType::INTEGER);
					$stmt->bindParam(':campaign_id', $cf['campaign_id'], ParameterType::INTEGER);
					$stmt->bindParam(':datatypes_id', $cf['datatype'], ParameterType::INTEGER);
					$stmt->bindParam(':fieldname', $cf['fieldname'], ParameterType::STRING);
					$mandatory = (int)$cf['mandatory'];
					$hide_in_ui_selection = (int)$cf['hideinuiselection']; // no underscores here
					$stmt->bindParam(':mandatory', $mandatory, ParameterType::INTEGER);
					$stmt->bindParam(':hide_in_ui_selection', $hide_in_ui_selection, ParameterType::INTEGER);
					$stmt->bindParam(':created_by', $this->userId, ParameterType::INTEGER);
					$stmt->bindParam(':updated_by', $this->userId, ParameterType::INTEGER);
					$stmt->execute();
					$inserted += $stmt->rowCount();
				}
			}
		} catch (\Exception $e) {
			$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
			return ['inserted' => $inserted, 'updated' => $updated];
		}

		return ['inserted' => $inserted, 'updated' => $updated];
	}


	/**
	 * delete project wizard customfields
	 * @param array $customfielddelete e.g. [0]['customfield_id' => 1, 'campaign_id' => 1649, 'objectregister_id'=> 54]
	 * @return int[] ['deleted' => int];
	 */
	public function deleteProjectCustomfields(array $customfielddelete): array
	{
		$useMioInterim = (bool)$_ENV['USE_MIOINTERIM'];
		$deleted = 0;
		if ($useMioInterim) {
			$deleteCustomfields = "DELETE FROM customfields 
            WHERE id = :id
            AND object_register_id = :objectregister_id
            AND campaign_id = :campaign_id
            AND ref_customfields_id is null"; // check / may be replace by subselect on object_register_meta
		} else {
			$deleteCustomfields = "DELETE FROM customfields 
            WHERE id = :id
            AND objectregister_id = :objectregister_id
            AND campaign_id = :campaign_id
            AND ref_customfields_id is null"; // check / may be replace by subselect on object_register_meta
        }

        try {
            foreach ($customfielddelete as $cf) {
                $stmt = $this->conn->prepare($deleteCustomfields);
                $stmt->bindParam(':id', $cf[ 'customfield_id' ], ParameterType::INTEGER);
                $stmt->bindParam(':objectregister_id', $cf[ 'objectregister_id' ], ParameterType::INTEGER);
                $stmt->bindParam(':campaign_id', $cf[ 'campaign_id' ], ParameterType::INTEGER);
                $stmt->execute();
                $deleted += $stmt->rowCount();
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return ['deleted' => $deleted];
        }
        return ['deleted' => $deleted];
    }


    /**
     * @param int $customFieldId
     * @return string|null
     * @throws JsonException
     * @author Pradeep
     */
    public function getCustomFieldNameById(int $customFieldId): ?string
    {
        $customFieldName = '';
        $this->logger->info(json_encode(['customFieldId' => $customFieldId], JSON_THROW_ON_ERROR));
        try {
            if ($customFieldId <= 0) {
                throw new RuntimeException('Invalid CustomFieldId provided');
            }
            $sql = 'SELECT fieldname
                    FROM customfields
                    WHERE id=:customfield_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':customfield_id', $customFieldId, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $this->logger->info(json_encode(['Sql_result' => $result], JSON_THROW_ON_ERROR));
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0) {
                $customFieldName = $result[ 0 ][ 'fieldname' ];
            }
            return $customFieldName;
        } catch (Exception | RuntimeException | JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $id
     * @return string|null
     * @author Pradeep
     * @internal Fetches Datatype for the customfield Id.
     */
    public function getDataType(string $id): ?string
    {
        $dataType = '';
        try {
            if ($id <= 0) {
                throw new RuntimeException('Invalid CustomField Id provided');
            }
            $sql = 'SELECT d.name as dtype
                    FROM customfields cf
                             JOIN datatypes d on cf.datatypes_id = d.id
                    WHERE cf.id = :id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result) && !empty($result[ 0 ][ 'dtype' ])) {
                $dataType = $result[ 0 ][ 'dtype' ];
            }
            $this->logger->info('datatype ' . $dataType);
            return $dataType;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $dataType;
        }
    }

}
