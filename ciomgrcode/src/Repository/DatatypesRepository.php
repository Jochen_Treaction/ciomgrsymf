<?php


namespace App\Repository;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AuthenticationService;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;

class
DatatypesRepository
{

    public const MIO_DATE = 'Date';
    public const MIO_DATETIME = 'DateTime';
    public const MIO_DECIMAL = 'Decimal';
    public const MIO_EMAIL = 'Email';
    public const MIO_INTEGER = 'Integer';
    public const MIO_LIST = 'List';
    public const MIO_PHONE = 'Phone';
    public const MIO_TEXT = 'Text';
    public const MIO_URL = 'URL';
    public const MIO_BOOLEAN = 'Boolean';
    public const PHP_STRING = 'string';
    public const PHP_DATE = 'date';
    public const PHP_INTEGER = 'integer';
    public const PHP_BOOLEAN = 'boolean';

    /**
     * @var Connection
     * database connection
     */
    private $conn;

    /**
     * @var Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var int $userId
     * taken from the logged in user of App\Services\AuthenticationService
     */
    private $userId;


    public function __construct(
        Connection $connection,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService,
        LoggerInterface $logger
    ) {
        $this->conn = $connection;
        $this->logger = $logger;

        // set useerId
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);
        $this->userId = $user['user_id'];
    }


    /**
     * @return array|null
     */
    public function getDatatypes(): ?array
    {
        $datatypes = [];
        $selectDatatypes = "SELECT id, name FROM datatypes";

        $stmt = $this->conn->prepare($selectDatatypes);

        try {
            $stmt->execute();
            $datatypes = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in datatypes', [__METHOD__, __LINE__]);
                return null;
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $datatypes;
    }

}
