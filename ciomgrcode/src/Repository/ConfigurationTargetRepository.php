<?php


namespace App\Repository;

use App\Types\ConfigurationTargetTypes;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;
use Exception;

class ConfigurationTargetRepository
{
    /**
     * @var Connection database connection
     */
    private $conn;

    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct(
        Connection $connection,
        LoggerInterface $logger
    ) {
        $this->conn = $connection;
        $this->logger = $logger;
    }


    /**
     * @param string $name
     * @return int|null
     * @author jsr
     */
    public function getDefaultProjectId(string $name = ConfigurationTargetTypes::PROJECT_DEFAULT): ?int  // TODO @jsr change => must select from unique_object_meta
    {
        $select = "SELECT id FROM configuration_targets WHERE  name = :name";

        $stmt = $this->conn->prepare($select);
        $stmt->bindValue(':name', $name, ParameterType::STRING);

        try {
            $stmt->execute();
            $value = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in configuration_targets for name=' . $name, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $value;
    }


    /**
     * @param string $name
     * @return int|null
     * @author jsr
     */
    public function getProjectId(string $name = ConfigurationTargetTypes::PROJECT): ?int
    {
        $select = "SELECT id FROM configuration_targets WHERE  name = :name";

        $stmt = $this->conn->prepare($select);
        $stmt->bindValue(':name', $name, ParameterType::STRING);

        try {
            $stmt->execute();
            $value = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in configuration_targets for name=' . $name, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $value;
    }


    /**
     * @param string $name
     * @return int|null
     * @author jsr
     */
    public function getDefaultPageId(string $name = ConfigurationTargetTypes::PAGE_DEFAULT): ?int
    {
        $select = "SELECT id FROM configuration_targets WHERE  name = :name";

        $stmt = $this->conn->prepare($select);
        $stmt->bindValue(':name', $name, ParameterType::STRING);

        try {
            $stmt->execute();
            $value = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in configuration_targets for name=' . $name, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $value;
    }


    /**
     * @param string $name
     * @return int|null
     * @author jsr
     */
    public function getPageId(string $name = ConfigurationTargetTypes::PAGE): ?int
    {
        $select = "SELECT id FROM configuration_targets WHERE  name = :name";

        $stmt = $this->conn->prepare($select);
        $stmt->bindValue(':name', $name, ParameterType::STRING);

        try {
            $stmt->execute();
            $value = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in configuration_targets for name=' . $name, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $value;
    }


    /**
     * @param string $name
     * @return int|null
     * @author jsr
     */
    public function getDefaultWebhookId(string $name = ConfigurationTargetTypes::WEBHOOK_DEFAULT): ?int
    {
        $select = "SELECT id FROM configuration_targets WHERE  name = :name";

        $stmt = $this->conn->prepare($select);
        $stmt->bindValue(':name', $name, ParameterType::STRING);

        try {
            $stmt->execute();
            $value = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in configuration_targets for name=' . $name, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $value;
    }


    /**
     * @param string $name
     * @return int|null
     * @author jsr
     */
    public function getWebhookId(string $name = ConfigurationTargetTypes::WEBHOOK): ?int
    {
        $select = "SELECT id FROM configuration_targets WHERE  name = :name";

        $stmt = $this->conn->prepare($select);
        $stmt->bindValue(':name', $name, ParameterType::STRING);

        try {
            $stmt->execute();
            $value = $stmt->fetchColumn();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in configuration_targets for name=' . $name, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $value;
    }


}
