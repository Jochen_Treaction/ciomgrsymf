<?php


namespace App\Repository;

use App\Entity\AdministratesUsers;
use App\Entity\Users;
use App\Repository\ObjectRegisterMetaRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Exception;
use Psr\Log\LoggerInterface;
use Doctrine\DBAL\ParameterType;



class AdministratesUsersRepository extends ServiceEntityRepository
{
    protected $logger;
    protected $conn;
    protected ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    private SeedPoolRepository $seedPoolRepository;

    public function __construct(Connection $connection,
                                ManagerRegistry $registry,
                                ObjectRegisterMetaRepository $objectRegisterMetaRepository,
                                LoggerInterface $logger,
                                SeedPoolRepository $seedPoolRepository
    )
    {
        parent::__construct($registry, AdministratesUsers::class);
        $this->logger = $logger;
        $this->conn = $connection;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->seedPoolRepository = $seedPoolRepository;
    }


    /**
     * @param int $user_id
     * @param int $invited_user_id
     * @param int $to_company_id
     * @return array
     */
    public function deleteUser(int $user_id, int $invited_user_id, int $to_company_id): array
    {
        $recordToDeleted = $this->getAdministratedUsersSingleRecord( $user_id, $invited_user_id, $to_company_id);

        if (!empty($recordToDeleted)) {
            try {
                $stmt = $this->conn->prepare('DELETE FROM users WHERE id = :user_id'); // cascades delete on administrates_users (!) => no need to delete records in administrates_users
                $stmt->bindValue('user_id', $user_id, ParameterType::INTEGER);
                $res = $stmt->execute();
                $deletedRecords = $stmt->rowCount();

                if ($res && $deletedRecords > 0) {
                    return $recordToDeleted;
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }
        return [];
    }


    /**
     * @param int    $invited_by_user_id
     * @param int    $invited_user_id
     * @param int    $to_company_id
     * @param string $with_roles
     * @return AdministratesUsers|null
     */
    public function addRecord(int $invited_by_user_id, int $invited_user_id, int $to_company_id, string $with_roles): ?array
    {
        try {
            $time_stamp = date('Y-m-d h:i:s');

            $stmt = $this->conn->prepare('INSERT INTO administrates_users (user_id, invited_user_id, to_company_id, with_roles, created, updated, created_by, updated_by) VALUES (:user_id, :invited_user_id, :to_company_id,:with_roles, :created, :updated, :created_by, :updated_by)');
            $stmt->bindParam('user_id', $invited_by_user_id, ParameterType::INTEGER);
            $stmt->bindParam('invited_user_id', $invited_user_id, ParameterType::INTEGER);
            $stmt->bindParam('to_company_id', $to_company_id, ParameterType::INTEGER);
            $stmt->bindParam('with_roles', $with_roles, ParameterType::STRING);
            $stmt->bindParam('to_company_id', $to_company_id, ParameterType::INTEGER);
            $stmt->bindParam('created', $time_stamp, ParameterType::STRING);
            $stmt->bindParam('updated', $time_stamp, ParameterType::STRING);
            $stmt->bindParam('created_by', $invited_by_user_id, ParameterType::INTEGER);
            $stmt->bindParam('updated_by', $invited_by_user_id, ParameterType::INTEGER);
            $res = $stmt->execute();
            try {
                $this->conn->commit();
            } catch (Exception $e) {
                ;// do nothing
            }
            $insertedRecords = $stmt->rowCount();

            // $this->logger->info('result = ', ['res'=> $res, 'insertedRecords'=> $insertedRecords, __METHOD__, __LINE__]);

            if ($res && $insertedRecords > 0) {
                $administratedUser = $this->getAdministratedUsersSingleRecord($invited_by_user_id, $invited_by_user_id,$to_company_id );
                return $administratedUser;
            } else {
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__,__LINE__]);
        }

        return null;
    }


    /**
     * @param int      $invited_user_id
     * @param int|null $to_company_id
     * @return AdministratesUsers|null
     */
    public function getRecordByInvitedUserId(int $invited_user_id, int $to_company_id=null):?AdministratesUsers
    {
        if($to_company_id !== null) {
            $toCompanyId = ['toCompany' => $to_company_id];
        } else {
            $toCompanyId = [];
        }

        try {
            $em = $this->getEntityManager();
            $administratedUser = $em->getRepository(AdministratesUsers::class)
                ->findOneBy(array_merge(['invitedUser' => $invited_user_id], $toCompanyId));
            return $administratedUser;

        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return null;
    }


    /**
     * @param string $email
     * @param int    $company_id
     */
    public function deleteUserFromCompany(string $email, int $company_id)
    {
        $em = $this->getEntityManager();
        $users = $em->getRepository(Users::class);
        $users->findOneBy(['']);
    }


    /**
     * @param int      $administratedUsersCompanyId
     * @param int      $administratedByUserId
     * @param int|null $superAdminUserId introduced for more flexibility for AdministrationService::getAdministratedUsersForJson, called by AdminstrationController::ajaxGetUsersByCompanyId
     * @return array [to_company_id.'-'.invited_user_id] [id,user_id,invited_user_id,to_company_id,invited_user_roles,invited_user_email,invited_user_first_name,invited_user_last_name,invited_user_status,invited_user_failed_logins,invited_user_company_name]
     */
    public function getAdministratedUsersForAdmin(int $administratedUsersCompanyId, int $administratedByUserId=null, int $superAdminUserId=null): array
    {
        $isSuperAmin = (null !== $superAdminUserId);

        $administrates_users = [];
        if ($isSuperAmin) {
            $sql = 'SELECT 
                999 AS id,
                IF(au.user_id IS NOT NULL, au.user_id, :superAdminUserId) AS user_id,
                u.id AS invited_user_id,
                c.id AS to_company_id,
                u.roles AS invited_user_roles,
                u.email AS invited_user_email,
                u.salutation AS invited_user_salutation,
                u.first_name AS invited_user_first_name,
                u.last_name AS invited_user_last_name,
                u.status AS invited_user_status,
                u.failed_logins AS invited_user_failed_logins,
                c.name AS invited_user_company_name 
            FROM company c 
                JOIN users u ON (u.company_id = c.id)
                LEFT OUTER JOIN administrates_users au ON (au.to_company_id = c.id AND au.invited_user_id = u.id) 
            WHERE c.id = :company_id';

        } else { // masterAdmin / Admin
            if (null !== $administratedByUserId) {
                $sql = 'SELECT 
                    au.id, 
                    au.user_id,
                    au.invited_user_id,
                    au.to_company_id,
                    au.with_roles AS invited_user_roles,
                    u.email AS invited_user_email,
                    u.salutation AS invited_user_salutation,
                    u.first_name AS invited_user_first_name,
                    u.last_name AS invited_user_last_name,
                    u.status AS invited_user_status,
                    u.failed_logins AS invited_user_failed_logins,
                    c.name AS invited_user_company_name 
                FROM administrates_users au 
                    JOIN users u ON (u.id = au.invited_user_id) 
                    JOIN company c ON (c.id = au.to_company_id) 
                WHERE au.user_id = :user_id
                AND au.to_company_id = :company_id';
            } else {
                $sql = 'SELECT 
                    au.id, 
                    au.user_id,
                    au.invited_user_id,
                    au.to_company_id,
                    au.with_roles AS invited_user_roles,
                    u.email AS invited_user_email,
                    u.salutation AS invited_user_salutation,
                    u.first_name AS invited_user_first_name,
                    u.last_name AS invited_user_last_name,
                    u.status AS invited_user_status,
                    u.failed_logins AS invited_user_failed_logins,
                    c.name AS invited_user_company_name 
                FROM administrates_users au 
                    JOIN users u ON (u.id = au.invited_user_id) 
                    JOIN company c ON (c.id = au.to_company_id) 
                WHERE au.to_company_id = :company_id';
            }
        }

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('company_id', $administratedUsersCompanyId); // always added

            if ($isSuperAmin) {
                $stmt->bindParam('superAdminUserId', $superAdminUserId, ParameterType::INTEGER);
            } else {
                if (null !== $administratedByUserId) {
                    $stmt->bindParam('user_id', $administratedByUserId, ParameterType::INTEGER);
                }
            }

            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            foreach ($temp as $user_id_list) {
                $administrates_users[ $user_id_list['to_company_id'] .'-'. $user_id_list['invited_user_id'] ] = $user_id_list;
            }
            return $administrates_users;

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $administrates_users;
    }


    /**
     * getAdministratedUsersSingleRecord
     * @param int $user_id set to -1 if requesting user is super admin
     * @param int $invited_user_id
     * @param int $to_company_id
     * @return array [to_company_id.'-'.invited_user_id] [id,user_id,invited_user_id,to_company_id,invited_user_roles,invited_user_email,invited_user_first_name,invited_user_last_name,invited_user_status,invited_user_failed_logins,invited_user_company_name]
     */
    public function getAdministratedUsersSingleRecord(int $user_id, int $invited_user_id, int $to_company_id):array
    {
        $this->logger->info('getAdministratedUsersSingleRecord', [$user_id,$invited_user_id,$to_company_id, __METHOD__,__LINE__]);

        // super admin special handling 2020-05-03
        if($user_id < 0 ) {
            $operator = '>';
            $user_id = 0;
        } else {
            $operator = '=';
        }

        $administrates_users = [];
        $sql = "SELECT 
                    au.id, 
                    au.user_id,
                    au.invited_user_id,
                    au.to_company_id,
                    au.with_roles AS invited_user_roles,
                    u.email AS invited_user_email,
                    u.salutation AS invited_user_salutation,
                    u.first_name AS invited_user_first_name,
                    u.last_name AS invited_user_last_name,
                    u.status AS invited_user_status,
                    u.failed_logins AS invited_user_failed_logins,
                    c.name AS invited_user_company_name 
                FROM administrates_users au 
                    JOIN users u ON (u.id = au.invited_user_id) 
                    JOIN company c ON (c.id = au.to_company_id) 
                WHERE au.user_id   {$operator} :user_id
                AND   au.invited_user_id = :invited_user_id
                AND   au.to_company_id   = :to_company_id";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('user_id', $user_id);// always added
            $stmt->bindParam('invited_user_id', $invited_user_id);// always added
            $stmt->bindParam('to_company_id', $to_company_id);// always added
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);
            foreach ($temp as $user_id_list) {
                $administrates_users[$user_id_list['to_company_id'] . '-' . $user_id_list['invited_user_id']] = $user_id_list;
            }
            $this->logger->info('$administrates_users',[$administrates_users, __METHOD__,__LINE__]);
            return $administrates_users;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $administrates_users;
    }


    /**
     * getAdministratedUsersForSuperAdmin
     * @param int|null $byCompanyId
     * @return array [to_company_id.'-'.invited_user_id] [id,user_id,invited_user_id,to_company_id,invited_user_roles,invited_user_email,invited_user_first_name,invited_user_last_name,invited_user_status,invited_user_failed_logins,invited_user_company_name]
     */
    public function getAdministratedUsersForSuperAdmin(int $superAdminUserId, int $byCompanyId = null): array
    {
        $administrates_users = [];

        if (null !== $byCompanyId) {
            $sql = 'SELECT 
                999 AS id, 
                :superAdminUserId AS user_id,
                u.id AS invited_user_id,
                c.id AS to_company_id,
                u.roles AS invited_user_roles,
                u.email AS invited_user_email,
                u.salutation AS invited_user_salutation,
                u.first_name AS invited_user_first_name,
                u.last_name AS invited_user_last_name,
                u.status AS invited_user_status,
                u.failed_logins AS invited_user_failed_logins,
                c.name AS invited_user_company_name 
            FROM company c 
                JOIN users u ON (u.company_id = c.id) 
            WHERE c.id = :company_id';
        } else {
            $sql = 'SELECT 
                999 AS id, 
                :superAdminUserId AS user_id,
                u.id AS invited_user_id,
                c.id AS to_company_id,
                u.roles AS invited_user_roles,
                u.email AS invited_user_email,
                u.salutation AS invited_user_salutation,
                u.first_name AS invited_user_first_name,
                u.last_name AS invited_user_last_name,
                u.status AS invited_user_status,
                u.failed_logins AS invited_user_failed_logins,
                c.name AS invited_user_company_name 
            FROM company c 
                JOIN users u ON (u.company_id = c.id)';
        }


        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('superAdminUserId', $superAdminUserId, ParameterType::INTEGER);

            if (null !== $byCompanyId) {
                $stmt->bindParam('company_id', $byCompanyId, ParameterType::INTEGER);
            }
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            foreach ($temp as $user_id_list) {
                $administrates_users[ $user_id_list['to_company_id'] .'-'. $user_id_list['invited_user_id'] ] = $user_id_list;
            }
            return $administrates_users;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $administrates_users;
    }


    /**
     * getAdministratedUsersForMasterAdmin
     * @param int $masterAdminUserId
     * @return array [to_company_id.'-'.invited_user_id] [id,user_id,invited_user_id,to_company_id,invited_user_roles,invited_user_email,invited_user_first_name,invited_user_last_name,invited_user_status,invited_user_failed_logins,invited_user_company_name]
     */
    public function getAdministratedUsersForMasterAdmin(int $masterAdminUserId): array
    {
        $administrates_users = [];
        try {
            $stmt = $this->conn->prepare('
            SELECT 
                au.id, 
                au.user_id,
                au.invited_user_id,
                au.to_company_id,
                au.with_roles AS invited_user_roles,
                u.email AS invited_user_email,
                u.salutation AS invited_user_salutation,
                u.first_name AS invited_user_first_name,
                u.last_name AS invited_user_last_name,
                u.status AS invited_user_status,
                u.failed_logins AS invited_user_failed_logins,
                c.name AS invited_user_company_name
            FROM administrates_users au 
                JOIN users u ON (u.id = au.invited_user_id) 
                JOIN company c ON (c.id = au.to_company_id) 
            WHERE au.user_id = :user_id');
            $stmt->bindParam('user_id', $masterAdminUserId);
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            foreach ($temp as $user_id_list) {
                $administrates_users[ $user_id_list['to_company_id'] .'-'. $user_id_list['invited_user_id'] ] = $user_id_list;
            }

            return $administrates_users;

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $administrates_users;
    }


    /**
     * @param array $userData
     * @return array
     */
    public function updateUserRole(array $userData):array
    {
        $in__user_id = $userData['user_id']; // keep incoming id for call to getAdministratedUsersSingleRecord

        if($userData['user_id'] < 0 ) {
            $operator = '>';
            $userData['user_id'] = 0;
        } else {
            $operator = '=';
        }

        $this->logger->info('$userData[\'role\']', [$userData['role'], __METHOD__,__LINE__]);
        $userData['role'] = json_encode($userData['role']); // change array to json

        try {
            $stmt = $this->conn->prepare("
            UPDATE administrates_users 
            SET to_company_id = :to_company_id,
                with_roles = :with_roles
            WHERE user_id {$operator} :user_id
            AND   to_company_id = :selected_company
            AND   invited_user_id = :selected_user");


            $stmt->bindParam('to_company_id', $userData['to_company'], ParameterType::INTEGER);
            $stmt->bindParam('with_roles', $userData['role'], ParameterType::STRING);
            $stmt->bindParam('user_id', $userData['user_id'], ParameterType::INTEGER);
            $stmt->bindParam('selected_company', $userData['selected_company'], ParameterType::INTEGER);
            $stmt->bindParam('selected_user', $userData['selected_user'], ParameterType::INTEGER);
            $res = $stmt->execute();

            if($res) {
                return $this->getAdministratedUsersSingleRecord($in__user_id, $userData['selected_user'], $userData['to_company']);
            }

        } catch (Exception $e) {
            $this->logger->error('SQL ERROR', [$e->getMessage(), __METHOD__, __LINE__]);
        }
        return [];
    }


    /**
     * @param int $company_id
     * @return array
     */
    public function getNoUsersCampaignsByCompanyId(int $company_id):array
    {
        $sql = '
        SELECT 
            c.id  AS company_id,
            u.no_user AS count_users,
            cp.no_camp AS count_campaigns
        FROM company c 
        LEFT OUTER JOIN (select u1.company_id, count(*) AS no_user FROM users u1 GROUP BY 1) AS u ON (u.company_id = c.id)
        LEFT OUTER JOIN (select c1.company_id, count(*) AS no_camp FROM campaign c1 GROUP BY 1) AS cp ON (cp.company_id = c.id)
        WHERE c.id = :company_id';

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('company_id', $company_id);
            $res = $stmt->execute();
            $data = $stmt->fetchAll(FetchMode::ASSOCIATIVE);
            if ($res) {
                return $data[0];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return [];
    }


    /**
     * @param int $company_id
     * @return array [id1,id2,...]
     */
    public function getUserIdsOfUsersCompany(int $company_id):array
    {
        $data = [];
        $sql = 'SELECT DISTINCT idlist.id FROM
                (SELECT id FROM users WHERE company_id = :company_id
                UNION 
                SELECT invited_user_id AS id FROM administrates_users WHERE to_company_id = :company_id) AS idlist;';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('company_id', $company_id);
            $res = $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            foreach ($temp as $d) {
                $data[] = $d['id'];
            }
            return $data;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $data;
    }


    /**
     * @param int $user_id
     * @param int $company_id
     * @return bool
     */
    public function doesUserManageCompany(int $user_id, int $company_id): bool
    {
        $sql = 'SELECT count(*) as manages FROM administrates_companies WHERE user_id = :user_id AND company_id = :company_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('user_id', $user_id);
            $stmt->bindParam('company_id', $company_id);
            $res = $stmt->execute();
            $this->logger->info('res', [$res, __METHOD__, __LINE__]);

            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);
            $this->logger->info('$temp', [$temp, __METHOD__, __LINE__]);

            return ((int)$temp[0]['manages'] === 1) ? true : false;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return false;
    }


    /**
     * @return array|null [id, company_id, login_date, account_name, account_no, account_object_unique_id, account_status,  CONCAT( u.salutation, ' ', u.first_name,' ' ,u.last_name) username, email, phone, mobile, failed_logins, status, dpa, dpa_date, roles]
     */
    public function getUserOverview(): ?array
    {
        $records = [];
        $selectUsers = "SELECT u.id, u.company_id, date_format(u.login_date,'%Y-%m-%d') as login_date, c.name account_name, c.account_no, o.id account_objectregister_id, o.object_unique_id account_object_unique_id, s.value account_status,  CONCAT( u.salutation, ' ', u.first_name,' ' ,u.last_name) username, u.email, u.phone, u.mobile, u.failed_logins, u.status, u.dpa, u.dpa_date, u.roles, date_format(u.created, '%Y-%m-%d') as created 
        FROM users u
        LEFT OUTER JOIN company c on u.company_id = c.id
        LEFT OUTER JOIN objectregister o ON o.id = c.objectregister_id
        LEFT OUTER JOIN statusdef s ON o.statusdef_id = s.id 
        ";

        $stmt = $this->conn->prepare($selectUsers);
        try {
            $stmt->execute();
            $records = $stmt->fetchAll();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in user', [__METHOD__, __LINE__]);
                return [];
            } else {
                foreach ($records as $key => $rec) {
                    $advertorialStatus = $this->objectRegisterMetaRepository->getMetaValueAsString($rec['account_objectregister_id'], $this->objectRegisterMetaRepository::METAKEY_ADVERTORIAL_STATUS );
                    $seedpoolStatus = $this->seedPoolRepository->hasSeedPool($rec['company_id']);
                    $roles = explode(',', preg_replace(['/\[/', '/\]/', '/"/'], ['', '', ''], $rec['roles']));
                    $maxRole = in_array('ROLE_SUPER_ADMIN', $roles) ? 'ROLE_SUPER_ADMIN' : ((in_array('ROLE_MASTER_ADMIN',
                        $roles)) ? 'ROLE_MASTER_ADMIN' : 'ROLE_USER');
                    $records[$key]['userrole'] = preg_replace(['/ROLE_/', '/_/'], ['', ' '], $maxRole);
                    unset($records[$key]['roles']);
                    $records[$key]['account_advertorial_status'] =  ($advertorialStatus === null) ? false : (bool) $advertorialStatus;
                    $records[$key]['account_seedpool_status'] =  ($seedpoolStatus === null) ? false : (bool) $seedpoolStatus;
                }
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $records;
    }

}
