<?php


namespace App\Repository;


use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * @property LoggerInterface logger
 * @property Connection conn
 */
class InboxMonitorRepository
{
    const DEFAULT_USER_ID = 99999;
    protected int $currentUserId;

    public function __construct(LoggerInterface $logger, Connection $connection)
    {
        $this->logger = $logger;
        $this->conn = $connection;
        $this->currentUserId = self::DEFAULT_USER_ID;
    }

    /**
     * set class <b>global current user id</b>, if before you run <b style="color:blue"><u>inserts</u> or <u>updates</u></b>, otherwise a default user id (99999) is used
     * @param int $user_id users.id
     */
    public function setRepostitoriesCurrentUserId(int $user_id)
    {
        $this->currentUserId = $user_id;
    }

    public function getMosentoBaseDetails(string $value): ?array
    {
        $id = 0;
        try {
            if (empty($value)) {
                throw new RuntimeException('Invalid statusdef valud provided');
            }
            $sql = 'SELECT id FROM statusdef WHERE value = :value';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':value', $value, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if (!empty($result) && $rowCount > 0) {
                $id = $result[0]['id'];
            }
            return $id;
        } catch (DBALException|RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $mosentoSeedPoolId
     * @return array
     * @internal Get all mosento_inbox_monitor_id's in array for a seed pool with mosento_seed_pool_id
     */
    public function getMosentoInboxMonitorIdsArray(string $mosentoSeedPoolId): array
    {
        $responseArray = [];
        $mosentoInboxMonitorIdsArray = [];
        try {
            $sql = 'SELECT mosento_inbox_monitor_id FROM inbox_monitor 
                                    JOIN seed_pool sp on inbox_monitor.seed_pool_id = sp.id 
                                                             and sp.mosento_seed_pool_id = :mosento_seed_pool_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':mosento_seed_pool_id', $mosentoSeedPoolId, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0) {
                $responseArray = $stmt->fetchAll();
                foreach ($responseArray as $item) {
                    $mosentoInboxMonitorIdsArray[] = $item['mosento_inbox_monitor_id'];
                }
            }
            return $mosentoInboxMonitorIdsArray;
        } catch (DBALException|RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $mosentoInboxMonitorIdsArray;
        }

    }

    /**
     * @param int $mosentoInboxMonitorId
     * @param array $inboxMonitor
     * @return bool
     * @author Aki
     * @internal Update inbox_monitor table with mosento_inbox_monitor_id
     */
    public function updateInboxMonitorWithMosentoInboxMonitorId(string $mosentoInboxMonitorId, array $inboxMonitor): bool
    {
        $status = false;
        if (empty($mosentoInboxMonitorId) || empty($inboxMonitor)) {
            throw new RuntimeException('Invalid parameters provided');
        }
        //Get the span reason
        $spamReason = '';
        if (count($inboxMonitor['header_info']) > 0) {
            $spamReason = implode(",", ($inboxMonitor['header_info']));
        }
        $updateSql = "update inbox_monitor  
                      set received_emails = :received_emails ,
                          inbox_emails = :inbox_emails,
                          spam_emails = :spam_emails,
                          spam_reason = :spam_reason
                      where mosento_inbox_monitor_id = :mosento_inbox_monitor_id";
        try {
            $stmt = $this->conn->prepare($updateSql);
            $stmt->bindParam(':received_emails', $inboxMonitor['received_total']);
            $stmt->bindParam(':inbox_emails', $inboxMonitor['received_inbox']);
            $stmt->bindValue(':spam_emails', $inboxMonitor['received_spam']);
            $stmt->bindValue(':spam_reason', $spamReason);
            $stmt->bindValue(':mosento_inbox_monitor_id', $mosentoInboxMonitorId, ParameterType::STRING);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }
        return $status;
    }

    /**
     * @param int $seedPoolId
     * @param int $domainId
     * @param int $objectRegisterIdForInboxMonitor
     * @param string $mosentoInboxMonitorId
     * @param string $name
     * @param string $from_domain
     * @param string $from_address
     * @param string $received_total
     * @param string $received_inbox
     * @param string $received_spam
     * @param string $spamReason
     * @return int|null
     * @author Aki
     * @internal Create entry in inbox monitor
     */
    public function createInboxMonitor(int    $seedPoolId,
                                       int    $domainId,
                                       int    $objectRegisterIdForInboxMonitor,
                                       string $mosentoInboxMonitorId,
                                       string $name,
                                       string $firstEmailReceived,
                                       string $from_domain,
                                       string $from_address,
                                       string $received_total,
                                       string $received_inbox,
                                       string $received_spam,
                                       string $spamReason
    ): ?int
    {
        $insertedId = null;
        $select = 'INSERT INTO inbox_monitor (
                           domain_id,
                           seed_pool_id,
                           objectregister_id,
                           mosento_inbox_monitor_id,
                           name,
                           received,
                           sender_alias,
                           sender_address,
                           received_emails,
                           inbox_emails,
                           spam_emails,
                           spam_reason,
                            created_by,
                           updated_by
                           )
                            values (
                                    :domain_id,
                                    :seed_pool_id,
                                    :objectregister_id,
                                    :mosento_inbox_monitor_id,
                                    :name,
                                    :received,
                                    :sender_alias,
                                    :sender_address,
                                    :received_emails,
                                    :inbox_emails,
                                    :spam_emails,
                                    :spam_reason,
                                    :created_by,
                                    :updated_by    
                            )';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':domain_id', $domainId);
            $stmt->bindParam(':seed_pool_id', $seedPoolId);
            $stmt->bindParam(':objectregister_id', $objectRegisterIdForInboxMonitor);
            $stmt->bindParam(':mosento_inbox_monitor_id', $mosentoInboxMonitorId);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':received', $firstEmailReceived);
            $stmt->bindParam(':sender_alias', $from_domain);
            $stmt->bindParam(':sender_address', $from_address);
            $stmt->bindParam(':received_emails', $received_total);
            $stmt->bindParam(':inbox_emails', $received_inbox);
            $stmt->bindParam(':spam_emails', $received_spam);
            $stmt->bindParam(':spam_reason', $spamReason);
            $stmt->bindParam(':created_by', $this->currentUserId);
            $stmt->bindParam(':updated_by', $this->currentUserId);
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                return null;
            }
            $insertedId = $this->conn->lastInsertId();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $insertedId;
    }

    /**
     * @param string $mosentoInboxMonitorId
     * @return bool
     * @author Aki
     * @internal Delete the entry in inbox_monitor table with mosento_inbox_monitor_id
     */
    public function deleteInboxMonitorEntryWithMosentoInboxMonitorId(string $mosentoInboxMonitorId): bool
    {
        $status = false;
        if (empty($mosentoInboxMonitorId)) {
            return $status;
        }
        $sql = "delete from inbox_monitor where mosento_inbox_monitor_id = :mosento_inbox_monitor_id";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':mosento_inbox_monitor_id', $mosentoInboxMonitorId, ParameterType::STRING);
            $result = $stmt->execute();
            if (is_bool($result) && $result) {
                $status = $result;
            }
            return $status;
        } catch (DBALException|RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param string $domain
     * @return bool
     * @author Aki
     * @internal Check if the domain exists in MIO_domains table
     */
    public function checkIfDomainExists(string $domain): bool
    {
        $select = 'SELECT 1 FROM domains WHERE domain = :domain';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':domain', $domain);
            $stmt->execute();
            $result = $stmt->fetch();
            if (empty($result)) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param string $domain
     * @return int|null
     * @author Aki
     * @internal Get domain.id with domain
     */
    public function getDomainIdWithDomain(string $domain): ?int
    {
        $select = 'SELECT id FROM domains WHERE domain = :domain';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':domain', $domain);
            $stmt->execute();
            $result = $stmt->fetch();
            if (empty($result)) {
                return null;
            }
            return $result['id'];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int|null $objectRegisterIdForDomain
     * @param string $domain
     * @param string|null $subDomain
     * @return int|null
     * @author Aki
     */
    public function createDomainEntry(?int   $objectRegisterIdForDomain,
                                      string $domain,
                                      string $subDomain,
                                      string $received,
                                      ?string $serverIpAddress,
                                      int $companyId,
                                      string $seedPool
    ): ?int
    {
        $select = 'INSERT INTO domains (
                           objectregister_id,
                           company_id,
                           domain,
                           subdomain,
                           received,
                           seed_pool,
                           server_ip_address,
                           created_by,
                           updated_by
                           )
                            values (
                                    :objectregister_id,
                                    :company_id,
                                    :domain,
                                    :subdomain,
                                    :received,
                                    :seed_pool,
                                    :server_ip_address,
                                    :created_by,
                                    :updated_by    
                            )';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':objectregister_id', $objectRegisterIdForDomain);
            $stmt->bindParam(':company_id', $companyId);
            $stmt->bindParam(':domain', $domain);
            $stmt->bindParam(':subdomain', $subDomain);
            $stmt->bindParam(':received', $received);
            $stmt->bindParam(':seed_pool', $seedPool);
            $stmt->bindParam(':server_ip_address', $serverIpAddress);
            $stmt->bindParam(':created_by', $this->currentUserId);
            $stmt->bindParam(':updated_by', $this->currentUserId);
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                return null;
            }
            $insertedId = $this->conn->lastInsertId();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $insertedId;
    }

    /**
     * @param $company_id
     * @return array
     * @internal - Get all domains belong to a company with company_id
     * @author Aki
     */
    public function getDomainsForCompanyInArray($company_id): array
    {
        $select =
            'SELECT DISTINCT 
                d.id, 
                d.domain 
            FROM domains d
            JOIN inbox_monitor im ON d.id = im.domain_id
            JOIN seed_pool sp ON im.seed_pool_id = sp.id
            JOIN company c ON sp.company_id = c.id
            WHERE c.id= :company_id';

        $domainsForCompany = [];

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->execute();
            $records = $stmt->fetchAll();
            foreach ($records as $item) {
                $domainsForCompany[$item['id']] = $item['domain'];
            }

            if ($stmt->rowCount() > 0) {
                return $domainsForCompany;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $domainsForCompany;

    }


    /**
     * @param int $company_id
     * @param string $seed_pool_id
     * @param string $domain_id
     * @param string $spam_reason
     * @param string $seed_pool_type
     * @param string $search_string
     * @return int
     * @internal - Get count of the records for total number of records for pagination bar
     * @author Aki
     */
    public function getTotalNumberOfRecordsForView(
        int    $company_id,
        string $seed_pool_group_id,
        string $seed_pool_id,
        string $domain_id,
        string $spam_reason,
        string $seed_pool_type,
        string $search_string
    ): int
    {

        if ('all' === strtolower(trim($seed_pool_id))) {
            $andSeedPoolIdSelection = '';
        } else {
            $andSeedPoolIdSelection = "AND sp.id = '$seed_pool_id'";
        }

        /*
        if ('all' === strtolower(trim($spam_reason))) {
            $whereSpamReasonSelection = '';
        } else {
            $whereSpamReasonSelection = "WHERE i.spam_reason like '%$spam_reason%'";
        }
        */

        if ('all' === strtolower(trim($spam_reason))) {
            $whereSpamReasonSelection = '';
        } elseif ('nospamreason' === strtolower(trim($spam_reason))) {
            $whereSpamReasonSelection = "WHERE i.spam_reason = ''";
        } else {
            $whereSpamReasonSelection = "WHERE i.spam_reason like '%$spam_reason%'";
        }


        if ('all' === strtolower(trim($seed_pool_type))) {
            $andSeedPoolTypeSelection = '';
        } else {
            $andSeedPoolTypeSelection = "AND sp.type = '$seed_pool_type'";
        }

        if ('all' === strtolower(trim($domain_id))) {
            $andDomainIdSelection = '';
        } else {
            //todo: Jsr please add domains table and search for domain name
            $andDomainIdSelection = "AND d.id = $domain_id";
        }

        if ('all' === strtolower(trim($seed_pool_group_id))) {
            $andSeedPoolGroupIdSelection = '';
        } else {
            $andSeedPoolGroupIdSelection = "AND sp.seed_pool_group_id = $seed_pool_group_id";
        }

        if (empty($search_string)) {
            $searchStringSelection = '';
        } else {
            //todo: Jsr please add search table and search for the string for count
            $searchStringSelection = "JOIN search_inbox_monitor sibm ON sibm.id = i.id AND MATCH(searchtext) AGAINST('{$search_string}')";
        }

        $count = 0;

        $select = "SELECT count(*) as count
                    FROM inbox_monitor i
                    JOIN domains d ON d.id = i.domain_id $andDomainIdSelection
                    JOIN seed_pool sp on i.seed_pool_id = sp.id AND sp.company_id = :company_id $andSeedPoolIdSelection $andSeedPoolTypeSelection $andSeedPoolGroupIdSelection
                    JOIN seed_pool_group sg ON sg.id = sp.seed_pool_group_id
                    $searchStringSelection 
                    $whereSpamReasonSelection";

        if ($_ENV['APP_ENV'] === 'dev') {
            try {
                $this->logger->info('inboxsql', [$select, __METHOD__, __LINE__]);
                file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . '../../var/log/totalNumRecordsInboxMonitor.sql', $select . "\ncompany_id=$company_id");
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $count = $stmt->fetchColumn();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        $this->logger->info('getTotalNumberOfRecordsForView inbox_monitor', [$count, __METHOD__, __LINE__]);
        return $count;
    }


    /**
     * @param int $company_id
     * @param string $seed_pool_id
     * @param string $domain_id
     * @param string $spam_reason
     * @param string $seed_pool_type
     * @param string $search_string
     * @param int $offset
     * @param int $limit
     * @return array
     * @internal Returns inbox monitor for a selection with limit and offset - No need to store anything in cache
     */
    public function getInboxMonitorForView(
        int    $company_id,
        string $seed_pool_group_id,
        string $seed_pool_id,
        string $domain_id,
        string $spam_reason,
        string $seed_pool_type,
        string $search_string,
        int    $offset,
        int    $limit
    ): array
    {
        if ('all' === strtolower(trim($seed_pool_id))) {
            $andSeedPoolIdSelection = '';
        } else {
            $andSeedPoolIdSelection = "AND sp.id = $seed_pool_id";
        }

        if ('all' === strtolower(trim($spam_reason))) {
            $whereSpamReasonSelection = '';
        } elseif ('nospamreason' === strtolower(trim($spam_reason))) {
            $whereSpamReasonSelection = "WHERE i.spam_reason = ''";
        } else {
            $whereSpamReasonSelection = "WHERE i.spam_reason like '%$spam_reason%'";
        }

        if ('all' === strtolower(trim($seed_pool_type))) {
            $andSeedPoolTypeSelection = '';
        } else {
            $andSeedPoolTypeSelection = "AND sp.type = '$seed_pool_type'";
        }

        if ('all' === strtolower(trim($domain_id))) {
            $andDomainIdSelection = '';
        } else {
            $andDomainIdSelection = "AND d.id = $domain_id";
        }

        if ('all' === strtolower(trim($domain_id))) {
            $andDomainNameSelection = '';
        } else {
            $andDomainNameSelection = "AND d.id = '$domain_id'";
        }

        if ('all' === strtolower(trim($seed_pool_group_id))) {
            $andSeedPoolGroupIdSelection = '';
        } else {
            $andSeedPoolGroupIdSelection = "AND sp.seed_pool_group_id = $seed_pool_group_id";
        }

        if (empty($search_string)) {
            $searchStringSelection = '';
        } else {
            $searchStringSelection = "JOIN search_inbox_monitor sibm ON sibm.id = i.id AND MATCH(searchtext) AGAINST('{$search_string}')";
        }

        //This is limit and offset - @jsr No need to store params in cache this handles all the stuff. @aki by JSR: Your solution is better and easier than the min id/max id one -> got it!
        $_limit = "LIMIT $offset, $limit";

        $select = "SELECT i.id, i.updated, i.name, i.received, sg.name as SeedPoolGroupName, sp.name as SeedPool ,sp.type as SeedPoolType, i.sender_alias, i.sender_address, i.received_emails, i.spam_emails, i.spam_reason, i.inbox_emails
                    FROM inbox_monitor i
                    JOIN domains d ON d.id = i.domain_id $andDomainIdSelection 
                    JOIN seed_pool sp on i.seed_pool_id = sp.id AND sp.company_id = :company_id $andSeedPoolIdSelection $andSeedPoolTypeSelection $andSeedPoolGroupIdSelection 
                    JOIN seed_pool_group sg ON sg.id = sp.seed_pool_group_id
                    $searchStringSelection 
                    $whereSpamReasonSelection
                    ORDER BY i.received DESC
                    $_limit
                    ";

        if ($_ENV['APP_ENV'] === 'dev') {
            try {
                $this->logger->info('inboxsql', [$select, __METHOD__, __LINE__]);
                file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . '../../var/log/selectInboxMonitor.sql', $select . "\ncompany_id=$company_id");
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }

        $records = [];
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->execute();
            $records = $stmt->fetchAll();
            return $records;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $records;
        }
    }


    /**
     * @param int $inboxMonitorId
     * @return array <code>[id, domain_id, seed_pool_id, objectregister_id, mosento_inbox_monitor_id, name, sender_alias, sender_address, received_emails, inbox_emails, spam_emails, spam_reason, created, updated, created_by, updated_by, InboxQuota, SeedPoolGroupName, SeedPool, SeedPoolType]</code>
     */
    public function getInboxMonitorForDetailsView(int $inboxMonitorId): array
    {
        $select = "SELECT i.*, (i.inbox_emails/i.received_emails) as InboxQuota, sg.name as SeedPoolGroupName, sp.name as SeedPool ,sp.type as SeedPoolType
                            FROM inbox_monitor i
                            JOIN domains d ON d.id = i.domain_id 
                            JOIN seed_pool sp on i.seed_pool_id = sp.id 
                            JOIN seed_pool_group sg ON sg.id = sp.seed_pool_group_id
                            WHERE i.id = :inboxMonitorId
                            ";

        $records = [];
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':inboxMonitorId', $inboxMonitorId);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        if ($_ENV['APP_ENV'] === 'dev') {
            try {
                file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . '../../var/log/selectInboxMonitorDetails.sql', $select . "\ninboxMonitorId=$inboxMonitorId");
                file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . '../../var/log/selectInboxMonitorDetails.sql', print_r($records, true), FILE_APPEND);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }
        return $records;
    }

    /**
     * @param int $domainId
     * @param array $domainBlacklistData
     * @return int|null
     * @author Aki
     */
    public function createDomainBlacklistEntry(int $domainId, array $domainBlacklistData): ?int
    {
        $select = 'INSERT INTO domain_blacklists (
                           domains_id,
                           cyren_blacklisted,
                           cyren_last_check,
                           cyren_url_categories,
                           public_bls_blacklisted,
                           public_bls_last_check,
                           public_bls_listed,
                           created_by,
                           updated_by
                           )
                            values (
                            :domains_id,
                           :cyren_blacklisted,
                           :cyren_last_check,
                           :cyren_url_categories,
                           :public_bls_blacklisted,
                           :public_bls_last_check,
                           :public_bls_listed,
                           :created_by,
                           :updated_by    
                            )';

        /* example of blacklist monitor data form mosento
        * "blacklist_monitor":[
        {
           "domain":"gmx.org",
           "hosting_ip":"82.165.229.87",
           "cyren_url_categories":[
              "Web-based Email"
           ],
           "cyren_blacklisted":false,
           "cyren_last_check":"2022-03-04 08:04:08",
           "public_bls_blacklisted":false,
           "public_bls_listed":null,
           "public_bls_last_check":"2022-03-09 19:52:13"
        }
            ]*/

        //mapping of data
        $cyrenUrlCategories = null;
        $publicBlsListed = null;
        $publicBlsBlacklisted = 0;
        $cyrenBlacklisted = 0;
        //convert string to time
        $cyrenLastCheckDate = $domainBlacklistData['cyren_last_check'];
        $publicLastCheckDate = $domainBlacklistData['public_bls_last_check'];
        if($cyrenLastCheckDate === '0000-00-00 00:00:00'){
            $cyrenLastCheckDate = null;
        }
        if($publicLastCheckDate === '0000-00-00 00:00:00'){
            $publicLastCheckDate = null;
        }
        //check the conditions and map data to database
        if(!is_null($domainBlacklistData['cyren_url_categories'])){
            $cyrenUrlCategories = implode (", ", $domainBlacklistData['cyren_url_categories']);
        }
        if(!is_null($domainBlacklistData['public_bls_listed'])){
            $publicBlsListed = implode (", ", $domainBlacklistData['public_bls_listed']);
        }
        if($domainBlacklistData['cyren_blacklisted'] === 'true'){
            $cyrenBlacklisted = 1;
        }
        if($domainBlacklistData['public_bls_blacklisted'] === 'true'){
            $publicBlsBlacklisted = 1;
        }



        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':domains_id', $domainId);
            $stmt->bindParam(':cyren_blacklisted', $cyrenBlacklisted);
            $stmt->bindParam(':cyren_last_check', $cyrenLastCheckDate);
            $stmt->bindParam(':cyren_url_categories', $cyrenUrlCategories);
            $stmt->bindParam(':public_bls_blacklisted', $publicBlsBlacklisted);
            $stmt->bindParam(':public_bls_last_check', $publicLastCheckDate);
            $stmt->bindParam(':public_bls_listed', $publicBlsListed);
            $stmt->bindParam(':created_by', $this->currentUserId);
            $stmt->bindParam(':updated_by', $this->currentUserId);
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                return null;
            }
            $insertedId = $this->conn->lastInsertId();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $insertedId;
    }

    /**
     * @param int $domainId
     * @return bool
     */
    public function checkIfDomainBlacklistEntryExists(int $domainId):bool
    {
        $select = 'SELECT 1 FROM domain_blacklists WHERE domains_id = :domain_id';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':domain_id', $domainId);
            $stmt->execute();
            $result = $stmt->fetch();
            if (empty($result)) {
                return false;
            }
            return true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }
}
