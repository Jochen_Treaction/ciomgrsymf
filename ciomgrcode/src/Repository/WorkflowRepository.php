<?php


namespace App\Repository;


use App\Services\AuthenticationService;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\ParameterType;
use Laminas\Code\Exception\RuntimeException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @property LoggerInterface logger
 * @property Connection conn
 * @property int|mixed userId
 */
class WorkflowRepository
{
    public function __construct(
        Connection $conn,
        LoggerInterface $logger,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService
    )
    {
        $this->conn = $conn;
        $this->logger = $logger;
        //set user
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);
        $this->userId = (!empty($user) && !empty($user['user_id'])) ? $user['user_id'] : 1;

    }

    /**
     * @param int $objectRegisterId
     * @param int $page_webhook_campaign_id
     * @param string $name
     * @return int|null
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function insert(
        int $objectRegisterId,
        int $page_webhook_campaign_id,
        string $name
    ): ?int
    {
        $id = null;

        $description = "This is workflow for campaign_id " . $page_webhook_campaign_id;

        if ($objectRegisterId <= 0 || $page_webhook_campaign_id <= 0) {
            throw new RuntimeException('Invalid parameters provided');
        }

        $sql = 'INSERT INTO workflow(objectregister_id,page_webhook_campaign_id,name,description,created_by,updated_by)
                    VALUE(:objectregister_id,:page_webhook_campaign_id,:name,:description,:created_by,:updated_by)';

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':objectregister_id', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindParam(':page_webhook_campaign_id', $page_webhook_campaign_id, ParameterType::INTEGER);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':created_by', $this->userId);
            $stmt->bindParam(':updated_by', $this->userId);
            $result = $stmt->execute();
            if ($result === true) {
                $id = $this->conn->lastInsertId();
            }
            return $id;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

    }

    /**
     * @param int $id
     * @return array|null
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function get(int $id): ?array
    {
        //basic check
        if ($id <= 0) {
            throw new RuntimeException('Invalid parameters provided');
        }

        $sql = 'select * from workflow where id=:id';

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':id', $id, ParameterType::INTEGER);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    public function update(
        int $id
    ): ?bool {

    }

    /**
     * @param int $oldCampaignId
     * @param int $newCampaignId
     * @return bool|null
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Pradeep
     * @internal after campaign is revised , new campaign id will be updated to workflow table.
     * @deprecated
     */
    public function updatePageWebhookCampaignId(int $oldCampaignId, int $newCampaignId): bool
    {
        if ($oldCampaignId == 0 || $newCampaignId == 0) {
            return false;
        }
        if ($oldCampaignId === $newCampaignId) {
            return true;
        }
        $sql = 'update workflow set `page_webhook_campaign_id` = :new_campaign_id where `page_webhook_campaign_id` = :old_campaign_id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':new_campaign_id', $newCampaignId);
        $stmt->bindValue(':old_campaign_id', $oldCampaignId);
        return $stmt->execute();
    }

    public function delete(int $id): ?bool
    {

    }

    /**
     * @param $user_processhook_id
     * @param $workflow_id
     * @param $predecessor_uph_id
     * @param $successor_uph_id
     * @return bool|null
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function insertUserProcesshookHasWorkflow(
        $user_processhook_id,
        $workflow_id,
        $predecessor_uph_id,
        $successor_uph_id
    ): ?bool
    {
        if ($user_processhook_id <= 0 || $workflow_id <= 0) {
            throw new RuntimeException('Invalid parameters provided');
        }

        $sql = 'INSERT INTO user_processhook_has_workflow(user_processhook_id,workflow_id,predecessor_uph_id,successor_uph_id)
                    VALUE(:user_processhook_id,:workflow_id,:predecessor_uph_id,:successor_uph_id)';

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':user_processhook_id', $user_processhook_id, ParameterType::INTEGER);
            $stmt->bindParam(':workflow_id', $workflow_id, ParameterType::INTEGER);
            $stmt->bindParam(':predecessor_uph_id', $predecessor_uph_id, ParameterType::INTEGER);
            $stmt->bindParam(':successor_uph_id', $successor_uph_id, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result === true) {
                return true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

    }

    public function getActiveProcesshooksInfo(int $userProcesshookId): ?array
    {
        if ($userProcesshookId <= 0) {
            throw new RuntimeException('Invalid parameters provided');
        }

        $sql = "SELECT c.name as activeOn,w.created as date,w.name as action,uot.unique_object_name as type
                    FROM user_processhook_has_workflow uphw
                    JOIN workflow w on uphw.workflow_id = w.id
                    JOIN campaign c on c.id = w.page_webhook_campaign_id
                    JOIN objectregister o on c.objectregister_id = o.id
                    JOIN statusdef s on o.statusdef_id = s.id
                    JOIN unique_object_type uot on o.unique_object_type_id = uot.id
                    WHERE uphw.user_processhook_id = :user_processhook_id and s.value = 'active'";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':user_processhook_id', $userProcesshookId, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result === true) {
                return $stmt->fetchAll();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

}