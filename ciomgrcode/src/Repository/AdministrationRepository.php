<?php


namespace App\Repository;

use App\Entity\Campaign;
use App\Entity\AdministratesCompanies;
use App\Entity\AdministratesUsers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Exception;
use Psr\Log\LoggerInterface;
use Doctrine\DBAL\ParameterType;

/**
 * Class AdministrationRepository all and only SuperAdmin stuff goes here
 * @package App\Repository
 * @author jsr
 */
class AdministrationRepository extends ServiceEntityRepository
{

    protected $logger;
    protected $conn;

    public function __construct(Connection $connection, ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Campaign::class);
        $this->logger = $logger;
        $this->conn = $connection;
    }

    // DELETE SECTION (ONLY FOR SUPER_ADMINS)
    // Delete user, and >>> ALL (!) <<< assigned user data: including his company, all campaigns, leads, lead data, surveys, ..., all other users of the users company!
    // START >>>


    /**
     * @param int $id
     * @return array
     */
    public function sqlStatement(int $id): array
    {
        $this->conn->beginTransaction();
        $sql = 'DELETE FROM users WHERE id=:id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam('id', $id);
        $result = $stmt->execute();
        // $result =  $stmt->fetchAll();

        $rowCount = $stmt->rowCount();
        if ($result && $rowCount > 0) {
            $this->conn->commit();
        } else {
            $this->logger->error(json_encode($this->conn->errorInfo()), [__METHOD__, __LINE__]);
            $this->conn->rollBack();
        }

        return $result;
    }


    /**
     * @param int $company_id
     * @return array|null
     */
    public function getObjectRegisterIdsFromAllTablesByCompanyId( int $company_id ):?array
    {
        $objectRegisterIds = [];

        try {
            $stmt = $this->conn->prepare(
                'SELECT c.objectregister_id objectregister_id
                    FROM company c
                    WHERE id = :company_id
                    UNION
                    SELECT cp.objectregister_id objectregister_id
                    FROM campaign cp
                    WHERE cp.company_id = :company_id
                    UNION
                    SELECT cf.objectregister_id objectregister_id
                    FROM customfields cf
                    JOIN campaign c2 ON cf.campaign_id = c2.id AND c2.company_id = :company_id
                    UNION
                    SELECT l.objectregister_id objectregister_id
                    FROM leads l
                    JOIN campaign c3 on l.campaign_id = c3.id AND c3.company_id = :company_id
					UNION 
					SELECT itg.objectregister_id objectregister_id 
					FROM integrations itg
					WHERE itg.company_id = :company_id
					UNION
					SELECT itg2.objectregister_id objectregister_id 
					FROM integrations itg2
					JOIN campaign c3 ON itg2.campaign_id = c3.id AND c3.company_id = :company_id
					UNION 
					SELECT uph.objectregister_id objectregister_id 
					FROM company c
					JOIN campaign cp on c.id = cp.company_id
					JOIN user_processhook uph on cp.id = uph.project_campaign_id 
					WHERE c.id = :company_id
					UNION
					SELECT s.objectregister_id objectregister_id 
					FROM campaign cp 
					JOIN survey s on cp.id = s.campaign_id
					WHERE cp.company_id = :company_id
					UNION
					SELECT w.objectregister_id objectregister_id 
					FROM campaign cp 
					JOIN workflow w on cp.id = w.page_webhook_campaign_id
					WHERE cp.company_id = :company_id
					UNION
					SELECT e.objectregister_id objectregister_id 
					FROM externaldata_queue e 
					WHERE e.company_id = :company_id
					UNION
					SELECT a.objectregister_id objectregister_id 
					FROM advertorials a 
					WHERE a.company_id = :company_id
					UNION
					SELECT sp.objectregister_id objectregister_id 
					FROM seed_pool sp 
					WHERE sp.company_id = :company_id
					UNION
					SELECT DISTINCT ibx.objectregister_id objectregister_id 
					FROM inbox_monitor ibx
					JOIN seed_pool sp on sp.id = ibx.seed_pool_id AND sp.company_id = :company_id'
				 );
            $stmt->bindParam(':company_id', $company_id, ParameterType::INTEGER);
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);
            foreach ($temp as $record) {
                $objectRegisterIds[] = (int)$record['objectregister_id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $objectRegisterIds;
    }

    /*
     * SELECT c.objectregister_id objectregister_id
FROM company c
WHERE id = 16
UNION
SELECT cp.objectregister_id objectregister_id
FROM campaign cp
WHERE cp.company_id = 16
UNION
SELECT cf.objectregister_id objectregister_id
FROM customfields cf
JOIN campaign c2 ON cf.campaign_id = c2.id AND c2.company_id = 16
UNION
SELECT l.objectregister_id objectregister_id
FROM leads l
JOIN campaign c3 on l.campaign_id = c3.id AND c3.company_id = 16;
     */



    /**
     * @param string $email
     * @return int $company_id
     */
    public function getCompanyIdOfUser(string $email): int
    {
        $company_id = $this->conn->fetchColumn('SELECT company_id FROM users WHERE email = ?', array($email), 0);
        $this->logger->info("getCompanyIdOfUser = $company_id", [__METHOD__, __LINE__]);
        return $company_id;
    }


    /**
     * @param string $email
     * @return int $company_id
     */
    public function getUserId(string $email): int
    {
        $id = $this->conn->fetchColumn('SELECT id FROM users WHERE email = ?', array($email), 0);
        $this->logger->info("getUserId = $id", [__METHOD__, __LINE__]);
        return $id;
    }


    /**
     * getCountUsersOfCompanyId
     * @param string $email
     * @return int
     */
    public function getCountUsersOfCompanyId(string $email): int
    {
        $company_id = $this->getCompanyIdOfUser($email);
        $count = $this->conn->fetchColumn('SELECT count(*) FROM users WHERE company_id = ?', array($company_id), 0);
        $this->logger->info("getCountUsersOfCompanyId = $count", [__METHOD__, __LINE__]);
        return $count;
    }


    public function currentUserBelongsToEmailsCompany(string $userToDeleteEmail):bool
    {
        return false; // TODO: code must be added!!!
    }

    /**
     * @param int $company_id
     * @return array $campaign_id_list
     */
    public function getCompanyCampaignIds(int $company_id): array
    {
        $campaign_id_list = [];
        try {
            $stmt = $this->conn->prepare('SELECT id FROM campaign WHERE company_id = :company_id');
            $stmt->bindParam('company_id', $company_id);
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            foreach ($temp as $campaign) {
                $campaign_id_list[] = (int)$campaign['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $campaign_id_list;
    }


    /**
     * @param int $company_id
     * @return array $users_id_list
     */
    public function getUsersByCompanyId(int $company_id): array
    {
        $users_id_list = [];
        try {
            $stmt = $this->conn->prepare('SELECT DISTINCT id FROM (SELECT id FROM users WHERE company_id = :company_id UNION SELECT invited_user_id AS id FROM administrates_users WHERE to_company_id = :company_id) AS idList') ;
            $stmt->bindParam('company_id', $company_id);
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            foreach ($temp as $users) {
                $users_id_list[] = (int)$users['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        $this->logger->info('$users_id_list', [$users_id_list, __METHOD__,__LINE__]);
        return $users_id_list;
    }

    public function getUsersByCompanyIdList(array $company_id_list): array
    {
        $list = ' ('.trim(implode(',', $company_id_list), ',').' )';
        return $list;
    }


    /**
     * @param array $campaign_id_list
     * @return array $leads_id_list
     */
    public function getLeadsByCampaignIds(array $campaign_id_list): array
    {
        $leads_id_list = [];
		$list = implode(',', $campaign_id_list);
        try {
            $stmt = $this->conn->prepare("SELECT id FROM leads WHERE campaign_id IN ({$list})");
            // $stmt->bindValue(1, implode(',', $campaign_id_list), ParameterType::STRING);
            $stmt->execute();
            $temp = $stmt->fetchAll();

            foreach ($temp as $record) {
                $leads_id_list[] = (int)$record['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $leads_id_list;
    }



    /**
     * getQuestionsBySurveyIds
     * @param array $survey_id_list
     * @return array $leads_id_list
     */
    public function getQuestionsIdsBySurveyIds(array $survey_id_list): array
    {
        $leads_id_list = [];
        try {
            $stmt = $this->conn->prepare('SELECT id FROM questions WHERE survey_id IN (?)');
            $stmt->bindValue(1, implode(',', $survey_id_list), ParameterType::STRING);
            $stmt->execute();
            $temp = $stmt->fetchAll();

            foreach ($temp as $record) {
                $leads_id_list[] = (int)$record['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $leads_id_list;
    }


    /**
     * getSurveyIds
     * @param int $company_id
     * @return array $survey_id_list
     */
    public function getSurveyIds(int $company_id): array
    {
        $survey_id_list = [];

        $select =
            'SELECT s.id 
            FROM survey s
            JOIN campaign cp ON cp.id = s.campaign_id
            JOIN company c ON c.id = cp.company_id AND c.id = :company_id';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam('company_id', $company_id, ParameterType::INTEGER);
            $stmt->execute();
            $temp = $stmt->fetchAll();

            foreach ($temp as $campaign) {
                $survey_id_list[] = (int)$campaign['survey_id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $survey_id_list;
    }


    /**
     * @param int $company_id
     * @return array $integrations_id_list
     */
    public function getCompanyIntegrationsIds(int $company_id): array
    {
        $integrations_id_list = [];
        try {
            $stmt = $this->conn->prepare('SELECT distinct id FROM integrations WHERE company_id = :company_id');
            $stmt->bindParam('company_id', $company_id, ParameterType::INTEGER);
            $stmt->execute();
            $temp = $stmt->fetchAll();

            foreach ($temp as $integrations) {
                $integrations_id_list[] = (int)$integrations['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $integrations_id_list;
    }

	/**
	 * @param int $company_id
	 * @return array $integrations_id_list
	 */
	public function getCampaignIntegrationsIds(array $campaign_id_list): array
	{
		$integrations_id_list = [];
		$list = ' ('.trim(implode(',', $campaign_id_list), ',').' )';

		try {
			$stmt = $this->conn->prepare("SELECT DISTINCT id FROM integrations WHERE campaign_id in {$list}");
			// $stmt->bindParam('campaign_id', $campaign_id, ParameterType::INTEGER);
			$stmt->execute();
			$temp = $stmt->fetchAll();

			foreach ($temp as $integrations) {
				$integrations_id_list[] = (int)$integrations['id'];
			}
		} catch (Exception $e) {
			$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
		}
		return $integrations_id_list;
	}


    /**
     * deleteSecurelog
     * @param int $user_id
     * @return int $deletedRecords
     */
    public function deleteSecurelog(int $user_id): int
    {
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare('DELETE FROM securelog WHERE user_id = :user_id AND id > 0');
            $stmt->bindParam('user_id', $user_id, ParameterType::INTEGER);
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteSecurelogByUsersIdList
     * @param array $users_id_list
     * @return int
     */
    public function deleteSecurelogByUsersIdList(array $users_id_list): int
    {
        $list = ' ('.trim(implode(',', $users_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM securelog WHERE user_id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteIntegrationsMeta
     * @param array $integrations_id_list
     * @return int $deletedRecords
	 * @deprecated table does not exist anymore
     */
    public function deleteIntegrationsMeta(array $integrations_id_list): int
    {
        $list = ' ('.trim(implode(',', $integrations_id_list), ',').' )';
        $deletedRecords = 0;

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM integrations_meta WHERE integrations_id IN $list AND id > 0");
            $stmt->execute();

            $deletedRecords = $stmt->rowCount();

            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteIntegrations
     * @param int $company_id
     * @return int $deletedRecords
     */
    public function deleteIntegrations(int $company_id): int
    {
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare('DELETE FROM integrations WHERE company_id = :company_id  AND id > 0');
            $stmt->bindValue('company_id', $company_id, ParameterType::INTEGER);
            $stmt->execute();
            $deletedRecords = $stmt->rowCount();

            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


	/**
	 * @param array $integrations_id_list
	 * @return int
	 */
	public function deleteIntegrationsByIdList(array $integrations_id_list): int
	{
		$list = trim(implode(',', $integrations_id_list), ',');
		$deletedRecords = 0;
		try {
			$stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
			$stmt->execute();
		} catch (Exception $e) {
			$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
		}

		try {
			$stmt = $this->conn->prepare("DELETE FROM integrations WHERE id in ({$list})" );
			// $stmt->bindValue('company_id', $company_id, ParameterType::INTEGER);
			// $stmt->execute();
			$deletedRecords = $stmt->rowCount();

			$stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
			$stmt->execute();

		} catch (Exception $e) {
			$this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
		}
		return $deletedRecords;
	}


    /**
     * deleteFraud
     * @param array $campaign_id_list
     * @return int $deletedRecords
     */
    public function deleteFraud(array $campaign_id_list): int
    {
        $list = ' ('.trim(implode(',', $campaign_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }


        try {
            $stmt = $this->conn->prepare("DELETE FROM fraud WHERE campaign_id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();

            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteCampaignParams
     * @param array $campaign_id_list
     * @return int $deletedRecords
     * @deprecated table campaign_params does not exist anymore
     */
    public function deleteCampaignParams(array $campaign_id_list): int
    {
        $list = ' ('.trim(implode(',', $campaign_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM campaign_params WHERE campaign_id IN $list AND id > 0");
            $stmt->execute();

            $deletedRecords = $stmt->rowCount();

            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteLeadCustomfieldContent
     * @param array $leads_id_list
     * @return int $deletedRecords
     */
    public function deleteLeadCustomfieldContent(array $leads_id_list): int
    {
        $list = ' ('.trim(implode(',', $leads_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM lead_customfield_content WHERE leads_id IN $list");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteCustomfields
     * @param array $campaign_id_list
     * @return int $deletedRecords
     */
    public function deleteCustomfields(array $campaign_id_list): int
    {
        $list = ' ('.trim(implode(',', $campaign_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM customfields WHERE campaign_id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteLeadAnswerContent
     * @param array $leads_id_list
     * @return int $deletedRecords
     */
    public function deleteLeadAnswerContent(array $leads_id_list): int
    {
        $list = ' ('.trim(implode(',', $leads_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM lead_answer_content WHERE leads_id IN $list");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteCustomerJourney
     * @param array $leads_id_list
     * @return int $deletedRecords
     */
    public function deleteCustomerJourney(array $leads_id_list): int
    {
        $list = ' ('.trim(implode(',', $leads_id_list), ',').' )';
        $deletedRecords = 0;

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM customer_journey WHERE lead_id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * delete searchleads by leads.id
     * @param array $leads_id_list
     * @return int $deletedRecords
     */
    public function deleteSearchLeads(array $leads_id_list): int
    {
        $list = ' ('.trim(implode(',', $leads_id_list), ',').' )';
        $deletedRecords = 0;

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM searchleads WHERE id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteQuestions
     * @param array $questions_id_list
     * @return int $deletedRecords
     */
    public function deleteImages(array $questions_id_list): int
    {
        $list = ' ('.trim(implode(',', $questions_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }


        try {
            $stmt = $this->conn->prepare("DELETE FROM images WHERE question_id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteQuestions
     * @param array $survey_id_list
     * @return int $deletedRecords
     */
    public function deleteQuestions(array $survey_id_list): int
    {
        $list = ' ('.trim(implode(',', $survey_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM questions WHERE survey_id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    ////////  ------------------  ////////
    ////////  DELETE MAIN TABLES  ////////
    ////////  ------------------  ////////

    /**
     * deleteLeads => deletes cascading to objectregister
     * @param array $leads_id_list
     * @return int $deletedRecords
     */
    public function deleteLeads(array $leads_id_list): int
    {
        $list = ' ('.trim(implode(',', $leads_id_list), ',').' )';
        $deletedRecords = 0;

//        try {
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
//            $stmt->execute();
//        } catch (Exception $e) {
//            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
//        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM leads WHERE id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
//            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * @param array $objectRegisterIds
     * @return int|null
     */
    public function deleteObjectRegister(array $objectRegisterIds):?int
    {
        $list = ' ('.trim(implode(',', $objectRegisterIds), ',').' )';
        $deletedRecords = 0;

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON"); //   ON when objectregister is deleted, all depending entries in related tables must be deleted also
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM objectregister WHERE id IN $list");
            $stmt->execute();
            $deletedRecords = $stmt->rowCount();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $deletedRecords;
    }



    /**
     * deleteSurvey
     * @param array $survey_id_list
     * @return int $deletedRecords
     */
    public function deleteSurvey(array $survey_id_list): int
    {
        $list = ' ('.trim(implode(',', $survey_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM survey WHERE id IN $list AND id > 0");
            $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteCampaign
     * @param array $survey_id_list
     * @return int $deletedRecords
     */
    public function deleteCampaign(array $campaign_id_list): int
    {
        $list = ' ('.trim(implode(',', $campaign_id_list), ',').' )';
        $deletedRecords = 0;

//        try {
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
//            $stmt->execute();
//        } catch (Exception $e) {
//            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
//        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM campaign WHERE id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
//            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }

    // todo: get and delete all companies from user

    /**
     * deleteAdministratesCompaniesByCompanyId
     * @param int $company_id
     * @return int
     */
    public function deleteAdministratesCompaniesByCompanyId(int $company_id)
    {
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare('DELETE FROM administrates_companies WHERE company_id = :company_id AND id > 0');
            $stmt->bindValue('company_id', $company_id, ParameterType::INTEGER);
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteAdministratesUsersByCompanyId
     * @param int $company_id
     * @return int
     */
    public function deleteAdministratesUsersByCompanyId(int $company_id)
    {
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare('DELETE FROM administrates_users WHERE to_company_id = :company_id AND id > 0');
            $stmt->bindValue('company_id', $company_id, ParameterType::INTEGER);
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * @param array $users_id_list
     * @param int   $to_company_id
     * @return int
     */
    public function deleteAdministratesUsersByInvitedUserIds(array $users_id_list, int $to_company_id): int
    {
        $deletedRecords = 0;
        if(empty($users_id_list)) {
            return $deletedRecords;
        }

        $list = ' ('.trim(implode(',', $users_id_list), ',').' )';
        $deletedRecords = 0;
        $this->logger->info('$users_id_list', [$users_id_list, __FUNCTION__, __LINE__]);
        $this->logger->info('$to_company_id', [$to_company_id, __FUNCTION__, __LINE__]);
        $this->logger->info('$list', [$list, __FUNCTION__, __LINE__]);

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM administrates_users WHERE invited_user_id IN $list AND to_company_id = :to_company_id AND id > 0");
            $stmt->bindValue('to_company_id', $to_company_id, ParameterType::INTEGER);
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteCompany
     * @param int $company_id
     * @return int $deletedRecords
     */
    public function deleteCompany(int $company_id): int
    {
        $deletedRecords = 0;

//        try {
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
//            $stmt->execute();
//        } catch (Exception $e) {
//            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
//        }

        try {
            $stmt = $this->conn->prepare('DELETE FROM company WHERE id = :company_id  AND id > 0');
            $stmt->bindValue('company_id', $company_id, ParameterType::INTEGER);
            $stmt->execute();
            $deletedRecords = $stmt->rowCount();
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
//            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }



    /**
     * delete externaldata_queue
     * @param  int $company_id
     * @return int $deletedRecords
     */
    public function deleteExternalDataQueue(int $company_id): int
    {
        $deletedRecords = 0;

//        try {
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
//            $stmt->execute();
//        } catch (Exception $e) {
//            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
//        }

        try {
            $stmt = $this->conn->prepare('DELETE FROM externaldata_queue WHERE company_id = :company_id  AND id > 0');
            $stmt->bindValue('company_id', $company_id, ParameterType::INTEGER);
            $stmt->execute();
            $deletedRecords = $stmt->rowCount();
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
//            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }



    /**
     * deleteUser
     * @param int $id
     * @return int $deletedRecords
     */
    public function deleteUser(int $id): int
    {
        $deletedRecords = 0;

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON"); // ON to ensure delete depending users_meta by fk cascade
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare('DELETE FROM users WHERE id = :id');
            $stmt->bindValue('id', $id, ParameterType::INTEGER);
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * deleteUserList
     * @param array $users_id_list
     * @return int
     */
    public function deleteUsersByIdList(array $users_id_list): int
    {
        $list = ' ('.trim(implode(',', $users_id_list), ',').' )';
        $deletedRecords = 0;
//        try {
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
//            $stmt->execute();
//        } catch (Exception $e) {
//            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
//        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM users WHERE id IN $list AND id > 0");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
//            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
//            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * @param array $advertorials_id_list
     * @return int
     */
    public function  deleteAdvertorials(array $advertorials_id_list):int
    {
        $list = ' ('.trim(implode(',', $advertorials_id_list), ',').' )';
        $deletedRecords = 0;

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM advertorials WHERE id IN $list");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * @param array $seedpool_id_list
     * @return int
     */
    public function deleteSeedpools(array $seedpool_id_list):int
    {
        $list = ' ('.trim(implode(',', $seedpool_id_list), ',').' )';
        $deletedRecords = 0;

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM seed_pool WHERE id IN $list");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * @param array $inboxmonitor_id_list
     * @return int
     */
    public function deleteInboxMonitors(?array $inboxmonitor_id_list):int
    {
        if(empty($inboxmonitor_id_list)) {
            return 0;
        }

        $list = ' ('.trim(implode(',', $inboxmonitor_id_list), ',').' )';
        $deletedRecords = 0;

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM inbox_monitor WHERE id IN $list");
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    public function deleteUsersByIdListAndCompanyId(array $users_id_list, int $company_id): int
    {
        $list = ' ('.trim(implode(',', $users_id_list), ',').' )';
        $deletedRecords = 0;

        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM users WHERE id IN $list AND company_id = :company_id AND id > 0");
            $stmt->bindValue('company_id', $company_id, ParameterType::INTEGER);
            $res = $stmt->execute();
            $deletedRecords = $stmt->rowCount();
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     * @param int $company_id
     * @return array [id1,id2,...]
     */
    public function getUsersByCompanyIdDelAll(int $company_id):array
    {
        $ret = [];
        $sql = 'SELECT 
                u.id 
            FROM users u 
            JOIN (SELECT invited_user_id, COUNT(*) FROM administrates_users au GROUP BY 1 HAVING COUNT(*) = 1) au ON (au.invited_user_id = u.id)
            WHERE u.company_id = :company_id';

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('company_id', $company_id);
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);
            foreach ($temp as $t) {
                $ret[] = $t['id'];
            }
            $this->logger->info('getUsersByCompanyIdDelAll', [$ret, __METHOD__, __LINE__]);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $ret;
    }


    /**
     * @param int $company_id
     * @return array|null
     */
    public function getAdvertorialIdsByCompanyId(int $company_id):?array
    {
        $ret = null;
        $select = 'SELECT id FROM advertorials WHERE company_id = :company_id';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam('company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $temp = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $ids=[];
            if($stmt->rowCount() >0 ) {
                foreach ($temp as $r) {
                    $ids[] = (int)$r['id'];
                }
                $ret = $ids;
            }
            $this->logger->info('getAdvertorialIdsByCompanyId', [$ret, __METHOD__, __LINE__]);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $ret;
    }


    /**
     * @param int $company_id
     * @return array|null
     */
    public function getSeedPoolIdsByCompanyId(int $company_id):?array
    {
        $ret = null;
        $select = 'SELECT id FROM seed_pool WHERE company_id = :company_id';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam('company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $temp = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $ids=[];
            if($stmt->rowCount() >0 ) {
                foreach ($temp as $r) {
                    $ids[] = (int)$r['id'];
                }
                $ret = $ids;
            }
            $this->logger->info('getAdvertorialIdsByCompanyId', [$ret, __METHOD__, __LINE__]);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $ret;
    }


    /**
     * @param $seedpool_id_list
     * @return array|null
     */
    public function getInboxMonitorIdsBySeedPoolIds(?array $seedpool_id_list):?array
    {
        if(empty($seedpool_id_list)) {
            return null;
        }

        $implode = implode(',', $seedpool_id_list);
        $list = ' ('.trim($implode, ',').' )';
        $this->logger->info('$seedpool_id_list', [$list, __METHOD__,__LINE__]);
        $ret = null;
        $select = "SELECT id FROM inbox_monitor WHERE seed_pool_id IN {$list}";
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->execute();
            $temp = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $ids=[];
            if($stmt->rowCount() >0 ) {
                foreach ($temp as $r) {
                    $ids[] = (int)$r['id'];
                }
                $ret = $ids;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $ret;
    }



    /**
     * @param int $company_id
     * @return array
     */
    public function getUsersAndCompanyByCompanyIdForUpdate(int $company_id):array
    {
        $ret = [];
        $sql = 'SELECT id AS user_id, MIN(to_company_id) AS company_id
                FROM  
                (SELECT 
                    u.id,
                    au.to_company_id
                FROM users u 
                JOIN (SELECT invited_user_id, to_company_id FROM administrates_users au WHERE to_company_id != :company_id ) au ON (au.invited_user_id = u.id)
                WHERE u.company_id = :company_id) AS list
                GROUP BY 1';

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('company_id', $company_id);
            $stmt->execute();
            $ret = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            $this->logger->info('getUsersAndCompanyByCompanyIdForUpdate', [$ret, __METHOD__, __LINE__]);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $ret;
    }


    /**
     * @param array $userIdCompanyId [['user_id' => u0, 'company_id' => c0], ['user_id' => u1, 'company_id' => c1], ...]
     * @return int updated records
     */
    public function updateUsersCompany(array $userIdCompanyId):int
    {
        $updatedRecords = 0;
        $sql = 'UPDATE users SET company_id = :company_id WHERE id = :user_id';
        try {
            foreach ($userIdCompanyId as $u) {
                $user_id = $u['user_id'];
                $company_id = $u['company_id'];
                $stmt = $this->conn->prepare($sql);
                $stmt->bindParam('user_id', $user_id, ParameterType::INTEGER);
                $stmt->bindParam('company_id', $company_id, ParameterType::INTEGER);
                $res = $stmt->execute();

                if($res) {
                    $updatedRecords += $stmt->rowCount();
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $updatedRecords;
    }


    public function markAccountForDeletion(string $email, $delete_at)
    {
        $insertIntoObjectRegisterMeta = "";
    }

    /**
     * @param array|null $objectregister_id_list
     * @return int
     */
    public function deleteObjectRegisterMeta(?array $objectregister_id_list):int
    {
        $list = ' ('.trim(implode(',', $objectregister_id_list), ',').' )';
        $deletedRecords = 0;
        try {
            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=OFF");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $stmt = $this->conn->prepare("DELETE FROM object_register_meta WHERE object_register_id IN $list AND id > 0");
            $stmt->execute();

            $deletedRecords = $stmt->rowCount();

            $stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
            $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;

    }


	/**
	 * @return bool
	 */
    public function enableForeignKeyContraintsExplicitly():bool
	{
		$stmt = $this->conn->prepare("SET SESSION foreign_key_checks=ON");
		return $stmt->execute();
	}


	public function getMioAioApikey():string {
		$stmt = $this->conn->prepare( "SELECT UUID() as apikey");
		$stmt->execute();
		return $stmt->fetchColumn();
	}

}
