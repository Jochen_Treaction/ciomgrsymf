<?php


namespace App\Repository;


use App\Types\UserMetaTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\ParameterType;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * @property LoggerInterface logger
 * @property Connection conn
 */
class UserMetaRepository
{

    /* These are the meta_key for the UserMeta.     */
	// TODO: use UserMetaTypes instead of defining const here
    public const USER_REG_FIRSTSTEP = 'Registration_FirstStep';
    public const USER_REG_SECONDSTEP = 'Registration_SecondStep';
    public const USER_REG_THIRDSTEP = 'Registration_ThirdStep';

    /**
     * UserMetaRepository constructor.
     * @param LoggerInterface $logger
     * @param Connection $connection
     */
    public function __construct(LoggerInterface $logger, Connection $connection)
    {
        $this->logger = $logger;
        $this->conn = $connection;
    }

    /**
     * @param int $id
     * @param int $userId
     * @param string $metaKey
     * @param string $metaValue
     * @return bool|null
     * @author Pradeep
     */
    public function update(int $id, int $userId, string $metaKey, string $metaValue): ?bool
    {

    }

    /**
     * @param $id
     * @return array|null
     * @author Pradeep
     */
    public function get($id): ?array
    {
        $userMeta = [];
        if ($id <= 0) {
            throw new RuntimeException('Invalid UserMetaID provided');
        }
        return $userMeta;
    }

    /**
     * @param int $userId
     * @param string $metaKey
     * @param string $metaValue
     * @param int $createdBy
     * @param int $updatedBy
     * @return int|null
     * @author Pradeep
     */
    public function insert(
        int $userId,
        string $metaKey,
        string $metaValue,
        int $createdBy = 1,
        int $updatedBy = 1
    ): ?int {
        $userMetaId = null;
        try {
            if ($userId <= 0 || empty($metaKey) || empty($metaValue)) {
                throw new RuntimeException('Invalid UserId or MetaKey Or MetaValue provided');
            }
            $sql = 'insert into users_meta(users_id, meta_key, meta_value, created_by, updated_by)
                    value(:users_id, :meta_key, :meta_value, :created_by, :updated_by)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':users_id', $userId);
            $stmt->bindParam(':meta_key', $metaKey);
            $stmt->bindParam(':meta_value', $metaValue);
            $stmt->bindParam(':created_by', $createdBy);
            $stmt->bindParam(':updated_by', $updatedBy);
            $result = $stmt->execute();
            if ($result === true) {
                $userMetaId = $this->conn->lastInsertId();
            }
        } catch (Exception | \Doctrine\DBAL\Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $userMetaId;
    }

    /**
     * @param int $userId
     * @param string $metaKey
     * @return array|null
     * @author Pradeep
     */
    public function getUserMetaValue(int $userId, string $metaKey): ?array
    {
        $userMeta = [];
        try {
            if ($userId <= 0 || empty($metaKey)) {
                throw new RuntimeException('Invalid UserId or MetaKey provided');
            }
            $sql = 'SELECT * FROM users_meta WHERE users_id =:users_id AND meta_key =:meta_key';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':users_id', $userId, ParameterType::INTEGER);
            $stmt->bindParam(':meta_key', $metaKey, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAssociative();
            if (!empty($result) && !empty($result[ 'meta_value' ])) {
                $userMeta = json_decode($result[ 'meta_value' ], true, 512, JSON_THROW_ON_ERROR);
            }
            return $userMeta;
        } catch (RuntimeException | JsonException | \Doctrine\DBAL\Exception | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }



	/**
	 * returns "AIO" or "MIO" or null in case of error or record not found
	 * @param int $user_id
	 * @return string|null
	 * @author jsr
	 */
	public function getUserRegistrationOrigin(int $user_id): ?string
	{
		$selectUsermeta = "SELECT um.meta_value FROM users u JOIN users_meta um on u.id = um.users_id AND um.meta_key =:meta_key WHERE u.id=:user_id";

		$metaKey = UserMetaTypes::REGISTRATION_FIRSTSTEP;

		try {
			$stmt = $this->conn->prepare($selectUsermeta);
			$stmt->bindParam(':meta_key', $metaKey, ParameterType::STRING);
			$stmt->bindParam(':user_id', $user_id, ParameterType::INTEGER);
			$stmt->execute();
			$metaValueJson = $stmt->fetchColumn();
			$this->logger->info('$metaValueJson', [$user_id, $metaKey, $metaValueJson, __METHOD__,__LINE__]);

			if ($stmt->rowCount() > 0) {
				$metaValueArray = json_decode($metaValueJson, true, 512, JSON_OBJECT_AS_ARRAY);
				return (!empty($metaValueArray['source'])) ? $metaValueArray['source'] : null; // "AIO" or "MIO"
			}
		} catch (Exception $e) {
			$this->logger->error('Exception', [$e->getMessage(), __METHOD__,__LINE__]);
		}

		return null;
	}
}
