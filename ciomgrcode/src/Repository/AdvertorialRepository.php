<?php

namespace App\Repository;


use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Psr\Log\LoggerInterface;
use Exception;

/**
 * <b>CRUD for tables</b>
 * - advertorials
 * - advertorial_images
 */
class AdvertorialRepository
{

    const DEFAULT_USER_ID = 99999;
    protected int $currentUserId;
    protected array $currentUser;
    protected Connection $conn;
    protected LoggerInterface $logger;


    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->conn = $connection;
        $this->logger =$logger;
        $this->currentUserId = self::DEFAULT_USER_ID;
    }


    /**
     * set class <b>global current user id</b>, if before you run <b style="color:blue"><u>inserts</u> or <u>updates</u></b>, otherwise a default user id (99999) is used
     * @param int $user_id users.id
     */
    public function setRepostitoriesCurrentUserId(int $user_id)
    {
        $this->currentUserId = $user_id;
    }


    /**
     * add a new Advertorial
     * @param int    $objectRegisterId
     * @param int    $companyId
     * @param int    $seedPoolGroupId
     * @param string $advertorialName
     * @param string $advertorialDescription
     * @param string $advertorialBeginDate
     * @param string $advertorialEndDate
     * @param string $fileNameHtml
     * @param string $contentUploadedFile
     * @param string $contentExtractImages
     * @param string $contentInlineCss
     * @param string $contentCleanHtml
     * @param string $fileNameZip
     * @param string $contentUploadedZipFile
     * @return int last advertorials.id inserted, if record was inserted, 0, if record was present or an error occurred
     */
    public function setAdvertorial(
        int $objectRegisterId,
        int $companyId,
        int $seedPoolGroupId,
        string $advertorialsSurrogateId,
        string $advertorialName,
        string $advertorialDescription,
        string $advertorialBeginDate,
        string $advertorialEndDate,
        string $fileNameHtml,
        string $contentUploadedFile,
        string $contentExtractImages,
        string $contentInlineCss,
        string $contentCleanHtml,
        string $fileNameZip,
        string $contentUploadedZipFile
    ): int {
        $return = 0;

        $insert =
            'INSERT INTO advertorials 
                (objectregister_id, company_id, seed_pool_group_id, advertorials_surrogate_id, advertorial_name, advertorial_description, advertorial_begin_date, advertorial_end_date, file_name_html, content_uploaded_html_file, content_extract_images, content_inline_css, content_clean_html, file_name_zip, content_uploaded_zip_file, created_by, updated_by) 
            VALUES 
                (:objectregister_id, :company_id, :seed_pool_group_id, :advertorials_surrogate_id, :advertorial_name, :advertorial_description, :advertorial_begin_date, :advertorial_end_date, :file_name_html, :content_uploaded_html_file, :content_extract_images, :content_inline_css, :content_clean_html, :file_name_zip, :content_uploaded_zip_file, :created_by, :updated_by)';

        try {
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':objectregister_id', $objectRegisterId,\PDO::PARAM_INT);
            $stmt->bindParam(':company_id', $companyId, \PDO::PARAM_INT);
            $stmt->bindParam(':seed_pool_group_id', $seedPoolGroupId, \PDO::PARAM_INT);
            $stmt->bindParam(':advertorials_surrogate_id', $advertorialsSurrogateId, \PDO::PARAM_STR);
            $stmt->bindParam(':advertorial_name', $advertorialName);
            $stmt->bindParam(':advertorial_description', $advertorialDescription);
            $stmt->bindParam(':advertorial_begin_date', $advertorialBeginDate);
            $stmt->bindParam(':advertorial_end_date', $advertorialEndDate);
            $stmt->bindParam(':file_name_html', $fileNameHtml);
            $stmt->bindParam(':content_uploaded_html_file', $contentUploadedFile);
            $stmt->bindParam(':content_extract_images', $contentExtractImages);
            $stmt->bindParam(':content_inline_css', $contentInlineCss);
            $stmt->bindParam(':content_clean_html', $contentCleanHtml);
            $stmt->bindParam(':file_name_zip', $fileNameZip);
            $stmt->bindParam(':content_uploaded_zip_file', $contentUploadedZipFile);
            $stmt->bindParam(':created_by', $this->currentUserId);
            $stmt->bindParam(':updated_by', $this->currentUserId);
            $stmt->execute();
            $return = $this->conn->lastInsertId();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $return;
    }


    /**
     * @param int    $advertorialId
     * @param int    $objectRegisterId
     * @param int    $companyId
     * @param int    $seedPoolGroupId
     * @param string $advertorialName
     * @param string $advertorialDescription
     * @param string $advertorialBeginDate
     * @param string $advertorialEndDate
     * @param string $fileNameHtml
     * @param string $contentUploadedFile
     * @param string $contentExtractImages
     * @param string $contentInlineCss
     * @param string $contentCleanHtml
     * @param string $fileNameZip
     * @param string $contentUploadedZipFile
     * @return int 0 in case update failed or $advertorialId
     */
    public function updateAdvertorial(
        int $advertorialId,
        int $companyId,
        int $seedPoolGroupId,
        string $advertorialSurrogateId,
        string $advertorialName,
        string $advertorialDescription,
        string $advertorialBeginDate,
        string $advertorialEndDate,
        string $fileNameHtml,
        string $contentUploadedFile,
        string $contentExtractImages,
        string $contentInlineCss,
        string $contentCleanHtml,
        string $fileNameZip,
        string $contentUploadedZipFile
    ): int {
        $return = 0;

        if(empty($advertorialSurrogateId)) {
            $advertorialSurrogateId = $this->getAdvertorialSurrogateId($advertorialId);
        }

        $update = 'UPDATE advertorials SET 
            company_id = :company_id,
            seed_pool_group_id = :seed_pool_group_id,
            advertorials_surrogate_id = :advertorials_surrogate_id, 
            advertorial_name = :advertorial_name,
            advertorial_description = :advertorial_description,
            advertorial_begin_date = :advertorial_begin_date,
            advertorial_end_date =:advertorial_end_date,
            file_name_html = :file_name_html,
            content_uploaded_html_file = :content_uploaded_html_file,
            content_extract_images = :content_extract_images,
            content_inline_css = :content_inline_css,
            content_clean_html = :content_clean_html,
            file_name_zip = :file_name_zip,
            content_uploaded_zip_file =:content_uploaded_zip_file,
            created_by =:created_by,
            updated_by =:updated_by
        WHERE id = :advertorial_id';

        try {
            $stmt = $this->conn->prepare($update);
            $stmt->bindParam(':advertorial_id', $advertorialId,\PDO::PARAM_INT);
            $stmt->bindParam(':company_id', $companyId, \PDO::PARAM_INT);
            $stmt->bindParam(':seed_pool_group_id', $seedPoolGroupId, \PDO::PARAM_INT);
            $stmt->bindParam(':advertorials_surrogate_id', $advertorialSurrogateId, \PDO::PARAM_STR);
            $stmt->bindParam(':advertorial_name', $advertorialName);
            $stmt->bindParam(':advertorial_description', $advertorialDescription);
            $stmt->bindParam(':advertorial_begin_date', $advertorialBeginDate);
            $stmt->bindParam(':advertorial_end_date', $advertorialEndDate);
            $stmt->bindParam(':file_name_html', $fileNameHtml);
            $stmt->bindParam(':content_uploaded_html_file', $contentUploadedFile);
            $stmt->bindParam(':content_extract_images', $contentExtractImages);
            $stmt->bindParam(':content_inline_css', $contentInlineCss);
            $stmt->bindParam(':content_clean_html', $contentCleanHtml);
            $stmt->bindParam(':file_name_zip', $fileNameZip);
            $stmt->bindParam(':content_uploaded_zip_file', $contentUploadedZipFile);
            $stmt->bindParam(':created_by', $this->currentUserId);
            $stmt->bindParam(':updated_by', $this->currentUserId);
            $stmt->execute();
            $return = ($stmt->rowCount() > 0) ? $advertorialId : 0;

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $return;
    }


    /**
     * @param int $advertorialId
     * @return string
     */
    private function getAdvertorialSurrogateId(int $advertorialId):string
    {
        $advertorialSurrogateId = '';
        $select = "SELECT a.advertorials_surrogate_id FROM advertorials a WHERE a.id = :advertorial_id";
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':advertorial_id', $advertorialId, \PDO::PARAM_INT);
            $stmt->execute();
            $advertorialSurrogateId = $stmt->fetchColumn();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $advertorialSurrogateId;
    }


    /**
     * @param int $advertorialId
     * @return int 0, if not deleted, else int > 0
     */
    public function deleteAdvertorialsById(int $advertorialId):int
    {
        $return = 0;
        $delete = 'DELETE FROM advertorials WHERE id = :advertorial_id';
        try {
            $stmt = $this->conn->prepare($delete);
            $stmt->bindParam(':advertorial_id', $advertorialId, \PDO::PARAM_INT);
            $stmt->execute();
            $return = $stmt->rowCount();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }



    /**
     * @param int $companyId
     * @return array|null
     */
    public function getAdvertorialsByCompanyId(int $companyId): ?array
    {
        $return = null;
        $select =
            "SELECT 
                a.*, 
                DATE_FORMAT(a.advertorial_begin_date, '%Y') as advertorial_begin_date_year,
                DATE_FORMAT(a.advertorial_end_date, '%Y') as advertorial_end_date_year,
                DATE_FORMAT(a.advertorial_begin_date, '%m') as advertorial_begin_date_month,
                DATE_FORMAT(a.advertorial_end_date, '%m') as advertorial_end_date_month,                
                DATE_FORMAT(a.updated, '%Y') as updated_year,
                DATE_FORMAT(a.updated, '%m') as updated_month,
                o.object_unique_id, 
                c.name as company_name, 
                c.account_no as company_account_no, 
                spg.name as seed_pool_group_name
            FROM advertorials a
            JOIN seed_pool_group spg ON spg.id = a.seed_pool_group_id
            JOIN objectregister o ON o.id = a.objectregister_id
            JOIN company c ON c.id = a.company_id 
            WHERE a.company_id = :company_id
            ORDER BY a.updated DESC";

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $companyId, \PDO::PARAM_INT);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if($stmt->rowCount() > 0) {
                $return = $records;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $this->logger->info('$records', [$return, __METHOD__, __LINE__]);
            $this->logger->info('$keys', [json_encode(array_keys($return[0])), __METHOD__, __LINE__]);
        } catch (Exception $e) {
            $this->logger->warning('logger info problem logging empty values', [__METHOD__, __LINE__]);
        }
        return $return;

    }


    /**
     * @param int $advertorialId
     * @return array|null
     */
    public function getAdvertorialById(int $advertorialId):?array
    {
        $return = null;
        $select =
            "SELECT 
                a.*, 
                DATE_FORMAT(a.advertorial_begin_date, '%Y') as advertorial_begin_date_year,
                DATE_FORMAT(a.advertorial_end_date, '%Y') as advertorial_end_date_year,
                DATE_FORMAT(a.advertorial_begin_date, '%m') as advertorial_begin_date_month,
                DATE_FORMAT(a.advertorial_end_date, '%m') as advertorial_end_date_month,                
                DATE_FORMAT(a.updated, '%Y') as updated_year,
                DATE_FORMAT(a.updated, '%m') as updated_month,
                o.object_unique_id, 
                c.name as company_name, 
                c.account_no as company_account_no, 
                spg.name as seed_pool_group_name
            FROM advertorials a
            JOIN seed_pool_group spg ON spg.id = a.seed_pool_group_id
            JOIN objectregister o ON o.id = a.objectregister_id
            JOIN company c ON c.id = a.company_id 
            WHERE a.id = :advertorial_id
            ORDER BY a.updated DESC";

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':advertorial_id', $advertorialId, \PDO::PARAM_INT);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            if($stmt->rowCount() > 0) {
                $return = $records[0]; // return one record
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $this->logger->info('$records', [$return, __METHOD__, __LINE__]);
            $this->logger->info('$keys', [json_encode(array_keys($return[0])), __METHOD__, __LINE__]);
        } catch (Exception $e) {
            $this->logger->warning('logger info problem logging empty values', [$e->getMessage(),__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * @param int   $advertorials_id
     * @param array $images [][original_url,url,original_image_name,image_hash_name,image_content]
     * @return array
     */
    public function createAvertorialImages(int $advertorials_id, array $images):array
    {
        $imgIds = [];
        foreach($images as $img) {
            if(true === $img['scan_status']) { // leave out tracking pixels
                $imgIds[] = $this->createAdvertorialImage(
                    $advertorials_id,
                    $img['original_url'],
                    $img['url'],
                    $img['original_image_name'],
                    $img['image_hash_name'],
                    gzcompress(base64_encode($img['image_content']), 9),
                    $img['mime_type'],
                    $img['scan_line_number'],
                    $img['scan_status'],
                    $img['scan_status_reason'],
                    empty($img['transfer_status']) ? 'no status found' : $img['transfer_status']
                );
            }
        }
        return $imgIds;
    }


    /**
     * create image record
     * @param int         $advertorials_id
     * @param string      $original_url
     * @param string      $url
     * @param string      $original_image_name
     * @param string      $image_hash_name
     * @param string      $image_content
     * @param string|null $mime_type
     * @param int         $scan_line_number
     * @param bool        $scan_status
     * @param string      $scan_status_reason
     * @param string|null $transfer_status
     * @return int
     */
    private function createAdvertorialImage(
        int $advertorials_id,
        string $original_url,
        ?string $url,
        string $original_image_name,
        ?string $image_hash_name,
        string $image_content,
        ?string $mime_type,
        int $scan_line_number,
        bool $scan_status,
        string $scan_status_reason,
        ?string $transfer_status
    ):int
    {
        $lastInsertedId = 0;
        $insert =
            "INSERT INTO advertorial_images 
            (advertorials_id, original_url, url, original_image_name, image_hash_name, image_content, mime_type, scan_line_number, scan_status, scan_status_reason, transfer_status, created_by, updated_by) 
            VALUES 
            (:advertorials_id, :original_url, :url, :original_image_name, :image_hash_name, :image_content, :mime_type, :scan_line_number, :scan_status, :scan_status_reason, :transfer_status, :created_by, :updated_by)";

        try {
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':advertorials_id', $advertorials_id, \PDO::PARAM_INT);
            $stmt->bindParam(':original_url', $original_url, \PDO::PARAM_STR);
            $url = (empty($url)) ? 'not substituted - tracking pixel url' : $url;
            $stmt->bindParam(':url', $url, \PDO::PARAM_STR);
            $stmt->bindParam(':original_image_name', $original_image_name, \PDO::PARAM_STR);
            $image_hash_name = (empty($image_hash_name)) ? 'not substituted - tracking pixel image' : $image_hash_name;
            $stmt->bindParam(':image_hash_name', $image_hash_name, \PDO::PARAM_STR);
            $stmt->bindParam(':image_content', $image_content, \PDO::PARAM_LOB);
            $stmt->bindParam(':mime_type', $mime_type, \PDO::PARAM_STR | \PDO::PARAM_NULL);
            $stmt->bindParam(':scan_line_number', $scan_line_number, \PDO::PARAM_STR);
            $stmt->bindParam(':scan_status', $scan_status, \PDO::PARAM_STR);
            $stmt->bindParam(':scan_status_reason', $scan_status_reason, \PDO::PARAM_STR);
            $stmt->bindParam(':transfer_status', $transfer_status, \PDO::PARAM_STR | \PDO::PARAM_NULL);
            $stmt->bindParam(':created_by', $this->currentUserId, \PDO::PARAM_STR);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_STR);
            $stmt->execute();
            $lastInsertedId = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $lastInsertedId;
    }


    /**
     * update advertorial images with transfer status to corresponding servers
     * @param int    $id
     * @param string $transfer_status
     * @return bool
     */
    public function updateAdvertorialImage(int $id, string $transfer_status):bool
    {
        $ret = false;
        $update = "UPDATE advertorial_images SET transfer_status = :transfer_status WHERE id=:id";
        try {
            $stmt = $this->conn->prepare($update);
            $stmt->bindParam(':id', $id, \PDO::PARAM_STR | \PDO::PARAM_NULL);
            $stmt->bindParam(':transfer_status', $transfer_status, \PDO::PARAM_STR);
            $ret = $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $ret;
    }
}
