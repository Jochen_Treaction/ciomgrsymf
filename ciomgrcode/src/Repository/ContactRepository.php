<?php


namespace App\Repository;

use App\Interfaces\LeadsInterface;
use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use JsonException;
use PDO;
use PhpParser\Node\Expr\Array_;
use Psr\Log\LoggerInterface;
use RuntimeException;


class ContactRepository
{
    private $logger;
    private $conn;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private $objectRegisterRepository;
    /**
     * @var CustomfieldsRepository
     * @author Pradeep
     */
    private $customfieldsRepository;
    /**
     * @var StandardFieldMappingRepository
     * @author Pradeep
     */
    private $standardFieldMappingRepository;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger,
        ObjectRegisterRepository $objectRegisterRepository,
        CustomfieldsRepository $customfieldsRepository,
        StandardFieldMappingRepository $standardFieldMappingRepository
    ) {
        $this->logger = $logger;
        $this->conn = $connection;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->customfieldsRepository = $customfieldsRepository;
        $this->standardFieldMappingRepository = $standardFieldMappingRepository;
    }

    /**
     * TODO: test!
     * @param string $domain
     * @return mixed
     * @author jsr
     * @deprecated just for test.
     */
    public function getLeadsCountByEmailDomain(string $domain): int
    {
        if (empty($domain) || !is_string($domain)) {
            return 0;
        }

        try {
            $sql = 'SELECT count(*) AS cnt FROM leads WHERE email LIKE ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, '%' . $domain . '%');
            $stmt->execute();
            $res = $stmt->fetch();
            return $res;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @param $company_id
     * @return array
     * @author jsr
     */
    public function getLeadsForDashboard(int $company_id): array
    {
        $leads = [];
        if ($company_id <= 0) {
            return $leads;
        }
        try { // TODO FIX ERROR: l.`status` must be fetched via objectregister and highest version
            $sql = 'SELECT 
                l.id,
                c.id as campaign_id,
                c.name as campaign,
                l.lead_reference,
                l.salutation,
                l.first_name,
                l.last_name,
                l.email,
                l.created,
                s.value as status
            FROM leads l 
            JOIN campaign c on c.id = l.campaign_id 
            Join objectregister o on l.objectregister_id = o.id
            join statusdef s on o.statusdef_id = s.id
            WHERE company_id = :company_id
            ORDER BY l.id DESC LIMIT 50 ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':company_id', $company_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (is_array($res) && !empty($res)) {
                $leads = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return [];
        }

        return $leads;
    }

    /**
     * @param int $company_id default 0
     * @return array
     * @author jsr
     */
    public function getLeadsCountForDashboardChart(int $company_id): array
    {
        $leads_count = [];
        if ($company_id <= 0) {
            return $leads_count;
        }

        try {
            $sql = 'SELECT
                c.name as campaign_name,
                count(*) as leads
            FROM leads l
            JOIN campaign c on c.id = l.campaign_id 
            JOIN company cy on cy.id = c.company_id
            WHERE cy.id = :company_id
            GROUP by campaign_name';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (is_array($res)) {
                $leads_count = $res;
            }
            return $leads_count;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return [];
    }

    /**
     * @param int $company_id default 0
     * @return array
     * @author
     * @deprecated
     */
    public function getLeadsPerPeriodForDashboardChart(int $company_id): array
    {
        $leads_per_period = [];
        if ($company_id <= 0) {
            return $leads_per_period;
        }

        try {
            $sql = 'SELECT
                MONTHNAME(l.created) AS create_month, 
                count(*) AS cnt
            FROM leads l 
            JOIN campaign c ON c.id = l.campaign_id 
            JOIN company cy ON cy.id = c.company_id
            WHERE cy.id=:company_id
            GROUP BY MONTHNAME(l.created)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->execute();
            $res = $stmt->fetchAll();

            if (is_array($res) && !empty($res)) {
                $leads_per_period = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return $leads_per_period;
    }

    /**
     * @param int $companyId
     * @return array|null
     * @author Pradeep
     * @deprecated
     */
    public function getLeadsForContactHistory(int $companyId): ?array
    {
        $leads = [];
        if ($companyId <= 0) {
            return $leads;
        }
        $sql = "
            select distinct *
            from (
                     select c.name                       as `Source`,
                            c.id                         as `CampaignId`,
                            s.value                      AS Status,
                            l.*,
                            cf.fieldname                 as `CustomFieldQuestion`,
                            lcf.lead_customfield_content as `CustomFieldAnswer`
                     from campaign c
                              join leads l on l.campaign_id = c.id 
                              left outer join lead_customfield_content lcf on lcf.leads_id = l.id
                              join customfields cf on lcf.customfields_id = cf.id
                              JOIN objectregister o on l.objectregister_id = o.id
                              JOIN statusdef s on o.statusdef_id = s.id
                         and o.statusdef_id = s.id
                         and cf.fieldname != ''
            
                     union
                     select c.name                as `Source`,
                            c.id                  as `CampaignId`,
                            s.value               AS Status,
                            l.*,
                            'CustomFieldQuestion' as `CustomFieldQuestion`,
                            'CustomFieldAnswer'   as `CustomFieldAnswer`
                     from campaign c
                              join leads l
                                   on l.campaign_id = c.id
                              JOIN objectregister o on l.objectregister_id = o.id
                              JOIN statusdef s on o.statusdef_id = s.id and o.statusdef_id = s.id
                 ) as u
            where u.account = ?
            order by u.id desc
        ";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(1, $companyId);
        $stmt->execute();
        $res = $stmt->fetchAll();
        if (is_array($res)) {
            $leads = $res;
        }
        return $leads;
    }


    /**
     * @param int $companyId
     * @param string $searchText
     * @param string $dateFrom
     * @param string $dateTo
     * @param string $filterStatus
     * @param int $lead_id
     * @param string $lead_id_operator
     * @param int $limit
     * @return array
     */
    public function getPagingLeadsForContactHistory(
        int $companyId,
        string $searchText,
        string $dateFrom,
        string $dateTo,
        string $filterStatus,
        int $lead_id,
        string $lead_id_operator,
        int $limit
    ): array {
        $contacts = [];
        if ($companyId <= 0) {
            return $contacts;
        }

        if (empty($searchText)) {
            $searchTextJoin = '';
        } else {
            $searchTextJoin = "JOIN searchleads sl ON sl.id = l.id AND MATCH(searchtext) AGAINST('{$searchText}')";
        }

        if (empty($filterStatus)) {

            $filterStatus = "IN ('active', 'fraud', 'inactive', 'blacklisted' )";
        } else {
            $filterStatus = "= '{$filterStatus}'";
        }

        $and_lead_id = "AND l.id $lead_id_operator $lead_id";
        $_limit = "LIMIT $limit";

        $sql = "
            SELECT DISTINCT *
            FROM (
                     SELECT c.name                       AS `Source`,
                            c.id                         AS `CampaignId`,
                            s.value                      AS Status,
                            l.*,
                            cf.fieldname                 AS `CustomFieldQuestion`,
                            lcf.lead_customfield_content AS `CustomFieldAnswer`
                     FROM campaign c
                              JOIN leads l ON l.campaign_id = c.id 
                                    AND l.account = :company_id
                                    AND l.created BETWEEN :dateFrom AND :dateTo
                                    $and_lead_id
                              $searchTextJoin
                              LEFT OUTER JOIN lead_customfield_content lcf ON lcf.leads_id = l.id
                              JOIN customfields cf ON lcf.customfields_id = cf.id
                              JOIN objectregister o ON l.objectregister_id = o.id
                              JOIN statusdef s ON o.statusdef_id = s.id AND s.value $filterStatus
                         AND o.statusdef_id = s.id
                         AND cf.fieldname != ''
            
                     UNION
                     SELECT c.name                AS `Source`,
                            c.id                  AS `CampaignId`,
                            s.value               AS Status,
                            l.*,
                            'CustomFieldQuestion' AS `CustomFieldQuestion`,
                            'CustomFieldAnswer'   AS `CustomFieldAnswer`
                     FROM campaign c
                              JOIN leads l ON l.campaign_id = c.id 
                                    AND l.account = :company_id
                                    $and_lead_id
                              $searchTextJoin
                              JOIN objectregister o ON l.objectregister_id = o.id
                              JOIN statusdef s ON o.statusdef_id = s.id and o.statusdef_id = s.id AND s.value $filterStatus
                 ) AS u
                 ORDER BY u.id DESC
                 $_limit
        ";

        $this->logger->info('$sql', [$sql, __METHOD__, __LINE__]);
        $this->logger->info('sql params', [
            [
                '$companyId' => $companyId,
                '$searchText' => $searchText,
                '$dateFrom' => $dateFrom,
                '$dateTo' => $dateTo,
                '$filterStatus' => $filterStatus,
                '$lead_id' => $lead_id,
                '$lead_id_operator' => $lead_id_operator,
                '$limit' => $limit,
            ],
            __METHOD__,
            __LINE__,
        ]);

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam('company_id', $companyId, PDO::PARAM_INT);
        $stmt->bindParam(':dateFrom', $dateFrom, PDO::PARAM_STR);
        $stmt->bindParam(':dateTo', $dateTo, PDO::PARAM_STR);

        $stmt->execute();
        $res = $stmt->fetchAll();

        $this->logger->info('$res', [$res, __METHOD__, __LINE__]);

        if (!empty($res)) {
            $contacts = $res;
        }

        $leadIds = [];
        $minId = 0;
        $maxId = 0;

        foreach ($res as $lead) {
            $leadIds[] = $lead[ 'id' ];
        }
        sort($leadIds);
        $minId = $leadIds[ 0 ];
        $maxId = $leadIds[ count($leadIds) - 1 ];
        $this->logger->info('$contactsHistory', [$contacts, __METHOD__, __LINE__]);
        $this->logger->info('$minId-history', [$minId, __METHOD__, __LINE__]);
        $this->logger->info('$maxId-history', [$maxId, __METHOD__, __LINE__]);
        return ['contacts' => $contacts, 'minId' => $minId, 'maxId' => $maxId];

    }


    /**
     * @param int $companyId
     * @param string $searchText
     * @param string $dateFrom
     * @param string $dateTo
     * @param string $filterStatus
     * @return int
     */
    public function getCountPagingLeadsForContactHistory(
        int $companyId,
        string $searchText,
        string $dateFrom,
        string $dateTo,
        string $filterStatus
    ): int {
        $contacts = [];
        if ($companyId <= 0) {
            return $contacts;
        }

        if (empty($searchText)) {
            $searchTextJoin = '';
        } else {
            $searchTextJoin = "JOIN searchleads sl ON sl.id = l.id AND MATCH(searchtext) AGAINST('{$searchText}')";
        }

        if (empty($filterStatus)) {

            $filterStatus = "IN ('active', 'fraud', 'inactive', 'blacklisted' )";
        } else {
            $filterStatus = "= '{$filterStatus}'";
        }


        $sql = "
            SELECT Count(*)
            FROM (
                     SELECT c.name                       AS `Source`,
                            c.id                         AS `CampaignId`,
                            s.value                      AS Status,
                            l.*,
                            cf.fieldname                 AS `CustomFieldQuestion`,
                            lcf.lead_customfield_content AS `CustomFieldAnswer`
                     FROM campaign c
                              JOIN leads l ON l.campaign_id = c.id 
                            AND l.account = :company_id
                            AND l.created BETWEEN :udateFrom AND :udateTo -- may be take 'AND l.created ...' out here
                              $searchTextJoin
                              LEFT OUTER JOIN lead_customfield_content lcf ON lcf.leads_id = l.id
                              JOIN customfields cf ON lcf.customfields_id = cf.id
                              JOIN objectregister o ON l.objectregister_id = o.id
                              JOIN statusdef s ON o.statusdef_id = s.id AND s.value $filterStatus
                         AND o.statusdef_id = s.id
                         AND cf.fieldname != ''
            
                     UNION
                     SELECT c.name                AS `Source`,
                            c.id                  AS `CampaignId`,
                            s.value               AS Status,
                            l.*,
                            'CustomFieldQuestion' AS `CustomFieldQuestion`,
                            'CustomFieldAnswer'   AS `CustomFieldAnswer`
                     FROM campaign c
                              JOIN leads l ON l.campaign_id = c.id 
                                    AND l.account = :company_id 
                              $searchTextJoin
                              JOIN objectregister o ON l.objectregister_id = o.id
                              JOIN statusdef s ON o.statusdef_id = s.id and o.statusdef_id = s.id AND s.value $filterStatus
                 ) AS u
        ";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam('company_id', $companyId, PDO::PARAM_INT);
        $stmt->bindParam(':udateFrom', $dateFrom, PDO::PARAM_STR);
        $stmt->bindParam(':udateTo', $dateTo, PDO::PARAM_STR);

        $stmt->execute();

        $count = $stmt->fetchOne();
        $this->logger->info('$count$history', [$count, __METHOD__]);
        return $count;
    }

    /**
     * @param int $company_id
     * @return array
     * @author pva
     * @deprecated
     */
    public function getLeadsForDataTableView(int $company_id): array
    {
        # Leads for DataTable
        $lead_for_dt = [];
        if ($company_id <= 0) {
            return $lead_for_dt;
        }
        $leads = $this->getCampaignLeadInfosByCompanyId($company_id);
        //$leads = $this->getLeadsForContactHistory($company_id);
        $lead_count = count($leads);
        if ($lead_count <= 0) {
            return $lead_for_dt;
        }
        $lead_for_dt = [
            'draw' => 1,
            'recordsTotal' => $lead_count,
            'recordsFiltered' => $lead_count,
            'data' => [],
        ];
        foreach ($leads as $lead) {
            $action = [
                $lead[ 'CompanyId' ],
                $lead[ 'CampaignId' ],
                $lead[ 'lead_id' ],
            ];
            $lead_data = [
                $lead[ 'lead_id' ],
                $lead[ 'Campaign Name' ],
                $lead[ 'Lead Reference' ],
                $lead[ 'Created' ],
                $lead[ 'Salutation' ],
                $lead[ 'Firstname' ],
                $lead[ 'Lastname' ],
                $lead[ 'Email' ],
                $lead[ 'Postalcode' ],
                $lead[ 'Trafficsource' ],
                $lead[ 'UTM Parameters' ],
                $lead[ 'TargetGroup' ],
                $lead[ 'AffiliateID' ],
                $lead[ 'AffiliateSubID' ],
                $lead[ 'Status' ],
                $action,
            ];
            $lead_for_dt[ 'data' ][] = $lead_data;
        }
        return $lead_for_dt;
    }

    /**
     *
     * @param int $company_id
     * @return array
     * @author jsr
     */
    public function getCampaignLeadInfosByCompanyId(int $company_id): array
    {
        $leads = [];
        if (empty($company_id) || !is_int($company_id)) {
            return $leads;
        }
        try { // TODO: get l.status via  objectregister (max. version) -> statusdef
            $sql = 'SELECT
                           c2.id AS CompanyId,
			               c.id AS CampaignId,
                           l.id  AS lead_id,
                           c.name AS \'Campaign Name\',
                           l.lead_reference AS \'Lead Reference\',
                           l.created AS Created,
                           l.salutation AS Salutation,
                           l.first_name AS Firstname,
                           l.last_name AS Lastname,
                           l.email AS Email,
                           l.postal_code AS Postalcode,
                           l.traffic_source  AS Trafficsource,
                           l.utm_parameters AS \'UTM Parameters\',
                           l.target_group AS \'TargetGroup\',
                           l.affiliate_id AS \'AffiliateID\',
                           l.affiliate_sub_id AS \'AffiliateSubID\', 
                            s2.value         AS Status
                          /* l.status AS Status*/
                    FROM leads l
                        JOIN campaign c ON c.id = l.campaign_id
                        JOIN company c2 ON c2.id = c.company_id
                        JOIN objectregister o on l.objectregister_id = o.id
                        JOIN statusdef s2 on o.statusdef_id = s2.id
                    WHERE c2.id = ? and l.objectregister_id >0
                    order by l.id desc';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $company_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (is_array($res)) {
                $leads = $res;
            }

            return $leads;
        } catch (DBALException|Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Changes the Contact status by Email.
     * Contact status is updates using ObjectRegisterId.
     * Updates the status for all the contacts which have same email address(all the history of contacts.)
     * @param string $email
     * @param string $newStatus
     * @return bool
     * @author pva
     */
    public function changeLeadStatus(string $email, string $newStatus): bool
    {
        try {
            if (empty($email) || empty($newStatus)) {
                throw new RuntimeException('Invalid LeadId or Status provided');
            }
            // get contact history of same email address.
            // update new status for all the contact history.
            $leadsWithSameEmail = $this->getContactsByEmail($email);
            foreach ($leadsWithSameEmail as $lead) {
                if (empty($lead) || !isset($lead[ 'id' ], $lead[ 'objectregister_id' ])) {
                    continue;
                }
                if (!$this->objectRegisterRepository->updateStatus($lead[ 'objectregister_id' ], $newStatus)) {
                    $this->logger->info('Failed to update ObjectRegister with Id = 
                    ' . $lead[ 'objectregister_id' ] . ' to status ' . $newStatus);
                }
            }
            return true;
        } catch (Exception|RuntimeException  $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * Fetches all the contacts (history) which have same email address.
     * @param string $email
     * @return array|null
     * @author Pradeep
     */
    public function getContactsByEmail(string $email): ?array
    {
        if (empty($email)) {
            return [];
        }
        $sql = 'select * from leads where email= :email';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':email', $email);
        $stmt->execute();
        $stmt->rowCount();
        return $stmt->fetchAll();
    }

    /**
     * @param int $id
     * @return array|null
     * @author Pradeep
     */
    public function get(int $id): ?array
    {
        $lead = [];
        try {
            if ($id <= 0) {
                throw new RuntimeException('Invalid LeadId provided');
            }
            $sql = 'SELECT * FROM leads WHERE id=:id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $id, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if ($stmt->rowCount() > 0 && !empty($result[ 0 ])) {
                $lead = $result[ 0 ];
            }
            return $lead;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $lead_id
     * @return array
     * @author pva
     */
    public function fetchDetailsForSingleLeadView(int $lead_id): array
    {
        $lead_details = [];
        if ($lead_id <= 0) {
            $this->logger->critical('Invalid Lead Id', [__METHOD__, __LINE__]);
            return $lead_details;
        }
        // TODO: 1.get l.status via  objectregister (max. version) -> statusdef and camapign status
        //       2.get custom fields
        $sql = 'SELECT
                   l.*,
                   c.name as c_name,
                   c.url as c_url,
                   c.created as c_created
            FROM leads l
                INNER JOIN campaign c ON c.id = l.campaign_id
                JOIN objectregister o on l.objectregister_id = o.id
                JOIN statusdef s on o.statusdef_id = s.id
            WHERE l.id = :id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $lead_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[ 0 ])) {
                $lead_details = $res[ 0 ];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $lead_details;
    }


    /**
     * @param int $companyId
     * @return bool
     * @author Pradeep
     */
    public function checkCampaignHasCustomFields(int $companyId): bool
    {
        $has_custom_field = false;
        if ($companyId <= 0) {
            return $has_custom_field;
        }
        $sql = '
                select cus.*
                from customfields cus
                Join objectregister o on o.id = cus.object_register_id
                join campaign c on cus.campaign_id = c.id
                join c.company_id = :company_id;
        ';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':company_id', $companyId);
        $stmt->execute();
        $res = $stmt->fetchAll();

        if (is_array($res) && !empty($res)) {
            $has_custom_field = true;
        }
        return $has_custom_field;
    }

    /**
     *
     * @param int $id
     * @return Array
     * @author Aki
     * @comment we need to change this function after the change of campaign table with UUID
     * @deprecated
     */
    public function getCountOfLeadsInCampaign(string $id): int
    {
        $result = 0;
        if (empty($id)) {
            return 0;
        }

        try {
            $sql = '
                SELECT count(*) AS cnt
                FROM leads l
                         JOIN campaign c on l.campaign_id = c.id
                         JOIN objectregister o ON o.id = c.objectregister_id
                WHERE o.object_unique_id =:object_unique_id
            ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':object_unique_id', $id, ParameterType::STRING);
            $stmt->execute();
            $res = $stmt->fetch();
            if ($stmt->rowCount() > 0 && !empty($res[ 'cnt' ])) {
                $result = $res[ 'cnt' ];
            }
            return $result;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return 0;
        }
    }

    /**
     * @param string $objectRegisterId
     * @return int
     * @author Aki
     */
    public function getUniqueObjectForLead(
        string $objectRegisterId
    ): string {
        $object_unique_id = '0';
        try {
            $sql = 'SELECT o.*
                            FROM objectregister o
                            WHERE o.id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $objectRegisterId);
            $stmt->execute();
            $object_register = $stmt->fetchAll();
            $object_unique_id = $object_register[ 0 ][ 'object_unique_id' ];

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $object_unique_id;
    }


    /**
     * @return array
     * @author Pradeep
     * @internal Fetches the list of standard fields from table 'miostandardfield'
     */
    public function getStandardFieldsFromDB(bool $getHiddenFields = false): array
    {
        $pageSDFields = [];
        $sql = 'SELECT  fieldname, htmllabel ,d.name as datatype, true as StandardFields
                FROM miostandardfield mstf
                    join datatypes d on mstf.datatypes_id = d.id
                WHERE hidden_field = false ORDER BY fieldname';
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if (empty($result)) {
            return $pageSDFields;
        }

        foreach ($result as $field) {
            $pageSDFields[ $field[ 'htmllabel' ] ] =
                [
                    'name' => $field[ 'fieldname' ],
                    'datatype' => $field[ 'datatype' ],
                ];
        }

        return $pageSDFields;
    }

    /**
     * @param int $cmpObjRegId
     * @return array|mixed[]
     * @author Pradeep
     * @deprecated
     */
    public function getLeadsByCampaignObjectRegisterId(string $objUniqueId): array
    {
        $leads = [];
        if (empty($objUniqueId)) {
            return $leads;
        }
        $sql = "
        select distinct * from (
                           select 
                                o.object_unique_id          as CampaignObjRegId,
                                c.id                         as `CampaignId`,
                                c.company_id                 as `CompanyId`,
                                c.name                       as `Campaign Name`,
                                s.value AS Status,
                                l.*,
                                cf.fieldname                 as `CustomFieldQuestion`,
                                lcf.lead_customfield_content as `CustomFieldAnswer`
                           from campaign c
                                    join leads l on l.campaign_id = c.id
                                    left outer join lead_customfield_content lcf on lcf.leads_id = l.id
                                    join customfields cf on lcf.customfields_id = cf.id
                                    JOIN objectregister o on c.objectregister_id = o.id
                                    JOIN statusdef s on o.statusdef_id = s.id and o.statusdef_id = s.id

                           union
                           select 
                                o.object_unique_id           as CampaignObjRegId,
                                c.id as `CampaignId`,
                                c.company_id as `CompanyId`,
                                c.name as `Campaign Name`,
                                s.value AS Status,
                                l.*,
                                'CustomFieldQuestion'  as `CustomFieldQuestion`,
                                'CustomFieldAnswer' as `CustomFieldAnswer`
                           from campaign c
                               join leads l on l.campaign_id = c.id
                               JOIN objectregister o on c.objectregister_id = o.id
                               JOIN statusdef s on o.statusdef_id = s.id and o.statusdef_id = s.id
                       ) as u where u.CampaignObjRegId = ? order by u.id desc
        ";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(1, $objUniqueId, ParameterType::STRING);
        $stmt->execute();
        $res = $stmt->fetchAll();
        if (is_array($res)) {
            $leads = $res;
        }
        return $leads;
    }

    /**
     * @param int $lead_id
     * @param string $uniqueObjectType
     * @return array
     * @author Pradeep
     */
    public function getLeadCustomFields(int $lead_id, string $uniqueObjectType = UniqueObjectTypes::CUSTOMFIELDS): array
    {
        $leadCustomFieldContent = [];
        $sql = 'select Distinct(c.fieldname), lcc.lead_customfield_content
                from leads l
                         join lead_customfield_content lcc on l.id = lcc.leads_id
                         join customfields c on c.id = lcc.customfields_id
                         join objectregister o on c.objectregister_id = o.id
                         join unique_object_type uot on o.unique_object_type_id = uot.id
                where l.id = ?
                  and uot.unique_object_name = ?
                  and c.fieldname != "" ';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(1, $lead_id);
        $stmt->bindParam(2, $uniqueObjectType);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if (!empty($result) && $stmt->rowCount() > 0) {
            foreach ($result as $res) {
                if (empty($res[ 'fieldname' ])) {
                    continue;
                }
                // Convert to snakeCase
                //$key = lcfirst(implode('', array_map('ucfirst', explode('_', $res[ 'fieldname' ]))));
                //$leadCustomFieldContent[ $key ] = $res[ 'lead_customfield_content' ];
                $leadCustomFieldContent[ $res[ 'fieldname' ] ] = $res[ 'lead_customfield_content' ];
            }
        }
        return $leadCustomFieldContent;
    }

    /**
     * @param int $companyId
     * @param int $lastSyncContactId
     * @param string $lastSyncDate
     * @return mixed[]
     * @author Pradeep
     */
    public function getCountForSync(int $companyId, int $leadId = 0): int
    {
        $count = 0;
        $query = '
                SELECT count(distinct(l.email)) as leadCount
                FROM leads l
                         JOIN objectregister o on o.id = l.objectregister_id
                         JOIN statusdef s on o.statusdef_id = s.id
                WHERE l.account = :account_id and s.value = :lead_status and l.id > :lead_id';

        $stmt = $this->conn->prepare($query);
        $stmt->bindValue('account_id', $companyId);
        $stmt->bindValue('lead_status', StatusDefRepository::ACTIVE);
        $stmt->bindValue('lead_id', $leadId, ParameterType::INTEGER);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if (!empty($result) && isset($result[ 0 ][ 'leadCount' ])) {
            $count = $result[ 0 ][ 'leadCount' ];
        }
        return $count;
    }

    public function getEndContactIdForSync(int $companyId)
    {
        $result = 0;
        $query = 'SELECT l.id
                FROM leads l
                         JOIN objectregister o on o.id = l.objectregister_id
                         JOIN statusdef s on o.statusdef_id = s.id
                WHERE l.account = :account_id and s.value = :lead_status 
                ORDER BY id DESC LIMIT 1 ';
        $stmt = $this->conn->prepare($query);
        $stmt->bindValue('account_id', $companyId);
        $stmt->bindValue('lead_status', StatusDefRepository::ACTIVE);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        if (isset($rows[ 0 ][ 'id' ]) && !empty($rows[ 0 ][ 'id' ])) {
            $result = $rows[ 0 ][ 'id' ];
        }
        return $result;
    }

    public function getStartContactIdForSync(int $companyId)
    {
        $result = 0;
        $query = 'SELECT l.id
                FROM leads l
                         JOIN objectregister o on o.id = l.objectregister_id
                         JOIN statusdef s on o.statusdef_id = s.id
                WHERE l.account = :account_id and s.value = :lead_status 
                ORDER BY id ASC LIMIT 1 ';
        $stmt = $this->conn->prepare($query);
        $stmt->bindValue('account_id', $companyId);
        $stmt->bindValue('lead_status', StatusDefRepository::ACTIVE);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        if (isset($rows[ 0 ][ 'id' ]) && !empty($rows[ 0 ][ 'id' ])) {
            $result = $rows[ 0 ][ 'id' ];
        }
        return $result;
    }

    /**
     * Filters duplicate records from sql query.
     * Due to the join between leads and custom field table. there are few duplicate lead records.
     * This functions removes the duplicate rows or records and merges the custom fields to contact array.
     *
     * @param array $contacts
     * @param int $companyId
     * @return array
     * @author Pradeep
     */
    private function filterDuplicateRecords(array $contacts, int $companyId): array
    {
        try {
            $uniqueRecords = [];
            $standardFields = $this->standardFieldMappingRepository->getMIOStandardFieldAsArray();
            $customFields = $this->customfieldsRepository->getCustomFieldsNames($companyId);
            $this->logger->info('FilterDuplicateRecords StandFields ' . json_encode($standardFields));
            $id = [];
            $leadId = 0;
            $i = 0;
            foreach ($contacts as $contact) {
                // Map to MIO standard and customFields
                $tempRecords = $this->filterColumns($contact, $standardFields, $customFields);
                // $this->logger->info('TempRecords ' . json_encode($tempRecords));
                if (!isset($id[ $contact[ 'email' ] ])) {
                    $i ++;
                    $id[ $contact[ 'email' ] ] = $contact[ 'lead_id' ];
                    $uniqueRecords[ $i ] = $tempRecords;
                    continue;
                }

                // Merge duplicate record
                if (isset($id[ $contact[ 'email' ] ], $tempRecords[ 'contact' ][ 'custom' ]) &&
                    $id[ $contact[ 'email' ] ] === $contact[ 'lead_id' ]) {
                    // $this->logger->info('uniqueRecords '.json_encode($uniqueRecords[ $i ][ 'custom' ]));
                    if (isset($uniqueRecords[ $i ][ 'contact' ][ 'custom' ])) {
                        $uniqueRecords[ $i ][ 'contact' ][ 'custom' ] = array_merge($uniqueRecords[ $i ][ 'contact' ][ 'custom' ],
                            $tempRecords[ 'contact' ][ 'custom' ]);
                    } else {
                        $uniqueRecords[ $i ][ 'contact' ][ 'custom' ] = $tempRecords[ 'contact' ][ 'custom' ];
                    }
                } else {
                    $this->logger->info('if failed');
                }
            }
            // $this->logger->info('Final uniqueRecords ' . json_encode($uniqueRecords));
            return $uniqueRecords;
        } catch (JsonException|Exception $e) {
            $this->logger->info($e->getMessage(), [__METHOD__, __LINE__]);
            return $uniqueRecords;
        }
    }

    /**
     * This function filters the 'CustomFieldAnswer' and 'CustomFieldQuestion' and
     * merges the values to the contact array.
     * SQL Query fetches the custom fields as 'CustomFieldQuestion'=> custom field name
     * and 'CustomFieldAnswer' => lead value for the custom field.
     *
     * @param array $contact
     * @param array $standardFieldsKey
     * @param array $customFieldsKey
     * @return array
     * @author Pradeep
     */
    private function filterColumns(array $contact, array $standardFieldsKey, array $customFieldsKey): array
    {
        $filtered = [];
        $ids = ['campaign_id', 'lead_id'];
        foreach ($contact as $key => $value) {
            if (in_array($key, $ids, true)) {
                $filtered[ 'base' ][ $key ] = $value;
            }
            if (in_array($key, $standardFieldsKey, true)) {
                // $filtered['contact'][ 'standard' ][ $key ] = $value;
                $filtered[ 'contact' ][ 'standard' ][] = [
                    $key => $value,
                    'required' => '',
                    'datatype' => '',
                    'regex' => '',
                ];
            }
            // Just for safety, when standard fields are present as customfields as well
            if ($key === 'CustomFieldQuestion' && in_array($value, $standardFieldsKey, true)) {
                continue;
            }
            if ($key === 'CustomFieldQuestion') {
                $tempCustomFieldKey = $value;
            }
            if ($key === 'CustomFieldAnswer') {
                $tempCustomFieldValue = $value;
            }
            if (!empty($tempCustomFieldKey) && !empty($tempCustomFieldValue) &&
                in_array($tempCustomFieldKey, $customFieldsKey, true)) {
                $filtered[ 'contact' ][ 'custom' ][] = [
                    $tempCustomFieldKey => $tempCustomFieldValue,
                    'required' => '',
                    'datatype' => '',
                    'regex' => '',
                ];
                $tempCustomFieldKey = '';
                $tempCustomFieldValue = '';
            } else {
                $filtered[ 'contact' ][ 'custom' ] = [];
            }
        }
//        $this->logger->info('filterColumn Final '.json_encode($filtered));
        return $filtered;
    }


    /**
     * Fetches all the latest contacts for contact overview twig.
     * @param int $companyId
     * @return array
     * @author Pradeep
     */
    public function getLeadsForContactOverview(int $companyId): array
    {
        $contacts = [];
        if ($companyId <= 0) {
            return $contacts;
        }

        $sql = "
        SELECT * FROM (
            SELECT l.*,
                   cf.fieldname                 as `CustomFieldQuestion`,
                   lcf.lead_customfield_content as `CustomFieldAnswer`
            from lead_customfield_content lcf
                     join leads l on l.id = lcf.leads_id
                     join customfields cf on lcf.customfields_id = cf.id
                     JOIN objectregister o on l.objectregister_id = o.id
                     JOIN statusdef s on o.statusdef_id = s.id
                     where l.account=?
               
            union
            
            SELECT l.*,
                   'CustomFieldQuestion' as `CustomFieldQuestion`,
                   'CustomFieldAnswer'   as `CustomFieldAnswer`
            from leads l
                     join (select email, MAX(id) as id from leads where account = ? group by email) as lj on lj.id = l.id
                     JOIN objectregister o on l.objectregister_id = o.id
                     JOIN statusdef s on o.statusdef_id = s.id
            where l.id not in (select lcf.leads_id from lead_customfield_content lcf)) 
            as ldquery Order by  ldquery.id desc ";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(1, $companyId);
        $stmt->bindValue(2, $companyId);
        $stmt->execute();
        $res = $stmt->fetchAll();

        if (empty($res) || !is_array($res)) {
            return $contacts;
        }

        //@internal -

        $duplicateEmail = [];
        $duplicateLeadId = 0;
        $duplicateEntries = [];
        $iteration = 0;
        foreach ($res as $lead) {

            if ($iteration === 0) {
                $contacts [] = $lead;
                $duplicateEntries[] = [
                    'email' => $lead[ 'email' ],
                    'id' => (int)$lead[ 'id' ],
                ];
                $duplicateEmail [] = $lead[ 'email' ];
                $duplicateLeadId = (int)$lead[ 'id' ];
            }

            if (($duplicateLeadId !== (int)$lead[ 'id' ]) && (in_array($lead[ 'email' ], $duplicateEmail, true))) {
                $iteration ++;
                continue;
            }

            $contacts [] = $lead;
            if (!in_array($lead[ 'email' ], array_column($duplicateEntries, 'email'), true)) {
                $duplicateEntries[] = [
                    'email' => $lead[ 'email' ],
                    'id' => (int)$lead[ 'id' ],
                ];
            }

            $duplicateEmail[] = trim($lead[ 'email' ]);
            $duplicateLeadId = (int)$lead[ 'id' ];

            $iteration ++;
        }
        return $contacts;
    }

    /**
     * Fetches all the latest contacts for contact overview twig.
     * returns array <pre>['contacts' => $contacts, 'minId' => $minId, 'maxId' =>$maxId]</pre>
     * @param int $companyId
     * @return array
     * @author jsr
     */
    public function getPagingLeadsForContactOverview(
        int $companyId,
        string $searchText,
        string $dateFrom,
        string $dateTo,
        string $filterStatus,
        int $lead_id,
        string $lead_id_operator,
        int $limit
    ): array {
        $contacts = [];
        if ($companyId <= 0) {
            return $contacts;
        }

        if (empty($searchText)) {
            $searchTextJoin = '';
        } else {
            $searchTextJoin = "JOIN searchleads sl ON sl.id = l.id AND MATCH(searchtext) AGAINST('{$searchText}')"; // TODO: WITH BETTER STATEMENT USE "LEFT OUTER JOIN searchleads sl ON sl.id = l.id AND MATCH(searchtext) AGAINST('{$searchText}')"
        }
        //converting date to date time for search in database as l.created and l.updated are datetime
        $dateTo .= ' 23:59:59.999';
        $dateFrom .= '  00:00:00.000';


        if (empty($filterStatus)) {

            $filterStatus = "IN ('active', 'fraud', 'inactive', 'blacklisted' )";
        } else {
            $filterStatus = "= '{$filterStatus}'";
        }

        $and_lead_id = "AND l.id $lead_id_operator $lead_id";
        $_limit = "LIMIT $limit";

        $sql = "
        SELECT * FROM (
            
            SELECT l.*,
                   cf.fieldname                 AS `CustomFieldQuestion`,
                   lcf.lead_customfield_content AS `CustomFieldAnswer`
            FROM lead_customfield_content lcf
                     JOIN leads l ON l.id = lcf.leads_id
                     $searchTextJoin
                     JOIN customfields cf ON lcf.customfields_id = cf.id
                     JOIN objectregister o ON l.objectregister_id = o.id
                     JOIN statusdef s ON o.statusdef_id = s.id AND s.value $filterStatus
                     WHERE l.account=:companyId
                     AND l.created BETWEEN :dateFrom AND :dateTo
                     $and_lead_id
            UNION
            SELECT l.*,
                   'CustomFieldQuestion' AS `CustomFieldQuestion`,
                   'CustomFieldAnswer'   AS `CustomFieldAnswer`
            FROM leads l
                     $searchTextJoin
                     JOIN (SELECT email, MAX(id) AS id FROM leads WHERE account = :companyId GROUP BY email) AS lj ON lj.id = l.id
                     JOIN objectregister o ON l.objectregister_id = o.id
                     JOIN statusdef s ON o.statusdef_id = s.id AND s.value $filterStatus
            WHERE l.id NOT IN (SELECT lcf.leads_id FROM lead_customfield_content lcf)
            $and_lead_id
            ) AS ldquery ORDER BY  ldquery.id DESC
            $_limit
            ";
        /* TODO: STATEMENT SHOULD BE THIS WAY
SELECT DISTINCT * FROM (

            SELECT l.*,
                   cf.fieldname                 AS `CustomFieldQuestion`,
                   lcf.lead_customfield_content AS `CustomFieldAnswer`
            FROM leads l
                     $searchTextJoin
                     LEFT OUTER JOIN lead_customfield_content lcf ON lcf.leads_id = l.id
                     LEFT OUTER JOIN customfields cf ON lcf.customfields_id = cf.id
                     LEFT OUTER JOIN objectregister o ON l.objectregister_id = o.id
                     LEFT OUTER JOIN statusdef s ON o.statusdef_id = s.id AND s.value IN ('active', 'fraud', 'inactive', 'blacklisted' )
                     WHERE l.account=:companyId
                     AND l.created BETWEEN :dateFrom AND :dateTo
                     $and_lead_id
            UNION
            SELECT l.*,
                   NULL AS `CustomFieldQuestion`,
                   NULL   AS `CustomFieldAnswer`
            FROM leads l
                     $searchTextJoin
                     LEFT OUTER JOIN (SELECT email, MAX(id) AS id FROM leads WHERE account = :companyId GROUP BY email) AS lj ON lj.id = l.id
                     LEFT OUTER JOIN objectregister o ON l.objectregister_id = o.id
                     LEFT OUTER JOIN statusdef s ON o.statusdef_id = s.id AND s.value IN ('active', 'fraud', 'inactive', 'blacklisted' )
            WHERE l.id NOT IN (SELECT lcf.leads_id FROM lead_customfield_content lcf)
            $and_lead_id
            ) AS ldquery ORDER BY  ldquery.id DESC
            $_limit
         */



        if (strtolower($_ENV[ 'APP_ENV' ]) === 'dev') {
            try {
                file_put_contents(__DIR__ . "/../../var/log/sql.log", $sql);
            } catch (Exception $e) {
                // do nothing
            }
        }

        $this->logger->info('$sql', [$sql, __METHOD__, __LINE__]);
        $this->logger->info('sql params', [
            [
                '$companyId' => $companyId,
                '$searchText' => $searchText,
                '$dateFrom' => $dateFrom,
                '$dateTo' => $dateTo . ' 23:59:59.999',//to make today's selection
                '$filterStatus' => $filterStatus,
                '$lead_id' => $lead_id,
                '$lead_id_operator' => $lead_id_operator,
                '$limit' => $limit,
            ],
            __METHOD__,
            __LINE__,
        ]);


        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':companyId', $companyId, PDO::PARAM_INT);
        $stmt->bindParam(':dateFrom', $dateFrom, PDO::PARAM_STR);
        $stmt->bindParam(':dateTo', $dateTo, PDO::PARAM_STR);


        $t1 = microtime(true);
        $stmt->execute();
        $res = $stmt->fetchAll();
        $t2 = microtime(true);

        if (strtolower($_ENV[ 'APP_ENV' ]) === 'dev') {
            try {
                // file_put_contents(__DIR__ . "/../../var/log/sql.log", 'PARAMS: ' . print_r(['$companyId' => $companyId, '$dateFrom' => $dateFrom, '$dateTo' => $dateTo]), FILE_APPEND);
                $time = $t2 - $t1;
                file_put_contents(__DIR__ . "/../../var/log/sql.log", 'PARAMS: ' . print_r([
                        '$companyId' => $companyId,
                        '$dateFrom' => $dateFrom,
                        '$dateTo' => $dateTo,
                        'executiontime [s]' => $time,
                    ], true), FILE_APPEND);
            } catch (Exception $e) {
                // do nothing
            }
        }


        $this->logger->info('$res', [$res, __METHOD__, __LINE__]);
        $this->logger->info('rowcount', [$stmt->rowCount(), __METHOD__, __LINE__]);
        $this->logger->info('errorInfo', [$stmt->errorInfo(), __METHOD__, __LINE__]);


        if (empty($res) || !is_array($res)) {
            return $contacts;
        }

        //@internal - format data according to the table view

        $duplicateEmail = [];
        $duplicateLeadId = 0;
        $duplicateEntries = [];
        $iteration = 0;
        $leadIds = [];
        $minId = 0;
        $maxId = 0;

        foreach ($res as $lead) {
            $leadIds[] = $lead[ 'id' ];

            if ($iteration === 0) {
                $contacts [] = $lead;
                $duplicateEntries[] = [
                    'email' => $lead[ 'email' ],
                    'id' => (int)$lead[ 'id' ],
                ];
                $duplicateEmail [] = $lead[ 'email' ];
                $duplicateLeadId = (int)$lead[ 'id' ];
            }

            if (($duplicateLeadId !== (int)$lead[ 'id' ]) && (in_array($lead[ 'email' ], $duplicateEmail, true))) {
                $iteration ++;
                continue;
            }

            $contacts [] = $lead;
            if (!in_array($lead[ 'email' ], array_column($duplicateEntries, 'email'), true)) {
                $duplicateEntries[] = [
                    'email' => $lead[ 'email' ],
                    'id' => (int)$lead[ 'id' ],
                ];
            }

            $duplicateEmail[] = trim($lead[ 'email' ]);
            $duplicateLeadId = (int)$lead[ 'id' ];

            $iteration ++;
        }

        sort($leadIds);
        $minId = $leadIds[ 0 ];
        $maxId = $leadIds[ count($leadIds) - 1 ];
        $this->logger->info('$contacts', [$contacts, __METHOD__, __LINE__]);
        $this->logger->info('$minId', [$minId, __METHOD__, __LINE__]);
        $this->logger->info('$maxId', [$maxId, __METHOD__, __LINE__]);
        return ['contacts' => $contacts, 'minId' => $minId, 'maxId' => $maxId];
    }


    /**
     * @param int $companyId
     * @param string $searchText
     * @param string $dateFrom
     * @param string $dateTo
     * @param string $filterStatus
     * @param int $limit
     * @return int row count of selection
     */
    public function getCountPagingLeadsForContactOverview(
        int $companyId,
        string $searchText,
        string $dateFrom,
        string $dateTo,
        string $filterStatus
    ): int {
        $this->logger->info('PARAMS',
            [$companyId, $searchText, $dateFrom, $dateTo, $filterStatus, __METHOD__, __LINE__]);

        if (empty($searchText)) {
            $searchTextJoin = '';
        } else {
            $searchTextJoin = "JOIN searchleads sl ON sl.id = l.id AND MATCH(searchtext) AGAINST('{$searchText}')";
        }

        if (empty($filterStatus)) {

            $filterStatus = "IN ('active', 'fraud', 'inactive', 'blacklisted' )";
        } else {
            $filterStatus = "= '{$filterStatus}'";
        }

        $contacts = [];
        if ($companyId <= 0) {
            return $contacts;
        }

        $sql = "
        SELECT count(*) FROM (
            
            SELECT l.*,
                   cf.fieldname                 AS `CustomFieldQuestion`,
                   lcf.lead_customfield_content AS `CustomFieldAnswer`
            FROM lead_customfield_content lcf
                     JOIN leads l ON l.id = lcf.leads_id
                     $searchTextJoin  
                     JOIN customfields cf ON lcf.customfields_id = cf.id
                     JOIN objectregister o ON l.objectregister_id = o.id
                     JOIN statusdef s ON o.statusdef_id = s.id AND s.value $filterStatus
                     WHERE l.account=:companyId
                     AND l.created BETWEEN :dateFrom AND :dateTo 
               
            UNION
            
            SELECT l.*,
                   'CustomFieldQuestion' AS `CustomFieldQuestion`,
                   'CustomFieldAnswer'   AS `CustomFieldAnswer`
            FROM leads l
                     $searchTextJoin
                     JOIN (SELECT email, MAX(id) AS id FROM leads WHERE account = :ucompanyId GROUP BY email) AS lj ON lj.id = l.id
                     JOIN objectregister o ON l.objectregister_id = o.id
                     JOIN statusdef s ON o.statusdef_id = s.id AND s.value $filterStatus
            WHERE l.id NOT IN (SELECT lcf.leads_id FROM lead_customfield_content lcf)
           
            ) AS ldquery
            ";


//        try {
//            file_put_contents(__DIR__ . "/../../var/log/countsql.log", $sql);
//        } catch (Exception $e) {
//            ;
//        }

        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':companyId', $companyId, PDO::PARAM_INT);
        $stmt->bindParam(':dateFrom', $dateFrom, PDO::PARAM_STR);
        $stmt->bindParam(':dateTo', $dateTo, PDO::PARAM_STR);

        $stmt->bindParam(':ucompanyId', $companyId, PDO::PARAM_INT);

        $stmt->execute();
        $count = $stmt->fetchOne();
        return $count;
    }


    /**
     * Filters the duplicate leads.
     * Due to join between lead and customfields, there are several duplicate lead entries with different customfields.
     * This functions merges the all the customfields related to single lead_id into single lead entry.
     * @param array $leads
     * @param array $customFields
     * @param string $uniqueIdentifier
     * @return array|null
     * @author Pradeep
     */
    public function appendContactWithCustomFields(
        array $leads,
        array $customFields,
        string $uniqueIdentifier = 'id'
    ): ?array {
        $formatted_leads = [];
        $tmp_lead_id = '';
        $i = 0;

        if (empty($leads)) {
            $this->logger->info('NO Leads found', [__METHOD__, __LINE__]);
            return $formatted_leads;
        }
        foreach ($leads as $lead) {
            if (empty($lead[ $uniqueIdentifier ])) {
                continue;
            }
            if ($lead[ $uniqueIdentifier ] !== $tmp_lead_id) {
                $i ++;
                $formatted_leads[ $i ] = $lead;
                $tmp_lead_id = $lead[ $uniqueIdentifier ];
                // SQL query (in function getLeadsByCampaignObjectRegisterId) fetches the Campaign status instead of Lead status,
                // Hence lead status is fetched hear again.
                $formatted_leads[ $i ][ 'Status' ] = $this->getLeadStatusById((int)$lead[ 'id' ]);
                if (isset($formatted_leads[ $i ][ 'CampaignObjRegId' ])) {
                    unset($formatted_leads[ $i ][ 'CampaignObjRegId' ]);
                }

                unset(
                    $formatted_leads[ $i ][ 'account' ],
                    $formatted_leads[ $i ][ 'campaign_id' ],
                    $formatted_leads[ $i ][ 'id' ]
                );

                $formatted_leads[ $i ][ 'Actions' ] = $lead[ 'id' ];
            }
            // Contacts with duplicate lead/contact Id but different customfields.
            // Hence, merging customfields into single contact.
            if (isset($lead[ 'CustomFieldQuestion' ], $lead[ 'CustomFieldAnswer' ])) {
                $custom_field_name = $lead[ 'CustomFieldQuestion' ];
                $custom_field_value = $lead[ 'CustomFieldAnswer' ];
                foreach ($customFields as $customfield) {
                    // Deleting customfields only make the fieldname empty in customfield table.
                    // So just ignore the empty field name entries.
                    if (empty($customfield[ 'fieldname' ])) {
                        continue;
                    }
                    if (empty($formatted_leads[ $i ][ $customfield[ 'fieldname' ] ]) &&
                        ($customfield[ 'fieldname' ] === $custom_field_name)) {
                        $formatted_leads[ $i ][ $customfield[ 'fieldname' ] ] = $custom_field_value;
                    }
                    if (!isset($formatted_leads[ $i ][ $customfield[ 'fieldname' ] ])) {
                        $formatted_leads[ $i ][ $customfield[ 'fieldname' ] ] = '';
                    }
                }
                unset(
                    $formatted_leads[ $i ][ 'CustomFieldQuestion' ],
                    $formatted_leads[ $i ][ 'CustomFieldAnswer' ],
                    $formatted_leads[ $i ][ 'account' ],
                    $formatted_leads[ $i ][ 'campaign_id' ],
                    $formatted_leads[ $i ][ 'id' ]
                );
            }
        }

        unset($lead);
        return $formatted_leads;
    }

    public function getLeadStatusById(int $leadId): ?string
    {
        $leadStatus = 'fraud';
        try {
            if ($leadId <= 0) {
                throw new RuntimeException('Invalid LeadId provided');
            }
            $sql = 'SELECT s.value as status
                    FROM leads l
                    join objectregister o on l.objectregister_id = o.id
                    join statusdef s on o.statusdef_id = s.id
                    where l.id = :lead_id
            ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':lead_id', $leadId);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result) && $stmt->rowCount() > 0) {
                $leadStatus = $result[ 0 ][ 'status' ];
            }
            return $leadStatus;
        } catch (Exception|RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $leadStatus;
        }
    }

    /**
     * @return array
     * @author Pradeep
     * @internal Currently contacts should be present in one of the bellow mentioned status.
     */
    public function getContactStatus(): array
    {
        return [
            'c_active' => StatusDefRepository::ACTIVE,
            'c_inactive' => StatusDefRepository::INACTIVE,
            'c_fraud' => StatusDefRepository::FRAUD,
            'c_blacklisted' => StatusDefRepository::STATUSDEF_BLACKLISTED,
        ];
    }

}
