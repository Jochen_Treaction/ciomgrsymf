<?php


namespace App\Repository;


use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * @property LoggerInterface logger
 * @property Connection conn
 */
class DomainsRepository
{
    const DEFAULT_USER_ID = 99999;
    protected int $currentUserId;

    public function __construct(LoggerInterface $logger, Connection $connection)
    {
        $this->logger = $logger;
        $this->conn = $connection;
        $this->currentUserId = self::DEFAULT_USER_ID;
    }

    /**
     * set class <b>global current user id</b>, if before you run <b style="color:blue"><u>inserts</u> or <u>updates</u></b>, otherwise a default user id (99999) is used
     * @param int $user_id users.id
     */
    public function setRepostitoriesCurrentUserId(int $user_id)
    {
        $this->currentUserId = $user_id;
    }

    /**
     * @param int $company_id
     * @param string $seed_pool_group_id
     * @param string $healthStatus
     * @param string $domainStatus
     * @param string $search_string
     * @return int
     * @internal - Get count of the records for total number of records for pagination bar
     * @author Aki
     */
    public function getTotalNumberOfRecordsForView(
        int    $company_id,
        string $seed_pool_group_id,
        string $healthStatus,
        string $domainStatus,
        string $search_string
    ): int
    {
        if ('all' === strtolower(trim($seed_pool_group_id))) {
            $andSeedPoolGroupIdSelection = '';
        } else {
            $andSeedPoolGroupIdSelection = "
            JOIN inbox_monitor im on d.id = im.domain_id
            JOIN seed_pool sp on im.seed_pool_id = sp.id AND sp.seed_pool_group_id = $seed_pool_group_id";
        }


        if ('all' === strtolower(trim($healthStatus))) {
            $andBlacklistStatus = '';
        } else if ($healthStatus === 'cyren_blacklisted') {
            $andBlacklistStatus = "AND dbl.cyren_blacklisted = 1";
        } else {
            $andBlacklistStatus = 'AND dbl.public_bls_blacklisted = 1';
        }

        if ('all' === strtolower(trim($domainStatus))) {
            $andDomainStatus = '';
        } else {
            $andDomainStatus = "JOIN objectregister o ON d.objectregister_id = o.id
                                JOIN statusdef s ON o.statusdef_id = s.id AND s.value = '$domainStatus'";
        }


        if (empty($search_string)) {
            $searchStringSelection = '';
        } else {
            $searchStringSelection = "JOIN search_domains sd on dbl.id = sd.id and match(searchtext) against ('{$search_string}')";
        }

        $count = 0;

        $select = "SELECT  count(DISTINCT domain) as count 
                                    FROM domain_blacklists dbl
                                    JOIN domains d ON dbl.domains_id = d.id
                                    $andSeedPoolGroupIdSelection
                                    $andDomainStatus
                                    $searchStringSelection
                                    WHERE d.company_id = :company_id $andBlacklistStatus
                                    ";

        if ($_ENV['APP_ENV'] === 'dev') {
            try {
                $this->logger->info('domainSql', [$select, __METHOD__, __LINE__]);
                file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . '../../var/log/selectInboxMonitor.sql', $select . "\ncompany_id=$company_id");
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }


        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $count = $stmt->fetchColumn();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        $this->logger->info('getTotalNumberOfRecordsForView inbox_monitor', [$count, __METHOD__, __LINE__]);
        return $count;
    }


    /**
     * @param int $company_id
     * @param string $seed_pool_group_id
     * @param string $healthStatus
     * @param string $domainStatus
     * @param string $search_string
     * @param int $offset
     * @param int $limit
     * @return array
     * @internal Returns inbox monitor for a selection with limit and offset - No need to store anything in cache
     * @author Aki
     */
    public function getDomainsPerPageForView(
        int    $company_id,
        string $seed_pool_group_id,
        string $healthStatus,
        string $domainStatus,
        string $search_string,
        int    $offset,
        int    $limit
    ): array
    {
        if ('all' === strtolower(trim($seed_pool_group_id))) {
            $andSeedPoolGroupIdSelection = '';
        } else {
            $andSeedPoolGroupIdSelection = "
            JOIN inbox_monitor im on d.id = im.domain_id
            JOIN seed_pool sp on im.seed_pool_id = sp.id AND sp.seed_pool_group_id = $seed_pool_group_id";
        }


        if ('all' === strtolower(trim($healthStatus))) {
            $andBlacklistStatus = '';
        } else if ($healthStatus === 'cyren_blacklisted') {
            $andBlacklistStatus = "AND dbl.cyren_blacklisted = 1";
        } else {
            $andBlacklistStatus = 'AND dbl.public_bls_blacklisted = 1';
        }

        if ('all' === strtolower(trim($domainStatus))) {
            $andDomainStatus = '';
        } else {
            $andDomainStatus = "JOIN objectregister o ON d.objectregister_id = o.id
                                JOIN statusdef s ON o.statusdef_id = s.id AND s.value = '$domainStatus'";
        }


        if (empty($search_string)) {
            $searchStringSelection = '';
        } else {
            $searchStringSelection = "JOIN search_domains sd on dbl.id = sd.id and match(searchtext) against ('{$search_string}')";
        }

        //This is limit and offset
        $_limit = "LIMIT $offset, $limit";


        $select = "SELECT DISTINCT 
                       d.received,
                       d.domain as name,
                       d.seed_pool as seedpool,
                       d.server_ip_address as ip,
                       dbl.cyren_blacklisted as cyrenBlacklist,
                       dbl.cyren_url_categories as cyrenCategories,
                       dbl.cyren_last_check as cyrenLastUpdate,
                       dbl.public_bls_blacklisted as publicBlacklist,
                       dbl.public_bls_listed as publicBlacklistCategories,
                       dbl.public_bls_last_check as publicBlacklistLastUpdate,
                       d.id as domainId
                FROM domain_blacklists dbl
                JOIN domains d on dbl.domains_id = d.id
                $andSeedPoolGroupIdSelection
                $andDomainStatus
                $searchStringSelection
                WHERE d.company_id = :company_id $andBlacklistStatus
                ORDER BY d.received DESC
                $_limit
                ";

        if ($_ENV['APP_ENV'] === 'dev') {
            try {
                $this->logger->info('domainSql', [$select, __METHOD__, __LINE__]);
                file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . '../../var/log/viewDomainsSelection.sql', $select . "\ncompany_id=$company_id");
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }


        $records = [];
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->execute();
            $records = $stmt->fetchAll();
            return $records;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $records;
        }
    }

}