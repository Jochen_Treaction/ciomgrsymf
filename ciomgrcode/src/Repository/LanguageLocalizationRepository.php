<?php

namespace App\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;
use RuntimeException;

class LanguageLocalizationRepository
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $connection;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger
    ) {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    /**
     * insert to language_localization
     * @param int $langId
     * @param int $objRegId
     * @param int $companyId
     * @param int $userId
     * @param bool $isDefault
     * @return int|null
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    public function insert(int $langId, int $objRegId, int $companyId, int $userId, bool $isDefault): ?int
    {
        try {
            if ($langId <= 0 || $companyId <= 0) {
                throw new RuntimeException("Invalid language id or CompanyId provided");
            }
            $sql = 'insert into language_localization
                        (objectregister_id, company_id, language_id,  is_default, created_by, updated_by)
                    value(:objectregister_id, :company_id, :language_id, :is_default, :created_by, :updated_by)';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(':objectregister_id', $objRegId, ParameterType::INTEGER);
            $stmt->bindValue(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->bindValue(':language_id', $langId, ParameterType::INTEGER);
            $stmt->bindValue(':is_default', $isDefault, ParameterType::BOOLEAN);
            $stmt->bindValue(':created_by', $userId, ParameterType::INTEGER);
            $stmt->bindValue(':updated_by', $userId, ParameterType::INTEGER);
            $status = $stmt->execute();
            if (!$status) {
                throw new RuntimeException("failed to insert into language_localization table");
            }
            return $this->connection->lastInsertId();

        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return null;
        }
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getAll(): array
    {
        return [];
    }

    /**
     * get single value from language_localization
     * @param int $id
     * @return array
     * @author Pradeep
     */
    public function get(int $id): array
    {
        $result = [];
        try {
            if ($id <= 0) {
                throw new RuntimeException('invalid id provided');
            }
            $sql = 'select * from language_localization where id=:id';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(':id', $id, ParameterType::INTEGER);
            if (!$stmt->execute()) {
                throw new RuntimeException('failed to execute select statement');
            }
            $rows = $stmt->fetchAll();
            if (!empty($rows) && isset($rows[ 0 ][ 'id' ])) {
                $result = $rows[ 0 ];
            }
        } catch (RuntimeException|Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $result;
    }

    /**
     * update the status of the language to 'default' or not.
     * @param int $id
     * @param bool $isDefault
     * @return bool
     * @author Pradeep
     */
    public function update(int $id, bool $isDefault): bool
    {
        try {
            if ($id <= 0) {
                throw new RuntimeException('invalid Id provided to update status of language localization');
            }
            $sql = '
                update language_localization
                    set is_default = :is_default
                    where id = :id;
            ';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue('is_default', $isDefault, ParameterType::BOOLEAN);
            $stmt->bindValue('id', $id, ParameterType::INTEGER);
            if (!$stmt->execute()) {
                throw new RuntimeException('failed to execute update sql statement');
            }
            return true;
        } catch (Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * deletes the entry from the table language_localization.
     * - id has to be the id of the language_localization table.
     * @param int $id
     * @return bool
     * @author Pradeep
     */
    public function delete(int $id): bool
    {
        try {
            if ($id <= 0) {
                throw new RuntimeException('Invalid id of language_localization table provided');
            }
            $sql = 'Delete from language_localization where id =:id';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(':id', $id, ParameterType::INTEGER);
            return $stmt->execute();
        } catch (Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return false;
        }
    }

    /**
     * extract the rows for the request-> languages/overview.
     * @param int $companyId
     * @return array
     * @throws Exception
     * @author Pradeep
     */
    public function overviewTwig(int $companyId): array
    {
        if ($companyId <= 0) {
            return [];
        }

        try {
            $sql = '
                select 
                       l.iso_code, 
                       l.name, 
                       s.value as lang_status, 
                       l.id as lang_id, 
                       ll.id as loc_lang_id, 
                       o.id as obj_reg_id
                from language_localization as ll
                         join languages l on ll.language_id = l.id
                         join objectregister o on ll.objectregister_id = o.id
                         join object_register_meta orm on ll.objectregister_id = orm.object_register_id
                         join statusdef s on o.statusdef_id = s.id
                where ll.company_id = :company_id';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }

    /**
     * getDefaultLanguage -> fetches the row which has is_default as true.
     * Only one language for a company can be set to default.
     * @return array
     * @author Pradeep
     */
    public function getDefaultLanguage(int $companyId): array
    {
        $isDefault = true;
        $defaultLang = [];
        try {
            if ($companyId <= 0) {
                throw new RuntimeException('Invalid companyId of language_localization table provided');
            }
            $sql = 'select * from language_localization where is_default=:is_default and company_id = :company_id';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(':is_default', $isDefault, ParameterType::BOOLEAN);
            $stmt->bindValue('company_id', $companyId, ParameterType::INTEGER);
            if (!$stmt->execute()) {
                throw new RuntimeException('failed to fetch default language');
            }
            $rows = $stmt->fetchAll();
            if (!empty($rows) && isset($rows[ 0 ][ 'id' ])) {
                $defaultLang = $rows[ 0 ];
            }
        } catch (Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $defaultLang;
    }


    /**
     * getNonConfiguredLanguages fetches the list of unconfigured list of languages
     * to configure for a company.
     * - This list is used in the wizard of language localization in dropdown.
     * @param int $companyId
     * @return array
     * @author Pradeep
     */
    public function getNonConfiguredLanguages(int $companyId): array
    {
        try {
            if ($companyId <= 0) {
                throw new RuntimeException('Invalid companyId of language_localization table provided');
            }
            $sql = '
                SELECT *
                FROM languages l
                WHERE l.id > 0
                  and l.id NOT IN (
                      select ll.language_id 
                      from language_localization ll
                      where ll.id > 0 and ll.company_id =:company_id );
            ';
            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue(':company_id', $companyId, ParameterType::INTEGER);
            if (!$stmt->execute()) {
                throw new RuntimeException('failed to fetch non configured languages');
            }
            return $stmt->fetchAll();
        } catch (Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }

    }
}