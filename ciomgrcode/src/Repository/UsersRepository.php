<?php

namespace App\Repository;

use App\Entity\Company;
use App\Entity\Users;
use App\Entity\AdministratesUsers;
use App\Entity\Securelog;
use App\Types\UniqueObjectTypes;
use App\Types\UserStatusTypes;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use GeoIp2\Exception\AddressNotFoundException;
use GeoIp2\Exception\AuthenticationException;
use GeoIp2\Exception\GeoIp2Exception;
use GeoIp2\Exception\HttpException;
use GeoIp2\Exception\InvalidRequestException;
use GeoIp2\Exception\OutOfQueriesException;
use GeoIp2\WebService\Client;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


// currently injected, but not used here
use App\Services\AppCacheService;
use Symfony\Component\Security\Core\User\User;

// use Symfony\Component\Security\Core\User\UserInterface;

class UsersRepository extends ServiceEntityRepository
{


    private $logger;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $password_encoder;
    private $users;
    private $apcuCacheService;
    /**
     * @var Client
     * @author Pradeep
     */
    private $geoIpClient;
    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var UserMetaRepository
     * @author Pradeep
     */
    private $userMetaRepository;


    public function __construct(
        ManagerRegistry $registry,
        LoggerInterface $logger,
        UserPasswordEncoderInterface $passwordEncoder,
        AppCacheService $apcuCacheService,
        UserMetaRepository $userMetaRepository,
        Connection $connection
    ) {
        parent::__construct($registry, Users::class);
        $this->logger = $logger;
        $this->conn = $connection;
        $this->userMetaRepository = $userMetaRepository;
        $this->password_encoder = $passwordEncoder;
        $this->apcuCacheService = $apcuCacheService;
        $this->geoIpClient = new Client(346418, 'NrHslBO4wNzcA53C');
    }


    /**
     * @param int $id
     * @return mixed
     * @uses      Users, AdministratesUsers
     * @internal  Delete a user from Users by his user-id and company-id! This delete is supposed to fail in case a user with that company assigned does not exists in Users.
     *            But that record can exist at least in AdministratesUsers (invited_user_id, to_company_id) for a invited user to that company and must be deleted here.
     */
    public function deleteUserByIdAndCompanyInUsersAndAdministratesUsers(int $id, int $company_id)
    {
        try {
            $emComp = $this->getEntityManager();
            $query = $emComp->createQuery('SELECT c 
                FROM App\Entity\Company c
                WHERE c.id = :id')->setParameter('id', $company_id);
            $company = $query->getOneOrNullResult();

            $em = $this->getEntityManager();
			$query = $em->createQuery('DELETE 
                FROM App\Entity\AdministratesUsers ac 
                WHERE ac.invitedUser = :invited_user_id 
                AND ac.toCompany = :to_company_id')->setParameters([
				'invited_user_id' => $id,
				'to_company_id' => $company_id,
			]);
            $query->execute();

			$query = $em->createQuery('DELETE   
                FROM App\Entity\Users u
                WHERE u.id = :id
                AND u.company = :company')->setParameters(['id' => $id, 'company' => $company]);
            $ret = $query->execute();

            $this->logger->info('$ret', [$ret, __METHOD__, __LINE__]); // todo: remove

            return $ret;
        } catch (NonUniqueResultException $e) {
            $this->logger->error($e->getMessage());
        }
    }


    /**
     * getUserRolesForCompany
     * @param string $currentUserEmail
     * @param int    $switchedToCompanyId
     * @return array ['origin' => ('users'|'administrates_users'), 'roles' => <roles-json-string>]
     * @internal WEB-4088
     * @author   jsr
     */
    public function getUserRolesForCompany(string $currentUserEmail, int $switchedToCompanyId): array
    {
        $this->logger->info('PARAMS $currentUserEmail/$switchedToCompanyId', [$currentUserEmail,$switchedToCompanyId,__METHOD__, __LINE__]);
        $res = [];
        $sql = "SELECT 
                'users' AS 'origin',
                u.roles
            FROM users u
            JOIN company c on u.company_id = c.id AND c.id = :switchedToCompanyId
            UNION
            SELECT 
                'administrates_users' AS 'origin',
                au.with_roles AS roles
            FROM administrates_users  au
            WHERE au.to_company_id = :switchedToCompanyId
            AND au.invited_user_id IN (SELECT u2.id FROM users u2 WHERE email = :currentUserEmail )";

        try {
            $stmt = $this->conn->prepare($sql);

            $stmt->bindParam('currentUserEmail', $currentUserEmail, ParameterType::STRING);
            $stmt->bindParam('switchedToCompanyId', $switchedToCompanyId, ParameterType::INTEGER);
            $stmt->execute();
            $res = $stmt->fetchAll();
            $this->logger->info('RES', [$res, __METHOD__, __LINE__]);
            return $res;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $res;
    }


    /**
     * @param int   $id
     * @param array $userData
     * @return Users|null
     * @internal WEB-3850, used by route /admintool/master/ajax/updateuser in AdministrationController
     */
    public function updateUserById(int $id, array $userData): ?Users
    {
        $em = $this->getEntityManager();
        $user = $this->getUserById($id);
        /*
        0: Object { name: "manageusers_action", value: "edit_user" }
        1: Object { name: "selected_company", value: "45" }
        2: Object { name: "selected_user", value: "MTMz" }
        3: Object { name: "salutation", value: "Mr." }
        4: Object { name: "first_name", value: "Öttö" }
        5: Object { name: "last_name", value: "Wälkes" }
        6: Object { name: "email", value: "otto.walkes@emden.de" }
        7: Object { name: "role", value: "USER" }
        8: Object { name: "to_company", value: "NTI=" } 52
        9: Object { name: "token", value: "MtKVN9gDSk7LlDq7e0AXXEbeYPq5NPDlbXNX7ZEJSto" }
        */
        // $company = $this->getEntityName()
        $company = $this->getCompanyById($userData['to_company']);

        if (null !== $user && null !== $company) {
            try {
                $user->setSalutation($userData['salutation']);
                $user->setFirstName($userData['first_name']);
                $user->setLastName($userData['last_name']);
                $user->setEmail($userData['email']);
                $user->setRoles($userData['role']);
                $user->setCompany($company);
                $em->persist($user);
                $em->flush();
                $em->clear();
                return $user;
            } catch (MappingException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            } catch (OptimisticLockException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            } catch (ORMException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }
        return null;
    }


    /**
     * @param int $id
     * @return Users|null
     */
    public function getUserById(int $id): ?Users
    {
        try {
            $em = $this->getEntityManager();
            $query = $em->createQuery('SELECT u 
                FROM App\Entity\Users u
                WHERE u.id = :id')->setParameter('id', $id);
            $ret = $query->getOneOrNullResult();

//            $x = ($ret instanceof  Users) ? 'ja user' : 'nein kein user';
//            $y = ($ret === null )? 'ja null' : 'nein nicht null';
//            $this->logger->info('$ret', [$x,$y, __METHOD__,__LINE__]);
            return $ret;
        } catch (NonUniqueResultException $e) {
            $this->logger->error($e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return Company|null
     */
    public function getCompanyById(int $id): ?Company
    {
        try {
            $em = $this->getEntityManager();
            $query = $em->createQuery('SELECT c 
                FROM App\Entity\Company c
                WHERE c.id = :id')->setParameter('id', $id);
            $ret = $query->getOneOrNullResult();

            return $ret;
        } catch (NonUniqueResultException $e) {
            $this->logger->error($e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return Users|null
     */
    public function unblockUserById(int $id): ?Users
    {
        $em = $this->getEntityManager();
        $user = $this->getUserById($id);
        /*
        0: Object { name: "manageusers_action", value: "edit_user" }
        1: Object { name: "selected_company", value: "45" }
        2: Object { name: "selected_user", value: "MTMz" }
        3: Object { name: "salutation", value: "Mr." }
        4: Object { name: "first_name", value: "Öttö" }
        5: Object { name: "last_name", value: "Wälkes" }
        6: Object { name: "email", value: "otto.walkes@emden.de" }
        7: Object { name: "role", value: "USER" }
        8: Object { name: "to_company", value: "NTI=" } 52
        9: Object { name: "token", value: "MtKVN9gDSk7LlDq7e0AXXEbeYPq5NPDlbXNX7ZEJSto" }
        */

        if (null !== $user) {
            try {
                $user->setFailedLogins('0');
                $user->setStatus(UserStatusTypes::ACTIVE);
                $em->persist($user);
                $em->flush();
                $em->clear();
                return $user;
            } catch (MappingException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            } catch (OptimisticLockException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            } catch (ORMException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }
        return null;
    }

    /**
     * @param int $userId
     * @param string $userStatus
     * @return bool
     * @author Pradeep
     */
    public function updateStatus(int $userId, string $userStatus): bool
    {
    	$this->logger->info('PARAMS',[$userId,$userStatus, __FUNCTION__, __LINE__]);

        try {
            if ($userId <= 0 || empty($userStatus)) {
                throw new RuntimeException('Invalid UserId or Userstatus provided');
            }
            $sql = 'UPDATE users SET status = :userstatus WHERE id = :id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':userstatus', $userStatus);
            $stmt->bindParam(':id', $userId, ParameterType::INTEGER);
            return $stmt->execute();
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param int $id
     * @return Users|null
     */
    public function blockUserById(int $id): ?Users
    {
        $em = $this->getEntityManager();
        $user = $this->getUserById($id);
        /*
        0: Object { name: "manageusers_action", value: "edit_user" }
        1: Object { name: "selected_company", value: "45" }
        2: Object { name: "selected_user", value: "MTMz" }
        3: Object { name: "salutation", value: "Mr." }
        4: Object { name: "first_name", value: "Öttö" }
        5: Object { name: "last_name", value: "Wälkes" }
        6: Object { name: "email", value: "otto.walkes@emden.de" }
        7: Object { name: "role", value: "USER" }
        8: Object { name: "to_company", value: "NTI=" } 52
        9: Object { name: "token", value: "MtKVN9gDSk7LlDq7e0AXXEbeYPq5NPDlbXNX7ZEJSto" }
        */

        if (null !== $user) {
            try {
                $user->setFailedLogins('999');
                $user->setStatus('blocked');
                $em->persist($user);
                $em->flush();
                $em->clear();
                return $user;
            } catch (MappingException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            } catch (OptimisticLockException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            } catch (ORMException $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }
        return null;
    }


    /**
     * adds entry to securelog and, in case of successful login, resets failed_logins to 0 for active users only
     * @param array $userRecord
     * @return Securelog|null
     * @throws MappingException
     * @throws ORMException
     * @throws OptimisticLockException
     * @author jsr
     */
    public function setSecureLogEntry(array $userRecord): ?Securelog
    {
        // early exit
        if (empty($userRecord)) {
            return null;
        }

        $em = $this->getEntityManager();
        $secureLog = new Securelog();
        $secureLog->setCompanyId($userRecord['company_id'])->setUserId($userRecord['user_id'])->setIp(empty($userRecord['ip']) ? '' : $userRecord['ip'])->setLogindate(new DateTime(date('Y-m-d H:i:s')))->setResult(empty($userRecord['result']) ? '' : $userRecord['result']);

        $em->persist($secureLog);
        $em->flush();
        $em->clear();

        if ('success' === $userRecord['result']) {
            try {
                $em = $this->getEntityManager();
                $user = $em->getRepository(Users::class)->findOneBy(['id' => (int)$userRecord['user_id']]);

                if ($user instanceof Users) {
                    if (UserStatusTypes::ACTIVE === $user->getStatus()) {
                        $user->setFailedLogins(0);
                        $em->persist($user);
                    }

                    $em->flush();
                    $em->clear();
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
        return $secureLog;
    }


    /**
     * @param int $user_id
     * @param int $company_id
     * @return Securelog|null
     * @author jsr
     */
    public function getSecureLogEntry(int $user_id, int $company_id): ?Securelog
    {
        // early exit
        if (null === $user_id || null === $company_id) {
            return null;
        }

        $em = $this->getEntityManager();
        try {
            $query = $em->createQuery('SELECT
                        s.company_id,
                        s.id,
                        s.ip,
                        s.logindate,
                        s.result,
                        s.user_id
                    FROM
                        securelog s
                        WHERE s.user_id = :user_id
                        AND s.company_id = :company_id')->setParameter('user_id', $user_id)->setParameter('company_id', $company_id);
            $ret = $query->getOneOrNullResult();
            return $ret;
        } catch (NonUniqueResultException $e) {
            $this->logger->error($e->getMessage());
        }
    }


    /**
     * todo: not used, may be removed
     * @param array   $user
     * @param Company $company
     * @return Users|null
     * @throws Exception
     */
    public function createUser(array $user, Company $company): ?Users
    {
        if (empty($user) || empty($user['email']) || $company === null) {
            return null;
        }
        $em = $this->getEntityManager();
        $time_stamp = date('Y-m-d h:i:s');
        $entity_usr = new Users();
        try {
            $entity_usr->setEmail($user['email']);
            $entity_usr->setFirstName($user['f_name']);
            $entity_usr->setLastName($user['l_name']);
            $entity_usr->setCompany($company);
            $entity_usr->setFailedLogins('0');
            // Encode password
            $pw = $this->password_encoder->encodePassword($entity_usr, $user['pass']);
            $entity_usr->setPassword($pw);
            $entity_usr->setRoles(['ROLE_USER']);
            //$entity_usr->setCreated(DateTime::createFromFormat('Y-m-d h:i:s', $time_stamp));
            //$entity_usr->setUpdated(DateTime::createFromFormat('Y-m-d h:i:s', $time_stamp));
            $em->persist($company);
            $em->persist($entity_usr);
            $em->flush();
            $em->clear();
        } catch (ORMException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        } catch (OptimisticLockException $op) {
            $this->logger->error($op->getMessage(), [__METHOD__, __LINE__]);
        } catch (MappingException $m) {
            $this->logger->error($m->getMessage(), [__METHOD__, __LINE__]);
        }
        return $entity_usr;
    }


    /**
     * todo: not used, may be removed
     * updates one Users entity
     * @param Users $user
     * @return Users|null
     * @author jsr
     */
    public function updateUser(Users $user): ?Users
    {
        try {
            $em = $this->getEntityManager();
            $em->persist($user);
            $em->flush();
            $em->clear();
            return $user;
        } catch (MappingException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        } catch (OptimisticLockException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        } catch (ORMException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }

        return null;
    }


    /**
     * @param array   $user
     * @param Company $company
     * @param array   $roles
     * @return Users|null
     * @author jsr
     */
    public function createNewUser(array $user, Company $company, array $roles): ?Users
    {
        $retUser = null;
        $checkEmail = filter_var(htmlentities($user['email']), FILTER_VALIDATE_EMAIL);
        $checkEmail = filter_var($checkEmail, FILTER_SANITIZE_EMAIL);

        $email = ($user['email'] === $checkEmail) ? $checkEmail : '';
        if (empty($email)) {
            $this->logger->error('INVALID EMAIL ADDRESS.', [__METHOD__, __LINE__]);
            return null;
        }

        $company_id = $company->getId();
        $userEntity = new Users();

        //$pw = $this->password_encoder->encodePassword($userEntity, $user['pass']);
        $pw = '';//$this->password_encoder->encodePassword($userEntity, $user['pass']);
        $roles = json_encode($roles); // WEB-3850
        $first_name = htmlentities($user['f_name']);
        $last_name = htmlentities($user['l_name']);
        $salutation = htmlentities($user['salutation']);
        $registration = '';
        $login_date = date('Y-m-d H:i:s');
        $phone = '';
        $mobile = '';
        $failed_logins = 0;
        $super_admin = 0;
        $permission = htmlentities($user['permission']);
        $created_by = 999999990;
        $updated_by = 999999990;

        try {
            $this->conn->beginTransaction();
            $sql = 'INSERT INTO users (company_id, email, password, roles, first_name, last_name, salutation, registration,login_date, phone, mobile, failed_logins, super_admin,permission ,created_by, updated_by) VALUES (:company_id, :email, :passwd, :roles, :first_name, :last_name, :salutation, :registration, :login_date, :phone, :mobile, :failed_logins, :super_admin, :permission ,:created_by, :updated_by)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('company_id', $company_id);
            $stmt->bindParam('email', $email);
            $stmt->bindParam('passwd', $pw);
            $stmt->bindParam('roles', $roles);
            $stmt->bindParam('first_name', $first_name);
            $stmt->bindParam('last_name', $last_name);
            $stmt->bindParam('salutation', $salutation);
            $stmt->bindParam('registration', $registration);
            $stmt->bindParam('login_date', $login_date);
            $stmt->bindParam('phone', $phone);
            $stmt->bindParam('mobile', $mobile);
            $stmt->bindParam('failed_logins', $failed_logins);
            $stmt->bindParam('super_admin', $super_admin);
            $stmt->bindParam('permission', $permission);
            $stmt->bindParam('created_by', $created_by);
            $stmt->bindParam('updated_by', $updated_by);
            $insert = $stmt->execute();

            $rowCount = $stmt->rowCount();
            if ($insert && $rowCount >= 1) {
                $this->conn->commit();
                $retUser = $this->getUserByEmail($email);
            } else {
                $this->logger->error(json_encode($this->conn->errorInfo()), [__METHOD__, __LINE__]);
                $this->conn->rollBack();
            }
            //return $retUser;
        } catch (Exception $e) {
            echo $e->getMessage();
            die('error');
            $this->logger->error($e->getMessage());
        }
        return $retUser;
    }


    /**
     * @param string $email
     * @return Users|null
     * @throws NonUniqueResultException
     * @author jsr
     */
    public function getUserByEmail(string $email): ?Users
    {
		$this->logger->info("getUserByEmail PARAM = ", [$email, __METHOD__, __LINE__]);
        if (empty($email)) {
            return null;
        }

        try {
            $em = $this->getEntityManager();
            $query = $em->createQuery('SELECT u 
                FROM App\Entity\Users u
                WHERE u.email = :email')->setParameter('email', $email);

            $this->logger->info('SQL=' , [print_r($query->getSQL(), true), __METHOD__,__LINE__]);

            $ret = $query->getOneOrNullResult();


            // $this->logger->info("USER_RECORD = ", [print_r((array)$ret, true), __METHOD__, __LINE__]);
            return $ret;
        } catch (NonUniqueResultException $e) {
            $this->logger->error($e->getMessage());
        }
    }


	/**
	 * @param int $company_id
	 * @return array
	 */
	public function getAllUserIdsByCompanyId(int $company_id):array
	{
		try {
			$sql = 'SELECT id 
                    FROM users 
                    WHERE company_id= ?';
			$stmt = $this->conn->prepare($sql);
			$stmt->bindParam(1, $company_id);
			$stmt->execute();
			$res = $stmt->fetchAll();

			$this->logger->info('$res', [print_r($res,true), __METHOD__, __LINE__]);
			return $res;
		} catch (Exception $e) {
			$this->logger->error($e->getMessage());
			return [];
		}
    }


	/**
	 * check if $email is assigned a company or not. return null if $email does not exist in users. Return true, if user exists but does not have company assigned => zombie. Return false, if user exists and has company => no zombie
	 * @param $email
	 * @return bool|null
	 */
    public function isZombieUserRecord($email)
	{
		$sql = "SELECT count(*) 
				FROM users u 
				JOIN company c on u.company_id = c.id
				WHERE email = :email";
		$stmt = $this->conn->prepare($sql);
		$stmt->bindParam(':email', $email);
		$stmt->execute();
		$res = $stmt->fetchOne();

		$this->logger->info('isZombieUserRecord', [$res, __METHOD__, __LINE__]);
		if( $res === 1) {
			// user exists and has company => no zombie
			return false;
		} elseif($res === 0 ) {
			// user exists but does not have company assigned => thats a zombie
			return true;
		} else {
			// user with that email does not exist
			return null;
		}
		return $res;
	}


    /**
     * @param UserInterface $user
     * @return void|null
     */
    /*    public function encodeUserPassword(): void
        {
            $user = new Users();
            $user->setPassword($this->password_encoder->encodePassword($user, $user->getPassword()));
        }
    */

    /**
     * @param string $email
     * @return bool
     * @author jsr
     */
    public function activateUser(Users $user): ?Users
    {
        $user->setFailedLogins('0');
        $user->setStatus(UserStatusTypes::ACTIVE);
        // $user->setUpdated(new DateTime('now'));
        try {
            $em = $this->getEntityManager();
            $em->persist($user);
            $em->flush();
            $em->clear();
            return $user;
        } catch (MappingException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        } catch (OptimisticLockException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        } catch (ORMException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }

        return null;
    }


	/**
	 * @param string $email
	 * @return bool
	 * @author jsr
	 */
	public function confirmUser(Users $user): ?Users
	{
		$user->setFailedLogins('0');
		$user->setStatus(UserStatusTypes::CONFIRMED);
		// $user->setUpdated(new DateTime('now'));
		try {
			$em = $this->getEntityManager();
			$em->persist($user);
			$em->flush();
			$em->clear();
			return $user;
		} catch (MappingException $e) {
			$this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
		} catch (OptimisticLockException $e) {
			$this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
		} catch (ORMException $e) {
			$this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
		}

		return null;
	}


    /**
     * @param int $user_id
     * @return array
     * @author PVA
     */
    public function getUserDetailsForUpdate(int $user_id): array
    {
        $details = [];

        if (empty($user_id)) {
            return $details;
        }
        try {
            $sql = 'SELECT * 
                    FROM users 
                    WHERE id= ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $user_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            return $res[0];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }


    /**
     * @param array $user_details
     * @return bool
     * @author PVA
     */
    public function updateUserDetails(array $user_details): bool
    {
        $status = false;
        if (empty($user_details)) {
            return $status;
        }
        try {
            $sql = 'UPDATE users
                    SET first_name= :first_name,
                    last_name = :last_name,
                    salutation= :salutation,
                    phone = :phone
                    WHERE id= :id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':first_name', $user_details['first_name']);
            $stmt->bindParam(':last_name', $user_details['last_name']);
            $stmt->bindParam(':salutation', $user_details['salutation']);
            $stmt->bindParam(':phone', $user_details['phone']);
            $stmt->bindParam(':id', $user_details['id']);
            $res = $stmt->execute();

            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $email
     * @return array [user_id, user_1stletter, first_name, last_name, user_roles, email, dpa, dpa_date, company_id, company_name, company_account_no, account_objectregister_id, project_objectregister_id] or [] empty array, if record was not found
     * @author       jsr
     * @internal     changed due to web-4088
     */
    public function getUserAndCompanyForApcu(string $email): array
    {
        $res = [];

        $selectedCompanyId = $this->apcuCacheService->get($this->apcuCacheService::SELECTED_COMPANY_ID_OF_USER . $email);

        if (empty($selectedCompanyId)) { // during login company_id is not yet cached => get Users default company_id from users table
            $userRecord = $this->getUserByEmail($email);
            if(!empty($userRecord)){
                $selectedCompanyId = $userRecord->getCompany()->getId();
            }
        }

        // early exit
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) or empty($selectedCompanyId)) {
            return $res;
        }

        try {
            /*            $sql = 'SELECT
                                    u.id AS user_id,
                                    substr(u.first_name,1,1) AS user_1stletter,
                                    u.first_name,
                                    u.last_name,
                                    u.roles AS user_roles,
                                    u.email,
                                    c.id AS company_id,
                                    c.name AS company_name,
                                    c.account_no AS company_account_no
                                FROM users u
                                JOIN company c ON c.id = u.company_id
                                WHERE u.email= ?';*/
            $sql = 'SELECT 
                    u.id AS user_id,
                    SUBSTR(u.first_name,1,1) AS user_1stletter,
                    u.first_name,
                    u.last_name,
                    u.roles AS user_roles,
                    u.email,
                    u.dpa,
                    u.dpa_date,
                    c.id AS company_id,
                    c.name AS company_name,
                    c.account_no AS company_account_no,
                    c.objectregister_id account_objectregister_id,
                    cp.objectregister_id project_objectregister_id
                FROM users u 
                JOIN company c ON (c.id = :selectedCompanyId AND c.id = u.company_id)
                LEFT OUTER JOIN objectregister o ON o.id = c.objectregister_id
                LEFT OUTER JOIN campaign cp ON c.id = cp.company_id 
                LEFT OUTER JOIN objectregister op ON op.id = cp.objectregister_id
                LEFT OUTER JOIN unique_object_type uot ON uot.id = op.unique_object_type_id AND uot.unique_object_name = :uniqueObjectTypeName
                WHERE u.email= :email
                UNION
                SELECT 
                    u.id AS user_id,
                    SUBSTR(u.first_name,1,1) AS user_1stletter,
                    u.first_name,
                    u.last_name,
                    au.with_roles AS user_roles,
                    u.email,
                    u.dpa,
                    u.dpa_date,
                    au.to_company_id AS company_id,
                    c.name AS company_name,
                    c.account_no AS company_account_no,
                    c.objectregister_id account_objectregister_id,
                    cp.objectregister_id project_objectregister_id
                FROM users u 
                JOIN administrates_users au ON (au.invited_user_id = u.id AND au.to_company_id = :selectedCompanyId)
                JOIN company c ON (c.id = au.to_company_id)
                LEFT OUTER JOIN objectregister o ON o.id = c.objectregister_id
                LEFT OUTER JOIN campaign cp ON c.id = cp.company_id 
                LEFT OUTER JOIN objectregister op ON op.id = cp.objectregister_id
                LEFT OUTER JOIN unique_object_type uot ON uot.id = op.unique_object_type_id AND uot.unique_object_name = :uniqueObjectTypeName  
                WHERE u.email = :email';

            $uniqueObjectTypeName = UniqueObjectTypes::PROJECT;


            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':email', $email, ParameterType::STRING);
            $stmt->bindParam(':selectedCompanyId', $selectedCompanyId, ParameterType::INTEGER);
            $stmt->bindParam(':uniqueObjectTypeName', $uniqueObjectTypeName, ParameterType::INTEGER);

            $stmt->execute();
            $res = $stmt->fetchAll();

            if (!empty($res) && !empty($res[0])) {
                return $res[0];
            } else {
                return $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }
    }


    /**
     * @param string $password
     * @param int    $id
     * @return bool
     */
    public function updateUserPassword(string $password, int $userId): bool
    {
        $status = true; // COMMENT: switch for local version to true, for dev/prod version to false
        if (empty($password) || $userId <= 0) {
            return $status;
        }

        $pw = $this->password_encoder->encodePassword(new Users(), $password);

        $sql = 'UPDATE users
                SET password = :password
                WHERE id=:id';

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':password', $pw);
            $stmt->bindParam(':id', $userId);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $email
     * @return bool
     * @author jsr
     */
    public function isUserActive(string $email): bool
    {
        // early exit
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $sql = "SELECT 1 AS isactive FROM users WHERE email = ? AND status = ?";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $email);
            $active = UserStatusTypes::ACTIVE;
            $stmt->bindParam(2, $active);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0]['isactive']) && 1 == $res[0]['isactive']) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
            return false;
        }
    }


    /**
     * @param string $email
     * @return int 999 in case of error, else number of failed logins {0,1,..3}
     */
    public function getFailedLogins(string $email): int
    {
        // early exit
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return 999;
        }
        $sql = "SELECT failed_logins FROM users WHERE email = ?";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $email);
            $stmt->execute();
            $res = $stmt->fetchAll();

            if (!empty($res) && isset($res[0]['failed_logins'])) {
                return (int)$res[0]['failed_logins'];
            } else {
                return 999;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
            return 999;
        }
    }


    /**
     * @param string $email
     * @return bool
     * @author jsr
     */
    public function incrementFailedLogins(string $email): bool
    {
        // early exit
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        $sql = 'UPDATE users
                SET failed_logins = failed_logins + 1
                WHERE email =?';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $email);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
            return false;
        }
    }


    /**
     * @param string $email
     * @return bool
     * @author jsr
     */
    public function blockUser(string $email)
    {
        // early exit
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        $sql = "UPDATE users
                SET status = 'blocked'
                WHERE email =?";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $email);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
            return false;
        }
    }


    /**
     *
     * used to initalize all user passwords and to setup corresponding roles
     *  its run by BastelController route /test/initpws
     * @author jsr
     */
    public function inializeAllUserPws()
    {
        try {
            // s$this->initSuperUser();
            $this->initUser();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __FUNCTION__], [__LINE__]);
        }
    }


    /**
     * @author jsr
     */
    private function initUser()
    {
        $tempusr = new Users();
        $pw = $this->password_encoder->encodePassword($tempusr, '%3Treaction-6Inital-9Passwort%');

        $this->logger->info('NEW PW = ' . $pw);

        $role_user = json_encode(['ROLE_USER']);
        $sql = "UPDATE users SET roles = ?, password = ? WHERE super_admin = 0 AND id > 0";
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(1, $role_user);
        $stmt->bindParam(2, $pw);

        $stmt->execute();
        $this->conn->commit();
    }


    public function inializeMyPw(string $email = null)
    {
        try {
            $this->initMe($email);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __FUNCTION__], [__LINE__]);
        }
    }


    private function initMe(string $email = null)
    {
        $tempusr = new Users();
        $appUrl = $_ENV['APPLICATION_URL'];
        $pw = $this->password_encoder->encodePassword($tempusr, '%3Treaction-6Inital-9Passwort%');

        $this->logger->info('NEW PW = ' . $pw);

        $role_user = json_encode(["ROLE_SUPER_ADMIN", "ROLE_MASTER_ADMIN", "ROLE_ADMIN", "ROLE_USER"]);
        if ($email === null) {
            $sql = "UPDATE users SET roles = ?, password = ?, failed_logins=0, status='active' WHERE email = 'jochen.schaefer@treaction.net' OR email = 'test20@'+$appUrl AND id > 0";
        } else {
            $sql = "UPDATE users SET roles = ?, password = ?, failed_logins=0, status='active'  WHERE email = ? OR email = 'test20@'+$appUrl AND id > 0";
        }

        $stmt = $this->conn->prepare($sql);

        if ($email !== null) {
            $stmt->bindParam(3, $email);
        }
        $stmt->bindParam(1, $role_user);
        $stmt->bindParam(2, $pw);

        $stmt->execute();
        $this->conn->commit();
    }


    /**
     * @param string $ip
     * @return bool
     * @author Pradeep
     */
    public function ValidateUserGeoLocation(string $ip)
    {
        $status = true;
        if (empty($ip) || empty($this->geoIpClient)) {
            return $status;
        }
        // Todo: remove ip address.
        try {
            // Our CIO accepts clients only from DE.
            //$geo_client = $this->geoIpClient->city('95.223.185.73');//todo:add our IP or find some solution
            $geo_client = $this->geoIpClient->city($ip);
            if ($geo_client->country->isoCode === 'DE') {
                $status = true;
            }
        } catch (AddressNotFoundException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        } catch (AuthenticationException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        } catch (InvalidRequestException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        } catch (HttpException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        } catch (OutOfQueriesException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        } catch (GeoIp2Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }

    /**
     * @param string $ipAddress
     * @return string|null
     * @author Pradeep
     */
    public function getUserLocationByIpAddress(string $ipAddress): ?string
    {
        if (empty($ipAddress) || empty($this->geoIpClient)) {
            return null;
        }
        try {
            $geo_client = $this->geoIpClient->city($ipAddress);
            return $geo_client->country->name;
        } catch (AddressNotFoundException | AuthenticationException | InvalidRequestException | HttpException
        | OutOfQueriesException | GeoIp2Exception $e) {
            $this->logger->warning($e->getMessage(), ['returning null',__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * @author jsr
     */
    private function initSuperUser()
    {
        $tempusr = new Users();
        $pw = $this->password_encoder->encodePassword($tempusr, '%3Treaction-6Inital-9Passwort%');

        $this->logger->info('NEW PW = ' . $pw);

        $role_super_admin = json_encode(['ROLE_USER', 'ROLE_SUPER_ADMIN']);

        $sql = 'UPDATE users SET roles = ?, password = ? WHERE super_admin = 1 AND id > 0';
        $stmt = $this->conn->prepare($sql);

        $stmt->bindParam(1, $role_super_admin);
        $stmt->bindParam(2, $pw);

        $stmt->execute();
        $this->conn->commit();
    }


    /**
     * @param string $userEmail
     * @return bool
     * @author Pradeep
     */
    public function verifyDPAStatusForUser(string $userEmail): bool
    {
        $status = false;
        if (empty($userEmail) || $this->conn === null) {
            return $status;
        }

        try {
            $sql = "SELECT dpa, dpa_date FROM users WHERE email LIKE ?";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $userEmail);
            $stmt->execute();
            $res = $stmt->fetchAll();

            if (!empty($res[0]) && ($res[0]['dpa']) && !empty($res[0]['dpa_date'])) {
                $status = true;
            }

            return $status;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }


    /**
     * @param int    $userId
     * @param bool   $dpa
     * @param string $dpaDate
     * @return bool
     * @author Pradeep
     */
    public function updateDPAAndDPADateForUser(int $userId, bool $dpa, string $dpaDate): bool
    {

    	$this->logger->info('updateDPAAndDPADateForUser', [$userId, $dpa, $dpaDate, __FUNCTION__, __LINE__]);
        $user_dpa_status = false;
        if ($userId <= 0 || empty($dpaDate) || empty($dpa)) {
            return $user_dpa_status;
        }

        try {
            $sql = 'UPDATE users d
                    SET 
                        dpa = ?, 
                        dpa_date= ?, 
                        status = ?
                        where id= ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $dpa);
            $stmt->bindParam(2, $dpaDate);
            $status = UserStatusTypes::ACTIVE;
            $stmt->bindParam(3, $status);
            $stmt->bindParam(4, $userId);
            return $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $user_dpa_status;
        }
    }

    /**
     * @param int $userId
     * @return string|null
     * @author Pradeep
     * @internal Fetches the user registration details from UserMeta whether user is registered from MIO or AIO
     */
    public function getUserRegistrationSource(int $userId): ?string
    {
        $source = '';
        try {
            if ($userId <= 0) {
                throw new RuntimeException('Invalid UserId provided');
            }
            $registrationDetails = $this->userMetaRepository->getUserMetaValue($userId,
                $this->userMetaRepository::USER_REG_FIRSTSTEP);
            if (empty($registrationDetails) || !isset($registrationDetails[ 'source' ]) || empty($registrationDetails[ 'source' ])) {
                return $source;
            }
            return $registrationDetails[ 'source' ];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @return int|null
     * @author Aki
     * @internal Get id of first super admin - used for inserts from cronjobs
     */
    public function getSuperAdminUserId():?int
    {
        $superAdminUserId = null;
        $userRole = '[\"ROLE_SUPER_ADMIN\", \"ROLE_MASTER_ADMIN\",\"ROLE_ADMIN\",\"ROLE_USER\"]';
        try {
            $sql = "SELECT id FROM users WHERE roles LIKE :userRole";
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':userRole', $userRole);
            $stmt->execute();
            $res = $stmt->fetchAll();

            if (!empty($res[0])) {
                $superAdminUserId = $res[0]['id'];
            }

            return $superAdminUserId;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $superAdminUserId;
        }
    }
}

