<?php

namespace App\Repository;


use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\FetchMode;
use Psr\Log\LoggerInterface;
use Exception;
use App\Types\UniqueObjectTypes;
use App\Services\CryptoService;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * class implements Repository methods for <b>tables</b>
 * <ul  style="color:blue; font-weight:bold">
 * <li>server_configuration</b>
 * <li>server_hosting</b>
 * <li>server_sftp_configuration</b>
 * </ul>
 *
 */
class ServerConfigurationRepository
{
    const DEFAULT_USER_ID = 99999;

    private int $currentUserId;
    private CryptoService $cryptoService;

    public function __construct(Connection $connection, CryptoService $cryptoService, LoggerInterface $logger)
    {
        $this->conn = $connection;
        $this->logger =$logger;
        $this->currentUserId = self::DEFAULT_USER_ID;
        $this->cryptoService = $cryptoService;
    }


    /**
     * @param int $user_id
     * @return void
     */
    public function setCurrentUserId(int $user_id)
    {
        $this->currentUserId = $user_id;
    }


    /**
     * set class <b>global current user id</b>, if before you run <b style="color:blue"><u>inserts</u> or <u>updates</u></b>, otherwise a default user id (99999) is used
     * @param int $user_id users.id
     */
    public function setRepostitoriesCurrentUserId(int $user_id)
    {
        $this->currentUserId = $user_id;
    }


    /**
     * returns null or <code>[][id, objectregister_id, company_id, seed_pool_group_id, ip_address, internal_name, description, hoster, default_domain, segment, plesk_url, plesk_user, plesk_pw, created, updated, created_by, updated_by]</code>
     * @param int $company_id
     * @return array|null
     */
    public function getServerConfiguration(int $company_id, int $seedPoolGroupId=-999, string $statusdef_value='all'):?array
    {
        if($seedPoolGroupId === -999) {
            $and_seed_pool_group_id='';
        } else {
            $and_seed_pool_group_id="AND spg.id = $seedPoolGroupId";
        }

        if($statusdef_value === 'all' ) {
            $and_statusdef_value = '';
        } else {
            $and_statusdef_value = "AND sd.value = '$statusdef_value'";
        }


        $records = null;
        $select =
            "SELECT 
                sc.*, 
                sh.host_name as hoster,
                oreg.id as objectregister_id,
                oreg.object_unique_id, 
                oreg.statusdef_id,
                sd.id as status_id, 
                sd.value as status_text,
                spg.name as seedpoolgroup_name,
                c.name as company_name,
                u.first_name
            FROM server_configuration sc
            JOIN objectregister oreg ON oreg.id = sc.objectregister_id
            JOIN statusdef sd ON oreg.statusdef_id = sd.id $and_statusdef_value 
            JOIN seed_pool_group spg ON spg.id = sc.seed_pool_group_id $and_seed_pool_group_id
            JOIN company c ON c.id = sc.company_id
            JOIN server_hosting sh ON sh.id = sc.server_hosting_id 
            LEFT OUTER JOIN users u ON u.id = sc.updated_by
            WHERE sc.company_id = :company_id
            ORDER BY sc.internal_name";
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);
/*          TOO SLOW FOR OVERVIEW
            foreach ($records as &$r) {
                $r['plesk_pw'] = $this->cryptoService->deCryptB64($r['plesk_pw']);
            }
*/
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $records;
    }


    /**
     * returns [] or <code>id, objectregister_id, company_id, seed_pool_group_id, internal_name, description, hoster, default_domain, segment, plesk_url, plesk_user, plesk_pw, created, updated, created_by, updated_by]</code>
     * @param int $company_id
     * @return array|null
     */
    public function getServerConfigurationByServerId(int $server_id):array
    {
        $records = [];
        $select =
            'SELECT 
                sc.*, 
                sh.host_name as hoster,
                oreg.id as objectregister_id,
                oreg.object_unique_id, 
                oreg.statusdef_id,
                sd.id as status_id, 
                sd.value as status_text,
                spg.name as seedpoolgroup_name,
                c.name as company_name,
                u.first_name 
            FROM server_configuration sc
            JOIN objectregister oreg ON oreg.id = sc.objectregister_id
            JOIN statusdef sd ON oreg.statusdef_id = sd.id
            JOIN seed_pool_group spg ON spg.id = sc.seed_pool_group_id
            JOIN company c ON c.id = sc.company_id
            JOIN server_hosting sh ON sh.id = sc.server_hosting_id
            LEFT OUTER JOIN users u ON u.id = sc.updated_by
            WHERE sc.id = :server_id
            ORDER BY sc.internal_name';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':server_id', $server_id, \PDO::PARAM_INT);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($records as &$r) {
                $r['plesk_pw'] = $this->cryptoService->deCryptB64($r['plesk_pw']);
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $records;
    }


    /**
     * returns null or <code>[][id, server_configuration_id, sftp_user, sftp_pw, comment, created, updated, created_by, updated_by]</code>
     * @param int $server_configuration_id
     * @return array|null
     */
    public function getServerSftpConfiguration(int $server_configuration_id):?array
    {
        $return = null;
        $select = 'SELECT * FROM server_sftp_configuration WHERE server_configuration_id = :server_configuration_id ORDER BY id';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':server_configuration_id', $server_configuration_id, \PDO::PARAM_INT);
            $stmt->execute();
            $return = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($return as &$r) {
                $r['sftp_pw'] = $this->cryptoService->deCryptB64($r['sftp_pw']);
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * returns null or <code>[][id, server_configuration_id, sender_mailbox, sender_mailbox_pw, forwarding_mailbox, created, updated, created_by, updated_by]</code>
     * @param int $server_configuration_id
     * @return array|null
     * @deprecated since PO killed feature
     */
    public function getServerEmailConfiguration(int $server_configuration_id):?array
    {
        $return = null;
        $select = 'SELECT * FROM server_email_configuration WHERE server_configuration_id = :server_configuration_id';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':server_configuration_id', $server_configuration_id, \PDO::PARAM_INT);
            $stmt->execute();
            $return = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * returns <code>[server_configuration_id][] [id, server_configuration_id, sftp_user, sftp_pw, comment, created, updated, created_by, updated_by]</code>
     * @param int $company_id
     * @param int $seedPoolGroupId optional
     * @return array
     */
    public function getServerSftpConfigurationByCompanyId(int $company_id, int $seedPoolGroupId=-999, string $statusdef_value='all'):array
    {
        $records = [];
        if($seedPoolGroupId === -999) {
            $and_seed_pool_group_id = '';
        } else {
            $and_seed_pool_group_id = "AND sc.seed_pool_group_id = $seedPoolGroupId";
        }

        if($statusdef_value === 'all' ) {
            $and_statusdef_value = '';
        } else {
            $and_statusdef_value = "AND sd.value = '$statusdef_value'";
        }

        $select =
            "SELECT ssc.* 
                FROM server_configuration sc 
                JOIN  server_sftp_configuration ssc ON ssc.server_configuration_id = sc.id
                JOIN objectregister oreg ON oreg.id = sc.objectregister_id
                JOIN statusdef sd ON oreg.statusdef_id = sd.id $and_statusdef_value 
                WHERE sc.company_id = :company_id
                $and_seed_pool_group_id
                ORDER BY ssc.id";
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            /* TOO SLOW FOR OVERVIEW
             foreach ($records as &$r) {
                $r['sftp_pw'] = $this->cryptoService->deCryptB64($r['sftp_pw']);
            }
            */
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $records;
    }


    /**
     * returns <code>[server_configuration_id][] [id, sender_mailbox, sender_mailbox_pw, forwarding_mailbox, created, updated, created_by, updated_by]</code>
     * @param int $company_id
     * @return array|null
     * @deprecated since not used
     */
    public function getServerEmailConfigurationByCompanyId(int $company_id):?array
    {
        $return = null;
        $select =
            'SELECT sec.* 
                FROM server_configuration sc 
                JOIN  server_email_configuration sec ON sec.server_configuration_id = sc.id
                WHERE sc.company_id = :company_id
                ORDER BY sec.server_configuration_id, sec.sender_mailbox';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':company_id', $company_id, \PDO::PARAM_INT);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            $temp = [];
            foreach ($records as $r) {
                $temp[ $r['server_configuration_id'] ][] = [
                    'id' => $r['id'],
                    'sender_mailbox' => $r['sender_mailbox'],
                    'sender_mailbox_pw' => $r['sender_mailbox_pw'],
                    'forwarding_mailbox' => $r['forwarding_mailbox'],
                    'created' => $r['created'],
                    'updated' => $r['updated'],
                    'created_by' => $r['created_by'],
                    'updated_by' => $r['updated_by'],
                ];
            }
            $return = $temp;

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * @param int         $objectregister_id
     * @param int         $company_id
     * @param int         $seed_pool_group_id
     * @param int         $server_hosting_id
     * @param string|null $ip_address
     * @param string      $internal_name
     * @param string|null $description
     * @param string|null $default_domain
     * @param string|null $segment
     * @param int         $cloudflare
     * @param string|null $blacklist
     * @param string|null $plesk_url
     * @param string|null $plesk_user
     * @param string|null $plesk_pw
     * @return int 0 in case of error, int > 0 as last inserted id in server_configuration
     */
    public function insertIntoServerConfiguration(
        int $objectregister_id,
        int $company_id,
        int $seed_pool_group_id,
        int $server_hosting_id,
        ?string $ip_address,
        string $internal_name,
        ?string $description,
        ?string $default_domain,
        ?string $segment,
        int $cloudflare,
        ?string $blacklist,
        ?string $plesk_url,
        ?string $plesk_user,
        ?string $plesk_pw
    ):int {
        $return = 0;
        $insert=
            'INSERT INTO server_configuration 
            (objectregister_id, company_id, seed_pool_group_id, ip_address, internal_name, `description`, server_hosting_id, default_domain, segment, cloudflare, blacklist, plesk_url, plesk_user, plesk_pw, created_by, updated_by) 
            VALUES 
            (:objectregister_id, :company_id, :seed_pool_group_id, :ip_address, :internal_name, :description, :server_hosting_id, :default_domain, :segment, :cloudflare, :blacklist, :plesk_url, :plesk_user, :plesk_pw, :created_by, :updated_by)';
        try {
            $plesk_pw = $this->cryptoService->enCryptB64($plesk_pw);
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':objectregister_id', $objectregister_id, \PDO::PARAM_INT);
            $stmt->bindParam(':company_id', $company_id, \PDO::PARAM_INT);
            $stmt->bindParam(':seed_pool_group_id', $seed_pool_group_id, \PDO::PARAM_INT);
            $stmt->bindParam(':ip_address', $ip_address, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':internal_name', $internal_name, \PDO::PARAM_STR);
            $stmt->bindParam(':description', $description, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':server_hosting_id', $server_hosting_id, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':default_domain', $default_domain, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':segment', $segment, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':cloudflare', $cloudflare, \PDO::PARAM_INT);
            $stmt->bindParam(':blacklist', $blacklist, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':plesk_url', $plesk_url, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':plesk_user', $plesk_user, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':plesk_pw', $plesk_pw, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':created_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->execute();
            $return = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * @param int         $id
     * @param int         $seed_pool_group_id
     * @param int         $server_hosting_id
     * @param string|null $ip_address
     * @param string      $internal_name
     * @param string|null $description
     * @param string|null $default_domain
     * @param string|null $segment
     * @param int         $cloudflare
     * @param string|null $blacklist
     * @param string|null $plesk_url
     * @param string|null $plesk_user
     * @param string|null $plesk_pw
     * @return bool
     */
    public function updateServerConfiguration(
        int $id,
        int $seed_pool_group_id,
        int $server_hosting_id,
        ?string $ip_address,
        string $internal_name,
        ?string $description,
        ?string $default_domain,
        ?string $segment,
        int $cloudflare,
        ?string $blacklist,
        ?string $plesk_url,
        ?string $plesk_user,
        ?string $plesk_pw
    ):bool {
        $return = false;

        $update =
            "UPDATE server_configuration SET  
                seed_pool_group_id = :seed_pool_group_id,
                ip_address = :ip_address,
                internal_name = :internal_name,
                `description` = :description,
                server_hosting_id = :server_hosting_id,
                default_domain = :default_domain,
                segment = :segment,
                cloudflare = :cloudflare,
                blacklist = :blacklist,
                plesk_url = :plesk_url,
                plesk_user = :plesk_user,
                plesk_pw = :plesk_pw,
                created_by = :created_by,
                updated_by = :updated_by
            WHERE id = :id";
        try {
            $plesk_pw = $this->cryptoService->enCryptB64($plesk_pw);
            $stmt = $this->conn->prepare($update);
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $stmt->bindParam(':seed_pool_group_id', $seed_pool_group_id, \PDO::PARAM_INT);
            $stmt->bindParam(':ip_address', $ip_address, \PDO::PARAM_STR);
            $stmt->bindParam(':internal_name', $internal_name, \PDO::PARAM_STR);
            $stmt->bindParam(':description', $description, \PDO::PARAM_STR);
            $stmt->bindParam(':server_hosting_id', $server_hosting_id, \PDO::PARAM_INT);
            $stmt->bindParam(':default_domain', $default_domain, \PDO::PARAM_STR);
            $stmt->bindParam(':segment', $segment, \PDO::PARAM_STR);
            $stmt->bindParam(':cloudflare', $cloudflare, \PDO::PARAM_INT);
            $stmt->bindParam(':blacklist', $blacklist, \PDO::PARAM_STR);
            $stmt->bindParam(':plesk_url', $plesk_url, \PDO::PARAM_STR);
            $stmt->bindParam(':plesk_user', $plesk_user, \PDO::PARAM_STR);
            $stmt->bindParam(':plesk_pw', $plesk_pw, \PDO::PARAM_STR);
            $stmt->bindParam(':created_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_INT);
            $return = $stmt->execute();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * @param int    $server_configuration_id
     * @param string $sftp_user
     * @param string $sftp_pw
     * @param string $comment
     * @return int 0 in case of error, int > 0 as last inserted id in server_configuration
     */
    private function insertIntoServerSftpConfiguration(
        int    $server_configuration_id,
        string $sftp_user,
        string $sftp_pw,
        ?string $comment
    ):int {
        $return = 0;
        $insert=
            'INSERT INTO server_sftp_configuration
             (server_configuration_id, sftp_user, sftp_pw, `comment`, created_by, updated_by)   
             VALUES
             (:server_configuration_id, :sftp_user, :sftp_pw, :comment, :created_by, :updated_by)';
        try {
            $sftp_pw = $this->cryptoService->enCryptB64($sftp_pw);
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':server_configuration_id', $server_configuration_id, \PDO::PARAM_INT);
            $stmt->bindParam(':sftp_user', $sftp_user, \PDO::PARAM_STR);
            $stmt->bindParam(':sftp_pw', $sftp_pw, \PDO::PARAM_STR);
            $stmt->bindParam(':comment', $comment, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':created_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->execute();
            $return = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }

    /**
     * @param int    $server_configuration_id
     * @param string $sftp_user
     * @param string $sftp_pw
     * @param string $comment
     * @return bool
     */
    private function updateServerSftpConfiguration(
        int $ftpid,
        int $server_configuration_id,
        string $sftp_user,
        string $sftp_pw,
        ?string $comment
    ):bool {
        $return = false;
        $insert=
            'UPDATE server_sftp_configuration SET
                 server_configuration_id = :server_configuration_id, 
                 `comment` = :comment, 
                 sftp_user = :sftp_user,  
                 sftp_pw = :sftp_pw, 
                 created_by = :created_by, 
                 updated_by = :updated_by   
             WHERE id = :ftpid';
        try {
            $sftp_pw = $this->cryptoService->enCryptB64($sftp_pw);
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':ftpid', $ftpid, \PDO::PARAM_INT);
            $stmt->bindParam(':server_configuration_id', $server_configuration_id, \PDO::PARAM_INT);
            $stmt->bindParam(':comment', $comment, \PDO::PARAM_STR|\PDO::PARAM_NULL);
            $stmt->bindParam(':sftp_user', $sftp_user, \PDO::PARAM_STR);
            $stmt->bindParam(':sftp_pw', $sftp_pw, \PDO::PARAM_STR);
            $stmt->bindParam(':created_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_INT);
            $return = $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * @param int   $serverConfigurationId
     * @param array $ftpRecords
     * @return bool if insert and / or update was done
     * @uses ServerConfigurationRepository::insertIntoServerSftpConfiguration()
     * @uses ServerConfigurationRepository::updateServerSftpConfiguration()
     */
    public function insertOrUpdateSftpRecords(int $serverConfigurationId, array $ftpRecords):bool
    {
        // TODO: encode ftp_pw with s-i-o
        $inserted = 999;
        $updateOkay = true;

        foreach ($ftpRecords as $f) {
            if($f['action'] === 'insert') {
                $inserted = $this->insertIntoServerSftpConfiguration($serverConfigurationId, $f['ftp_user'], $f['ftp_pw'], $f['comment']);
            } else { // update
                $updateOkay = $this->updateServerSftpConfiguration($f['ftpid'], $serverConfigurationId, $f['ftp_user'], $f['ftp_pw'], $f['comment']);
            }
        }
        return ($inserted > 0 && $updateOkay);
    }


    /**
     * @param int    $server_configuration_id
     * @param string $sender_mailbox
     * @param string $sender_mailbox_pw
     * @param string $forwarding_mailbox
     * @return int
     * @deprecated since not used
     */
    public function insertIntoServerEmailConfiguration(
        int $server_configuration_id,
        string $sender_mailbox,
        string $sender_mailbox_pw,
        string $forwarding_mailbox
    ):int {
        $return = 0;
        $insert=
            'INSERT INTO server_email_configuration
             (server_configuration_id, sender_mailbox, sender_mailbox_pw, forwarding_mailbox, created_by, updated_by)   
             VALUES
             (:server_configuration_id, :sender_mailbox, :sender_mailbox_pw, :forwarding_mailbox, :created_by, :updated_by)';
        try {
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':server_configuration_id', $server_configuration_id, \PDO::PARAM_INT);
            $stmt->bindParam(':sender_mailbox', $sender_mailbox, \PDO::PARAM_STR);
            $stmt->bindParam(':sender_mailbox_pw', $sender_mailbox_pw, \PDO::PARAM_STR);
            $stmt->bindParam(':forwarding_mailbox', $forwarding_mailbox, \PDO::PARAM_STR);
            $stmt->bindParam(':sft_user', $sft_user, \PDO::PARAM_STR);
            $stmt->bindParam(':sftp_pw', $sftp_pw, \PDO::PARAM_STR);
            $stmt->bindParam(':created_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->execute();
            $return = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * deletes record in <b>server_configuration</b> as well as <i><b>cascading</b></i> on both tables <b>server_email_configuration</b> and <b>server_sftp_configuration</b>
     * @param int $server_id
     * @return int number of deleted records
     */
    public function deleteServerConfigurationCascading(int $server_id):int
    {
        $deletedRecords = 0;
        $delete = "DELETE FROM server_configuration WHERE id=:server_id";
        try {
            $stmt = $this->conn->prepare($delete);
            $stmt->bindParam(':server_id', $server_id, \PDO::PARAM_INT);
            $stmt->execute();
            $deletedRecords = $stmt->rowCount();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $deletedRecords;
    }


    /**
     *
     * @return array
     */
    public function getStatusListForServerConfiguration():array
    {
        $records = [];
        $uniqueObjectTypeName = UniqueObjectTypes::SERVER;
        $select=
            'SELECT 
                sd.id as status_id, 
                sd.value as status_text
            FROM status_model sm
            JOIN unique_object_type ut ON ut.id = sm.unique_object_type_id AND ut.unique_object_name =:uniqueObjectTypeName 
            JOIN statusdef sd ON sd.id = sm.statusdef_id';

        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':uniqueObjectTypeName', $uniqueObjectTypeName, \PDO::PARAM_STR);
            $stmt->execute();
            $records = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $records;
    }


    /**
     * get all hosting providers
     * @return array [][id, host_name]
     */
    public function getServerHosting():array
    {
        $return = [];
        $select = 'SELECT id, host_name FROM server_hosting ORDER BY host_name';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->execute();
            $return = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * add a new hosting provider
     * @param string $host_name
     * @return bool
     */
    public function createServerHosting(string $host_name):?int
    {
        $ret = null;
        $insert = 'INSERT INTO server_hosting (host_name,created_by, updated_by) VALUES (:host_name, :created_by, :updated_by)';
        try {
            $stmt = $this->conn->prepare($insert);
            $stmt->bindParam(':host_name', $host_name, \PDO::PARAM_STR);
            $stmt->bindParam(':created_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->bindParam(':updated_by', $this->currentUserId, \PDO::PARAM_INT);
            $stmt->execute();
            $ret = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $ret;
    }


    /**
     * @param string $host_name
     * @return int 0, if name dose not exists, else id of host_name
     */
    public function getServerHostingId(string $host_name):int
    {
        $id = 0;
        $select = 'SELECT id FROM server_hosting WHERE host_name = :host_name';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':host_name', $host_name, \PDO::PARAM_STR);
            $stmt->execute();
            $ret = $stmt->fetchColumn();
            $id = $this->conn->lastInsertId();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $id;
    }


    public function tempEncodePWs():float
    {
        $t1 = microtime(true);
/*        $serverConfig = $this->getServerConfiguration(161);
        foreach($serverConfig as $s) {
            $newPW = $this->cryptoService->enCryptB64($s['plesk_pw']);
            $this->updateServerConfiguration($s['id'], $s['seed_pool_group_id'], $s['server_hosting_id'], $s['ip_address'], $s['internal_name'], $s['description'],
                $s['default_domain'], $s['segment'], $s['cloudflare'], $s['blacklist'], $s['plesk_url'], $s['plesk_user'], $newPW);
        }*/

        $ftpConfig = $this->getServerSftpConfigurationByCompanyId(161);
        foreach($ftpConfig as $f) {
            $newPW = $this->cryptoService->enCryptB64($f['sftp_pw']);
            $this->updateServerSftpConfiguration($f['id'], $f['server_configuration_id'], $f['sftp_user'], $newPW, $f['comment']);
        }
        $t2 = microtime(true);

        return ($t2 - $t1);
    }


    /**
     * @param int $server_id
     * @return string e.g. <i style="color:blue">"blueleadsVS9@DomainFactory" => server_configuration.internal_name@server_hosting.host_name</i>
     */
    public function getServerNameById(int $server_id):string
    {
        $return = 'Server not defined';
        $select =
            'SELECT  
                CONCAT(sc.internal_name, "@" ,sh.host_name) as servername 
            FROM server_configuration sc 
            JOIN server_hosting sh ON sh.id = sc.server_hosting_id
            WHERE sc.id = :server_id';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':server_id', $server_id, \PDO::PARAM_INT);
            $stmt->execute();
            $servername = $stmt->fetchColumn();
            if($stmt->rowCount() > 0) {
                $return = $servername;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }


    /**
     * @param int $server_id
     * @return string defaultDomain of server
     */
    public function getServerDefaultDomainById(int $server_id):string
    {
        $return = '';
        $select =
            'SELECT  
                sc.default_domain
            FROM server_configuration sc 
            WHERE sc.id = :server_id';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':server_id', $server_id, \PDO::PARAM_INT);
            $stmt->execute();
            $defaultDomain = $stmt->fetchColumn();
            if($stmt->rowCount() > 0) {
                $defaultDomainParts = explode('/', $defaultDomain);
                $return = $defaultDomainParts[2];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        $this->logger->info('defaultDomain', [$return, __METHOD__,__LINE__]);
        return $return;

    }

    /**
     * @param int $server_sftp_configuration_id
     * @return string
     */
    public function getServerIpFtpUserById(int $server_sftp_configuration_id):string
    {
        $return = 'sftp connection not defined';
        $select =
            'SELECT  
                CONCAT(sf.sftp_user, "@" ,sc.ip_address) as ftpname 
            FROM server_sftp_configuration sf  
            JOIN server_configuration sc ON sc.id = sf.server_configuration_id 
            WHERE sf.id = :server_sftp_configuration_id';
        try {
            $stmt = $this->conn->prepare($select);
            $stmt->bindParam(':server_sftp_configuration_id', $server_sftp_configuration_id, \PDO::PARAM_INT);
            $stmt->execute();
            $ftpname = $stmt->fetchColumn();
            if($stmt->rowCount() > 0) {
                $return = $ftpname;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }

}
