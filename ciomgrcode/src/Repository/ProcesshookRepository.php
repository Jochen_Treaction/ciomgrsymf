<?php


namespace App\Repository;

use App\Services\AuthenticationService;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Psr\Log\LoggerInterface;
use App\Services\UtilsService;
use Doctrine\DBAL\ParameterType;
use Exception;
use RuntimeException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


/**
 * @property LoggerInterface logger
 * @property Connection conn
 * @property UtilsService utilsService
 * @property int|mixed userId
 */
class ProcesshookRepository
{

    public function __construct(
        UtilsService $utilsService,
        Connection $conn,
        LoggerInterface $logger,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService
    ) {
        $this->conn = $conn;
        $this->logger = $logger;
        $this->utilsService = $utilsService;
        //set user
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);
        $this->userId = (!empty($user) && !empty($user[ 'user_id' ])) ? $user[ 'user_id' ] : 1;

    }


    /**
     * @param string $processHookName
     * @param int $projectCampaignId
     * @return array|null
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     */
    public function getUserProcessHookByUniqueIndex(string $processHookName, int $projectCampaignId): ?array
    {
        $userProcessHooks = [];

        try {
            if (empty($processHookName) || $projectCampaignId <= 0) {
                throw new RuntimeException("Invalid ProcesshookName or ProjectCampaignId");
            }
            $sql = 'SELECT * FROM user_processhook WHERE processhook_name = ? AND project_campaign_id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $processHookName);
            $stmt->bindParam(2, $projectCampaignId, ParameterType::INTEGER);
            $status = $stmt->execute();
            $result = $stmt->fetchAll();
            if ($status && isset($result[ 0 ]) && !empty($result[ 0 ][ 'id' ])) {
                $userProcessHooks = $result[ 0 ];
            }
        } catch (\Doctrine\DBAL\Driver\Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $userProcessHooks;
        }
        return $userProcessHooks;
    }

    public function get(int $id): ?array
    {
        if ($id <= 0) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $sqlSelect = "SELECT * FROM user_processhook WHERE id = :id";
        try {
            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $result = $stmt->fetch();
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $result;

    }

    /**
     * @return array|null
     * @throws \Doctrine\DBAL\Exception
     * @auther aki
     */
    public function getAvailableProcesshooks(): ?array
    {
        $processhook = "processhook";
        $availableProcessHooks = [];
        // Process Hooks which are not supported for initial release of MIO.
        $unSupportedHooks = $this->unSupportedProcesshooks();
        $sqlSelect = "select * from unique_object_type where assigned_table = :processhook";
        try {
            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindParam(':processhook', $processhook);
            $stmt->execute();
            $result = $stmt->fetchAll();

            foreach ($result as $row) {
                if (empty($row) || !isset($row[ 'id' ]) || in_array($row[ 'unique_object_name' ], $unSupportedHooks,
                        true)) {
                    continue;
                }
                $availableProcessHooks[] = $row;
            }
            return $availableProcessHooks;
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @return string[]
     * @author Pradeep
     * @internal ProcessHooks which are not supported for initial release
     */
    private function unSupportedProcesshooks(): array
    {
        return ['show_popup', 'hide_or_unhide_section'];
    }

    /**
     * @return array `[][id => processhook.id, name => processhook.name] OR [] in case of error
     * @author jsr
     */
    public function getMarketingAutomationProcessHooks(): array
    {
        $return = [];
        // TODO: add "start_marketing_automation" to a ProcesshooksTypes class
        $ma = 'start_marketing_automation';
        $selectPh = "SELECT p.id, p.name, p.objectregister_id, a.name action_name, IFNULL(0, w.objectregister_id) workflow_objectregister_id 
					FROM processhook p
					JOIN action a ON p.action_id = a.id AND a.name = :ma
					LEFT OUTER JOIN workflow_processhooks wp on p.id = wp.processhook_id
					LEFT OUTER JOIN workflow w on wp.workflow_id = w.id";
        try {
            $stmt = $this->conn->prepare($selectPh);
            $stmt->bindParam(':ma', $ma);
            $stmt->execute();
            $ret = $stmt->fetchAll(FetchMode::ASSOCIATIVE);
            if ($stmt->rowCount() > 0) {
                foreach ($ret as $key => $r) {
                    $ret[ $key ] = [
                        'id' => $r[ 'id' ],
                        'name' => $r[ 'name' ],
                        'action_name' => $r[ 'action_name' ],
                        'objectregister_id' => $r[ 'objectregister_id' ],
                        'workflow_objectregister_id' => $r[ 'workflow_objectregister_id' ],
                        'uiname' => $this->utilsService->stringSnakeCaseToUiLabel($r[ 'name' ]),
                    ];
                }
                return $ret;
            }
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error('EXCEPTION', [$e->getMessage(), __METHOD__, __LINE__]);
        } catch (\Doctrine\DBAL\Exception $e) {
            $this->logger->error('EXCEPTION', [$e->getMessage(), __METHOD__, __LINE__]);
        }

        return $return;
    }

    /**
     * @param int $objectRegisterId
     * @param int $companyId
     * @param int $actionId
     * @param string $acts
     * @param string $name
     * @param string $description
     * @return int|null
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function insertProcesshook(
        int $objectRegisterId,
        int $companyId,
        int $actionId,
        string $acts,
        string $name,
        string $description
    ): ?int {
        $id = null;
        if ($actionId <= 0 || $objectRegisterId <= 0 || $companyId <= 0
            || empty($acts) || empty($name) || empty($description)) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $sql = 'INSERT INTO processhook(objectregister_id, action_id,acts,name,description)
                    VALUE(:objectregister_id,:company_id, :action_id,:acts,:name,:description,:created_by,:updated_by)';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':objectregister_id', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->bindParam(':action_id', $actionId, ParameterType::INTEGER);
            $stmt->bindParam(':acts', $acts);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':created_by', $this->userId);
            $stmt->bindParam(':updated_by', $this->userId);
            $result = $stmt->execute();
            if ($result === true) {
                $id = $this->conn->lastInsertId();
            }
            return $id;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $processhookType
     * @return int|null
     * @throws \Doctrine\DBAL\Exception
     * @author aki
     */
    public function getProcesshookIdByType(string $processhookType): ?int
    {
        if (empty($processhookType)) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $sqlSelect = "select id from processhook where name = :processhookType";
        try {
            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindParam(':processhookType', $processhookType);
            $stmt->execute();
            $result = $stmt->fetch();
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return (int)$result[ 'id' ];
    }

    /**
     * @param string $processhookType
     * @return string|null
     * @throws \Doctrine\DBAL\Exception
     * @author aki
     */
    public function getDescriptionFromType(string $processhookType): ?string
    {
        if (empty($processhookType)) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $processhook = "processhook";
        $sqlSelect = "select description from unique_object_type where assigned_table = :processhook and unique_object_name = :processhookType";
        try {
            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindParam(':processhook', $processhook);
            $stmt->bindParam(':processhookType', $processhookType);
            $stmt->execute();
            $result = $stmt->fetch();
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $result[ 'description' ];

    }

    /**
     * @param string $status
     * @param int $project_campaign_id
     * @return array|null
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author aki
     */
    public function getProcesshooksByStatus(string $status, int $project_campaign_id): ?array
    {
        $processhooks = null;
        if (empty($status)) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $sql = "select p.*,uot.unique_object_name,uot.description from user_processhook p
                        join objectregister o on p.objectregister_id = o.id
                        join statusdef s on o.statusdef_id = s.id
                        join unique_object_type uot on o.unique_object_type_id = uot.id
                        where s.value = :status and p.project_campaign_id = :project_campaign_id";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':status', $status, ParameterType::STRING);
        $stmt->bindValue(':project_campaign_id', $project_campaign_id, ParameterType::INTEGER);
        try {
            $stmt->execute();
            $res = $stmt->fetchAll();
            $processhooks = !empty($res) ? $res : [];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return $processhooks;
    }

    /**
     * @param int $statusDefId
     * @param int $objectRegisterId
     * @return bool|null
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function changeStatusOfProcesshook(int $statusDefId, int $objectRegisterId): ?bool
    {
        $status = false;
        if ($statusDefId <= 0 || $objectRegisterId <= 0) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $updateSql = "update objectregister  set statusdef_id = :statusdefId , updated_by = :userID where id = :objectregisterId";
        try {
            $stmt = $this->conn->prepare($updateSql);
            $stmt->bindParam(':statusdefId', $statusDefId, ParameterType::INTEGER);
            $stmt->bindParam(':objectregisterId', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindValue(':userID', $this->userId, ParameterType::INTEGER);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }
        return $status;

    }

    /**
     * @return array `[][id => processhook.id, name => processhook.name] OR [] in case of error
     * @auther jsr
     */
    public function getMarketingAutomationProcessHook(): array
    {
        $return = [];
        // TODO: add "start_marketing_automation" to a ProcesshooksTypes class
        $ma = 'start_marketing_automation';
        // TODO: CURRENTLY relation action:processhook is setup in data as 1:1, but relation can be 1:n. In 1:n case a additional selection criteria is needed to restrict the processhook
        $selectPh = "SELECT p.id, p.name, p.objectregister_id, a.name action_name 
					FROM processhook p
					JOIN action a ON p.action_id = a.id AND a.name = :ma";
        try {
            $stmt = $this->conn->prepare($selectPh);
            $stmt->bindParam(':ma', $ma);
            $stmt->execute();
            $ret = $stmt->fetchAssociative(); // TODO: for action:processhook  = 1:n, use fetchAll
            $this->logger->info('$ret', [$ret, __LINE__]);
            if ($stmt->rowCount() > 1) {
                foreach ($ret as $key => $r) {
                    $return[ $key ] = [ // TODO: for action:processhook  = 1:n, use $ret =
                        'id' => $r[ 'id' ],
                        'name' => $r[ 'name' ],
                        'objectregister_id' => $r[ 'objectregister_id' ],
                        'uiname' => $this->utilsService->stringSnakeCaseToUiLabel($r[ 'name' ]),
                        'action_name' => $r[ 'action_name' ],
                    ];
                }
                return $return;
            } elseif ($stmt->rowCount() === 1) {
                return [
                    'id' => $ret[ 'id' ],
                    'name' => $ret[ 'name' ],
                    'objectregister_id' => $ret[ 'objectregister_id' ],
                    'uiname' => $this->utilsService->stringSnakeCaseToUiLabel($ret[ 'name' ]),
                    'action_name' => $ret[ 'action_name' ],
                ];

            } else {
                return [];
            }


        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error('EXCEPTION', [$e->getMessage(), __METHOD__, __LINE__]);
        } catch (\Doctrine\DBAL\Exception $e) {
            $this->logger->error('EXCEPTION', [$e->getMessage(), __METHOD__, __LINE__]);
        }
        return $return;

    }

    /**
     *
     * @author aki
     */
    public function insertUserProcesshook(
        int $objectRegisterId,
        int $processhook_id,
        int $project_campaign_id,
        string $user_processhook_name
    ): ?int {
        $id = null;
        if ($processhook_id <= 0 || $objectRegisterId <= 0 || $project_campaign_id <= 0
            || empty($user_processhook_name)) {
            throw new RuntimeException('Invalid parameters provided');
        }
        $sql = 'INSERT INTO user_processhook(processhook_id,project_campaign_id,objectregister_id,processhook_name,created_by,updated_by)
                    VALUE(:processhook_id,:project_campaign_id, :objectregister_id,:processhook_name,:created_by,:updated_by)';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':processhook_id', $processhook_id, ParameterType::INTEGER);
            $stmt->bindParam(':project_campaign_id', $project_campaign_id, ParameterType::INTEGER);
            $stmt->bindParam(':objectregister_id', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindParam(':processhook_name', $user_processhook_name);
            $stmt->bindParam(':created_by', $this->userId);
            $stmt->bindParam(':updated_by', $this->userId);
            $result = $stmt->execute();
            if ($result === true) {
                $id = $this->conn->lastInsertId();
            }
            return $id;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * @param int $project_campaign_id
     * @return array|null
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function getProcesshookNames(int $project_campaign_id): ?array
    {
        $selectProcesshookName = "SELECT DISTINCT up.processhook_name FROM user_processhook up WHERE project_campaign_id=:project_campaign_id";
        $stmt = $this->conn->prepare($selectProcesshookName);
        $stmt->bindParam(':project_campaign_id', $project_campaign_id, ParameterType::INTEGER);

        try {
            $stmt->execute();
            $processhookNames = $stmt->fetchAll(FetchMode::ASSOCIATIVE); // TODO: if there are more than one projects possible for one company => fetchAll and return array

            if ($stmt->rowCount() === 0) {
                $this->logger->warning('no data found', [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $processhookNames;

    }

    /**
     * @param string $workflow
     * @param int $workflowObjectRegisterMetaId
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function updateWorkflow(string $workflow, int $workflowObjectRegisterMetaId): bool
    {
        if (empty($workflow) || $workflowObjectRegisterMetaId <= 0) {
            return false;
        }
        $status = false;
        try {
            $sql = 'update object_register_meta set meta_value =:meta_value where id = :id;';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':meta_value', $workflow, ParameterType::STRING);
            $stmt->bindParam(':id', $workflowObjectRegisterMetaId, ParameterType::INTEGER);
            $result = $stmt->execute();
            if (is_bool($result) && $result) {
                $status = $result;
            }
            return $status;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }

    }

    /**
     * @param int $workflowId
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function deleteOldRelationsInUserProcesshookHasWorkflow(int $workflowId): bool
    {
        $status = false;
        if ($workflowId <= 0) {
            return $status;
        }
        $sql = "delete from user_processhook_has_workflow where workflow_id = :workflowId";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':workflowId', $workflowId, ParameterType::INTEGER);
            $result = $stmt->execute();
            if (is_bool($result) && $result) {
                $status = $result;
            }
            return $status;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }

    }
}
