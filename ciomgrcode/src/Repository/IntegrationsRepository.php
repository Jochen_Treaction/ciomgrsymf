<?php


namespace App\Repository;


use App\Types\IntegrationTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * @property ObjectTypeMetaRepository ObjectTypeMetaRepository
 * @property ObjectRegisterMetaRepository objectRegisterMetaRepository
 * @property ObjectRegisterRepository objectRegisterRepository
 */
class IntegrationsRepository
{


    private CustomfieldsRepository $customfieldsRepository;

    public function __construct(
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        ObjectTypeMetaRepository $objectTypeMetaRepository,
        IntegrationTypes $integrationTypes,
        CustomfieldsRepository $customfieldsRepository,
        Connection $conn,
        LoggerInterface $logger
    ) {
        $this->ObjectTypeMetaRepository = $objectTypeMetaRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->integrationTypes = $integrationTypes;
        $this->customfieldsRepository = $customfieldsRepository;
        $this->conn = $conn;
        $this->logger = $logger;

    }

    public function update(
        int $id,
        int $objectregisterId,
        int $companyId,
        int $campaignId,
        int $createdBy,
        int $updatedBy
    ): ?bool {

    }

    public function delete(int $id): ?bool
    {

    }

    public function get(int $id): ?array
    {

    }

    public function getIntegrationsForCompany(
        int $companyId,
        string $status = StatusDefRepository::ACTIVE
    ): ?array {
        $integrationsOverview = [];
        if ($companyId <= 0) {
            throw new RuntimeException('Invalid CompanyId provided');
        }
        try {
            $sql = 'SELECT om.meta_key as integration_key, om.meta_value as integration_value
                    FROM integrations i
                        JOIN objectregister o ON i.objectregister_id = o.id
                        JOIN object_register_meta om ON o.id = om.object_register_id
                        JOIN statusdef s ON o.statusdef_id = s.id
                    WHERE i.company_id = :company_id AND s.value = :status';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->bindValue(':status', $status, ParameterType::STRING);
            $stmt->execute();
            $results = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 && !empty($results)) {
                foreach ($results as $result) {
                    if (!isset($result[ 'integration_key' ]) && $result[ 'integration_value' ]) {
                        continue;
                    }
                    $integrationsOverview[ $result[ 'integration_key' ] ] = json_decode($result[ 'integration_value' ],
                        true);
                }
            }
            return $integrationsOverview;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getAllAvailableIntegrationsOverview(): ?array
    {
        $integrationsDefault = $this->ObjectTypeMetaRepository->getObjectTypeMetaDescriptionForAssignedTable('integrations');
        return $integrationsDefault;
    }

    /**
     * @param int $integrationObjectRegisterMetaId
     * @param int $integrationObjectregisterId
     * @param string $uniqueObjectName
     * @param array $integrationConfig
     * @return bool
     * @author Pradeep
     */
    public function updateIntegrationsConfigForCompany(
        int $integrationObjectRegisterMetaId,
        int $integrationObjectregisterId,
        string $uniqueObjectName,
        array $integrationConfig
    ): bool {
        try {
            if ($integrationObjectRegisterMetaId <= 0 || $integrationObjectregisterId <= 0 || empty($uniqueObjectName)) {
                throw new RuntimeException('Invalid configuration provided.');
            }
            $metaValue = json_encode($integrationConfig, JSON_THROW_ON_ERROR);
            if (!$this->objectRegisterMetaRepository->update($integrationObjectRegisterMetaId,
                $integrationObjectregisterId, $uniqueObjectName, $metaValue)) {
                throw new RuntimeException('Failed to update Object Register Meta Value');
            }
            return true;
        } catch (JsonException | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param array $MIOCustomFields
     * @param int $company_id
     * @return bool|null
     * @author Pradeep | Aki
     * @internal Loop through eMIOIntegration mapping data and update(remove/add) the eMIO custom fields according to MIO custom fields from project wizard
     */
    public function updateeMIOCustomFieldsFromProject(array $MIOCustomFields, int $company_id): ?bool
    {
        try {
            $status = false;
            if (empty($MIOCustomFields) || $company_id <= 0) {
                throw new RuntimeException('Invalid CustomFields or Company Id provided');
            }
            // get the Integrations from eMIO.
            $eMIOIntegrationConfig = $this->getIntegrationConfigForCompany($company_id, UniqueObjectTypes::EMAILINONE,
                StatusDefRepository::ACTIVE);
            if (!isset($eMIOIntegrationConfig[ 'settings' ][ 'mapping' ]) || empty($eMIOIntegrationConfig[ 'settings' ][ 'mapping' ])) {
                return $status;
            }
            $eMIOCustomFields = json_decode(base64_decode($eMIOIntegrationConfig[ 'settings' ][ 'mapping' ]), true, 512,
                JSON_THROW_ON_ERROR);
            $MIOCustomFieldList = $MIOCustomFields[ 'customfieldlist' ];
            //todo:Accordingly remove/delete the customfields in eMIO. - As of now customfields which are created automatically in eMIO are not touched.
            //ForLoop to detect the deleted customfield in MIO and which is present in eMIO and 'remove' the custom field from mapping table
            foreach ($eMIOCustomFields as $key => $value) {
                $eMIOFieldIsPresent = false;
                if (!isset($value[ 'marketing-in-one-custom-field' ])) {
                    continue;
                }
                $eMIOField = $value[ 'marketing-in-one-custom-field' ];
                foreach ($MIOCustomFieldList as $customfield) {
                    if ($eMIOField !== $customfield[ 'fieldname' ] || $customfield[ 'instruction' ] !== 'delete') {
                        continue;
                    }
                    $eMIOFieldIsPresent = true;
                }
                // remove the customfield from eMIO which has been deleted in MIO
                if ($eMIOFieldIsPresent) {
                    unset($eMIOCustomFields[ $key ]);
                }
            }
            //Add newly added custom fields to eMIOCustomField mapping
            //Get all available customFields of company from the database
            $customFields = $this->customfieldsRepository->getProjectCustomfields($company_id);
            //check for missing custom fields in eMIO integration mapping and 'add' the custom field
            foreach ($customFields as $key => $value) {
                $eMIOFieldIsNotPresent = true;
                foreach ($eMIOCustomFields as $eMIOCustomField) {
                    if($value['id'] === $eMIOCustomField['marketing-in-one-custom-field-id']){
                        $eMIOFieldIsNotPresent = false;
                    }
                }
                //if not present in the data add the new custom field to eMIO custom fields
                if($eMIOFieldIsNotPresent){
                    $newCustomField = [
                        'marketing-in-one-custom-field' => $value['fieldname'],
                        'html_label' => 'Contact.'.$value['fieldname'],
                        'marketing-in-one-custom-field-id' => $value['id'],
                        'email-in-one-custom-field' => 'MIO_'.$value['fieldname'],
                    ];
                    $eMIOCustomFields[]= $newCustomField;
                }
            }
            $eMIOIntegrationConfig[ 'settings' ][ 'mapping' ] = base64_encode(json_encode($eMIOCustomFields,
                JSON_THROW_ON_ERROR));
            $metaValue = json_encode($eMIOIntegrationConfig, JSON_THROW_ON_ERROR);
            $metaKey = UniqueObjectTypes::EMAILINONE;
            //update new settings to database
            $status = $this->objectRegisterMetaRepository->update((int)$eMIOIntegrationConfig[ 'relation' ][ 'integration_object_register_meta_id' ],
                (int)$eMIOIntegrationConfig[ 'relation' ][ 'integration_objectregister_id' ], $metaKey, $metaValue);
            if (!$status) {
                throw new RuntimeException('Failed to update Object register meta');
            }
            $this->logger->info('status of integration update ' . $status);
        } catch (JsonException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $status;
    }

    /**
     * @param int $companyId
     * @param string $uniqueObjectName
     * @param string $status
     * @return array|null
     * @author Pradeep
     */
    public function getIntegrationConfigForCompany(
        int $companyId,
        string $uniqueObjectName,
        string $status = StatusDefRepository::ACTIVE
    ): ?array {
        
        $this->logger->info('PARAMS', [$companyId, $uniqueObjectName, $status, __METHOD__, __LINE__]);
        
        $emailServerConfig = [];
        if ($companyId <= 0 || empty($uniqueObjectName)) {
            throw new RuntimeException('Invalid companyId or UniqueObjectName provided');
        }
        try {
            $sql = 'SELECT om.*
                    FROM object_register_meta om
                        JOIN objectregister o ON om.object_register_id = o.id
                        JOIN integrations i ON o.id = i.objectregister_id
                        JOIN unique_object_type uot ON o.unique_object_type_id = uot.id
                        JOIN statusdef s ON o.statusdef_id = s.id
                    WHERE i.company_id = :company_id
                      AND uot.unique_object_name LIKE :unique_object_name AND s.value=:status ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->bindValue(':unique_object_name', $uniqueObjectName, ParameterType::STRING);
            $stmt->bindValue(':status', $status, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0 && !empty($result[ 0 ]) && (!empty($result[ 0 ][ 'meta_value' ]))) {
                $emailServerJson = $result[ 0 ][ 'meta_value' ];
                $emailServerConfig = json_decode($emailServerJson, true, 512, JSON_THROW_ON_ERROR);
                $emailServerConfig[ 'relation' ][ 'integration_object_register_meta_id' ] = $result[ 0 ][ 'id' ];
            }
            return $emailServerConfig;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @return bool|null
     * @author Pradeep
     */
    public function checkIfDefaultServerConfigured(): ?bool
    {
        $status = false;
        $spObjRegId = $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        if (is_null($spObjRegId)) {
            return false;
        }
        $settings = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($spObjRegId, UniqueObjectTypes::EMAILSERVER);
        if (!empty($settings)) {
            $status = true;
        }
        return $status;
    }

    /**
     * @return string|null
     * @author Pradeep
     */
    public function getEMailInOneAPIkeySyncUserForNewsletter(): ?string
    {
        $apikey = '';
        $objRegId = $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        if ($objRegId === null) {
            return $apikey;
        }
        $objRegMetaDetails = $this->objectRegisterMetaRepository->getJsonMetaValueAsArray($objRegId,
            UniqueObjectTypes::EMAILINONE);
        if (!empty($objRegMetaDetails) && isset($objRegMetaDetails[ 'settings' ][ 'apikey' ])) {
            $apikey = $objRegMetaDetails[ 'settings' ][ 'apikey' ];
        }
        return $apikey;
    }

    /**
     * @param int $companyId
     * @param int $userId
     * @param int $projectId
     * @return bool|null
     * @author Pradeep
     */
    public function installDefaultEmailServer(int $companyId, int $userId, int $projectId): ?bool
    {
        $status = false;
        try {
            $eMailServerConfig = $this->integrationTypes->getEmailServerConfig();
            $eMailServerConfig[ 'settings' ][ 'server_type' ] = 'default_server';
            $integrationConfig = $this->insertIntegrationsConfigForCompany($userId, $projectId, $companyId,
                UniqueObjectTypes::EMAILSERVER, $eMailServerConfig);
            if (!empty($integrationConfig)) {
                $status = true;
            } else {
                throw new RuntimeException('Failed to install Default EMailServer');
            }
            return $status;
        } catch (Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }

    /**
     * @param int $userId
     * @param int $campaignId
     * @param int $companyId
     * @param string $uniqueObjectName
     * @param array $integrationConfig
     * @return array|null
     * @author Pradeep
     */
    public function insertIntegrationsConfigForCompany(
        int $userId,
        int $campaignId,
        int $companyId,
        string $uniqueObjectName,
        array $integrationConfig
    ): ?array {
        $config = [];
        try {
            if ($companyId <= 0 || empty($uniqueObjectName) || empty($integrationConfig)) {
                throw new RuntimeException('Invalid CompanyId or UniqueObjectName Or IntegrationConfig provided');
            }
            $config = $integrationConfig;
            // create ObjectRegister
            $objectRegisterId = $this->objectRegisterRepository->createInitialObjectRegisterEntry($companyId,
                $uniqueObjectName);
            $this->logger->info("Objectregisterid " . $objectRegisterId);
            if ($objectRegisterId === null) {
                throw new RuntimeException('Failed to create ObjectRegister Id');
            }
            $config[ 'relation' ][ 'integration_objectregister_id' ] = $objectRegisterId;
            $config[ 'relation' ][ 'integration_object_unique_id' ] =
                $this->objectRegisterRepository->getUniqueObjectTypeIdByName($uniqueObjectName);
            // create Integrations for Company.
            if ($this->insert($objectRegisterId, $companyId, $campaignId, $userId, $userId) === null) {
                throw new RuntimeException('Failed to insert ObjectRegisterId to Integrations');
            }
            $metaValue = json_encode($config, JSON_THROW_ON_ERROR);
            // create Object register meta to save integration configuration
            $objectRegisterMetaId = $this->objectRegisterMetaRepository->insert($objectRegisterId,
                $uniqueObjectName, $metaValue);
            if ($objectRegisterMetaId === null) {
                throw new RuntimeException('Failed to insert EmailServer settings to objectRegisterMetaRepository');
            }
            $this->logger->info("config " . json_encode($config, JSON_THROW_ON_ERROR));
            $config[ 'relation' ][ 'integration_object_register_meta_id' ] = $objectRegisterMetaId;
            return $config;
        } catch (JsonException | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $objectregisterId
     * @param int $companyId
     * @param int $campaignId
     * @param int $createdBy
     * @param int $updatedBy
     * @return int|null
     * @author Pradeep
     */
    public function insert(int $objectregisterId, int $companyId, int $campaignId, int $createdBy, int $updatedBy): ?int
    {
        $integrationsId = null;
        try {
            if ($objectregisterId <= 0 || $companyId <= 0 || $campaignId <= 0) {
                throw new RuntimeException('Invalid objectregisterId or CompanyId or CampaignId provided');
            }
            $sql = 'INSERT INTO integrations(objectregister_id, company_id, campaign_id, created_by, updated_by) 
                    VALUE(?,?,?,?,?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(1, $objectregisterId, ParameterType::INTEGER);
            $stmt->bindParam(2, $companyId, ParameterType::INTEGER);
            $stmt->bindParam(3, $campaignId, ParameterType::INTEGER);
            $stmt->bindParam(4, $createdBy, ParameterType::INTEGER);
            $stmt->bindParam(5, $updatedBy, ParameterType::INTEGER);
            $result = $stmt->execute();
            if (is_bool($result) && $result) {
                $integrationsId = $this->conn->lastInsertId();
            }
            return $integrationsId;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $company_id
     * @param int $project_campaign_id
     * @return string|null
     */
    public function getIntegrationsEMioApiKey(int $company_id, int $project_campaign_id): ?string
    {


        $email_in_one = strtolower(IntegrationTypes::INTEGRATION_EMIO);
        $active = StatusDefRepository::ACTIVE;

        $this->logger->info('PARAMS',
            [$company_id, $project_campaign_id, $email_in_one, $active, __METHOD__, __LINE__]);


        $selectEmioIntegration =
            "SELECT orm.meta_value 
				FROM company c 
				JOIN campaign cp on cp.company_id = c.id AND cp.id = :project_campaign_id
				JOIN integrations i on c.id = i.company_id AND i.campaign_id = :project_campaign_id
				JOIN objectregister o on i.objectregister_id = o.id
				JOIN statusdef s on o.statusdef_id = s.id AND s.value = :active
				JOIN object_register_meta orm ON orm.object_register_id = i.objectregister_id AND orm.meta_key = :email_in_one
				WHERE c.id = :company_id";

        try {
            $stmt = $this->conn->prepare($selectEmioIntegration);
            $stmt->bindValue(':company_id', $company_id, ParameterType::INTEGER);
            $stmt->bindValue(':project_campaign_id', $project_campaign_id, ParameterType::INTEGER);
            $stmt->bindValue(':email_in_one', $email_in_one, ParameterType::STRING);
            $stmt->bindValue(':active', $active, ParameterType::STRING);
            $stmt->execute();
            $jsonConfiguration = $stmt->fetchColumn();

            $this->logger->info('aa$jsonConfiguration', [$jsonConfiguration, __METHOD__, __LINE__]);

            if ($stmt->rowCount() > 0) {
                $configArr = json_decode($jsonConfiguration, true, 512, JSON_OBJECT_AS_ARRAY);

                // > remove
                $x = (!empty($configArr[ 'settings' ][ 'apikey' ])) ? $configArr[ 'settings' ][ 'apikey' ] : 'not found';
                $this->logger->info('$configArr[settings][apikey]', [$x, __METHOD__, __LINE__]);
                // < remove

                return (!empty($configArr[ 'settings' ][ 'apikey' ])) ? $configArr[ 'settings' ][ 'apikey' ] : null;
            }
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            return null;
        } catch (\Doctrine\DBAL\Exception $e) {
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
            return null;
        }
        return null;
    }

    /**
     * @param $integrationName
     * @param int $companyId
     * @author Pradeep
     */
    public function getSettings(string $integrationName, int $companyId): array
    {
        $return = [];

        try {
            if (empty($integrationName) || $companyId <= 0) {
                throw new Exception('Invalid Parameters provided to fetch settings ' . json_encode([
                        'integrationName' => $integrationName,
                        'companyId' => $companyId,
                    ], JSON_THROW_ON_ERROR));
            }
            $sql = '
                select orm.meta_value as settings, i.id 
                from object_register_meta orm
                    join objectregister o on orm.object_register_id = o.id
                    join integrations i on i.objectregister_id = o.id
                    join unique_object_type uot on o.unique_object_type_id = uot.id
                where i.company_id = ? and uot.unique_object_name LIKE ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $companyId, ParameterType::INTEGER);
            $stmt->bindValue(2, $integrationName, ParameterType::STRING);
            $status = $stmt->execute();
            $result = $stmt->fetchAll();
            if (!$status || empty($result[ 0 ][ 'id' ]) || empty($result[ 0 ][ 'settings' ])) {
                return $return;
            }
            $return[ 'id' ] = $result[ 0 ][ 'id' ];
            $json = $result[ 0 ][ 'settings' ];
            $arr = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
            if (!is_array($arr) || !isset($arr[ 'settings' ])) {
                return [];
            }
            $return[ 'settings' ] = $arr[ 'settings' ];
            return $return;
        } catch (\Doctrine\DBAL\Driver\Exception | \Doctrine\DBAL\Exception | JsonException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }
}
