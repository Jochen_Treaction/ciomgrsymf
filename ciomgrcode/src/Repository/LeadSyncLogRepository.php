<?php


namespace App\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;

class LeadSyncLogRepository
{

    public const SYNC_STATUS_TYP_NEW = 'new';
    public const SYNC_STATUS_TYP_LOCKED = 'working';
    public const SYNC_STATUS_TYP_DONE = 'done';
    public const SYNC_STATUS_TYP_ERROR = 'failed';
    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger
    ) {
        $this->conn = $connection;
        $this->logger = $logger;
    }

    /**
     * get fetches the details of lead_sync_log based on integration_id.
     * @param int $integrationId
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Pradeep
     */
    public function get(int $integrationId): array
    {
        $this->logger->info('get :' . json_encode($integrationId));
        try {
            if ($integrationId <= 0) {
                throw new Exception('Invalid Integration Id provided');
            }
            $stmt = 'select * from lead_sync_log where integration_id = ? ORDER BY id desc limit 1';
            $sql = $this->conn->prepare($stmt);
            $sql->bindValue(1, $integrationId);
            $sql->execute();
            $result = $sql->fetchAll();
            if (!is_array($result) || empty($result) || !isset($result[ 0 ])) {
                return [];
            }
            $this->logger->info('get result:' . json_encode($result));
            return $result[ 0 ];
        } catch (Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }

    public function getAll(): array
    {

    }

    /**
     * Insert new record for lead_sync_log.
     * @param int $integrationId
     * @param array $settings
     * @param int $startContactId
     * @param int $endContactId
     * @param int $totalContactsToSync
     * @param int $totalContactsSyncSuccessfully
     * @param int $totalContactsPendingToSync
     * @param string $status
     * @param string $message
     * @return bool
     * @author Pradeep
     */
    public function insert(
        int $integrationId,
        array $settings,
        int $startContactId,
        int $endContactId,
        int $totalContactsToSync = 0,
        int $totalContactsSyncSuccessfully = 0,
        int $totalContactsPendingToSync = 0,
        string $status = self::SYNC_STATUS_TYP_NEW,
        string $message = ""
    ): bool
    {
        $timeStamp = $this->getCurrentTimeStamp();
        $createdTimeStamp = $timeStamp;
        $updatedTimeStamp = $timeStamp;
        $startTimeStamp = $timeStamp;
        $settingsAsStr = json_encode($settings);
        try {
            $stmt = "insert into `lead_sync_log` (
                             integration_id, 
                             settings, 
                             start_contact_id,
                             last_contact_id,
                             total_contacts_to_sync, 
                             total_contacts_sync_succcessfully,
                             total_contacts_pending_to_sync, 
                             start,                             
                             status, 
                             message,
                             created,
                             updated
                             ) values (?,?,?,?,?,?,?,?,?,?,?,?);";
            $sql = $this->conn->prepare($stmt);
            $sql->bindValue(1, $integrationId, ParameterType::INTEGER);
            $sql->bindValue(2, $settingsAsStr);
            $sql->bindValue(3, $startContactId, ParameterType::INTEGER);
            $sql->bindValue(4, $endContactId, ParameterType::INTEGER);
            $sql->bindValue(5, $totalContactsToSync, ParameterType::INTEGER);
            $sql->bindValue(6, $totalContactsSyncSuccessfully, ParameterType::INTEGER);
            $sql->bindValue(7, $totalContactsPendingToSync, ParameterType::INTEGER);
            $sql->bindValue(8, $startTimeStamp);
            $sql->bindValue(9, $status);
            $sql->bindValue(10, $message);
            $sql->bindValue(11, $createdTimeStamp);
            $sql->bindValue(12, $updatedTimeStamp);
            $result = $sql->execute();
            $this->logger->info('insert :' . json_encode($result), [__METHOD__, __LINE__]);
            return $result;
        } catch (Exception|\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    public function update(): bool
    {

    }


    /**
     * Validates whether the integration config (eMIO - apikey or hubspot- apikey)
     *  is changed or same
     * @param $integrationConfig
     * @param array $previousSyncConfig
     * @return bool|null
     * @author Pradeep
     */
    public function isSettingsChanged($integrationConfig, array $previousSyncConfig): ?bool
    {
        $changed = false;
        try {
            if (empty($integrationConfig) || empty($previousSyncConfig)) {
                throw new Exception('Invalid prameters provided ' . json_encode([
                        'currentIntegration' => $integrationConfig,
                        'previousIntegration' => '$previousSyncConfig',
                    ], JSON_THROW_ON_ERROR));
            }
            if ($this->parseAPIKey($integrationConfig) !== $this->parseAPIKey($previousSyncConfig)) {
                $changed = true;
            }
            return $changed;
        } catch (JsonException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * - parseAPIKey is a helper function to parse the APIKEY value for the given
     *  integration config array.
     * @param array $integrationConfig
     * @return mixed|string
     * @author Pradeep
     */
    private function parseAPIKey(array $integrationConfig)
    {
        if (empty($integrationConfig) || !isset($integrationConfig[ 'apikey' ]) || empty($integrationConfig[ 'apikey' ])) {
            return '';
        }
        return $integrationConfig[ 'apikey' ];
    }

    /**
     * getLastContactId returns the value of 'last_contact_id' column value for the given company id.
     * -  the value is fetched from lead_sync_log table.
     * @param $companyId
     * @author Pradeep
     */
    public function getLastContactId($companyId)
    {
        $contactId = 0;
        if ($companyId <= 0) {
            return $contactId;
        }
        $stmt = "
            SELECT lsl.last_contact_id
            FROM lead_sync_log lsl
                     JOIN integrations i on lsl.integration_id = i.id
            WHERE i.company_id = :company_id";
        $sql = $this->conn->prepare($stmt);
        $sql->bindValue(":company_id", $companyId, ParameterType::INTEGER);
        $sql->execute();
        $row = $sql->fetchAll();
        if (!empty($row) && isset($row[ 0 ][ 'last_contact_id' ])) {
            $contactId = $row[ 0 ][ 'last_contact_id' ];
        }
        return $contactId;
    }


    /**
     * helper function to get the current datetime stamp.
     * @return string
     * @author Pradeep
     */
    public function getCurrentTimeStamp(): string
    {
        return date("Y-m-d H:i:s");
    }
}