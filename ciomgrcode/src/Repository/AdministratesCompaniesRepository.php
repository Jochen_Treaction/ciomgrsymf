<?php


namespace App\Repository;



use App\Entity\AdministratesCompanies;
use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Exception;
use Psr\Log\LoggerInterface;
use Doctrine\DBAL\ParameterType;

class AdministratesCompaniesRepository extends ServiceEntityRepository
{

    protected $logger;
    protected $conn;

    public function __construct(Connection $connection, ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, AdministratesCompanies::class);
        $this->logger = $logger;
        $this->conn = $connection;
    }


    /**
     * @param int|null $user_id
     * @return array company_id, name
     */
    public function getManagedCompanies(int $user_id=null):array
    {
        $company_list = [];
        if ($user_id === null) {
            $sql = 'SELECT c.id, c.name FROM company c'; // superAdmin
        } else {
            // $sql = 'SELECT c.id, c.name FROM company c JOIN administrates_companies ac ON (ac.company_id = c.id AND ac.user_id = :user_id)'; // Others
            $sql = 'SELECT c.id, c.name 
                    FROM company c 
                    JOIN administrates_companies ac ON (ac.company_id = c.id AND ac.user_id = :user_id)';
            // TODO: jochen: JOIN USERS on :User_id and JOIN company on USERS own company
        }

        try {
            $stmt = $this->conn->prepare($sql);
            if ($user_id !== null) {
                $stmt->bindParam('user_id', $user_id, ParameterType::INTEGER);
            }
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            if (!empty($temp)) {
                foreach ($temp as $id => $administrated_companies) {
                    $company_list[] = ['id' => $administrated_companies['id'], 'name' => $administrated_companies['name']];
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        // todo: remove
        $this->logger->info('COMPANY LIST=',[$company_list,__METHOD__,__LINE__]);
        return $company_list;
    }


    public function getAssignedCompanies(int $user_id=null):array
    {
        $company_list = [];
        if ($user_id === null) {
            $sql = 'SELECT c.id, c.name FROM company c'; // superAdmin
        } else {
            // $sql = 'SELECT c.id, c.name FROM company c JOIN administrates_companies ac ON (ac.company_id = c.id AND ac.user_id = :user_id)'; // Others
            $sql = 'SELECT c.id, c.name 
                    FROM company c 
                    JOIN administrates_companies ac ON (ac.company_id = c.id AND ac.user_id = :user_id)
                    UNION 
                    SELECT au.to_company_id AS id, c.name 
                    FROM administrates_users au 
                    JOIN company c ON (c.id = au.to_company_id)
                    WHERE au.invited_user_id = :user_id';
        }

        try {
            $stmt = $this->conn->prepare($sql);
            if ($user_id !== null) {
                $stmt->bindParam('user_id', $user_id, ParameterType::INTEGER);
            }
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);

            if (!empty($temp)) {
                foreach ($temp as $id => $administrated_companies) {
                    $company_list[] = ['id' => $administrated_companies['id'], 'name' => $administrated_companies['name']];
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        // todo: remove
        $this->logger->info('COMPANY LIST=',[$company_list,__METHOD__,__LINE__]);
        return $company_list;
    }

    /**
     * @param int $user_id
     * @param int $company_id
     * @return bool
     * @author jsr
     * @internal WEB-3850
     */
    public function doesUserManageCompany(int $user_id, int $company_id):bool
    {
        try {
            $stmt = $this->conn->prepare('SELECT COUNT(*) as cnt FROM administrates_companies WHERE user_id = :user_id AND company_id = :company_id');
            $stmt->bindParam('user_id', $user_id, ParameterType::INTEGER);
            $stmt->bindParam('company_id', $company_id, ParameterType::INTEGER);
            $stmt->execute();
            $temp = $stmt->fetchAll(FetchMode::ASSOCIATIVE);
            $count = $temp[0]['cnt'];
            if ($count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(),[__METHOD__,__LINE__]);
        }
        return false;
    }


    /**
     * @param int $user_id
     * @param int $company_id
     * @return bool
     * @author jsr
     * @internal WEB-3850
     */
    public function addManagedCompany(int $user_id, int $company_id):bool
    {
        try {
            $stmt = $this->conn->prepare('INSERT INTO administrates_companies (user_id, company_id) VALUES (:user_id, :company_id)');
            $stmt->bindParam('user_id', $user_id, ParameterType::INTEGER);
            $stmt->bindParam('company_id', $company_id, ParameterType::INTEGER);
            return $stmt->execute();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__,__LINE__]);
            return false;
        }
    }
}
