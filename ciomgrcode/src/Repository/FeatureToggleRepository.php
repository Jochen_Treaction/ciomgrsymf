<?php

namespace App\Repository;


use Psr\Log\LoggerInterface;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\ParameterType;
use RuntimeException;
use App\Entity\Company;
use DateTime;
use App\Entity\Objectregister;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

class FeatureToggleRepository extends ServiceEntityRepository
{

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $conn;

    public function __construct(Connection $connection, ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Company::class);
        $this->logger = $logger;
        $this->conn = $connection;
    }


    /**
     * @return array ['hubspot' => true, 'PagesABTest' => false, ... ]
     */
    public function getFeatureToggles(): array
    {
        $features=[];

        try {

            $sql = 'SELECT feature, status FROM feature_toggle';
            $stmt = $this->conn->prepare($sql);

            if ($stmt->execute()) {
                $rows = $stmt->fetchAll();

                if(!empty($rows)) {
                    foreach ($rows as $row) {
                        $features[$row['feature']] = ((int) $row['status'] === 1) ? true: false;
                    }
                }
            }
            return $features;
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $features;
        }
    }


    /**
     * @param string $featureName (can be upper-, lower-, camelcase)
     * @return null|bool
     */
    public function getFeatureToggleStatusByName(string $featureName): ?bool
    {
        $this->logger->info('$featureName', [$featureName, __METHOD__, __LINE__]);
        try {
            $sql ='SELECT status as stat FROM feature_toggle WHERE LOWER(feature) = :paramfeature';

            $stmt = $this->conn->prepare($sql);
            $strtolowerFeature = trim(strtolower($featureName));
            $stmt->bindParam('paramfeature', $strtolowerFeature , ParameterType::STRING);
            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                $status = $stmt->fetchColumn();

                if (false === $status) {
                    return null;
                } elseif (0 == $status) {
                    return false;
                } elseif (1 == $status) {
                    return true;
                } else {
                    return null;
                }
            } else {
                return null;
            }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    /**
     * @return object|null
     */
    public function getFeatureTogglesAsObject():?object
    {
        $features = $this->getFeatureToggles();
        return (!empty($features)) ? (object) $features : null;
    }

}
