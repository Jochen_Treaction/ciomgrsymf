<?php

namespace App\Repository;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;


class ExternalDataQueueRepository
{
    private $logger;
    private $conn;
    /**
     * @var ObjectRegisterRepository
     * @author Pradeep
     */
    private $objectRegisterRepository;
    /**
     * @var CustomfieldsRepository
     * @author Pradeep
     */
    private $customfieldsRepository;
    /**
     * @var StandardFieldMappingRepository
     * @author Pradeep
     */
    private $standardFieldMappingRepository;

    public function __construct(
        Connection $connection,
        LoggerInterface $logger,
        ObjectRegisterRepository $objectRegisterRepository,
        CustomfieldsRepository $customfieldsRepository,
        StandardFieldMappingRepository $standardFieldMappingRepository
    ) {
        $this->logger = $logger;
        $this->conn = $connection;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->customfieldsRepository = $customfieldsRepository;
        $this->standardFieldMappingRepository = $standardFieldMappingRepository;
    }


    /**
     * @param int    $companyId
     * @param string $webhookType
     * @param string $queueStatus
     * @return int
     */
    public function getCountPagingForExternalDataQueueView(int $companyId, string $webhookType, string $queueStatus):int
    {
        if('all' === strtolower(trim($webhookType))) {
            $andWebhookType = '';
        }  else {
            $andWebhookType = "AND c.name = $webhookType";
        }

        if( 'all' === strtolower(trim($queueStatus))) {
            $andStatus = '';
        } else {
            $andStatus = "AND eq.status = '$queueStatus'";
        }

        try {
            $sql =
                "SELECT count(*) 
                FROM externaldata_queue eq
                JOIN campaign c ON c.company_id = eq.company_id AND c.objectregister_id = eq.webhook_objectregister_id $andWebhookType
                WHERE eq.company_id = :companyId
                $andStatus";

            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':companyId', $companyId,   \PDO::PARAM_INT);

            $stmt->execute();
            return $stmt->fetchColumn();

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return 0;
        }
    }


    /**
     * returns [] in case of error or all records of externaldata_queue along with data from joined with campaign, objectregister, statusdef<br>
     * <code>[id, company_id, account_id, webhook_objectregister_id, start, end, status, message, created, updated, webhook_objectregister_id__campaign__name, webhook_objectregister_id__objectregister__statusdef__status, webhook_objectregister_id__objectregister__comment, webhook_objectregister_id__objectregister__object_unique_id]</code>
     *
     * @param int $companyId
     * @return array
     */
    public function getAllRecordsForExternalDataQueueView(int $companyId=0):array
    {
        $allRecordsExternalDataQueue =[];
        $sql =
            "SELECT 
                eq.id,
                eq.company_id ,
                eq.account_id,
                eq.webhook_objectregister_id,
                eq.start,
                eq.end,
                eq.status,
                eq.updated,
                cp.name as webhook,
                cp.id as webhook_id,
                o.object_unique_id as webhook_UUID
            FROM externaldata_queue eq
            JOIN campaign cp ON cp.company_id = eq.company_id AND cp.objectregister_id = eq.webhook_objectregister_id
            JOIN objectregister o ON o.id = cp.objectregister_id
            JOIN statusdef sd ON sd.id = o.statusdef_id
            WHERE eq.company_id = :companyId
            ORDER BY eq.updated DESC
            ";


        //@pradeep - change the id to updated desc
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':companyId', $companyId, \PDO::PARAM_INT);
            $stmt->execute();
            $allRecordsExternalDataQueue = $stmt->fetchAll();
        } catch (Exception $e) {
            $this->logger->error('DBAL ERROR '.$e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $allRecordsExternalDataQueue;
    }


    /**
     * returns array <code>['records' => $allRecordsExternalDataQueue, 'minId' => $minId, 'maxId' =>$maxId]</code>
     * @param int    $companyId
     * @param string $externaldata_queue_status
     * @param string $externaldata_queue_webhookname
     * @param int    $externaldata_queue_id
     * @param string $externaldata_queue_operator
     * @param int    $limit
     * @return array
     */
    public function getPagingExternalDataQueueView(int $companyId,
                                                   string $externaldata_queue_status,
                                                   string $externaldata_queue_webhookname,
                                                   int $offset,
                                                   int $limit): array
    {
        $allRecordsExternalDataQueue = [];
        $ids = [];
        $minId = 0;
        $maxId = 0;

        if ('all' === strtolower(trim($externaldata_queue_status))) {
            $and_Status = '';
        } else {
            $and_Status = "AND eq.status = '$externaldata_queue_status'";
        }

        if ('all' === strtolower(trim($externaldata_queue_webhookname))) {
            $and_CpName = '';
        } else {
            $and_CpName = "AND cp.name = '$externaldata_queue_webhookname'";
        }

        $_limit = "LIMIT $offset , $limit";

        $sql =
            "SELECT 
                eq.id,
                eq.company_id ,
                eq.account_id,
                eq.webhook_objectregister_id,
                eq.start,
                eq.end,
                eq.status,
                eq.message,
                eq.created,
                eq.updated,
                cp.name as webhook,
                o.id as webhook_id,
                o.object_unique_id as webhook_UUID
            FROM externaldata_queue eq
            JOIN campaign cp ON cp.company_id = eq.company_id AND cp.objectregister_id = eq.webhook_objectregister_id $and_CpName
            JOIN objectregister o ON o.id = cp.objectregister_id
            JOIN statusdef sd ON sd.id = o.statusdef_id
            $and_Status
            AND eq.company_id = :companyId
            ORDER BY eq.updated DESC
            $_limit
            ";



        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':companyId', $companyId, \PDO::PARAM_INT);
            $stmt->execute();
            $allRecordsExternalDataQueue = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $this->logger->error('$$$queueDebug DBAL ERROR '.$e->getMessage(), [__METHOD__, __LINE__]);
        }

        if($stmt->rowCount() > 0) {
            foreach($allRecordsExternalDataQueue as $record) {
                $ids[] = $record[ 'id' ];
            }
            sort($ids);
            $minId = $ids[ 0 ];
            $maxId = $ids[ count($ids) - 1 ];
        }

        return ['records' => $allRecordsExternalDataQueue, 'minId' => $minId, 'maxId' => $maxId];
    }

    public function getNumberOfUnProcessedRecords(int $companyId): int
    {
        $result = 0;
        $sql = "select count(*) as records from externaldata_queue where company_id = :company_id and status = 'new'";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(":company_id", $companyId, ParameterType::INTEGER);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        if (isset($rows[ 0 ][ 'records' ])) {
            $result = (int)$rows[ 0 ][ 'records' ];
        }
        return $result;
    }

    /**
     * returns all campaign names alias "webhook names" that are present in externaldata_queue for a specific company alias "account"
     * <br><code>['webhook_objectregister_id__campaign__name' => 'some name', ... ]</code>
     * @param int $companyId
     * @return array
     */
    public function getAllWebhookNamesForSelectBox(int $companyId = 0): array
    {
        $allNames = [];
        $sql =
            "
            select distinct c.name from campaign c
                                join objectregister o on o.id = c.objectregister_id
                                join statusdef s on o.statusdef_id = s.id and s.value = 'active'
                                join unique_object_type uot on o.unique_object_type_id = uot.id and uot.unique_object_name = 'webhook'
                                where c.company_id= :company_id order by updated desc
            ";

        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $companyId, \PDO::PARAM_INT);
            $stmt->execute();
            $selectedNames = $stmt->fetchAll();

            $allNames[] = 'All';
            foreach ($selectedNames as $record) {
                $allNames[] = $record['name'];
            }

        } catch (Exception $e) {
            $this->logger->error('DBAL ERROR '.$e->getMessage(), [__METHOD__, __LINE__]);
        }

        $this->logger->info('allNames', [$allNames, __METHOD__,__LINE__]);
        return $allNames;
    }

    /**
     * @param int $externalDataQueueId
     * @return array
     * @author Aki
     */
    public function getWebhookQueueContactDataWithId(int $externalDataQueueId):array
    {
        $externalContactData = [];
        //basic check
        if(!$externalDataQueueId){
            return $externalContactData;
        }
        //fetch the data from DB
        $sql = "select data_to_process from externaldata_queue where id = :externaldata_queue_id";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':externaldata_queue_id', $externalDataQueueId, \PDO::PARAM_INT);
            $stmt->execute();
            $compressedExternalContactData = $stmt->fetch();
        } catch (Exception $e) {
            $this->logger->error('DBAL ERROR '.$e->getMessage(), [__METHOD__, __LINE__]);
        }
        try{
            //process the data
            $decodedExternalContactData = base64_decode($compressedExternalContactData['data_to_process']);
            // decompress the data if 'gzcompress' is used to compress the data.
            $decodedString = gzuncompress($decodedExternalContactData);
            $externalContactData = json_decode($decodedString, true, 512, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
        } catch (Exception $e) {
            $this->logger->error('External Contact Data Conversion Error '.$e->getMessage(),
                [__METHOD__, __LINE__]);
        }
        return $externalContactData;

    }
}
