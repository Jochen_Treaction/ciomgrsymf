<?php


namespace App\Repository;


use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;
use RuntimeException;

class StandardFieldMappingRepository
{
    public const eMIOSystem = UniqueObjectTypes::EMAILINONE;
    public const hubspotSystem = UniqueObjectTypes::HUBSPOT;
    // flag is obtained from frontend, either in integration-emo-twig.js Or emailinone.twig.
    public const auto_create_flag = 'Auto-Create';

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var Connection
     * @author Pradeep
     */
    private $conn;

    public function __construct(LoggerInterface $logger, Connection $conn)
    {
        $this->logger = $logger;
        $this->conn = $conn;
    }

    /**
     * @param int $systemId
     * @param int $mioStandardFieldId
     * @param string $name
     * @param string $datatype
     * @return bool|null
     * @author Pradeep
     */
    public function insert(int $systemId, int $mioStandardFieldId, string $name, string $datatype): ?bool
    {
        try {
            if ($systemId <= 0 || $mioStandardFieldId <= 0 || empty($name)) {
                throw new RuntimeException("Invalid SystemId or MIOStandardFieldId or Name provided.");
            }
            $this->logger->info('Insert ' . json_encode([
                    'systemId' => $systemId,
                    'mioStandardFieldId' => $mioStandardFieldId,
                    'name' => $name,
                    'datatype' => $datatype,
                ], JSON_THROW_ON_ERROR | true));
            $sql = '    
                INSERT INTO standardfieldmapping(system_id, miostandardfield_id, name, datatype) 
                VALUES (:system_id, :miostandardfield_id, :name, :datatype)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':system_id', $systemId, ParameterType::INTEGER);
            $stmt->bindParam(':miostandardfield_id', $mioStandardFieldId, ParameterType::INTEGER);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':datatype', $datatype);
            return $stmt->execute();
        } catch (Exception | RuntimeException | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    public function getEMIOFieldsForMapping(string $systemName): ?array
    {
        if (!$this->isValidSystem($systemName)) {
            return [];
        }
        $fields[ 'mapping' ] = $this->getAll($systemName);
        //$fields[ 'mio' ] = $this->contactRepository->getStandardFieldsForSuperAdminEMIOMappingWizard();
        $fields[ 'mio' ] = $this->getMIOStandardFields();
        //$fields[ 'emio' ] = ['SALUTATION', 'FIRSTNAME', 'LASTNAME'];

        return $fields;
    }

    protected function isValidSystem(string $systemName): bool
    {
        return true;
    }

    public function getAll(string $systemName): ?array
    {
        try {
            $systemId = $this->getSystemId($systemName);
            if (is_null($systemId)) {
                throw new RuntimeException("Failed to get SystemId");
            }
            $sql = 'SELECT m.id, sfm.name, sfm.datatype as emioDType, m.fieldname,m.htmllabel, d.name as mioDType
                    FROM standardfieldmapping sfm
                             join miostandardfield m on sfm.miostandardfield_id = m.id
                             join datatypes d on m.datatypes_id = d.id
                             join system s On sfm.system_id = s.id
                    WHERE s.name = :system_name
                    ORDER BY m.fieldname';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':system_name', $systemName);
            if ($stmt->execute()) {
                $result = $stmt->fetchAll();
            }
            return $result;
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return null;
        }
    }

    /**
     * @param string $systemName
     * @return int|null
     * @author Pradeep
     */
    public function getSystemId(string $systemName): ?int
    {
        $systemId = 0;
        try {
            if (!$this->isValidSystem($systemName)) {
                throw new RuntimeException("Invalid SystemName provided");
            }
            $sql = 'SELECT * FROM system WHERE name =:system_name';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':system_name', $systemName);
            $status = $stmt->execute();
            $result = $stmt->fetchAll();
            if ($status && !empty($result[ 0 ])) {
                $systemId = $result[ 0 ][ 'id' ];
            }
            return $systemId;
        } catch (\Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $systemId;
        }
    }

    public function getMIOTrackingFields(): ?array
    {
        $result = [];
        $sql = 'SELECT msf.*, d.name as datatype
                FROM miostandardfield msf
                    join datatypes d on d.id = msf.datatypes_id 
                WHERE msf.hidden_field=true
                ORDER BY msf.fieldname';
        try {
            $stmt = $this->conn->prepare($sql);
            if ($stmt->execute()) {
                $result = $stmt->fetchAll();
            }
            return $result;
        } catch (\Doctrine\DBAL\Exception | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return $result;
        }
    }

    /**
     * @param int $id
     * @return array|null
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Pradeep
     * @internal Fetches the MIOStandardField information for a given id.
     */
    public function getSingleMIOStandardField(int $id): ?array
    {
        $result = [];
        $sql = 'SELECT msf.*, d.name as datatype
                FROM miostandardfield msf
                    join datatypes d on d.id = msf.datatypes_id 
                WHERE msf.id=?';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(1, $id, ParameterType::INTEGER);
        $status = $stmt->execute();
        $result = $stmt->fetchAll();
        if (!$status || empty($result[ 0 ])) {
            return [];
        }
        return $result[ 0 ];
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getMIOStandardFields(): ?array
    {
        $result = [];
        $sql = 'SELECT msf.*, d.name as datatype
                FROM miostandardfield msf
                    join datatypes d on d.id = msf.datatypes_id 
                ORDER BY msf.fieldname';
        try {
            $stmt = $this->conn->prepare($sql);
            if ($stmt->execute()) {
                $result = $stmt->fetchAll();
            }
            return $result;
        } catch (\Doctrine\DBAL\Exception | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__, __FUNCTION__]);
            return $result;
        }
    }

    /**
     * @return array
     * @author Pradeep
     */
    public function getMIOStandardFieldAsArray()
    {
        $fields = $this->getMIOStandardFields();
        $keys = array_column($fields, 'fieldname');
        $trimmedArray = array_map('trim', $keys);
        return array_unique($trimmedArray);
    }

    /**
     * @param int $systemId
     * @param $miostandardfieldId
     * @return array|null
     * @author Pradeep
     */
    public function getId(int $systemId, int $miostandardfieldId): ?array
    {
        $standardFieldMapping = [];
        try {
            if ($systemId <= 0 || $miostandardfieldId <= 0) {
                throw new RuntimeException('Invalid SystemId or MIOStandardFieldId provided');
            }
            $sql = 'SELECT * 
                    FROM standardfieldmapping 
                    WHERE system_id =:system_id AND miostandardfield_id =:miostandardfield_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':system_id', $systemId, ParameterType::INTEGER);
            $stmt->bindValue(':miostandardfield_id', $miostandardfieldId, ParameterType::INTEGER);
            $status = $stmt->execute();
            $result = $stmt->fetchAll();
            if ($status && !empty($result[ 0 ])) {
                $standardFieldMapping = $result[ 0 ];
            }
            return $standardFieldMapping;
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $systemId
     * @param int $mioStandardFieldId
     * @return bool|null
     * @author Pradeep
     */
    public function delete(int $systemId, int $mioStandardFieldId): ?bool
    {
        try {
            if ($systemId <= 0 || $mioStandardFieldId <= 0) {
                throw new RuntimeException('Invalid SystemId or MIOStandardFieldId provided');
            }
            $sql = 'DELETE FROM standardfieldmapping WHERE system_id =:systemId and miostandardfield_id =:miostandardfield_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':systemId', $systemId, ParameterType::INTEGER);
            $stmt->bindValue(':miostandardfield_id', $mioStandardFieldId, ParameterType::INTEGER);
            return $stmt->execute();
        } catch (Exception | \Doctrine\DBAL\Exception  $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param string $systemName
     * @return array|null
     * @author Pradeep
     */
    public function getAutoCreateFields(string $systemName): ?array
    {
        try {
            if (empty($systemName)) {
                throw new RuntimeException('Invalid SystemName provided');
            }
            $sql = 'select m.fieldname, sfm.id, m.id as mio_id from standardfieldmapping sfm
                        join `system` s on s.id = sfm.system_id
                        join miostandardfield m on sfm.miostandardfield_id = m.id
                    where s.name =:system_name AND (sfm.name LIKE :fieldname OR sfm.name LIKE "%MIO_%") ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':system_name', $systemName);
            $stmt->bindValue(':fieldname', self::auto_create_flag);
            if ($stmt->execute()) {
                return $stmt->fetchAll();
            }
            return [];
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $fieldName
     * @param int $standardFieldMappingId
     * @return bool
     * @author Pradeep
     */
    public function updateAutoCreateField(string $fieldName, int $standardFieldMappingId): bool
    {
/*        $this->logger->info('updateAutoCreateField ' . json_encode([
                'fieldName' => $fieldName,
                'id' => $standardFieldMappingId,
            ], JSON_THROW_ON_ERROR | true));*/
        try {
            if (empty($fieldName) || $standardFieldMappingId <= 0) {
                throw new RuntimeException('Invalid FieldName or StandardFieldMappingId');
            }
            $fieldMapping = $this->get($standardFieldMappingId);
            /*            $this->logger->info('Field Mapping ' . json_encode([
                                '$fieldMapping' => $fieldMapping,
                            ], JSON_THROW_ON_ERROR | true));*/
            if (empty($fieldMapping)) {
                throw new RuntimeException('Failed to fetch Mapping for StandardFieldMappingId ' . $standardFieldMappingId);
            }// update field with Fieldname.
            if (!$this->update($fieldMapping[ 'id' ], $fieldMapping[ 'system_id' ],
                $fieldMapping[ 'miostandardfield_id' ], $fieldName, 'string')) {
                throw new RuntimeException('Failed to update FieldName');
            }
            return true;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    public function get($id): ?array
    {
        $standardFieldMapping = [];
        try {
            if ($id <= 0) {
                throw new RuntimeException('Invalid StandardFieldMapping Id provided');
            }
            $sql = 'SELECT * FROM standardfieldmapping WHERE id =:id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':id', $id, ParameterType::INTEGER);
            $status = $stmt->execute();
            $result = $stmt->fetchAll();
            if ($status && !empty($result[ 0 ])) {
                $standardFieldMapping = $result[ 0 ];
            }
            return $standardFieldMapping;
        } catch (Exception | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $standardFieldMapping;
        }
    }

    /**
     * @param int $id
     * @param int $systemId
     * @param int $mioStandardFieldId
     * @param string $name
     * @param string $datatype
     * @return bool|null
     * @author Pradeep
     */
    public function update(int $id, int $systemId, int $mioStandardFieldId, string $name, string $datatype): ?bool
    {
        if ($id <= 0 || $systemId <= 0 || $mioStandardFieldId <= 0 || empty($name)) {
            throw new RuntimeException("Invalid SystemId or MIOStandardFieldId or Name provided.");
        }
        $this->logger->info('Update ' . json_encode([
                'id' => $id,
                'systemId' => $systemId,
                'mioStandardFieldId' => $mioStandardFieldId,
                'name' => $name,
                'dtatatype' => $datatype,
            ], JSON_THROW_ON_ERROR | true));
        try {
            $sql = '    
                UPDATE standardfieldmapping
                SET system_id           = :system_id,
                    miostandardfield_id = :miostandardfield_id,
                    name                = :name,
                    datatype            = :datatype
                WHERE id = :id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $id, ParameterType::INTEGER);
            $stmt->bindParam(':system_id', $systemId, ParameterType::INTEGER);
            $stmt->bindParam(':miostandardfield_id', $mioStandardFieldId, ParameterType::INTEGER);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':datatype', $datatype);
            return $stmt->execute();
        } catch (Exception | RuntimeException | \Doctrine\DBAL\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param int $mioStandardFieldId
     * @return string|null
     * @author Pradeep
     */
    public function getDataType(int $mioStandardFieldId): ?string
    {
        $datatype = '';
        try {
            if ($mioStandardFieldId <= 0) {
                throw new RuntimeException('Invalid MIO StandardField Id provided');
            }
            $sql = 'SELECT d.name mioDType
                    FROM miostandardfield msf
                             JOIN datatypes d on msf.datatypes_id = d.id
                    WHERE msf.id = :miostandardfield_id ';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':miostandardfield_id', $mioStandardFieldId, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result[ 0 ])) {
                $datatype = $result[ 0 ][ 'mioDType' ];
            }
            $this->logger->info('MIOStandardFieldId ' . $mioStandardFieldId);
            $this->logger->info('datatype' . $datatype);
            return $datatype;
        } catch (Exception | \Doctrine\DBAL\Exception | RuntimeException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $datatype;
        }

    }
}
