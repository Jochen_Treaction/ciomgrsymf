<?php


namespace App\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * @property LoggerInterface logger
 * @property Connection conn
 */
class StatusDefRepository
{
    public const ACTIVE = 'active';
    public const ARCHIVED = 'archived';
    public const INSTALLED = 'installed';
    public const AVAILABLE = 'available';
    public const DRAFT = 'draft';
    public const INACTIVE = 'inactive';
    public const FRAUD = 'fraud';
    public const DEFINED = 'defined';
    public const STATUSDEF_NEW = 'new';
    public const STATUSDEF_CONFIRMED = 'confirmed';
    public const STATUSDEF_MARKEDFORDELETION = 'markedfordeletion';
    public const STATUSDEF_UNSUBSCRIBED = 'unsubscribed';
    public const STATUSDEF_BLACKLISTED = 'blacklisted';
    public const STATUSDEF_BLOCKED = 'blocked';


    public function __construct(LoggerInterface $logger, Connection $connection)
    {
        $this->logger = $logger;
        $this->conn = $connection;
    }

    /**
     * @param string $value
     * @param int $createdBy
     * @param int $updatedBy
     * @return int|null
     * @author Pradeep
     */
    public function insert(string $value, int $createdBy, int $updatedBy) :?int
    {
        return 1;
    }

    public function update()
    {

    }

    public function delete()
    {

    }

    public function get(int $statusDefId):?array
    {
        return [];
    }

    public function getId(string $value): ?int
    {
        $id = 0;
        try {
            if (empty($value)) {
                throw new RuntimeException('Invalid statusdef valud provided');
            }
            $sql = 'SELECT id FROM statusdef WHERE value = :value';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':value', $value, ParameterType::STRING);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if (!empty($result) && $rowCount > 0) {
                $id = $result[ 0 ][ 'id' ];
            }
            return $id;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $status
     * @return bool
     * @author Pradeep
     */
    public function isValidStatus(string $status): bool
    {
        $isValid = false;
        $statusId = $this->getId($status);
        if ($statusId > 0 && !is_null($statusId)) {
            $isValid = true;
        }
        return $isValid;
    }
}