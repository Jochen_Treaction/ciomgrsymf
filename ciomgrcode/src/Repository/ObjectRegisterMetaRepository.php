<?php


namespace App\Repository;


// use App\Services\AuthenticationService;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

// use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class ObjectRegisterMetaRepository
{
    //public constants
    public const MetaKeyProject = 'project';
    public const MetaKeyWebHook = 'webhook';
    public const MetaKeyPage = 'page';
    public const MetaKeyAPIkey = 'account_apikey';
    public const MetaKeyMessage = 'messages';
    public const MetaKeyFraudSettings = 'fraud_settings';
    public const METAKEY_ADVERTORIAL_STATUS = 'advertorial_status';
    public const METAKEY_SEEDPOOL_STATUS = 'seedpool_status';

    private $authenticationUtils;
    private $authenticationService;
    private $userId;

    public function __construct(
        Connection      $connection,
        LoggerInterface $logger
        // AuthenticationUtils $authenticationUtils,
        // AuthenticationService $authenticationService
    )
    {
        // $this->authenticationService = $authenticationService;
        // $this->authenticationUtils = $authenticationUtils;
        $this->conn = $connection;
        $this->logger = $logger;
        //user Details
        $this->setUserId();
    }


    private function setUserId()
    {
        // set userId
        $user = null;

        //$userEmail = $this->authenticationUtils->getLastUsername();
        //$user = $this->authenticationService->getUserParametersFromCache($userEmail);

        if (isset($user['user_id'])) {
            $this->userId = $user['user_id'];
        } else {
            $this->userId = 1;
        }
    }

    /**
     * @param int $objectregisterId
     * @param string $metaKey
     * @param string $metaValue
     * @return int|null
     */
    public function insert(int $objectregisterId, string $metaKey, string $metaValue): ?int
    {
        $id = null;
        $userId = $this->userId;
        try {
            if ($objectregisterId <= 0 || empty($metaKey) || empty($metaValue)) {
                throw new RuntimeException('Invalid Parameters provided');
            }
            $sql = 'INSERT INTO object_register_meta(object_register_id, meta_key, meta_value, created_by, updated_by) 
                    VALUE(:objectregister_id, :meta_key, :meta_value, :created_by,:updated_by)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':objectregister_id', $objectregisterId, ParameterType::INTEGER);
            $stmt->bindParam(':meta_key', $metaKey, ParameterType::STRING);
            $stmt->bindParam(':meta_value', $metaValue, ParameterType::STRING);
            $stmt->bindParam(':created_by', $userId, ParameterType::INTEGER);
            $stmt->bindParam(':updated_by', $userId, ParameterType::INTEGER);
            $result = $stmt->execute();
            if ($result === true) {
                $id = $this->conn->lastInsertId();
            }
            return $id;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    public function update(int $id, int $objectregisterId, string $metaKey, string $metaValue): bool
    {
        $this->logger->info('PARAMS', [$id, $objectregisterId, $metaKey, $metaValue, __METHOD__, __LINE__]);
        $status = false;
        try {
            if ($id <= 0 || $objectregisterId <= 0 || empty($metaKey) || ('0' !== $metaValue && empty($metaValue))) {
                throw new RuntimeException('Invalid parameters provided');
            }
            $sql = 'UPDATE object_register_meta
                    SET meta_key           = :meta_key,
                        meta_value         = :meta_value,
                        object_register_id = :object_register_id
                    WHERE id = :id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':meta_key', $metaKey, \PDO::PARAM_STR);
            $stmt->bindParam(':meta_value', $metaValue, \PDO::PARAM_STR);
            $stmt->bindParam(':object_register_id', $objectregisterId, \PDO::PARAM_INT);
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
            $result = $stmt->execute();

            try {
                $this->logger->info('rowcount', [$stmt->rowCount(), $this->conn->lastInsertId(), __METHOD__, __LINE__]);
            } catch (Exception $e) {
                ;
            }
            if (is_bool($result) && $result) {
                $status = $result;
            }
            return $status;
        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @param int $id
     * @return array|null
     * @throws \Doctrine\DBAL\Exception
     * @author Aki
     */
    public function get(int $id): ?array
    {
        // basic check
        if ($id <= 0) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return null;
        }
        $sqlSelect = "select * from object_register_meta where id= :id";
        try {
            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindValue(':id', $id, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetch();
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $result;

    }

    /**
     * @param int $id
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Aki
     */
    public function delete(int $id): bool
    {
        if ($id <= 0) {
            throw new RuntimeException('Invalid Parameters provided');
        }
        $deleteSql = "delete from object_register_meta where id = :id";
        try {
            $stmt = $this->conn->prepare($deleteSql);
            $stmt->bindParam(':id', $id, ParameterType::INTEGER);
            $result = $stmt->execute();
            return (is_bool($result) && $result) ? $result : false;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
        return $result;

    }

    /**
     * @param int $objectregisterId
     * @return string
     * @author Pradeep
     */
    public function getAccountAPIKey(int $objectregisterId): ?string
    {
        $apikey = null;
        try {
            if ($objectregisterId <= 0) {
                throw new RuntimeException('Invalid ObjectRegisterId');
            }
            $sql = 'SELECT object_register_meta.meta_value as apikey FROM object_register_meta
                        JOIN objectregister o on object_register_meta.object_register_id = o.id
                    WHERE o.id=:objectregisterId AND object_register_meta.meta_key LIKE "account_apikey"';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':objectregisterId', $objectregisterId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[0])) {
                $apikey = $result[0]['apikey'];
            }
            return $apikey;
        } catch (DBALException|RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param int $objectRegisterId
     * @param string $metaKey
     * @return array|null
     * @author AKI
     */
    public function getJsonMetaValueAsArray(int $objectRegisterId, string $metaKey): ?array
    {
        $value = $this->getMetaValue($objectRegisterId, $metaKey);
        if (null !== $value) {
            return json_decode($value, true, 50, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
        }
        return null;
    }

    public function getMetaValueAsString(int $objectRegisterId, string $metaKey):?string
    {
        $value = $this->getMetaValue($objectRegisterId, $metaKey);
        return $value ?? null;
    }

    private function getMetaValue(int $objectRegisterId, string $metaKey): ?string
    {
        if ($objectRegisterId <= 0 || empty($metaKey)) {
            return null;
        }
        try {
            $sql = '
                SELECT meta_value
                FROM object_register_meta
                WHERE object_register_id = :objectRegisterId AND meta_key = :metakey';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':objectRegisterId', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindValue(':metakey', $metaKey, ParameterType::STRING);
            $stmt->execute();
            $value = $stmt->fetchColumn();
            if ($stmt->rowCount() <= 0) {
                $this->logger->warning('No rows found for objectRegisterMeta for given objectRegisterId=' . $objectRegisterId,
                    [__METHOD__, __LINE__]);
                return null;
            }

        } catch (\Doctrine\DBAL\Driver\Exception|RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $value;
    }

    /**
     * @param int $objectRegisterId
     * @param string $value
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Aki
     */
    public function updateProjectSettings(int $objectRegisterId, string $value): bool
    {
        $updateCampaignParams = "update object_register_meta set meta_value = :value  
                                    where object_register_id = :objectRegisterId and meta_key =:metakey";

        $stmt = $this->conn->prepare($updateCampaignParams);
        $stmt->bindValue(':objectRegisterId', $objectRegisterId, ParameterType::INTEGER);
        $stmt->bindValue(':value', $value, ParameterType::STRING);
        $stmt->bindValue(':metakey', self::MetaKeyProject, ParameterType::STRING);

        try {
            $stmt->execute();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('update for campaign_params related to campaign.objectregister_id=' . $objectRegisterId . ' failed.',
                    [__METHOD__, __LINE__]);
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }

        return true;

    }

    /**
     * @param int $objectRegisterId
     * @param string $metaKey
     * @return int|null
     * @author Aki
     */
    public function getIdWithObjectRegisterId(int $objectRegisterId, string $metaKey): ?int
    {
        // basic check
        if ($objectRegisterId <= 0 || empty($metaKey)) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return null;
        }
        $sqlQuery = "select id from object_register_meta where object_register_id = :objectRegisterId and meta_key = :metaKey";
        try {
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->bindValue(':objectRegisterId', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindValue(':metaKey', $metaKey, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetch();
            if ($result) {
                $id = $result['id'];
                return $id;
            }
            return null;
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    public function getObjectRegisterMetaForCompany(int $companyId, string $uniqueObjectName): ?array
    {
        $ObjectRegisterMeta = null;
        try {
            if ($companyId <= 0 || empty($uniqueObjectName)) {
                throw new RuntimeException('Invalid CompanyId or UniqueObjectName passed');
            }
            $sql = 'select orm.*
                    from object_register_meta orm
                             join objectregister o on o.id = orm.object_register_id
                             join integrations i on o.id = i.objectregister_id
                             join unique_object_type uot on o.unique_object_type_id = uot.id
                    where i.company_id = :company_id
                      and uot.unique_object_name = :unique_object_name';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $companyId);
            $stmt->bindParam(':unique_object_name', $uniqueObjectName);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result)) {
                $ObjectRegisterMeta = $result;
            }
            return $ObjectRegisterMeta;
        } catch (DBALException|Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $ObjectRegisterMeta;
        }
    }

    /**
     * returns <code>[][id,    object_register_id,    meta_key,    meta_value,    created_at,    created_by,    updated_at,    updated_by]</code>
     * @param int $objectRegisterId
     * @return array|null
     * @author Pradeep
     */
    public function getObjectRegisterMetaDetails(int $objectRegisterId, string $metaKey): ?array
    {
        $objectRegisterDetails = [];
        try {
            if ($objectRegisterId <= 0) {
                throw new RuntimeException('Invalid ObjectRegisterId provided');
            }
            $sql = 'SELECT * FROM object_register_meta WHERE object_register_id=:object_register_id AND meta_key=:meta_key';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':object_register_id', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindValue(':meta_key', $metaKey);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if ($stmt->rowCount() > 0 && !empty($result[0])) {
                $objectRegisterDetails = $result[0];
            }
            return $objectRegisterDetails;
        } catch (\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception|RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage());
            return $objectRegisterDetails;
        }
    }

    /**
     * @param int $objectRegisterId
     * @param string $value
     * @param string $metaKey
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Aki
     */
    public function updateSettings(int $objectRegisterId, string $value, string $metaKey): bool
    {
        $updateCampaignParams = "update object_register_meta set meta_value = :value  
                                    where object_register_id = :objectRegisterId and meta_key =:metakey";
        $stmt = $this->conn->prepare($updateCampaignParams);
        $stmt->bindValue(':objectRegisterId', $objectRegisterId, ParameterType::INTEGER);
        $stmt->bindValue(':value', $value, ParameterType::STRING);
        $stmt->bindValue(':metakey', $metaKey, ParameterType::STRING);

        try {
            $stmt->execute();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('update for campaign_params related to campaign.objectregister_id=' . $objectRegisterId . ' failed.',
                    [__METHOD__, __LINE__]);
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }

        return true;

    }

}
