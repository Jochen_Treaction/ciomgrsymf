<?php


namespace App\Repository;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * @property Connection conn
 * @property LoggerInterface logger
 */
class ObjectTypeMetaRepository
{



    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->conn = $connection;
        $this->logger = $logger;
    }

    /**
     * @param string $uniqueObjectName
     * @return array|null
     * @author Pradeep
     */
    public function getObjectTypeMetaDescription(string $uniqueObjectName):?array
    {
        $objectTypeDescription = [];
        try {
            if (empty($uniqueObjectName)) {
                throw new RuntimeException('Invalid Unique Object Type Id provided');
            }
            $sql = 'SELECT otm.meta_value
                    FROM object_type_meta otm
                             JOIN unique_object_type uot ON otm.unique_object_type_id = uot.id
                    WHERE uot.unique_object_name LIKE :unique_object_name
                      AND meta_key LIKE "type_description"';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':unique_object_name', $uniqueObjectName, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0) {
                $objectTypeDescription = $result[ 0 ][ 'meta_value' ];
            }
            return $objectTypeDescription;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $assignedTable
     * @return array|null
     * @author Pradeep
     */
    public function getObjectTypeMetaDescriptionForAssignedTable(string $assignedTable):?array
    {
        $objectTypeDescription=[];
        try {
            $sql = 'SELECT otm.meta_value, uot.unique_object_name as meta_key FROM object_type_meta otm
                        JOIN unique_object_type uot ON otm.unique_object_type_id = uot.id
                    WHERE uot.assigned_table LIKE :assigned_table AND otm.meta_key LIKE "type_description"
                    AND uot.unique_object_name NOT LIKE "webinaris"  
                    AND uot.unique_object_name NOT LIKE "digistore"';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':assigned_table', $assignedTable, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $this->logger->info('getIntFromDB :' . json_encode($result));
            $rowCount = $stmt->rowCount();
            foreach($result as $res) {
                if(!isset($res['meta_value']) || empty($res['meta_value']) ) {
                    continue;
                }
                $objectTypeDescription[$res['meta_key']] = json_decode($res[ 'meta_value' ], true);
            }
            return $objectTypeDescription;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


    public function update():?bool
    {

    }
    public function get(int $objectTypeMetaId) :?array
    {

    }
    public function insert():?int
    {

    }

    /**
     * @param string $uniqueObjectName
     * @return string|null
     * @auther aki
     */
    public function getDefaultSettingsFromObjectTypeMeta(string $uniqueObjectName): ?string
    {
        $defaultSettings = null;
        try {
            if (empty($uniqueObjectName)) {
                throw new RuntimeException('Invalid Unique Object Type Id provided');
            }
            $sql = 'SELECT otm.meta_value
                    FROM object_type_meta otm
                             JOIN unique_object_type uot ON otm.unique_object_type_id = uot.id
                    WHERE uot.unique_object_name LIKE :unique_object_name
                      AND meta_key LIKE "type_default_settings"';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':unique_object_name', $uniqueObjectName, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $rowCount = $stmt->rowCount();
            if ($rowCount > 0) {
                $defaultSettings = $result[0]['meta_value'];
            }
            return $defaultSettings;
        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }


}
