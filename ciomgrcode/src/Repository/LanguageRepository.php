<?php

namespace App\Repository;

use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use Psr\Log\LoggerInterface;
use RuntimeException;

class LanguageRepository
{

    /**
     * @var Connection
     * @author Pradeep
     */
    private Connection $connection;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(Connection $connection, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    /**
     * Returns all the supported languages
     * @return array
     * @author Pradeep
     */
    public function getAll(): array
    {
        try {
            $query = 'SELECT * FROM languages WHERE id > 0';
            $stmt = $this->connection->prepare($query);
            $stmt->execute();
            return $stmt->fetchAll();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
    }

    /**
     * gets the language details based on Id.
     * @param int $id
     * @return array
     * @author Pradeep
     */
    public function get(int $id): array
    {
        $language = [];
        try {
            if ($id <= 0) {
                throw new RuntimeException("Invalid id provided while fetch language details");
            }
            $query = 'SELECT * FROM languages WHERE id = :id';
            $stmt = $this->connection->prepare($query);
            $stmt->bindValue(':id', $id, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $this->logger->info(__CLASS__ . __METHOD__ . 'SQLResult : ' . json_encode($result));
            if (!empty($result) && isset($result[ 0 ][ 'id' ])) {
                $language = $result[ 0 ];
            }
        } catch (RuntimeException|\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $language;
    }

    /**
     * @param string $languageName
     * @return array
     * @author Pradeep
     */
    public function getLanguageByName(string $languageName): array
    {
        $language = [];

        try {
            if (empty($languageName)) {
                throw new RuntimeException("Invalid language Name provided while fetch language details");
            }
            $query = 'SELECT * FROM languages WHERE name = :name';
            $stmt = $this->connection->prepare($query);
            $stmt->bindValue(':name', $languageName);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $this->logger->info(__CLASS__ . __METHOD__ . 'SQLResult : ' . json_encode($result));
            if (!empty($result) && isset($result[ 0 ][ 'id' ])) {
                $language = $result[ 0 ];
            }

        } catch (RuntimeException|\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $language;
    }

}