<?php


namespace App\Repository;

//use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
//use App\Services\AuthenticationService;
use App\Entity\Campaign;
use App\Types\AccountStatusTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\ParameterType;
use Exception;
use Psr\Log\LoggerInterface;
use Types\ConfigurationTargetTypes;
use App\Types\ProjectWizardTypes;

use const MaxMind\Db\Reader\_MM_MAX_INT_BYTES;

/**
 * @author  jsr
 * Class CampaignRepository
 * @package App\Repository
 */
class CampaignRepository extends ServiceEntityRepository
{

    private $logger;
    private $conn;
    private $camp_domain = 'https://dev.campaign-in-one.de';
    private $companyRepository;
    private $configurationTargetRepository;
    private $projectWizardTypes;
    private $objectRegisterRepository;
    private $authenticationUtils;
    private $authenticationService;
    //campaign types
    public const PAGES = "pages";
    public const WEBHOOKS = "webhooks";

    /**
     * @var userId of logged in user from App\Services\AuthenticationService
     */
    private $userId;


    public function __construct(
        ObjectRegisterRepository $objectRegisterRepository,
        ProjectWizardTypes $projectWizardTypes,
        ConfigurationTargetRepository $configurationTargetRepository,
        Connection $connection,
        ManagerRegistry $registry,
        LoggerInterface $logger,
        CompanyRepository $companyRepository
        //AuthenticationUtils $authenticationUtils,
        //AuthenticationService $authenticationService
    ) {
    	//$this->authenticationService = $authenticationService;
    	//$this->authenticationUtils = $authenticationUtils;
        parent::__construct($registry, Campaign::class);
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->projectWizardTypes = $projectWizardTypes;
        $this->configurationTargetRepository = $configurationTargetRepository;
        $this->companyRepository = $companyRepository;
        $this->logger = $logger;
        $this->conn = $connection;

        // set userId
		$this->setUserId();

    }


    private function setUserId()
	{
		// set userId
		// TODO: enable getLastUsername
		// $userEmail = $this->authenticationUtils->getLastUsername();
		// $user = $this->authenticationService->getUserParametersFromCache($userEmail);
		$user = 0;
		if(isset($user['user_id'])) {
			$this->userId = $user['user_id'];
		} else {
			$this->userId = 1;
		}
	}


    /**
     * @param $company_id
     * @param $projectName
     * @return array
     * @author jsr
     */
    public function getCampaignsForDashboard(int $company_id): array
    {
        $result = [];
        try { // TODO FIX ERROR: c.`status` must be fetched via objectregister
            $sql = 'select 
                c.id,
                c.name,
                c.url,
                s.value as status
            from campaign c
            join objectregister o ON c.objectregister_id = o.id
            join statusdef s ON o.statusdef_id = s.id AND s.value = :status_value
            join unique_object_type uot on o.unique_object_type_id = uot.id
            where c.company_id = :company_id and uot.unique_object_name !=:project
            ORDER BY c.id DESC LIMIT 10';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':company_id', $company_id, ParameterType::INTEGER);
            $status = 'active';
            $stmt->bindValue(':status_value', $status, ParameterType::STRING);
            $project='project';
            $stmt->bindValue(':project', $project, ParameterType::STRING);
            $stmt->execute();
            $result = $stmt->fetchAll();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return $result;
    }

    /**
     * @param int $id
     * @return array|null
     * @author Aki
     */
    public function get(int $id): ?array
    {
        // basic check
        if ($id <= 0) {
            $this->logger->critical('Invalid params passed',[__METHOD__,__LINE__]);
            return null;
        }
        $sqlSelect = "select * from campaign where id= :id";
        try {
            $stmt = $this->conn->prepare($sqlSelect);
            $stmt->bindValue(':id', $id, ParameterType::INTEGER);
            $stmt->execute();
            $result = $stmt->fetch();
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $result;
    }


    /**
     * @param int $company_id
     * @return bool
     * @author   jsr
     * @internal WEB-3850
     */
    public function hasCompanyIdCampaigns(int $company_id): bool
    {
        try {
            //TODO : update check with pages and webhooks.
            $sql = 'select 
                count(*) as no_of_camp
            from campaign c
            where company_id = ? and usecase_type not like "%project%"
            ORDER BY c.id DESC LIMIT 10';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $company_id);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if ($result[0]['no_of_camp'] > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
    }


    /**
     * @param int $company_name
     * @return bool
     * @author   jsr
     * @internal WEB-3850
     */
    public function hasCompanyNameCampaigns(string $company_name): bool
    {
        try {
            $sql = 'SELECT COUNT(*) AS no_of_camp FROM campaign ca WHERE ca.company_id IN (SELECT id FROM company WHERE name = ?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $company_name);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if ($result[0]['no_of_camp'] > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return false;
        }
    }


    /**
     * @param string $status
     * @param int    $company_id
     * @param string $useCase_type
     * @return array
     * @author aki
     * @deprecated
     */

    public function getCampaignsByStatusForCompany(string $status, int $company_id, string $useCase_type): array
    {
        $this->logger->info('company_id ' . $company_id, [__METHOD__, __LINE__]);
        $campaigns = [];
        switch ($status) {
            case 'active':
                $sql = 'SELECT c.*,
                            c.activationdate,
                            u.email as created_by,
                            u1.email as updated_by
                            FROM campaign c
                            INNER JOIN users u on c.created_by = u.id
                            INNER JOIN users u1 on c.updated_by = u1.id
                            WHERE c.company_id = ? and c.status = "active"  and c.usecase_type = ?
                            ORDER BY c.activationdate DESC';
                break;
            case 'draft':
                $sql = 'SELECT c.*,
                            CONCAT(c.url,"/versions/",c.version) as url,
                            c.created,
                            u.email as created_by
                            FROM campaign c
                            INNER JOIN users u on c.created_by = u.id
                            WHERE c.company_id = ? and c.status = "draft" and c.usecase_type = ?
                            ORDER BY c.created DESC';

                break;
            case 'archive':
                $sql = 'SELECT c.*,
                            CONCAT(c.url,"/versions/",c.version) as url,
                            c.archivedate,
                            u.email as created_by,
                            u1.email as updated_by
                            FROM campaign c
                            INNER JOIN users u on c.created_by = u.id
                            INNER JOIN users u1 on c.updated_by = u1.id
                            WHERE c.company_id = ? and c.status = "archive" and c.usecase_type = ?
                            ORDER BY c.archivedate DESC';

                break;
            default:
                $sql = '';
        }
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $company_id);
            $stmt->bindValue(2, $useCase_type);
            $stmt->execute();
            $res = $stmt->fetchAll();
            $campaigns = !empty($res) ? $res : [];
            foreach ($campaigns as &$campaign) {
                $sp_config = $this->fetchCampaignParams($campaign['id'], 'Splittest');
                if (empty($sp_config) || empty($sp_config['camp_param_value']) || !isset($sp_config['camp_param_value'])) {
                    continue;
                }
                $json_decoded = json_decode($sp_config['camp_param_value'], true, 512, JSON_THROW_ON_ERROR);
                $campaign['splittest'] = (isset($json_decoded['status']) && $json_decoded['status'] === 'on') ? $json_decoded : '';
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
        return $campaigns;
    }


    /**
     * @param int    $camp_id
     * @param string $cnf_tgt
     * @return array
     * @author pva
     * @deprecated
     */
    public function fetchCampaignParams(int $camp_id, string $cnf_tgt): array
    {
        $camp_params = [];
        if ($camp_id <= 0 || empty($cnf_tgt)) {
            $this->logger->critical('Invalid Campaign Id/Configuration Target', [__METHOD__, __LINE__]);
            return $camp_params;
        }
        $sql = 'SELECT
                    cp.id,
                    cp.value AS camp_param_value,
                    ct.name AS cnf_tgt,
                    cg.name AS cnf_grp
                FROM campaign_params cp
                INNER JOIN configuration_targets ct on cp.configuration_target_id = ct.id
                INNER JOIN configuration_groups cg on ct.configuration_group_id = cg.id
                WHERE ct.name LIKE :cnf_tgt AND cp.campaign_Id = :camp_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue('cnf_tgt', $cnf_tgt);
            $stmt->bindValue('camp_id', $camp_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) || !empty($res[0])) {
                $camp_params = $res[0];
            }
            // Check and Decode Json string
            $camp_params['c_set'] = $this->decodeJsonString($camp_params['camp_param_value']);
            $camp_params['c_set']['id'] = $camp_params['id'];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $camp_params;
    }


    /**
     * @param string $json_str
     * @return array
     * @author pva
     */
    private function decodeJsonString(string $json_str): array
    {
        $arr_str = [];
        if (empty($json_str)) {
            $this->logger->info('Empty String', [__METHOD__, __LINE__]);
            return $arr_str;
        }
        // Check for Valid Json string
        if ((json_decode($json_str, true, 512, JSON_THROW_ON_ERROR)) === false) {
            $this->logger->info('Invalid Json string', [__METHOD__, __LINE__]);
            return $arr_str;
        }
        $arr_str = json_decode($json_str, true, 512, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE);
        return $arr_str;
    }


    /**
     * @param int $company_id
     * @return array
     * @author pva
     */
    public function getCampaignForLeadView(int $company_id): array
    {
        $campaigns = [];
        $draft = 'draft';
        $project = 'project';
        if ($company_id <= 0) {
            return $campaigns;
        }
        try {
            /*            $sql = 'SELECT id, name, objectregister_id
                                FROM campaign
                                WHERE company_id = :company_id and objectregister_id > 0
                                group by id, objectregister_id order by id desc ';*/

            $sql = 'SELECT c.id, c.name, c.objectregister_id
                    FROM campaign c
                    Join objectregister o on c.objectregister_id = o.id
                    join unique_object_type uot on o.unique_object_type_id = uot.id
                    join statusdef s on o.statusdef_id = s.id
                    WHERE c.company_id = :company_id and c.objectregister_id > 0 and s.value != :draft and uot.unique_object_name != :project
                    group by c.id, c.objectregister_id order by id desc';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->bindParam(':draft', $draft);
            $stmt->bindParam(':project', $project);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res)) {
                $campaigns = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $campaigns;
    }


    /**
     *
     * @param int    $camp_id
     * @param int    $company_id
     * @param string $company
     * @param int    $user_id
     * @param string $useCase_type
     * @return bool
     * @auther pva
     * @deprecated
     */
    public function createAdditionalVersionsForCampaign(
        int $camp_id,
        int $company_id,
        string $company,
        int $user_id,
        string $useCase_type,
        string $domain
    ): bool {
        $status = false;
        $latest_version = 1;
        if ($camp_id <= 0 || $company_id <= 0 || $user_id <= 0 || empty($company)) {
            $this->logger->critical('invalid params passed', [__METHOD__, __LINE__]);
            return $status;
        }
        $campaign_name = $this->fetchCampaignNameById($camp_id);
        if (empty($campaign_name)) {
            $this->logger->critical('invalid Campaign Name returned', [__METHOD__, __LINE__]);
            return $status;
        }
        // Check for latest version of the campaign.
        try {
            $sql = 'SELECT * 
                    FROM campaign
                    WHERE name LIKE :camp_name AND company_id =:company_id
                    ORDER BY id  DESC';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':camp_name', $campaign_name);
            $stmt->bindValue(':company_id', $company_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            $uniqueObject_id = 0;
            $comment = '';
            if (!empty($res) && !empty($res[0]['version'])) {
                $latest_version = $res[0]['version'];
                $uniqueObject_id = $res[0]['objectregister_id'];
                $comment = $res[0]['comment'];
            }
            $new_version = (int)$latest_version + 1;
            $url = $this->createCampaignURL($campaign_name, $company, $domain);
            if ($uniqueObject_id === null) {//old campaigns we will have unique id 0
                $uniqueObject_id = 0;
            }
            // Check the campaign Id.
            if ($this->createPageInDB($uniqueObject_id, $campaign_name, $useCase_type, $comment, $company_id, $url, $user_id, $new_version) > 0) {
                $status = true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param int $campaign_id
     * @return string
     * @author pva
     */
    public function fetchCampaignNameById(int $campaign_id): string
    {
        $c_name = '';
        if ($campaign_id <= 0) {
            return $c_name;
        }

        $sql = 'SELECT c.name 
                FROM campaign c
                WHERE id= :c_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam('c_id', $campaign_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0]) && !empty($res[0]['name'])) {
                $c_name = $res[0]['name'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $c_name;
    }


    /**
     * @param string $camp_name
     * @param string $company
     * @return string
     * @author pva
     */
    public function createCampaignURL(string $camp_name, string $company, string $domain): string
    {
        $camp_url = '';
        if (!empty($domain) && !empty($camp_name) && !empty($company)) {
            $camp_url = $domain . '/' . $company . '/' . $camp_name;
        }
        return $camp_url;
    }


    /**
     * @param int $ObjectregisterId
     * @param string $name
     * @param int $company_id
     * @param string $url
     * @param int $user_id
     * @return int
     * @auther aki
     */
    public function createPageInDB(
        int $ObjectregisterId,
        string $name,
        int $company_id,
        string $url,
        int $user_id
    ): int {
        $camp_id = 0;
        if (empty($name) || $company_id <= 0 || empty($url) || $user_id <= 0) {
            $this->logger->critical('Invalid params', [__METHOD__, __LINE__]);
            return $camp_id;
        }
        $company = $this->companyRepository->getCompanyDetailById($company_id);
        $comment = 'page for account ' . $company['name'] . ' / ' . $company['account_no'];
        $time_stamp = date('Y-m-d h:i:s');
        try {
            $sql = 'INSERT INTO campaign 
                    (company_id,objectregister_id, name,account_no, url, comment, created, created_by, updated, 
                     updated_by)
                     VALUES (?, ?,?, ?, ?, ?, ?, ?, ?, ?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $company_id);
            $stmt->bindValue(2, $ObjectregisterId);
            $stmt->bindValue(3, $name);
            $stmt->bindValue(4, $company['account_no']);
            $stmt->bindValue(5, $url);
            $stmt->bindValue(6, $comment);
            $stmt->bindValue(7, $time_stamp);
            $stmt->bindValue(8, $user_id);
            $stmt->bindValue(9, $time_stamp);
            $stmt->bindValue(10, $user_id);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                // Get the Inserted Campaign Id
                $camp_id = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $camp_id;
    }


    /**
     * @param string $camp_name
     * @param int    $company_id
     * @return array
     * @author pva
     */
    public function getLatestCampaignInDraft(string $camp_name, int $company_id): array
    {
        $camp_details = [];
        if (empty($camp_name) || $company_id <= 0) {
            return $camp_details;
        }
        $sql = 'SELECT * 
                FROM campaign
                WHERE name LIKE :camp_name and status = "draft" and campaign.company_id = :company_id 
                ORDER BY id DESC';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':camp_name', $camp_name);
            $stmt->bindValue(':company_id', $company_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0])) {
                $camp_details = $res[0];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $camp_details;
    }


    /**
     *
     * @param string $campaign_name
     * @param int    $company_id
     * @return bool
     * @auther aki
     */
    public function isCampaignNameAvailable(string $campaign_name, int $company_id): bool
    {
        $available = false;
        //checking if campaign name is available
        try {
            $sql = 'select * from campaign where name = ? and company_id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $campaign_name);
            $stmt->bindValue(2, $company_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0]) && (int)$res[0]['id'] > 0) {
                $available = true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $available;
    }


    /**
     *
     * @param int $campaign_id
     * @param int $user_id
     * @param int $company_id
     * @return bool
     * @auther aki
     * @deprecated
     */
    public function archivePreviousVersionsOfActiveCampaign(int $campaign_id, int $user_id, int $company_id): bool
    {
        $status = false;
        if ($campaign_id <= 0 || $user_id <= 0 || $company_id <= 0) {
            $this->logger->critical('Invalid params passed', [__METHOD__, __LINE__]);
            return $status;
        }
        $objectRegisterId = $this->getobjectregisterIdWithCampaignId($campaign_id);
        if (empty($camp_name)) {
            $this->logger->critical('Failed to fetch Campaign Name', [__METHOD__, __LINE__]);
            return $status;
        }
        try {
            $sql = 'select c.id
                    from campaign c
                    JOIN objectregister o on c.objectregister_id = o.id
                    where c.name = :campaignName and o.statusdef_id = 1';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':camp_name', $camp_name);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (empty($res) || empty($res[0])) {
                return true;
            }
            $camp_active_id = $res[0]['id'];

            $status = $this->updateCampaign($camp_active_id, 'archive', $user_id);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     *
     * @param int    $campId
     * @param string $updateTo
     * @param int    $userId
     * @return bool
     * @auther aki
     */
    public function updateCampaign(int $campId, string $updateTo, int $userId): bool
    {
        $updateStatus = false;
        if(!empty($updateTo)){
            $statusdef_id = $this->getStatusdefIdByName($updateTo);
        }
        if(!empty($campId) && !empty($statusdef_id)){
            $objectRegisterId = $this->getobjectregisterIdWithCampaignId($campId);
            $updateSql = "update objectregister  set statusdef_id = :statusdefId , updated_by = :userID where id = :objectregisterId";
            $stmt = $this->conn->prepare($updateSql);

            $stmt->bindParam(':statusdefId', $statusdef_id, ParameterType::INTEGER);
            $stmt->bindParam(':objectregisterId', $objectRegisterId, ParameterType::INTEGER);
            $stmt->bindParam(':userID', $userId, ParameterType::INTEGER);
            try{
                $updateStatus = $stmt->execute();
            }catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }
        return $updateStatus;
    }


    /**
     * @param int    $camp_id
     * @param array  $campaign_parameters
     * @param string $campaignCreationType
     * @return bool
     * @deprecated
     */
    public function storeDefaultParams(
        int $camp_id,
        array $campaign_parameters,
        string $campaignCreationType
    ): bool {
        // Default config targets
        if ($camp_id <= 0 || empty($campaign_parameters)) {
            $this->logger->critical('Invalid params passed');
            return false;
        }
        //add default values to campaign Params
        if ($campaignCreationType === 'newCampaign') {
            $default_campaign_params['Settings'] = $this->getDefaultParamsForNewCampaign();

            $default_campaign_params['Settings']['General']['value'] = json_encode(array_merge($campaign_parameters['general'],
                                                                        $campaign_parameters['projectSettings']['advancedsettings']));
            $default_campaign_params['Integrations']['Mapping']['value'] = json_encode($campaign_parameters['mapping']['mappingTable']);
            $default_campaign_params['Flow Control']['Pageflow']['value'] = json_encode($campaign_parameters['pages']);
            $default_campaign_params['Flow Control']['Splittest']['value'] = json_encode($campaign_parameters['splittest']);
        }
        $configuration_target_id = 2;//this is for pages
        $cp_value = json_encode($default_campaign_params);
        $insertIntoCampaignParams = "INSERT INTO campaign_params 
                (campaign_Id, configuration_target_id, value, created_by, updated_by) 
                VALUES 
                (:campaign_Id, :configuration_target_id, :cp_value, :created_by, :updated_by)";

        $stmt = $this->conn->prepare($insertIntoCampaignParams);

        $stmt->bindParam(':campaign_Id', $camp_id, ParameterType::INTEGER);
        $stmt->bindParam(':configuration_target_id', $configuration_target_id, ParameterType::INTEGER);
        $stmt->bindParam(':cp_value', $cp_value, ParameterType::STRING);
        $stmt->bindParam(':created_by', $this->userId, ParameterType::INTEGER);
        $stmt->bindParam(':updated_by', $this->userId, ParameterType::INTEGER);

        try {
            $stmt->execute();

            $lastInsertId = $this->conn->lastInsertId();
            if ($stmt->rowCount() == 0) {
                $this->logger->warning('could not insert record into campaign_params for campaign_id ' . $camp_id, [__METHOD__, __LINE__]);
                return flase;
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return flase;
        }
        return true;
    }


    /**
     * @return array
     * @author Pradeep
     * @deprecated
     */
    public function getDefaultParamsForNewCampaign(): array
    {
        return [
            'Messages' => [
                'singleSubmitErrorText' => 'You have already registred.',
                'countdownExpireMessage' => 'Die Aktion ist beendet',
                'ErrorMsg_Color' => '#FF0000',
                'ErrorMsgDuplicate' => '* Sie haben bereits teilgenommen',
                'ErrorMsgEmail' => '* Bitte überprüfen Sie die E-Mail-Adresse',
                'ErrorMsgTelefon' => '* Bitte überprüfen Sie die Telefonnummer',
                'ErrorMsgimageUpload' => '* Bitte überprüfen Sie die Bildformat nur png, jpg sind erlaubt',
                'ErrorMsgPostalcode' => '* Bitte überprüfen Sie die Postleitzahl',
                'ErrorMsgBlockFreeMailer' => '* Bitte nutzen Sie Ihre geschäftliche E-Mail-Adresse',
                'ErrorMsgAddress' => ' * Bitte überprüfen Sie die Adresse',
                'ErrorMsgCampaignEndDate' => 'Die Aktion ist beendet',
                'ExpirationColor' => '#FF0000',
                'ExpirationTransparency' => '0.1',
            ],
        ];
    }

    /**
     * @param int $campaignId
     * @return array|null
     * @author aki
     * @deprecated
     */
    public function getProjectParamsWithId(int $campaignId):?array
    {
        $ProjectParams = [];
        $getSql = "select value
                    from campaign_params
                    join campaign c on c.id = campaign_params.campaign_Id
                    where campaign_Id = :campaign_id;";
        try {
            $stmt = $this->conn->prepare($getSql);
            $stmt->bindValue(':campaign_id', $campaignId, ParameterType::INTEGER);
            $stmt->execute();
            $res = $stmt->fetch();
            $ProjectParams = $res['value'];
            if (!empty($res)) {
                $cnf_tgts = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return json_decode($ProjectParams,true);
    }


    /**
     * @return array
     */
    private function fetchConfigurationTargets(): array
    {
        $cnf_tgts = [];
        $sql = 'SELECT id, name
                FROM configuration_targets';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res)) {
                $cnf_tgts = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $cnf_tgts;
    }


    /**
     * @param int    $camp_id
     * @param string $cnf_tgt
     * @param array  $campaign_parameters
     * @return bool
     * @author pva
     * @deprecated
     */
    public function storeCampaignParams(int $camp_id, string $cnf_tgt, array $campaign_parameters): bool
    {
        $status = false;
        $cnf_tgt_id = $this->fetchConfigurationTargetId($cnf_tgt);
        if ($camp_id <= 0 || $cnf_tgt_id <= 0) {
            $this->logger->critical('Invalid Campaign id / Configuration Target', [__METHOD__, __LINE__]);
            return $status;
        }
        // Used for update query
        $id = (!empty($campaign_parameters['id']) && (int)$campaign_parameters['id'] > 0) ? $campaign_parameters['id'] : 0;
        $camp_params_json = json_encode($campaign_parameters, JSON_THROW_ON_ERROR | true, 512);
        $sql = 'INSERT INTO campaign_params (id , campaign_Id, configuration_target_id, value) 
                VALUES(:id , :camp_id, :cnf_tgt_id, :camp_params) 
                ON DUPLICATE KEY UPDATE 
                value = :camp_params';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':camp_id', $camp_id);
            $stmt->bindParam(':cnf_tgt_id', $cnf_tgt_id);
            $stmt->bindParam(':camp_params', $camp_params_json);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    /**
     * @param string $cnf_tgt
     * @return int
     * @author pva
     */
    private function fetchConfigurationTargetId(string $cnf_tgt): int
    {
        $cnf_tgt_id = 0;
        if (empty($cnf_tgt)) {
            $this->logger->critical('Invalid Configuration Target', [__METHOD__, __LINE__]);
            return $cnf_tgt_id;
        }
        $sql = 'SELECT id 
                FROM configuration_targets 
                WHERE name LIKE :cnf_tgt';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue('cnf_tgt', $cnf_tgt);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0]) && !empty($res[0]['id'])) {
                $cnf_tgt_id = $res[0]['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $cnf_tgt_id;
    }


    /**
     * @return array
     * @author pva
     */
    public function fetchMappingTemplate(): array
    {
        $sql = 'SELECT * FROM mapping_templates';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetchAll();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $res;
    }


    public function isSurveyConfiguredForCampaign(int $campObjRegId): bool
    {
        $status = false;
        if ($campObjRegId <= 0) {
            return $status;
        }
        $sql = 'SELECT s.id,
                       c.id AS camp_id, 
                       s.survey_description,
                       s.survey_is_online,
                       s.layout_type_id,
                       s.survey_description
                FROM survey s
                INNER JOIN campaign c ON c.survey_id = s.id
                WHERE c.objectregister_id = :camp_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue('camp_id', $campObjRegId);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (is_array($res) && !empty($res)) {
                $status = true;
            }
            return $status;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $status;
        }
    }


    /**
     * @param int $camp_id
     * @return array
     * @author pva
     */
    public function fetchSurveyForCampaign(int $camp_id): array
    {
        $survey = [];
        if ($camp_id <= 0) {
            return $survey;
        }

        $sql = 'SELECT s.id,
                       c.id AS camp_id, 
                       s.survey_description,
                       s.survey_is_online,
                       s.layout_status_id,
                       s2.status_txt
                FROM survey s
                INNER JOIN campaign c ON c.survey_id = s.id
                INNER JOIN status s2 on s.layout_status_id = s2.id
                WHERE c.id = :camp_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue('camp_id', $camp_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (is_array($res) && !empty($res)) {
                $survey = $res[0];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $survey;
    }


    /**
     * Fetches questions from DB based on Campaign id and question id.
     * Fetches single question if question id is mentioned.
     * Fetches all the questions if question id is not mentioned.
     * @param int $camp_id
     * @param int $question_id
     * @return array
     * @author pva
     */
    public function fetchSurveyQuestionsForCampaign(int $camp_id, int $question_id = 0): array
    {
        $questions = [];
        if ($camp_id <= 0) {
            $this->logger->critical('Invalid Campaign Id', [__METHOD__, __LINE__]);
            return $questions;
        }
        $operator = (0 === $question_id) ? '>' : '=';
        $sql = "SELECT q.id AS db_id,
                       q.question_json_representation AS questions_json,
                       q.question_question_txt As question,
                       qt.question_type_html_tag As question_type,
                       q.question_hint_txt As question_hint
                FROM questions q
                INNER JOIN survey s ON q.survey_id = s.id
                INNER JOIN campaign c ON c.survey_id = s.id
                INNER JOIN question_types qt ON q.question_types_id = qt.id
                WHERE c.id = :camp_id AND q.id {$operator} :question_id
                ORDER BY q.id ASC";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue('camp_id', $camp_id);
            $stmt->bindValue(':question_id', $question_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!is_array($res) || empty($res)) {
                $this->logger->info('Survey is empty', [__METHOD__, __LINE__]);
                return $res;
            }
            $questions = $this->formatSurveyQuestions($res);

            // Fetch images for Question (Select and Multiple choice)
            foreach ($questions as &$question) {
                if (!isset($question['db_id']) || $question['question']['type'] === 'range' || $question['question']['type'] === 'textarea') {
                    continue;
                }
                $opt = 1;
                $total_options = count($question['question'][$question['question']['type']]['options']);
                while ($opt <= $total_options) {
                    $image = $this->fetchImageForSurveyQuestion($question['db_id'], $opt);
                    if (!empty($image) && isset($image['img'])) {
                        $question['question'][$question['question']['type']]['options'][$opt]['link_ref'] = $image['img'];
                    }
                    $opt++;
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $questions;
    }


    /**
     * @param array $questions
     * @return array
     * @author pva
     */
    public function formatSurveyQuestions(array $questions): array
    {
        if (empty($questions)) {
            return [];
        }
        foreach ($questions as &$question) {
            if (empty($question['questions_json'])) {
                continue;
            }
            $question['question'] = $this->decodeJsonString($question['questions_json']);
        }
        return $questions;
    }


    /**
     * @param int $question_id
     * @param int $option_id
     * @return array
     * @author Pradeep
     */
    private function fetchImageForSurveyQuestion(int $question_id, int $option_id): array
    {
        $image = [];
        if ($question_id <= 0 || $option_id <= 0) {
            return $image;
        }
        $sql = 'SELECT * FROM images WHERE question_id =:question_id AND option_id =:option_id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':option_id', $option_id);
        $stmt->bindValue(':question_id', $question_id);
        $stmt->execute();
        $res = $stmt->fetchAll();
        if (!empty($res) && !empty($res[0]['img'])) {
            $image = $res[0];
        }
        return $image;
    }


    /**
     * @param int        $camp_id
     * @param int        $survey_id
     * @param array      $questions
     * @param array|null $normalized_images
     * @return bool
     * @author pva
     */
    public function storeSurveyQuestions(
        int $camp_id,
        int $survey_id,
        array $questions,
        array $normalized_images = null
    ): bool {
        $status = false;
        $question_context_id = 1;
        $this->logger->info('storeSurveyQuestions', [__METHOD__, __LINE__]);
        if ($camp_id <= 0 || $survey_id <= 0 || empty($questions)) {
            return $status;
        }
        $sql = 'INSERT INTO 
                questions(id, survey_id, context_id, question_types_id, question_question_txt, 
                          question_hint_txt, question_json_representation) 
                VALUES (:id, :survey_id, :context_id, :question_types_id, :question_question_txt, 
                        :question_hint_txt, :question_json_representation)
                ON DUPLICATE KEY UPDATE 
                context_id = :context_id,
                question_types_id = :question_types_id,
                question_question_txt = :question_question_txt,
                question_hint_txt = :question_hint_txt,
                question_json_representation = :question_json_representation';
        try {
            $stmt = $this->conn->prepare($sql);
            $i = 1;
            foreach ($questions as $question) {
                $id = (isset($question['db_id']) && !empty($question['db_id'])) ? $question['db_id'] : 0;
                $ques_type_id = $this->fetchQuestionTypeIdForSurvey($question['type']);
                // Check for question type id
                if ($ques_type_id <= 0) {
                    $this->logger->critical('Invalid question type Id', [__METHOD__, __LINE__]);
                    continue;
                }
                $question_json = json_encode($question, JSON_THROW_ON_ERROR, 512);
                // Check if json string
                if (is_bool($question_json) && $question_json === false) {
                    $this->logger->critical('json_encode was unsuccessful', [__METHOD__, __LINE__]);
                    continue;
                }
                $stmt->bindValue(':id', $id);
                $stmt->bindValue(':survey_id', $survey_id);
                $stmt->bindValue(':context_id', $question_context_id);
                $stmt->bindValue(':question_types_id', $ques_type_id);
                $stmt->bindValue(':question_question_txt', $question['text']);
                $stmt->bindValue(':question_hint_txt', $question['hint']);
                $stmt->bindValue(':question_json_representation', $question_json);
                $res = $stmt->execute();

                if (is_bool($res) && $res === false) {
                    $this->logger->critical('Insert is not successful', [__METHOD__, __LINE__]);
                }

                $question_id = $this->conn->lastInsertId();
                if ($question['type'] === 'select' || $question['type'] === 'checkbox') {
                    $option = 1;
                    $option_count = count($normalized_images['survey']['question'][$i][$question['type']]['options']);
                    while ($option <= $option_count) {
                        $normalized_image = $normalized_images['survey']['question'][$i][$question['type']]['options'][$option]['link_ref'];
                        if (empty($normalized_image)) {
                            $option++;
                            continue;
                        }
                        $image_id = 0;
                        $image_details = $this->fetchImageForSurveyQuestion($question_id, $option);
                        if (!empty($image_details) && isset($image_details['id'])) {
                            $image_id = $image_details['id'];
                        }
                        $this->storeImageForSurveyQuestion($question_id, $option, $normalized_image, '', $image_id);
                        $option++;
                    }
                }
                $status = $res;
                $i++;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    /**
     * @param string $question_type
     * @return int
     * @author pva
     */
    private function fetchQuestionTypeIdForSurvey(string $question_type): int
    {
        $ques_type_id = 0;
        if (empty($question_type)) {
            $this->logger->critical('Invalid question type', [__METHOD__, __LINE__]);
            return $ques_type_id;
        }
        $sql = 'SELECT qt.id 
                FROM question_types qt 
                WHERE qt.question_type_html_tag LIKE :question_type';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':question_type', $question_type);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0])) {
                $ques_type_id = $res[0]['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $ques_type_id;
    }


    /**
     * @param int    $question_id
     * @param int    $option_id
     * @param array  $normalized_image
     * @param string $img_type
     * @param int    $img_id
     * @return bool
     * @author pva
     */
    private function storeImageForSurveyQuestion(
        int $question_id,
        int $option_id,
        string $normalized_image,
        string $img_type = '',
        int $img_id = 0
    ): bool {
        $status = false;
        if ($question_id <= 0 || empty($normalized_image)) {
            return $status;
        }
        $sql = 'INSERT INTO images(id, question_id, option_id ,imgtype, img)
                VALUES(:id, :question_id, :option_id, :imgtype, :img)
                ON DUPLICATE KEY UPDATE 
                question_id = :question_id,
                option_id = :option_id,
                imgtype = :imgtype,
                img = :img';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':id', $img_id);
            $stmt->bindValue(':option_id', $option_id);
            $stmt->bindValue(':question_id', $question_id);
            $stmt->bindValue(':imgtype', $img_type);
            $stmt->bindValue(':img', $normalized_image);
            $res = $stmt->execute();
            $status = (is_bool($res) && $res) ? $res : $status;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    /**
     * @param int $question_id
     * @return bool
     */
    public function deleteQuestionForSurvey(int $question_id): bool
    {
        $status = false;
        if ($question_id <= 0) {
            $this->logger->critical('Invalid question id', [__METHOD__, __LINE__]);
            return $status;
        }
        $sql = 'DELETE FROM questions WHERE id=:question_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':question_id', $question_id);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    public function parseImageDetailsForSurvey(array $normalized_images, string $question_type): array
    {
    }


    /**
     * @param int $camp_id
     * @return bool
     */
    public function deleteCampaign(int $camp_id): bool
    {
        $status = false;
        if ($camp_id <= 0) {
            return $status;
        }
        try {
            $sql = 'DELETE FROM campaign where id = :camp_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':camp_id', $camp_id);
            $res = $stmt->execute();
            $status = (is_bool($res) && $res) ? $res : $status;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    /**
     * @param int $old_campaign_id
     * @param int $new_campaign_id
     * @return bool
     * @author Pradeep
     */
    public function copyCampaignParams(int $old_campaign_id, int $new_campaign_id): bool // TODO: make nicer: $sql_insert
    {
        $status = false;
        if ($old_campaign_id <= 0 || $new_campaign_id <= 0) {
            return $status;
        }
        $sql_1 = 'SELECT * FROM campaign_params where campaign_Id=:old_campaign_id';
        $stmt = $this->conn->prepare($sql_1);
        $stmt->bindValue(':old_campaign_id', $old_campaign_id);
        $status = $stmt->execute();
        $old_campaign_params = $stmt->fetchAll();
        // Generate multi insert statement for campaign params.
        if (!empty($old_campaign_params) && $status) {
            $count_params = count($old_campaign_params);
            $sql_insert = 'INSERT INTO campaign_params(campaign_Id, configuration_target_id, value) VALUES';
            foreach ($old_campaign_params as $param) {
                $sql_insert .= "(" . $new_campaign_id . ", " . $param['configuration_target_id'] . ",'" . $param['value'] . "')";
                $sql_insert .= ($count_params !== 1) ? ',' : '';
                $count_params--;
            }
            $stmt = $this->conn->prepare($sql_insert);
            $status = $stmt->execute();
        }

        return $status;
    }


    /**
     * @param int $old_campaign_id
     * @param int $new_campaign_id
     * @return bool
     * @author Pradeep
     */
    public function copySurvey(int $old_campaign_id, int $new_campaign_id): bool
    {
        $status = false;
        if ($old_campaign_id <= 0 || $new_campaign_id <= 0) {
            return $status;
        }
        // get campaign details for survey.
        $old_campaign = $this->getCampaignDetailsById($old_campaign_id);
        $sql_old_survey = 'SELECT * FROM survey WHERE id=:old_survey_id';
        try {
            $stmt = $this->conn->prepare($sql_old_survey);
            $stmt->bindValue(':old_survey_id', $old_campaign['survey_id']);
            $status = $stmt->execute();
            // survey is not configured for the campaign.
            $old_survey_details = $stmt->fetchAll();
            if (empty($old_survey_details[0]) && $status) {
                return $status;
            }
            $new_survey_version = $old_survey_details[0];
            $old_survey_id = $new_survey_version['id'];
            $new_survey_version['id'] = '';// remove the id field.
            $new_survey_id = $this->storeSurveyDescription($new_survey_version);
            if ($new_campaign_id > 0 && $this->updateSurveyIdToCampaigns($new_campaign_id, $new_survey_id) && $this->copySurveyQuestions($old_survey_id,
                    $new_survey_id)) {
                $status = true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $id
     * @return array : Array
     * @author aki
     * @deprecated
     */
    public function getCampaignDetailsById(string $id): array
    {
        $camp_details = array();

        if (!is_numeric($id) || (int)$id <= 0) {
            return $camp_details;
        }

        try {
            $sql = 'select * from campaign where id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $id);
            $stmt->execute();
            $res = $stmt->fetchAll();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return [];
        }
        $camp_details = $res[0];
        return $camp_details;
    }


    /**
     * @param array $survey
     * @return int
     * @author pva
     */
    public function storeSurveyDescription(array $survey): int
    {
        $lst_insert_id = 0;

        if (empty($survey)) {
            $this->logger->critical('Invalid Survey Description', [__METHOD__, __LINE__]);
            return $lst_insert_id;
        }

        $id = (!empty($survey['id']) && $survey['id'] > 0) ? $survey['id'] : 0;
        $sql = 'INSERT INTO survey(id, layout_status_id, survey_is_online, survey_description) 
                VALUES(:id, :layout_status_id, :survey_is_online, :survey_description)
                ON DUPLICATE KEY UPDATE 
                layout_status_id = :layout_status_id,
                survey_is_online = :survey_is_online,
                survey_description = :survey_description';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':id', $id);
            $stmt->bindValue(':layout_status_id', $survey['layout_status_id']);
            $stmt->bindValue(':survey_is_online', $survey['survey_is_online']);
            $stmt->bindValue(':survey_description', $survey['survey_description']);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $lst_insert_id = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $lst_insert_id;
    }


    /**
     * @param int $camp_id
     * @param int $survey_id
     * @return bool
     * @author pva
     */
    public function updateSurveyIdToCampaigns(int $camp_id, int $survey_id): bool
    {
        $status = false;
        if ($camp_id <= 0 || $survey_id <= 0) {
            $this->logger->critical('Invalid SurveyId/CampaignId', [__METHOD__, __LINE__]);
            return $status;
        }

        $sql = 'UPDATE campaign 
                SET survey_id = :survey_id
                WHERE id = :camp_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':camp_id', $camp_id);
            $stmt->bindValue(':survey_id', $survey_id);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    /**
     * @param int $old_survey_id
     * @param int $new_survey_id
     * @return bool
     * @author Pradeep
     */
    public function copySurveyQuestions(int $old_survey_id, int $new_survey_id): bool
    {
        $status = false;
        if ($new_survey_id <= 0 || $old_survey_id <= 0) {
            return $status;
        }
        $sql_old_questions = 'SELECT * FROM questions WHERE survey_id =:old_survey_id';
        try {
            $stmt = $this->conn->prepare($sql_old_questions);
            $stmt->bindValue(':old_survey_id', $old_survey_id);
            $old_survey_status = $stmt->execute();
            $result = $stmt->fetchAll();

            if (empty($result) && empty($result[0])) {
                return $old_survey_status;
            }

            foreach ($result as $new_version_questions) {
                $sql_insert = 'INSERT INTO questions (
                        survey_id, 
                        context_id, 
                        question_types_id, 
                        question_question_txt, 
                        question_hint_txt, 
                        question_json_representation
                    ) 
                    VALUES (
                        :survey_id, 
                        :context_id, 
                        :question_types_id,
                        :question_question_txt, 
                        :question_hint_txt, 
                        :question_json_representation 
                    )';
                $stmt = $this->conn->prepare($sql_insert);
                $stmt->bindValue(':survey_id', $new_survey_id);
                $stmt->bindValue(':context_id', $new_version_questions['context_id']);
                $stmt->bindValue(':question_types_id', $new_version_questions['question_types_id']);
                $stmt->bindValue(':question_question_txt', $new_version_questions['question_question_txt']);
                $stmt->bindValue(':question_hint_txt', $new_version_questions['question_hint_txt']);
                $stmt->bindValue(':question_json_representation', $new_version_questions['question_json_representation']);
                $result = $stmt->execute();
                if (is_bool($result) && $result) {
                    $status = $result;
                    // var_dump($new_version_questions[ 'question_types_id' ]);
                    // Images are present for only question id 1,4 (select, checkbox)
                    if ((int)$new_version_questions['question_types_id'] === 1 || (int)$new_version_questions['question_types_id'] === 4) {
                        $new_question_id = $this->conn->lastInsertId();
                        $status = $this->copySurveyImages($new_version_questions['id'], $new_question_id);
                    }
                } else {
                    return $result;
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }


        return $status;
    }


    /**
     * @param $old_question_id
     * @param $new_question_id
     * @return bool
     * @author Pradeep
     */
    private function copySurveyImages(int $old_question_id, int $new_question_id): bool
    {
        $status = false;
        if ($old_question_id <= 0 || $new_question_id <= 0) {
            return $status;
        }
        // Get Images from old question_id
        $sql_fetch_images = 'SELECT * FROM images WHERE question_id=:question_id';
        $stmt = $this->conn->prepare($sql_fetch_images);
        $stmt->bindValue(':question_id', $old_question_id);
        $status = $stmt->execute();
        $image_rows = $stmt->fetchAll();
        if (empty($image_rows)) {
            return $status;
        }
        // Insert Images to new Question id for each options.
        foreach ($image_rows as $image_row) {
            $sql_insert_images = 'INSERT into images(
                    question_id, 
                    option_id, 
                    imgtype, 
                    img
                )
                VALUES(
                    :question_id, 
                    :option_id, 
                    :imgtype, 
                    :img
                )';
            $stmt = $this->conn->prepare($sql_insert_images);
            $stmt->bindValue(':question_id', $new_question_id);
            $stmt->bindValue(':option_id', $image_row['option_id']);
            $stmt->bindValue(':imgtype', $image_row['imgtype']);
            $stmt->bindValue(':img', $image_row['img']);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $status = $res;
            }
        }

        return $status;
    }


    /**
     * @param int $old_campaign_id
     * @param int $new_campaign_id
     * @return bool
     * @author Pradeep
     */
    public function copyIntegrationParams(int $old_campaign_id, int $new_campaign_id): bool // TODO: make nicer: $sql_insert
    {
        $status = false;
        if ($old_campaign_id <= 0 || $new_campaign_id <= 0) {
            return $status;
        }
        $sql_1 = 'SELECT * FROM campaign_params where campaign_Id=:old_campaign_id';
        $stmt = $this->conn->prepare($sql_1);
        $stmt->bindValue(':old_campaign_id', $old_campaign_id);
        $status = $stmt->execute();
        $old_campaign_params = $stmt->fetchAll();
        // Generate multi insert statement for campaign params.
        if (!empty($old_campaign_params) && $status) {
            $count_params = count($old_campaign_params);
            $sql_insert = 'INSERT INTO campaign_params(campaign_Id, configuration_target_id, value) VALUES';
            foreach ($old_campaign_params as $param) {
                $sql_insert .= "(" . $new_campaign_id . ", " . $param['configuration_target_id'] . ",'" . $param['value'] . "')";
                $sql_insert .= ($count_params !== 1) ? ',' : '';
                $count_params--;
            }
            $stmt = $this->conn->prepare($sql_insert);
            $status = $stmt->execute();
        }
    }


    /**
     * @param int $campaign_id
     * @return array
     * @author Pradeep
     */
    public function fetchCampaignParamsForCopyCampaign(int $campaign_id): array
    {
        $campaign_params = [];
        if ($campaign_id <= 0) {
            return $campaign_params;
        }

        $sql = 'SELECT 
                c.name,
                c.comment,
                cp.id,
                cp.campaign_Id,
                cp.value,
                ct.name as cnf_target
                FROM campaign_params cp 
                    join configuration_targets ct on cp.configuration_target_id = ct.id
                    join campaign c on cp.campaign_Id = c.id
                WHERE campaign_Id=:campaign_id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':campaign_id', $campaign_id);
        $stmt->execute();
        $params = $stmt->fetchAll();
        foreach ($params as $param) {
            if (empty($param['value']) || empty($param['cnf_target'])) {
                continue;
            }
            $param_value = json_decode($param['value'], true, 512, JSON_THROW_ON_ERROR);
            foreach ($param_value as $key => $value) {
                if (empty($key)) {
                    continue;
                }
                // Since all mapping tables are base64 encoded
                if ($key === 'mappingTable') {
                    $decoded_base64 = base64_decode($value);
                    $decode_json = json_decode($decoded_base64, true, 512, JSON_THROW_ON_ERROR);
                    $campaign_params[strtolower($param['cnf_target'])] = $decode_json;
                } else {
                    $campaign_params[strtolower($param['cnf_target'])][$key] = $value;
                }
            }
            $campaign_params['name'] = $param['name'];
            $campaign_params['description'] = $param['comment'];
            $campaign_params['standard_fields'] = $this->getStandardFields();
        }
        return $campaign_params;
    }


    /**
     * @return array
     * @author Pradeep
     */
    public function getStandardFields(): array
    {
        $standard_fields = [];
        try {
            $sql = 'SHOW COLUMNS FROM leads';
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $lead_columns = $stmt->fetchAll();
            foreach ($lead_columns as $column) {
                if ($column['Field'] === 'id' || $column['Field'] === 'created' || $column['Field'] === 'created_by' || $column['Field'] === 'updated' || $column['Field'] === 'updated_by' || $column['Field'] === 'modification') {
                    continue;
                }
                $standard_fields[$column['Field']] = [
                    'label' => $column['Field'],
                    'type' => $column['Type'],
                ];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $standard_fields;
    }


    /**
     * @param int $campaign_id
     * @return array
     * @author Pradeep
     */
    public function getCustomFieldsForCampaign(int $campaign_id): array
    {
        $custom_fields = [];
        if ($campaign_id <= 0) {
            return $custom_fields;
        }
        $sql = 'SELECT fieldname,  datatype FROM customfields WHERE campaign_id =:campaign_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetchAll();
            if (!empty($result)) {
                $custom_fields = $result;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $custom_fields;
    }


    /**
     * @param int $survey_id
     * @return array
     * @author Pradeep
     */
    public function fetchSurveyQuestions(int $survey_id): array
    {
        $survey_questions = [];
        if ($survey_id <= 0) {
            return $survey_questions;
        }
        $sql = 'select question_question_txt from questions where survey_id=:survey_id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':survey_id', $survey_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if (empty($result)) {
            return $survey_questions;
        }
        foreach ($result as $res) {
            if (empty($res['question_question_txt'])) {
                continue;
            }
            // Trim the question for more than 255 characters.
            $question = strlen($res['question_question_txt']) >= 255 ? substr($res['question_question_txt'], 0, 255) : $res['question_question_txt'];

            $survey_questions[] = $question;
        }

        return $survey_questions;
    }


    /**
     * @param int $campaign_id
     * @return array
     * @author Aki
     * @deprecated
     */
    public function fetchCampaignSettings(int $campaign_id): array
    {
        $campaign_params_parsed = [];
        if ($campaign_id <= 0) {
            return $campaign_params_parsed;
        }

        $sql = 'select value
                from campaign_params where campaign_Id = :campaign_id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':campaign_id', $campaign_id);
        $stmt->execute();
        $campaign_params = $stmt->fetch();

        if(!empty($campaign_params)){
            $campaign_params_parsed = json_decode($campaign_params['value'],true);
        }
        return $campaign_params_parsed;
    }


    /**
     * @param string $UniqueObjectValue
     * @param int    $uniqueObjectTypeId
     * @param int    $userId
     * @return int
     * @author Aki
     */
    public function createUniqueObjectInDB(
        string $UniqueObjectValue,
        int $uniqueObjectTypeId,
        int $userId
    ): int {
        $unique_object_table_id = 0;
        //$time_stamp = date('Y-m-d h:i:s');
        try {
            $sql = 'INSERT INTO objectregister 
                    (object_unique_id, unique_object_type_id, created_by, updated_by) 
                     VALUES (?, ?, ?, ?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $UniqueObjectValue);
            $stmt->bindValue(2, $uniqueObjectTypeId);
            $stmt->bindValue(3, $userId);
            $stmt->bindValue(4, $userId);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                // Get the Inserted Campaign Id
                $unique_object_table_id = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $unique_object_table_id;
    }


    /**
     * @param int    $question_id
     * @param int    $option_id
     * @param string $normalized_image
     * @param string $img_type
     * @return bool
     * @author Pradeep
     */
    private function updateImageForSurveyQuestion(
        int $question_id,
        int $option_id,
        string $normalized_image,
        string $img_type = ''
    ): bool {
        $status = false;
        if ($question_id <= 0 || empty($normalized_image)) {
            return $status;
        }
        $sql = 'UPDATE images SET img =:img WHERE question_id =:question_id and option_id =:option_id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':option_id', $option_id);
        $stmt->bindValue(':question_id', $question_id);
        $res = $stmt->execute();
        if (is_bool($res) && $res) {
            $status = $res;
        }
        return $status;
    }


    /**
     * @param array $campaign_parameters
     * @return array
     * @author Aki
     * @deprecated
     */
    private function addAdditionalDefaultSettings(array $campaign_parameters): array
    {
        if (empty($campaign_parameters)) {
            return $campaign_parameters;
        }
        $campaign_parameters['general']['keep_url_parms'] = 'on';
        $campaign_parameters['general']['track_cus_jour'] = 'on';
        $campaign_parameters['pageflow'] = [
            'success_page' => 'success.html',
            'error_page' => 'error.html',
            'redirect_page' => 'treaction.net',
            'pages' => array(
                'custom' => array(
                    0 => array(
                        'name' => 'index.html',
                    ),
                ),
            ),
        ];

        return $campaign_parameters;
    }


    /**
     * @param string $objectRegisterId
     * @return int
     * @author Aki
     */
    public function getUniqueObjectForCampaign(
        string $objectRegisterId
    ): string {
        $object_unique_id = '0';
        try {
            $sql = 'SELECT o.*
                            FROM objectregister o
                            WHERE o.id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $objectRegisterId);
            $stmt->execute();
            $object_register = $stmt->fetchAll();
            $object_unique_id = $object_register[0]['object_unique_id'];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $object_unique_id;
    }


    /**
     *
     * @param int $campaignID
     * @return string
     * @author Aki
     */
    public function getUniqueObjectIdWithcampaignID(
        int $campaignID
    ): string {
        $object_register_id = '0';
        try {
            $sql = 'SELECT objectregister_id
                            FROM campaign
                            WHERE id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $campaignID);
            $stmt->execute();
            $queryReturn = $stmt->fetchAll();
            $object_register_id = $queryReturn[0]['objectregister_id'];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $object_register_id;
    }


    /**
     *
     * @param int $AccountID
     * @return string
     * @author Aki
     */
    public function getAccountNumberWithAccountID(
        int $AccountID
    ): string {
        $AccountNumber = '0';
        try {
            $sql = 'SELECT account_no
                            FROM company
                            WHERE id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $AccountID);
            $stmt->execute();
            $queryReturn = $stmt->fetchAll();
            $AccountNumber = $queryReturn[0]['account_no'];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $AccountNumber;
    }


    /**
     *
     * @param int $AccountID
     * @return string
     * @author Aki
     */
    public function getAPIkeyForCampaign(
        int $AccountID
    ): string {
        $ApiKey = '';

        try {
            $sql = 'SELECT account_no
                            FROM company
                            WHERE id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $AccountID);
            $stmt->execute();
            $queryReturn = $stmt->fetchAll();
            $AccountNumber = $queryReturn[0]['account_no'];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $ApiKey;
    }


    /**
     *
     * @param int $CampaignId
     * @return string
     * @author Aki
     */
    public function getCampaignNameWithId(
        int $CampaignId
    ): string {
        $name = '';

        try {
            $sql = 'SELECT name
                            FROM campaign
                            WHERE id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $CampaignId);
            $stmt->execute();
            $queryReturn = $stmt->fetchAll();
            $name = $queryReturn[0]['name'];
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $name;
    }


    /**
     *
     * @param int   $CampaignId
     * @param array $CampaignParams
     * @return bool
     * @author  Aki
     * @comment This functions updates custom fields table only if fields other than leads tables are present in mapping!!!
     */
    public function storeMappingParams(
        int $CampaignId,
        array $CampaignParams
    ): bool {
        $status = false;
        if (empty($CampaignParams)) {
            return $status;
        }
        $checkSqlInsert = [];
        try {
            $mapping = json_decode(base64_decode($CampaignParams['mapping']['mappingTable']), true, 512, JSON_THROW_ON_ERROR);
            $userId = $CampaignParams['user_id'];
            $campaign_details = $this->getCampaignDetailsById($CampaignId);
            $additionalFields = [
                'Order Number',
                'Order Date',
                'Smart Tags',
                'Net Order Value',
                'URL_Parameter',
            ];
            foreach ($mapping as $key => $value) {
                if ($value['field type'] === 'custom') {
                    $fieldTypeId = 4;  //todo: dynamically get the id of the fieldtype
                    $checkSqlInsert[$key] = $this->addCustomFields($CampaignId, $campaign_details['objectregister_id'], $fieldTypeId, $value, $userId);
                } else {
                    if (in_array($value['marketing-in-one'], $additionalFields, true)) {
                        $fieldTypeId = 5;  //todo: dynamically get the id of the fieldtype
                        $checkSqlInsert[$key] = $this->addCustomFields($CampaignId, $campaign_details['objectregister_id'], $fieldTypeId, $value, $userId);
                    }
                }
            }
            foreach ($checkSqlInsert as $key => $value) {
                if (!$value) {
                    return $status;
                }
            }
        } catch (\JsonException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
        return true;
    }

    /**
     * @param int $campId
     * @return int
     * @author aki
     */
    public function getobjectregisterIdWithCampaignId(int $campId):?int
    {
        $objectRegisterId = 0;

        if(!empty($campId)){
            $selectSql = 'select c.objectregister_id
            from campaign c where id = :campId';
            $stmt = $this->conn->prepare($selectSql);
            $stmt->bindValue(':campId', $campId, ParameterType::INTEGER);
            try {
                $stmt->execute();
                $objectRegisterId = $stmt->fetchColumn();


                if ($stmt->rowCount() == 0) {
                    $this->logger->warning('no rows found in related campaign_params for campaign id=' . $campId, [__METHOD__, __LINE__]);
                    return null;
                }
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                return null;
            }

        }
        return $objectRegisterId;
    }

    /**
     * @param string $name
     * @return int|null
     * @author aki
     */
    public function getStatusdefIdByName(string $name): ?int
    {
        $name = strtolower($name);
        $selectStatusDef = "select id from statusdef where value = :name";
        $stmt = $this->conn->prepare($selectStatusDef);
        $stmt->bindParam(':name', $name, ParameterType::STRING);

        try {
            $stmt->execute();
            $record = $stmt->fetch();
            $rowCount = $stmt->rowCount();
            if ($rowCount == 0) {
                return null;
            }
        } catch (Exception $e) {
            return null;
        }

        return $record['id'];

    }

    /**
     * @param int $campaign_id
     * @return string|null
     * @author aki
     */
    public function getVersionFromCampaignId(int $campaign_id):?string
    {
        $selectSql = "select version
                        from objectregister
                        join campaign c on objectregister.id = c.objectregister_id and c.id = :campaignId";

        $stmt = $this->conn->prepare($selectSql);
        $stmt->bindValue(':campaignId', $campaign_id, ParameterType::INTEGER);
        try {
            $stmt->execute();
            $record = $stmt->fetch();
        } catch (Exception $e) {
            return null;
        }

        return $record['version'];


    }

    /**
     * @param $campaignId
     * @param $camapignObjRegId
     * @param $fieldTypeId
     * @param $value
     * @param $userId
     * @return bool
     */
    private function addCustomFields($campaignId, $camapignObjRegId, $fieldTypeId, $value, $userId): bool
    {
        $status = false;
        $customFieldsId = 0;
        $required = $value['required'];
        $requiredValue = 0;
        if ($required === 'true') {//boolean to tiny int conversion
            $requiredValue = 1;
        }
        try {
            // TODO: change object_register_id to objectregister_id for MIO DB
            $sql = 'INSERT INTO customfields 
                    (campaign_id, object_register_id , fieldname,datatype, mandatory,created_by, updated_by) 
                     VALUES (?, ?,?, ?, ?,?,?,?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $campaignId);
            $stmt->bindValue(2, $camapignObjRegId);
            $stmt->bindValue(3, $fieldTypeId);
            $stmt->bindValue(4, $value['marketing-in-one']);
            $stmt->bindValue(5, $value['data type']);
            $stmt->bindValue(6, $requiredValue);
            $stmt->bindValue(7, $userId);
            $stmt->bindValue(8, $userId);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                // Get the Inserted Custom Field Id
                $customFieldsId = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        if ($customFieldsId > 0) {
            $status = true;
        }
        return $status;
    }


    /**
     * @param int $company_id
     * @return array empty or ['objectregister.object_unique_id' => string, 'objectregister.id' => int, campaign.id as campaign_id => int]
     * @author jsr
     */
    public function getProjectObjectRegisterUOIDbyCompanyId(int $company_id): ?array
    {
        $return = [];
        $this->logger->info('getProjectObjectRegisterUOIDbyCompanyId $company_id', [$company_id, __METHOD__, __LINE__]);

        $selectObjRegId = "SELECT o.object_unique_id, o.id, c.id campaign_id 
                FROM objectregister o 
                JOIN unique_object_type uot on o.unique_object_type_id = uot.id AND uot.unique_object_name = :project
                JOIN campaign c on o.id = c.objectregister_id AND c.company_id = :company_id";
        $stmt = $this->conn->prepare($selectObjRegId);
        $stmt->bindValue(':company_id', $company_id, ParameterType::INTEGER);
        $project = UniqueObjectTypes::PROJECT;
        $stmt->bindValue(':project', $project, ParameterType::STRING);

        try {
            $stmt->execute();
            $res = $stmt->fetch(FetchMode::ASSOCIATIVE);


            $this->logger->info('RESULT $res', [$res, __METHOD__, __LINE__]);
            $this->logger->info('RESULT $res', [$res['object_unique_id'], __METHOD__, __LINE__]);
            $this->logger->info('RESULT $res', [$res['id'], __METHOD__, __LINE__]);

            if ($stmt->rowCount() > 0) {
                $return = ['object_unique_id' => $res['object_unique_id'], 'id' => $res['id'], 'campaign_id' => $res['campaign_id']];
            } else {
                $this->logger->warning('no project found for company_id ' . $company_id, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), ['company_id' => $company_id, __METHOD__, __LINE__]);
            return null;
        }

        return $return;
    }


	/**
	 * @param int $company_id
	 * @return int|null
	 */
    public function getSelectedProjectCampaignId(int $company_id):?int
	{
		// TODO: once there are more than one projects possible get the currently selected project (the one the user is working on) from the cache
		$selectProjectCampaignId = "SELECT cp.id 
			FROM company c
			JOIN campaign cp on c.id = cp.company_id
			JOIN objectregister o ON cp.objectregister_id = o.id
			JOIN unique_object_type uot on o.unique_object_type_id = uot.id AND uot.unique_object_name = :project
			WHERE c.id = :company_id";

		try {
			$stmt = $this->conn->prepare($selectProjectCampaignId);
			$stmt->bindValue(':company_id', $company_id, ParameterType::INTEGER);
			$project = UniqueObjectTypes::PROJECT;
			$stmt->bindValue(':project', $project, ParameterType::STRING);

			$stmt->execute();

			$projectCampaignId = $stmt->fetchColumn();

			return ($stmt->rowCount()>0 &&  !empty($projectCampaignId))
				? $projectCampaignId
				: null;
		} catch (\Exception $e) {
			$this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
		}

		return null;
	}


    /**
     * creates the project entry in campaign table - used after ObjectRegisterRepository::createInitialObjectRegisterEntry which is used account creation
     * @param int $company_id
     * @param int $objectregister_id
     * @return int|null campaign.id | null
     */
    public function createProjectEntry(int $company_id, int $objectregister_id): ?int
    {
        $insertedId = null;
        $databaseIsMioInterim = (bool)$_ENV['USE_MIOINTERIM'];

        $company = $this->companyRepository->getCompanyDetailById($company_id);
        $comment = 'project for account ' . $company['name'] . ' / ' . $company['account_no'];
        $name = $company['name'];
        $currentDate = date('Y-m-d H:i:s');

        if ($databaseIsMioInterim) {
            $insertIntoCampaign = "INSERT INTO campaign 
                (company_id, objectregister_id, name, usecase_type, archivedate, activationdate, account_no, url, comment, activationby, archiveby, version, status,  created_by, updated_by) 
                VALUES 
                (:company_id, :objectregister_id, :projectname, 'project', :currentDate, :currentDate, :account_no, 'n.a.', :projectcomment, :activationby, 0, '999999', 'active',  :created_by, :updated_by)";
        } else {
            $insertIntoCampaign = "INSERT INTO campaign 
                (company_id, objectregister_id, name, url, account_no, comment, created_by, updated_by) 
                VALUES 
                (:company_id, :objectregister_id, :projectname, 'https://default.value', :account_no, :projectcomment, :created_by, :updated_by)";
        }

        $stmt = $this->conn->prepare($insertIntoCampaign);

        if ($databaseIsMioInterim) {
            $stmt->bindParam(':company_id', $company_id, ParameterType::INTEGER);
            $stmt->bindParam(':objectregister_id', $objectregister_id, ParameterType::INTEGER);
            $stmt->bindParam(':projectname', $name, ParameterType::STRING);
            $stmt->bindParam(':currentDate', $currentDate, ParameterType::STRING);
            $stmt->bindParam(':account_no', $company['account_no'], ParameterType::STRING);
            $stmt->bindParam(':projectcomment', $comment, ParameterType::STRING);
            $stmt->bindParam(':activationby', $this->userId, ParameterType::INTEGER);
            $stmt->bindParam(':created_by', $this->userId, ParameterType::INTEGER);
            $stmt->bindParam(':updated_by', $this->userId, ParameterType::INTEGER);
        } else {
            $stmt->bindParam(':company_id', $company_id, ParameterType::INTEGER);
            $stmt->bindParam(':objectregister_id', $objectregister_id, ParameterType::INTEGER);
            $stmt->bindParam(':projectname', $name, ParameterType::STRING);
            $stmt->bindParam(':account_no', $company['account_no'], ParameterType::STRING);
            $stmt->bindParam(':projectcomment', $comment, ParameterType::STRING);
            $stmt->bindParam(':created_by', $this->userId, ParameterType::INTEGER);
            $stmt->bindParam(':updated_by', $this->userId, ParameterType::INTEGER);
        }

        try {
            $stmt->execute();

            if ($stmt->rowCount() !== 1) {
                $this->logger->error('could not insert Project entry in campaign for company_id=' . $company_id . ' objectregister_id=' . $objectregister_id,
                    [__METHOD__, __LINE__]);
                return null;
            }
            $insertedId = $this->conn->lastInsertId();
        } catch (\Exception $e) {
            $this->logger->error('could not insert CustomFieldSet Entry in objectregister for company_id=' . $company_id. ' objectregister_id=' . $objectregister_id, [__METHOD__, __LINE__]);
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $insertedId;
    }


    /**
     * the one and only insert into campaign_params for a project => initial setup during account (company) dpa confirmation
     * @param int    $company_id
     * @param int    $campaign_id
     * @param string $client_ipv4_address
     * @return int|null  lastInsertId => (int) campaign_params.id
     * @see    App\Types\ProjectWizardTypes
     * @author jsr
     * @deprecated
     */
    public function insertProjectDefaultCampaignParams(int $company_id, int $campaign_id, string $client_ipv4_address = null): ?int
    {
        $company = $this->companyRepository->getCompanyDetailById($company_id);
        $projectObjectRegister = $this->getProjectObjectRegisterUOIDbyCompanyId($company_id);
        /*
        $isCorrectIp = ($client_ipv4_address === filter_var($client_ipv4_address, FILTER_VALIDATE_IP));
        $countryByIp = ($isCorrectIp) ? $this->campaignService->getCountryByIp($client_ipv4_address) : 'Germany';
        $country = (empty($countryByIp)) ? 'Germany' : $countryByIp;
        */
        $country = 'Germany';
        $defaultProject = $this->objectRegisterRepository->getProjectTypeDefaultSettingsAsArray();//takes data from Object layer
        $defaultProject['advancedsettings']['country'] = $country;
        $defaultProject['relation']['company_name'] = $company['name'];
        $defaultProject['relation']['company_id'] = $company['id'];
        $defaultProject['relation']['project_object_unique_id'] = $projectObjectRegister['object_unique_id'];
        $defaultProject['relation']['project_objectregister_id'] = $projectObjectRegister['id'];
        $cp_value = json_encode($defaultProject);


        $configuration_target_id = $this->configurationTargetRepository->getDefaultProjectId();

        $insertIntoCampaignParams = "INSERT INTO campaign_params 
                (campaign_Id, configuration_target_id, value, created_by, updated_by) 
                VALUES 
                (:campaign_Id, :configuration_target_id, :cp_value, :created_by, :updated_by)";

        $stmt = $this->conn->prepare($insertIntoCampaignParams);

        $stmt->bindParam(':campaign_Id', $campaign_id, ParameterType::INTEGER);
        $stmt->bindParam(':configuration_target_id', $configuration_target_id, ParameterType::INTEGER);
        $stmt->bindParam(':cp_value', $cp_value, ParameterType::STRING);
        $stmt->bindParam(':created_by', $this->userId, ParameterType::INTEGER);
        $stmt->bindParam(':updated_by', $this->userId, ParameterType::INTEGER);

        try {
            $stmt->execute();

            $lastInsertId = $this->conn->lastInsertId();
            if ($stmt->rowCount() == 0) {
                $this->logger->warning('could not insert record into campaign_params for campaign_id ' . $campaign_id, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $lastInsertId;
    }



    /**
     * @param int $objectregister_id
     * @return array|null
     * @see    App\Types\ProjectWizardTypes
     * @author jsr
     */
    public function getSavedProjectCampaignParams(int $objectregister_id): ?array
    {
        if(null === $objectregister_id) {
            return null;
        }

        $selectCampaignParams = "SELECT cp.value FROM campaign c
            JOIN campaign_params cp on c.id = cp.campaign_Id 
            WHERE c.objectregister_id = :objectregister_id";

        $stmt = $this->conn->prepare($selectCampaignParams);
        $stmt->bindValue(':objectregister_id', $objectregister_id, ParameterType::INTEGER);

        try {
            $stmt->execute();
            $value = $stmt->fetchColumn();


            if ($stmt->rowCount() == 0) {
                $this->logger->warning('no rows found in related campaign_params for campaign.objectregister_id=' . $objectregister_id, [__METHOD__, __LINE__]);
                return null;
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return json_decode($value, true, 50, JSON_OBJECT_AS_ARRAY);
    }


    /**
     * @param int    $objectregister_id
     * @param string $value json representation of project's campaign_params.value see ProjectWizardTypes
     * @return void|null
     * @see    App\Types\ProjectWizardTypes
     * @author jsr
     */
    public function updateProjectCampaignParams(int $objectregister_id, string $value): bool
    {
        // TODO: could be more tight by extending sub-select on tables objectregister and unique_object_type
// [2020-11-11 15:11:51] app.ERROR: An exception occurred while executing
// 'UPDATE campaign_params cp              SET cp.value = :value
//              WHERE cp.campaign_Id = (SELECT c.id FROM campaign c WHERE c.objectregister_id = :51)'
//  with params
// [51,
// "{\"relation\":{\"company_id\":\"20\",\"company_name\":\"solar\",\"project_object_unique_id\":\"4c566dc6-179c-11eb-890f-0800276141bb\",\"project_objectregister_id\":\"51\"},\"settings\":{\"description\":\"project definitions for solar\",\"url_option\":\"custom-domain\",\"beginDate\":\"20\\\/11\\\/19 04:08\",\"expireDate\":\"20\\\/11\\\/30 04:08\",\"commercial_relationships\":\"b2c\"},\"advancedsettings\":{\"register_multiple_times\":false,\"fraud_prevention_level\":\"low\",\"forward_lead_by_email\":\"jo@web.de\",\"bool_redirect_after_registration\":false,\"redirect_after_registration\":\"https:\\\/\\\/gotosite.de\"},\"customfieldlist\":[{\"instruction\":\"update\",\"id\":0,\"objectregister_id\":54,\"campaign_id\":1649,\"fieldname\":\"Auto\",\"regexpvalidation\":\"\",\"datatype\":\"8\",\"mandatory\":true,\"hideinuiselection\":false},{\"instruction\":\"update\",\"id\":1,\"objectregister_id\":54,\"campaign_id\":1649,\"fieldname\":\"Zulassungsdatum\",\"regexpvalidation\":\"\",\"datatype\":\"1\",\"mandatory\":true,\"hideinuiselection\":false},{\"instruction\":\"update\",\"id\":2,\"objectregister_id\":54,\"campaign_id\":1649,\"fieldname\":\"Anzahl T\\u00fcren\",\"regexpvalidation\":\"\",\"datatype\":\"5\",\"mandatory\":true,\"hideinuiselection\":false},{\"instruction\":\"update\",\"id\":3,\"objectregister_id\":54,\"campaign_id\":1649,\"fieldname\":\"Hersteller Website\",\"regexpvalidation\":\"\",\"datatype\":\"9\",\"mandatory\":true,\"hideinuiselection\":false},{\"instruction\":\"insert\",\"objectregister_id\":54,\"campaign_id\":1649,\"fieldname\":\"AAAA\",\"regexpvalidation\":\"\",\"datatype\":\"6\",\"mandatory\":false,\"hideinuiselection\":false},{\"instruction\":\"insert\",\"objectregister_id\":54,\"campaign_id\":1649,\"fieldname\":\"BBBB\",\"regexpvalidation\":\"\",\"datatype\":\"3\",\"mandatory\":false,\"hideinuiselection\":false}],\"customfielddelete\":[{\"instruction\":\"delete\",\"customfield_id\":7,\"campaign_id\":1649,\"objectregister_id\":54}],\"tracker\":{\"google_api_key\":\"GTRACK\",\"facebook_pixel\":\"FTRACK\",\"webgains_id\":\"WTRACK\",\"outbrain_id\":\"OTRACK\",\"bool_outbrain_track_after_30_seconds\":true}}"]:
//  SQLSTATE[HY093]: Invalid parameter number: parameter was not defined ["App\\Repository\\CampaignRepository::updateProjectCampaignParams",2307] [
        $updateCampaignParams = "UPDATE campaign_params cp 
            SET cp.value = :value 
            WHERE cp.campaign_Id = (SELECT c.id FROM campaign c WHERE c.objectregister_id = :objectregister_id)";

        $stmt = $this->conn->prepare($updateCampaignParams);
        $stmt->bindValue(':objectregister_id', $objectregister_id, ParameterType::INTEGER);
        $stmt->bindValue(':value', $value, ParameterType::STRING);

        try {
            $stmt->execute();

            if ($stmt->rowCount() == 0) {
                $this->logger->warning('update for campaign_params related to campaign.objectregister_id=' . $objectregister_id . ' failed.',
                    [__METHOD__, __LINE__]);
                return false;
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }

        return true;
    }

    /**
     * @param int $companyId
     * @return array|null [id, company_id, objectregister_id, name, account_no, url, comment, created, created_by, updated, updated_by]
     * @author Pradeep
     */
    public function getProjectDetails(int $companyId):?array
    {
        $project = [];
        if($companyId <= 0) {
            return [];
        }
        $sql = 'select c.* from campaign c
                    Join objectregister o on o.id = c.objectregister_id
                    join unique_object_type uot on uot.id = o.unique_object_type_id
                where uot.unique_object_name LIKE "project" AND c.company_id = :company_id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':company_id', $companyId);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if (!empty($result)) {
            $project = $result[ 0 ];
        }
        return $project;
    }

    /**
     * @param int $campaignObjRegId
     * @return array|null
     * @author Pradeep
     */
    public function getCampaignDetailsBasedOnObjectRegId(int $campaignObjRegId): ?array
    {
        $campaign = [];
        if ($campaignObjRegId <= 0) {
            return null;
        }
        $sql = 'SELECT * FROM campaign WHERE objectregister_id=:objectregister_id';
        $stmt = $this->conn->prepare($sql);
        $stmt->bindParam(':objectregister_id', $campaignObjRegId);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if ($stmt->rowCount() > 0 && !empty($result[ 0 ])) {
            $campaign = $result[ 0 ];
        }
        return $campaign;
    }
}


