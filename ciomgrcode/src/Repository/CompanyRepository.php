<?php


namespace App\Repository;


use App\Entity\Company;
use DateTime;
use App\Entity\Objectregister;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;


class CompanyRepository extends ServiceEntityRepository
{
    private $logger;
    /**
     * @var Connection
     */
    private $conn;


    public function __construct(Connection $connection, ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Company::class);
        $this->logger = $logger;
        $this->conn = $connection;
    }


    /**
     * @param string $domain
     * @return Company|null
     * @throws NonUniqueResultException
     */
    public function getCompanyByDomain(string $domain): ?Company
    {
        $cmp = null;
        if (empty($domain)) {
            return $cmp;
        }
        try {
            $em = $this->getEntityManager();
            $query = $em->createQuery('SELECT c
                    FROM App\Entity\Company c
                    WHERE c.website = :domain')->setParameter('domain', $domain);
            $result = $query->getOneOrNullResult();

            if ($result !== null) {
                $cmp = $result;
            }
            return $cmp;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
        }
    }


    /**
     * @param string $company_name
     * @return bool
     * @author   jsr
     * @internal WEB-3850
     */
    public function doesCompanyNameExist(string $company_name): bool
    {
        $cmp = false;
        if (empty($company_name)) {
            return $cmp;
        }
        try {
            $em = $this->getEntityManager();
            $query = $em->createQuery('SELECT c
                    FROM App\Entity\Company c
                    WHERE c.name = :company_name')->setParameter('company_name', $company_name);
            $result = $query->getOneOrNullResult();

            if ($result !== null) {
                return true;
            }
            return $cmp;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
        }
    }


    /**
     * @param $domain
     * @param $companyName
     * @return Company|null
     * @throws MappingException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     * @internal   added $companyName since WEB-3850 07.04.2020
     * @deprecated since 2020-11-16
     * @author     jsr
     */
    public function createCompany(string $domain, string $companyName): ?Company
    {
        $name = '';
        try {
            // check domain
            $domainChecked = preg_replace('/[ ]*/', '', filter_var(htmlentities($domain), FILTER_VALIDATE_DOMAIN));
            if (empty($domain) || $domain !== $domainChecked) {
                $this->logger->critical('POSSIBLE XSS. DOMAIN NAME OF EMAIL CONTAINED INVALID CHARACTERS.');
                throw new Exception('DOMAIN NAME OF EMAIL CONTAINED INVALID CHARACTERS. [' . $domain . ':' . $domainChecked . ']');
                return null;
            } else {
                $name = filter_var(trim($companyName), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            }

            if (!empty($name)) {
                $em_cmp = $this->getEntityManager();
                $acc_no = $this->getNewAccountNo();
                $time_stamp = date('Y-m-d h:i:s');
                $en_cmp = new Company();
                try {
                    $en_cmp->setAccountNo($acc_no);
                    $en_cmp->setName($name);
                    $en_cmp->setStreet('');
                    $en_cmp->setHouseNumber('');
                    $en_cmp->setPostalCode('');
                    $en_cmp->setCity('');
                    $en_cmp->setCountry('');
                    $en_cmp->setWebsite($domain);
                    $en_cmp->setPhone('');
                    $en_cmp->setVat('');
                    $en_cmp->setLoginUrl('');
                    $en_cmp->setCreatedBy(1);
                    $en_cmp->setUpdatedBy(1);
                    $en_cmp->setCreated(DateTime::createFromFormat('Y-m-d h:i:s', $time_stamp));
                    $en_cmp->setUpdated(DateTime::createFromFormat('Y-m-d h:i:s', $time_stamp));
                    $en_cmp->setModification(0);
                    $em_cmp->persist($en_cmp);
                    $em_cmp->flush();
                    $em_cmp->clear();
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
                }
                return $en_cmp;
            }
        } catch (MappingException $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
        } catch (OptimisticLockException $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
        } catch (ORMException $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
        }
    }


    /**
     * @param string $domain
     * @param string $companyName
     * @param int    $objectregister_id
     * @return Company|null
     * @throws MappingException
     * @throws OptimisticLockException
     */
    public function createInitalCompany(int $objectregister_id,string $domain): ?Company
    {
        $company = null;
        $insertCompany = "INSERT INTO company 
            (objectregister_id,  account_no, name, website, modification, created_by, updated_by) 
            VALUES 
            (:objectregister_id, :account_no, :companyname, :domain, 0, :created_by, :updated_by)";

        $account_no = $this->getNewAccountNo();
        $companyName = $account_no;
        $cu_by = 1;
        $stmt = $this->conn->prepare($insertCompany);
        $stmt->bindParam(':objectregister_id', $objectregister_id, ParameterType::INTEGER);
        $stmt->bindParam(':account_no', $account_no, ParameterType::STRING);
        $stmt->bindParam(':companyname', $companyName, ParameterType::STRING);
        $stmt->bindParam(':domain', $domain, ParameterType::STRING);
        $stmt->bindParam(':created_by', $cu_by, ParameterType::INTEGER);
        $stmt->bindParam(':updated_by', $cu_by, ParameterType::INTEGER);

        try {
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $company_id = $this->conn->lastInsertId();
                $company = $this->getCompanyById($company_id);
            }
        } catch (Exception $e) {
            $this->logger->error('could not cteate company entry for objectregister_id =' . $objectregister_id, [__METHOD__, __LINE__]);
            return null;
        }

        return $company;
    }


    /**
     * @param array $company [company_name,url_option,delegated_domain,street,postal_code,city,country,website,phone,vat]
     * @return Company|null
     * @throws MappingException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     * @internal added $companyName since WEB-3850 07.04.2020
     * @author   jsr
     */
    public function masterAdminCreateNewCompany(array $company): ?Company
    {
        $name = '';
        try {
            // sanitize
            $company_name = filter_var(trim($company['company_name']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            $url_option = filter_var(trim($company['url_option']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            $delegated_domain = filter_var(trim($company['delegated_domain']), FILTER_SANITIZE_URL);
            $website = filter_var(trim($company['website']), FILTER_SANITIZE_URL);
            $street = filter_var(trim($company['street']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            $postal_code = filter_var(trim($company['postal_code']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            $city = filter_var(trim($company['city']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            $country = filter_var(trim($company['country']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            $phone = filter_var(trim($company['phone']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            $vat = filter_var(trim($company['vat']), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);


            $em_cmp = $this->getEntityManager();
            $acc_no = $this->getNewAccountNo();
            $time_stamp = date('Y-m-d h:i:s');
            $en_cmp = new Company();

            try {
                $en_cmp->setAccountNo($acc_no);
                $en_cmp->setName($company_name);
                $en_cmp->setUrlOption($url_option);
                $en_cmp->setDelegatedDomain($delegated_domain);
                $en_cmp->setStreet($street);
                $en_cmp->setHouseNumber('');
                $en_cmp->setPostalCode($postal_code);
                $en_cmp->setCity($city);
                $en_cmp->setCountry($country);
                $en_cmp->setWebsite($website);
                $en_cmp->setPhone($phone);
                $en_cmp->setVat($vat);
                $en_cmp->setLoginUrl('');
                $en_cmp->setCreatedBy(999999980);
                $en_cmp->setUpdatedBy(999999980);
                $en_cmp->setCreated(DateTime::createFromFormat('Y-m-d h:i:s', $time_stamp));
                $en_cmp->setUpdated(DateTime::createFromFormat('Y-m-d h:i:s', $time_stamp));
                $en_cmp->setModification('');
                $em_cmp->persist($en_cmp);
                $em_cmp->flush();
                $em_cmp->clear();
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
            }
            return $en_cmp;
        } catch (MappingException $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
        } catch (OptimisticLockException $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
        } catch (ORMException $e) {
            $this->logger->error($e->getMessage(), [__FUNCTION__, __LINE__]);
        }

        return null;
    }


    /**
	 * <b>get a unique, use once only account_no based on company.id autoincrement</b>
     * @return int
     * @throws Exception
     * @internal changed to public
	 * @author jsr
	 * @since changed @ 2021-02-03
     */
    public function getNewAccountNo(): int
    {
		/**
		 * new, since 2021-02-03: account_no is now generated based on the autoincrement column "id" of table "company"
		 * to create a unique, use "once only" account_no
		 *
		 */
        $account_no = 1000;

        try {
			$sql ="SELECT database()";
			$stmt = $this->conn->prepare($sql);
			$stmt->execute();
			$database = $stmt->fetchOne();

        	$sql ="SELECT auto_increment FROM information_schema.TABLES WHERE TABLE_NAME ='company' and TABLE_SCHEMA=:database";
			$stmt = $this->conn->prepare($sql);
			$stmt->bindParam(':database', $database);
			$stmt->execute();
			$result = $stmt->fetchOne();
/*
 			OLD:
            $em = $this->getEntityManager();
            $query = $em->createQuery('SELECT c
                FROM App\Entity\Company c
                ORDER BY c.accountNo DESC');
            $em->flush();
            $em->clear();
            $result = $query->getResult();
*/

            // $acc_id = (!empty($result)) ? $result[0]->getId() : $account_no; // OLD
            $acc_id = (!empty($result)) ? $result : $account_no;
            // $this->logger->info('$query->getResult()=', [$query->getResult(), __METHOD__, __LINE__]); // OLD
            $this->logger->info('$result=', [$result, __METHOD__, __LINE__]);

            if (empty($acc_id)) {
                $acc_id = 0;
            }

			if($account_no != $acc_id) {
				$account_no += (int)$acc_id;
			}

            return $account_no;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage() . "no company record present. returning with account_no #$account_no#", [__FUNCTION__, __LINE__]);
            return $account_no;
        }
    }


    /**
     * used in app.twig
     *
     * @param string $account_no
     * @return array
     * @author jsr
     */
    public function getCompanyDetailsByAccountId(string $account_no): array
    {
        try {
            $sql = 'SELECT * FROM company WHERE account_no = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $account_no);
            $stmt->execute();
            $res = $stmt->fetch();
            return $res;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return [];
        }
    }


    /**
     * @return array
     */
    public function getAllCompanyDetails(): array
    {
        try {
            $sql = 'SELECT * FROM company';
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetch();
            return $res;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), __METHOD__ . __LINE__);
        }
    }


    /**
     * @param int $id
     * @return object|null
     * @internal WEB-3850
     * @author   jsr
     */
    public function adminGetCompanyById(int $id): ?array
    {
        $companyArray = [];
        $companyObj = $this->findOneBy(['id' => $id]);
        $company = $companyObj->toArray();
        $map = $this->getCompanyFieldMapping();

        if (!empty($company) && !empty($map)) {
            foreach ($company as $key => $value) {
                $companyArray[$map->{$key}] = $value;
            }
            return $companyArray;
        }
        return $companyArray;
    }


    /**
     * getCompanyById
     * @param int $company_id
     * @return Company|null
     */
    public function getCompanyById(int $company_id): ?Company
    {
        return $this->findOneBy(['id' => $company_id]);
    }


    /**
     * select * from company
     * @param int $id
     * @return array
     * @author PVA
     */
    public function getCompanyDetailById(int $id): array
    {
        $cmp_details = [];
        if (empty($id) || $id <= 0) {
            return $cmp_details;
        }

        try {
            $sql = 'SELECT *
                    FROM company
                    WHERE id=:id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $id, ParameterType::INTEGER);
            $stmt->execute();
            $res = $stmt->fetchAll();
            $this->logger->info('$res', [$res, __METHOD__, __LINE__]);

            if (!empty($res) && !empty($res[0])) {
                $cmp_details = $res[0];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }

        $this->logger->info('$cmp_details', [$cmp_details, __METHOD__, __LINE__]);
        return $cmp_details;
    }


    /**
     * @param array $cmp_details
     * @return bool
     * @author   PVA, jsr
     * @internal WEB-3850 jsr added updated_by
     */
    public function updateCompanyDetails(array $cmp_details): bool
    {

    	$this->logger->info('$cmp_details',[$cmp_details, __METHOD__, __LINE__]);
        $status = false;
        if (empty($cmp_details)) {
            return $status;
        }

        if (!empty($cmp_details['updated_by'])) {
            $updated_by = (int)$cmp_details['updated_by'];
        } else {
            $updated_by = 999999980;
        }

        $sql = 'UPDATE company
                SET name        = :company_name,
                    phone       = :phone,
                    website     = :website,
                    url_imprint = :url_imprint,
                    url_privacy_policy = :url_privacy_policy,
                    url_option  = :url_option,
                    url_gtc  = :url_gtc,
                    delegated_domain = :delegated_domain,
                    street      = :address,
                    city        = :city,
                    postal_code = :plz,
                    country     = :country,
                    vat         = :vat,
                    updated_by  = :updated_by
                WHERE id=:id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':address', $cmp_details['address'], ParameterType::STRING);
            $stmt->bindParam(':company_name', $cmp_details['company_name'], ParameterType::STRING);
            $stmt->bindParam(':phone', $cmp_details['phone'], ParameterType::STRING);
            $stmt->bindParam(':website', $cmp_details['website'], ParameterType::STRING);
            $stmt->bindParam(':url_imprint', $cmp_details['url_imprint'], ParameterType::STRING);
            $stmt->bindParam(':url_privacy_policy', $cmp_details['url_privacy_policy'], ParameterType::STRING);
            $stmt->bindParam(':url_gtc', $cmp_details['url_gtc'], ParameterType::STRING);
            $stmt->bindParam(':url_option', $cmp_details['url_option'], ParameterType::STRING);
            $stmt->bindParam(':delegated_domain', $cmp_details['delegated_domain'], ParameterType::STRING);
            $stmt->bindParam(':address', $cmp_details['address'], ParameterType::STRING);
            $stmt->bindParam(':city', $cmp_details['city'], ParameterType::STRING);
            $stmt->bindParam(':plz', $cmp_details['plz'], ParameterType::STRING);
            $stmt->bindParam(':country', $cmp_details['country'], ParameterType::STRING);
            $stmt->bindParam(':vat', $cmp_details['vat'], ParameterType::STRING);
            $stmt->bindParam(':id', $cmp_details['id'], ParameterType::INTEGER);
            $stmt->bindParam(':updated_by', $updated_by, ParameterType::INTEGER);
            $res = $stmt->execute();

            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }
        return $status;
    }


    /**
     * @param int    $company_id
     * @param string $cnf_tgt
     * @return array
     * @author PVA
     * @deprecated
     */
    public function getIntegrationsForCompany(int $company_id, string $cnf_tgt): array
    {
        $cmp_integrations = [];
        if ($company_id <= 0 || empty($cnf_tgt)) {
            return $cmp_integrations;
        }

        try {
            $sql = 'SELECT
                        in_meta.id,
                        in_meta.integrations_id,
                        in_meta.meta_key ,
                        in_meta.meta_value ,
                        i.id,
                        i.company_id,
                        c_t.id,
                        c_t.name
                    FROM integrations_meta in_meta
                        LEFT JOIN integrations i on in_meta.integrations_id = i.id
                        LEFT JOIN configuration_targets c_t on i.configuration_target_id = c_t.id
                    WHERE c_t.name LIKE :cnf_tgt and i.company_id = :id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $company_id);
            $stmt->bindParam(':cnf_tgt', $cnf_tgt);
            $stmt->execute();
            $res = $stmt->fetchAll();
            // Get all integration rows and transfer to array(key => value).
            if (!empty($res) && !empty($res[0])) {
                $r = $res[0];
                $cmp_integrations = ($js_to_arr = json_decode($r['meta_value'], 'true')) ? $js_to_arr : $r['meta_value'];
                $cmp_integrations['id'] = $r['id'];
            }
            /*            foreach ($res as $r) {
                            if(empty($r) || empty($r['meta_key'])) {
                                continue;
                            }
                            $cmp_integrations[$r['meta_key']] =
                                ($js_to_arr = json_decode($r['meta_value'], 'true')) ? $js_to_arr : $r['meta_value'];
            /*                $cmp_integrations['id'] = $r['id'];
                        }*/
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }

        return $cmp_integrations;
    }


    /**
     * @param int    $company_id
     * @param string $cnf_tgt
     * @param array  $cnf_tgt_settings
     * @return bool
     * @author PVA
     */
    public function storeIntegrationsForCompany(int $company_id, string $cnf_tgt, array $cnf_tgt_settings): bool
    {
        $status = false;
        $meta_key = 'settings';
        // check for id from twig file to update the row .
        $id = (isset($cnf_tgt_settings['id']) && is_numeric($cnf_tgt_settings['id']) && $cnf_tgt_settings['id'] > 0) ? $cnf_tgt_settings['id'] : 0;
        try {
            $js_tgt_settings = json_encode($cnf_tgt_settings, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE, 512);
        } catch (\JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        if ($company_id <= 0 || empty($cnf_tgt) || $js_tgt_settings === false) {
            return $status;
        }
        $cnf_tgt_id = $this->fetchConfigTargetId($cnf_tgt);

        $int_id = $this->fetchIntegrationId($company_id, $cnf_tgt_id);
        if ($int_id === 0) {
            $int_id = $this->storeConfigTargetToIntegration($company_id, $cnf_tgt_id);
        }

        $sql = 'INSERT INTO integrations_meta( 
                    id, 
                    integrations_id, 
                    meta_key, 
                    meta_value
                ) 
                VALUES (
                    :id , 
                    :integrations_id, 
                    :meta_key, 
                    :meta_value
                )
                ON DUPLICATE KEY UPDATE
                integrations_id = :integrations_id,
                meta_key = :meta_key,
                meta_value = :meta_value
                ';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':integrations_id', $int_id);
            $stmt->bindParam(':meta_key', $meta_key);
            $stmt->bindParam(':meta_value', $js_tgt_settings);
            $res = $stmt->execute();

            if (is_bool($res) && $res) {
                $status = $res;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }

        return $status;
    }


    /**
     * @param string $cnf_tgt
     * @return int
     * @author PVA
     */
    private function fetchConfigTargetId(
        string $cnf_tgt
    ): int {
        $cnf_tgt_id = 0;
        if (empty($cnf_tgt)) {
            return $cnf_tgt_id;
        }

        $sql = 'SELECT id  
                FROM configuration_targets 
                WHERE name LIKE :cnf_tgt';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':cnf_tgt', $cnf_tgt);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) || !empty($res[0])) {
                $cnf_tgt_id = $res[0]['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }

        return $cnf_tgt_id;
    }


    /**
     * @param int $company_id
     * @param int $cnf_tgt_id
     * @return int
     * @author PVA
     */
    private function fetchIntegrationId(
        int $company_id,
        int $cnf_tgt_id
    ): int {
        $int_id = 0;
        if ($company_id <= 0 || $cnf_tgt_id <= 0) {
            return $int_id;
        }
        $sql = 'SELECT id 
                FROM integrations 
                WHERE 
                    company_id = :company_id AND 
                    configuration_target_id = :cnf_tgt_id';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->bindParam(':cnf_tgt_id', $cnf_tgt_id);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (!empty($res) && !empty($res[0])) {
                $int_id = $res[0]['id'];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }
        return $int_id;
    }


    /**
     * @param int $company_id
     * @param int $cnf_tgt_id
     * @return int
     * @author PVA
     */
    private function storeConfigTargetToIntegration(
        int $company_id,
        int $cnf_tgt_id
    ): int {
        $int_id = 0;
        $user_id = 1;
        if ($company_id <= 0 || $cnf_tgt_id <= 0) {
            return $int_id;
        }
        $sql = 'INSERT INTO integrations (
            company_id, 
            configuration_target_id
        ) 
        VALUES (
            :company_id, 
            :configuration_target_id
        )';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->bindParam(':configuration_target_id', $cnf_tgt_id);
            $res = $stmt->execute();
            if (is_bool($res) && $res) {
                $int_id = $this->conn->lastInsertId();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__ . __LINE__]);
        }

        return $int_id;
    }


    /**
     * @return array
     */
    public function getCompanyDetailsForSwitchAccount(): array
    {
        $acc_details = [];
        $sql = 'SELECT 
                    c.id,
                    c.name
                FROM company c';
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            $res = $stmt->fetchAll();
            if (is_array($res) && !empty($res)) {
                $acc_details = $res;
            }
        } catch (Exception $e) {
        }
        return $acc_details;
    }


    /**
     * @return \stdClass
     * @internal WEB-3850, get field mapping of company table for managecompanies.twig form
     * @author   jsr
     */
    private function getCompanyFieldMapping(): \stdClass
    {
        return json_decode('{
            "id": "id",
            "accountNo": "account_no",
            "name": "company_name",
            "street": "street",
            "houseNumber": "house_number",
            "postalCode": "postal_code",
            "city": "city",
            "country": "country",
            "website": "website",
            "phone": "phone",
            "vat": "vat",
            "loginUrl": "login_url",
            "delegatedDomain": "delegated_domain",
            "urlOption": "url_option",
            "created": "created",
            "createdBy": "created_by",
            "updated": "updated",
            "updatedBy": "updated_by",
            "modification": "modification"
          }');
    }


    /**
     * @param int $company_id
     * @return array
     * @author Pradeep
     */
    public function getCompanyIntegrationMetaData(int $company_id): array
    {
        $company_meta_data = [];
        if ($company_id <= 0) {
            return $company_meta_data;
        }

        try {
            $sql = 'select ct.name,
                           im.meta_value
                    from integrations
                    join configuration_targets ct on integrations.configuration_target_id = ct.id
                    join integrations_meta im on integrations.id = im.integrations_id
                    where company_id = :company_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->execute();
            $company_integrations = $stmt->fetchAll();
            foreach ($company_integrations as $integration) {
                if (empty($integration['meta_value'])) {
                    continue;
                }
                $value = json_decode($integration['meta_value'], true);
                if (!empty($value['password']) || !empty($value['apikey']) || !empty($value['apiPassword'])) {
                    $company_meta_data[$integration['name']] = true;
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }


        return $company_meta_data;
    }


    /**
     * @param int $company_id
     * @return array
     * @author Pradeep
     * @deprecated
     * // TODO : Change the structure to JSON Types
     */
    public function storeDefaultIntegrationsToCompany(int $company_id): void
    {
        $cnf_tgt_settings['maxLeadsPerDomain'] = 4; // default value of settings
        $cnf_tgt_settings['maxLeadsPerIp'] = 4;
        $cnf_tgt_settings['geoBlocking'] = 'on';
        try {
            $this->storeIntegrationsForCompany($company_id, $cnf_trgt, $cnf_tgt_settings);
        } catch (\JsonException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
    }


    /**
     * used in bastelcontroller to generate project object register entries for companies that do not have such an entry
     * @return array|null
     * @deprecated by switch from miointerim to mio
     */
    public function getCompaniesWithoutProjectObjectRegisterEntry(): ?array
    {
        $select = "select distinct cy.id, cy.name, cy.account_no
            from miointerim.company cy
            left outer join miointerim.campaign cp on cp.company_id = cy.id
            where cy.id not in (3,2,20,199,207)";
        $stmt = $this->conn->prepare($select);

        try {
            $stmt->execute();
            $companiesWithoutProjectObjectRegisterEntry = $stmt->fetchAll();
            if ($stmt->rowCount() === 0) {
                return [];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }

        return $companiesWithoutProjectObjectRegisterEntry;
    }


    /**
     * @param int $companyId
     * @return string|null
     * @author Pradeep
     */
    public function getMarketingInOneAPIKeyForCompany(int $companyId): ?string
    {

        $apikey = null;
        try {
            if ($companyId <= 0) {
                throw new RuntimeException('Invalid param $companyId');
            }
            $sql = 'SELECT object_register_meta.meta_value as apikey FROM object_register_meta
                        JOIN objectregister o on object_register_meta.object_register_id = o.id
                        JOIN company c on o.id = c.objectregister_id
                    WHERE c.id=:company_id AND object_register_meta.meta_key = "account_apikey"';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindParam(':company_id', $companyId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $apikey = $stmt->fetchColumn();
            $this->logger->info('APIKEY IN '.__FUNCTION__,[$apikey, __LINE__]);

            return (!empty($apikey)) ? $apikey : null;

        } catch (DBALException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $apikey;
        }

        return $apikey;
    }


    /**
     * @param int $objectRegisterId
     * @return array|null
     * @author Pradeep
     */
    public function getCompanyDetailsByObjectRegisterId(int $objectRegisterId): ?array
    {
        try {
            if ($objectRegisterId <= 0) {
                throw new RuntimeException('Invalid ObjectRegisterId provided');
            }
            $sql = 'SELECT * FROM company c WHERE c.objectregister_id = :objectregister_id';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(':objectregister_id', $objectRegisterId, ParameterType::INTEGER);
            $stmt->execute();
            $rowCount = $stmt->rowCount();
            $result = $stmt->fetchAll();
            if ($rowCount > 0 && !empty($result[ 0 ])) {
                $company = $result[ 0 ];
            }
            return $company;
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__LINE__, __METHOD__]);
            return null;
        }
    }


	/**
	 * @param int $company_id
	 * @return string|null
	 */
	public function getCompanyWebsiteUrl(int $company_id): ?string
	{
		try {
			if ($company_id <= 0) {
				throw new RuntimeException('Invalid ObjectRegisterId provided');
			}
			$sql = 'SELECT c.website FROM company c WHERE c.id = :company_id';
			$stmt = $this->conn->prepare($sql);
			$stmt->bindValue(':company_id', $company_id, ParameterType::INTEGER);
			$stmt->execute();
			$domain = $stmt->fetchColumn();
			$this->logger->info('domain',[$domain, __METHOD__,__LINE__]);

			if ($stmt->rowCount() > 0 && !empty($domain)) {
				return 'https://'. $domain;
			}

		} catch (RuntimeException | Exception $e) {
			$this->logger->error($e->getMessage(), [__LINE__, __METHOD__]);
		}

		return null;

    }
}
