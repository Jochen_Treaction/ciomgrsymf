<?php

namespace App\Listeners;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AppCacheService;
use Psr\Log\LoggerInterface;

class LogoutListener implements LogoutHandlerInterface
{
    protected $apcuCache;
    protected $logger;
    protected $userEmail;
    protected $cryptoService;
    protected $authenticationUtils;
    protected $prefix;

    const USERACTIVATION = 'Iigei7na_user_activation';
    const USERPWRESET = 'Unsd24oK_user_pw_reset';


    public function __construct(
        AppCacheService $apcuCache,
        LoggerInterface $logger,
        AuthenticationUtils $authenticationUtils
    ) {
        $this->prefix = $_ENV['APP_ENV'].':';
        $this->authenticationUtils = $authenticationUtils;
        $this->apcuCache = $apcuCache;
        $this->logger = $logger;
        $this->userEmail = $authenticationUtils->getLastUsername();
    }


    /**
     * @inheritDoc
     */
    public function logout(Request $request, Response $response, TokenInterface $token)
    {
        // TODO: Implement logout() method.
        $hasEntryBefore = ($this->apcuCache->entryExists($this->getUserKey($this->userEmail))) ? 'YES' : 'NO';
//        $this->apcuCache->del($this->userEmail);
//        $this->apcuCache->del(self::USERPWRESET);
//        $this->apcuCache->del(self::USERACTIVATION);
//        $this->apcuCache->del($this->apcuCache::SELECTED_COMPANY_ID_OF_USER.$this->userEmail);
        $this->apcuCache->clearCache();
        $hasEntryAfter = ($this->apcuCache->entryExists($this->getUserKey($this->userEmail))) ? 'YES' : 'NO';
        $hasCompEntryAfter = ($this->apcuCache->entryExists($this->apcuCache::SELECTED_COMPANY_ID_OF_USER.$this->userEmail)) ? 'YES' : 'NO';
        $this->logger->info('LOGOUT-LISTENER =  ', [
            'emailtologout' => $this->userEmail,
            'token' => $token,
            '$hasEntryBefore'=>$hasEntryBefore,
            '$hasEntryAfter'=>$hasEntryAfter,
            '$hasCompEntryAfter' => $hasCompEntryAfter,
            // 'apcu' => apcu_cache_info() // too much !
        ]);
    }


    /**
     * @param string $key
     * @return string
     */
    protected function getUserKey(string $key=''):string
    {
        $this->logger->info('key parts', [$this->prefix, $this->userEmail, hash("md5", $this->userEmail), $key, __METHOD__, __LINE__]);

        $userKey = $this->prefix . hash("md5", $this->userEmail);
        $userKey .= (!empty($key)) ? ':' . $key : '';

        $this->logger->info('userKey', [$userKey, __METHOD__, __LINE__]);
        return $userKey;
    }

}
