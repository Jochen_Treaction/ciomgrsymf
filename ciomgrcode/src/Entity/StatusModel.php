<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StatusModel
 *
 * @ORM\Table(name="status_model", indexes={@ORM\Index(name="fk_unique_object_type_id_idx", columns={"unique_object_type_id"}), @ORM\Index(name="fk_status_model_statusdef1_idx", columns={"statusdef_id"})})
 * @ORM\Entity
 */
class StatusModel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="modelname", type="string", length=80, nullable=false)
     */
    private $modelname;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \Statusdef
     *
     * @ORM\ManyToOne(targetEntity="Statusdef")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusdef_id", referencedColumnName="id")
     * })
     */
    private $statusdef;

    /**
     * @var \UniqueObjectType
     *
     * @ORM\ManyToOne(targetEntity="UniqueObjectType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unique_object_type_id", referencedColumnName="id")
     * })
     */
    private $uniqueObjectType;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getModelname(): ?string
    {
        return $this->modelname;
    }

    public function setModelname(string $modelname): self
    {
        $this->modelname = $modelname;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?string $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getStatusdef(): ?Statusdef
    {
        return $this->statusdef;
    }

    public function setStatusdef(?Statusdef $statusdef): self
    {
        $this->statusdef = $statusdef;

        return $this;
    }

    public function getUniqueObjectType(): ?UniqueObjectType
    {
        return $this->uniqueObjectType;
    }

    public function setUniqueObjectType(?UniqueObjectType $uniqueObjectType): self
    {
        $this->uniqueObjectType = $uniqueObjectType;

        return $this;
    }


}
