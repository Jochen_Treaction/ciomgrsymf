<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LeadCustomfieldContent
 *
 * @ORM\Table(name="lead_customfield_content", indexes={@ORM\Index(name="fk_leads_idx", columns={"leads_id"}), @ORM\Index(name="fk_customfields_idx", columns={"customfields_id"})})
 * @ORM\Entity
 */
class LeadCustomfieldContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lead_customfield_content", type="text", length=0, nullable=true, options={"comment"="this field must be encrypted"})
     */
    private $leadCustomfieldContent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_lead_customfield_content", type="string", length=128, nullable=true, options={"comment"="thats the hash of the unencypted lead_custom_field_content"})
     */
    private $hashLeadCustomfieldContent;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \Customfields
     *
     * @ORM\ManyToOne(targetEntity="Customfields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customfields_id", referencedColumnName="id")
     * })
     */
    private $customfields;

    /**
     * @var \Leads
     *
     * @ORM\ManyToOne(targetEntity="Leads")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="leads_id", referencedColumnName="id")
     * })
     */
    private $leads;

    public function getId(): ?string
    {
        return $this->id;
    }


    public function getLeadCustomfieldContent(): ?string
    {
        return $this->leadCustomfieldContent;
    }


    public function setLeadCustomfieldContent(?string $leadCustomfieldContent): self
    {
        $this->leadCustomfieldContent = $leadCustomfieldContent;

        return $this;
    }


    public function getHashLeadCustomfieldContent(): ?string
    {
        return $this->hashLeadCustomfieldContent;
    }


    public function setHashLeadCustomfieldContent(?string $hashLeadCustomfieldContent): self
    {
        $this->hashLeadCustomfieldContent = $hashLeadCustomfieldContent;

        return $this;
    }


    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }


    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getCustomfields(): ?Customfields
    {
        return $this->customfields;
    }

    public function setCustomfields(?Customfields $customfields): self
    {
        $this->customfields = $customfields;

        return $this;
    }

    public function getLeads(): ?Leads
    {
        return $this->leads;
    }

    public function setLeads(?Leads $leads): self
    {
        $this->leads = $leads;

        return $this;
    }


}
