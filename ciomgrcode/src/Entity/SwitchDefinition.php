<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SwitchDefinition
 *
 * @ORM\Table(name="switch_definition", indexes={@ORM\Index(name="fk_switch_definition_switch_operators1_idx", columns={"switch_operators_id"})})
 * @ORM\Entity
 */
class SwitchDefinition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="expected_value", type="text", length=65535, nullable=true)
     */
    private $expectedValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="check_value", type="text", length=65535, nullable=true)
     */
    private $checkValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var \SwitchOperators
     *
     * @ORM\ManyToOne(targetEntity="SwitchOperators")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="switch_operators_id", referencedColumnName="id")
     * })
     */
    private $switchOperators;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getExpectedValue(): ?string
    {
        return $this->expectedValue;
    }

    public function setExpectedValue(?string $expectedValue): self
    {
        $this->expectedValue = $expectedValue;

        return $this;
    }

    public function getCheckValue(): ?string
    {
        return $this->checkValue;
    }

    public function setCheckValue(?string $checkValue): self
    {
        $this->checkValue = $checkValue;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSwitchOperators(): ?SwitchOperators
    {
        return $this->switchOperators;
    }

    public function setSwitchOperators(?SwitchOperators $switchOperators): self
    {
        $this->switchOperators = $switchOperators;

        return $this;
    }


}
