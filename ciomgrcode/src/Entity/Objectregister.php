<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Objectregister
 *
 * @ORM\Table(name="objectregister", uniqueConstraints={@ORM\UniqueConstraint(name="uk_ouid_version", columns={"object_unique_id", "version"})}, indexes={@ORM\Index(name="fk_obj_reg_statusdef1_idx", columns={"statusdef_id"}), @ORM\Index(name="fk_obj_reg_obj_type1_idx", columns={"unique_object_type_id"})})
 * @ORM\Entity
 */
class Objectregister
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="version", type="integer", nullable=false, options={"default"="1","unsigned"=true})
     */
    private $version = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="object_unique_id", type="string", length=190, nullable=false, options={"default"="00000000-0000-0000-00000000000000000"})
     */
    private $objectUniqueId = '00000000-0000-0000-00000000000000000';

    /**
     * @var int
     *
     * @ORM\Column(name="modification_no", type="bigint", nullable=false, options={"unsigned"=true})
     */
    private $modificationNo = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \UniqueObjectType
     *
     * @ORM\ManyToOne(targetEntity="UniqueObjectType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unique_object_type_id", referencedColumnName="id")
     * })
     */
    private $uniqueObjectType;

    /**
     * @var \Statusdef
     *
     * @ORM\ManyToOne(targetEntity="Statusdef")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statusdef_id", referencedColumnName="id")
     * })
     */
    private $statusdef;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getVersion(): ?int
    {
        return $this->version;
    }

    public function setVersion(int $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getObjectUniqueId(): ?string
    {
        return $this->objectUniqueId;
    }

    public function setObjectUniqueId(string $objectUniqueId): self
    {
        $this->objectUniqueId = $objectUniqueId;

        return $this;
    }

    public function getModificationNo(): ?string
    {
        return $this->modificationNo;
    }

    public function setModificationNo(string $modificationNo): self
    {
        $this->modificationNo = $modificationNo;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?string $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getUniqueObjectType(): ?UniqueObjectType
    {
        return $this->uniqueObjectType;
    }

    public function setUniqueObjectType(?UniqueObjectType $uniqueObjectType): self
    {
        $this->uniqueObjectType = $uniqueObjectType;

        return $this;
    }

    public function getStatusdef(): ?Statusdef
    {
        return $this->statusdef;
    }

    public function setStatusdef(?Statusdef $statusdef): self
    {
        $this->statusdef = $statusdef;

        return $this;
    }


}
