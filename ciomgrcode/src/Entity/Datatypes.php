<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Datatypes
 *
 * @ORM\Table(name="datatypes")
 * @ORM\Entity
 */
class Datatypes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=80, nullable=false, options={"comment"="for example string, int, email, double, boolean, ..."})
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phptype", type="string", length=80, nullable=true)
     */
    private $phptype;

    /**
     * @var string|null
     *
     * @ORM\Column(name="base_regex", type="text", length=65535, nullable=true)
     */
    private $baseRegex;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    public function getId(): ?string
    {
        return $this->id;
    }


    public function getName(): ?string
    {
        return $this->name;
    }


    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getPhptype(): ?string
    {
        return $this->phptype;
    }


    public function setPhptype(?string $phptype): self
    {
        $this->phptype = $phptype;

        return $this;
    }


    public function getBaseRegex(): ?string
    {
        return $this->baseRegex;
    }


    public function setBaseRegex(?string $baseRegex): self
    {
        $this->baseRegex = $baseRegex;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }


    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?string $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }


}
