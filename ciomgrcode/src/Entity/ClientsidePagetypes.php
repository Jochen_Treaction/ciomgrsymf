<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientsidePagetypes
 *
 * @ORM\Table(name="clientside_pagetypes", indexes={@ORM\Index(name="fk_clientside_pagetypes_processhook1_idx", columns={"processhook_id"})})
 * @ORM\Entity
 */
class ClientsidePagetypes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pagetype", type="string", length=45, nullable=false)
     */
    private $pagetype;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;

    /**
     * @var \Processhook
     *
     * @ORM\ManyToOne(targetEntity="Processhook")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processhook_id", referencedColumnName="id")
     * })
     */
    private $processhook;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPagetype(): ?string
    {
        return $this->pagetype;
    }

    public function setPagetype(string $pagetype): self
    {
        $this->pagetype = $pagetype;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getProcesshook(): ?Processhook
    {
        return $this->processhook;
    }

    public function setProcesshook(?Processhook $processhook): self
    {
        $this->processhook = $processhook;

        return $this;
    }


}
