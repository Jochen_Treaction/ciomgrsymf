<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DatainterfaceFields
 *
 * @ORM\Table(name="datainterface_fields", indexes={@ORM\Index(name="fk_datainterface_fields_output_datainterface1_idx", columns={"output_datainterface_id"}), @ORM\Index(name="fk_objectregister_id_idx", columns={"source_objectregister_id"}), @ORM\Index(name="fk_datainterface_fields_input_datainterface1_idx", columns={"input_datainterface_id"})})
 * @ORM\Entity
 */
class DatainterfaceFields
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \InputDatainterface
     *
     * @ORM\ManyToOne(targetEntity="InputDatainterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="input_datainterface_id", referencedColumnName="id")
     * })
     */
    private $inputDatainterface;

    /**
     * @var \OutputDatainterface
     *
     * @ORM\ManyToOne(targetEntity="OutputDatainterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="output_datainterface_id", referencedColumnName="id")
     * })
     */
    private $outputDatainterface;

    /**
     * @var \Objectregister
     *
     * @ORM\ManyToOne(targetEntity="Objectregister")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="source_objectregister_id", referencedColumnName="id")
     * })
     */
    private $sourceObjectregister;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getInputDatainterface(): ?InputDatainterface
    {
        return $this->inputDatainterface;
    }

    public function setInputDatainterface(?InputDatainterface $inputDatainterface): self
    {
        $this->inputDatainterface = $inputDatainterface;

        return $this;
    }

    public function getOutputDatainterface(): ?OutputDatainterface
    {
        return $this->outputDatainterface;
    }

    public function setOutputDatainterface(?OutputDatainterface $outputDatainterface): self
    {
        $this->outputDatainterface = $outputDatainterface;

        return $this;
    }

    public function getSourceObjectregister(): ?Objectregister
    {
        return $this->sourceObjectregister;
    }

    public function setSourceObjectregister(?Objectregister $sourceObjectregister): self
    {
        $this->sourceObjectregister = $sourceObjectregister;

        return $this;
    }


}
