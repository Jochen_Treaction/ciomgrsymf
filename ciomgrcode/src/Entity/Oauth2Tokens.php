<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Oauth2Tokens
 *
 * @ORM\Table(name="oauth2_tokens", indexes={@ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class Oauth2Tokens
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="access_tokens", type="string", length=255, nullable=false)
     */
    private $accessTokens;

    /**
     * @var string
     *
     * @ORM\Column(name="expire_time", type="string", length=255, nullable=false)
     */
    private $expireTime;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAccessTokens(): ?string
    {
        return $this->accessTokens;
    }

    public function setAccessTokens(string $accessTokens): self
    {
        $this->accessTokens = $accessTokens;

        return $this;
    }

    public function getExpireTime(): ?string
    {
        return $this->expireTime;
    }

    public function setExpireTime(string $expireTime): self
    {
        $this->expireTime = $expireTime;

        return $this;
    }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }


}
