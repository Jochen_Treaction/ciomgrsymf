<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="uk_users_email", columns={"email"})}, indexes={@ORM\Index(name="company_id", columns={"company_id"})})
 * @ORM\Entity
 */
class Users implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=180, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="roles", type="text", length=0, nullable=true)
     */
    private $roles;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="string", length=100, nullable=true)
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="string", length=100, nullable=true)
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="salutation", type="string", length=100, nullable=true)
     */
    private $salutation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="registration", type="string", length=100, nullable=true)
     */
    private $registration;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="login_date", type="datetime", nullable=true)
     */
    private $loginDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mobile", type="string", length=100, nullable=true)
     */
    private $mobile;

    /**
     * @var int
     *
     * @ORM\Column(name="failed_logins", type="integer", nullable=false)
     */
    private $failedLogins = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="super_admin", type="boolean", nullable=false)
     */
    private $superAdmin = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="permission", type="boolean", nullable=false, options={"default"="1"})
     */
    private $permission = true;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="dpa", type="boolean", nullable=true)
     */
    private $dpa;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dpa_date", type="datetime", nullable=true)
     */
    private $dpaDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=0, nullable=false, options={"default"="new"})
     */
    private $status = 'new';

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = json_decode($this->roles);
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = json_encode($roles);

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }


    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }


    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getSalutation(): ?string
    {
        return $this->salutation;
    }


    public function setSalutation(?string $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getRegistration(): ?string
    {
        return $this->registration;
    }


    public function setRegistration(?string $registration): self
    {
        $this->registration = $registration;

        return $this;
    }


    public function getLoginDate(): ?\DateTimeInterface
    {
        return $this->loginDate;
    }


    public function setLoginDate(?\DateTimeInterface $loginDate): self
    {
        $this->loginDate = $loginDate;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }


    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }


    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }


    public function getFailedLogins(): ?int
    {
        return $this->failedLogins;
    }


    public function setFailedLogins(int $failedLogins): self
    {
        $this->failedLogins = $failedLogins;

        return $this;
    }

    public function getSuperAdmin(): ?bool
    {
        return $this->superAdmin;
    }

    public function setSuperAdmin(bool $superAdmin): self
    {
        $this->superAdmin = $superAdmin;

        return $this;
    }

    public function getPermission(): ?bool
    {
        return $this->permission;
    }

    public function setPermission(bool $permission): self
    {
        $this->permission = $permission;

        return $this;
    }

    public function getDpa(): ?bool
    {
        return $this->dpa;
    }

    public function setDpa(?bool $dpa): self
    {
        $this->dpa = $dpa;

        return $this;
    }

    public function getDpaDate(): ?\DateTimeInterface
    {
        return $this->dpaDate;
    }

    public function setDpaDate(?\DateTimeInterface $dpaDate): self
    {
        $this->dpaDate = $dpaDate;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }


    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }


    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt():string
    {
        return '#6lz,@mzrlF+_@eJbh+\'N(9B{Qitlj$\V=!,9XtRBgVn>n0\'~<m?]MWZQ*s&AR\'@#>VhovJ$>,>-4}5+u#SF~*X9Rk&M?nw<>$"XUqGK:Z!uKxG#*/A?w+=*XgSC`=Wd{#:Reanbs=A`P}AxU:FOjQ|d>1C.JHpKJ*:jgo]M/6M5Y~BC0%Ipg[**g?gM(Pv$~+tk6Dm?0i18.vMQ%d@Z*e@-(_/MM=>T$7g./UaZpOsqwh!8]ww@(=U9vcx%a?,z';
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials():self
    {
        // TODO: Implement eraseCredentials() method.
        $this->setRoles([]);
        return $this;
    }

	public function getRolesAsString(): string
	{
		$roles = json_decode($this->roles);
		// guarantee every user at least has ROLE_USER
		$roles[] = 'ROLE_USER';

		return json_encode(array_unique($roles));
	}

	public function toString():string
	{
		return json_encode($this->toArray());
	}


	public function toArray(): array
	{
		return [
			'id' => $this->id,
			'company_id' => $this->company,
			'email' => $this->email,
			'password' => $this->password,
			'roles' => $this->roles,
			'first_name' => $this->firstName,
			'last_name' => $this->lastName,
			'salutation' => $this->salutation,
			'registration' => $this->registration,
			'login_date' => $this->loginDate,
			'phone' => $this->phone,
			'mobile' => $this->mobile,
			'failed_logins' => $this->failedLogins,
			'super_admin' => $this->superAdmin,
			'status' => $this->status,
		];
	}
}
