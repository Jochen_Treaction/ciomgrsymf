<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mailsystems
 *
 * @ORM\Table(name="mailsystems")
 * @ORM\Entity
 */
class Mailsystems
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="vendor", type="string", length=128, nullable=false, options={"default"="not defined"})
     */
    private $vendor = 'not defined';

    /**
     * @var string|null
     *
     * @ORM\Column(name="Website", type="string", length=255, nullable=true, options={"default"="null"})
     */
    private $website = 'null';


    public function getId(): ?string
    {
        return $this->id;
    }


    public function getVendor(): ?string
    {
        return $this->vendor;
    }


    public function setVendor(string $vendor): self
    {
        $this->vendor = $vendor;

        return $this;
    }


    public function getWebsite(): ?string
    {
        return $this->website;
    }


    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }


}
