<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customfields
 *
 * @ORM\Table(name="customfields", indexes={@ORM\Index(name="fk_cf_campaign_idx", columns={"campaign_id"}), @ORM\Index(name="fk_customfields_object_register1_idx", columns={"objectregister_id"}), @ORM\Index(name="fk_customfields_datatypes1_idx", columns={"datatypes_id"})})
 * @ORM\Entity
 */
class Customfields
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ref_customfields_id", type="integer", nullable=true)
     */
    private $refCustomfieldsId;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldname", type="string", length=80, nullable=false, options={"default"="undefined"})
     */
    private $fieldname = 'undefined';

    /**
     * @var bool
     *
     * @ORM\Column(name="hide_in_ui_selection", type="boolean", nullable=false, options={"comment"="can be set by user in project-level UI to leave this field active for running campaigns but disable it for selection in future pages or webhooks"})
     */
    private $hideInUiSelection = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="mandatory", type="boolean", nullable=false)
     */
    private $mandatory = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="regexp_validation", type="text", length=65535, nullable=true)
     */
    private $regexpValidation;

    /**
     * @var \Campaign
     *
     * @ORM\ManyToOne(targetEntity="Campaign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     * })
     */
    private $campaign;

    /**
     * @var \Datatypes
     *
     * @ORM\ManyToOne(targetEntity="Datatypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="datatypes_id", referencedColumnName="id")
     * })
     */
    private $datatypes;

    /**
     * @var \Objectregister
     *
     * @ORM\ManyToOne(targetEntity="Objectregister")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="objectregister_id", referencedColumnName="id")
     * })
     */
    private $objectregister;


    public function getId(): ?string
    {
        return $this->id;
    }


    public function getRefCustomfieldsId(): ?int
    {
        return $this->refCustomfieldsId;
    }


    public function setRefCustomfieldsId(?int $refCustomfieldsId): self
    {
        $this->refCustomfieldsId = $refCustomfieldsId;

        return $this;
    }


    public function getFieldname(): ?string
    {
        return $this->fieldname;
    }


    public function setFieldname(string $fieldname): self
    {
        $this->fieldname = $fieldname;

        return $this;
    }


    public function getHideInUiSelection(): ?bool
    {
        return $this->hideInUiSelection;
    }


    public function setHideInUiSelection(bool $hideInUiSelection): self
    {
        $this->hideInUiSelection = $hideInUiSelection;

        return $this;
    }

    public function getMandatory(): ?bool
    {
        return $this->mandatory;
    }

    public function setMandatory(bool $mandatory): self
    {
        $this->mandatory = $mandatory;

        return $this;
    }


    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }


    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }


    public function setUpdatedBy(?string $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }


    public function getRegexpValidation(): ?string
    {
        return $this->regexpValidation;
    }


    public function setRegexpValidation(?string $regexpValidation): self
    {
        $this->regexpValidation = $regexpValidation;

        return $this;
    }


    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }


    public function setCampaign(?Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    public function getDatatypes(): ?Datatypes
    {
        return $this->datatypes;
    }

    public function setDatatypes(?Datatypes $datatypes): self
    {
        $this->datatypes = $datatypes;

        return $this;
    }


    public function getObjectregister(): ?Objectregister
    {
        return $this->objectregister;
    }


    public function setObjectregister(?Objectregister $objectregister): self
    {
        $this->objectregister = $objectregister;

        return $this;
    }


}
