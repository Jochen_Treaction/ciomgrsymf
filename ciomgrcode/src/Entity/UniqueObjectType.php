<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UniqueObjectType
 *
 * @ORM\Table(name="unique_object_type", uniqueConstraints={@ORM\UniqueConstraint(name="uk_unique_object_name", columns={"unique_object_name"})})
 * @ORM\Entity
 */
class UniqueObjectType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="assigned_table", type="string", length=80, nullable=false, options={"comment"="existing tablename of a table that imports objectregister.id as foreign key"})
     */
    private $assignedTable;

    /**
     * @var string
     *
     * @ORM\Column(name="unique_object_name", type="string", length=190, nullable=false, options={"comment"="for example order-customfields, car-customfields, default-customfields"})
     */
    private $uniqueObjectName;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_versioning", type="boolean", nullable=false, options={"default"="1","comment"="object per dafault have versioning. currently ther is only project, which does not have versioning. if versioning is set to false, then the instance in objectregister will have only version = 1"})
     */
    private $hasVersioning = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_encryption", type="boolean", nullable=false)
     */
    private $hasEncryption = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAssignedTable(): ?string
    {
        return $this->assignedTable;
    }

    public function setAssignedTable(string $assignedTable): self
    {
        $this->assignedTable = $assignedTable;

        return $this;
    }

    public function getUniqueObjectName(): ?string
    {
        return $this->uniqueObjectName;
    }

    public function setUniqueObjectName(string $uniqueObjectName): self
    {
        $this->uniqueObjectName = $uniqueObjectName;

        return $this;
    }


    public function getHasVersioning(): ?bool
    {
        return $this->hasVersioning;
    }


    public function setHasVersioning(bool $hasVersioning): self
    {
        $this->hasVersioning = $hasVersioning;

        return $this;
    }


    public function getHasEncryption(): ?bool
    {
        return $this->hasEncryption;
    }


    public function setHasEncryption(bool $hasEncryption): self
    {
        $this->hasEncryption = $hasEncryption;

        return $this;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }


    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?string $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }


}
