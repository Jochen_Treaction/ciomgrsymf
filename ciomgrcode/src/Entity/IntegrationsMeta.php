<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IntegrationsMeta
 *
 * @ORM\Table(name="integrations_meta", indexes={@ORM\Index(name="idx_meta_key", columns={"meta_key"}), @ORM\Index(name="fk_integrations_meta_integrations_idx", columns={"integrations_id"})})
 * @ORM\Entity
 */
class IntegrationsMeta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_key", type="string", length=255, nullable=false, options={"default"="undefined"})
     */
    private $metaKey = 'undefined';

    /**
     * @var string|null
     *
     * @ORM\Column(name="meta_value", type="text", length=16777215, nullable=true)
     */
    private $metaValue;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \Integrations
     *
     * @ORM\ManyToOne(targetEntity="Integrations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integrations_id", referencedColumnName="id")
     * })
     */
    private $integrations;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getMetaKey(): ?string
    {
        return $this->metaKey;
    }

    public function setMetaKey(string $metaKey): self
    {
        $this->metaKey = $metaKey;

        return $this;
    }

    public function getMetaValue(): ?string
    {
        return $this->metaValue;
    }

    public function setMetaValue(?string $metaValue): self
    {
        $this->metaValue = $metaValue;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }


    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }


    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getIntegrations(): ?Integrations
    {
        return $this->integrations;
    }

    public function setIntegrations(?Integrations $integrations): self
    {
        $this->integrations = $integrations;

        return $this;
    }


}
