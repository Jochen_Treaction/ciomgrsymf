<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 *
 * @ORM\Table(name="action")
 * @ORM\Entity
 */
class Action
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="method", type="string", length=10, nullable=true)
     */
    private $method;

    /**
     * @var string|null
     *
     * @ORM\Column(name="host", type="string", length=255, nullable=true)
     */
    private $host;

    /**
     * @var string|null
     *
     * @ORM\Column(name="end_point", type="string", length=255, nullable=true)
     */
    private $endPoint;

    /**
     * @var string|null
     *
     * @ORM\Column(name="params", type="string", length=255, nullable=true)
     */
    private $params;

    /**
     * @var string|null
     *
     * @ORM\Column(name="output_status", type="string", length=255, nullable=true)
     */
    private $outputStatus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="output_message", type="string", length=255, nullable=true)
     */
    private $outputMessage;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(?string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(?string $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getEndPoint(): ?string
    {
        return $this->endPoint;
    }

    public function setEndPoint(?string $endPoint): self
    {
        $this->endPoint = $endPoint;

        return $this;
    }

    public function getParams(): ?string
    {
        return $this->params;
    }

    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function getOutputStatus(): ?string
    {
        return $this->outputStatus;
    }

    public function setOutputStatus(?string $outputStatus): self
    {
        $this->outputStatus = $outputStatus;

        return $this;
    }

    public function getOutputMessage(): ?string
    {
        return $this->outputMessage;
    }

    public function setOutputMessage(?string $outputMessage): self
    {
        $this->outputMessage = $outputMessage;

        return $this;
    }


}
