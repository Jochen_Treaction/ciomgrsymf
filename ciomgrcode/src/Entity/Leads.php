<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Leads
 *
 * @ORM\Table(name="leads", indexes={@ORM\Index(name="ix_hash_campaign", columns={"hash_campaign"}), @ORM\Index(name="campaign_id", columns={"campaign_id"}), @ORM\Index(name="ix_hash_salutation", columns={"hash_salutation"}), @ORM\Index(name="fk_obj_reg_idx", columns={"objectregister_id"}), @ORM\Index(name="ix_hash_first_name", columns={"hash_first_name"}), @ORM\Index(name="ix_hash_firma", columns={"hash_firma"}), @ORM\Index(name="ix_hash_city", columns={"hash_city"}), @ORM\Index(name="ix_hash_country", columns={"hash_country"}), @ORM\Index(name="ix_hash_lead_reference", columns={"hash_lead_reference"}), @ORM\Index(name="account", columns={"account"}), @ORM\Index(name="ix_hash_email", columns={"hash_email"}), @ORM\Index(name="ix_hash_last_name", columns={"hash_last_name"}), @ORM\Index(name="ix_hash_postal_code", columns={"hash_postal_code"})})
 * @ORM\Entity
 */
class Leads
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="account", type="bigint", nullable=true, options={"unsigned"=true,"comment"="account = company_id - shoud have a FK to company.id"})
     */
    private $account;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="text", length=65535, nullable=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="salutation", type="string", length=255, nullable=true)
     */
    private $salutation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="text", length=65535, nullable=true)
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="text", length=65535, nullable=true)
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="firma", type="text", length=65535, nullable=true)
     */
    private $firma;

    /**
     * @var string|null
     *
     * @ORM\Column(name="street", type="text", length=65535, nullable=true)
     */
    private $street;

    /**
     * @var string|null
     *
     * @ORM\Column(name="postal_code", type="text", length=65535, nullable=true)
     */
    private $postalCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="text", length=65535, nullable=true)
     */
    private $city;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country", type="text", length=65535, nullable=true)
     */
    private $country;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="text", length=65535, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="campaign", type="text", length=65535, nullable=true)
     */
    private $campaign;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lead_reference", type="text", length=65535, nullable=true)
     */
    private $leadReference;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="text", length=65535, nullable=true)
     */
    private $url;

    /**
     * @var string|null
     *
     * @ORM\Column(name="final_url", type="text", length=65535, nullable=true)
     */
    private $finalUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="trafficsource", type="text", length=65535, nullable=true)
     */
    private $trafficsource;

    /**
     * @var string|null
     *
     * @ORM\Column(name="utm_parameters", type="text", length=65535, nullable=true)
     */
    private $utmParameters;

    /**
     * @var string|null
     *
     * @ORM\Column(name="split_version", type="text", length=65535, nullable=true)
     */
    private $splitVersion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="targetgroup", type="text", length=65535, nullable=true)
     */
    private $targetgroup;

    /**
     * @var string|null
     *
     * @ORM\Column(name="affiliateID", type="text", length=65535, nullable=true, options={"comment"="Affiliate Id from campaign URL"})
     */
    private $affiliateid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="affiliateSubID", type="text", length=65535, nullable=true, options={"comment"="AffiliateSubId from campaign URL"})
     */
    private $affiliatesubid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=true)
     */
    private $ip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="device_type", type="string", length=255, nullable=true)
     */
    private $deviceType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="device_browser", type="string", length=255, nullable=true)
     */
    private $deviceBrowser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="device_os", type="string", length=255, nullable=true)
     */
    private $deviceOs;

    /**
     * @var string|null
     *
     * @ORM\Column(name="device_os_version", type="string", length=255, nullable=true)
     */
    private $deviceOsVersion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="device_browser_version", type="string", length=255, nullable=true)
     */
    private $deviceBrowserVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="hash_email", type="string", length=128, nullable=false, options={"comment"="needed for fast, index based selection based on hashed for selection) email-adress"})
     */
    private $hashEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_salutation", type="string", length=128, nullable=true)
     */
    private $hashSalutation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_first_name", type="string", length=128, nullable=true)
     */
    private $hashFirstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_last_name", type="string", length=128, nullable=true)
     */
    private $hashLastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_firma", type="string", length=128, nullable=true)
     */
    private $hashFirma;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_street", type="string", length=128, nullable=true)
     */
    private $hashStreet;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_postal_code", type="string", length=128, nullable=true)
     */
    private $hashPostalCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_city", type="string", length=128, nullable=true)
     */
    private $hashCity;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_country", type="string", length=128, nullable=true)
     */
    private $hashCountry;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_phone", type="string", length=128, nullable=true)
     */
    private $hashPhone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_campaign", type="string", length=128, nullable=true)
     */
    private $hashCampaign;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_lead_reference", type="string", length=128, nullable=true)
     */
    private $hashLeadReference;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_url", type="string", length=128, nullable=true)
     */
    private $hashUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_final_url", type="string", length=128, nullable=true)
     */
    private $hashFinalUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_trafficsource", type="string", length=128, nullable=true)
     */
    private $hashTrafficsource;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_utm_parameters", type="string", length=128, nullable=true)
     */
    private $hashUtmParameters;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_split_version", type="string", length=128, nullable=true)
     */
    private $hashSplitVersion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_targetgroup", type="string", length=128, nullable=true)
     */
    private $hashTargetgroup;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_affiliateID", type="string", length=128, nullable=true)
     */
    private $hashAffiliateid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_affiliateSubID", type="string", length=128, nullable=true)
     */
    private $hashAffiliatesubid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_ip", type="string", length=128, nullable=true)
     */
    private $hashIp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_device_type", type="string", length=128, nullable=true)
     */
    private $hashDeviceType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_device_browser", type="string", length=128, nullable=true)
     */
    private $hashDeviceBrowser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_device_os", type="string", length=128, nullable=true)
     */
    private $hashDeviceOs;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_device_os_version", type="string", length=128, nullable=true)
     */
    private $hashDeviceOsVersion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_device_browser_version", type="string", length=128, nullable=true)
     */
    private $hashDeviceBrowserVersion;

    /**
     * @var int|null
     *
     * @ORM\Column(name="modification", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $modification;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \Campaign
     *
     * @ORM\ManyToOne(targetEntity="Campaign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     * })
     */
    private $campaign2;

    /**
     * @var \Objectregister
     *
     * @ORM\ManyToOne(targetEntity="Objectregister")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="objectregister_id", referencedColumnName="id")
     * })
     */
    private $objectregister;

    public function getId(): ?string
    {
        return $this->id;
    }


    public function getAccount(): ?string
    {
        return $this->account;
    }


    public function setAccount(?string $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSalutation(): ?string
    {
        return $this->salutation;
    }


    public function setSalutation(?string $salutation): self
    {
        $this->salutation = $salutation;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }


    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }


    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirma(): ?string
    {
        return $this->firma;
    }


    public function setFirma(?string $firma): self
    {
        $this->firma = $firma;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }


    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }


    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }


    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }


    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }


    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }


    public function getCampaign(): ?string
    {
        return $this->campaign;
    }


    public function setCampaign(?string $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }


    public function getLeadReference(): ?string
    {
        return $this->leadReference;
    }


    public function setLeadReference(?string $leadReference): self
    {
        $this->leadReference = $leadReference;

        return $this;
    }


    public function getUrl(): ?string
    {
        return $this->url;
    }


    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getFinalUrl(): ?string
    {
        return $this->finalUrl;
    }


    public function setFinalUrl(?string $finalUrl): self
    {
        $this->finalUrl = $finalUrl;

        return $this;
    }

    public function getTrafficsource(): ?string
    {
        return $this->trafficsource;
    }


    public function setTrafficsource(?string $trafficsource): self
    {
        $this->trafficsource = $trafficsource;

        return $this;
    }

    public function getUtmParameters(): ?string
    {
        return $this->utmParameters;
    }


    public function setUtmParameters(?string $utmParameters): self
    {
        $this->utmParameters = $utmParameters;

        return $this;
    }

    public function getSplitVersion(): ?string
    {
        return $this->splitVersion;
    }


    public function setSplitVersion(?string $splitVersion): self
    {
        $this->splitVersion = $splitVersion;

        return $this;
    }

    public function getTargetgroup(): ?string
    {
        return $this->targetgroup;
    }

    public function setTargetgroup(?string $targetgroup): self
    {
        $this->targetgroup = $targetgroup;

        return $this;
    }

    public function getAffiliateid(): ?string
    {
        return $this->affiliateid;
    }

    public function setAffiliateid(?string $affiliateid): self
    {
        $this->affiliateid = $affiliateid;

        return $this;
    }

    public function getAffiliatesubid(): ?string
    {
        return $this->affiliatesubid;
    }

    public function setAffiliatesubid(?string $affiliatesubid): self
    {
        $this->affiliatesubid = $affiliatesubid;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }


    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getDeviceType(): ?string
    {
        return $this->deviceType;
    }


    public function setDeviceType(?string $deviceType): self
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    public function getDeviceBrowser(): ?string
    {
        return $this->deviceBrowser;
    }


    public function setDeviceBrowser(?string $deviceBrowser): self
    {
        $this->deviceBrowser = $deviceBrowser;

        return $this;
    }

    public function getDeviceOs(): ?string
    {
        return $this->deviceOs;
    }


    public function setDeviceOs(?string $deviceOs): self
    {
        $this->deviceOs = $deviceOs;

        return $this;
    }

    public function getDeviceOsVersion(): ?string
    {
        return $this->deviceOsVersion;
    }


    public function setDeviceOsVersion(?string $deviceOsVersion): self
    {
        $this->deviceOsVersion = $deviceOsVersion;

        return $this;
    }

    public function getDeviceBrowserVersion(): ?string
    {
        return $this->deviceBrowserVersion;
    }


    public function setDeviceBrowserVersion(?string $deviceBrowserVersion): self
    {
        $this->deviceBrowserVersion = $deviceBrowserVersion;

        return $this;
    }


    public function getHashEmail(): ?string
    {
        return $this->hashEmail;
    }


    public function setHashEmail(string $hashEmail): self
    {
        $this->hashEmail = $hashEmail;

        return $this;
    }


    public function getHashSalutation(): ?string
    {
        return $this->hashSalutation;
    }


    public function setHashSalutation(?string $hashSalutation): self
    {
        $this->hashSalutation = $hashSalutation;

        return $this;
    }


    public function getHashFirstName(): ?string
    {
        return $this->hashFirstName;
    }


    public function setHashFirstName(?string $hashFirstName): self
    {
        $this->hashFirstName = $hashFirstName;

        return $this;
    }


    public function getHashLastName(): ?string
    {
        return $this->hashLastName;
    }


    public function setHashLastName(?string $hashLastName): self
    {
        $this->hashLastName = $hashLastName;

        return $this;
    }


    public function getHashFirma(): ?string
    {
        return $this->hashFirma;
    }


    public function setHashFirma(?string $hashFirma): self
    {
        $this->hashFirma = $hashFirma;

        return $this;
    }


    public function getHashStreet(): ?string
    {
        return $this->hashStreet;
    }


    public function setHashStreet(?string $hashStreet): self
    {
        $this->hashStreet = $hashStreet;

        return $this;
    }


    public function getHashPostalCode(): ?string
    {
        return $this->hashPostalCode;
    }


    public function setHashPostalCode(?string $hashPostalCode): self
    {
        $this->hashPostalCode = $hashPostalCode;

        return $this;
    }


    public function getHashCity(): ?string
    {
        return $this->hashCity;
    }


    public function setHashCity(?string $hashCity): self
    {
        $this->hashCity = $hashCity;

        return $this;
    }


    public function getHashCountry(): ?string
    {
        return $this->hashCountry;
    }


    public function setHashCountry(?string $hashCountry): self
    {
        $this->hashCountry = $hashCountry;

        return $this;
    }


    public function getHashPhone(): ?string
    {
        return $this->hashPhone;
    }


    public function setHashPhone(?string $hashPhone): self
    {
        $this->hashPhone = $hashPhone;

        return $this;
    }


    public function getHashCampaign(): ?string
    {
        return $this->hashCampaign;
    }


    public function setHashCampaign(?string $hashCampaign): self
    {
        $this->hashCampaign = $hashCampaign;

        return $this;
    }


    public function getHashLeadReference(): ?string
    {
        return $this->hashLeadReference;
    }


    public function setHashLeadReference(?string $hashLeadReference): self
    {
        $this->hashLeadReference = $hashLeadReference;

        return $this;
    }


    public function getHashUrl(): ?string
    {
        return $this->hashUrl;
    }


    public function setHashUrl(?string $hashUrl): self
    {
        $this->hashUrl = $hashUrl;

        return $this;
    }


    public function getHashFinalUrl(): ?string
    {
        return $this->hashFinalUrl;
    }


    public function setHashFinalUrl(?string $hashFinalUrl): self
    {
        $this->hashFinalUrl = $hashFinalUrl;

        return $this;
    }


    public function getHashTrafficsource(): ?string
    {
        return $this->hashTrafficsource;
    }


    public function setHashTrafficsource(?string $hashTrafficsource): self
    {
        $this->hashTrafficsource = $hashTrafficsource;

        return $this;
    }


    public function getHashUtmParameters(): ?string
    {
        return $this->hashUtmParameters;
    }


    public function setHashUtmParameters(?string $hashUtmParameters): self
    {
        $this->hashUtmParameters = $hashUtmParameters;

        return $this;
    }


    public function getHashSplitVersion(): ?string
    {
        return $this->hashSplitVersion;
    }


    public function setHashSplitVersion(?string $hashSplitVersion): self
    {
        $this->hashSplitVersion = $hashSplitVersion;

        return $this;
    }


    public function getHashTargetgroup(): ?string
    {
        return $this->hashTargetgroup;
    }


    public function setHashTargetgroup(?string $hashTargetgroup): self
    {
        $this->hashTargetgroup = $hashTargetgroup;

        return $this;
    }


    public function getHashAffiliateid(): ?string
    {
        return $this->hashAffiliateid;
    }


    public function setHashAffiliateid(?string $hashAffiliateid): self
    {
        $this->hashAffiliateid = $hashAffiliateid;

        return $this;
    }


    public function getHashAffiliatesubid(): ?string
    {
        return $this->hashAffiliatesubid;
    }


    public function setHashAffiliatesubid(?string $hashAffiliatesubid): self
    {
        $this->hashAffiliatesubid = $hashAffiliatesubid;

        return $this;
    }


    public function getHashIp(): ?string
    {
        return $this->hashIp;
    }


    public function setHashIp(?string $hashIp): self
    {
        $this->hashIp = $hashIp;

        return $this;
    }


    public function getHashDeviceType(): ?string
    {
        return $this->hashDeviceType;
    }


    public function setHashDeviceType(?string $hashDeviceType): self
    {
        $this->hashDeviceType = $hashDeviceType;

        return $this;
    }


    public function getHashDeviceBrowser(): ?string
    {
        return $this->hashDeviceBrowser;
    }


    public function setHashDeviceBrowser(?string $hashDeviceBrowser): self
    {
        $this->hashDeviceBrowser = $hashDeviceBrowser;

        return $this;
    }


    public function getHashDeviceOs(): ?string
    {
        return $this->hashDeviceOs;
    }


    public function setHashDeviceOs(?string $hashDeviceOs): self
    {
        $this->hashDeviceOs = $hashDeviceOs;

        return $this;
    }


    public function getHashDeviceOsVersion(): ?string
    {
        return $this->hashDeviceOsVersion;
    }


    public function setHashDeviceOsVersion(?string $hashDeviceOsVersion): self
    {
        $this->hashDeviceOsVersion = $hashDeviceOsVersion;

        return $this;
    }


    public function getHashDeviceBrowserVersion(): ?string
    {
        return $this->hashDeviceBrowserVersion;
    }


    public function setHashDeviceBrowserVersion(?string $hashDeviceBrowserVersion): self
    {
        $this->hashDeviceBrowserVersion = $hashDeviceBrowserVersion;

        return $this;
    }


    public function getModification(): ?string
    {
        return $this->modification;
    }


    public function setModification(?string $modification): self
    {
        $this->modification = $modification;

        return $this;
    }


    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }


    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }


    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }


    public function getCampaign2(): ?Campaign
    {
        return $this->campaign2;
    }


    public function setCampaign2(?Campaign $campaign2): self
    {
        $this->campaign2 = $campaign2;

        return $this;
    }

    public function getObjectregister(): ?Objectregister
    {
        return $this->objectregister;
    }

    public function setObjectregister(?Objectregister $objectregister): self
    {
        $this->objectregister = $objectregister;

        return $this;
    }


}
