<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Miostandardfield
 *
 * @ORM\Table(name="miostandardfield", indexes={@ORM\Index(name="fk_standardfields_datatypes1_idx", columns={"datatypes_id"})})
 * @ORM\Entity
 */
class Miostandardfield
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldname", type="string", length=255, nullable=false)
     */
    private $fieldname;

    /**
     * @var string
     *
     * @ORM\Column(name="htmllabel", type="string", length=255, nullable=false)
     */
    private $htmllabel;

    /**
     * @var \Datatypes
     *
     * @ORM\ManyToOne(targetEntity="Datatypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="datatypes_id", referencedColumnName="id")
     * })
     */
    private $datatypes;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getFieldname(): ?string
    {
        return $this->fieldname;
    }

    public function setFieldname(string $fieldname): self
    {
        $this->fieldname = $fieldname;

        return $this;
    }

    public function getHtmllabel(): ?string
    {
        return $this->htmllabel;
    }

    public function setHtmllabel(string $htmllabel): self
    {
        $this->htmllabel = $htmllabel;

        return $this;
    }

    public function getDatatypes(): ?Datatypes
    {
        return $this->datatypes;
    }

    public function setDatatypes(?Datatypes $datatypes): self
    {
        $this->datatypes = $datatypes;

        return $this;
    }


}
