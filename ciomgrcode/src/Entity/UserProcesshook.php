<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserProcesshook
 *
 * @ORM\Table(name="user_processhook", uniqueConstraints={@ORM\UniqueConstraint(name="uk_id_name", columns={"processhook_name", "processhook_id"})}, indexes={@ORM\Index(name="fk_user_processhook_campaign11_idx", columns={"project_campaign_id"}), @ORM\Index(name="fk_user_processhook_processhook11_idx", columns={"processhook_id"}), @ORM\Index(name="fk_objectregister11_idx", columns={"objectregister_id"})})
 * @ORM\Entity
 */
class UserProcesshook
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="processhook_name", type="string", length=255, nullable=false)
     */
    private $processhookName;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true)
     */
    private $createdBy;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true)
     */
    private $updatedBy;

    /**
     * @var \Objectregister
     *
     * @ORM\ManyToOne(targetEntity="Objectregister")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="objectregister_id", referencedColumnName="id")
     * })
     */
    private $objectregister;

    /**
     * @var \Campaign
     *
     * @ORM\ManyToOne(targetEntity="Campaign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_campaign_id", referencedColumnName="id")
     * })
     */
    private $projectCampaign;

    /**
     * @var \Processhook
     *
     * @ORM\ManyToOne(targetEntity="Processhook")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processhook_id", referencedColumnName="id")
     * })
     */
    private $processhook;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getProcesshookName(): ?string
    {
        return $this->processhookName;
    }

    public function setProcesshookName(string $processhookName): self
    {
        $this->processhookName = $processhookName;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getObjectregister(): ?Objectregister
    {
        return $this->objectregister;
    }

    public function setObjectregister(?Objectregister $objectregister): self
    {
        $this->objectregister = $objectregister;

        return $this;
    }

    public function getProjectCampaign(): ?Campaign
    {
        return $this->projectCampaign;
    }

    public function setProjectCampaign(?Campaign $projectCampaign): self
    {
        $this->projectCampaign = $projectCampaign;

        return $this;
    }

    public function getProcesshook(): ?Processhook
    {
        return $this->processhook;
    }

    public function setProcesshook(?Processhook $processhook): self
    {
        $this->processhook = $processhook;

        return $this;
    }


}
