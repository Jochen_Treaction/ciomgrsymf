<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Questions
 *
 * @ORM\Table(name="questions", indexes={@ORM\Index(name="fk_questions_context1_idx", columns={"context_id"}), @ORM\Index(name="fk_questions_question_types1_idx", columns={"question_types_id"}), @ORM\Index(name="fk_questions_survey1_idx", columns={"survey_id"})})
 * @ORM\Entity
 */
class Questions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question_question_txt", type="text", length=65535, nullable=false, options={"comment"="the survey question"})
     */
    private $questionQuestionTxt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="question_hint_txt", type="text", length=65535, nullable=true, options={"comment"="frontend: text hint for  leads about the question and how to answer it ..."})
     */
    private $questionHintTxt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="question_json_representation", type="text", length=16777215, nullable=true, options={"comment"="MEDIUMTEXT must be changed to JSON datatype in modern MySql Version"})
     */
    private $questionJsonRepresentation;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true,"comment"="decoupled users.id"})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true,"comment"="decoupled users.id"})
     */
    private $updatedBy;

    /**
     * @var \QuestionContext
     *
     * @ORM\ManyToOne(targetEntity="QuestionContext")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="context_id", referencedColumnName="id")
     * })
     */
    private $context;

    /**
     * @var \QuestionTypes
     *
     * @ORM\ManyToOne(targetEntity="QuestionTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="question_types_id", referencedColumnName="id")
     * })
     */
    private $questionTypes;

    /**
     * @var \Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="survey_id", referencedColumnName="id")
     * })
     */
    private $survey;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getQuestionQuestionTxt(): ?string
    {
        return $this->questionQuestionTxt;
    }

    public function setQuestionQuestionTxt(string $questionQuestionTxt): self
    {
        $this->questionQuestionTxt = $questionQuestionTxt;

        return $this;
    }

    public function getQuestionHintTxt(): ?string
    {
        return $this->questionHintTxt;
    }

    public function setQuestionHintTxt(?string $questionHintTxt): self
    {
        $this->questionHintTxt = $questionHintTxt;

        return $this;
    }

    public function getQuestionJsonRepresentation(): ?string
    {
        return $this->questionJsonRepresentation;
    }

    public function setQuestionJsonRepresentation(?string $questionJsonRepresentation): self
    {
        $this->questionJsonRepresentation = $questionJsonRepresentation;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }


    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getContext(): ?QuestionContext
    {
        return $this->context;
    }

    public function setContext(?QuestionContext $context): self
    {
        $this->context = $context;

        return $this;
    }

    public function getQuestionTypes(): ?QuestionTypes
    {
        return $this->questionTypes;
    }

    public function setQuestionTypes(?QuestionTypes $questionTypes): self
    {
        $this->questionTypes = $questionTypes;

        return $this;
    }

    public function getSurvey(): ?Survey
    {
        return $this->survey;
    }

    public function setSurvey(?Survey $survey): self
    {
        $this->survey = $survey;

        return $this;
    }


}
