<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LeadAnswerContent
 *
 * @ORM\Table(name="lead_answer_content", uniqueConstraints={@ORM\UniqueConstraint(name="uidx_leads_questions", columns={"leads_id", "questions_id"})}, indexes={@ORM\Index(name="fk_lac_questions_idx", columns={"questions_id"}), @ORM\Index(name="IDX_F660167E747817FD", columns={"leads_id"})})
 * @ORM\Entity
 */
class LeadAnswerContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lead_answer_content", type="text", length=0, nullable=true)
     */
    private $leadAnswerContent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lead_answer_score", type="decimal", precision=20, scale=0, nullable=true)
     */
    private $leadAnswerScore = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_lead_answer_content", type="string", length=128, nullable=true)
     */
    private $hashLeadAnswerContent;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var \Leads
     *
     * @ORM\ManyToOne(targetEntity="Leads")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="leads_id", referencedColumnName="id")
     * })
     */
    private $leads;

    /**
     * @var \Questions
     *
     * @ORM\ManyToOne(targetEntity="Questions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questions_id", referencedColumnName="id")
     * })
     */
    private $questions;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getLeadAnswerContent(): ?string
    {
        return $this->leadAnswerContent;
    }

    public function setLeadAnswerContent(?string $leadAnswerContent): self
    {
        $this->leadAnswerContent = $leadAnswerContent;

        return $this;
    }


    public function getLeadAnswerScore(): ?string
    {
        return $this->leadAnswerScore;
    }


    public function setLeadAnswerScore(?string $leadAnswerScore): self
    {
        $this->leadAnswerScore = $leadAnswerScore;

        return $this;
    }


    public function getHashLeadAnswerContent(): ?string
    {
        return $this->hashLeadAnswerContent;
    }


    public function setHashLeadAnswerContent(?string $hashLeadAnswerContent): self
    {
        $this->hashLeadAnswerContent = $hashLeadAnswerContent;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }


    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }


    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    public function getLeads(): ?Leads
    {
        return $this->leads;
    }


    public function setLeads(?Leads $leads): self
    {
        $this->leads = $leads;

        return $this;
    }

    public function getQuestions(): ?Questions
    {
        return $this->questions;
    }

    public function setQuestions(?Questions $questions): self
    {
        $this->questions = $questions;

        return $this;
    }


}
