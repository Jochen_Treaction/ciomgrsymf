<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserProcesshookHasWorkflow
 *
 * @ORM\Table(name="user_processhook_has_workflow", indexes={@ORM\Index(name="fk_user_processhook_has_workflow_user_processhook1_idx", columns={"user_processhook_id"}), @ORM\Index(name="fk_successor_idx", columns={"successor_uph_id"}), @ORM\Index(name="fk_user_processhook_has_workflow_workflow1_idx", columns={"workflow_id"}), @ORM\Index(name="fk_predessor_idx", columns={"predecessor_uph_id"})})
 * @ORM\Entity
 */
class UserProcesshookHasWorkflow
{
    /**
     * @var \UserProcesshook
     *
     * @ORM\ManyToOne(targetEntity="UserProcesshook")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="predecessor_uph_id", referencedColumnName="id")
     * })
     */
    private $predecessorUph;

    /**
     * @var \UserProcesshook
     *
     * @ORM\ManyToOne(targetEntity="UserProcesshook")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="successor_uph_id", referencedColumnName="id")
     * })
     */
    private $successorUph;

    /**
     * @var \UserProcesshook
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="UserProcesshook")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_processhook_id", referencedColumnName="id")
     * })
     */
    private $userProcesshook;

    /**
     * @var \Workflow
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Workflow")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workflow_id", referencedColumnName="id")
     * })
     */
    private $workflow;

    public function getPredecessorUph(): ?UserProcesshook
    {
        return $this->predecessorUph;
    }

    public function setPredecessorUph(?UserProcesshook $predecessorUph): self
    {
        $this->predecessorUph = $predecessorUph;

        return $this;
    }

    public function getSuccessorUph(): ?UserProcesshook
    {
        return $this->successorUph;
    }

    public function setSuccessorUph(?UserProcesshook $successorUph): self
    {
        $this->successorUph = $successorUph;

        return $this;
    }

    public function getUserProcesshook(): ?UserProcesshook
    {
        return $this->userProcesshook;
    }

    public function setUserProcesshook(?UserProcesshook $userProcesshook): self
    {
        $this->userProcesshook = $userProcesshook;

        return $this;
    }

    public function getWorkflow(): ?Workflow
    {
        return $this->workflow;
    }

    public function setWorkflow(?Workflow $workflow): self
    {
        $this->workflow = $workflow;

        return $this;
    }


}
