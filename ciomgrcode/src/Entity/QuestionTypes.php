<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuestionTypes
 *
 * @ORM\Table(name="question_types")
 * @ORM\Entity
 */
class QuestionTypes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="question_type_html_tag", type="string", length=40, nullable=false, options={"comment"="inital values for html_tag:  select,  range, textarea, checkbox"})
     */
    private $questionTypeHtmlTag;

    /**
     * @var string|null
     *
     * @ORM\Column(name="question_type_html_template", type="text", length=16777215, nullable=true, options={"comment"="optional pre-defined html template for select,  checkboxes and so on"})
     */
    private $questionTypeHtmlTemplate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="question_type_frontend_hint", type="string", length=255, nullable=true, options={"comment"="human synonmys for html tags to show in frontend\\nselect => Dropdown-list\\nrange => Slider\\ntextarea => Freitext-Antwort\\ncheckbox => Fragenkatalog"})
     */
    private $questionTypeFrontendHint;

    /**
     * @var string|null
     *
     * @ORM\Column(name="question_type_description", type="text", length=65535, nullable=true)
     */
    private $questionTypeDescription;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true,"comment"="decoupled users.id"})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updatedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true,"comment"="decoupled users.id"})
     */
    private $updatedBy;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getQuestionTypeHtmlTag(): ?string
    {
        return $this->questionTypeHtmlTag;
    }

    public function setQuestionTypeHtmlTag(string $questionTypeHtmlTag): self
    {
        $this->questionTypeHtmlTag = $questionTypeHtmlTag;

        return $this;
    }

    public function getQuestionTypeHtmlTemplate(): ?string
    {
        return $this->questionTypeHtmlTemplate;
    }

    public function setQuestionTypeHtmlTemplate(?string $questionTypeHtmlTemplate): self
    {
        $this->questionTypeHtmlTemplate = $questionTypeHtmlTemplate;

        return $this;
    }

    public function getQuestionTypeFrontendHint(): ?string
    {
        return $this->questionTypeFrontendHint;
    }

    public function setQuestionTypeFrontendHint(?string $questionTypeFrontendHint): self
    {
        $this->questionTypeFrontendHint = $questionTypeFrontendHint;

        return $this;
    }

    public function getQuestionTypeDescription(): ?string
    {
        return $this->questionTypeDescription;
    }

    public function setQuestionTypeDescription(?string $questionTypeDescription): self
    {
        $this->questionTypeDescription = $questionTypeDescription;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }


    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }


}
