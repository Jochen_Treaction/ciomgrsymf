<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Processhook
 *
 * @ORM\Table(name="processhook", indexes={@ORM\Index(name="fk_event_idx", columns={"is_triggered_by_event_id"}), @ORM\Index(name="fk_processhook_input_datainterface1_idx", columns={"input_datainterface_id"}), @ORM\Index(name="fk_processhook_action1_idx", columns={"action_id"}), @ORM\Index(name="fk_processhook_switch_definition1_idx", columns={"switch_definition_id"}), @ORM\Index(name="fk_processhook_output_datainterface1_idx", columns={"output_datainterface_id"})})
 * @ORM\Entity
 */
class Processhook
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="acts", type="string", length=0, nullable=true, options={"default"="serverside","comment"="possible values serverside or clientsside"})
     */
    private $acts = 'serverside';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="execution_status", type="string", length=45, nullable=true)
     */
    private $executionStatus;

    /**
     * @var string|null
     *
     * @ORM\Column(name="on_error_instruction", type="string", length=45, nullable=true)
     */
    private $onErrorInstruction;

    /**
     * @var \Event
     *
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="is_triggered_by_event_id", referencedColumnName="id")
     * })
     */
    private $isTriggeredByEvent;

    /**
     * @var \Action
     *
     * @ORM\ManyToOne(targetEntity="Action")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     * })
     */
    private $action;

    /**
     * @var \InputDatainterface
     *
     * @ORM\ManyToOne(targetEntity="InputDatainterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="input_datainterface_id", referencedColumnName="id")
     * })
     */
    private $inputDatainterface;

    /**
     * @var \OutputDatainterface
     *
     * @ORM\ManyToOne(targetEntity="OutputDatainterface")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="output_datainterface_id", referencedColumnName="id")
     * })
     */
    private $outputDatainterface;

    /**
     * @var \SwitchDefinition
     *
     * @ORM\ManyToOne(targetEntity="SwitchDefinition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="switch_definition_id", referencedColumnName="id")
     * })
     */
    private $switchDefinition;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getActs(): ?string
    {
        return $this->acts;
    }

    public function setActs(?string $acts): self
    {
        $this->acts = $acts;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getExecutionStatus(): ?string
    {
        return $this->executionStatus;
    }

    public function setExecutionStatus(?string $executionStatus): self
    {
        $this->executionStatus = $executionStatus;

        return $this;
    }

    public function getOnErrorInstruction(): ?string
    {
        return $this->onErrorInstruction;
    }

    public function setOnErrorInstruction(?string $onErrorInstruction): self
    {
        $this->onErrorInstruction = $onErrorInstruction;

        return $this;
    }

    public function getIsTriggeredByEvent(): ?Event
    {
        return $this->isTriggeredByEvent;
    }

    public function setIsTriggeredByEvent(?Event $isTriggeredByEvent): self
    {
        $this->isTriggeredByEvent = $isTriggeredByEvent;

        return $this;
    }

    public function getAction(): ?Action
    {
        return $this->action;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getInputDatainterface(): ?InputDatainterface
    {
        return $this->inputDatainterface;
    }

    public function setInputDatainterface(?InputDatainterface $inputDatainterface): self
    {
        $this->inputDatainterface = $inputDatainterface;

        return $this;
    }

    public function getOutputDatainterface(): ?OutputDatainterface
    {
        return $this->outputDatainterface;
    }

    public function setOutputDatainterface(?OutputDatainterface $outputDatainterface): self
    {
        $this->outputDatainterface = $outputDatainterface;

        return $this;
    }

    public function getSwitchDefinition(): ?SwitchDefinition
    {
        return $this->switchDefinition;
    }

    public function setSwitchDefinition(?SwitchDefinition $switchDefinition): self
    {
        $this->switchDefinition = $switchDefinition;

        return $this;
    }


}
