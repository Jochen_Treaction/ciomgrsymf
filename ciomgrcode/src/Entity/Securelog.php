<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Securelog
 *
 * @ORM\Table(name="securelog", indexes={@ORM\Index(name="company_id", columns={"company_id"}), @ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class Securelog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false, options={"default"="1","unsigned"=true})
     */
    private $userId = '1';

    /**
     * @var int|null
     *
     * @ORM\Column(name="company_id", type="bigint", nullable=true, options={"unsigned"=true,"comment"="delete that col since ref tab users has company_id"})
     */
    private $companyId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="logindate", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $logindate = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=false)
     */
    private $ip = '';

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", length=0, nullable=false, options={"default"="success"})
     */
    private $result = 'success';

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getCompanyId(): ?string
    {
        return $this->companyId;
    }

    public function setCompanyId(?string $companyId): self
    {
        $this->companyId = $companyId;

        return $this;
    }

    public function getLogindate(): ?\DateTimeInterface
    {
        return $this->logindate;
    }

    public function setLogindate(\DateTimeInterface $logindate): self
    {
        $this->logindate = $logindate;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function setResult(string $result): self
    {
        $this->result = $result;

        return $this;
    }


}
