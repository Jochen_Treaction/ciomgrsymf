<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WorkflowProcesshooks
 *
 * @ORM\Table(name="workflow_processhooks", uniqueConstraints={@ORM\UniqueConstraint(name="uk_ph_wf", columns={"workflow_id", "processhook_id"})}, indexes={@ORM\Index(name="fk_workflow_processhooks_switch_definition1_idx", columns={"switch_definition_id"}), @ORM\Index(name="fk_processhook_has_workflow_workflow1_idx", columns={"workflow_id"}), @ORM\Index(name="fk_processhook_has_workflow_processhook2_idx1", columns={"predecessor_id"}), @ORM\Index(name="fk_processhook_has_workflow_processhook3_idx2", columns={"successor_id"}), @ORM\Index(name="fk_processhook_has_workflow_processhook1_idx", columns={"processhook_id"})})
 * @ORM\Entity
 */
class WorkflowProcesshooks
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Processhook
     *
     * @ORM\ManyToOne(targetEntity="Processhook")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processhook_id", referencedColumnName="id")
     * })
     */
    private $processhook;

    /**
     * @var \Processhook
     *
     * @ORM\ManyToOne(targetEntity="Processhook")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="predecessor_id", referencedColumnName="id")
     * })
     */
    private $predecessor;

    /**
     * @var \Processhook
     *
     * @ORM\ManyToOne(targetEntity="Processhook")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="successor_id", referencedColumnName="id")
     * })
     */
    private $successor;

    /**
     * @var \Workflow
     *
     * @ORM\ManyToOne(targetEntity="Workflow")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workflow_id", referencedColumnName="id")
     * })
     */
    private $workflow;

    /**
     * @var \SwitchDefinition
     *
     * @ORM\ManyToOne(targetEntity="SwitchDefinition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="switch_definition_id", referencedColumnName="id")
     * })
     */
    private $switchDefinition;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getProcesshook(): ?Processhook
    {
        return $this->processhook;
    }

    public function setProcesshook(?Processhook $processhook): self
    {
        $this->processhook = $processhook;

        return $this;
    }

    public function getPredecessor(): ?Processhook
    {
        return $this->predecessor;
    }

    public function setPredecessor(?Processhook $predecessor): self
    {
        $this->predecessor = $predecessor;

        return $this;
    }

    public function getSuccessor(): ?Processhook
    {
        return $this->successor;
    }

    public function setSuccessor(?Processhook $successor): self
    {
        $this->successor = $successor;

        return $this;
    }

    public function getWorkflow(): ?Workflow
    {
        return $this->workflow;
    }

    public function setWorkflow(?Workflow $workflow): self
    {
        $this->workflow = $workflow;

        return $this;
    }

    public function getSwitchDefinition(): ?SwitchDefinition
    {
        return $this->switchDefinition;
    }

    public function setSwitchDefinition(?SwitchDefinition $switchDefinition): self
    {
        $this->switchDefinition = $switchDefinition;

        return $this;
    }


}
