<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LeadSyncLog
 *
 * @ORM\Table(name="lead_sync_log", indexes={@ORM\Index(name="lead_sync_log_integrations_id_fk", columns={"integration_id"})})
 * @ORM\Entity
 */
class LeadSyncLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="settings", type="text", length=16777215, nullable=false)
     */
    private $settings;

    /**
     * @var int|null
     *
     * @ORM\Column(name="no_of_contacts_synchronized", type="integer", nullable=true)
     */
    private $noOfContactsSynchronized;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_sync_date", type="datetime", nullable=false)
     */
    private $lastSyncDate;

    /**
     * @var int
     *
     * @ORM\Column(name="last_sync_contact_id", type="integer", nullable=false)
     */
    private $lastSyncContactId;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="message", type="string", length=255, nullable=true)
     */
    private $message;

    /**
     * @var \Integrations
     *
     * @ORM\ManyToOne(targetEntity="Integrations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="integration_id", referencedColumnName="id")
     * })
     */
    private $integration;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getSettings(): ?string
    {
        return $this->settings;
    }

    public function setSettings(string $settings): self
    {
        $this->settings = $settings;

        return $this;
    }

    public function getNoOfContactsSynchronized(): ?int
    {
        return $this->noOfContactsSynchronized;
    }

    public function setNoOfContactsSynchronized(?int $noOfContactsSynchronized): self
    {
        $this->noOfContactsSynchronized = $noOfContactsSynchronized;

        return $this;
    }

    public function getLastSyncDate(): ?\DateTimeInterface
    {
        return $this->lastSyncDate;
    }

    public function setLastSyncDate(\DateTimeInterface $lastSyncDate): self
    {
        $this->lastSyncDate = $lastSyncDate;

        return $this;
    }

    public function getLastSyncContactId(): ?int
    {
        return $this->lastSyncContactId;
    }

    public function setLastSyncContactId(int $lastSyncContactId): self
    {
        $this->lastSyncContactId = $lastSyncContactId;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getIntegration(): ?Integrations
    {
        return $this->integration;
    }

    public function setIntegration(?Integrations $integration): self
    {
        $this->integration = $integration;

        return $this;
    }


}
