<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fraud
 *
 * @ORM\Table(name="fraud", indexes={@ORM\Index(name="idxfk_fraud_campaign_id", columns={"campaign_id"})})
 * @ORM\Entity
 */
class Fraud
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true, options={"comment"="IP Block, Blacklist Domain, Fraud Limit"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false, options={"comment"="Client email id"})
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="domain", type="string", length=255, nullable=true, options={"comment"="Client Domain name"})
     */
    private $domain;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=100, nullable=true, options={"comment"="Client Ip address"})
     */
    private $ip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="device_browser", type="string", length=255, nullable=true, options={"comment"="Client Browser"})
     */
    private $deviceBrowser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="device_os", type="string", length=255, nullable=true, options={"comment"="Client OS name "})
     */
    private $deviceOs;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_stamp", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP","comment"="Current time stamp"})
     */
    private $timeStamp = 'CURRENT_TIMESTAMP';

    /**
     * @var \Campaign
     *
     * @ORM\ManyToOne(targetEntity="Campaign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     * })
     */
    private $campaign;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(?string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getDeviceBrowser(): ?string
    {
        return $this->deviceBrowser;
    }

    public function setDeviceBrowser(?string $deviceBrowser): self
    {
        $this->deviceBrowser = $deviceBrowser;

        return $this;
    }

    public function getDeviceOs(): ?string
    {
        return $this->deviceOs;
    }

    public function setDeviceOs(?string $deviceOs): self
    {
        $this->deviceOs = $deviceOs;

        return $this;
    }

    public function getTimeStamp(): ?\DateTimeInterface
    {
        return $this->timeStamp;
    }

    public function setTimeStamp(\DateTimeInterface $timeStamp): self
    {
        $this->timeStamp = $timeStamp;

        return $this;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(?Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }


}
