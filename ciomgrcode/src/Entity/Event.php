<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ui_name", type="string", length=255, nullable=false)
     */
    private $uiName;

    /**
     * @var string
     *
     * @ORM\Column(name="attached_dom_element", type="string", length=255, nullable=false)
     */
    private $attachedDomElement;

    /**
     * @var string
     *
     * @ORM\Column(name="attached_js_event", type="string", length=255, nullable=false)
     */
    private $attachedJsEvent;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true, options={"comment"="add as comment what the fuck that bnutton is: button [submit] or button[custom1], button[custom2], button[custom3]"})
     */
    private $comment;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getUiName(): ?string
    {
        return $this->uiName;
    }

    public function setUiName(string $uiName): self
    {
        $this->uiName = $uiName;

        return $this;
    }

    public function getAttachedDomElement(): ?string
    {
        return $this->attachedDomElement;
    }

    public function setAttachedDomElement(string $attachedDomElement): self
    {
        $this->attachedDomElement = $attachedDomElement;

        return $this;
    }

    public function getAttachedJsEvent(): ?string
    {
        return $this->attachedJsEvent;
    }

    public function setAttachedJsEvent(string $attachedJsEvent): self
    {
        $this->attachedJsEvent = $attachedJsEvent;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }


}
