<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company", uniqueConstraints={@ORM\UniqueConstraint(name="uidx_account_no", columns={"account_no"})}, indexes={@ORM\Index(name="fk_company_objectregister_id_idx", columns={"objectregister_id"}), @ORM\Index(name="id", columns={"id"})})
 * @ORM\Entity
 */
class Company
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="account_no", type="string", length=100, nullable=false)
     */
    private $accountNo;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="street", type="text", length=65535, nullable=true)
     */
    private $street;

    /**
     * @var string|null
     *
     * @ORM\Column(name="house_number", type="text", length=65535, nullable=true)
     */
    private $houseNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="postal_code", type="text", length=65535, nullable=true)
     */
    private $postalCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="text", length=65535, nullable=true)
     */
    private $city;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country", type="text", length=65535, nullable=true)
     */
    private $country;

    /**
     * @var string|null
     *
     * @ORM\Column(name="website", type="text", length=65535, nullable=true)
     */
    private $website;


	/**
	 * @var string|null
	 *
	 * @ORM\Column(name="url_privacy_policy", type="text", length=255, nullable=true)
	 */
    private $urlPrivacyPolicy;

	/**
	 * @var string|null
	 *
	 * @ORM\Column(name="url_imprint", type="text", length=255, nullable=true)
	 */
    private $urlImprint;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="text", length=65535, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vat", type="text", length=65535, nullable=true)
     */
    private $vat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="login_url", type="text", length=65535, nullable=true)
     */
    private $loginUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="delegated_domain", type="text", length=65535, nullable=true, options={"comment"="Only when URL option is delegated domain"})
     */
    private $delegatedDomain;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url_option", type="string", length=255, nullable=true, options={"default"="folder","comment"="default was folder(must be removed for TEXT) , subdomain, deligate domain"})
     */
    private $urlOption = 'folder';

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_name", type="string", length=128, nullable=true)
     */
    private $hashName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_street", type="string", length=128, nullable=true)
     */
    private $hashStreet;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_house_number", type="string", length=128, nullable=true)
     */
    private $hashHouseNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_postal_code", type="string", length=128, nullable=true)
     */
    private $hashPostalCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_city", type="string", length=128, nullable=true)
     */
    private $hashCity;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_country", type="string", length=128, nullable=true)
     */
    private $hashCountry;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_website", type="string", length=128, nullable=true)
     */
    private $hashWebsite;


    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_url_privacy_policy", type="string", length=128, nullable=true)
     */
    private $hashUrlPrivacyPolicy;


    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_url_imprint", type="string", length=128, nullable=true)
     */
    private $hashUrlImprint;


    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_phone", type="string", length=128, nullable=true)
     */
    private $hashPhone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_vat", type="string", length=128, nullable=true)
     */
    private $hashVat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_login_url", type="string", length=128, nullable=true)
     */
    private $hashLoginUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hash_delegated_domain", type="string", length=128, nullable=true)
     */
    private $hashDelegatedDomain;

    /**
     * @var int|null
     *
     * @ORM\Column(name="modification", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $modification;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $created = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $updated = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \Objectregister
     *
     * @ORM\ManyToOne(targetEntity="Objectregister", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="objectregister_id", referencedColumnName="id")
     * })
     */
    private $objectregister;

//    /**
//     * @var \Doctrine\Common\Collections\Collection
//     *
//     * @ORM\ManyToMany(targetEntity="Accessconfigurations", inversedBy="company")
//     * @ORM\JoinTable(name="company_has_accessconfigurations",
//     *   joinColumns={
//     *     @ORM\JoinColumn(name="company_id", referencedColumnName="id")
//     *   },
//     *   inverseJoinColumns={
//     *     @ORM\JoinColumn(name="accessconfigurations_id", referencedColumnName="id")
//     *   }
//     * )
//     */
//    private $accessconfigurations;


    /**
     * Constructor
     */
    public function __construct()
    {
        // $this->accessconfigurations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAccountNo(): ?string
    {
        return $this->accountNo;
    }

    public function setAccountNo(string $accountNo): self
    {
        $this->accountNo = $accountNo;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }


	/**
	 * @return string|null
	 */
	public function getUrlImprint(): ?string
	{
		return $this->urlImprint;
	}


	/**
	 * @param string|null $urlImprint
	 */
	public function setUrlImprint(?string $urlImprint): self
	{
		$this->urlImprint = $urlImprint;
		return $this;
	}


	/**
	 * @return string|null
	 */
	public function getUrlPrivacyPolicy(): ?string
	{
		return $this->urlPrivacyPolicy;
	}


	/**
	 * @param string|null $urlPrivacyPolicy
	 */
	public function setUrlPrivacyPolicy(?string $urlPrivacyPolicy): self
	{
		$this->urlPrivacyPolicy = $urlPrivacyPolicy;
		return $this;
	}


    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getVat(): ?string
    {
        return $this->vat;
    }

    public function setVat(?string $vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    public function getLoginUrl(): ?string
    {
        return $this->loginUrl;
    }

    public function setLoginUrl(?string $loginUrl): self
    {
        $this->loginUrl = $loginUrl;

        return $this;
    }

    public function getDelegatedDomain(): ?string
    {
        return $this->delegatedDomain;
    }

    public function setDelegatedDomain(?string $delegatedDomain): self
    {
        $this->delegatedDomain = $delegatedDomain;

        return $this;
    }

    public function getUrlOption(): ?string
    {
        return $this->urlOption;
    }

    public function setUrlOption(?string $urlOption): self
    {
        $this->urlOption = $urlOption;

        return $this;
    }

    public function getHashName(): ?string
    {
        return $this->hashName;
    }

    public function setHashName(?string $hashName): self
    {
        $this->hashName = $hashName;

        return $this;
    }

    public function getHashStreet(): ?string
    {
        return $this->hashStreet;
    }

    public function setHashStreet(?string $hashStreet): self
    {
        $this->hashStreet = $hashStreet;

        return $this;
    }

    public function getHashHouseNumber(): ?string
    {
        return $this->hashHouseNumber;
    }

    public function setHashHouseNumber(?string $hashHouseNumber): self
    {
        $this->hashHouseNumber = $hashHouseNumber;

        return $this;
    }

    public function getHashPostalCode(): ?string
    {
        return $this->hashPostalCode;
    }

    public function setHashPostalCode(?string $hashPostalCode): self
    {
        $this->hashPostalCode = $hashPostalCode;

        return $this;
    }

    public function getHashCity(): ?string
    {
        return $this->hashCity;
    }

    public function setHashCity(?string $hashCity): self
    {
        $this->hashCity = $hashCity;

        return $this;
    }

    public function getHashCountry(): ?string
    {
        return $this->hashCountry;
    }

    public function setHashCountry(?string $hashCountry): self
    {
        $this->hashCountry = $hashCountry;

        return $this;
    }

    public function getHashWebsite(): ?string
    {
        return $this->hashWebsite;
    }

    public function setHashWebsite(?string $hashWebsite): self
    {
        $this->hashWebsite = $hashWebsite;

        return $this;
    }


	/**
	 * @return string|null
	 */
	public function getHashUrlImprint(): ?string
	{
		return $this->hashUrlImprint;
	}


	/**
	 * @param string|null $hashUrlImprint
	 */
	public function setHashUrlImprint(?string $hashUrlImprint): self
	{
		$this->hashUrlImprint = $hashUrlImprint;
		return $this;
	}


	/**
	 * @return string|null
	 */
	public function getHashUrlPrivacyPolicy(): ?string
	{
		return $this->hashUrlPrivacyPolicy;
	}


	/**
	 * @param string|null $hashUrlPrivacyPolicy
	 */
	public function setHashUrlPrivacyPolicy(?string $hashUrlPrivacyPolicy): self
	{
		$this->hashUrlPrivacyPolicy = $hashUrlPrivacyPolicy;
		return $this;
	}

    public function getHashPhone(): ?string
    {
        return $this->hashPhone;
    }

    public function setHashPhone(?string $hashPhone): self
    {
        $this->hashPhone = $hashPhone;

        return $this;
    }

    public function getHashVat(): ?string
    {
        return $this->hashVat;
    }

    public function setHashVat(?string $hashVat): self
    {
        $this->hashVat = $hashVat;

        return $this;
    }

    public function getHashLoginUrl(): ?string
    {
        return $this->hashLoginUrl;
    }

    public function setHashLoginUrl(?string $hashLoginUrl): self
    {
        $this->hashLoginUrl = $hashLoginUrl;

        return $this;
    }

    public function getHashDelegatedDomain(): ?string
    {
        return $this->hashDelegatedDomain;
    }

    public function setHashDelegatedDomain(?string $hashDelegatedDomain): self
    {
        $this->hashDelegatedDomain = $hashDelegatedDomain;

        return $this;
    }

    public function getModification(): ?string
    {
        return $this->modification;
    }

    public function setModification(?string $modification): self
    {
        $this->modification = $modification;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreatedBy(): ?int
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?int $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdatedBy(): ?int
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?int $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getObjectregister(): ?Objectregister
    {
        return $this->objectregister;
    }

    public function setObjectregister(?Objectregister $objectregister): self
    {
        $this->objectregister = $objectregister;

        return $this;
    }

//    /**
//     * @return Collection|Accessconfigurations[]
//     */
//    public function getAccessconfigurations(): Collection
//    {
//        return $this->accessconfigurations;
//    }
//
//    public function addAccessconfiguration(Accessconfigurations $accessconfiguration): self
//    {
//        if (!$this->accessconfigurations->contains($accessconfiguration)) {
//            $this->accessconfigurations[] = $accessconfiguration;
//        }
//
//        return $this;
//    }
//
//    public function removeAccessconfiguration(Accessconfigurations $accessconfiguration): self
//    {
//        $this->accessconfigurations->removeElement($accessconfiguration);
//
//        return $this;
//    }

}
