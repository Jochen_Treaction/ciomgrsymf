<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Accessconfigurations
 *
 * @ORM\Table(name="accessconfigurations", indexes={@ORM\Index(name="fk_mailsystems_id_idx", columns={"mailsystems_id"})})
 * @ORM\Entity
 */
class Accessconfigurations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mailsystemaccountname", type="string", length=128, nullable=false, options={"default"="not defined"})
     */
    private $mailsystemaccountname = 'not defined';

    /**
     * @var string|null
     *
     * @ORM\Column(name="apitype", type="string", length=128, nullable=true, options={"comment"="RestApi, SOAP, XML and so on"})
     */
    private $apitype;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apikey", type="text", length=65535, nullable=true)
     */
    private $apikey;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apikeyhash", type="string", length=128, nullable=true)
     */
    private $apikeyhash;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="valid_from", type="datetime", nullable=true)
     */
    private $validFrom;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="valid_to", type="datetime", nullable=true)
     */
    private $validTo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=true)
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="bigint", nullable=true)
     */
    private $updatedBy;

    /**
     * @var \Mailsystems
     *
     * @ORM\ManyToOne(targetEntity="Mailsystems")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mailsystems_id", referencedColumnName="id")
     * })
     */
    private $mailsystems;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Company", mappedBy="accessconfigurations")
     */
    private $company;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->company = new \Doctrine\Common\Collections\ArrayCollection();
    }


    public function getId(): ?string
    {
        return $this->id;
    }


    public function getMailsystemaccountname(): ?string
    {
        return $this->mailsystemaccountname;
    }


    public function setMailsystemaccountname(string $mailsystemaccountname): self
    {
        $this->mailsystemaccountname = $mailsystemaccountname;

        return $this;
    }


    public function getApitype(): ?string
    {
        return $this->apitype;
    }


    public function setApitype(?string $apitype): self
    {
        $this->apitype = $apitype;

        return $this;
    }


    public function getApikey(): ?string
    {
        return $this->apikey;
    }


    public function setApikey(?string $apikey): self
    {
        $this->apikey = $apikey;

        return $this;
    }


    public function getApikeyhash(): ?string
    {
        return $this->apikeyhash;
    }


    public function setApikeyhash(?string $apikeyhash): self
    {
        $this->apikeyhash = $apikeyhash;

        return $this;
    }


    public function getValidFrom(): ?\DateTimeInterface
    {
        return $this->validFrom;
    }


    public function setValidFrom(?\DateTimeInterface $validFrom): self
    {
        $this->validFrom = $validFrom;

        return $this;
    }


    public function getValidTo(): ?\DateTimeInterface
    {
        return $this->validTo;
    }


    public function setValidTo(?\DateTimeInterface $validTo): self
    {
        $this->validTo = $validTo;

        return $this;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }


    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }


    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }


    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }


    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }


    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }


    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }


    public function setUpdatedBy(?string $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }


    public function getMailsystems(): ?Mailsystems
    {
        return $this->mailsystems;
    }


    public function setMailsystems(?Mailsystems $mailsystems): self
    {
        $this->mailsystems = $mailsystems;

        return $this;
    }


    /**
     * @return Collection|Company[]
     */
    public function getCompany(): Collection
    {
        return $this->company;
    }


    public function addCompany(Company $company): self
    {
        if (!$this->company->contains($company)) {
            $this->company[] = $company;
            $company->addAccessconfiguration($this);
        }

        return $this;
    }


    public function removeCompany(Company $company): self
    {
        if ($this->company->contains($company)) {
            $this->company->removeElement($company);
            $company->removeAccessconfiguration($this);
        }

        return $this;
    }

}
