<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Standardfieldmapping
 *
 * @ORM\Table(name="standardfieldmapping", indexes={@ORM\Index(name="fk_standardfieldmapping_system1_idx", columns={"system_id"}), @ORM\Index(name="fk_standardfieldmapping_miostandardfield1_idx", columns={"miostandardfield_id"})})
 * @ORM\Entity
 */
class Standardfieldmapping
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="datatype", type="string", length=255, nullable=true)
     */
    private $datatype;

    /**
     * @var \Miostandardfield
     *
     * @ORM\ManyToOne(targetEntity="Miostandardfield")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="miostandardfield_id", referencedColumnName="id")
     * })
     */
    private $miostandardfield;

    /**
     * @var \System
     *
     * @ORM\ManyToOne(targetEntity="System")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="system_id", referencedColumnName="id")
     * })
     */
    private $system;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDatatype(): ?string
    {
        return $this->datatype;
    }

    public function setDatatype(?string $datatype): self
    {
        $this->datatype = $datatype;

        return $this;
    }

    public function getMiostandardfield(): ?Miostandardfield
    {
        return $this->miostandardfield;
    }

    public function setMiostandardfield(?Miostandardfield $miostandardfield): self
    {
        $this->miostandardfield = $miostandardfield;

        return $this;
    }

    public function getSystem(): ?System
    {
        return $this->system;
    }

    public function setSystem(?System $system): self
    {
        $this->system = $system;

        return $this;
    }


}
