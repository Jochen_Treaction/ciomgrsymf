<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ObjectRegisterMeta
 *
 * @ORM\Table(name="object_register_meta", indexes={@ORM\Index(name="fk_object_register_meta_object_register1_idx", columns={"object_register_id"})})
 * @ORM\Entity
 */
class ObjectRegisterMeta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_key", type="string", length=190, nullable=false)
     */
    private $metaKey;

    /**
     * @var string|null
     *
     * @ORM\Column(name="meta_value", type="text", length=16777215, nullable=true)
     */
    private $metaValue;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private $updatedBy;

    /**
     * @var \Objectregister
     *
     * @ORM\ManyToOne(targetEntity="Objectregister")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="object_register_id", referencedColumnName="id")
     * })
     */
    private $objectRegister;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getMetaKey(): ?string
    {
        return $this->metaKey;
    }

    public function setMetaKey(string $metaKey): self
    {
        $this->metaKey = $metaKey;

        return $this;
    }

    public function getMetaValue(): ?string
    {
        return $this->metaValue;
    }

    public function setMetaValue(?string $metaValue): self
    {
        $this->metaValue = $metaValue;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?string $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getObjectRegister(): ?Objectregister
    {
        return $this->objectRegister;
    }

    public function setObjectRegister(?Objectregister $objectRegister): self
    {
        $this->objectRegister = $objectRegister;

        return $this;
    }


}
