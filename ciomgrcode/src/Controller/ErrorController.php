<?php

namespace App\Controller;

use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

/**
 * used to capture errors and expose them in a defined template templates/bundles/TwigBundle/Exception/error.html.twig
 */
class ErrorController extends AbstractController
{
	/**
	 * used config/packages/framework.yaml
	 * @param FlattenException          $exception
	 * @param DebugLoggerInterface|null $logger
	 */
	public function showerr(FlattenException $exception, DebugLoggerInterface $logger = null) // do not add a return type
	{
		return $this->render('bundles/TwigBundle/Exception/error.html.twig', [
			"code" => $exception->getStatusCode(),
			"message" =>$exception->getStatusText()
		]);
	}
}

