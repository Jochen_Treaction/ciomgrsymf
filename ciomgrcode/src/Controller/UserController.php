<?php

namespace App\Controller;

//use App\Repository\UsersRepository;
// use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// use Symfony\Component\Routing\Annotation\Route;
// use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\HttpFoundation\RedirectResponse;
// use App\Services\AuthenticationService;
// use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use App\Entity\Users;
use App\Repository\AdministratesCompaniesRepository;
use App\Repository\AdministrationRepository;
use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\UserMetaRepository;
use App\Repository\UsersRepository;
use App\Repository\ContactRepository;
use App\Services\AioServices;
use App\Services\AppCacheService;
use App\Services\AuthenticationService;
use App\Services\CryptoService;
use App\Services\FormFieldValidationService;
use App\Services\ProcesshookService;
use App\Services\TwigGlobalsService;
use App\Services\SwiftMailerService;
use App\Types\AccountStatusTypes;
use App\Types\UserStatusTypes;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Tests\Functional\Bundle\CsrfFormLoginBundle\CsrfFormLoginBundle;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;

// from git 2020-01-09
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AdministrationService;
use App\Services\RolesService;
use WhichBrowser\Parser;


class UserController extends AbstractController
{

	protected $authenticationUtils;
	protected $authenticationService;
	protected $usersRespository;
	protected $swiftMailerService;
	protected $logger;
	protected $administrationService;
	protected $rolesService;
	protected  $objectRegisterRepository;


	/**
	 * UserController constructor.
	 * @param AuthenticationUtils   $authenticationUtils
	 * @param AuthenticationService $authenticationService
	 * @author jsr
	 */
	public function __construct(
		AuthenticationUtils $authenticationUtils,
		AuthenticationService $authenticationService,
		SwiftMailerService $swiftMailerService,
		LoggerInterface $logger,
		AdministrationService $administrationService,
		RolesService $rolesService,
		ObjectRegisterRepository $objectRegisterRepository
	) {
		$this->logger = $logger;
		$this->swiftMailerService = $swiftMailerService;
		$this->authenticationUtils = $authenticationUtils;
		$this->authenticationService = $authenticationService;
		$this->administrationService = $administrationService;
		$this->rolesService = $rolesService;
		$this->objectRegisterRepository = $objectRegisterRepository;
	}


	/**
     * @param AuthenticationUtils $authenticationUtils
     * @param UsersRepository $repository
     * @param LoggerInterface $logger
     * @return Response
     * @internal before part of AuthController, after submitting the form of /login/signin.twig route auth.signin is
     * called and redirects to auth.home
     * @author   jsr
     * Route No. 1, Refactorname auth.getsignin
     * $app->get('/', 'AuthController:getSignIn')->setName('auth.signin');
     * #Route( "/", name="auth.signin", methods={"GET"})
     * @Route( "/", name="auth.getSigninScreen", methods={"GET"})
     */
	public function loginform(
        Request $request,
        AppCacheService $apcuCache,
        AuthenticationUtils $authenticationUtils,
        UsersRepository $repository,
        LoggerInterface $logger
	): Response {
        $this->logger->info('@Route( "/", name="auth.getSigninScreen", methods={"GET"})', [__METHOD__, __LINE__]);
        // Clear Apcu Cache before login.
        $apcuCache->clearCache();
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $usrLoginMsg = '';

        // Message when user clicks the activation link second time (After the user status is active)
        // The status message is provided while activating user.
        $statusMsg = $request->get('s');
        if (!empty($statusMsg) && base64_decode($statusMsg) === 'account-is-active') {
            $usrLoginMsg = [
                'status' => 'success',
                'msg' => 'Account is activated, Please login with EMail and Password',
                'class' => 'alert-success',
            ];
        }

        $params = ['last_username' => $lastUsername, 'error' => $error, 'user_msg' => $usrLoginMsg];

        // check for user activation
        $signin_resp = $apcuCache->get($apcuCache::USERACTIVATION);

        if ($signin_resp && is_array($signin_resp)) {
            $params = array_merge($params, $signin_resp);
            $apcuCache->del($apcuCache::USERACTIVATION);
        }

        return $this->render('/login/signin.twig', $params);
	}

	// =======================================  AuthController Part ============================


	/**
	 * @param Request $request
	 * @return RedirectResponse
	 * @internal redirects from auth.getSigninScreen  to auth.home = dashboard
	 * Route No. 2, Refactorname auth.postsignin
	 * $app->post('/', 'AuthController:postSignIn');
	 * #Route( "/", name="auth.signin", methods={"POST"})
	 * @Route( "/", name="auth.signin", methods={"POST"})
	 * @author   jsr | from git 2020-01-09
	 */
	public function redirectToDashboard(Request $request, AdministrationService $administrationService)
	{
		$this->logger->info('@Route( "/", name="auth.signin", methods={"POST"})', [__METHOD__, __LINE__]);
		$redirectRouteParam = $request->get('redirect');
		if (isset($redirectRouteParam) && !empty($redirectRouteParam)) {
			$route = $administrationService->decodeRedirectParamFromUrl($redirectRouteParam);
		}
		$route = $route ?? 'auth.home';
		return $this->redirectToRoute($route); // dashboard
	}


	/**
	 * Todo: remove since app_logout was replaced by ciomgrcode/src/Listeners/LogoutListener.php
	 * @internal before part of AuthController
	 * Route No. 3, Refactorname auth.getsignout
	 * $app->get('/signout', 'AuthController:getSignOut')->setName('auth.signout');
	 * #Route( "/signout", name="auth.signout", methods={"GET"})
	 * @Route( "/signout", name="auth.signout", methods={"GET"})
	 * @author   jsr
	 * @since    2020-01-09
	 */
	public function signout(
		LoggerInterface $logger,
		AuthenticationUtils $authenticationUtils,
		AppCacheService $apcuCache
	) {
		$this->logger->info('@Route( "/signout", name="auth.signout", methods={"GET"})', [__METHOD__, __LINE__]);
		return $this->redirectToRoute('app_logout');
	}


	/**
	 * @param AuthenticationUtils $authenticationUtils
	 * @param Request             $request
	 * @param ContactRepository   $leadsRepo
	 * @param CampaignRepository  $campaignRepository
	 * @param UsersRepository     $usersRepository
	 * @param AppCacheService     $apcuCache
	 * @param TwigGlobalsService  $twigGlobals
	 * @param LoggerInterface     $logger
	 * @return Response
	 * @throws Exception
	 * @internal before part of AuthController
	 * Route No. 4, Refactorname auth.getdashboard
	 * $app->get('/dashboard', 'AuthController:getDashboard')->setName('auth.home');
	 * #Route( "/dashboard", name="auth.home", methods={"GET"})
	 * @Route( "/dashboard", name="auth.home", methods={"GET"})
	 */
	public function getDashboard(
		AuthenticationUtils $authenticationUtils,
		AuthenticationService $authenticationService,
		CompanyRepository $companyRepository,
		Request $request,
		ContactRepository $leadsRepo,
		CampaignRepository $campaignRepository,
		UsersRepository $usersRepository,
		AppCacheService $apcuCache,
		LoggerInterface $logger
	) {
		$userEmail = $authenticationUtils->getLastUsername();
		$userRecord = $authenticationService->getUserParametersFromCache($userEmail);
		$companyName = $userRecord['company_name'];
		if (empty($userEmail) || empty($userRecord)) {
			$this->redirect('/');
		}
		//Check for DPA acceptance
		if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
			//$error = ['error' => 'Please accept the DPA to proceed further'];
			return $this->redirectToRoute('acc_dpa_accept');
		}
		// Show the APIKey as POP if present in Route.
		$popupParameter = $request->get('popup');
		if (!empty($popupParameter) && (base64_decode($popupParameter) === 'apikey')) {
			$popup['apikey'] = $companyRepository->getMarketingInOneAPIKeyForCompany((int)$userRecord['company_id']);
			$popup['accountNumber'] = $userRecord['company_account_no'];
		} else {
			$popup = [];
		}
		if ($userEmail === filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
			// get from cache or from db and set cache
			$user_company_record = $this->authenticationService->getUserParametersFromCache($userEmail);
			$usersRepository->setSecureLogEntry(array_merge($user_company_record, ['ip' => $request->getClientIp(), 'result' => 'success']));

			// reset users.failed_logins to 0 and  users.status to 'active'
			$usersObj = $usersRepository->getUserById($user_company_record['user_id']); // added jsr 2020-11-13
			$usersRepository->activateUser($usersObj); // added jsr 2020-11-13
		}

		$leads = $leadsRepo->getLeadsForDashboard($user_company_record['company_id']);
		$campaigns = $campaignRepository->getCampaignsForDashboard($user_company_record['company_id']); // get all records
		$message = $apcuCache->get('projectStatus');
		// $message = apcu_fetch('projectStatus'); // not multiuser capable
		if (false === $message) {
			$message = $apcuCache->get('messageDraft');
			// $message = apcu_fetch('messageDraft'); // not multiuser capable
			$apcuCache->del('messageDraft');
			// apcu_delete('messageDraft'); // not multiuser capable
		}
		$dashboard = [
			'leads_count' => $leadsRepo->getLeadsPerPeriodForDashboardChart($user_company_record['company_id']),
			'leads_for_camp' => $leadsRepo->getLeadsCountForDashboardChart($user_company_record['company_id']),
			'leads' => $leads,
			'campaigns' => $campaigns,
		];

		if (false !==$message) {
			$apcuCache->del('projectStatus');
			// apcu_delete('projectStatus'); // not multiuser capable
			$dashboard['cacheMessage'] = $message;
		}

		return $this->render('/dashboard.twig', [
				'curr_acc_name' => $companyName,
				'dashboard' => $dashboard,
				'current_user' => $user_company_record,
				'popup' => $popup,
			]);
	}


	/**
	 * @param Request         $request
	 * @param LoggerInterface $logger
	 * @param CryptoService   $cryptoService
	 * @return Response
	 * @Route("/setpassword", name="user.setPasswordForNewUser", methods={"GET"})
	 */
	public function setPasswordForNewUser(
		Request $request,
		LoggerInterface $logger,
		CryptoService $cryptoService
	): Response {
		// GET Method
		if ($request->getMethod() === 'GET') {
			$userEmailEncrypted = base64_decode($request->get('p'));
			$userEmail = $cryptoService->simpleDecrypt($userEmailEncrypted);
			return $this->render('/account/change_password/change_password.twig', ['email' => $userEmail]);
		}
		return $this->redirectToRoute('auth.getSigninScreen'); // auth.getSigninScreen"
	}


	/**
	 * @internal before part of AuthController
	 * Route No. 5, Refactorname auth.getuserresetpassword
	 * $app->get('/resetpassword', 'AuthController:getUserResetPassword')->setName('auth.resetpassword');
	 * @Route( "/resetpassword", name="auth.resetpassword", methods={"GET"})
	 * @author   jsr (updated 2020-01-09)
	 *
	 */
	public function getResetPasswordForm(
		Request $request,
		AuthenticationUtils $authenticationUtils,
		AuthenticationService $authenticationService,
		AppCacheService $apcuCache,
		CryptoService $cryptoService,
		LoggerInterface $logger
	): Response {
		$this->logger->info('@Route( "/resetpassword", name="auth.resetpassword", methods={"GET"})', [__METHOD__, __LINE__]);
		$userEmailCrypt = $request->get('r');

		if (!empty($userEmailCrypt)) {
			$logger->info("PARAM r = $userEmailCrypt", [__METHOD__, __LINE__]);// TODO: remove
			$userEmail = $cryptoService->simpleDecrypt(base64_decode($userEmailCrypt));
			$logger->info("IF USEREMAIL = $userEmail", [__METHOD__, __LINE__]);// TODO: remove
		}

		if (!empty($userEmail)) {
            $userData = $apcuCache->get(md5($apcuCache::USERPWRESET . $userEmail));

            //$logger->info("getUserResetPassword for userEmail={$userEmail} and userData={$userData['email']}", [__METHOD__, __LINE__]);

            if (!empty($userData) && $userData[ 'email' ] === $userEmail) {
                $logger->info('getUserResetPassword = ', [__METHOD__, __LINE__]);
                // TODO: may be
                return $this->render("/account/change_password/change_password.twig",
                    //['success' => 'Reset your password', 'email' => $userEmail, 'current_user' => $user]);
                    ['success' => 'Reset your password', 'email' => $userEmail]);
            } else {
                // redirect to forgotpassword with error message 'timeout'
                return $this->render('/login/forgotpassword.twig',
					//['error' => 'The time to reset your password expired. Please try again.', 'current_user' => $user]);
					[
						'fgpwd' => ['failure' => 'The time to reset your password expired. Please try again.'],
						'email' => $userEmail,
					]);
			}
		} else {
			$userEmail = $authenticationUtils->getLastUsername();
		}

		return $this->render('/account/change_password/change_password.twig', ['email' => $userEmail]);
	}


	/**
	 * @param Request         $request
	 * @param LoggerInterface $logger
	 * @param UsersRepository $users
	 * @return Response
	 * @Route( "/resetpassword", name="auth.postresetpassword", methods={"POST"})
	 * @internal before part of AuthController
	 * @author   jsr | Pradeep
	 */
	public function setResetedPassword(
		Request $request,
		LoggerInterface $logger,
		UsersRepository $users,
		SwiftMailerService $swiftMailerService
	): Response {
		$token = $request->get('token');

		/*        if (!$this->isCsrfTokenValid('change-password', $token)) {
					var_dump('faield ');
					die();
					return $this->redirectToRoute('auth.getSigninScreen'); // auth.getSigninScreen"
				}*/

		$this->logger->info('setResetPassword');
		try {
			$checkEmail = $request->get('checkemail');
			$logger->info("\$CHECKEMAIL = $checkEmail ", [__METHOD__, __LINE__]);
			$password = $request->get('n1_password');
			$password2 = $request->get('n2_password');
			$fitsRequirements = (1 === preg_match('/(?=.*\d)(?=.*[^a-zA-Z0-9]{1,})(?=.*[a-z])(?=.*[A-Z]).{12,}/', $password));
			$logger->info("PASSWORDS=" . json_encode([$password, $password2]), [__METHOD__, __LINE__]);// get and perhaps set current user
			$currentUser = $this->getCurrentUser();
			if (empty($currentUser)) {
				//get current user from db, set it in cache and return the data
				$currentUser = $this->authenticationService->getUserParametersFromCache($checkEmail);
			}
			if ($password !== $password2) {
				return $this->render('/account/change_password/change_password.twig', [
					'error' => htmlentities('Your passwords do not match.'),
					'current_user' => $currentUser,
					'email' => $checkEmail,
				]);
			}
			if (!$fitsRequirements) {
				return $this->render('/account/change_password/change_password.twig', [
					'error' => htmlentities('A password must contain at least one number, one uppercase, one lowercase letter, one special character, and at least 12 characters!'),
					'current_user' => $currentUser,
					'email' => $checkEmail,
				]);
			}
			$user = $users->getUserByEmail($checkEmail);

			if ($user instanceof Users) {
				$status = $users->updateUserPassword($password, $user->getId());
				$users->activateUser($user); // added jsr 2020-11-13
			}

			// SENDMAIL
			if(empty($user->getDpa())) {
				try { // remove
					$sendAccountFinishSetupNotification = $swiftMailerService->sendAccountFinishSetupNotification($currentUser);
					if (false === $sendAccountFinishSetupNotification['ret']) {
						throw new Exception($sendAccountFinishSetupNotification['msg'], __LINE__);
					}
				} catch (Exception $e) { // remove
					;
				}
			}

			return $this->redirectToRoute('auth.getSigninScreen');
		} catch (NonUniqueResultException | Exception $e) {
			return $this->redirectToRoute('auth.getSigninScreen'); // auth.getSigninScreen"
		}
	}


	/**
	 * @return array
	 * @author jsr
	 */
	protected function getCurrentUser(): array
	{
		$user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
		if (!empty($user)) {
			return $user;
		} else {
			return [];
		}
	}


	/**
	 * @internal before part of AuthController
	 * Route No. 6, Refactorname auth.getforgotpassword
	 * $app->get('/forgotpassword', 'AuthController:getForgotPassword')->setName('auth.forgotpassword');
	 * #Route( "/forgotpassword", name="auth.forgotpassword", methods={"GET"})
     * @Route( "/forgotpassword", name="auth.forgotpassword", methods={"GET"})
     *
     */
    public function getForgotPasswordForm()
    {
        $this->logger->info(' @Route( "/forgotpassword", name="auth.forgotpassword", methods={"GET"})',
            [__METHOD__, __LINE__]);
        return $this->render('/login/forgotpassword.twig');
    }


    /**
     * @param Request         $request
     * @param AppCacheService $apcuCache
     * @param UsersRepository $usersRepository
     * @param CryptoService   $cryptoService
     * @param LoggerInterface $logger
     * @return Response
     * @throws NonUniqueResultException
     * @author   jsr
     * @internal before part of AuthController
     * Route No. 7, Refactorname auth.postforgotpassword
     * $app->post('/forgotpassword', 'AuthController:postForgotPassword');
     * #Route( "/forgotpassword", name="auth.forgotpassword", methods={"POST"})
     * @Route( "/processforgotpassword", name="auth.processforgotpassword", methods={"POST"})
     */
    public function processForgotPassword(
        Request $request,
        AppCacheService $apcuCache,
        UsersRepository $usersRepository,
        CryptoService $cryptoService,
        LoggerInterface $logger
    ) {
        $this->logger->info('@Route( "/processforgotpassword", name="auth.processforgotpassword", methods={"POST"})',
            [__METHOD__, __LINE__]);
        // check token
        $submittedToken = $request->request->get('_csrf_token');

		/*        if (!$this->isCsrfTokenValid('fgpw', $submittedToken)) {
					$fgpwd = ['failure' => htmlentities("Unknown form error. Please try again.")];
					return $this->render('/login/forgotpassword.twig', ['fgpwd' => $fgpwd]);
				}*/

		$email = $request->get('email');

		$logger->info("PROCESSFORGOTPASSWORD: EMAIL={$email}, TOKEN={$submittedToken}");

        // email must be provided
        $checkedEmail = filter_var($email, FILTER_VALIDATE_EMAIL, FILTER_SANITIZE_EMAIL);
        if (empty($email) || $email !== $checkedEmail) {
            // $fgpwd = ['failure' => htmlentities("Invaild email address.")]; // replaced by WEB-5606
            $fgpwd = ['success' => htmlentities("Please check your email inbox for a reset link.")]; // spoof message
            return $this->render('/login/forgotpassword.twig', ['fgpwd' => $fgpwd]);
        }

        // check if user exists
        $user = $usersRepository->getUserByEmail($checkedEmail);

        if (!($user instanceof Users) || ($checkedEmail !== $user->getEmail()) ||
            (UserStatusTypes::BLOCKBYADMIN === $user->getStatus())) {
            // $fgpwd = ['failure' => htmlentities("Unknown email address.")]; // replaced by WEB-5606
            $fgpwd = ['success' => htmlentities("Please check your email inbox for a reset link.")];  // do not spoof this message
            return $this->render('/login/forgotpassword.twig', ['fgpwd' => $fgpwd]);
        }elseif (AccountStatusTypes::BLOCK === $user->getCompany()->getObjectregister()->getStatusdef()->getValue()) {
            // $fgpwd = ['failure' => htmlentities("Your Account was blocked, please contact our support client-success@treaction.net")]; // replaced by WEB-5606
            $fgpwd = ['success' => htmlentities("Please check your email inbox for a reset link.")];  // spoof message
            return $this->render('/login/forgotpassword.twig', ['fgpwd' => $fgpwd]);
        }

        $logger->info("PROCESSFORGOTPASSWORD: USER={$user->getUsername()} FOUND");

        //$logger->info('SENDING MAIL TO ' . $usr->getEmail() . ' USR-REC:' . json_encode((array)$usr) . ', COMP-REC:' . json_encode((array)$company));
        $emailEncrypted = $cryptoService->simpleEncrypt($checkedEmail);
        $resetPwLink = (stripos(strtolower($request->server->get('SERVER_PROTOCOL')),
            'https')) ?: 'https://' . $request->server->get('HTTP_HOST') . '/resetpassword/?r='  // TODO: change link and add reset target /passwordreset mit
			. base64_encode($emailEncrypted);

		// TODO: remove
		$logger->info('RESTPW, URL=' . json_encode([
				'link' => $resetPwLink,
				'emailEncrypted' => $emailEncrypted,
				'urlencode' => base64_encode($emailEncrypted),
			]), [__METHOD__, __LINE__]);

//        $current_time = new \DateTime('now');
//        $resetPwUntil = $current_time->modify('+ 15 minutes')->format('Y-m-d H:i:s');

		$apcuCache->addOrOverwrite(md5($apcuCache::USERPWRESET . $checkedEmail), ['email' => $checkedEmail], //, 'until' => $resetPwUntil],
			$apcuCache::FIFTEEN_MINUTES);

		// TODO:
		// implement  see https://ourcodeworld.com/articles/read/459/how-to-authenticate-login-manually-an-user-in-a-controller-with-or-without-fosuserbundle-on-symfony-3
		// SENDMAIL
		$status = $this->swiftMailerService->sendPWResetLink($user->getEmail(), $user->getFirstName() , $user->getLastName(), $resetPwLink);

		if ($status['ret'] > 0) {
			$usersRepository->activateUser($user); // added jsr 2020-11-13
			$fgpwd = ['success' => htmlentities("Please check your inbox for reset link.")];
			return $this->render('/login/forgotpassword.twig', ['fgpwd' => $fgpwd]);
		} else {
			$fgpwd = ['failure' => htmlentities("Failed to send reset link. Please try again.")];
			return $this->render('/login/forgotpassword.twig', ['fgpwd' => $fgpwd]);
		}
		/*
		 *         $fgemail = $request->getParams();
		$auth_Obj = new \App\Models\Authenticate($this->container->dbh);
		$status = $auth_Obj->validateUserEmail($fgemail['email']);

		if (!$status) {
			$msg['failure'] = "Email address not found";
			#return $response->withRedirect($this->container->router->pathFor('auth.forgotpassword'));
			return $this->container->view->render($response, 'login/forgotpassword.twig', array('fgpwd' => $msg));
		}
		$this->user_details = $auth_Obj->getUserDetailsByEmail($fgemail['email']);
		$mail = new \App\Mailer\mail($this->user_details);
		$status = $mail->sendMailResetPassword();

		if (!$status) {
			$msg['failure'] = "Failed to send reset link";
			return $this->container->view->render($response, 'login/forgotpassword.twig', array('fgpwd' => $msg));
		}
		$msg['success'] = "Please check your inbox for reset link";
		return $this->container->view->render($response, 'login/forgotpassword.twig', array('fgpwd' => $msg));

		 */
	}


	/**
	 * @internal before part of AuthController
	 * Route No. 8, Refactorname auth.getsignup
	 * $app->get('signup', 'AuthController:getSignUp');
	 * @Route( "/signup", name="auth.getsignup", methods={"GET"})
	 * @author   jsr
	 */
	public function getSignUp()
	{
		$this->logger->info('@Route( "/signup", name="auth.getsignup", methods={"GET"})', [__METHOD__, __LINE__]);
		return $this->render('/login/signup.twig');
	}


	/**
	 * Route No. 8.1, Refactorname auth.postsignup
	 * $app->post('signup', 'AuthController:postSignUp');
	 * @Route( "/signup", name="auth.postSignUp", methods={"POST"})
	 * @param LoggerInterface                  $logger
	 * @param UsersRepository                  $usersRepository
	 * @param UserMetaRepository               $userMetaRepository
	 * @param CompanyRepository                $companyRepository
	 * @param AdministratesCompaniesRepository $administratesCompaniesRepository
	 * @param ObjectRegisterRepository         $objectRegisterRepository
	 * @param CryptoService                    $cryptoService
	 * @param AppCacheService                  $cacheService
	 * @param AioServices                      $aioServices
	 * @param Request                          $request
	 * @return Response
	 * @throws NonUniqueResultException
	 */
	public function registerUser(
		LoggerInterface $logger,
		UsersRepository $usersRepository,
		UserMetaRepository $userMetaRepository,
		CompanyRepository $companyRepository,
		AdministratesCompaniesRepository $administratesCompaniesRepository,
		AdministrationRepository $administrationRepository,
		ObjectRegisterRepository $objectRegisterRepository,
		CryptoService $cryptoService,
		AppCacheService $cacheService,
		ProcesshookService $processhookService,
		Request $request
	): Response
    {
        $pattern = "/^[\w \.\-]{2,50}$/mu"; // check firstname, lastname
        $this->logger->info('@Route( "/signup", name="auth.postSignUp", methods={"POST"})', [__METHOD__, __LINE__]);
        $usr[ 'salutation' ] = trim($request->get('salutation'));
        $usr[ 'f_name' ] = trim($request->get('first_name'));
        $usr[ 'l_name' ] = trim($request->get('last_name'));
        $usr[ 'email' ] = trim($request->get('email'));
        $usr[ 'permission' ] = ($request->get('permission')) === 'on' ? 1 : 0;
        $headers = $request->server->getHeaders();
        $ipAddress = $request->getClientIp();
        $location = $usersRepository->getUserLocationByIpAddress($ipAddress);
        $whichBrowser = new Parser($headers[ 'USER_AGENT' ]);
        $device = $whichBrowser->getType();
        $browserName = $whichBrowser->browser->name;
        $browserOs = $whichBrowser->os->name;


		// check csrf token
		$submittedToken = $request->request->get('token');
		if (!$this->isCsrfTokenValid('register', $submittedToken)) {
			$this->render('/standards/forbidden.html.twig');
		}


		if(1 !== preg_match($pattern, $usr['f_name'])) {
			$signup_resp = ['error' => 'Invaild Firstname.'];
			return $this->render('/login/signup.twig', $signup_resp);
		}

		if(1 !== preg_match($pattern, $usr['l_name'])) {
			$signup_resp = ['error' => 'Invaild Firstname.'];
			return $this->render('/login/signup.twig', $signup_resp);
		}

		// email must be provided
		$checkedEmail = filter_var($usr['email'], FILTER_VALIDATE_EMAIL, FILTER_SANITIZE_EMAIL);
		if (empty($usr['email']) || $usr['email'] !== $checkedEmail) {
			$signup_resp = ['error' => 'Invaild email address.'];
			return $this->render('/login/signup.twig', $signup_resp);
		}

		// Check User is exists
		$this->logger->info('$usr', [$usr, __METHOD__, __LINE__]);
		$db_usr = $usersRepository->getUserByEmail($usr['email']);

		// User Email already exists in DB.
		if ($db_usr !== null) {
			$signup_resp = ['error' => 'Invaild email or password. Try again.', 'last_username' => $usr['email']];
			return $this->render('/login/signup.twig', $signup_resp);
		}


		 $domain = explode('@', $usr['email'])[1];  // Get Domain from Email // deprecated since 2021-02-08
		// Check if Account is present. // deprecated since 2021-02-08 check: before users with same domain were assigned to same company => if you want to do this use "manage users"!
		// $company = $companyRepository->getCompanyByDomain($domain); // deprecated since 2021-02-08: new user is always assigned a new company
		// if ($company === null) { // since 2021-02-08: new user is always assigned a new company
		try {
			// create objectregister for account
			$objectregisterId = $objectRegisterRepository->createInitialObjectRegisterEntryForNewAccount();
			$this->logger->debug('$objectregisterId', [$objectregisterId, __METHOD__, __LINE__]);
			$companyName = $companyRepository->getNewAccountNo();
			$this->logger->debug('$companyName', [$companyName, __METHOD__, __LINE__]);
			$company = $companyRepository->createInitalCompany($objectregisterId,$domain);

			if(null !== $company) {
                $this->logger->debug('$company->getName()', [$company->getName(), __METHOD__, __LINE__]);
                $this->logger->debug('$company->getAccountNo()', [$company->getAccountNo(), __METHOD__, __LINE__]);
            }

			// update comment in object register with company name and account no
			$apikey = $administrationRepository->getMioAioApikey(); // added 2021-03-08 // FIX
			$this->logger->info("APIKEY", [$apikey]);

			$objectRegisterRepository->updateAccountObjectRegisterEntry(
				$objectregisterId,
				$company->getName(),
				$company->getAccountNo(),
				$apikey
			);

			// set account status
			$objectRegisterRepository->setCompanyObjectRegisterStatusToAccountStatusType($objectregisterId, AccountStatusTypes::NEW);
		} catch (Exception $e) {
			$logger->error($e->getMessage(), [__METHOD__, __LINE__]);
			$logger->error($e->getTraceAsString(), [__METHOD__, __LINE__]);
		}
		//}

		// Create User as MasterAdmin (can create companies, invite/maintain users, maintain any campaign of his companies)
		$usr = $usersRepository->createNewUser($usr, $company, $this->rolesService->getRoleMasterAdmin()); // WEB-3850
		$registration_firstStep_details = json_encode([
            'source' => 'MIO',
            'ip' => (string)$ipAddress,
            'time_stamp' => (string)date('Y-m-d H:i:s'),
            'location' => (string)$location,
            'device' => $device,
            'broswer' => (string)$browserName,
            'device_os' => (string)$browserOs,
        ], JSON_THROW_ON_ERROR);

		// Insert UserMetaRepository details for 1stStep.
		// add record to administrates_companies
		if ($company !== null && $usr !== null) {
            if (is_null($userMetaRepository->insert((int)$usr->getId(), $userMetaRepository::USER_REG_FIRSTSTEP,
                $registration_firstStep_details, (int)$usr->getId(),
                (int)$usr->getId()))) {
                $logger->critical('Failed to create User FirstStep Registration Details. ', [__METHOD__, __LINE__]);
            }
            $administratesCompaniesRepository->addManagedCompany((int)$usr->getId(), (int)$company->getId());
            // $companyRepository->storeDefaultIntegrationsToCompany((int)$company->getId()); // removed, 2021-02-05 => deprecated

        } else {
			$logger->critical('COULD NOT CREATE ADMINISTRATES_COMPANIES ENTRY. ', [__METHOD__, __LINE__]);
		}

		if (!($usr instanceof Users)) {
			$signup_resp = ['error' => 'Something went wrong , Please contact our Support center '];
			return $this->render('/login/signup.twig', $signup_resp);
		}

		$emailEncrypted = $cryptoService->simpleEncrypt($checkedEmail);
		$activationLink = (stripos(strtolower($request->server->get('SERVER_PROTOCOL')),
			'https')) ?: 'https://' . $request->server->get('HTTP_HOST') . '/doi/?p=' . base64_encode($emailEncrypted);
		$logger->info('activationLink', (array)($activationLink));
		// SENDMAIL
		$status = $this->swiftMailerService->sendActivationLink($usr->getEmail(), $usr->getFirstName(), $usr->getLastName(), $activationLink, $usr->getCompany()->getAccountNo());
		$signup_resp = ($status['ret'] > 0) ? ['success' => 'Please check your Mailbox for activation link'] : ['error' => 'Something went wrong , Please contact our Support center.'];

		// $cacheService->addOrOverwrite($cacheService::USERACTIVATION, $cacheService::ONE_DAY); // wrong second param is ttl, but should be value
		$cacheService->addOrOverwrite($cacheService::USERACTIVATION.$emailEncrypted, $checkedEmail, $cacheService::FIFTEEN_MINUTES);
		return $this->render('/login/signup.twig', $signup_resp);
	}


	/**
	 * @param LoggerInterface $logger
	 * @param UsersRepository $usersRepository
	 * @param Request         $request
	 * @param CryptoService   $cryptoService
	 * @param AppCacheService $cacheService
	 * @param AppCacheService $apcuCache
	 * @return Response
	 * @throws NonUniqueResultException
	 * @internal before part of AuthController
	 * Route No. 8.2, Refactorname auth.getactivateuser
	 * $app->get('/activateuser', 'AuthController:getActivateUser');
	 * #Route( "/activateuser", name="auth.getactivateuser", methods={"GET"})
	 * @Route( "/doi", name="auth_activateuser", methods={"GET"})
	 * @author   jsr
	 */
	public function activateUser(
		LoggerInterface $logger,
        UsersRepository $usersRepository,
        Request $request,
        ObjectRegisterRepository $objectRegisterRepository,
        CryptoService $cryptoService,
        UserMetaRepository $userMetaRepository,
        AppCacheService $apcuCache,
        AioServices $aioServices
	): Response {
		$this->logger->info('@Route( "/doi", name="auth_activateuser", methods={"GET"})', [__METHOD__, __LINE__]);
		$retUser = null;

		$userEmailEncrypted = base64_decode($request->get('p'));
		$userEmail = $cryptoService->simpleDecrypt($userEmailEncrypted);

		// TODO: enable ckech of activation link expiration again, when redis replaces apcu
		// reason:
		//  AIO handles Shop-MVP registrations. But it cannot set an APCU time limit (e.g. $apcuCacheService->addOrOverwrite($apcuCacheService::USERACTIVATION.$userEmailEncrypted, $data['email'], $apcuCacheService::FIFTEEN_MINUTES); )
		//  Since Shop-MVP reistrator has set his password in MIO, the check, if the has expired is removed here.

		// actually it should be checked if the link has expired ...
		// $activationLinkActive = $apcuCache->get($apcuCache::USERACTIVATION.$userEmailEncrypted);
		// if ( false === $activationLinkActive ) { set error message and optionally redirect }

		$user = $usersRepository->getUserByEmail($userEmail);
		$headers = $request->server->getHeaders();
		$ipAddress = $request->getClientIp();
		$location = $usersRepository->getUserLocationByIpAddress($ipAddress);
		$whichBrowser = new Parser($headers['USER_AGENT']);
		$browserName = $whichBrowser->browser->name;
		$browserOs = $whichBrowser->os->name;
		$device = $whichBrowser->getType();

		$registration_second_details = json_encode([
            'source' => 'MIO',
            'ip' => (string)$ipAddress,
            'time_stamp' => (string)date('Y-m-d H:i:s'),
            'location' => (string)$location,
            'device' => $device,
            'browser' => (string)$browserName,
            'device_os' => (string)$browserOs,
        ], JSON_THROW_ON_ERROR);

        if ($user instanceof Users) { // if user does not exist => e.g. sending activation link of deleted user
            $user->setFailedLogins(0);
        } else {
            // redirect to login
            return $this->redirectToRoute('auth.getSigninScreen');
        }

        // User status with confirmed has already activated himself.
        // So redirect the user directly to login screen
        if (UserStatusTypes::ACTIVE === $user->getStatus()) {
            return $this->redirectToRoute('auth.getSigninScreen', ['s' => base64_encode('account-is-active')]);
        }

        if (UserStatusTypes::NEW === $user->getStatus()) {
            $user->setStatus(UserStatusTypes::CONFIRMED);
            $retUser = $usersRepository->confirmUser($user);
        }
        if (is_null($userMetaRepository->insert((int)$user->getId(), $userMetaRepository::USER_REG_SECONDSTEP,
            $registration_second_details, (int)$user->getId(), (int)$user->getId()))) {
            $logger->critical('Failed to create User FirstStep Registration Details. ', [__METHOD__, __LINE__]);
        }

        if ($retUser instanceof Users && $userEmail === $retUser->getEmail()) {
            $signin_resp = [
                'success' => 'User has been successfully activated',
                'last_username' => (!empty($retUser)) ? $retUser->getEmail() : '',
            ];

            // when user sees login form his email is already added to Email address input field
            $apcuCache->addOrOverwrite($apcuCache::USERACTIVATION, $signin_resp);
        } else {
            $signin_resp = ['error' => 'Something went wrong , Please contact our Support center.'];
            // when user sees login form his email is already added to Email address input field
            $apcuCache->addOrOverwrite($apcuCache::USERACTIVATION, $signin_resp);
        }

        // createUserToeMIO(string email);
        if ($aioServices->createContactToeMIO($user)) {
            $logger->error('Failed to create Contact in eMIO ', [__METHOD__, __LINE__]);
        }
        $company_objectregister_id = $user->getCompany()->getObjectregister()->getId();
        $objectRegisterRepository->setCompanyObjectRegisterStatusToAccountStatusType($company_objectregister_id,
            AccountStatusTypes::CONFIRMED);

        $link_to_set_password = (stripos(strtolower($request->server->get('SERVER_PROTOCOL')),
            'https')) ?: 'https://' . $request->server->get('HTTP_HOST') . '/setpassword/?p=' . base64_encode($userEmailEncrypted);

        return $this->redirect($link_to_set_password);
    }


	/**
	 * @param LoggerInterface $logger
	 * @param UsersRepository $usersRepository
	 * @param Request         $request
	 * @param CryptoService   $cryptoService
	 * @return Response
	 * @throws NonUniqueResultException
	 * @internal WEB-3850
	 * @Route( "/invitation/doi", name="authInvitationDoi", methods={"GET"})
	 * @author   jsr
	 */
	public function activateInvitedUser(
		LoggerInterface $logger,
		UsersRepository $usersRepository,
		Request $request,
		CryptoService $cryptoService,
		AppCacheService $cacheService,
		AppCacheService $apcuCache
	): Response {
		$this->logger->info('@Route( "/invitation/doi", name="authInvitationDoi", methods={"GET"})', [__METHOD__, __LINE__]);
		$retUser = null;

		// TODO: remove logger
		$logger->info('INVITATION-URL = ', [json_encode($request->get('p')), __METHOD__, __LINE__]);
		// $userEmailEncrypted = base64_decode($request->get('p'));
		$userEmail = $cryptoService->simpleDecrypt($request->get('p')); // get the key to read email-address from apcu
		$logger->info('INVITATION-URL $userEmail = ', [$userEmail, __METHOD__, __LINE__]);
		$logger->info('INVITATION-URL cache = ', [$cacheService->get($cacheService::USERINVITATION . $userEmail), __METHOD__, __LINE__]);

		if (false === $cacheService->get($cacheService::USERINVITATION . $userEmail)) {
			$signin_resp = ['error' => 'Your activation link expired. Please ask the inviting person to send an invitation again.'];
			$apcuCache->addOrOverwrite($apcuCache::USERACTIVATION,
				$signin_resp); // when user sees login form his email is already added to Email address input field
			return $this->redirectToRoute('auth.getSigninScreen');
		} else {
			$user = $usersRepository->getUserByEmail($userEmail);
		}

		if ($user instanceof Users) { // if user does not exist => e.g. sending activation link of deleted user
			$user->setFailedLogins(0);
		} else {
			// redirect to login
			return $this->redirectToRoute('auth.getSigninScreen');
		}

		if (UserStatusTypes::NEW === $user->getStatus()) {
			$user->setStatus(UserStatusTypes::ACTIVE);
			$retUser = $usersRepository->activateUser($user);
		}

		if ($retUser instanceof Users && $userEmail === $retUser->getEmail()) {
			$signin_resp = [
				'success' => 'User has been successfully activated',
				'last_username' => (!empty($retUser)) ? $retUser->getEmail() : '',
			];

			$logger->info('$signin_resp' . json_encode($signin_resp), [__METHOD__, __LINE__]);
			$apcuCache->addOrOverwrite($cacheService::USERACTIVATION,
				$signin_resp); // when user sees login form his email is already added to Email address input field
			$cacheService->del($cacheService::USERINVITATION . $userEmail);
		} else {
			$signin_resp = ['error' => 'Something went wrong , Please contact our Support center.'];
			$apcuCache->addOrOverwrite($cacheService::USERACTIVATION,
				$signin_resp); // when user sees login form his email is already added to Email address input field
		}

		return $this->redirectToRoute('auth.getSigninScreen');
	}


	/**
	 * @Route("/user/unblockaccount", name="user.unblockaccount", methods={"GET"})
	 * @param Request            $request
	 * @param SwiftMailerService $swiftMailerService
	 * @throws Exception
	 * @see  App\Services\SwiftMailerService (route used by)
	 */
	public function userUnblockAccount(Request $request, SwiftMailerService $swiftMailerService, UsersRepository  $usersRepository, CompanyRepository $companyRepository):Response
	{
		$currentUser = $this->getCurrentUser();
		$user = $usersRepository->getUserByEmail($currentUser['email']);

		$companyObjectregisterId = $user->getCompany()->getObjectregister()->getId();

		$objectRegisterMetaId = $this->objectRegisterRepository->removeCompanyDeletionDate(
			$companyObjectregisterId
		);

		if(null === $objectRegisterMetaId) {
			$swiftMailerService->sendAccountReactivationNotification($currentUser, 'failed with error e'.__LINE__.'. Please contact the Marketing-In-One support.' );
			return $this->redirectToRoute('auth.home');
		}


		if (!$this->objectRegisterRepository->updateStatus($companyObjectregisterId,
			AccountStatusTypes::ACTIVE)) {
			$swiftMailerService->sendAccountReactivationNotification($currentUser, 'failed with error e'.__LINE__.'. Please contact the Marketing-In-One support.' );
			$this->logger->error('Failed to reactivate account (objectRegisterRepository->updateStatus) e'.__LINE__, [__METHOD__, __LINE__]);
			return $this->redirectToRoute('auth.home');
		}

		$companyDetails = $companyRepository->getCompanyDetailsByObjectRegisterId($companyObjectregisterId);

		if (empty($companyDetails)) {
			$swiftMailerService->sendAccountReactivationNotification($currentUser, 'failed with error e'.__LINE__.'. Please contact the Marketing-In-One support.' );
			$this->logger->error('Failed to reactivate account (companyRepository->getCompanyDetailsByObjectRegisterId) e'.__LINE__, [__METHOD__, __LINE__]);
			return $this->redirectToRoute('auth.home');
		}

		$usersIds = $usersRepository->getAllUserIdsByCompanyId($companyDetails[ 'id' ]);
		// Change the status of all the Users to active.
		foreach ($usersIds as $userId) {
			if (is_null($usersRepository->unblockUserById($userId[ 'id' ]))) {
				$this->logger->error('Failed to unblock User with Id ' . $userId);
				continue;
			}
		}

		$swiftMailerService->sendAccountReactivationNotification($currentUser, 'was successful!' );
		return $this->redirectToRoute('auth.home');

	}




	// =======================================  UserController Part ============================


	/**
	 * Route No. 19, Refactorname user.getuserdetialsbyid
	 * $app->get('/account/{acc_id}/user/{user_id}/read', 'UserController:getUserDetialsById')->setName('acc.details');
	 * #Route( "/account/{acc_id}/user/{user_id}/read", name="acc.details", methods={"GET"})
	 * @Route( "/account/{acc_id}/user/{user_id}/read", name="acc.details", methods={"GET"})
	 * @deprecated
	 */
	public function getUserDetialsById()
	{
	}


	/**
	 * Route No. 20, Refactorname user.getblockuserbyid
	 * $app->get('/account/{acc_id}/user/{user_id}/blockuser', 'UserController:getBlockUserById')->setName('user.blockUserById');
	 * #Route( "/account/{acc_id}/user/{user_id}/blockuser", name="user.blockUserById", methods={"GET"})
	 * @Route( "/account/{acc_id}/user/{user_id}/blockuser", name="user.blockUserById", methods={"GET"})
	 * @deprecated
	 */
	public function getBlockUserById() // todo: implement for superadmin forms
	{
	}


	/**
	 *
	 * Route No. 21, Refactorname user.getunblockuserbyid
	 * $app->get('/account/{acc_id}/user/{user_id}/unblockuser', 'UserController:getUnBlockUserById')->setName('user.UnBlockBlockUserById');
	 * #Route( "/account/{acc_id}/user/{user_id}/unblockuser", name="user.UnBlockBlockUserById", methods={"GET"})
	 * @Route( "/account/{acc_id}/user/{user_id}/unblockuser", name="user.UnBlockBlockUserById", methods={"GET"})
	 * @deprecated
	 */
	public function getUnBlockUserById() // todo: implement for superadmin forms
	{
	}


	/**
	 * Route No. 22, Refactorname user.getusersignup
	 * $app->get('/usersignup', 'UserController:getUserSignUp')->setName('user.signup');
	 * #Route( "/usersignup", name="user.signup", methods={"GET"})
	 * @Route( "/usersignup", name="user.signup", methods={"GET"})
	 * @deprecated
	 */
	public function getUserSignUp() // done todo:remove
	{
	}


	/**
	 * Route No. 23, Refactorname user.postusersignup
	 * $app->post('/usersignup', 'UserController:postUserSignUp');
	 * #Route( "/usersignup", name="user.signup", methods={"POST"})
	 * @Route( "/usersignup", name="user.signup", methods={"POST"})
	 * @deprecated
	 */
	public function postUserSignUp() // done todo:remove
	{
	}


	/**
	 * Route No. 24, Refactorname user.getupdateuser
	 * $app->get('/account/userupdate', 'UserController:getUpdateUser')->setName('user.update')
	 * @Route( "/account/userupdate", name="user.getUpdateUser", methods={"GET"})
	 * @param UsersRepository       $user
	 * @param AuthenticationService $authenticationService
	 * @param AuthenticationUtils   $authenticationUtils
	 * @return Response
	 */
	public function getUpdateUser(
		UsersRepository $user,
		AuthenticationService $authenticationService,
		AuthenticationUtils $authenticationUtils
	) {
		$AuthDetails = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
		if (empty($AuthDetails)) {
			$this->redirect('/');
		}
		$user_details = $user->getUserDetailsForUpdate($AuthDetails['user_id']);
        return $this->render('/account/user_information/userupdate.twig',
            ['userInfo' => $user_details, 'current_user' => $AuthDetails, "error" => ""]);
	}


    /**
     * @param Request $request
     * @param UsersRepository $user
     * @return Response
     * @author PVA
     * Route No. 24.1, Refactorname user.postupdateuser
     * $app->get('/account/userupdate', 'UserController:postUpdateUser')
     * @Route( "/account/userupdate", name="user.postUpdateUser", methods={"POST"})
     */
    public function updateUser(
        Request $request,
        UsersRepository $user,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        FormFieldValidationService $fieldValidationService,
        LoggerInterface $logger
    ): Response {

        try {
            $token = $request->request->get('token');
            $authDetails = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());

            if (!$this->isCsrfTokenValid('user-update', $token) || empty($authDetails)) {
                return $this->redirectToRoute('auth.getSigninScreen');
            }
            $usr[ 'id' ] = $request->get('id');
            $usr[ 'salutation' ] = trim($request->get('salutation'));
            $usr[ 'first_name' ] = trim($request->get('first_name'));
            $usr[ 'last_name' ] = trim($request->get('last_name'));
            $usr[ 'mobile' ] = trim($request->get('mobile'));
            $usr[ 'phone' ] = trim($request->get('phone'));
            $usr[ 'email' ] = trim($request->get('email'));// check for salutation
            if (!$fieldValidationService->isValidSalutation($usr[ 'salutation' ])) {
                throw new RuntimeException("Invalid salutation");
            }
            if (!$fieldValidationService->isValidEmail($usr[ 'email' ])) {
                throw new RuntimeException("Invalid Email");
            }
            if (!($fieldValidationService->isValidName($usr[ 'first_name' ]) && $fieldValidationService->isValidName($usr[ 'last_name' ]))) {
                throw new RuntimeException("Invalid Name");
            }
            if (!empty($usr[ 'phone' ]) && !$fieldValidationService->isValidPhone($usr[ 'phone' ])) {
                throw new RuntimeException("Invalid Phone Number");
            }
            $status = $user->updateUserDetails($usr);
            $response = $status ? ['success' => 'User details are updated successfully'] : ['error' => 'Something went wrong, Failed to Update User Details'];
            $response[ 'user' ] = $user->getUserDetailsForUpdate((int)$usr[ 'id' ]);
            return $this->redirectToRoute('user.getUpdateUser');
        } catch (Exception | RuntimeException $e) {
            $logger->info("User Update " . $e->getMessage());
            return $this->render('/account/user_information/userupdate.twig', [
                'userInfo' => $usr,
                'current_user' =>
                    $authDetails,
                'error' => $e->getMessage(),
            ]);
        }
    }


	/**
	 * todo: remove, since replaced by route /account/userupdate
	 * @param UsersRepository $usersRepository
	 * @return RedirectResponse
	 * @author PVA
	 * Route No. 25, Refactorname user.getupdateuserbyid
	 * $app->get('/account/{acc_id}/user/{user_id}/update', 'UserController:getUpdateUserById')->setName('user.updateById');
	 * #Route( "/account/{acc_id}/user/{user_id}/update", name="user.updateById", methods={"GET"})
	 * @Route( "/account/{acc_id}/user/{user_id}/update", name="user.updateById", methods={"GET"})
	 */
	/*
	public function getUpdateUserById(UsersRepository $usersRepository)
	{

		$userDetails = $usersRepository->getUserDetailsForUpdate();
		return $this->redirect('/account/user_information/userupdate.twig');
	}
	*/


	/**
	 *  todo: remove, since replaced
	 * Route No. 26, Refactorname user.postupdateuserbyid
	 * $app->post('/account/{acc_id}/user/{user_id}/update', 'UserController:postUpdateUserById');
	 * @Route( "/account/user/{user_id}/update", name="user.updateById", methods={"POST"})
	 */
	/*
	public function postUpdateUserById()
	{
	}
	*/


	/**
	 * todo: remove, since replace by users doi
	 * Route No. 27, Refactorname user.getactivateuserbyid
	 * $app->get('/account/{acc_id}/user/{user_id}/activateuser[/{expire_time}]', 'UserController:getActivateUserById')->setName('user.ActivateUserById');
	 * #Route( "/account/{acc_id}/user/{user_id}/activateuser[/{expire_time}]", name="user.ActivateUserById", methods={"GET"})
	 * @Route( "/account/{acc_id}/user/{user_id}/activateuser[/{expire_time}]", name="user.ActivateUserById", methods={"GET"})
	 *
	 */
	/*
	public function getActivateUserById()
	{
	}
	*/


}
