<?php


namespace App\Controller;


use App\Entity\Integrations;
use App\Repository\CampaignRepository;
use App\Repository\ContactRepository;
use App\Repository\CustomfieldsRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\StatusDefRepository;
use App\Services\AuthenticationService;
use App\Services\IntegrationService;
use App\Types\IntegrationTypes;
use App\Types\UniqueObjectTypes;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class IntegrationsController extends AbstractController
{

    /**
     * @Route( "/account/integrations/{status}", defaults={"status"="available"}, name="integrations.available", methods={"GET"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param StatusDefRepository $statusdefRepository
     * @param IntegrationsRepository $integrationsRepository
     * @param IntegrationTypes $integrationTypes
     * @return Response
     * @author Pradeep
     */
    public function getIntegrations(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        StatusdefRepository $statusdefRepository,
        IntegrationsRepository $integrationsRepository,
        IntegrationTypes $integrationTypes
    ): Response {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $companyId = $user[ 'company_id' ];
        $status = $request->get('status');
        $integrations = [];
        if ($status === $statusdefRepository::ARCHIVED) {
            $integrations = $integrationsRepository->getIntegrationsForCompany($companyId,
                StatusDefRepository::ARCHIVED);
            $twig = 'integrations/integrations_archived.twig';
            // get integration in status 'draft' with companyId
        } elseif ($status === $statusdefRepository::AVAILABLE) {
            $integrations = $integrationsRepository->getAllAvailableIntegrationsOverview();
            $twig = 'integrations/integrations.twig';
        } elseif ($status === $statusdefRepository::INSTALLED) {
            $integrations = $integrationsRepository->getIntegrationsForCompany($companyId,
                StatusDefRepository::ACTIVE);
            $twig = 'integrations/integrations_installed.twig';
            // get integration in status 'installed' with companyId
        } else {
            return $this->redirectToRoute('auth.home');
        }

        return $this->render($twig, ['current_user' => $user, 'integrations' => $integrations]);
    }

    /**
     * @param Request $request
     * @param IntegrationTypes $integrationTypes
     * @return Response|null
     * @internal redirects to specific integration wizard.
     * @Route( "/account/integrations/{integration}/wizard", name="integration_wizard.get", methods={"GET"})
     * @author Pradeep
     */
    public function getIntegrationWizard(Request $request, IntegrationTypes $integrationTypes): ?Response
    {
        $integration = $request->get('integration');
        $emailMatchString = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '',
            $integrationTypes::INTEGRATION_EMAILSERVER));
        $eMIOMatchString = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', $integrationTypes::INTEGRATION_EMIO));

        if ($integration === $emailMatchString) {
            $route = 'emailserver.get';
        } elseif ($integration === $eMIOMatchString) {
            $route = 'emailinone.get';
        } else {
            $route = $integration;
        }

        return $this->redirectToRoute($route);
    }


    /**
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param IntegrationsRepository $integrationsRepository
     * @param AuthenticationUtils $authenticationUtils
     * @param IntegrationTypes $integrationTypes
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param LoggerInterface $logger
     * @return Response|null
     * @internal redirects to specific integration wizard.
     * @Route( "/account/integrations/{integration}/remove", name="integration.remove", methods={"GET"})
     * @author Pradeep
     */
    public function removeIntegration(
        Request $request,
        AuthenticationService $authenticationService,
        IntegrationsRepository $integrationsRepository,
        AuthenticationUtils $authenticationUtils,
        IntegrationTypes $integrationTypes,
        ObjectRegisterRepository $objectRegisterRepository,
        LoggerInterface $logger
    ): ?Response {
        try {
            $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
            $companyId = $user[ 'company_id' ];
            $integration = $request->get('integration');
            if (empty($integration)) {
                throw new RuntimeException('invalid integration provided');
            }
            $allIntegrationsFromDB = $integrationsRepository->getAllAvailableIntegrationsOverview();
            foreach ($allIntegrationsFromDB as $integrationFromDB) {
                if (!isset($integrationFromDB[ 'overview' ][ 'title' ])) {
                    continue;
                }
                $matchString = strtolower(preg_replace('/[^A-Za-z0-9\-]/', '',
                    $integrationFromDB[ 'overview' ][ 'title' ]));
                if ($integration !== $matchString) {
                    continue;
                }
                // Todo :: Hotfix for regression testing, need to replace matchstring with UniqueObjectType.
                $integrationConfig = $integrationsRepository->getIntegrationConfigForCompany($companyId,
                    $matchString);
                if ($integrationConfig === null ||
                    !isset($integrationConfig[ 'relation' ][ 'integration_objectregister_id' ]) ||
                    !$objectRegisterRepository->updateStatus(
                        (int)$integrationConfig[ 'relation' ][ 'integration_objectregister_id' ],
                        StatusDefRepository::ARCHIVED)
                ) {
                    throw new RuntimeException('Failed to change the status of Integration to archived');
                }

            }
            return $this->redirectToRoute('integrations.available', ["status" => "archived"]);
        } catch (Exception | RuntimeException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->redirectToRoute('integrations.available', ["status" => "installed"]);
        }
    }

    /**
     * @Route( "/account/integration/emailserver/", name="emailserver.get", methods={"GET"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param IntegrationsRepository $integrationsRepository
     * @param AuthenticationUtils $authenticationUtils
     * @param IntegrationTypes $integrationTypes
     * @return Response
     * @author Pradeep
     */
    public function getEmailServerIntegrations(
        Request $request,
        AuthenticationService $authenticationService,
        IntegrationsRepository $integrationsRepository,
        AuthenticationUtils $authenticationUtils,
        IntegrationTypes $integrationTypes
    ): Response {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $companyId = $user[ 'company_id' ];
        // create new Object register Id
        $eMailIntegrationConfig = $integrationsRepository->getIntegrationConfigForCompany($companyId,
            UniqueObjectTypes::EMAILSERVER);
        if ($eMailIntegrationConfig === null || empty($eMailIntegrationConfig)) {
            $eMailIntegrationConfig = $integrationTypes->getEmailServerConfig();
        }
        //$eMailIntegrationConfig[ 'current_user' ] = $user;
        $eMailIntegrationConfig[ 'b64msmail' ] = base64_encode($_ENV[ 'MSMAIL_URL' ] . '/email/test_cio_config');

        return $this->render('integrations/wizard/emailserver.twig',
            ['current_user' => $user, 'emailserver' => $eMailIntegrationConfig, 'success' => '', 'error' => '']);
    }

    /**
     * @Route( "/account/integration/emailserver/", name="emailserver.post", methods={"POST"})
     * @param Request $request
     * @param IntegrationTypes $integrationTypes
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignRepository $campaignRepository
     * @param IntegrationsRepository $integrationsRepository
     * @param LoggerInterface $logger
     * @return Response
     * @throws JsonException
     * @author Pradeep
     */
    public function postEmailServerIntegrations(
        Request $request,
        IntegrationTypes $integrationTypes,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        CampaignRepository $campaignRepository,
        IntegrationsRepository $integrationsRepository,
        LoggerInterface $logger
    ): Response {
        $eMailIntegrationConfig = [];
        try {
            $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
            $project = $campaignRepository->getProjectDetails($user[ 'company_id' ]);
            $companyId = $user[ 'company_id' ];
            $campaignId = $project[ 'id' ];
            $userId = $user[ 'user_id' ];// get previous configured emailserver settings
            $eMailIntegrationConfig = $integrationsRepository->getIntegrationConfigForCompany($companyId,UniqueObjectTypes::EMAILSERVER);
            $logger->info('$eMailIntegrationConfig', [$eMailIntegrationConfig, __METHOD__,__LINE__]);

            // if record was archived it won't have been found by  $integrationsRepository->getIntegrationConfigForCompany(...)
            if ($eMailIntegrationConfig === null || empty($eMailIntegrationConfig)) {
               $eMailIntegrationConfig = $integrationTypes->getEmailServerConfig();
            }

            // added 2022-07-01 =>
            // (2022-07-01) treat exiting records in production without property "integration_object_register_meta_id" in $eMailIntegrationConfig[ 'relation' ][ 'integration_object_register_meta_id' ], which was not provided in IntegrationTypes::getEmailServerConfig during AccountController::acceptDPAForNewUser (completion of user registration process)
            if (empty($eMailIntegrationConfig['relation']['integration_object_register_meta_id'])) {

                $emailServerObjRegMetaValArray = $objectRegisterMetaRepository->getJsonMetaValueAsArray((int) $eMailIntegrationConfig['relation']['integration_objectregister_id'], UniqueObjectTypes::EMAILSERVER);
                $logger->info('$emailServerObjRegMetaValArray', [$emailServerObjRegMetaValArray, __METHOD__, __LINE__]);

                if(empty($emailServerObjRegMetaValArray) || empty($emailServerObjRegMetaValArray['relation']['integration_object_register_meta_id']) ) {
                    $eMailIntegrationConfig['relation']['integration_object_register_meta_id'] = 0; //  create at least the missing property "integration_object_register_meta_id" and set value 0 as default,
                } else {
                    $eMailIntegrationConfig['relation']['integration_object_register_meta_id'] = $emailServerObjRegMetaValArray['relation']['integration_object_register_meta_id'];
                }
            }

            $eMailIntegrationConfig[ 'relation' ][ 'company_id' ] = $companyId;
            $serverType = $request->get('server-type');

            $logger->info("integrations POST $serverType", [$serverType, __METHOD__, __LINE__]);

            $sendUserName = $request->get('sender-user-name');
            $eMailIntegrationConfig[ 'settings' ][ 'server_type' ] = $serverType;
            $eMailIntegrationConfig[ 'settings' ][ 'custom_server' ][ 'smtp_server_domain' ] = $request->get('smtp-server-domain') ?? '';
            $eMailIntegrationConfig[ 'settings' ][ 'custom_server' ][ 'sender_user_email' ] = $request->get('sender-user-email') ?? '';
            $eMailIntegrationConfig[ 'settings' ][ 'custom_server' ][ 'sender_user_name' ] = (empty($sendUserName)) ? $eMailIntegrationConfig[ 'settings' ][ 'custom_server' ][ 'sender_user_email' ] : $sendUserName;
            $eMailIntegrationConfig[ 'settings' ][ 'custom_server' ][ 'sender_password' ] = $request->get('sender-password') ?? '';
            $eMailIntegrationConfig[ 'settings' ][ 'custom_server' ][ 'port' ] = $request->get('port') ?? '';
            $eMailIntegrationConfig[ 'relation' ][ 'integration_object_register_meta_id' ] = $request->get('object-register-meta-id') ?? '';

            $logger->info('$eMailIntegrationConfig', [$eMailIntegrationConfig, __METHOD__, __LINE__]);

            if (!empty($eMailIntegrationConfig['relation']['integration_object_register_meta_id']) && $eMailIntegrationConfig['relation']['integration_object_register_meta_id'] > 0) {
                // update object register
                $metaKey = UniqueObjectTypes::EMAILSERVER;
                $metaValue = json_encode($eMailIntegrationConfig, JSON_THROW_ON_ERROR);
                if (!$objectRegisterMetaRepository->update((int) $eMailIntegrationConfig[ 'relation' ][ 'integration_object_register_meta_id' ],
                    (int) $eMailIntegrationConfig[ 'relation' ][ 'integration_objectregister_id' ], $metaKey, $metaValue)) {
                    throw new RuntimeException('Failed to Update the Email server settings to ObjectRegisterMetaRepository');
                }
                $response = $integrationsRepository->getIntegrationConfigForCompany($companyId,
                    UniqueObjectTypes::EMAILSERVER);
            } else {
                // (2022-07-01)
                // @Pradeep: WHAT IS CURRENTLY DONE HERE IS DOES NOT WORK!
                // - there will exactly one record in object_register_meta after registration in object_register_meta => YOU MUST WORK ONLY ON THIS ONE AND ONLY RECORD!
                //  if the record is active, update the active record
                //  if the record is archived, set it to active and update it with the content provided by the post
                //          ==> @Pradeep: THERE IS ANOTHER BUG: if you archive a email server integration, you'll get two (!!!) archived records for that company => there MUST BE ALWAYS ONLY ONE with corresponding status

                // (2022-07-01)
                // @Pradeep: DO NOT INSERT FURTHER RECORDS! Only update the existing one!

                // insert new object register.
                $response = $integrationsRepository->insertIntegrationsConfigForCompany($userId, $campaignId,
                    $companyId,
                    UniqueObjectTypes::EMAILSERVER, $eMailIntegrationConfig);
            }
            $eMailIntegrationConfig[ 'b64msmail' ] = base64_encode($_ENV[ 'MSMAIL_URL' ] . '/email/test_cio_config');
            return $this->redirectToRoute('integrations.available', ["status" => "installed"]);
        } catch (JsonException | RuntimeException | Exception $e) {
            return $this->render('integrations/wizard/emailserver.twig',
                [
                    'current_user' => $user,
                    'emailserver' => $eMailIntegrationConfig,
                    'success' => '',
                    'error' => $e->getMessage(),
                ]);
        }
    }


    /**
     * @Route( "/account/integration/emailinone/", name="emailinone.get", methods={"GET"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param IntegrationsRepository $integrationsRepository
     * @param CustomfieldsRepository $customfieldsRepository
     * @return Response
     * @author Pradeep
     */
    public function geteMIOIntegration(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        IntegrationsRepository $integrationsRepository,
        CustomfieldsRepository $customfieldsRepository,
        ContactRepository $contactRepository
    ): Response {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $companyId = $user[ 'company_id' ];
        // Get the eMio settings from DB
        $eMIOIntegrationConfig = $integrationsRepository->getIntegrationConfigForCompany($companyId,
            UniqueObjectTypes::EMAILINONE);
        //Get all the available custom fields for the account
        $customFields = $customfieldsRepository->getProjectCustomfields($companyId);
        //$standardCustomFields = $contactRepository->getStandardCustomFields();
        $endPoint = $_ENV[ 'MSMIO_URL' ];

        return $this->render('integrations/wizard/emailinone.twig',
            [
                'current_user' => $user,
                'emailinone' => $eMIOIntegrationConfig,
                'mio_customfiedls' => $customFields,
                'mio_standard_custom_fields' => [],
                'endpoint' => $endPoint,
            ]
        );
    }


    /**
     * @Route("/account/integration/emailinone/", name="emailinone.post", methods={"POST"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param IntegrationsRepository $integrationsRepository
     * @param CustomfieldsRepository $customfieldsRepository
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param IntegrationService $integrationService
     * @param IntegrationTypes $integrationTypes
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param CampaignRepository $campaignRepository
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function postEMIOIntegration(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        IntegrationsRepository $integrationsRepository,
        CustomfieldsRepository $customfieldsRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        IntegrationService $integrationService,
        IntegrationTypes $integrationTypes,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        CampaignRepository $campaignRepository,
        LoggerInterface $logger
    ): Response {
        try {
            $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
            $project = $campaignRepository->getProjectDetails($user[ 'company_id' ]);
            $companyId = $user[ 'company_id' ];
            $campaignId = $project[ 'id' ];
            $userId = $user[ 'user_id' ];

            $eMIOIntegrationConfig = $integrationsRepository->getIntegrationConfigForCompany($companyId,
                UniqueObjectTypes::EMAILINONE);

            if (empty($eMIOIntegrationConfig) || $eMIOIntegrationConfig === null) {
                $eMIOIntegrationConfig = $integrationTypes->getEMailInOneConfig();
            }

            $eMIOIntegrationConfig[ 'relation' ][ 'company_id' ] = $companyId;
            $eMIOIntegrationConfig[ 'settings' ][ 'apikey' ] = $request->get('apikey');
            $eMIOIntegrationConfig[ 'settings' ][ 'doi_mailing' ] = $request->get('doi_mailing');
            $eMIOIntegrationConfig[ 'settings' ][ 'blacklist_id' ] = $request->get('blacklist_id');
            $eMIOIntegrationConfig[ 'settings' ][ 'blacklisting_flagged_name' ] = $request->get('blacklisting_flagged_name');
            $eMIOIntegrationConfig[ 'settings' ][ 'reactivate_unsubscibers' ] = $request->get('reactivate_unsubscibers');
            $eMIOIntegrationConfig[ 'settings' ][ 'mapping' ] = $request->get('mapping');
            $eMIOIntegrationConfig[ 'relation' ][ 'integration_object_register_meta_id' ] = $request->get('object-register-meta-id');
            $logger->info('eMIOMapping ' . $eMIOIntegrationConfig[ 'settings' ][ 'mapping' ]);
            $eMIOIntegrationConfig = $integrationService->autoCreateEMIOCustomField($eMIOIntegrationConfig);
            if ((int)$eMIOIntegrationConfig[ 'relation' ][ 'integration_object_register_meta_id' ] > 0) {
                // update object register
                $metaKey = UniqueObjectTypes::EMAILINONE;
                $metaValue = json_encode($eMIOIntegrationConfig, JSON_THROW_ON_ERROR);
                if (!$objectRegisterMetaRepository->update($eMIOIntegrationConfig[ 'relation' ][ 'integration_object_register_meta_id' ],
                    $eMIOIntegrationConfig[ 'relation' ][ 'integration_objectregister_id' ], $metaKey, $metaValue)) {
                    throw new RuntimeException('Failed to Update the Email server settings to ObjectRegisterMetaRepository');
                }
                $response = $integrationsRepository->getIntegrationConfigForCompany($companyId,
                    UniqueObjectTypes::EMAILINONE);
            } else {
                // insert
                $response = $integrationsRepository->insertIntegrationsConfigForCompany($userId, $campaignId,
                    $companyId,
                    UniqueObjectTypes::EMAILINONE, $eMIOIntegrationConfig);
            }
            $customFields = $customfieldsRepository->getProjectCustomfields($companyId);
            return $this->redirectToRoute('integrations.available', ["status" => "installed"]);

        } catch (JsonException | RuntimeException | Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->render('integrations/wizard/emailinone.twig', [
                'current_user' => $user,
                'emailinone' => $eMIOIntegrationConfig,
                'success' => '',
                'error' => $e->getMessage(),
            ]);
        }
    }

    /**
     * @Route("/account/integration/hubspot/", name="hubspot", methods={"GET","POST"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CustomfieldsRepository $customfieldsRepository
     * @param CampaignRepository $campaignRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param IntegrationTypes $integrationTypes
     * @param IntegrationsRepository $integrationsRepository
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     * @internal HUBSPOT Integration for GET and POST.
     */
    public function hubspot(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        CustomfieldsRepository $customfieldsRepository,
        CampaignRepository $campaignRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        IntegrationTypes $integrationTypes,
        IntegrationService $integrationService,
        IntegrationsRepository $integrationsRepository,
        LoggerInterface $logger
    ): Response {
        $hubspot = UniqueObjectTypes::HUBSPOT;
        $endPoint = $_ENV[ 'MSHB_URL' ];
        try {
            $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
            if (!isset($user[ 'company_id' ], $user[ 'user_id' ]) || empty($user)) {
                throw new RuntimeException('Invalid User');
            }
            $project = $campaignRepository->getProjectDetails($user[ 'company_id' ]);
            if (empty($project) || !isset($project[ 'id' ])) {
                throw new RuntimeException('Failed to get Project Information');
            }
            $hbConfig = $integrationsRepository->getIntegrationConfigForCompany($user[ 'company_id' ],
                $hubspot); // GET HUBSPOT FROM DATABASE
            $customFields = $customfieldsRepository->getProjectCustomfields($user[ 'company_id' ],
                UniqueObjectTypes::CUSTOMFIELDS); // GET FROM DB
            // GET METHOD
            if ($request->getMethod() === 'GET') {
                return $this->render('integrations/wizard/hubspot.twig',
                    [
                        'current_user' => $user,
                        'hb' => $hbConfig,
                        'mio_customfields' => $customFields,
                        'endpoint' => $endPoint,
                    ]);
            }
            // POST METHOD
            if ($request->getMethod() === 'POST') {
                if (empty($hbConfig)) { // GET DEFAULT HUBSPOT TYPE
                    $hbConfig = $integrationTypes->getHubspotConfig();
                }
                $hbConfig[ 'settings' ][ 'apikey' ] = $request->get('apikey');
                $hbConfig[ 'settings' ][ 'owner_id' ] = $request->get('owner_id');
                $hbConfig[ 'settings' ][ 'mapping' ] = $request->get('mapping');

                $hbConfig = $integrationService->autoCreateHubspotCustomField($hbConfig);

                if ((int)$hbConfig[ 'relation' ][ 'integration_object_register_meta_id' ] > 0) { // UPDATE HUBSPOT TO DB
                    $metaValue = json_encode($hbConfig, JSON_THROW_ON_ERROR);
                    if (!$objectRegisterMetaRepository->update($hbConfig[ 'relation' ][ 'integration_object_register_meta_id' ],
                        $hbConfig[ 'relation' ][ 'integration_objectregister_id' ], $hubspot, $metaValue)) {
                        throw new RuntimeException('Failed to Update Hubspot Configuration');
                    }
                } elseif (empty($integrationsRepository->insertIntegrationsConfigForCompany($user[ 'user_id' ],
                    $project[ 'id' ], $user[ 'company_id' ], $hubspot, $hbConfig))) { // INSERT TO DB
                    throw new RuntimeException('Failed to Insert the Hubspot Configuration');
                }
            }
        } catch (RuntimeException | Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->render('integrations/wizard/hubspot.twig', [
                'current_user' => $user,
                'hb' => $hbConfig,
                'success' => '',
                'error' => $e->getMessage(),
            ]);
        }
        return $this->redirectToRoute('integrations.available', ["status" => "installed"]);
    }


    /**
     * @Route("/account/integration/webinaris/", name="webinaris", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function webinaris(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        CampaignRepository $campaignRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        IntegrationsRepository $integrationsRepository,
        IntegrationTypes $integrationTypes
    ): Response {
        $webinaris = UniqueObjectTypes::WEBINARIS;
        $endPoint = $_ENV[ 'MSWEBINARIS_URL' ];
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (!isset($user[ 'company_id' ], $user[ 'user_id' ]) || empty($user)) {
            throw new RuntimeException('Invalid User');
        }
        $project = $campaignRepository->getProjectDetails($user[ 'company_id' ]);
        if (empty($project) || !isset($project[ 'id' ])) {
            throw new RuntimeException('Failed to get Project Information');
        }
        $webiConfig = $integrationsRepository->getIntegrationConfigForCompany($user[ 'company_id' ], $webinaris);
        if ($request->getMethod() === 'GET') { // GET METHOD
            return $this->render('integrations/wizard/webinaris.twig',
                ['current_user' => $user, 'webi' => $webiConfig, 'endpoint' => $endPoint]);
        }
        if ($request->getMethod() === 'POST') { // POST METHOD
            $webiConfig = empty($webiConfig) ? $integrationTypes->getWebinarisConfig() : $webiConfig;
            $webiConfig[ 'settings' ][ 'user_id' ] = $request->get('user-id');
            $webiConfig[ 'settings' ][ 'password' ] = $request->get('password');
            if ((int)$webiConfig[ 'relation' ][ 'integration_object_register_meta_id' ] > 0) { // UPDATE TO DB
                $metaValue = json_encode($webiConfig, JSON_THROW_ON_ERROR);
                if (!$objectRegisterMetaRepository->update($webiConfig[ 'relation' ][ 'integration_object_register_meta_id' ],
                    $webiConfig[ 'relation' ][ 'integration_objectregister_id' ], $webinaris, $metaValue)) {
                    throw new RuntimeException('Failed to Update Digistore Configuration');
                }
            } elseif (empty($integrationsRepository->insertIntegrationsConfigForCompany($user[ 'user_id' ],
                $project[ 'id' ], $user[ 'company_id' ], $webinaris, $webiConfig))) { // INSERT TO DB
                throw new RuntimeException('Failed to Insert the Digistore Configuration');
            }
            return $this->redirectToRoute('integrations.available', ["status" => "installed"]);
        }
        return $this->render('integrations/wizard/webinaris.twig', ['current_user' => $user, 'digi' => $webiConfig]);
    }

    /**
     * @Route("/account/integration/digistore/", name="digistore", methods={"GET","POST"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignRepository $campaignRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param IntegrationsRepository $integrationsRepository
     * @param IntegrationTypes $integrationTypes
     * @return Response
     * @throws JsonException
     * @author Pradeep
     */
    public function digistore(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        CampaignRepository $campaignRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        IntegrationsRepository $integrationsRepository,
        IntegrationTypes $integrationTypes
    ): Response {
        $digistore = UniqueObjectTypes::DIGISTORE;
        $endpoint = $_ENV[ 'MSDIGISTORE_URL' ];
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (!isset($user[ 'company_id' ], $user[ 'user_id' ]) || empty($user)) {
            throw new RuntimeException('Invalid User');
        }
        $project = $campaignRepository->getProjectDetails($user[ 'company_id' ]);
        if (empty($project) || !isset($project[ 'id' ])) {
            throw new RuntimeException('Failed to get Project Information');
        }

        $digiConfig = $integrationsRepository->getIntegrationConfigForCompany($user[ 'company_id' ], $digistore);
        if ($request->getMethod() === 'GET') { // GET METHOD
            return $this->render('integrations/wizard/digistore.twig',
                ['current_user' => $user, 'digi' => $digiConfig, 'endpoint' => $endpoint]);
        }
        if ($request->getMethod() === 'POST') { // POST METHOD
            $digiConfig = empty($digiConfig) ? $integrationTypes->getDigistoreConfig() : $digiConfig;
            $digiConfig[ 'settings' ][ 'apikey' ] = $request->get('apikey');
            if ((int)$digiConfig[ 'relation' ][ 'integration_object_register_meta_id' ] > 0) { // UPDATE TO DB
                $metaValue = json_encode($digiConfig, JSON_THROW_ON_ERROR);
                if (!$objectRegisterMetaRepository->update($digiConfig[ 'relation' ][ 'integration_object_register_meta_id' ],
                    $digiConfig[ 'relation' ][ 'integration_objectregister_id' ], $digistore, $metaValue)) {
                    throw new RuntimeException('Failed to Update Digistore Configuration');
                }
            } elseif (empty($integrationsRepository->insertIntegrationsConfigForCompany($user[ 'user_id' ],
                $project[ 'id' ], $user[ 'company_id' ], $digistore, $digiConfig))) { // INSERT TO DB
                throw new RuntimeException('Failed to Insert the Digistore Configuration');
            }
            return $this->redirectToRoute('integrations.available', ["status" => "installed"]);
        }
        return $this->render('integrations/wizard/digistore.twig', ['current_user' => $user, 'digi' => $digiConfig]);
    }
}
