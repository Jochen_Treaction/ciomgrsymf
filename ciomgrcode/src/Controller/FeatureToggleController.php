<?php

namespace App\Controller;

use App\Entity\FeatureToggle;
use App\Form\FeatureToggleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AuthenticationService;

/**
 * @Route("/feature/toggle")
 */
class FeatureToggleController extends AbstractController
{
    protected $authenticationUtils;
    protected $authenticationService;
    protected $user;

    public function __construct(AuthenticationUtils $authenticationUtils, AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
        $this->authenticationUtils = $authenticationUtils;
        $this->user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
    }


    /**
     * @Route("/", name="feature_toggle_index", methods={"GET"})
     */
    public function index(): Response
    {
        $featureToggles = $this->getDoctrine()
            ->getRepository(FeatureToggle::class)
            ->findAll();

        return $this->render('feature_toggle/index.html.twig', [
            'feature_toggles' => $featureToggles,
            'current_user' => $this->user,
        ]);
    }

    /**
     * @Route("/new", name="feature_toggle_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $featureToggle = new FeatureToggle();
        $form = $this->createForm(FeatureToggleType::class, $featureToggle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($featureToggle);
            $entityManager->flush();

            return $this->redirectToRoute('feature_toggle_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('feature_toggle/new.html.twig', [
            'feature_toggle' => $featureToggle,
            'form' => $form->createView(),
            'current_user' => $this->user,
        ]);
    }

    /**
     * @Route("/{id}", name="feature_toggle_show", methods={"GET"})
     */
    public function show(FeatureToggle $featureToggle): Response
    {
        return $this->render('feature_toggle/show.html.twig', [
            'feature_toggle' => $featureToggle,
            'current_user' => $this->user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="feature_toggle_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FeatureToggle $featureToggle): Response
    {
        $form = $this->createForm(FeatureToggleType::class, $featureToggle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('feature_toggle_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('feature_toggle/edit.html.twig', [
            'feature_toggle' => $featureToggle,
            'form' => $form->createView(),
            'current_user' => $this->user,
        ]);
    }

    /**
     * @Route("/{id}", name="feature_toggle_delete", methods={"POST"})
     */
    public function delete(Request $request, FeatureToggle $featureToggle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$featureToggle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($featureToggle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('feature_toggle_index', ['current_user' => $this->user], Response::HTTP_SEE_OTHER);
    }
}
