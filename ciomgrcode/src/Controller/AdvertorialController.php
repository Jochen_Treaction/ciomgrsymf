<?php


namespace App\Controller;
ini_set('post_max_size', '-1');

use App\Repository\AdvertorialRepository;
use App\Repository\ObjectRegisterRepository;
use App\Services\AdvertorialService;
use App\Services\AppCacheService;
use App\Services\AuthenticationService;
use App\Services\UtilsService;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Repository\SeedPoolRepository;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @Route("/advertorial")
 */
class AdvertorialController extends AbstractController
{
    private AuthenticationUtils $authenticationUtils;
    private AuthenticationService $authenticationService;
    private AppCacheService $apcuCacheService;
    private SeedPoolRepository $seedPoolRepository;
    private UtilsService $utilsService;
    private AdvertorialService $advertorialService;
    private LoggerInterface $logger;


    public function __construct(
        AuthenticationUtils   $authenticationUtils,
        AuthenticationService $authenticationService,
        UtilsService          $utilsService,
        AppCacheService       $apcuCacheService,
        AdvertorialService    $advertorialService,
        SeedPoolRepository    $seedPoolRepository,
        LoggerInterface       $logger
    )
    {
        $this->seedPoolRepository = $seedPoolRepository;
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->apcuCacheService = $apcuCacheService;
        $this->utilsService = $utilsService;
        $this->advertorialService = $advertorialService;
        $this->logger = $logger;

    }


    /**
     * start the overview
     * @Route( "/overview", name="advertorialOverview", methods={"GET"}),
     */
    public function advertorialOverview(AdvertorialRepository $advertorialRepository, SeedPoolRepository $seedPoolRepository): Response
    {
        $currentUser = $this->getCurrentUser();
        $advertorialRecords = $advertorialRepository->getAdvertorialsByCompanyId($currentUser['company_id']);
        $seedPoolGroups = $seedPoolRepository->getSeedPoolGroups();
        $advertorialOverviewTwigArray = $this->getAdvertorialOverviewTwigArray($advertorialRecords, $currentUser);

        return $this->render('advertorials/advertorial_overview.twig', $advertorialOverviewTwigArray);
    }


    /**
     * start the wizard
     * @Route( "/wizard", name="advertorialWizard", methods={"GET"}),
     */
    public function advertorialWizard(): Response
    {
        $currentUser = $this->getCurrentUser();
        $seedPoolGroups = $this->seedPoolRepository->getSeedPoolGroupsByCompanyIdForAdvertorial($currentUser['company_id']);
        return $this->render(
            'advertorials/advertorialswizard.twig',
            [
                'current_user' => $currentUser,
                'seed_pool_groups' => $seedPoolGroups,
                'company_id' => $currentUser['company_id'],
                'company_name' => $currentUser['company_name']
            ]
        );
    }


    /**
     * start the wizard
     * @Route( "/edit/{advertorial_id}", name="advertorialEdit", methods={"GET"}),
     */
    public function advertorialEdit(Request $request, AdvertorialRepository $advertorialRepository): Response
    {
        $advertorialId = $request->get('advertorial_id');
        $currentUser = $this->getCurrentUser();
        $advertorial = $advertorialRepository->getAdvertorialById($advertorialId);

        $advertorial['content_uploaded_html_file'] = $this->b64Decompress($advertorial['content_uploaded_html_file']);
        $advertorial['content_extract_images'] = $this->b64Decompress($advertorial['content_extract_images']);
        $advertorial['content_inline_css'] = $this->b64Decompress($advertorial['content_inline_css']);
        $advertorial['content_clean_html'] = $this->b64Decompress($advertorial['content_clean_html']);

        $seedPoolGroups = $this->seedPoolRepository->getSeedPoolGroupsByCompanyId($currentUser['company_id']);
        return $this->render('advertorials/advertorialswizard_edit.twig', ['current_user' => $currentUser, 'seed_pool_groups' => $seedPoolGroups, 'advertorial' => $advertorial]);
    }


    /**
     *  delete advertorial by get request, then show advertorial overview
     * @Route( "/delete/{objectregister_id}", name="advertorialDelete", methods={"GET"}),
     */
    public function ajaxAdvertorialDelete(Request $request, LoggerInterface $logger, ObjectRegisterRepository $objectRegisterRepository): Response
    {
        $deleted = 0;

        $objectregisterId = $request->get('objectregister_id');
        $deleted += (int)$objectRegisterRepository->delete($objectregisterId); // deletes cascading referenced advertorials record
        $logger->info("deleted ($deleted) of advertorial-ObjRegId $objectregisterId", [__METHOD__, __LINE__]);

        return $this->json(['success' => ($deleted === 0) ? false : true, 'recordsDeleted' => $deleted]);
    }


    /**
     * call microservice mshtmloptimiser without (!!!) inline css TODO: microservice must one file: a cleaned up HTML to store it to a database table
     * @Route( "/cleanup/html/", name="advertorialCleanupHtml", methods={"POST"})
     */
    public function advertorialCleanupHtml(Request $request, LoggerInterface $logger, HttpClientInterface $httpClient): JsonResponse
    {

        $body = $request->getContent();

        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ],
            'body' => $body,
        ];

        try {
            $msHtmlOptimiserUrl = $this->getParameter('mshtmloptimiserurl');
            // $response contains: ['success' => true, 'msg' => 'done', 'cleanhtml' => base64_encode($cleanhtml)]
            $response = $httpClient->request('POST', $msHtmlOptimiserUrl . '/advertorial/cleanup/html', $options); // TODO: remove hard coding in new code !!!!q
        } catch (\Exception $e) {
            return $this->json(['response' => ['success' => false, 'msg' => $e->getMessage(), 'cleanhtml' => '']]);
        }

        if ($response->getStatusCode() === 200) {
            return $this->json($response->toArray());
        }

        return $this->json(['response' => ['success' => false, 'msg' => $response->getStatusCode() ]]);
    }


    /**
     * call microservice mshtmloptimiser with inline css TODO: microservice must return two files: a inlineCss-HTML  plus  a cleaned up inlineCss-HTML to store them to a database table
     * @Route( "/inlinecss/cleanup/html/", name="advertorialInlineCssCleanupHtml", methods={"POST"})
     */
    public function advertorialInlineCssCleanupHtml(Request $request, LoggerInterface $logger, HttpClientInterface $httpClient): JsonResponse
    {

        $body = $request->getContent();

        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ],
            'body' => $body,
        ];

        try {
            $msHtmlOptimiserUrl = $this->getParameter('mshtmloptimiserurl');
            $logger->info('$msHtmlOptimiserUrl', [$msHtmlOptimiserUrl, __METHOD__, __LINE__]);
            // $response contains: ['success' => true, 'msg' => 'done', 'cleanhtml' => base64_encode($cleanhtml), 'inlineCssHtml' => base64_encode($inlineCssHtml)]
            $response = $httpClient->request('POST', $msHtmlOptimiserUrl . '/advertorial/inlinecss/cleanup/html', $options); // TODO: remove hard coding in new code !!!!
            $logger->info('$response', [$response, __METHOD__, __LINE__]);
        } catch (\Exception $e) {
            return $this->json(['response' => ['success' => false, 'msg' => $e->getMessage(), 'cleanhtml' => '', 'inlineCssHtml' => '']]);
        }

        if ($response->getStatusCode() === 200) {
            return $this->json($response->toArray());
        }

        $logger->error('$response->getStatusCode', [$response->getStatusCode(), __METHOD__, __LINE__]);

        return $this->json(['response' => ['success' => false, 'msg' => 'oops ..., something went wrong', 'cleanhtml' => '', 'inlineCssHtml' => '']]);
    }


    /**
     * @Route( "/surrogateid", name="advertorialSurrogateId", methods={"POST"}),
     */
    public function getadvertorialSurrogateId(Request $request): JsonResponse
    {
        $fileName = $request->getContent();
        return $this->json(['status' => true, 'id' => hash('md5', $fileName . date('U') . uniqid('advertorialSurrogateId', true))]);
    }


    /**
     * @Route( "/processzipfile", name="processZipFile", methods={"POST"})
     * @author Aki
     * @internal Process zip file i.e unzip the file and find the html file and return the html file
     */
    public function processZipFile(Request $request): JsonResponse
    {
        //Get the params and file from request
        $advertorialSurrogateId = $request->get('advertorialSurrogateId');
        $companyId = (int)$request->get('companyId');
        $zipFile = $request->files->get('zipFile');
        $uploadDir = $this->getParameter('advertorials_upload_dir');
        //Generate a dynamic file name
        $fileExtension = $zipFile->getClientOriginalExtension();
        $fileName = $advertorialSurrogateId . '_' . $companyId . '_' . date('dmyHis') . '.' . $fileExtension;
        //Error for unsupported zip formates
        if ($fileExtension !== "zip") {
            $htmlFileContent = "<p><span class='bad'>Zip File Formate not correct</span></p>";
            //return proper error html
            return $this->json([
                'status' => false,
                'content' => $htmlFileContent,
            ]);
        }
        //upload zip file locally to files folder (path z.B '../files/advertorials/178/a3917191a76e90e9ef58ef90f70876ae')
        $destination = $uploadDir . DIRECTORY_SEPARATOR . $companyId . DIRECTORY_SEPARATOR . $advertorialSurrogateId;
        $zipFile->move($destination, $fileName);
        //unzip the file
        if (!$this->advertorialService->unZipFile($destination, $fileName)) {
            $htmlFileContent = "<p><span class='bad'>unable to unzip the file</span></p>";
            //return error
            return $this->json([
                'status' => false,
                'content' => $htmlFileContent,
            ]);
        }
        //Search for html file
        $htmlFileInfo = $this->advertorialService->getHtmlFileInfoFromUploadedZip($destination);
        //error for no html found
        if (is_null($htmlFileInfo)) {
            $htmlFileContent = "<p><span class='bad'>No HTML file found</span></p>";
            //return proper error html
            return $this->json([
                'status' => false,
                'content' => $htmlFileContent,
            ]);
        }

        //get the html file contents
        $htmlFileContent =
            file_get_contents($htmlFileInfo[AdvertorialService::DIRECTORY] . DIRECTORY_SEPARATOR . $htmlFileInfo[AdvertorialService::FILENAME]);

        return $this->json([
            'status' => true,
            'content' => $htmlFileContent,
            'htmlFilePath' => $htmlFileInfo[AdvertorialService::DIRECTORY],
            'htmlFileName' => $htmlFileInfo[AdvertorialService::FILENAME]
        ]);
    }


    /**
     * save wizard data
     * @Route( "/save", name="advertorialSave", methods={"POST"}),
     */
    public function ajaxAdvertorialSave(
        Request                  $request, LoggerInterface $logger,
        ObjectRegisterRepository $objectRegisterRepository,
        AdvertorialRepository    $advertorialRepository,
        AppCacheService          $appCacheService
    ): JsonResponse {
        $advertorialId = 0;

        $encodedContent = $request->getContent();//getData
        $decodedContent = base64_decode($encodedContent);
        $content = json_decode($decodedContent, true, 512, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
        $logger->info('request $contentArr', [$content, __METHOD__, __LINE__]);

        // check if images where provided and try to get them from the cache (apcu or redis)
        $images = []; // initialize $images

        if (!empty($content['imagesKey'])) {
            $logger->info('imagesKey', [$content['imagesKey'], __METHOD__, __LINE__]);
            $images = json_decode($appCacheService->get($content['imagesKey']), true, 100, JSON_OBJECT_AS_ARRAY);
            $logger->info('images len from redis', [strlen(print_r($images, true)), __METHOD__, __LINE__]);

            if (empty($images) || !is_array($images)) {
                $logger->warning("no images found for {$content['advertorialName']}", [__METHOD__, __LINE__]);
            }

            // write log in dev
            if ($_ENV['APP_ENV'] === 'dev') {
                foreach ($images as $k => $img) {
                    if (isset($img['image_hash_name'])) {
                        $logger->info("img from redis: ($k)  {$img['image_hash_name']}", [__METHOD__, __LINE__]);
                    }

                    if (isset($img['transfer_status'])) {
                        $logger->info("img transfer_status from redis: ($k)  {$img['transfer_status']}", [__METHOD__, __LINE__]);
                    }
                }
            }
        }

        $objectRegisterId = $objectRegisterRepository->createObjectRegisterEntryAdvertorial(
            $content['advertorialName'],
            $content['advertorialDescription'],
            $content['companyId'],
            $content['seedPoolGroupName']
        );

        $logger->info('$objectRegisterId', [$objectRegisterId, __METHOD__, __LINE__]);

        if (null !== $objectRegisterId) {
            // TODO: call advertorialRepository setAdvertorial
            $currentUser = $this->getCurrentUser();
            $advertorialRepository->setRepostitoriesCurrentUserId($currentUser['user_id']);
            $advertorialId = $advertorialRepository->setAdvertorial(
                $objectRegisterId,
                $content['companyId'],
                $content['seedPoolGroupId'],
                $content['advertorialSurrogateId'],
                $content['advertorialName'],
                $content['advertorialDescription'],
                $content['advertorialBeginDate'],
                $content['advertorialEndDate'],
                $content['fileNameHtml'],
                $this->b64Compress($content['contentUploadedFile']),
                $this->b64Compress($content['contentExtractImages']),
                $this->b64Compress($content['contentInlineCss']),
                $this->b64Compress($content['contentCleanHtml']),
                $content['fileNameZip'],
                $content['contentUploadedZipFile']);
        }

        $logger->info('$advertorialId', [$advertorialId, __METHOD__, __LINE__]);

        if (!empty($images) && !empty($advertorialId)) {
            $logger->info('count of $images', [count($images), __METHOD__, __LINE__]);


            if ($_ENV['APP_ENV'] === 'dev') {
                file_put_contents(__DIR__ . '/../../var/log/imagesfromRedis.json', json_encode($images, JSON_PRETTY_PRINT));
            }

            $advertorialImageIds = $advertorialRepository->createAvertorialImages($advertorialId, $images);
            $logger->info('saved $advertorialImageIds', [$advertorialImageIds, __METHOD__, __LINE__]);
        } else {
            $advertorialImageIds = [];
            $logger->warning("did not write images to database. either \$images was empty or \$advertorialId was empty (advert not stored to DB)", ['images' => $images, 'advertorialId' => $advertorialId, __METHOD__, __LINE__]);
        }

        if (0 === $advertorialId) {
            return $this->json(['success' => false, 'msg' => "error: advertorial could not be saved"]);
        } else {
            return $this->json(['success' => true, 'msg' => "advertorial was saved with id=$advertorialId"]);
        }
    }



    /**
     * update advertorial by ajax request, returns json
     * @Route( "/update", name="advertorialUpdate", methods={"POST"}),
     */
    public function ajaxAdvertorialUpdate(Request $request, LoggerInterface $logger, ObjectRegisterRepository $objectRegisterRepository, AdvertorialRepository $advertorialRepository, SeedPoolRepository $seedPoolRepository): JsonResponse
    {
        $failMsg = '';
        $advertorialId = 0;

        try {
            $encodedContent = $request->getContent();//getData
            $decodedContent = base64_decode($encodedContent);
            $content = json_decode($decodedContent, true, 512, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
            $logger->info('request $contentArr', [$content, __METHOD__, __LINE__]);
        } catch (\Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->json(['success' => false, 'msg' => 'error: advertorial update content could not be extracted', 'details' => $e->getMessage()]);
        }

        if (!empty($content['objectRegisterId'])) {
            $seedPoolGroupName = $seedPoolRepository->getSeedPoolGroupNameById($content['seedPoolGroupId']);
            $comment = substr("Ad: {$content['advertorialName']}, CmpnyId: {$content['companyId']}, SdPlGrp: {$seedPoolGroupName}, Desc: {$content['advertorialDescription']}", 0, 254);
            $objectRegisterRepository->updateComment($content['objectRegisterId'], $comment);
        } else {
            $failMsg = "warning: could not update objectRegister, objectRegisterId not provided by request";
        }

        $logger->info('$objectRegisterId', [$content['objectRegisterId'], __METHOD__, __LINE__]);

        if (!empty($content['advertorialId'])) {
            // TODO: call advertorialRepository setAdvertorial
            $currentUser = $this->getCurrentUser();
            $advertorialRepository->setRepostitoriesCurrentUserId($currentUser['user_id']);
            $advertorialId = $advertorialRepository->updateAdvertorial(
                (int)$content['advertorialId'],
                (int)$content['companyId'],
                (int)$content['seedPoolGroupId'],
                (int)$content['advertorialSurrogateId'],
                $content['advertorialName'],
                $content['advertorialDescription'],
                $content['advertorialBeginDate'],
                $content['advertorialEndDate'],
                $content['fileNameHtml'],
                $this->b64Compress($content['contentUploadedFile']),
                $this->b64Compress($content['contentExtractImages']),
                $this->b64Compress($content['contentInlineCss']),
                $this->b64Compress($content['contentCleanHtml']),
                $content['fileNameZip'],
                $content['contentUploadedZipFile']);
        } else {
            $failMsg .= "\nerror: could not update advertorials, advertorialId not provided by request";
        }

        $logger->info('$advertorialId (0=update failed)', [$advertorialId, __METHOD__, __LINE__]);

        if (!empty($failMsg) or empty($advertorialId)) {
            return $this->json(['success' => false, 'msg' => 'error: advertorial could not be updated', 'details' => $failMsg]);
        } else {
            return $this->json(['success' => true, 'msg' => "advertorial was updated with id=$advertorialId"]);
        }
    }


    /**
     * @Route( "/extract/images/html", name="advertorialExtractImagesFromHtml", methods={"POST"}),
     * @param Request $request
     * @return void
     */
    public function ajaxExtractImagesFromHtml(Request $request, AdvertorialService $advertorialService, AppCacheService $appCacheService, HttpClientInterface $httpClient, LoggerInterface $logger): JsonResponse
    {
        $currentUser = $this->getCurrentUser();
        $data = json_decode(base64_decode($request->getContent()), true, 100, JSON_OBJECT_AS_ARRAY);

        $companyId = (int)$data['companyId'];
        $seedPoolGroupId = (int)$data['seedPoolGroupId'];
        $advertorialSurrogateId = $data['advertorialSurrogateId'];
        $htmlFileContent = base64_decode($data['htmlFileContent']);
        $isFileTypeZip = $data['isFileTypeZip'];
        $filePath = null;
        $htmlFileName = null;
        if ($isFileTypeZip) {
            $filePath = $data['filePathInMIO'];
            $htmlFileName = $data['htmlFileName'];
        }
        //$logger->info('$data', ['$companyId' => $companyId,'$advertorialSurrogateId'=>$advertorialSurrogateId, '$htmlFileContent' => substr($htmlFileContent, 0, 100), __METHOD__, __LINE__]);

        $imagesKey = $currentUser['user_id'] . '_' . $advertorialSurrogateId;
        $images = [];

        try {
            $newImgHtml = $advertorialService->extractAndTransferImagesFromHtml($httpClient,
                $companyId,
                $seedPoolGroupId,
                $advertorialSurrogateId,
                $htmlFileContent,
                $images,
                $isFileTypeZip,
                $filePath,
                $htmlFileName
            );
        } catch (\Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->json([
                'status' => false,
                'newImgHtml' => $e->getMessage(),
                'imgProcessLog' => [],
            ]);
        }

        $appCacheService->addOrOverwrite($imagesKey, json_encode($images));

        if ($_ENV['APP_ENV'] === 'dev') {
            try {
                file_put_contents(__DIR__ . '/../../var/log/imageHtmlFile.html', base64_decode($newImgHtml));
                file_put_contents(__DIR__ . '/../../var/log/imageLog.html', $this->getImageProcessLog($images));
            } catch (\Exception $e) {
                $logger->warning($e->getMessage(), [__METHOD__, __LINE__]);
            }
        }

        return $this->json([
            'status' => true,
            'newImgHtml' => $newImgHtml,
            'imagesKey' => $imagesKey,
            'imgProcessLog' => base64_encode($this->getImageProcessLog($images))
        ]);

    }

    /**
     * @Route( "/removeZipFileFromLocalFolder", name="remove_zip_advertorials",  methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @author Aki
     */
    public function removeZipAdvertorialFiles(
        Request $request
    ): JsonResponse
    {

        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->authenticationService->getUserParametersFromCache($userEmail);
        $response = new JsonResponse();
        //basic user check
        if (empty($currentUser) || empty($userEmail)) {
            $message = "Invalid user";
            return $this->utilsService->setResponseWithMessage($response, $message, false);
        }
        //Get the params
        $companyId = $request->get('companyId');
        $zipFileUploadDirectory = $this->getParameter('advertorials_upload_dir');
        $path = $zipFileUploadDirectory.DIRECTORY_SEPARATOR.$companyId;
        //check the ftp connection
        $removalStatus = $this->advertorialService->removeAllFilesAndDirectoryFromFolder($path);
        if (!$removalStatus) {
            $message = "Can\'t remove Upload ZIP File from Files folder";
            return $this->utilsService->setResponseWithMessage($response, $message, false);
        }
        $message = 'success';
        return $this->utilsService->setResponseWithMessage($response, $message, true);
    }


    /**
     * @Route( "/ftpcheck", name="ftp_check",  methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @author Aki
     */
    public function checkFtpConnection(
        Request $request
    ): JsonResponse
    {

        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->authenticationService->getUserParametersFromCache($userEmail);
        $response = new JsonResponse();
        //basic user check
        if (empty($currentUser) || empty($userEmail)) {
            $message = "Invalid user";
            return $this->utilsService->setResponseWithMessage($response, $message, false);
        }
        //Get the params
        $ipAddress = $request->get('ip_address');
        $sftpUserId = $request->get('sftp_user');
        $sftpPassword = $request->get('sftp_password');
        //check the ftp connection
        $data = $this->advertorialService->checkFtpConnection($ipAddress, $sftpUserId, $sftpPassword);
        if (empty($data)) {
            $message = "No data from Mosento API";
            return $this->utilsService->setResponseWithMessage($response, $message, false);
        }
        $message = 'success';
        return $this->utilsService->setResponseWithMessage($response, $message, true, $data);
    }


    /**
     * @param array $images
     * @return string image log as string
     */
    private function getImageProcessLog(array $images): string
    {
        $log = '';

        if ($_ENV['APP_ENV'] === 'dev') {
            file_put_contents(__DIR__ . '/../../var/log/imagesInController.json', json_encode($images, JSON_PRETTY_PRINT));
        }

        foreach ($images as $img) {

            if ($img['scan_status'] === false) {
                $log .= "<p class='bad log'> line {$img['scan_line_number']}, not extracted, {$img['scan_status_reason']}, original url {$img['original_url']} transfer_status: ./.</p>";
            } else {
                // TODO: extract the overall transfer_status
                $log .= "<p class='okay log'>line {$img['scan_line_number']}, extracted, {$img['original_image_name']}, new url {$img['url']}, transfer_status: {$img['transfer_status']}</p>";
            }
        }

        return $log;
    }


    /**
     * prepare data array for twig template advertorials/advertorial_overview.twig
     * @param array $advertorialRecords
     * @param array $currentUser
     * @param array $seedPoolGroups
     * @return array
     */
    private function getAdvertorialOverviewTwigArray(?array $advertorialRecords, array $currentUser): array
    {
        if (null === $advertorialRecords) {
            return [
                'current_user' => $currentUser,
                'account' => $currentUser['company_name'],
                'advertorialRecords' => [],
                'seedPoolGroups' => [],
                'advertorialBeginDateYear' => '',
                'advertorialEndDateYear' => '',
                'advertorialBeginDateMonth' => '',
                'advertorialEndDateMonth' => '',
                'updatedYear' => '',
                'updatedMonth' => '',
            ];
        }

        $advertorialBeginDateYear = [];
        $advertorialEndDateYear = [];
        $advertorialBeginDateMonth = [];
        $advertorialEndDateMonth = [];
        $updatedYear = [];
        $updatedMonth = [];
        $seedPoolGroups = [];

        foreach ($advertorialRecords as $index => &$record) {
            $record['content_uploaded_html_file'] = (!empty($record['content_uploaded_html_file'])) ? $this->b64Decompress($record['content_uploaded_html_file']) : '';
            $record['content_extract_images'] = (!empty($record['content_extract_images'])) ? $this->b64Decompress($record['content_extract_images']) : '';
            $record['content_inline_css'] = (!empty($record['content_inline_css'])) ? $this->b64Decompress($record['content_inline_css']) : '';
            $record['content_clean_html'] = (!empty($record['content_clean_html'])) ? $this->b64Decompress($record['content_clean_html']) : '';

            $advertorialBeginDateYear[] = $record['advertorial_begin_date_year'];
            $advertorialEndDateYear[] = $record['advertorial_end_date_year'];
            $advertorialBeginDateMonth[] = $record['advertorial_begin_date_month'];
            $advertorialEndDateMonth[] = $record['advertorial_end_date_month'];
            $updatedYear[] = $record['updated_year'];
            $updatedMonth[] = $record['updated_month'];
            $seedPoolGroups[] = $record['seed_pool_group_name'];
        }
        unset($record);

        $advertorialBeginDateYear = array_unique($advertorialBeginDateYear);
        $advertorialEndDateYear = array_unique($advertorialEndDateYear);
        $advertorialBeginDateMonth = array_unique($advertorialBeginDateMonth);
        $advertorialEndDateMonth = array_unique($advertorialEndDateMonth);
        $updatedYear = array_unique($updatedYear);
        $updatedMonth = array_unique($updatedMonth);
        $seedPoolGroups = array_unique($seedPoolGroups);

        sort($advertorialBeginDateYear);
        sort($advertorialEndDateYear);
        sort($advertorialBeginDateMonth);
        sort($advertorialEndDateMonth);
        sort($updatedYear);
        sort($updatedMonth);
        sort($seedPoolGroups);

        return [
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'],
            'advertorialRecords' => $advertorialRecords,
            'seedPoolGroups' => $seedPoolGroups,
            'advertorialBeginDateYear' => $advertorialBeginDateYear,
            'advertorialEndDateYear' => $advertorialEndDateYear,
            'advertorialBeginDateMonth' => $advertorialBeginDateMonth,
            'advertorialEndDateMonth' => $advertorialEndDateMonth,
            'updatedYear' => $updatedYear,
            'updatedMonth' => $updatedMonth,
        ];
    }


    /**
     * @param string $b64 base64 encoded string
     * @return string  base64 encode compressed $content
     */
    private function b64Compress(string $b64): string
    {
        return base64_encode(bzcompress(base64_decode($b64), 9));
    }


    /**
     * @param string $b64Zip base64 encode compressed $b64Zip
     * @return string decompressed and then base64 encoded string
     */
    private function b64Decompress(string $b64Zip): string
    {
        return base64_encode(bzdecompress(base64_decode($b64Zip)));
    }


    /**
     * @return array
     * @author jsr
     */
    private function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {

            $company_id = $this->apcuCacheService->get($this->apcuCacheService::SELECTED_COMPANY_ID_OF_USER . $user['email']);
            if (false !== $company_id) {
                $user['company_id'] = $company_id;
            }

            return $user;
        }
        return [];
    }
}
