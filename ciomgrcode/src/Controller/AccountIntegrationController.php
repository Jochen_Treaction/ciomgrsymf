<?php

namespace App\Controller;

use App\Repository\CompanyRepository;
use App\Repository\IntegrationsMetaRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\UsersRepository;
use App\Services\AuthenticationService;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class AccountIntegrationController
 * @package App\Controller
 *
 * @Route("/account")
 *
 */
class AccountIntegrationController extends AbstractController
{

    protected $authenticationUtils;
    protected $authenticationService;
    private $logger;


    /**
     * LeadController constructor.
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @author jsr
     */
    public function __construct(
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService,
        LoggerInterface $logger
    ) {
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param CompanyRepository $company
     * @param AuthenticationUtils $authenticationUtils
     * @param UsersRepository $usersRepository
     * @return Response
     * Route No. 92, Refactorname accountintegration.getupdatesettingsdigistore
     * @Route( "/settings/digistore", name="acc.getSettingsDigistore", methods={"GET"})
     * @author PVA | jsr
     * @deprecated since version 2020 after change in design in Database
     */
    public function getSettingsDigistore(
        Request $request,
        CompanyRepository $company,
        AuthenticationUtils $authenticationUtils,
        UsersRepository $usersRepository
    ): Response {
        $userEmail = $authenticationUtils->getLastUsername();
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }

        $user = $this->getCurrentUser();
        $response[ 'current_user' ] = $user;
        $id = $user[ 'company_id' ];
        $cnf_tgt = 'Digistore';
        $response[ 'digistore' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        return $this->render('/account/account_integrations/digistore.twig', $response);
    }

    /**
     * @return array
     * @author jsr
     */
    public function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {
            return $user;
        }

        return [];
    }

    /**
     * @param Request $request
     * @param CompanyRepository $company
     * @return Response
     * Route No. 92.1, Refactorname accountintegration.postupdatesettingsdigistore
     * @Route( "/settings/digistore", name="acc.postSettingsDigistore", methods={"POST"})
     * @author PVA | jsr
     * @deprecated since version 2020 after change in design in Database
     */
    public function settingsDigistore(Request $request, CompanyRepository $company): Response
    {
        $user = $this->getCurrentUser();
        $response[ 'current_user' ] = $user;
        $id = $user[ 'company_id' ];
        $cnf_tgt = 'Digistore';
        $response[ 'digistore' ] = '';
        $token = $request->get('token');
        if (!$this->isCsrfTokenValid('company-digistore', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        $cnf_tgt_settings[ 'apikey' ] = $request->get('apikey');
        $cnf_tgt_settings[ 'id' ] = $request->get('id');

        if ($company->storeIntegrationsForCompany($id, $cnf_tgt, $cnf_tgt_settings)) {
            $response[ 'digistore' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        }

        return $this->render('/account/account_integrations/digistore.twig', $response);
    }

    /**
     * Route No. 93, Refactorname accountintegration.getdigistoresettingsbyid
     * $app->get('/account/{acc_id}/updateDigistore', 'AccountIntegrationController:getDigistoreSettingsById')->setName('acc.digistore');
     * #Route( "/account/{acc_id}/updateDigistore", name="acc.digistore", methods={"GET"}) !twig
     * @Route( "/{acc_id}/Digistore", name="acc.getSettingsDigistoreById", methods={"GET"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function getDigistoreSettingsById()
    {
    }


    /**
     *
     * Route No. 94, Refactorname accountintegration.postdigistoresettingsbyid
     * #Route( "/account/{acc_id}/updateDigistore", name="acc.digistore", methods={"POST"}) !twig
     * @Route( "/{acc_id}/update/Digistore", name="acc.postDigistoreSettingsById", methods={"POST"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function postDigistoreSettingsById()
    {
    }


    /**
     * @param CompanyRepository $company
     * @param AuthenticationUtils $authenticationUtils
     * @param UsersRepository $usersRepository
     * @return Response
     * @author pva | jsr
     * Route No. 95, Refactorname accountintegration.getsettingswebinaris
     * @Route( "/settings/webinaris", name="acc.getsettingsWebinaris", methods={"GET"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function getSettingsWebinaris(
        CompanyRepository $company,
        AuthenticationUtils $authenticationUtils,
        UsersRepository $usersRepository
    ): Response {

        $userEmail = $authenticationUtils->getLastUsername();
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }

        $user = $this->getCurrentUser();
        $response[ 'current_user' ] = $user;
        $id = $user[ 'company_id' ];
        $cnf_tgt = 'Webinaris';
        $response[ 'current_user' ] = $this->getCurrentUser();
        $response[ 'webinaris' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        return $this->render('/account/account_integrations/webinaris.twig', $response);
    }

    /**
     * @param Request $request
     * @param CompanyRepository $company
     * @return Response
     * @author pva | jsr
     * Route No. 95.1, Refactorname accountintegration.postsettingswebinaris
     * @Route( "/settings/webinaris", name="acc.postsettingsWebinaris", methods={"POST"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function setSettingsWebinaris(Request $request, CompanyRepository $company): Response
    {
        $cnf_tgt = 'Webinaris';
        $user = $this->getCurrentUser();
        $response[ 'current_user' ] = $user;
        $response[ 'webinaris' ] = '';

        $company_id = $user[ 'company_id' ];
        $token = $request->get('token');
        if (!$this->isCsrfTokenValid('company-webinaris', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        $cnf_tgt_settings[ 'apiPassword' ] = $request->get('apiPassword');
        $cnf_tgt_settings[ 'id' ] = $request->get('id');

        if ($company->storeIntegrationsForCompany($company_id, $cnf_tgt, $cnf_tgt_settings)) {
            $response[ 'webinaris' ] = $company->getIntegrationsForCompany($company_id, $cnf_tgt);
        }

        return $this->render('/account/account_integrations/webinaris.twig', $response);
    }

    /**
     *
     * Route No. 96, Refactorname accountintegration.getwebinarissettingsbyid
     * #Route( "/account/{acc_id}/updateWebinaris", name="acc.Webinaris", methods={"GET"})
     * @Route( "/{acc_id}/Webinaris", name="acc.getWebinaris", methods={"GET"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function getWebinarisSettingsById()
    {
    }


    /**
     *
     * Route No. 97, Refactorname accountintegration.postwebinarissettingsbyid
     * #Route( "/account/{acc_id}/updateWebinaris", name="acc.Webinaris", methods={"POST"})
     * @Route( "/{acc_id}/update/Webinaris", name="acc.updateWebinaris", methods={"POST"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function postWebinarisSettingsById()
    {
    }


    /**
     *
     * Route No. 98, Refactorname accountintegration.getSettingsEmail
     * #Route( "/accountsettingsEmail", name="acc.settingsEmail", methods={"GET"})
     * @Route( "/settings/email", name="acc.getSettingsEmail", methods={"GET"})
     * @param CompanyRepository $company
     * @return Response
     * @deprecated since version 2020 after change in design in Database
     */
    public function getSettingsEmail(
        CompanyRepository $company,
        AuthenticationUtils $authenticationUtils,
        UsersRepository $usersRepository
    ): Response {
        $userEmail = $authenticationUtils->getLastUsername();
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }

        $user = $this->getCurrentUser();
        $id = $user[ 'company_id' ];
        $cnf_tgt = 'Email';
        $response[ 'current_user' ] = $user;
        $response[ 'email' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        $response[ 'b64msmail' ] = base64_encode($_ENV[ 'MSMAIL_URL' ] . '/email/test_cio_config');
        return $this->render('/account/account_integrations/email.twig', $response);
    }

    /**
     *
     * Route No. 98.1, Refactorname accountintegration.postSettingsEmail
     * @Route( "/settings/email", name="acc.postSettingsEmail", methods={"POST"})
     * @param Request $request
     * @param CompanyRepository $company
     * @return Response
     * @author pva | jsr
     * @deprecated since version 2020 after change in design in Database
     */
    public function setSettingsEmail(Request $request, CompanyRepository $company): Response
    {
        $user = $this->getCurrentUser();
        $cnf_tgt = 'Email';
        $response[ 'current_user' ] = $user;
//        $response['host'] = $request->get('host');
//        $response['email'] = $request->get('user');
//        $response['port'] = $request->get('port');
        $response[ 'b64msmail' ] = base64_encode($_ENV[ 'MSMAIL_URL' ] . '/email/test_cio_config');
        $this->logger->info('$response=' . json_encode($response), [__METHOD__, __LINE__]);


        $id = $user[ 'company_id' ];
        $token = $request->get('token');
        if (!$this->isCsrfTokenValid('company-email', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        $cnf_tgt_settings[ 'host' ] = $request->get('host');
        $cnf_tgt_settings[ 'id' ] = $request->get('id');
        $cnf_tgt_settings[ 'password' ] = $request->get('password');
        $cnf_tgt_settings[ 'port' ] = $request->get('port');
        $cnf_tgt_settings[ 'smtpAuth' ] = $request->get('smtpAuth');
        $cnf_tgt_settings[ 'secureProtocol' ] = $request->get('secureProtocol');
        $cnf_tgt_settings[ 'user' ] = $request->get('user');

        if ($company->storeIntegrationsForCompany($id, $cnf_tgt, $cnf_tgt_settings)) {
            $response[ 'email' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);

        }
        return $this->render('/account/account_integrations/email.twig', $response);
    }

    /**
     *
     * Route No. 99, Refactorname accountintegration.getemailsettingsyid
     * #Route( "/account/{acc_id}/updateEmail", name="acc.Email", methods={"GET"})
     * @Route( "/{acc_id}/Email", name="acc.getEmail", methods={"GET"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function getEmailSettingsyId()
    {
    }


    /**
     *
     * Route No. 100, Refactorname accountintegration.postemailsettingsbyid
     * #Route( "/account/{acc_id}/updateEmail", name="acc.Email", methods={"POST"})
     * @Route( "/{acc_id}/update/Email", name="acc.updateEmail", methods={"POST"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function postEmailSettingsById()
    {
    }


    /**
     *
     * Route No. 101, Refactorname accountintegration.getsettingsfraud
     * #Route( "/accountsettingsFraud", name="acc.settingsFraud", methods={"GET"})
     * @Route( "/settings/fraud", name="acc.getSettingsFraud", methods={"GET"})
     * @param Request $request
     * @param CompanyRepository $company
     * @param AuthenticationUtils $authenticationUtils
     * @param UsersRepository $usersRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @return Response
     * @author jsr
     */
    public function getSettingsFraud(
        Request $request,
        CompanyRepository $company,
        AuthenticationUtils $authenticationUtils,
        UsersRepository $usersRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository
    ): Response {
        $userEmail = $authenticationUtils->getLastUsername();
        $metaKey = $objectRegisterMetaRepository::MetaKeyFraudSettings;
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }
        $user = $this->getCurrentUser();
        $companyId = $user[ 'company_id' ];
        $cnf_tgt = 'FraudSettings';
        $response[ 'current_user' ] = $user;
        $accountObjRegId = $objectRegisterRepository->getCompanyObjectRegisterId($companyId);
        $response[ 'fraud' ] = $objectRegisterMetaRepository->getJsonMetaValueAsArray($accountObjRegId, $metaKey);
        return $this->render('/account/account_integrations/fraud.twig', $response);
    }


    /**
     *
     * Route No. 101.1, Refactorname accountintegration.postsettingsfraud
     * #Route( "/accountsettingsFraud", name="acc.settingsFraud", methods={"POST"})
     * @Route( "/settings/fraud", name="acc.postSettingsFraud", methods={"POST"})
     * @param Request $request
     * @param CompanyRepository $company
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @return Response
     */
    public function setSettingsFraud(
        Request $request,
        CompanyRepository $company,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository
    ): Response {
        $user = $this->getCurrentUser();
        $companyId = $user[ 'company_id' ];
        $metaKey = $objectRegisterMetaRepository::MetaKeyFraudSettings;
        $response[ 'fraud' ] = '';
        $response[ 'current_user' ] = $user;
        $token = $request->get('token');

        if (!$this->isCsrfTokenValid('company-fraud', $token)) {
            return $this->redirect('/login/signin.twig');
        }

        try {
            $fraudSettings[ 'maxLeadsPerDomain' ] = $request->get('maxLeadsPerDomain');
            $fraudSettings[ 'maxLeadsPerIp' ] = $request->get('maxLeadsPerIp');
            $fraudSettings[ 'urlToReplaceLinks' ] = $request->get('urlToReplaceLinks');
            $fraudSettings[ 'geoBlocking' ] = $request->get('geoBlocking');
            $fraudSettings[ 'urlToHidePage' ] = $request->get('urlToHidePage');
            $fraudSettings[ 'useIntermediatePage' ] = $request->get('useIntermediatePage');
            $fraudSettings[ 'scrambleUrl' ] = $request->get('scrambleUrl');
            $fraudSettings[ 'id' ] = $request->get('id');
            $metaValue = json_encode($fraudSettings, JSON_THROW_ON_ERROR);
            $accountObjRegId = $objectRegisterRepository->getCompanyObjectRegisterId($companyId);
            $objRegMetaDetails = $objectRegisterMetaRepository->getObjectRegisterMetaDetails($accountObjRegId,
                $metaKey);
            if ($objRegMetaDetails === null || !isset($objRegMetaDetails[ 'id' ]) || $objRegMetaDetails[ 'id' ] <= 0) {
                // insert
                if (is_null($objectRegisterMetaRepository->insert($accountObjRegId, $metaKey, $metaValue))) {
                    throw new RuntimeException('Failed to insert FraudSettings for the Account');
                }
            } else {
                // update
                $objRegMetaId = $objRegMetaDetails[ 'id' ];
                if (!$objectRegisterMetaRepository->update($objRegMetaId, $accountObjRegId, $metaKey, $metaValue)) {
                    throw new RuntimeException('Failed to update FraudSettings for the Account');
                }
            }
            $response[ 'fraud' ] = $objectRegisterMetaRepository->getJsonMetaValueAsArray($accountObjRegId, $metaKey);
        } catch (JsonException | RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $this->render('/account/account_integrations/fraud.twig', $response);
    }

    /**
     *
     * Route No. 102, Refactorname accountintegration.getfraudsettingsbyid
     * #Route( "/account/{acc_id}/updateFraud", name="acc.Fraud", methods={"GET"})
     * @Route( "/{acc_id}/Fraud", name="acc.getFraud", methods={"GET"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function getFraudSettingsById()
    {
    }


    /**
     *
     * Route No. 103, Refactorname accountintegration.postfraudsettingsbyid
     * #Route( "/account/{acc_id}/updateFraud", name="acc.Fraud", methods={"POST"})
     * @Route( "/{acc_id}/update/Fraud", name="acc.updateFraud", methods={"POST"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function postFraudSettingsById()
    {
    }


    /**
     *
     * Route No. 104, Refactorname accountintegration.getsettingsmio
     * @Route( "/settings/mio", name="acc.getSettingsMIO", methods={"GET"})
     * @param CompanyRepository $company
     * @return Response
     * @author pva | jsr
     * @deprecated since version 2020 after change in design in Database
     *
     */
    public function getSettingsMIO(
        CompanyRepository $company,
        AuthenticationUtils $authenticationUtils,
        UsersRepository $usersRepository
    ): Response {
        $userEmail = $authenticationUtils->getLastUsername();
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }
        $user = $this->getCurrentUser();
        $id = $user[ 'company_id' ];
        $response[ 'current_user' ] = $user;
        $cnf_tgt = 'MailInOne';
        $response[ 'mio' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        return $this->render('/account/account_integrations/mio.twig', $response);
    }

    /**
     *
     * Route No. 104.1, Refactorname accountintegration.postsettingsmio
     * @Route( "/settings/mio", name="acc.postSettingsMIO", methods={"POST"})
     * @param Request $request
     * @param CompanyRepository $company
     * @return Response
     * @author pva | jsr
     * @deprecated since version 2020 after change in design in Database
     */
    public function setSettingsMIO(Request $request, CompanyRepository $company): Response
    {
        $user = $this->getCurrentUser();
        $id = $user[ 'company_id' ];
        $response[ 'current_user' ] = $user;

        $cnf_tgt = 'MailInOne';
        $response[ 'mio' ] = '';

        $token = $request->get('token');
        if (!$this->isCsrfTokenValid('company-mio', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        $cnf_tgt_settings[ 'apikey' ] = $request->get('apikey');
        $cnf_tgt_settings[ 'reactive_unsubscriber' ] = $request->get('reactive_unsubscriber');
        $cnf_tgt_settings[ 'blk_flter_name' ] = $request->get('blk_flter_name');
        $cnf_tgt_settings[ 'blk_id' ] = $request->get('blk_id');
        $cnf_tgt_settings[ 'id' ] = $request->get('id');

        if ($company->storeIntegrationsForCompany($id, $cnf_tgt, $cnf_tgt_settings)) {
            $response[ 'mio' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        }

        return $this->render('/account/account_integrations/mio.twig', $response);
    }

    /**
     *
     * Route No. 105, Refactorname accountintegration.getmiosettingsbyid
     * #Route( "/account/{acc_id}/updateMIO", name="acc.MIO", methods={"GET"})
     * @Route( "/{acc_id}/MIO", name="acc.getMIO", methods={"GET"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function getMIOSettingsById()
    {
    }


    /**
     *
     * Route No. 106, Refactorname accountintegration.postmiosettingsbyid
     * #Route( "/account/{acc_id}/updateMIO", name="acc.MIO", methods={"POST"})
     * @Route( "/{acc_id}/update/MIO", name="acc.updateMIO", methods={"POST"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function postMIOSettingsById()
    {
    }


    /**
     *
     * Route No. 107, Refactorname accountintegration.getsettingshubspot
     * @param CompanyRepository $company
     * @return Response
     * @author pva
     * @Route( "/settings/hubspot", name="acc.getSettingsHubspot", methods={"GET"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function getSettingsHubspot(
        CompanyRepository $company,
        AuthenticationUtils $authenticationUtils,
        UsersRepository $usersRepository
    ): Response {
        $userEmail = $authenticationUtils->getLastUsername();
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }
        $user = $this->getCurrentUser();
        $id = $user[ 'company_id' ];
        $response[ 'current_user' ] = $user;

        $cnf_tgt = 'Hubspot';
        $response[ 'hubspot' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        return $this->render('/account/account_integrations/hubspot.twig', $response);
    }


    /**
     * Route No. 107.1, Refactorname accountintegration.postsettingshubspot
     * @param Request $request
     * @param CompanyRepository $company
     * @return Response
     * @author pva | jsr
     * @Route( "/settings/hubspot", name="acc.postSettingsHubspot", methods={"POST"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function setSettingsHubspot(Request $request, CompanyRepository $company): Response
    {
        $user = $this->getCurrentUser();
        $id = $user[ 'company_id' ];
        $response[ 'current_user' ] = $user;

        $cnf_tgt = 'Hubspot';
        $response[ 'hubspot' ] = '';

        $token = $request->get('token');
        if (!$this->isCsrfTokenValid('company-hubspot', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        $cnf_tgt_settings[ 'apikey' ] = $request->get('apikey');
        $cnf_tgt_settings[ 'ownerId' ] = $request->get('ownerId');
        $cnf_tgt_settings[ 'id' ] = $request->get('id');

        if ($company->storeIntegrationsForCompany($id, $cnf_tgt, $cnf_tgt_settings)) {
            $response[ 'hubspot' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        }

        return $this->render('/account/account_integrations/hubspot.twig', $response);
    }


    /**
     *
     * Route No. 108, Refactorname accountintegration.gethubspotsettingsbyid
     * #Route( "/account/{acc_id}/updateHubspot", name="acc.Hubspot", methods={"GET"})
     * @Route( "/{acc_id}/Hubspot", name="acc.getHubspot", methods={"GET"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function getHubspotSettingsById()
    {
    }


    /**
     *
     * Route No. 109, Refactorname accountintegration.posthubspotsettingsbyid
     * #Route( "/account/{acc_id}/updateHubspot", name="acc.Hubspot", methods={"POST"})
     * @Route( "/{acc_id}/update/Hubspot", name="acc.updateHubspot", methods={"POST"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function postHubspotSettingsById()
    {
    }


    /**
     * @param CompanyRepository $companyRepository
     * @param AuthenticationUtils $authenticationUtils
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param UsersRepository $usersRepository
     * @return Response
     * @author pva
     * Route No. 110, Refactorname accountintegration.getSettingsGeneral
     * @Route( "/settings/general", name="acc.getSettingsGeneral", methods={"GET"})
     */
    public function getSettingsGeneral(
        CompanyRepository $companyRepository,
        AuthenticationUtils $authenticationUtils,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        UsersRepository $usersRepository
    ): Response {
        $userEmail = $authenticationUtils->getLastUsername();
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }

        $user = $this->getCurrentUser();
        $id = $user[ 'company_id' ];
        $response[ 'current_user' ] = $user;
        $company = $companyRepository->getCompanyDetailById($user[ 'company_id' ]);
        $cnf_tgt = 'General';
        $response[ 'general' ][ 'cioApiKey' ] = $objectRegisterMetaRepository->getAccountAPIKey($company[ 'objectregister_id' ]);
        //$response[ 'general' ] = $companyRepository->getIntegrationsForCompany($id, $cnf_tgt);
        $response[ 'general' ] [ 'company_account_no' ] = (int)$user[ 'company_account_no' ];

        return $this->render('/account/account_integrations/general.twig', $response);
    }

    /**
     * @param Request $request
     * @param CompanyRepository $company
     * @param LoggerInterface $logger
     * @return Response
     * @author pva| jsr
     * Route No. 110.1, Refactorname accountintegration.postSettingsGeneral
     * @Route( "/settings/general", name="acc.postSettingsGeneral", methods={"POST"})
     */
    public function postSettingsGeneral(Request $request, CompanyRepository $company, LoggerInterface $logger): Response
    {
        $user = $this->getCurrentUser();
        $id = $user[ 'company_id' ];
        $response[ 'current_user' ] = $user;

        $cnf_tgt = 'General';
        $response[ 'general' ] = '';
        $token = $request->get('token');

        try {
            if (!$this->isCsrfTokenValid('company-general', $token)) {
                return $this->redirect('/login/signin.twig');
            }
            $cnf_tgt_settings[ 'excludeDomains' ] = $request->get('excludeDomains');
            $cnf_tgt_settings[ 'cioApiKey' ] = $request->get('cioApiKey');
            $cnf_tgt_settings[ 'id' ] = $request->get('id');
            if ($company->storeIntegrationsForCompany($id, $cnf_tgt, $cnf_tgt_settings)) {
                $response[ 'general' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
            }
        } catch (JsonException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $this->render('/account/account_integrations/general.twig', $response);
    }

    /**
     *
     * Route No. 111, Refactorname accountintegration.getgeneralsettingsbyid
     * #Route( "/account/{acc_id}/updateGeneral", name="acc.General", methods={"GET"})
     * @Route( "/{acc_id}/General", name="acc.getGeneralSettingsById", methods={"GET"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function getGeneralSettingsById()
    {
    }


    /**
     *
     * Route No. 112, Refactorname accountintegration.postgeneralsettingsbyid
     * #Route( "/account/{acc_id}/updateGeneral", name="acc.General", methods={"POST"})
     * @Route( "/{acc_id}/update/General", name="acc.updateGeneral", methods={"POST"})
     * @deprecated since version 2020 after change in design in Database
     */
    public function postGeneralSettingsById()
    {
    }


    /**
     * @param CompanyRepository $company
     * @return Response
     * @author pva | jsr
     * Route No. 113, Refactorname accountintegration.getsettingsurlapi
     * #Route( "/accountsettingsURLApi", name="acc.settingsURLApi", methods={"GET"})
     * @Route( "/settings/urlapi", name="acc.getSettingsURLApi", methods={"GET"})
     */
    public function getSettingsURLApi(CompanyRepository $company): Response
    {
        $user = $this->getCurrentUser();
        $id = $user[ 'company_id' ];
        $response[ 'current_user' ] = $user;

        $cnf_tgt = 'URLAPI';
        $response[ 'urlapi' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        return $this->render('/account/account_integrations/urlapi.twig', $response);
    }

    /**
     * @param Request $request
     * @param CompanyRepository $company
     * @return Response
     * @author pva | jsr
     * Route No. 113.1, Refactorname accountintegration.postsettingsurlapi
     * @Route( "/settings/urlapi", name="acc.postSettingsURLApi", methods={"POST"})
     */
    public function postSettingsURLApi(Request $request, CompanyRepository $company): Response
    {
        $user = $this->getCurrentUser();
        $id = $user[ 'company_id' ];
        $response[ 'current_user' ] = $user;

        $cnf_tgt = 'URLAPI';
        $response[ 'urlapi' ] = '';

        $token = $request->get('token');
        if (!$this->isCsrfTokenValid('company-urlapi', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        $cnf_tgt_settings[ 'httpMethod' ] = $request->get('httpMethod');
        $cnf_tgt_settings[ 'encryptionStyle' ] = $request->get('encryptionStyle');
        $cnf_tgt_settings[ 'encryptionMethod' ] = $request->get('encryptionMethod');
        $cnf_tgt_settings[ 'id' ] = $request->get('id');

        if ($company->storeIntegrationsForCompany($id, $cnf_tgt, $cnf_tgt_settings)) {
            $response[ 'urlapi' ] = $company->getIntegrationsForCompany($id, $cnf_tgt);
        }

        return $this->render('/account/account_integrations/urlapi.twig', $response);
    }


    /**
     *
     * Route No. 114, Refactorname accountintegration.geturlapisettingsbyid
     * #Route( "/account/{acc_id}/updateURLApi", name="acc.UrlApi", methods={"GET"})
     * @Route( "/{acc_id}/URLApi", name="acc.getSettingsUrlApiById", methods={"GET"})
     *
     */
    public function getURLApiSettingsById()
    {
    }


    /**
     *
     * Route No. 115, Refactorname accountintegration.posturlapisettingsbyid
     * #Route( "/account/{acc_id}/updateURLApi", name="acc.UrlApi", methods={"POST"})
     * @Route( "/{acc_id}/update/URLApi", name="acc.updateUrlApi", methods={"POST"})
     *
     */
    public function postURLApiSettingsById()
    {
    }


}
