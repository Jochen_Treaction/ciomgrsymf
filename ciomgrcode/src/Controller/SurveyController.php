<?php

namespace App\Controller;

use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Services\CampaignService;
use App\Services\AuthenticationService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class SurveyController extends AbstractController
{
    /**
     *
     * Route No. 50, Refactorname surveyintegration.getSurveyForCampaign
     * @param Request $request
     * @param CampaignRepository $campaign
     * @param CompanyRepository $companyRepository
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @author pva
     * @Route( "/account/campaign/{camp_id}/survey/{view_type}", name="survey.getSurveyForCampaign", methods={"GET"})
     */
    public function getSurveyForCampaign(
        Request $request,
        CampaignRepository $campaign,
        CompanyRepository $companyRepository,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils
    ): Response {
        $camp_id = $request->get('camp_id');
        $view_type = $request->get('view_type');
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $response[ 'survey' ] = $campaign->fetchSurveyForCampaign($camp_id);
        $response[ 'questions' ] = $campaign->fetchSurveyQuestionsForCampaign($camp_id, 0);
        $response[ 'c_set' ][ 'camp_id' ] = $camp_id;
        $response[ 'c_set' ][ 'view_type' ] = $view_type;
        $response[ 'current_user' ] = $user;
        $response[ 'c_set' ][ 'menu_integration_status' ] = $companyRepository->getCompanyIntegrationMetaData($user[ 'company_id' ]);

        if (empty($response[ 'survey' ])) {
            return $this->render('campaign_settings/survey/survey.twig', $response);
        }
        return $this->render('campaign_settings/survey/survey_with_values.twig', $response);
    }


    /**
     *
     * Route No. 51, Refactorname surveyintegration.storeSurveyForCampaign
     * @Route( "/account/campaign/{camp_id}/survey/{view_type}", name="survey.storeSurveyForCampaign", methods={"POST"})
     * @param Request $request
     * @param CampaignRepository $campaign
     * @param CampaignService $campaignService
     * @param NormalizerInterface $normalizer
     * @param LoggerInterface $logger
     * @return Response
     * @throws ExceptionInterface
     */
    public function storeSurveyForCampaign(
        Request $request,
        CampaignRepository $campaign,
        CampaignService $campaignService,
        NormalizerInterface $normalizer,
        LoggerInterface $logger
    ): Response {
        $token = $request->get('token');
        // CSRF Token Validation
        if (!$this->isCsrfTokenValid('survey-update', $token)) {
            return $this->redirect('/login/signin.twig');
        }

        $camp_id = $request->get('camp_id');
        $survey = $request->get('survey');
        $view_type = $request->get('view_type');
        $survey_questions = $survey[ 'question' ];
        $images = $request->files->all();
        $survey_id = $campaign->storeSurveyDescription($survey);
        $normalized_images = !empty($images) ? $normalizer->normalize($images) : null;
        if ($survey_id > 0 && $campaign->updateSurveyIdToCampaigns($camp_id, $survey_id)) {
            if ($campaign->storeSurveyQuestions($camp_id, $survey_id, $survey_questions,
                    $normalized_images) === false) {
                $logger->critical('Failed to store Questions', [__METHOD__, __LINE__]);
            }
        } else {
            $logger->critical('Failed to store survey table or update survey_id to campaign table',
                [__METHOD__, __LINE__]);
        }
        //add default flow control after adding survey
        $campaignService->addFlowControlSettings($camp_id);

        return $this->redirectToRoute('survey.getSurveyForCampaign',
            ['camp_id' => $camp_id, 'view_type' => $view_type]);
    }


    /**
     *
     * Route No. 52, Refactorname surveyintegration.deleteQuestionForSurvey
     * @Route( "/account/campaign/{camp_id}/survey/{survey_id}/{ques_id}/delete", name="survey.deleteQuestionForSurvey", methods={"GET"})
     * @param Request $request
     * @param CampaignRepository $campaign
     * @param LoggerInterface $logger
     * @return Response
     */
    public function deleteQuestionForSurvey(
        Request $request,
        CampaignRepository $campaign,
        LoggerInterface $logger
    ): Response {
        $camp_id = $request->get('camp_id');
        $ques_id = $request->get('ques_id');
        $view_type = $request->get('view_type');
        $status = $campaign->deleteQuestionForSurvey($ques_id);
        if (is_bool($status) && !$status) {
            $logger->critical('Failed to delete question', [__METHOD__, __LINE__]);
        }
        return $this->redirectToRoute('survey.getSurveyForCampaign', ['camp_id' => $camp_id, 'view_type' => 'edit']);
    }


    /**
     *
     * Route No. 53, Refactorname surveyintegration.fetchQuestionForSurvey
     * @Route( "/account/campaign/{camp_id}/survey/{survey_id}/{ques_id}/update", name="survey.fetchQuestionForSurvey", methods={"GET"})
     * @param Request $request
     * @param CampaignRepository $campaign
     * @param LoggerInterface $logger
     * @return Response
     */
    public function fetchQuestionForSurvey(
        Request $request,
        CampaignRepository $campaign,
        LoggerInterface $logger
    ): Response {
        $camp_id = $request->get('camp_id');
        $ques_id = $request->get('ques_id');
        $view_type = $request->get('view_type');
        $response[ 'survey' ] = $campaign->fetchSurveyForCampaign($camp_id);
        $response[ 'questions' ] = $campaign->fetchSurveyQuestionsForCampaign($camp_id, $ques_id);
        $response[ 'c_set' ][ 'camp_id' ] = $camp_id;

        return $this->render('campaign_settings/survey/update_question.twig', $response);
    }

    /**
     *
     * Route No. 53, Refactorname surveyintegration.storeQuestionForSurvey
     * @Route( "/account/campaign/{camp_id}/survey/{survey_id}/{ques_id}/update", name="survey.storeQuestionForSurvey", methods={"POST"})
     * @param Request $request
     * @param CampaignRepository $campaign
     * @param LoggerInterface $logger
     * @param NormalizerInterface $normalizer
     * @return Response
     * @throws ExceptionInterface
     */
    public function storeQuestionForSurvey(
        Request $request,
        CampaignRepository $campaign,
        LoggerInterface $logger,
        NormalizerInterface $normalizer
    ): Response {
        $token = $request->get('token');
        // CSRF Token Validation
        if (!$this->isCsrfTokenValid('survey-update-question', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        $camp_id = $request->get('camp_id');
        $ques_id = $request->get('ques_id');
        $survey_id = $request->get('survey_id');
        $survey = $request->get('survey');
        $images = $request->files->all();
        $normalized_images = !empty($images) ? $normalizer->normalize($images) : '';
        $survey_questions = $survey[ 'question' ];
        if ($campaign->storeSurveyQuestions($camp_id, $survey_id, $survey_questions, $normalized_images) === false) {
            $logger->critical('Failed to update question', [__METHOD__, __LINE__]);
        }

        return $this->redirectToRoute('survey.fetchQuestionForSurvey',
            ['camp_id' => $camp_id, 'survey_id' => $survey_id, 'ques_id' => $ques_id]);
    }


}
