<?php


namespace App\Controller;

use App\Repository\IntegrationsRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\ProcesshookRepository;
use App\Repository\StatusDefRepository;
use App\Services\AppCacheService;
use App\Services\AuthenticationService;
use App\Services\UtilsService;
use App\Types\ProcesshookTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\ProcesshookService;

/**
 * @property ProcesshookService    processhookService
 * @property ProcesshookRepository processhookRepository
 * @property LoggerInterface       logger
 * @property AppCacheService       apcuCache
 */
class ProcesshookController extends AbstractController
{

    public function __construct(
        LoggerInterface $logger,
        ProcesshookService $processhookService,
        ProcesshookRepository $processhookRepository,
        AppCacheService $apcuCache
    )
    {
        $this->logger = $logger;
        $this->processhookService = $processhookService;
        $this->processhookRepository = $processhookRepository;
        $this->apcuCache = $apcuCache;
    }

    /**
     *
     * @Route( "/processhook/{status}", defaults={"status"="available"}, name="processhook.status", methods={"GET"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param StatusDefRepository $statusdefRepository
     * @param IntegrationsRepository $integrationsRepository
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return Response
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author aki
     */
    public function processHook(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        StatusdefRepository $statusdefRepository,
		IntegrationsRepository $integrationsRepository,
		ObjectRegisterRepository $objectRegisterRepository
    ): Response
    {
		$eMioApiKey = null;
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            //return to home page
            return $this->redirect('/');
        }
        $company_id = $user['company_id'];
        $status = $request->get('status');
        $message = $this->apcuCache->get($user['user_id'].'processhookStatus');
        //fetch all processhooks
        $processhooks = $this->processhookService->fetchProcesshooks($status, $company_id);
        if ($status === $statusdefRepository::ARCHIVED) {
            $twig = 'processhooks/processhook_archived.twig';
        } elseif ($status === $statusdefRepository::AVAILABLE) {
            $twig = 'processhooks/processhook_available.twig';
            $eMioApiKey = $integrationsRepository->getIntegrationsEMioApiKey($user['company_id'], $objectRegisterRepository->getProjectCampaignId($user['company_id']));
        } elseif ($status === $statusdefRepository::DEFINED) {
            $twig = 'processhooks/processhook_defined.twig';
        } elseif ($status === "test") {
            $twig = 'processhooks/processhooks.twig';
        } else {
            return $this->redirectToRoute('auth.home');
        }
        //message handling
        if (!empty($message)) {
            if (!empty($message['success'])) {
                $processhooks[0]['success'] = $message['success'];
            } elseif (!empty($message['error'])) {
                $processhooks[0]['error'] = $message['error'];
            }
            //delete key in apcu cache
            $this->apcuCache->del($user['user_id'].'processhookStatus');
        }

        return $this->render($twig, ['current_user' => $user, 'processhooks' => $processhooks, 'emio_apikey' => $eMioApiKey]);
    }

    /**
     *
     * @Route( "/processhook/configure/{processhook}", name="processhook.configure", methods={"GET","POST"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param StatusDefRepository $statusdefRepository
     * @return Response
     * @throws JsonException|\Doctrine\DBAL\Driver\Exception|Exception
     * @author aki
     */
    public function configure(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        StatusdefRepository $statusdefRepository
    ): Response
    {
        //variables required
        $defaultSettings = [];
        $processhookSettings = [];
        //get user
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            //return to home page
            return $this->redirect('/');
        }
        $processhook = $request->get('processhook');
        $company_id = $user['company_id'];
        //get settings dynamically
        try {
            $defaultSettings = $this->processhookService->getSettings($processhook);
            $this->logger->info('$defaultSettings', [$defaultSettings, __FUNCTION__, __LINE__]);
        } catch (\Exception $e) {
            // throw new \Exception('Exception in '. __METHOD__.'=> MSG: '. $e->getMessage(), __LINE__);
            $this->logger->error('Exception', [$e->getMessage(), __METHOD__, __LINE__]);
        }

        //post request
        if ($request->isMethod('POST')) {
            $token = $request->get('token');

            //security check
            if (!$this->isCsrfTokenValid('processhook-create', $token)) {
                return $this->redirect('/dashboard');//return to homepage
            }
            //get setting from view
            $processhookSettings = $request->get('settings');//get settings from view
            $processhookSettings = $this->processhookService->formatProcesshookSettings($processhookSettings,$defaultSettings);

            //fill object register layer and save settings
            $isSettingsSaved = $this->processhookService->saveProcesshookSettings(
                $company_id,
                $processhook,
                $processhookSettings);

            if (is_null($isSettingsSaved)) {
                return $this->redirectToRoute('processhook.status', ["status" => $statusdefRepository::DEFINED,
                    "message" => "error:Processhook settings not saved successfully"]);
            }

            //fill processhook table
            $processhookId = $this->processhookService->saveProcesshook(
                $isSettingsSaved["objectRegisterId"],
                $processhook,
                $company_id,
                $processhookSettings
            );

            if (null === $processhookId) {

                $this->apcuCache->addIfNotExists($user['user_id'].'processhookStatus', ['error' => 'Processhook configuration could not be saved.'], 600);

                // ajax response for ProcesshookTypes::Call_Contact_Event
                if (ProcesshookTypes::CALL_CONTACT_EVENT === $processhook || ProcesshookTypes::SEND_DOI_EMAIL_OR_CALL_CONTACT_EVENT === $processhook) {
                    return $this->json(['status' => false, 'msg' => 'Processhook configuration could not be saved.']);
                }
                return $this->redirectToRoute('processhook.status', ["status" => $statusdefRepository::DEFINED]);
            }

            $this->apcuCache->addIfNotExists($user['user_id'].'processhookStatus', ['success' => 'Processhook successfully defined'], 600);

            // ajax response for ProcesshookTypes::Call_Contact_Event or ProcesshookTypes::SEND_DOI_EMAIL_OR_CALL_CONTACT_EVENT === $processhook
            if (ProcesshookTypes::CALL_CONTACT_EVENT === $processhook || ProcesshookTypes::SEND_DOI_EMAIL_OR_CALL_CONTACT_EVENT === $processhook) {
                return $this->json(['status' => true, 'msg' => 'Processhook successfully defined', 'processhook_id' => $processhookId]);
            }

            return $this->redirectToRoute('processhook.status', ["status" => $statusdefRepository::DEFINED]);
        }
        //generate view dynamically
        $twig = 'processhooks/configure/wizard_' . $processhook . '.html.twig';
        $this->logger->info('settings', [$defaultSettings, __FUNCTION__, __LINE__]);
        return $this->render($twig, ['current_user' => $user, 'settings' => $defaultSettings]);
    }

    /**
     * @Route( "/processhook/update/{user_processhook_name}/{processhook_id}", name="processhook.update", methods={"GET"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param UtilsService $utilsService
     * @return Response
     * @throws \Exception|\Doctrine\DBAL\Driver\Exception
     * @author Pradeep
     */
    public function updateGetAction(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        UtilsService $utilsService
    ): Response {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            return $this->redirect('/');
        }
        $processhookId = $request->get('processhook_id');
        $userProcesshookName = $request->get('user_processhook_name');
        // used to send in the response
        $processhook = [
            'id' => $processhookId,
            'user_processhook_name' => $userProcesshookName,
        ];
        $twig = $utilsService->getProcesshookTwigTemplateName($userProcesshookName);
        // Get ProcessHook configuration from DB to identify the processhookName and accordingly get the defaultConfiguration.
        $data = $this->processhookService->processhookRepository->get($processhookId);
        if (empty($data) || empty($data[ 'objectregister_id' ])) {
            throw new RuntimeException('Failed to fetch processhook information');
        }
        $defaultSettings = $this->processhookService->getSettings($userProcesshookName);
        $settings = $this->processhookService->objectRegisterMetaRepository->getJsonMetaValueAsArray($data[ 'objectregister_id' ],
            $userProcesshookName);
        /*        $this->logger->info('default Settings '.json_encode($defaultSettings));
                $this->logger->info('before Settings '.json_encode($settings));*/
        foreach ($defaultSettings as $key => $value) {
            if (isset($settings[ $key ])) {
                $this->logger->info('Settings has ' . $key);
                continue;
            }
            $settings[ $key ] = $value;
        }
        $this->logger->info('before Settings ' . json_encode($settings));
        return $this->render('processhooks/configure/' . $twig,
            ['current_user' => $user, 'settings' => $settings, 'processhook' => $processhook]);
    }

    /**
     * @Route( "/processhook/update/{user_processhook_name}/{processhook_id}", name="processhook.postupdate", methods={"POST"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param UtilsService $utilsService
     * @return Response
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Pradeep
     */
    public function updatePostAction(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        UtilsService $utilsService
    ): Response {
        try {
            $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
            if (empty($user)) {
                return $this->redirect('/');
            }
            $processhookId = $request->get('processhook_id');
            $processhookName = $request->get('user_processhook_name');
            $settings = $request->get('settings');

            $company_id = $user[ 'company_id' ];
            $twig = $utilsService->getProcesshookTwigTemplateName($processhookName);
            if ((int)$processhookId <= 0 || empty($processhookName) || empty($settings)) {
                return $this->redirectToRoute('processhook.defined');
            }
            $defaultSettings = $this->processhookService->getSettings($processhookName);
            $processhookInfo = $this->processhookService->processhookRepository->get($processhookId);
            if (empty($processhookInfo) || empty($processhookInfo[ 'id' ]) || empty($processhookInfo[ 'objectregister_id' ])) {
                $this->logger->error('Failed to fetch Processhook Information from DB');
                return $this->redirectToRoute('processhook.defined');
            }
            $processhookSettings = $this->processhookService->formatProcesshookSettings($settings, $defaultSettings);
            //fill object register layer and save settings
            $isSettingsSaved = $this->processhookService->saveProcesshookSettings($company_id, $processhookName,
                $processhookSettings, $processhookInfo[ 'objectregister_id' ], 'update');
            if (empty($isSettingsSaved[ 'objectRegisterId' ])) {
                $this->logger->error('Failed to fetch Processhook Information from DB');
                return $this->redirectToRoute('processhook.defined');
            }
            // ajax reponse for ProcesshookTypes::Call_Contact_Event
            if (ProcesshookTypes::CALL_CONTACT_EVENT === $processhookName || ProcesshookTypes::SEND_DOI_EMAIL_OR_CALL_CONTACT_EVENT === $processhookName) {
                return $this->json([
                    'status' => true,
                    'msg' => 'Processhook successfully updated',
                    'processhook_id' => $processhookId,
                ]);
            }
            return $this->redirectToRoute('processhook.status', ["status" => "defined"]);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->redirectToRoute('processhook.status', ["status" => "defined"]);
        }

    }

    /**
     * @Route( "/processhook/archive/{processhook_id}", name="processhook.archive", methods={"GET"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param StatusDefRepository $statusdefRepository
     * @return Response
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Aki
     */
    public function archive(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        StatusdefRepository $statusdefRepository
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            //return to home page
            return $this->redirect('/');
        }
        $processhookId = $request->get('processhook_id');
        $company_id = $user['company_id'];
        $IsProcesshookArchived = $this->processhookService->changeStatusOfProcesshook($company_id, $processhookId,
            $statusdefRepository::ARCHIVED);
        if ($IsProcesshookArchived === false) {
            $this->apcuCache->addIfNotExists($user['user_id'].'processhookStatus', ['error' => 'Processhook not archived successfully'], 600);
            return $this->redirectToRoute('processhook.status', ["status" => $statusdefRepository::ARCHIVED]);
        }
        $this->apcuCache->addIfNotExists($user['user_id'].'processhookStatus', ['success' => 'Processhook successfully Archived'], 600);
        return $this->redirectToRoute('processhook.status', ["status" => $statusdefRepository::ARCHIVED]);

    }

    /**
     * @Route( "/processhook/defined/{processhook_id}", name="processhook.defined", methods={"GET"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param StatusDefRepository $statusdefRepository
     * @return Response
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Aki
     */
    public function defined(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        StatusdefRepository $statusdefRepository
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            //return to home page
            return $this->redirect('/');
        }
        $processhookId = $request->get('processhook_id');
        $company_id = $user['company_id'];
        $IsProcesshookArchived = $this->processhookService->changeStatusOfProcesshook($company_id, $processhookId,
            $statusdefRepository::DEFINED);
        if ($IsProcesshookArchived === false) {
            $this->apcuCache->addIfNotExists($user['user_id'].'processhookStatus', ['error' => 'Processhook not Moved to Defined successfully'], 600);
            return $this->redirectToRoute('processhook.status', ["status" => $statusdefRepository::DEFINED]);
        }
        $this->apcuCache->addIfNotExists($user['user_id'].'processhookStatus', ['success' => 'Processhook successfully updated'], 600);
        return $this->redirectToRoute('processhook.status', ["status" => $statusdefRepository::DEFINED]);

    }

    /**
     * @Route( "/processhook/get/mio_apikey", name="processhook.get.mio_api_key", methods={"GET"})
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param IntegrationsRepository $integrationsRepository
     * @return JsonResponse
     * @author Aki
     */
    public function get_mio_APIkey(
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        IntegrationsRepository $integrationsRepository
    ): JsonResponse
    {
        //get user params
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            //return error if not a valid user
            return $this->json([
                'status' => "Invalid user"
            ]);
        }
        $company_id = $user['company_id'];
        $apikey = "";
        $eMIOIntegrationConfig = $integrationsRepository->getIntegrationConfigForCompany($company_id,
            UniqueObjectTypes::EMAILINONE);
        if (!empty($eMIOIntegrationConfig)) {
            $apikey = $eMIOIntegrationConfig['settings']['apikey'];
        }
        return $this->json([
            'status' => !empty($apikey),
            'apikey' => $apikey,
        ]);
    }

    /**
     * @Route( "/processhook/validate/{name}", name="processhook.validate.processhookName", methods={"GET"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param IntegrationsRepository $integrationsRepository
     * @return JsonResponse
     * @author Aki
     */
    public function validateProcesshookName(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        IntegrationsRepository $integrationsRepository
    ): JsonResponse
    {
        //get user params
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            //return error if not a valid user
            return $this->json([
                'status' => "Invalid user"
            ]);
        }
        $processhookName =  $request->get('name');
        $company_id = $user['company_id'];
        $status = $this->processhookService->checkForProcesshookName($processhookName,$company_id);
        if($status === true){
            $message = "Available";
        }else{
            $message =  $processhookName .' is already in use! Please chose another name.';
        }
        return $this->json([
            'status' => $status,
            'message' => $message,
        ]);
    }

}
