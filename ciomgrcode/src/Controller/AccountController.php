<?php

namespace App\Controller;

use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\InboxMonitorRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\UserMetaRepository;
use App\Repository\UsersRepository;
use App\Services\AioServices;
use App\Services\CampaignService;
use App\Services\InboxMonitorService;
use App\Services\LanguageOptInTextServices;
use App\Services\ProcesshookService;
use App\Services\ProjectServices;
use App\Services\AppCacheService;
use App\Services\AuthenticationService;
use App\Services\SwiftMailerService;
use App\Types\FraudSettingsTypes;
use App\Types\UniqueObjectTypes;
use JsonException;
use Psr\Log\LoggerInterface;
use App\Types\AccountStatusTypes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use WhichBrowser\Parser;


class AccountController extends AbstractController
{

    protected $authenticationUtils;
    protected $authenticationService;
    private ObjectRegisterMetaRepository $objectRegisterMetaRepository;
    private ObjectRegisterRepository $objectRegisterRepository;
    private InboxMonitorService $inboxMonitorService;
    private AppCacheService $apcuCache;
    private CompanyRepository $companyRepository;


    /**
     * LeadController constructor.
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @author jsr | aki
     */
    public function __construct(AuthenticationUtils $authenticationUtils,
                                AuthenticationService $authenticationService,
                                ObjectRegisterMetaRepository $objectRegisterMetaRepository,
                                ObjectRegisterRepository $objectRegisterRepository,
                                InboxMonitorService $inboxMonitorService,
                                AppCacheService $apcuCache,
                                CompanyRepository $companyRepository
    )
    {
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->inboxMonitorService = $inboxMonitorService;
        $this->apcuCache = $apcuCache;
        $this->companyRepository = $companyRepository;
    }


    /**
     * @Route( "/check/alive", name="check.alive", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function checkAlive(Request $request):JsonResponse
    {
        $dateTime = new \DateTime();

        return $this->json([
            'status' => true,
            'msg' => 'I am alive',
            'yourIP' => $request->getClientIp(),
            'currentTimeStamp' => date('Y-m-d H:i:s'),
            'timezone' => $dateTime->format('T'),
        ]);
    }


    /**
     * @param Request             $request
     * @param CompanyRepository   $companyRepository
     * @param AuthenticationUtils $authenticationUtils
     * @param AppCacheService     $apcuCacheService
     * @return Response
     * @author PVA
     * Route No. 8, Refactorname account.accountSwitch
     * @Route( "/account/{acc_id}/switch", name="acc.accountswitch", methods={"GET"})
     */
    public function accountSwitch(
        Request $request,
        CompanyRepository $companyRepository,
        UsersRepository $usersRepository,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService,
        AppCacheService $apcuCacheService,
        LoggerInterface $logger
    ): Response {

        $userEmail = $authenticationUtils->getLastUsername();
        $currentUser = $authenticationService->getUserParametersFromCache($userEmail);
        $currentUserIsSuperAdmin = $currentUser[ 'is_super_admin' ];
        $logger->info('$userEmail', [$userEmail, __METHOD__, __LINE__]);

        $company_id = $request->get('acc_id');

        if ((int)$company_id >= 0) {
            $company = $companyRepository->getCompanyDetailById($company_id);
            $user_apcu_record = $apcuCacheService->get($userEmail);
            $user_apcu_record[ 'company_name' ] = $company[ 'name' ];
            $user_apcu_record[ 'company_account_no' ] = $company[ 'account_no' ];
            $user_apcu_record[ 'company_id' ] = $company_id;

            if (!$currentUserIsSuperAdmin) {
                $userRoles = $usersRepository->getUserRolesForCompany($userEmail, $company_id);
                $logger->info('$userRoles[0]', [$userRoles[ 0 ], __METHOD__, __LINE__]);

                $user_apcu_record[ 'user_roles' ] = $userRoles[ 0 ][ 'roles' ];
            }

            $apcuCacheService->addOrOverwrite($userEmail, $user_apcu_record);
            $apcuCacheService->addOrOverwrite($apcuCacheService::SELECTED_COMPANY_ID_OF_USER . $userEmail,$company_id); // for later use in UsersRepository
        }

        return $this->redirectToRoute('auth.home');
    }


    /**
     * @param CompanyRepository $company
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @return Response
     * @author  PVA | jsr
     * Route No. 16, Refactorname account.getupdateaccount
     * $app->get('/accountupdate', 'AccountController:getUpdateAccount')
     * @Route( "/accountupdate", name="acc.getupdate", methods={"GET"})
     */
    public function getUpdateAccount(
        Request $request,
        CompanyRepository $company,
        CampaignService $campaignService,
        ObjectRegisterRepository $objectRegisterRepository,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService
    ): Response {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $companyMarkedForDeletionDate = $objectRegisterRepository->getCompanyMarkedForDeletionDate($user[ 'company_id' ]);

        $country = $campaignService->getCountryByIp($request->getClientIp());
        $response[ 'deletion_date' ] = (null === $companyMarkedForDeletionDate) ? '' : $companyMarkedForDeletionDate;
        $response[ 'current_user' ] = $user;
        $response[ 'company' ] = $company->getCompanyDetailById((int)$user[ 'company_id' ]);
        $response[ 'company' ][ 'country' ] = (empty($response[ 'company' ][ 'country' ])) ? $country : $response[ 'company' ][ 'country' ];
        $response[ 'hide_menu' ] = false;
        return $this->render('/account/account_information/accupdate.twig', $response);
    }


    /**
     * @param CompanyRepository $company
     * @return Response
     * @author  jsr
     * Route No. 16.1, Refactorname account.getupdateaccount
     * $app->get('/accafterupdate', 'AccountController:getUpdateAccount')
     * @Route( "/accafterupdate", name="acc.afterupdate", methods={"POST"})
     */
    public function afterUpdateAccount(
        Request $request
//        ,AuthenticationUtils $authenticationUtils
//        ,AuthenticationService $authenticationService
    ): Response
    {
        $company = $request->get('company');
        $current_user = $request->get('current_user');

//        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
//        $response[ 'current_user' ] = $user;
//        $response[ 'company' ] = $company->getCompanyDetailById($user[ 'company_id' ]);

        $response[ 'current_user' ] = $current_user;
        $response[ 'company' ] = $company;

        return $this->render('/account/account_information/accupdate.twig', $response);
    }


    /**
     * @param Request $request
     * @param CompanyRepository $company
     * @param CampaignRepository $campaignRepository
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param LoggerInterface $logger
     * @return Response
     * @author  PVA
     * Route No. 16.1, Refactorname account.postupdateaccount
     * $app->get('/accountupdate', 'AccountController:postUpdateAccount');
     * @Route( "/accountupdate", name="acc.postupdate", methods={"POST"})
     */
    public function updateAccount(
        Request $request,
        CompanyRepository $company,
        CampaignRepository $campaignRepository,
        UsersRepository $usersRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        LoggerInterface $logger
    ): Response {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $logger->info('OLD CURRENT_USER = ', [$user, __METHOD__, __LINE__]);
        $token = $request->get('token');
        if (!$this->isCsrfTokenValid('company-update', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        // todo: sanitize rest of request params

        // check if company has campaigns => do not allow to change company name if campaigns exist
        if ($campaignRepository->hasCompanyIdCampaigns($user[ 'company_id' ])) {
            $response = ['error' => "company {$user['company_name']} has campaigns. change of company name not possible"];
            $response[ 'company' ] = $company->getCompanyDetailById($user[ 'company_id' ]);
            $response[ 'current_user' ] = $user;
            return $this->redirectToRoute('acc.getupdate', $response);
        }

        $cmp[ 'id' ] = $request->get('id');
        $cmp[ 'address' ] = filter_var($request->get('address', $request->get('street')), FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW);
        $cmp[ 'city' ] = filter_var($request->get('city'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp[ 'company_name' ] = filter_var($request->get('company_name'), FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW);
        $cmp[ 'country' ] = filter_var($request->get('country'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp[ 'delegated_domain' ] = $request->get('delegated_domain'); // todo: sanitize
        $cmp[ 'phone' ] = filter_var($request->get('phone'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp[ 'plz' ] = filter_var($request->get('plz', $request->get('postal_code')), FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW);
        $cmp[ 'url_option' ] = $request->get('url_option'); // todo: sanitize
        $cmp[ 'vat' ] = filter_var($request->get('vat'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp[ 'website' ] = $request->get('website'); // todo: sanitize
		$cmp[ 'url_privacy_policy' ] = $request->get('url_privacy_policy');
		$cmp[ 'url_imprint' ] = $request->get('url_imprint');
		$cmp[ 'url_gtc' ] = $request->get('url_gtc');
        $cmp[ 'dpa' ] = filter_var($request->get('dpa'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $cmp[ 'dpa_date' ] = date('Y-m-d H:i:s');
        if ($cmp[ 'dpa' ] === null) {
            $cmp[ 'dpa' ] = false;
        } else {
            $objectRegisterRepository->setCompanyObjectRegisterStatusToAccountStatusType($user[ 'account_objectregister_id' ],
                AccountStatusTypes::ACTIVE);
        }

        $user[ 'company_name' ] = $cmp[ 'company_name' ];

        //Update DPA and DPA Date to the User Repository
        if (!$usersRepository->updateDPAAndDPADateForUser($user[ 'user_id' ], $cmp[ 'dpa' ], $cmp[ 'dpa_date' ])) {
            $response = ['error' => 'Something went wrong, Failed to update Account information'];
            $response = $this->forward('App\Controller\AccountController::afterUpdateAccount', [], $response);
            return $response;
        }

        // send a DPA document to User via Email .

        // update cache here
        $current_user = $this->authenticationService->updateUserParametersInCache($user);

		$logger->info('$cmp', [$cmp, __METHOD__, __LINE__]);
        $status = $company->updateCompanyDetails($cmp);

        $response = $status ? ['success' => 'Account information is updated successfully'] : ['error' => 'Something went wrong, Failed to update Account information'];
        $response[ 'company' ] = $company->getCompanyDetailById($user[ 'company_id' ]);
        $response[ 'current_user' ] = $current_user;

        $response = $this->forward('App\Controller\AccountController::afterUpdateAccount', [], $response);
        return $response;
        // return $this->redirectToRoute('acc.afterupdate',$response);
    }


    /**
     * @param Request $request
     * @Route( "/companynamecheck", name="acc.companynamecheck", methods={"POST"})
     * @author   jsr
     * @internal WEB-3850
     */
    public function ajaxCompanyNameCheck(
        Request $request,
        CampaignRepository $campaignRepository,
        CompanyRepository $companyRepository,
        LoggerInterface $logger
    ): JsonResponse {
        $data = json_decode(base64_decode($request->getContent()));
        $company_name = trim($data->company_name);
        $old_company_name = trim($data->old_company_name);
        $action = trim($data->action);

        if (empty($company_name)) {
            return $this->json([
                'msg' => 'Empty company name, please provide your company name. e' . __LINE__,
                'status' => false,
                'company_name' => '',
            ]);
        }

        $entitiesCompanyName = htmlentities($company_name, ENT_HTML5 | ENT_QUOTES);
        $checkedCompanyName = filter_var($company_name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

        if ($checkedCompanyName !== $company_name) {
            return $this->json([
                'msg' => "Unallowed characters in company name {$entitiesCompanyName}. e" . __LINE__,
                'status' => false,
                'company_name' => '',
            ]);
        }

        $companyNameExists = $companyRepository->doesCompanyNameExist($company_name);

        $hasCampaigns = false;
        if ($action === 'update_company') {
            $hasCampaigns = $campaignRepository->hasCompanyNameCampaigns($old_company_name);
        }

        $issue = 0;
        $msg = '';
        $issue += ($companyNameExists) ? 1 : $issue;
        $msg .= ($companyNameExists) ? "Issue #{$issue}: Company {$company_name} is already in use, please choose another name." : '';
        $issue += ($hasCampaigns) ? 1 : $issue;
        $msg .= ($hasCampaigns) ? "\nIssue #{$issue}: There campaigns defined for {$old_company_name}, to be able to change the company name you have to delete all campaigns that are assigned to the company." : '';


        if (empty($msg)) {
            return $this->json([
                'msg' => "Company name {$company_name} is valid! i" . __LINE__,
                'status' => true,
                'company_name' => $company_name,
            ]);
        } else {
            return $this->json(['msg' => $msg . " e" . __LINE__, 'status' => false, 'company_name' => '']);
        }
    }


    public function ajaxHasCampaigns(Request $request)
    {
    }


    /**
     * @param CompanyRepository $company
     * @return JsonResponse
     * @Route( "/ajaxgetcompanybyid", name="acc.ajaxGetCompanyById", methods={"GET"})
     */
    public function ajaxGetCompanyById(
        Request $request,
        CompanyRepository $company,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService
    ): JsonResponse {
        $data = json_decode(base64_decode($request->getContent()));
        $company_id = (int)trim($data->company_id);
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (!empty($user)) {
            return $this->json([
                'status' => true,
                'msg' => '',
                'content' => $company->getCompanyDetailById($company_id),
            ]);
        }
        return $this->json(['status' => false, 'msg' => 'Error e' . __LINE__, 'content' => []]);
    }


    /**
     * @param Request $request
     * @param CompanyRepository $company
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @return JsonResponse
     * @author  jsr
     * @Route( "/ajaxupdatecompany", name="acc.ajaxUpdateCompany", methods={"POST"})
     */
    public function ajaxUpdateCompany(
        Request $request,
        CompanyRepository $company,
        CampaignRepository $campaignRepository,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        LoggerInterface $logger
    ): JsonResponse {
        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $logger->info('OLD CURRENT_USER = ', [$user, __METHOD__, __LINE__]);
        $token = $request->get('token');
        if (!$this->isCsrfTokenValid('company-update', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        // todo: sanitize rest of request params

        // check if company has campaigns => do not allow to change company name if campaigns exist
        if ($campaignRepository->hasCompanyIdCampaigns($user[ 'company_id' ])) {
            $response = ['error' => "company {$user['company_name']} has campaigns. change of company name not possible"];
            $response[ 'company' ] = $company->getCompanyDetailById($user[ 'company_id' ]);
            $response[ 'current_user' ] = $user;
            return $this->redirectToRoute('acc.getupdate', $response);
        }

        $cmp[ 'id' ] = $request->get('id');
        $cmp[ 'address' ] = filter_var($request->get('address', $request->get('street')), FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW);
        $cmp[ 'city' ] = filter_var($request->get('city'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp[ 'company_name' ] = filter_var($request->get('company_name'), FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW);
        $cmp[ 'country' ] = filter_var($request->get('country'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp[ 'delegated_domain' ] = $request->get('delegated_domain'); // todo: sanitize
        $cmp[ 'phone' ] = filter_var($request->get('phone'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp[ 'plz' ] = filter_var($request->get('plz', $request->get('postal_code')), FILTER_SANITIZE_STRING,
            FILTER_FLAG_STRIP_LOW);
        $cmp[ 'url_option' ] = $request->get('url_option'); // todo: sanitize
        $cmp[ 'vat' ] = filter_var($request->get('vat'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp[ 'website' ] = $request->get('website'); // todo: sanitize

        $user[ 'company_name' ] = $cmp[ 'company_name' ];

        // update cache here
        $current_user = $this->authenticationService->updateUserParametersInCache($user);

        $logger->info('NEW CURRENT_USER = ', [$current_user, __METHOD__, __LINE__]);


        $status = $company->updateCompanyDetails($cmp);
        $response = $status ? ['success' => 'Account information is updated successfully'] : ['error' => 'Something went wrong, Failed to update Account information'];
        $response[ 'company' ] = $company->getCompanyDetailById($user[ 'company_id' ]);
        $response[ 'current_user' ] = $current_user;
        // return $this->render('/account/account_information/accupdate.twig', $response); // old
        //return $this->redirectToRoute('acc.getupdate',$response); // works as get

        $response = $this->forward('App\Controller\AccountController::afterUpdateAccount', [], $response);
        return $response;
        // return $this->redirectToRoute('acc.afterupdate',$response);
    }


    /**
     * @Route( "/dpa/accept", name="acc_dpa_accept", methods={"POST","GET"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignRepository $campaignRepository
     * @param LoggerInterface $logger
     * @param CompanyRepository $companyRepository
     * @param UsersRepository $usersRepository
     * @param UserMetaRepository $userMetaRepository
     * @param SwiftMailerService $swiftMailerService
     * @param IntegrationsRepository $integrationsRepository
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ProjectServices $projectServices
     * @param FraudSettingsTypes $fraudSettingsTypes
     * @param AioServices $aioServices
     * @return Response
     * @throws JsonException
     * @author Pradeep
     */
    public function acceptDPAForNewUser(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        CampaignRepository $campaignRepository,
        LoggerInterface $logger,
        CompanyRepository $companyRepository,
        UsersRepository $usersRepository,
        UserMetaRepository $userMetaRepository,
        SwiftMailerService $swiftMailerService,
        IntegrationsRepository $integrationsRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        ProjectServices $projectServices,
        FraudSettingsTypes $fraudSettingsTypes,
        LanguageOptInTextServices $languageOptInTextServices,
        ProcesshookService $processhookService,
        AioServices $aioServices
    ): Response
	{
		$user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
		$headers = $request->server->getHeaders();
		$projectId = '';
		$ipAddress = $request->getClientIp();
		$location = $usersRepository->getUserLocationByIpAddress($ipAddress);
		$whichBrowser = new Parser($headers['USER_AGENT']);
		$browserName = $whichBrowser->browser->name;
		$browserOs = $whichBrowser->os->name;
		$device = $whichBrowser->getType();

		$registration_thirdstep_details = json_encode([
			'ip' => (string)$ipAddress,
			'time_stamp' => (string)date('Y-m-d H:i:s'),
			'location' => (string)$location,
			'device' => $device,
			'browser' => (string)$browserName,
			'device_os' => (string)$browserOs,
		], JSON_THROW_ON_ERROR);

		if (empty($user)) {
			return $this->redirect('/');
		}
		$logger->info('$user', [$user, __METHOD__, __LINE__]);
		$logger->info('$request->getMethod()', [$request->getMethod(), __METHOD__, __LINE__]);
		$response['current_user'] = $user;
		$response['company'] = $companyRepository->getCompanyDetailById((int)$user['company_id']);
		if ($request->getMethod() === 'GET') {
			$response['hide_menu'] = true;
			$response['current_user']['company_name'] = $user['company_account_no'];

			// show the popup only after User accepts DPA.
			if (isset($response['dpa'], $response['dpa_date'])) {
				$response['apikey'] = $companyRepository->getMarketingInOneAPIKeyForCompany((int)$user['company_id']);
			}
			return $this->render('/account/account_information/accupdate.twig', $response);
		}

		if ($request->getMethod() === 'POST') {
			// Update the parameters to DB.
			$company['id'] = $request->get('id');
			$company['address'] = filter_var($request->get('address'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['city'] = filter_var($request->get('city'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['company_name'] = filter_var($request->get('company_name'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['country'] = filter_var($request->get('country'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['delegated_domain'] = $request->get('delegated_domain'); // todo: sanitize
			$company['phone'] = filter_var($request->get('phone'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['plz'] = filter_var($request->get('plz', $request->get('postal_code')), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['url_option'] = $request->get('url_option'); // todo: sanitize
			$company['vat'] = filter_var($request->get('vat'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['website'] = $request->get('website'); // todo: sanitize
			$user['company_name'] = $company['company_name'];
			$company['dpa'] = filter_var($request->get('dpa'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
			$company['url_imprint'] = filter_var($request->get('url_imprint'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['url_privacy_policy'] = filter_var($request->get('url_privacy_policy'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['url_gtc'] = filter_var($request->get('url_gtc'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
			$company['dpa_date'] = date('Y-m-d H:i:s');


			if ($company['dpa'] === null) {
				$company['dpa'] = false;
			} else {
				// create initial project entry in object register, create project entry in campaign table and create initial entry in campaign_params
				$projectDetails = $campaignRepository->getProjectDetails((int)$user['company_id']);
				if (empty($projectDetails) || !isset($projectDetails['id'])) {
					$projectServices->createProject((int)$user['company_id'], $request->getClientIp());
				}
				// Change the status of the Project from 'New' to 'Active'
				$objectRegisterRepository->setCompanyObjectRegisterStatusToAccountStatusType($user['account_objectregister_id'], AccountStatusTypes::ACTIVE);
			}

			// WEB-5238 create standard Lead Notification Processhook when account was created
			if (!$processhookService->createPhLeadNotificationOnAccountCreation($usersRepository->getUserById($user['user_id']))) {
				$response = ['error' => 'Something went wrong, Failed to create initial lead notification processhook.'];
				$response = $this->forward('App\Controller\AccountController::afterUpdateAccount', [], $response);
				return $response;
			}

			$user['company_name'] = $company['company_name'];

			//Update DPA and DPA Date to the User Repository
			if (!$usersRepository->updateDPAAndDPADateForUser($user['user_id'], $company['dpa'], $company['dpa_date'])) {
				$response = ['error' => 'Something went wrong, Failed to update Account information'];
				$response = $this->forward('App\Controller\AccountController::afterUpdateAccount', [], $response);
				return $response;
			}

			// Get the companyDetails.
			$companyDetails = $companyRepository->getCompanyDetailById($user['company_id']);

			if (empty($companyDetails)) {
				$response = ['error' => 'Something went wrong, Failed to get Company Information'];
				$response = $this->forward('App\Controller\AccountController::afterUpdateAccount', [], $response);
				return $response;
			}

			// Check if the Default EMail Server installed ?
			$projectDetails = $campaignRepository->getProjectDetails((int)$companyDetails['id']);

			if ($integrationsRepository->checkIfDefaultServerConfigured() && (int)$projectDetails['id'] > 0) {
				$integrationsRepository->installDefaultEmailServer($company['id'], (int)$user['user_id'], (int)$projectDetails['id']);
			}

            // Install FraudSettings for Account
            // Used by AIO (FraudDetectionService) while creating Contacts
            $accountFraudSettings = $fraudSettingsTypes->getDefaultFraudSettingsForAccountAsJson();

            if (empty($accountFraudSettings) || is_null($objectRegisterMetaRepository->insert($companyDetails[ 'objectregister_id' ],
                    $fraudSettingsTypes::ACC_FRAUD_SETTINGS, $accountFraudSettings))) {
                $logger->error('Failed to insert Account fraudsettings to ObjectRegisterMetaRepository');
            }

            // Default language selection along with default OptinText creation.
            $dfltLangWithOptinTextSelection = !$languageOptInTextServices->setDefaultLangForCreatingAccount(
                (int)$user[ 'user_id' ],
                (int)$company[ 'id' ],
                $user[ 'email' ],
                $company[ 'company_name' ],
                $company[ 'url_gtc' ],
                $company[ 'url_privacy_policy' ],
                $company[ 'url_imprint' ]);
            if (!$dfltLangWithOptinTextSelection) {
                $logger->error('Failed to select a default language.');
            }

            $current_user = $this->authenticationService->updateUserParametersInCache($user);
            $status = $companyRepository->updateCompanyDetails($company);
            $response = $status ? ['success' => 'Account information is updated successfully'] : ['error' => 'Something went wrong, Failed to update Account information'];

            $response[ 'company' ] = $companyDetails;

            if (is_null($userMetaRepository->insert((int)$user[ 'user_id' ], $userMetaRepository::USER_REG_THIRDSTEP,
                $registration_thirdstep_details,
                (int)$user[ 'user_id' ], (int)$user[ 'user_id' ]))) {
                $logger->critical('Failed to create User ThirdStep Registration Details. ', [__METHOD__, __LINE__]);
			}

			$logger->info('$user', [$user, __METHOD__, __LINE__]);

			// show the popup only after User accepts DPA.
			if ($usersRepository->verifyDPAStatusForUser($user['email'])) {
				$apikey = $companyRepository->getMarketingInOneAPIKeyForCompany((int)$user['company_id']);
				$logger->info('APIKEY IN '.__FUNCTION__, [$apikey, __LINE__]);
				// Check APIKey and Generate APIKey if necessary.
				$response['apikey'] = (!empty($apikey)) ? $apikey : $aioServices->generateMIOAPIKey($companyDetails['account_no']);

				// send verify APIKey EMail to the User.
				$logger->info('$companyDetails', [$companyDetails], __LINE__);
				$logger->info('$user', [$user, __LINE__]);
				$logger->info('$response[apikey]', [$response['apikey'], __LINE__]);


				// SENDMAIL -> SwiftMailerService
				$retSendAPIKeyToClient =$swiftMailerService->sendAPIKeyToClient($companyDetails, $user, $response['apikey']);
				if (false === $retSendAPIKeyToClient['ret']) {
					$logger->error('Failed to send ApiKey Notification', [$retSendAPIKeyToClient['msg'],__METHOD__, __LINE__]);
				}

				// SENDMAIL
				// Send an email to 'client-success@treaction.net' with Subject 'DEV Marketing-In-One'
				$retSendNewAccountNotification = $swiftMailerService->sendNewAccountNotification($user, $company);
				if (false === $retSendNewAccountNotification['ret']) {
					$logger->error('Failed to send Email Notification', [$retSendNewAccountNotification['msg'],__METHOD__, __LINE__]);
				}

				$response['current_user'] = $current_user;
			}

			// Show APIKey pop up only if the UserRegistered From AIO(shopMVP, wordpress, other CRM.)
			if (isset($response['apikey']) && !empty($response['apikey']) && ($usersRepository->getUserRegistrationSource($user['user_id']) === 'AIO')) {
				$parameter = ['popup' => base64_encode('apikey')];
			} else {
				$parameter = [];
			}
			return $this->redirectToRoute('auth.home', $parameter);
		}
	}


    /**
     * @param Request $request
     * @return Response
     * @author Aki
     * @Route( "/account/assignseedpool/{acc_id}", name="acc.assign_seedpool", methods={"GET","POST"})
     */
    public function assignSeedPoolsToAccount(
        Request $request
    ): Response {
        //Get the current user
        $currentUser = $this->getCurrentUser();
        //Basic check
        if(!$currentUser[ 'is_super_admin' ]){
            return $this->redirectToRoute('auth.home');
        }
        //get the required data
        $company_id = (int)$request->get('acc_id');
        $superAdminObjectRegisterId = $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        // show error if superadmin object register id not found
        if ($superAdminObjectRegisterId === null) {
            $errorMessage = "Super Admin configuration not found";
            //return the rendered twig
            return $this->render('/account/superadmin/assign_seed_pools.twig',
                ['current_user' => $currentUser,'seed_pool_mapping' => [], 'error' => $errorMessage]);

        }
        // Get mosento config from database
        $mosentoConfig = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails($superAdminObjectRegisterId,
            UniqueObjectTypes::INBOX_MONITOR_CONFIG);
        //Throw error if no config is saved
        if(empty($mosentoConfig)){
            $errorMessage = "Please configure inbox-monitor settings first";
            //return the rendered twig
            return $this->render('/account/superadmin/assign_seed_pools.twig',
                ['current_user' => $currentUser,'seed_pool_mapping' => [], 'error' => $errorMessage]);
        }
        $apiEndPoint = $this->getParameter('mosento_api_endpoint');//value is defined in service.yaml in config
        //Get all the required data for the mapping table
        $seedPoolDataForMapping = $this->inboxMonitorService->getSeedPoolDataForAssignSeedPool($superAdminObjectRegisterId,
            $apiEndPoint,$company_id);

        if(is_null($seedPoolDataForMapping) || empty($seedPoolDataForMapping)){
            $errorMessage = "Error getting seed pools from API";
            //return the rendered twig
            return $this->render('/account/superadmin/assign_seed_pools.twig',
                ['current_user' => $currentUser,'seed_pool_mapping' => [], 'error' => $errorMessage]);
        }
        //post request
        if ($request->getMethod() === 'POST') {
            //Get the data from the request
            try{
                $SeedPoolMappingTable = json_decode($request->get('seedPoolMappingTable'), true, 512, JSON_THROW_ON_ERROR);
            }catch (JsonException $e){
                //return the rendered twig
                return $this->render('/account/superadmin/assign_seed_pools.twig',
                    ['current_user' => $currentUser,'seed_pool_mapping' => $seedPoolDataForMapping, 'error' => $e->getMessage()]);
            }

            //Save the data into database tables
            if(!$this->inboxMonitorService->saveSeedPoolData($SeedPoolMappingTable,
                $company_id,
                $superAdminObjectRegisterId,
                $apiEndPoint,
                $currentUser['user_id']
            )){
                $errorMessage = "Something went wrong saving seed pool data";
                //return the rendered twig
                return $this->render('/account/superadmin/assign_seed_pools.twig',
                    ['current_user' => $currentUser,'seed_pool_mapping' => $seedPoolDataForMapping, 'error' => $errorMessage]);
            }
            //Get company details for message
            $company = $this->companyRepository->getCompanyDetailById($company_id);
            //set the cache message
            $this->apcuCache->addIfNotExists('seed_pool_assign_message',
                'Seed pools assigned to account: '.$company['name']);
            //return to administration overview page
            return $this->redirectToRoute('superadminUserOverview');

        }

        //return the rendered twig
        return $this->render('/account/superadmin/assign_seed_pools.twig',
            ['current_user' => $currentUser, 'seed_pool_mapping' => $seedPoolDataForMapping]);

    }


    /**
     * @return array
     * @author jsr
     */
    protected function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {
            return $user;
        }

        return [];
    }
}
