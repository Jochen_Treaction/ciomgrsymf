<?php

namespace App\Controller;


use App\Repository\LanguageLocalizationRepository;
use App\Repository\LanguageRepository;
use App\Services\AuthenticationService;
use App\Services\LanguageOptInTextServices;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LocalizationController extends AbstractController
{


    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;
    /**
     * @var AuthenticationUtils
     * @author Pradeep
     */
    private AuthenticationUtils $authenticationUtils;
    /**
     * @var AuthenticationService
     * @author Pradeep
     */
    private AuthenticationService $authenticationService;
    /**
     * @var LanguageOptInTextServices
     * @author Pradeep
     */
    private LanguageOptInTextServices $localizationServices;

    public function __construct(
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService,
        LanguageOptInTextServices $localizationServices,
        LoggerInterface $logger
    ) {
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->localizationServices = $localizationServices;
        $this->logger = $logger;
    }

    /**
     * @Route( "/language/overview", name="languages_overview", methods={"GET"})
     */

    /**
     * Shows the list of languages supported by the account.
     * - Supported languages should have OPTIN-TEXT for Newsletter and ecommerce
     *   translated in the specific language.
     * @Route( "/language/overview", name="languages_overview", methods={"GET"})
     * @param Request $request
     * @param LanguageLocalizationRepository $languageLocalizationRepository
     * @return RedirectResponse|Response
     * @throws \Doctrine\DBAL\Driver\Exception
     * @author Pradeep
     */
    public function overview(
        Request $request,
        LanguageLocalizationRepository $languageLocalizationRepository
    ) {
        $user = $this->getCurrentUser();
        if (empty ($user) || empty($user[ 'company_name' ]) || empty($user[ 'company_id' ])) {
            return $this->redirect('/');
        }
        $error = $request->get('error') ?? 'false';
        $message = $request->get('message') ?? '';
        $companyId = (int)$user[ 'company_id' ];
        $languagesOverview = $languageLocalizationRepository->overviewTwig($companyId);
        // return $this->json($languagesOverview);
        return $this->render('localization/overview.twig', [
            'current_user' => $user,
            'account' => $user[ 'company_name' ],
            'overview' => $languagesOverview,
            'error' => $error,
            'message' => $message,
        ]);
    }

    /**
     * Helper function for getting the user information from cache.
     * @return array
     * @author Pradeep
     */
    private function getCurrentUser(): array
    {
        $email = $this->authenticationUtils->getLastUsername();
        return $this->authenticationService->getUserParametersFromCache($email);
    }

    /**
     * Display the wizard to configure the OPTIN Text for newsletter or ecommerce forms
     * in different languages.
     * @Route( "/language/create", name="localization.language-wizard", methods={"GET"})
     * @param Request $request
     * @param LanguageLocalizationRepository $languageLocalizationRepository
     * @param LanguageRepository $languageRepository
     * @return RedirectResponse|Response
     * @author Pradeep
     */
    public function getWizard(
        Request $request,
        LanguageLocalizationRepository $languageLocalizationRepository,
        LanguageRepository $languageRepository
    ) {
        $user = $this->getCurrentUser();
        if (empty ($user) || empty($user[ 'company_name' ])) {
            return $this->redirect('/');
        }
        $companyId = $user[ 'company_id' ];
        $languages = $languageLocalizationRepository->getNonConfiguredLanguages($companyId);
        if (empty($languages)) {
            return $this->redirectToRoute('languages_overview', [
                'error' => true,
                'message' => 'No new languages to configure',
            ]);
        }
        //return $this->json($languages);
        return $this->render('localization/language_wizard.twig', [
            'current_user' => $user,
            'account' => $user[ 'company_name' ],
            'lang_iso_codes' => $languages,
        ]);
    }

    /**
     * Post method for saving the specified OPTIN Text for newsletter or ecommerce forms.
     * - each language creates a new objectregister object without version.
     * - all the specified settings are saved to objRegMeta table.
     * @Route( "/language/create", name="localization.language-create", methods={"POST"})
     * @param Request $request
     * @return RedirectResponse
     * @throws \Doctrine\DBAL\ConnectionException
     * @author Pradeep
     */
    public function addLanguage(Request $request): RedirectResponse
    {
        $error = false;
        $message = 'Successfully added new language';
        $user = $this->getCurrentUser();
        if (empty ($user) || empty($user[ 'company_name' ])) {
            return $this->redirect('/');
        }
        $wizardValues = $request->get('loc');
        //return $this->json($wizardValues);
        if (!$this->localizationServices->addLanguage($wizardValues, $user[ 'user_id' ], $user[ 'company_id' ])) {
            $error = true;
            $message = 'failed to add new language';
        }
        return $this->redirectToRoute('languages_overview', ['error' => $error, 'message' => $message]);
    }

    /**
     * @Route( "/language/{loc_lang_id}/modify/", name="get_modify_language_settings", methods={"GET"})
     */

    /**
     * Update functionality for the wizard.
     * updated are collected and saved to objectregister_meta table.
     * loc_lang_id - id of the language_localization table.
     *
     * @Route( "/language/{loc_lang_id}/modify/", name="post_modify_language_settings", methods={"POST"})
     * @param Request $request
     * @param LanguageRepository $languageRepository
     * @param LanguageOptInTextServices $localizationServices
     * @return RedirectResponse
     * @throws \Doctrine\DBAL\ConnectionException
     * @author Pradeep
     */
    public function saveModifiedWizardSetting(
        Request $request,
        LanguageRepository $languageRepository,
        LanguageOptInTextServices $localizationServices
    ): RedirectResponse {
        $error = false;
        $message = 'successfully updated language settings';

        $user = $this->getCurrentUser();
        if (empty ($user) || empty($user[ 'company_name' ])) {
            return $this->redirect('/');
        }
        $wizardValues = $request->get('loc');
        if (!$this->localizationServices->modify($wizardValues, $user[ 'user_id' ], $user[ 'company_id' ])) {
            $error = true;
            $message = 'failed to modify language settings';
        }
        return $this->redirectToRoute('languages_overview', ['error' => $error, 'message' => $message]);
    }

    /**
     * Update fucntionality of the wizard.
     * This method is only for GET Method.
     * Gets the wizard values (langauge values) for the specified selected language
     * loc_lang_id - id of the language_localization table.
     *
     * @Route( "/language/{loc_lang_id}/modify/", name="get_modify_language_settings", methods={"GET"})
     * @param Request $request
     * @param LanguageRepository $languageRepository
     * @param LanguageOptInTextServices $localizationServices
     * @return RedirectResponse|Response
     * @author Pradeep
     */
    public function getWizardSettingsToModify(
        Request $request,
        LanguageRepository $languageRepository,
        LanguageOptInTextServices $localizationServices
    ) {
        $user = $this->getCurrentUser();
        if (empty ($user) || empty($user[ 'company_name' ])) {
            return $this->redirect('/');
        }

        $locLangId = $request->get('loc_lang_id');
        $wizardValues = $localizationServices->getWizardSettings($locLangId);

        if (empty($wizardValues)) {
            return $this->redirectToRoute('languages_overview', $request->query->all());
        }
        $languages = $languageRepository->getAll();
        // return $this->redirectToRoute('languages_overview', ['error'=>false,'message'=>'successfully loaded']);
        return $this->render('localization/language_wizard.twig', [
            'current_user' => $user,
            'account' => $user[ 'company_name' ],
            'wizard' => $wizardValues,
            'lang_iso_codes' => $languages,
        ]);
    }


    /**
     * Just for testing
     * @Route( "/test/language/{loc_lang_id}/modify", name="language_test_modify", methods={"GET"})
     */
    public function testGet(
        Request $request,
        LanguageRepository $languageRepository,
        LanguageOptInTextServices $localizationServices
    ) {
        $user = $this->getCurrentUser();
        if (empty ($user) || empty($user[ 'company_name' ])) {
            return $this->redirect('/');
        }
        $langObjRegId = $request->get('loc_lang_id');
        $wizardValues = $localizationServices->getWizardSettings($langObjRegId);
        return $this->json($wizardValues);
    }

    /**
     * removes or deletes the language and settings specified for that the company.
     * - deletes the entry in language_localization, objectregister and objectregister_meta tables.
     * - Default language cannot be deleted or removed.
     * - loc_lang_id - id of the language_localization table.
     * @Route( "/language/{loc_lang_id}/remove", name="language_remove", methods={"GET"})
     * @param Request $request
     * @return RedirectResponse
     * @author Pradeep
     */
    public function removeLanguage(Request $request): RedirectResponse
    {
        $user = $this->getCurrentUser();
        if (empty ($user) || empty($user[ 'company_name' ])) {
            return $this->redirect('/');
        }
        try {
            $objectRegId = $request->get('loc_lang_id');
            if ($objectRegId <= 0) {
                throw new RuntimeException('Invalid language id or companyId');
            }
            if ($this->localizationServices->removeLanguage($objectRegId)) {
                $error = false;
                $message = 'Successfully removed language';
            } else {
                $error = true;
                $message = 'Failed to remove language';
            }
        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__, __LINE__]);
            $error = true;
            $message = 'Failed to remove language';
        }
        return $this->redirectToRoute('languages_overview', ['error' => $error, 'message' => $message]);
    }

    /**
     * Only one language for the company can be set to default.
     * - When a new language is marked as default, then previously default language should be set to false.
     * - loc_lang_id - id of the language_localization table.
     * @Route( "/language/default/{loc_lang_id}/set", name="language_set_default", methods={"GET"})
     * @param Request $request
     * @return RedirectResponse
     * @author Pradeep
     */
    public function setLanguageAsDefault(Request $request): RedirectResponse
    {
        $user = $this->getCurrentUser();
        if (empty ($user) || empty($user[ 'company_name' ])) {
            return $this->redirect('/');
        }
        try {
            $id = $request->get('loc_lang_id');
            $companyId = $user[ 'company_id' ];
            if ($id <= 0 || $companyId <= 0) {
                throw new RuntimeException('Invalid language id or companyId to set as default');
            }
            if ($this->localizationServices->makeLanguageAsDefault($id, $companyId)) {
                $error = false;
                $message = 'Successfully set language as default';
            } else {
                $error = true;
                $message = 'Failed to set language as default';
            }
        } catch (RuntimeException|Exception $e) {
            $this->logger->error($e->getMessage(), [__CLASS__, __METHOD__, __LINE__]);
            $error = true;
            $message = 'Failed to set language as default';
        }
        return $this->redirectToRoute('languages_overview', ['error' => $error, 'message' => $message]);
    }
}