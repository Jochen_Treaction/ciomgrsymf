<?php

namespace App\Controller;

use App\Repository\InboxMonitorRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\SeedPoolRepository;
use App\Services\AppCacheService;
use App\Services\InboxMonitorService;
use App\Services\UtilsService;
use App\Types\UniqueObjectTypes;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AuthenticationService;
use Psr\Log\LoggerInterface;

class InboxMonitorController extends AbstractController
{


    private LoggerInterface $logger;
    private AuthenticationService $authenticationService;
    private AuthenticationUtils $authenticationUtils;
    private $userId;
    private $user;
    private UtilsService $utilsService;
    private InboxMonitorService $inboxMonitorService;
    private SeedPoolRepository $seedPoolRepository;
    private ObjectRegisterRepository $objectRegisterRepository;
    private AppCacheService $apcuCache;
    private InboxMonitorRepository $inboxMonitorRepository;

    public function __construct(
        LoggerInterface $logger,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        UtilsService $utilsService,
        SeedPoolRepository $seedPoolRepository,
        InboxMonitorService $inboxMonitorService,
        ObjectRegisterRepository $objectRegisterRepository,
        AppCacheService $apcuCache,
        InboxMonitorRepository $inboxMonitorRepository
    )
    {
        $this->logger = $logger;
        $this->authenticationService = $authenticationService;
        $this->authenticationUtils = $authenticationUtils;
        $this->utilsService = $utilsService;
        $this->inboxMonitorService = $inboxMonitorService;
        $this->seedPoolRepository = $seedPoolRepository;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->apcuCache = $apcuCache;
        $this->inboxMonitorRepository = $inboxMonitorRepository;
    }

    /**
     * @Route( "/inboxmonitor/apicheck", name="inboxmonitor.apicheck",  methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @internal communicates with mosento and returns response sent from mosento in JSON
     * @author Aki
     */
    public function checkMosentoApikey(
        Request $request
    ): JsonResponse {
        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->authenticationService->getUserParametersFromCache($userEmail);
        $response = new JsonResponse();
        //basic user check
        if (empty($currentUser) || empty($userEmail)) {
            $message = "Invalid user";
            return $this->utilsService->setResponseWithMessage($response,$message,false);
        }
        //Get the params
        $userId = $request->get('user_id');
        $apiKey = $request->get('apikey');
        $apiEndPoint = $this->getParameter('mosento_api_endpoint');//value is defined in service.yaml in config

        $data = $this->inboxMonitorService->getSeedPoolsFromMosento($userId,$apiKey,$apiEndPoint);
        if(empty($data)){
            $message = "No data from Mosento API";
            return $this->utilsService->setResponseWithMessage($response,$message,false);
        }
        $message = 'success';
        return $this->utilsService->setResponseWithMessage($response,$message,true,$data);
    }


    /**
     * @Route( "/inboxmonitor/overview", name="inbox_monitor.view",  methods={"GET"})
     * @param Request $request
     * @return Response
     * @internal communicates with mosento and returns response sent from mosento in JSON
     * @author Aki
     */
    public function getInboxMonitorView(
        Request $request
    ): Response {
        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->authenticationService->getUserParametersFromCache($userEmail);
        //basic user check
        if (empty($currentUser) || empty($userEmail)) {
            return $this->redirect('/');
        }
        //Get all seed pools for company

        $this->logger->info('/inboxmonitor/overview INBOXcurrentUser', [$currentUser,__METHOD__,__LINE__]);
        $seedPools = $this->seedPoolRepository->getSeedPoolNamesForCompanyInArray($currentUser['company_id']);
        $seedPoolGroups = $this->seedPoolRepository->getSeedPoolGroupsByCompanyId($currentUser['company_id']);
        //Get all domains for the company
        $domains = $this->inboxMonitorRepository->getDomainsForCompanyInArray($currentUser['company_id']);
        //return the response
        return $this->render('delivery_management/inbox_monitor.twig',
            ['current_user' => $currentUser,'domains' => $domains,'seed_pools' => $seedPools, 'seed_pool_groups' => $seedPoolGroups]);
    }


    /**
     * @Route( "/inboxmonitor/details/{inboxMonitorId}", name="inboxMonitorDetails",  methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @internal communicates with mosento and returns response sent from mosento in JSON
     * @author Aki
     */
    public function getInboxMonitorSingleView(
        InboxMonitorRepository $inboxMonitorRepository,
        Request $request
    ): Response {
        $inboxMonitorId = $request->get('inboxMonitorId');
        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->authenticationService->getUserParametersFromCache($userEmail);
        $response = new JsonResponse();
        //basic user check



        if (empty($currentUser) || empty($userEmail)) {
            $message = "Invalid user";
            return $this->utilsService->setResponseWithMessage($response,$message,false);
        }
        // TODO: get data for details view
        $inboxMonitorRecord = $inboxMonitorRepository->getInboxMonitorForDetailsView($inboxMonitorId);


        //return the response
        return $this->render('delivery_management/inbox_monitor_single_view.twig', ['current_user' => $currentUser, 'inboxmonitor' => $inboxMonitorRecord[0]]);
    }


    /**
     * @Route( "/inboxmonitor/sync", name="inboxmonitor.sync",  methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @internal Sync the inbox monitor data between MIO and mosento
     * @internal Every 15 min's cron calls the route in plesk with description "This is to sync data from mosento to MIO DB"
     * todo:This syncs all the company's we need a separate api for single company
     * @author Aki
     */
    public function inboxMonitorSync(
        Request $request
    ): JsonResponse {
        $response = new JsonResponse();

        $this->logger->info('is_inbox_monitor_sync_running', [$this->apcuCache->get('is_inbox_monitor_sync_running'), __METHOD__, __LINE__]);

        if(!$this->apcuCache->get('is_inbox_monitor_sync_running')){
            //set the metaphor add $user_id
            $this->apcuCache->addOrOverwrite('is_inbox_monitor_sync_running',true,300);
            //Get mosento config
            $mosentoConfig = $this->inboxMonitorService->getMosentoConfig(
                $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin()
            )['settings'];
            $this->logger->info('$mosentoConfig', [$mosentoConfig, __METHOD__, __LINE__]);
            //validate the mosento config
            if(empty($mosentoConfig)){
                $message = "Please check the mosento config";
                $this->apcuCache->addOrOverwrite('is_inbox_monitor_sync_running',false,300);
                return $this->utilsService->setResponseWithMessage($response,$message,false);
            }
            //Get all seed pools form database
            $seedPools = $this->seedPoolRepository->getAllSeedPools();
            //$this->logger->info('$seedPools', [$seedPools, __METHOD__, __LINE__]);

            //API end point
            $apiEndPoint = $this->getParameter('mosento_api_endpoint');//value is defined in service.yaml in config
            $this->logger->info('$apiEndPoint', [$apiEndPoint, __METHOD__, __LINE__]);
            //Loop through seed-pool and get inbox monitor data from the mosento
            foreach ($seedPools as $seedPool){
                $mosentoSeedPoolId = $seedPool['mosento_seed_pool_id'];
                $this->logger->info('$mosentoSeedPoolId', [$mosentoSeedPoolId, __METHOD__, __LINE__]);
                $inboxMonitorResponse = $this->inboxMonitorService->getInboxMonitorFromMosento($mosentoSeedPoolId,
                    $mosentoConfig,
                    $apiEndPoint);

                //$this->logger->info('$inboxMonitorResponse', [$inboxMonitorResponse, __METHOD__, __LINE__]);
                //check db for inbox monitor and compare and make CURD for inbox monitor
                if(empty($inboxMonitorResponse) || !$this->inboxMonitorService->syncInboxMonitor($inboxMonitorResponse,
                        $mosentoSeedPoolId,$mosentoConfig,$apiEndPoint)){
                    //Send error response with a message
                    $message = "Something went wrong in syncing the inbox-monitor data to database";
                    $this->logger->info('err $message', [$message, __METHOD__, __LINE__]);
                    $this->apcuCache->addOrOverwrite('is_inbox_monitor_sync_running',false,300);
                    return $this->utilsService->setResponseWithMessage($response,$message,false);
                }
            }
            //Return the response
            $message = 'success';
            $this->logger->info('okay $message', [$message, __METHOD__, __LINE__]);
            $this->apcuCache->addOrOverwrite('is_inbox_monitor_sync_running',false,300);
            return $this->utilsService->setResponseWithMessage($response,$message,true);
        }
        //Send error response with a message
        $message = "Inbox-monitor sync running please try after some time";
        $this->logger->info('warn $message', [$message, __METHOD__, __LINE__]);
        return $this->utilsService->setResponseWithMessage($response,$message,false);
    }

    /**
     * @Route( "/domains/overview", name="domain.overview", methods={"GET"})
     * @return Response
     */
    public function domainsView():response
    {
        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->authenticationService->getUserParametersFromCache($userEmail);
        //basic user check
        if (empty($currentUser) || empty($userEmail)) {
            return $this->redirect('/');
        }
        //Get all seed pools for company
        $seedPoolGroups = $this->seedPoolRepository->getSeedPoolGroupsByCompanyId($currentUser['company_id']);

        return $this->render('/delivery_management/domain/domain_overview.twig', [
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'],
            'seed_pool_groups' => $seedPoolGroups
        ]);

    }

    /**
     * @Route( "/domains/singleview/{domain_id}", name="domain.singleview", methods={"GET"})
     * @return Response
     */
    public function domainsSingleView():response
    {
        $currentUser = $this->getCurrentUser();

        return $this->render('/delivery_management/domain/domain_single_view.twig', [
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'],

        ]);
    }

    /**
     * @return array
     * @author jsr
     */
    private function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {

            $company_id = $this->apcuCache->get($this->apcuCache::SELECTED_COMPANY_ID_OF_USER . $user[ 'email' ]);
            if (false !== $company_id) {
                $user[ 'company_id' ] = $company_id;
            }

            return $user;
        }
        return [];
    }


    /**
     * @Route( "/project/languages", name="project.languages", methods={"GET"})
     * @return Response
     */
    public function getLanguageView()
    {
        $currentUser = $this->getCurrentUser();

        return $this->render('/project/languages/languages.twig', [
            'current_user' => $currentUser,
            'account' => $currentUser[ 'company_name' ],

        ]);

    }

    /**
     * @Route( "/project/languages_wizard", name="project.languages_wizard", methods={"GET"})
     * @return Response
     */
    public function getLanguagesWizard()
    {
        $currentUser = $this->getCurrentUser();

        return $this->render('/project/languages/languageswizard.twig', [
            'current_user' => $currentUser,
            'account' => $currentUser[ 'company_name' ],

        ]);

    }

}
