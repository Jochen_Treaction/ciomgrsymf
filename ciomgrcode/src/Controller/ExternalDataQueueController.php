<?php

namespace App\Controller;

use App\Repository\CompanyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Repository\UsersRepository;
use App\Services\AioServices;
use App\Services\CampaignService;
use App\Types\MessagesTypes;
use App\Types\UniqueObjectTypes;
use JsonException;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AuthenticationService;
use App\Repository\ContactRepository;
use App\Repository\CampaignRepository;
use Psr\Log\LoggerInterface;
use App\Types\ProjectWizardTypes;
use App\Repository\ExternalDataQueueRepository;
use App\Services\AppCacheService;

//@internal webhookQueue in backend called as externalDataQueue
class ExternalDataQueueController extends AbstractController
{

    private AuthenticationUtils $authenticationUtils;
    private AuthenticationService $authenticationService;
    private ExternalDataQueueRepository $externalDataQueueRepository;
    private AppCacheService $apcuCacheService;
    private LoggerInterface $logger;

    public function __construct(AuthenticationUtils $authenticationUtils,
                                AuthenticationService $authenticationService,
                                ExternalDataQueueRepository $externalDataQueueRepository,
                                AppCacheService $apcuCacheService,
                                LoggerInterface $logger
    )
    {
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->externalDataQueueRepository = $externalDataQueueRepository;
        $this->apcuCacheService = $apcuCacheService;
        $this->logger = $logger;

    }


    /**
     * @Route( "/webhook/queue", name="webhook.queue", methods={"GET"})
     * @return Response
     */
    public function getInitialExternalDataView(): Response
    {
        $currentUser = $this->getCurrentUser();

        $allRecordsForExternalDataQueue = $this->externalDataQueueRepository->getAllRecordsForExternalDataQueueView($currentUser['company_id']);
        $selectBoxContent = $this->externalDataQueueRepository->getAllWebhookNamesForSelectBox($currentUser['company_id']);

        return $this->render('/externaldataqueue/externaldata_queue_statuslist.twig', [
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'],
            'externaldataqueue' => $allRecordsForExternalDataQueue,
            'select_id_webhooknames' => $selectBoxContent
        ]);
    }


    /**
     *
     * @Route( "/webhook/queue/download/{webhookQueueId}", name="webhookqueue.Download", methods={"GET"})
     * @param Request $request
     * @return Response
     * @author aki
     * @internal This controller generates code for webhooks
     */
    public function downloadContactsByExternalDataQueueId(
        Request $request
    ): Response
    {
        //Basic security check
        $currentUser = $this->getCurrentUser();
        if (empty($currentUser)) {
            return $this->redirect('/');
        }
        //get the data
        //@internal - externalDataQueue table === webhookQueue in UI
        $externalDataQueueId = $request->attributes->get('webhookQueueId');
        $externalContactData = $this->externalDataQueueRepository->getWebhookQueueContactDataWithId($externalDataQueueId);

        // File name with extension
        $filename = 'webhookQueueContactData_'.$externalDataQueueId.'.json';
        // The dynamically created content of the file
        try {
            $fileContent = json_encode($externalContactData, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT);
        } catch (JsonException $e) {
            $this->logger->error('External Contact Data Conversion Error '.$e->getMessage(),
                [__METHOD__, __LINE__]);
        }

        // Return a response with a specific content
        $response = new Response($fileContent);
        // Create the disposition of the file
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);
        // Dispatch request
        return $response;
    }


    /**
     * @return array
     * @author jsr
     */
    protected function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {

            $company_id = $this->apcuCacheService->get($this->apcuCacheService::SELECTED_COMPANY_ID_OF_USER . $user['email']);
            if (false !== $company_id) {
                $user['company_id'] = $company_id;
            }

            return $user;
        }
        return [];
    }
}
