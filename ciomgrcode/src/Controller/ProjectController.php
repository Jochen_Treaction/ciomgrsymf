<?php


namespace App\Controller;

use App\Repository\CompanyRepository;
use App\Repository\CustomfieldsRepository;
use App\Repository\DatatypesRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\StatusDefRepository;
use App\Repository\UsersRepository;
use App\Services\AioServices;
use App\Services\CampaignService;
use App\Types\MessagesTypes;
use App\Types\UniqueObjectTypes;
use JsonException;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
// use App\Services\TwigGlobalsService;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AuthenticationService;
use App\Repository\ContactRepository;
use App\Repository\CampaignRepository;
use Psr\Log\LoggerInterface;
use App\Types\ProjectWizardTypes;
use App\Services\AppCacheService;


/**
 * Class ProjectController
 * @package App\Controller
 * @Route( "/project")
 */
class ProjectController extends AbstractController
{

    /**
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param ContactRepository $contactRepository
     * @param CampaignRepository $campaignRepository
     * @param CustomfieldsRepository $customfieldsRepository
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param CampaignService $campaignService
     * @param DatatypesRepository $datatypesRepository
     * @param ProjectWizardTypes $projectWizardTypes
     * @param UsersRepository $usersRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param LoggerInterface $logger
     * @return Response
     * @Route("/wizard", name="projectWizard", methods={"GET"} )
     */
    public function getWizard(
        Request $request,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService,
        ContactRepository $contactRepository,
        CampaignRepository $campaignRepository,
        CompanyRepository $companyRepository,
        CustomfieldsRepository $customfieldsRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        CampaignService $campaignService,
        DatatypesRepository $datatypesRepository,
        ProjectWizardTypes $projectWizardTypes,
        UsersRepository $usersRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        LoggerInterface $logger
    ): Response
    {
        $country = $campaignService->getCountryByIp($request->getClientIp());
        $logger->info('$country', [$country, __METHOD__, __LINE__]);

        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);
        $logger->info('$user', [$user, __METHOD__, __LINE__]);

        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }

        if (empty($user)) {
            return $this->redirect('/');
        }

        $projectObjectRegister = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id']);


        $logger->info('Project object register', [print_r($projectObjectRegister, true), __METHOD__, __LINE__]);
        $hasCustomfieldObjectRegisterEntryForProject = $objectRegisterRepository->hasCustomfieldObjectRegisterEntryForProject($user['company_id']);

        $logger->info('$hasCustomfieldObjectRegisterEntryForProject',
            [print_r($hasCustomfieldObjectRegisterEntryForProject, true), __METHOD__, __LINE__]);

        if (!empty($hasCustomfieldObjectRegisterEntryForProject)) {

            $customfieldsObjectRegisterId = $hasCustomfieldObjectRegisterEntryForProject;
            $customFields = $customfieldsRepository->getProjectCustomfields($user[ 'company_id' ],
                UniqueObjectTypes::CUSTOMFIELDS);
            $logger->info('$customFields', [$customFields, __METHOD__, __LINE__]);
            $customfield_ids = [];
            foreach ($customFields as $customfield) {
                $customfield_ids[] = $customfield['id'];
            }

            $customfields_used_in_campaign = $objectRegisterRepository->getCustomfieldUsedInPageOrWebhook($user['company_id'],
                $customfield_ids);
            $logger->info('$customfields_used_in_campaign', [$customfields_used_in_campaign, __METHOD__, __LINE__]);
        } else {
            $customfieldsObjectRegisterId = $objectRegisterRepository->createInitialObjectRegisterEntry($user['company_id'],
                UniqueObjectTypes::CUSTOMFIELDS);
            $customFields = [];
        }

        $datatypes = $datatypesRepository->getDatatypes();


        $params['success'] = '';
        $params['error'] = '';

        // add linked to (page, webhook) indicator to customfields
        foreach ($customFields as $cfKey => $cf) {
            $linked = false;
            $linkedTo = [];
            // $customfieldsCampaignId = $cf['campaign_id']; // since the same, it can be overwritten

            if (null !== $customfields_used_in_campaign) {
                foreach ($customfields_used_in_campaign as $ccf) {
                    if ($ccf['ref_customfields_id'] === $cf['id']) {
                        $linked = true;
                        $linkedTo[] = "{$ccf['unique_object_name']} '{$ccf['campaign_name']}' / #{$ccf['campaign_id']}";
                    }
                }
                $customFields[$cfKey]['linked'] = $linked;
                $customFields[$cfKey]['linkedTo'] = 'field is used by ' . implode(',', $linkedTo);
            } else {
                $customFields[$cfKey]['linked'] = false;
                $customFields[$cfKey]['linkedTo'] = '';
            }
        }

        $logger->info('customFields', [$customFields, __METHOD__, __LINE__]);

        // $projectWizardEntries = $projectWizardTypes->getArrayWithDefaultValues(); // TODO: this must be replaced by access to object_type_meta.meta_key = 'type_default_settings' for project
        $savedProjectCampaignParams = $objectRegisterMetaRepository->getJsonMetaValueAsArray($projectObjectRegister['id'],
            ObjectRegisterMetaRepository::MetaKeyProject);
        $logger->info('$savedProjectCampaignParams', [$savedProjectCampaignParams, __METHOD__, __LINE__]);

        if (null === $savedProjectCampaignParams) { // not yet set, get defaults from object_type_meta
            $projectWizardEntries = $objectRegisterRepository->getProjectTypeDefaultSettingsAsArray();
            $logger->info('$projectWizardEntries', [$projectWizardEntries, __METHOD__, __LINE__]);

//            should not be needed, if DB data is configured
//            if(null === $projectWizardEntries) {
//                $projectWizardEntries = $projectWizardTypes->getArrayWithDefaultValues();
//            }

            $projectWizardEntries['relation']['company_name'] = $user['company_name'];
            $projectWizardEntries['relation']['company_id'] = $user['company_id'];
            $projectWizardEntries['relation']['project_object_unique_id'] = ($projectObjectRegister['object_unique_id']) ? $projectObjectRegister['object_unique_id'] : '';
            $projectWizardEntries['relation']['project_objectregister_id'] = ($projectObjectRegister['id']) ? $projectObjectRegister['id'] : '';
            // $projectWizardEntries['customfieldlist'] = json_encode($customFields);
            $logger->info('json_encode($customFields)', [json_encode($customFields), __METHOD__, __LINE__]);
            $projectWizardEntries['settings']['description'] =
				(empty($projectWizardEntries['advancedsettings']['description']))
					? 'project definitions for ' . $user['company_name']
					: $projectWizardEntries['advancedsettings']['description'];


			$logger->info('REDIRECT', [$projectWizardEntries['advancedsettings']['redirect_after_registration'], __METHOD__,__LINE__]);

            $projectWizardEntries['advancedsettings']['country'] = $country;

            $logger->info('$projectWizardEntries', [$projectWizardEntries, __METHOD__, __LINE__]);
        } else {
            $projectWizardEntries = $savedProjectCampaignParams;
        }

		$projectWizardEntries['advancedsettings']['redirect_after_registration'] =
			(empty($projectWizardEntries['advancedsettings']['redirect_after_registration']))
				? $companyRepository->getCompanyWebsiteUrl($user['company_id'])	// add default entry from company.website only when the user enters the project wizard for the first time
				: $projectWizardEntries['advancedsettings']['redirect_after_registration'];

        $logger->info('$projectWizardEntries', [$projectWizardEntries, __METHOD__, __LINE__]);


        // TODO: getProjectdefault settings (currently hard coded in projectwizard.twig) from ConfigurationTargetRepository and submit to projectwizard.twig
        return $this->render('project/projectwizard.twig', [
            'createProjectRoute' => 'project/create',
            'customfieldsObjectRegisterId' => $customfieldsObjectRegisterId,
            'customfieldsCampaignId' => $projectObjectRegister['campaign_id'],
            'prjwiz' => $projectWizardEntries,
            'current_user' => $user,
            'datatypes' => $datatypes,
            'customfields' => $customFields,
        ]);
        //return $this->render('', ['put project params' => 'here']);

    }


    /**
     *
     * $app->post('/wizard', 'ProjectController:postWizard');
     * #Route( "/wizard", name="project.postCreate", methods={"POST"})
     * @Route( "/wizard", name="project.postCreate", methods={"POST"})
     * @return Response
	 * @deprecated
     */
    public function postWizard(): Response
    {

        die();

    }


    /**
     * @param Request $request
     * @param CustomfieldsRepository $customFieldsRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param AioServices $aioServices
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @throws JsonException
     * @Route ("/create", name="projectCreate", methods={"POST"})
     */
    public function create(
        Request $request,
        CustomfieldsRepository $customFieldsRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        IntegrationsRepository $integrationsRepository,
        AioServices $aioServices,
		AppCacheService $apcuCacheService,
        LoggerInterface $logger
    ): JsonResponse
    {
        $encodedContent = $request->getContent();//getData
        $decodedContent = base64_decode($encodedContent);
        $contentArr = json_decode($decodedContent, true, 512, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
        $campaign_params = [];
        $logger->info('$content', [$decodedContent, __METHOD__, __LINE__]);

        foreach ($contentArr as $key => $value) {
            if ($key === 'customfieldlist') {
                //insert custom fields for project
                $campaign_params['customfieldlist'] = $value;
                $insertUdateProjectCustomfields = $customFieldsRepository->insertUpdateProjectCustomfields($value);
                $logger->info("(customfieldlist=" . print_r($campaign_params['customfieldlist'], true) . ")");
            } elseif ($key === 'customfielddelete') {
                //remove custom fields if deleted in wizard
                $campaign_params['customfielddelete'] = $value;
                $deleteProjectCustomfields = $customFieldsRepository->deleteProjectCustomfields($value);
                $logger->info("(customfielddelete=" . print_r($campaign_params['customfielddelete'], true) . ")");
            } elseif ($key === 'token') {
                //check csrf token
                if (!$this->isCsrfTokenValid('project-wizard', $value)) {
                    return $this->json([
                        'status' => 'error',
                        'cud_customfields' => [],
                        'u_campaign_params' => false,
                    ]);
                }
            } else {
                //add all normal fields to campaign params
                $sectionField = $this->getKeys($key);
                $campaign_params[$sectionField['section']][$sectionField['fieldname']] = $value;
            }
        }
        //Merge new customfield array for updated and deleted ones
        $eMIOCustomFields[ 'customfieldlist' ] = array_merge($campaign_params[ 'customfieldlist' ],
            $campaign_params[ 'customfielddelete' ]);
        $eMIOCustomFields[ 'relation' ] = $campaign_params[ 'relation' ];
        $eMIOIntegration = $integrationsRepository->getIntegrationConfigForCompany((int)$campaign_params[ 'relation' ][ 'company_id' ],
            UniqueObjectTypes::EMAILINONE, StatusDefRepository::ACTIVE);

        if ($eMIOIntegration !== null && isset($eMIOIntegration['settings']['apikey'])) {
            //if emio integration is saved
            $eMIOAPIKey = $eMIOIntegration['settings']['apikey'];
            // Sync Customfields with eMIO Integration
            if (!$integrationsRepository->updateeMIOCustomFieldsFromProject($eMIOCustomFields,
                (int)$campaign_params[ 'relation' ][ 'company_id' ])) {
                $logger->error('Fialed to update eMIO CustomFields from Project to integrations');
            }
            // Sync customfields with eMIO Microservice.
            if (!$aioServices->updateCustomFieldsToeMIO($eMIOCustomFields, $eMIOAPIKey)) {
                $logger->error('Failed to update customfield to eMIO');
            }
        }
        $projectCustomfields = $customFieldsRepository->getProjectCustomfields((int)$campaign_params[ 'relation' ][ 'company_id' ]);
        //   $logger->info( "$projectCustomfields " .json_encode($projectCustomfields));

        $campaign_params[ 'customfieldlist' ] = $projectCustomfields;
        unset($campaign_params[ 'customfielddelete' ]);


        $boolUpdateProjectCampaignParams = $objectRegisterMetaRepository->updateProjectSettings((int)$campaign_params[ 'relation' ][ 'project_objectregister_id' ],
            json_encode($campaign_params));

        $logger->info('$campaign_params', [$campaign_params, __METHOD__, __LINE__]);
        $logger->info('$campaign_params json ', [json_encode($campaign_params), __METHOD__, __LINE__]);
        $apcuCacheService->addOrOverwrite("projectStatus", "Project settings saved successfully", $apcuCacheService::FIVE_MINUTES);
        // apcu_store("projectStatus", "Project settings saved successfully", 400); // not multiuser capable
        return $this->json([
            // (?) cud = create (insert), update, delete
            'status' => 'success',
            'cud_customfields' => array_merge($insertUdateProjectCustomfields, $deleteProjectCustomfields),
            // (?) u = update
            'u_campaign_params' => $boolUpdateProjectCampaignParams,
        ]);
    }


    /**
     * helper function
     * @param string $str e.g. 'settings_commercial_relationships'
     * @return array ['section' => 'settings', 'fieldname' => 'commercial_relationships']
     */
    private function getKeys(string $str): array
    {
        $pos = stripos($str, '_', 0);
        $section = substr($str, 0, $pos);
        $fieldname = substr($str, $pos + 1, strlen($str));

        return ['section' => $section, 'fieldname' => $fieldname];
    }


    /**
     * $app->get('/messages', 'ProjectController:copyCampaign')->setName('project.messages');
     * @Route( "/messages", name="project.messages", methods={"GET", "POST"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param MessagesTypes $messagesTypes
     * @param LoggerInterface $logger
     * @return Response
     * @author Aki
     * @internal All messages are handled by this method
     */
    public function messages(
        Request $request,
        CampaignRepository $campaignRepository,
        CampaignService $campaignService,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        MessagesTypes $messagesTypes,
        AppCacheService $apcuCacheService,
        LoggerInterface $logger
    ): Response
    {
        //Basic check
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            return $this->redirect('/');
        }
        //get object register id of project
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
        $logger->info('Project object register', [print_r($projectObjectRegisterId, true), __METHOD__, __LINE__]);
        //check if messages are present in DB
        $checkIfMessagesAlreadyExists = $objectRegisterMetaRepository->getIdWithObjectRegisterId($projectObjectRegisterId,
            ObjectRegisterMetaRepository::MetaKeyMessage);
        //post request
        if ($request->isMethod('post')) {
            //Basic validation
            $token = $request->get('token');
            if (!$this->isCsrfTokenValid('messages-update', $token)) {
                return $this->redirect('/login/signin.twig');
            }
            // Get the request content
            $content = $request->getContent();
            $messages = $campaignService->convertRequestToArray($content);
            //update settings to object register meta
            if (is_null($checkIfMessagesAlreadyExists)) {
                //save messages
                $status = $objectRegisterMetaRepository->insert($projectObjectRegisterId,
                    ObjectRegisterMetaRepository::MetaKeyMessage,json_encode($messages ,JSON_UNESCAPED_UNICODE));

            } else {
                //update messages
                $status = $objectRegisterMetaRepository->updateSettings($projectObjectRegisterId,
                    json_encode($messages,JSON_UNESCAPED_UNICODE),ObjectRegisterMetaRepository::MetaKeyMessage);

            }
            if(isset($status)){
                //redirect to home page with message
				$apcuCacheService->addOrOverwrite('messageDraft', 'Messages saved successfully', $apcuCacheService::FIVE_MINUTES);
                // apcu_store('messageDraft', 'Messages saved successfully', 400); // not multiuser capable
                return $this->redirectToRoute('auth.home');
            }
            //redirect to home page with message
			$apcuCacheService->addOrOverwrite('messageDraft', 'Something went wrong in saving the settings', $apcuCacheService::FIVE_MINUTES);
            // apcu_store('messageDraft', 'Something went wrong in saving the settings', 400); // not multiuser capable
            return $this->redirectToRoute('auth.home');

        }
        //Get messages for view
        if (is_null($checkIfMessagesAlreadyExists)) {
            //gets default value
            $messages = $messagesTypes->getArrayWithDefaultValues();
        } else {
            //take messages from DB
            $messages = $objectRegisterMetaRepository->getJsonMetaValueAsArray($projectObjectRegisterId,
                ObjectRegisterMetaRepository::MetaKeyMessage);
        }
        return $this->render('project/messages.twig', ['messages' => $messages, 'current_user' => $user]);
    }

}

