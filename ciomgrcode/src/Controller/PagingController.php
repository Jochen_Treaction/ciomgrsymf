<?php

namespace App\Controller;

use App\PagingServices\GenericStatementFactory;
use App\PagingServices\PagingHandlerService;
use App\Repository\ContactRepository;
use App\Repository\CustomfieldsRepository;
use App\Repository\DomainsRepository;
use App\Repository\InboxMonitorRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\ExternalDataQueueRepository;
use App\Repository\UsersRepository;
use App\Services\AppCacheService;
use App\Services\AuthenticationService;
use App\Types\ContactDataTableViewTypes;
use App\Types\UniqueObjectTypes;
use App\Types\PagingViewType;
use App\Types\SelectionKeyOperatorValueTypes;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route( "/paging")
 */
class PagingController extends AbstractController
{

    protected AuthenticationUtils $authenticationUtils;
    protected AuthenticationService $authenticationService;
    protected AppCacheService $apcuCacheService;


    public function __construct(AuthenticationUtils $authenticationUtils,
                                AuthenticationService $authenticationService,
                                AppCacheService $apcuCacheService)
    {
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->apcuCacheService = $apcuCacheService;
    }

    /**
     * @Route( "/contact/{pageIndex}", name="paging.overviewByPageNumber", methods={"POST"})
     * @param Request                    $request
     * @param LoggerInterface            $logger
     * @param PagingViewType             $pagingViewType
     * @param PagingHandlerService       $pagingService
     * @param ContactRepository          $contactRepository
     * @param CustomfieldsRepository     $customFieldsRepository
     * @param MIOStandardFieldRepository $mioStandardFieldRepository
     * @param ContactDataTableViewTypes  $contactDataTableViewTypes
     * @param UsersRepository            $usersRepository
     * @param UniqueObjectTypes          $uniqueObjectTypes
     * @param AppCacheService            $apcuCacheService
     * @return JsonResponse
     * @author Jsr
     * @internal This route returns contacts with respect to page number, Pagination size is defined in services.yaml
     *           <br>treats <b style="color:blue">viewname=contact</b> for standard contact overview and <b style="color:blue">viewname=contacthistory</b> for contact history twig
     * <br><b>EXPECTED POST PARAMETERS</b>
     * <br>- <b>viewname</b> ="contact" (standard overview) | "contacthistory" (history of leads => versions)
     * <br>- <b>contact_search</b> = "some user added text" | null
     * <br>- <b>filter_by_status</b> = "active" | "inactive" | "fraud" | "blacklisted" | null (status of lead in objReg)
     * <br>- <b>daterange</b> = "2022-01-30#2022-02-17" | null
     * <br>- <b>records_per_page</b> = 100 | some int value
     */
    public function getContactListByPage(
        Request $request,
        LoggerInterface $logger,
        PagingViewType $pagingViewType,
        PagingHandlerService $pagingService,
        ContactRepository           $contactRepository,
        CustomfieldsRepository      $customFieldsRepository,
        MIOStandardFieldRepository  $mioStandardFieldRepository,
        ContactDataTableViewTypes   $contactDataTableViewTypes,
        UsersRepository $usersRepository,
        UniqueObjectTypes $uniqueObjectTypes,
        CustomfieldsRepository $customfieldsRepository,
        AppCacheService $apcuCacheService
    ): JsonResponse {

        // viewname="contact" (standard overview) | "contacthistory" (history of leads => versions)
        // contact_search= "some user added text" | null
        // filter_by_status = "active" | "inactive" | "fraud" | "blacklisted" | null (status of lead in objReg)
        // daterange="2022-01-30#2022-02-17" | null
        // records_per_page
        $t1_prepare = microtime(true);
        $postBodyJson = $request->getContent();
        $postBody = json_decode($postBodyJson, true, 512, JSON_OBJECT_AS_ARRAY);
        $postBody['page_index'] = $request->get('pageIndex'); // add the by route param provided pageIndex to postBody, to have everything in one place
        $logger->info('$postBody', [$postBody,__METHOD__,__LINE__]);


        // exit if not all params are provided
        if(!isset($postBody['viewname']) ||!isset($postBody['contact_search']) ||!isset($postBody['filter_by_status']) || !isset($postBody['daterange']) ||!isset($postBody['page_index']) ||!isset($postBody['records_per_page']) ) {
            $msg = '';
            foreach ( ['viewname',  'contact_search','filter_by_status', 'daterange', 'page_index','records_per_page' ] as $param) {
                $msg .= (!isset($postBody[$param])) ? $postBody[$param].' ': '';
            }
            $logger->error('not provided post parameters', ['msg'=> $msg, __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'error: not provided post parameters '.$msg]);
        }

        /*
        $supportedView =['contact','contacthistory'];

        // exit when wrong data for view was requested
        if( in_array($postBody['viewname']) ) {
            $logger->error('invalid route for contact', [ __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'error: invalid route for contact: '.strtolower(trim($postBody['viewname']))]);
        }
        */

        // get data of current user
        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->getCurrentUser();
        $logger->info('$currentUser', [$currentUser, __METHOD__, __LINE__]);

        //Check for DPA acceptance -> don't know if this is really required here ...
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $message = "User DPA not accepted";
            $logger->warning($message, [ __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => $message]);
        }

        // instantiate your StatementService (here: contact)
        $pagingViewType->setPagingView(strtolower($postBody['viewname']));
        $logger->info('$postBody[viewname]', [$postBody['viewname'], __METHOD__, __LINE__]);
        $logger->info('$pagingViewType', [$pagingViewType->getPagingView(), __METHOD__, __LINE__]);
        $contactViewDataProvider = GenericStatementFactory::createGenericStatementService($pagingViewType);

        if(null === $contactViewDataProvider) {
            return $this->json([
                'status' => false,
                'message' => "could not instantiate Service for {$postBody['viewname']}",
            ]);
        }

        // inject needed objects for "contact" => mio.leads table
        $contactViewDataProvider->inject([
            '$contactRepository' => $contactRepository,
            '$customFieldsRepository' => $customFieldsRepository,
            '$mioStandardFieldRepository' => $mioStandardFieldRepository,
            '$contactDataTableViewTypes' => $contactDataTableViewTypes,
            '$uniqueObjectTypes' => $uniqueObjectTypes,
            '$apcuCacheService' => $apcuCacheService,
        ]);

        $logger->info('daterange', [explode('#', $postBody['daterange']), __METHOD__, __LINE__]);

        // setup basic selection criteria without minId, maxId (are provided later)
        $mySelections = new SelectionKeyOperatorValueTypes();
        // set limit of data of the main table in $contactViewDataProvider (here: leads) you expect to receive in TWIG template
        $customfields = $customfieldsRepository->getProjectCustomfields($currentUser['company_id']);
        $countCustomfields = (count($customfields) === 0) ? 1 : count($customfields);


        $contactViewDataProvider->setLimit((int)($postBody['records_per_page'] * $countCustomfields));
        $contactViewDataProvider
            ->setSelection('searchleads.searchtext', 'MATCHES', $postBody['contact_search'])
            ->setSelection('leads.account', '=', $currentUser['company_id'])
            ->setSelection('objectregister.statusdef_value', '=', $postBody['filter_by_status'])
            ->setSelection('leads.created_from', '>', explode('#', $postBody['daterange'])[0])
            ->setSelection('leads.created_to', '<',explode('#', $postBody['daterange'])[1]);

        if('contact' === $postBody['viewname']) {
            $contactViewDataProvider->setSelection('useRepositoryMethod', '=', 'getPagingLeadsForContactOverview');
        } else if ('contacthistory' === $postBody['viewname']) {
            $contactViewDataProvider->setSelection('useRepositoryMethod', '=', 'getPagingLeadsForContactHistory');
        }else{
            $logger->error('invalid route for contact', [ __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'error: invalid route for contact: '.strtolower(trim($postBody['viewname']))]);
        }

        $baseSelection = clone $contactViewDataProvider->getSelection(); // keep the base selection separately, since it's used as part of cache key and cannot change for a specific user defined selection
        $logger->info('$baseSelection', [$baseSelection->getAllSelectionSettings(), __METHOD__, __LINE__]);


        // check if a page switch happened
        $indexSelection = $pagingService->managePaging($currentUser, $postBody, $baseSelection); // you MUST USE the basic selection here!!!

        // set the indexSelection returned by $pagingService->managePaging
        $contactViewDataProvider->setSelection('leads.id', $indexSelection['operator'], $indexSelection['id']);

        // get the total number of rows of thios selection without respecting the LIMIT ($postBody['records_per_page'])
        $logger->info('getAllSelectionSettings min/max' , [$contactViewDataProvider->getSelection()->getAllSelectionSettings(), __METHOD__, __LINE__]);
        $logger->info('getAllSelectionSettings $baseSelection' , [$baseSelection->getAllSelectionSettings(), __METHOD__, __LINE__]);
        $logger->info('getAllSelectionSettings $baseSelection leads.account' , [$baseSelection->getSelectionValue('leads.account'), __METHOD__, __LINE__]);
        $logger->info('getAllSelectionSettings $baseSelection searchleads.searchtext' , [$baseSelection->getSelectionValue('searchleads.searchtext'), __METHOD__, __LINE__]);
        $logger->info('getAllSelectionSettings $baseSelection objectregister.statusdef_value' , [$baseSelection->getSelectionValue('objectregister.statusdef_value'), __METHOD__, __LINE__]);
        $logger->info('getAllSelectionSettings $baseSelection leads.created_from' , [$baseSelection->getSelectionValue('leads.created_from'), __METHOD__, __LINE__]);
        $logger->info('getAllSelectionSettings $baseSelection leads.created_to' , [$baseSelection->getSelectionValue('leads.created_to'), __METHOD__, __LINE__]);

        $t2_prepare = microtime(true);

        $t1_totalNumberOfRows = microtime(true);
        $totalNumberOfRows = $contactViewDataProvider->getTotalNumberOfRows($baseSelection, $currentUser); // TODO: cache result for user's baseSelection
        $t2_totalNumberOfRows = microtime(true);


        // run the selection and receive the expected result sets
        $t1_runSelection = microtime(true);
        $ret = $contactViewDataProvider->runSelection($contactViewDataProvider->getSelection()); // returns ['status' => true|false, 'message' => 'some message', 'minId' => leads.id, 'maxId' => leads.id, 'c_leads' => [] ]  => different arrays of records need in TWIG-Template
        if( false === $ret['status']) {
            return $this->json([
                'status' => $ret['status'],
                'message' => $ret['message'],
                'total_number_of_rows' => $totalNumberOfRows,
                'c_leads' => [],
                'current_user' => $currentUser,
                'account' => $currentUser['company_name'], // old twig-template needs that separately
            ]);
        }
        $t2_runSelection = microtime(true);

        $pagingService->setIds($ret['minId'], $ret['maxId'], $currentUser, $baseSelection);

        return $this->json([
            'status' => $ret['status'],
            'message' => $ret['message'],
            'total_number_of_rows' => $totalNumberOfRows,
            'c_leads' => $ret['c_leads'],
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'], // old twig-template needs that separately
            'minId' => $ret['minId'],
            'maxID'=> $ret['maxId'],
            'prepare [s]' => ($t2_prepare-$t1_prepare),
            'totalNumberOfRows [s]' => ($t2_totalNumberOfRows - $t2_totalNumberOfRows),
            'runSelection [s]' => ($t2_runSelection - $t1_runSelection),
        ]);
    }


    /**
     * @Route( "/externaldataqueue/{pageIndex}", name="paging.externaldataqueue", methods={"POST"})
     * @param Request                     $request
     * @param LoggerInterface             $logger
     * @param PagingViewType              $pagingViewType
     * @param PagingHandlerService        $pagingService
     * @param UsersRepository             $usersRepository
     * @param AppCacheService             $apcuCacheService
     * @param ExternalDataQueueRepository $externalDataQueueRepository
     * @return Response
     */
    public function externalDataQueue(
        Request $request,
        LoggerInterface $logger,
        PagingViewType $pagingViewType,
        PagingHandlerService $pagingService,
        UsersRepository $usersRepository,
        AppCacheService $apcuCacheService,
        ExternalDataQueueRepository $externalDataQueueRepository
    ):Response
    {

        $postBodyJson = $request->getContent();
        $postBody = json_decode($postBodyJson, true, 512, JSON_OBJECT_AS_ARRAY);
        $postBody['page_index'] = $request->get('pageIndex'); // add the by route param provided pageIndex to postBody, to have everything in one place
        $logger->info('$postBody', [$postBody,__METHOD__,__LINE__]);

        // exit if not all params are provided
        if(!isset($postBody['viewname']) ||!isset($postBody['webhookname']) ||!isset($postBody['statusqueue']) ||!isset($postBody['page_index']) ||!isset($postBody['records_per_page']) ) {
            $msg = '';
            foreach ( ['viewname',  'webhookname','statusqueue', 'daterange', 'page_index','records_per_page' ] as $param) {
                $msg .= (!isset($postBody[$param])) ? $postBody[$param].' ': '';
            }
            $logger->error('not provided post parameters', ['msg'=> $msg, __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'error: not provided post parameters '.$msg]);
        }

        // exit when wrong data for view was requested
        if('externaldataqueue' !==  strtolower(trim($postBody['viewname']))) {
            $logger->error('invalid route for contact', [ __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'error: invalid route for contact: '.strtolower(trim($postBody['viewname']))]);
        }

        // get data of current user

        $currentUser = $this->getCurrentUser();
        $userEmail = $currentUser['email'];
        $logger->info('$currentUser', [$currentUser, __METHOD__, __LINE__]);


        //Check for DPA acceptance -> don't know if this is really required here ...
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $message = "User DPA not accepted";
            $logger->warning($message, [ __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => $message]);
        }

        // instantiate your StatementService (here: contact)
        $pagingViewType->setPagingView(strtolower($postBody['viewname']));
        $logger->info('$postBody[viewname]', [$postBody['viewname'], __METHOD__, __LINE__]);
        $logger->info('$pagingViewType', [$pagingViewType->getPagingView(), __METHOD__, __LINE__]);
        $externalDataQueueViewDataProvider = GenericStatementFactory::createGenericStatementService($pagingViewType);

        if(null === $externalDataQueueViewDataProvider) {
            return $this->json([
                'status' => false,
                'message' => "could not instantiate Service for {$postBody['viewname']}",
            ]);
        }

        // inject needed objects for "contact" => mio.leads table
        $externalDataQueueViewDataProvider->inject([
            '$externalDataQueueRepository' => $externalDataQueueRepository,
            '$apcuCacheService' => $apcuCacheService,
        ]);

        $externalDataQueueViewDataProvider->setLimit($postBody['records_per_page']);
        $externalDataQueueViewDataProvider
            ->setSelection('viewname', '=', $postBody['viewname'])
            ->setSelection('externaldataqueue.companyId', '=', $currentUser['company_id'])
            ->setSelection('objectregister.webhookname', '=', $postBody['webhookname'])
            ->setSelection('externaldataqueue.status', '=', $postBody['statusqueue']);

        $baseSelection = clone $externalDataQueueViewDataProvider->getSelection(); // keep the base selection separately, since it's used as part of cache key and cannot change for a specific user defined selection
        $logger->info('$baseSelection', [$baseSelection->getAllSelectionSettings(), __METHOD__, __LINE__]);

        // set the indexSelection returned by $pagingService->managePaging
        $offValue = $pagingService->managePagingOffSetValueForPaginationBar($postBody['records_per_page'],$postBody['page_index']);
        $externalDataQueueViewDataProvider->setSelection('externaldataqueue.offset', '=', $offValue);

        $totalNumberOfRows = $externalDataQueueViewDataProvider->getTotalNumberOfRows($baseSelection, $currentUser);
        $ret = $externalDataQueueViewDataProvider->runSelection($externalDataQueueViewDataProvider->getSelection()); // returns ['status' => true|false, 'message' => 'some message', 'minId' => leads.id, 'maxId' => leads.id, 'c_leads' => [] ]  => different arrays of records need in TWIG-Template

        if( false === $ret['status']) {
            return $this->json([
                'status' => $ret['status'],
                'message' => $ret['message'],
                'total_number_of_rows' => 0,
                'records' => [],
                'current_user' => $currentUser,
                'account' => $currentUser['company_name'] // old twig-template needs that separately
            ]);
        }

        $pagingService->setIds($ret['minId'], $ret['maxId'], $currentUser, $baseSelection);

        return $this->json([
            'status' => $ret['status'],
            'message' => $ret['message'],
            'total_number_of_rows' => $totalNumberOfRows,
            'records' => $ret['records'],
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'], // old twig-template needs that separately
            'minId' => $ret['minId'],
            'maxID'=> $ret['maxId'],
        ]);
    }


    /**
     * @Route( "/inboxmonitor/{pageIndex}", name="pagingInboxMonitor", methods={"POST"})
     * @param Request                     $request
     * @param LoggerInterface             $logger
     * @param PagingViewType              $pagingViewType
     * @param PagingHandlerService        $pagingService
     * @param UsersRepository             $usersRepository
     * @param AppCacheService             $appCacheService
     * @return Response
     * @author Aki || Jsr
     * @internal Manage paging for inbox monitor
     */
    public function getInboxMonitorList(
        Request $request,
        LoggerInterface $logger,
        PagingViewType $pagingViewType,
        PagingHandlerService $pagingService,
        UsersRepository $usersRepository,
        AppCacheService $appCacheService,
        InboxMonitorRepository $inboxMonitorRepository
    ):Response
    {

        $postBodyJson = $request->getContent();
        $postBody = json_decode($postBodyJson, true, 512, JSON_OBJECT_AS_ARRAY);
        $postBody['page_index'] = $request->get('pageIndex'); // add the by route param provided pageIndex to postBody, to have everything in one place
        $logger->info('$postBody', [$postBody,__METHOD__,__LINE__]);


        // exit when wrong data for view was requested
        if('inboxmonitor' !==  strtolower(trim($postBody['viewname']))) {
            $logger->error('invalid route for inboxmonitor paging', [ __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'error: invalid route for contact: '.strtolower(trim($postBody['viewname']))]);
        }

        // exit if not all params are provided
        if(!isset($postBody['seed_pool_group_id']) || !isset($postBody['seed_pool_name']) ||!isset($postBody['domain']) ||!isset($postBody['spam_reason']) ||!isset($postBody['seed_pool_type']) ||!isset($postBody['records_per_page']) ) {
            $msg = '';
            foreach ( ['seed_pool_group_id', 'seed_pool_name',  'domain','spam_reason', 'seed_pool_type','records_per_page' ] as $param) {
                $msg .= (!isset($postBody[$param])) ? $postBody[$param].' ': '';
            }
            $logger->error('not provided post parameters', ['msg'=> $msg, __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'error: not provided post parameters '.$msg]);
        }

        // get data of current user

        $currentUser = $this->getCurrentUser();
        $userEmail = $currentUser['email'];

        $logger->info('/inboxmonitor/{pageIndex} INBOXcurrentUser', [$currentUser, __METHOD__, __LINE__]);


        //Check for DPA acceptance -> don't know if this is really required here ...
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $message = "User DPA not accepted";
            $logger->warning($message, [ __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => $message]);
        }

        // instantiate your StatementService (here: contact)
        $pagingViewType->setPagingView(strtolower($postBody['viewname']));
        $logger->info('$postBody[viewname]', [$postBody['viewname'], __METHOD__, __LINE__]);
        $logger->info('$pagingViewType', [$pagingViewType->getPagingView(), __METHOD__, __LINE__]);
        $inboxMonitorViewDataProvider = GenericStatementFactory::createGenericStatementService($pagingViewType);

        if(null === $inboxMonitorViewDataProvider) {
            return $this->json([
                'status' => false,
                'message' => "could not instantiate Service for {$postBody['viewname']}",
            ]);
        }

        // inject needed objects for "contact" => mio.leads table
        $inboxMonitorViewDataProvider->inject([
            '$inboxMonitorRepository' => $inboxMonitorRepository,
            '$apcuCacheService' => $appCacheService,
        ]);
        //todo:@jsr please add 'search string' to set selection
        $inboxMonitorViewDataProvider->setLimit($postBody['records_per_page']);
        $inboxMonitorViewDataProvider
            ->setSelection('viewname', '=', $postBody['viewname'])
            ->setSelection('company_id', '=', $currentUser['company_id'])
            ->setSelection('seed_pool_group_id', '=', $postBody['seed_pool_group_id'])
            ->setSelection('seed_pool_name', '=', $postBody['seed_pool_name'])
            ->setSelection('domain', '=', $postBody['domain'])
            ->setSelection('spam_reason', '=', $postBody['spam_reason'])
            ->setSelection('inboxmonitor_search_string', '=', $postBody['search_string'])
            ->setSelection('inboxmonitor_type', '=', $postBody['seed_pool_type']);


        $baseSelection = clone $inboxMonitorViewDataProvider->getSelection(); // keep the base selection separately, since it's used as part of cache key and cannot change for a specific user defined selection
        $logger->info('$baseSelection', [$baseSelection->getAllSelectionSettings(), __METHOD__, __LINE__]);

        // set the indexSelection returned by $pagingService->managePaging
        $offValue = $pagingService->managePagingOffSetValueForPaginationBar($postBody['records_per_page'],(int)$postBody['page_index']);
        $inboxMonitorViewDataProvider->setSelection('inboxmonitor.offset', '=', $offValue);

        $totalNumberOfRows = $inboxMonitorViewDataProvider->getTotalNumberOfRows($baseSelection, $currentUser);
        $ret = $inboxMonitorViewDataProvider->runSelection($inboxMonitorViewDataProvider->getSelection()); // returns ['status' => true|false, 'message' => 'some message', 'minId' => leads.id, 'maxId' => leads.id, 'c_leads' => [] ]  => different arrays of records need in TWIG-Template

        if( false === $ret['status']) {
            return $this->json([
                'status' => $ret['status'],
                'message' => $ret['message'],
                'total_number_of_rows' => 0,
                'records' => [],
                'current_user' => $currentUser,
                'account' => $currentUser['company_name'] // old twig-template needs that separately
            ]);
        }

        return $this->json([
            'status' => $ret['status'],
            'message' => $ret['message'],
            'total_number_of_rows' => $totalNumberOfRows,
            'records' => $ret['records'],
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'], // old twig-template needs that separately
        ]);
    }


    /**
     * @Route( "/domains/{pageIndex}", name="pagingDomains", methods={"POST"})
     * @param Request $request
     * @param LoggerInterface $logger
     * @param PagingViewType $pagingViewType
     * @param PagingHandlerService $pagingService
     * @param UsersRepository $usersRepository
     * @param AppCacheService $appCacheService
     * @param DomainsRepository $domainsRepository
     * @return Response
     * @author Aki
     * @internal Manage paging for inbox monitor
     */
    public function getDomainsDataPerPage(
        Request $request,
        LoggerInterface $logger,
        PagingViewType $pagingViewType,
        PagingHandlerService $pagingService,
        UsersRepository $usersRepository,
        AppCacheService $appCacheService,
        DomainsRepository $domainsRepository
    ):Response
    {

        $postBodyJson = $request->getContent();
        $postBody = json_decode($postBodyJson, true, 512, JSON_OBJECT_AS_ARRAY);
        $postBody['page_index'] = $request->get('pageIndex'); // add the by route param provided pageIndex to postBody, to have everything in one place
        $logger->info('$postBody für domains', [$postBody,__METHOD__,__LINE__]);


        // exit when wrong data for view was requested
        if('domains' !==  strtolower(trim($postBody['viewname']))) {
            $logger->error('invalid route for domains paging', [ __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'error: invalid route for contact: '.strtolower(trim($postBody['viewname']))]);
        }

        // exit if not all params are provided
        if(!isset($postBody['seed_pool_group_id'], $postBody['health_status'], $postBody['domain_statusDef'], $postBody['records_per_page'])) {
            $msg = '';
            foreach ( ['seed_pool_group_id', 'health_status',  'domain_statusDef','records_per_page' ] as $param) {
                $msg .= (!isset($postBody[$param])) ? $postBody[$param].' ': '';
            }
            $logger->error('not provided post parameters', ['msg'=> $msg, __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => 'error: not provided post parameters '.$msg]);
        }

        // get data of current user
        $currentUser = $this->getCurrentUser();
        $userEmail = $currentUser['email'];


        //Check for DPA acceptance -> don't know if this is really required here ...
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $message = "User DPA not accepted";
            $logger->warning($message, [ __METHOD__, __LINE__]);
            return $this->json(['status' => false, 'message' => $message]);
        }

        // instantiate your StatementService (here: contact)
        $pagingViewType->setPagingView(strtolower($postBody['viewname']));
        $logger->info('$postBody[viewname]', [$postBody['viewname'], __METHOD__, __LINE__]);
        $logger->info('$pagingViewType', [$pagingViewType->getPagingView(), __METHOD__, __LINE__]);
        $domainViewDataProvider = GenericStatementFactory::createGenericStatementService($pagingViewType);

        if(null === $domainViewDataProvider) {
            return $this->json([
                'status' => false,
                'message' => "could not instantiate Service for {$postBody['viewname']}",
            ]);
        }

        // inject needed objects for "contact" => mio.leads table
        $domainViewDataProvider->inject([
            '$domainsRepository' => $domainsRepository,
            '$apcuCacheService' => $appCacheService,
        ]);

        $domainViewDataProvider->setLimit($postBody['records_per_page']);
        $domainViewDataProvider
            ->setSelection('viewname', '=', $postBody['viewname'])
            ->setSelection('company_id', '=', $currentUser['company_id'])
            ->setSelection('seed_pool_group_id', '=', $postBody['seed_pool_group_id'])
            ->setSelection('health_status', '=', $postBody['health_status'])
            ->setSelection('domain_status', '=', $postBody['domain_statusDef'])
            ->setSelection('search_string', '=', $postBody['search_string']);


        $baseSelection = clone $domainViewDataProvider->getSelection(); // keep the base selection separately, since it's used as part of cache key and cannot change for a specific user defined selection
        $logger->info('$baseSelection', [$baseSelection->getAllSelectionSettings(), __METHOD__, __LINE__]);

        // set the indexSelection returned by $pagingService->managePaging
        $offValue = $pagingService->managePagingOffSetValueForPaginationBar($postBody['records_per_page'],(int)$postBody['page_index']);
        $domainViewDataProvider->setSelection('domains.offset', '=', $offValue);

        $totalNumberOfRows = $domainViewDataProvider->getTotalNumberOfRows($baseSelection, $currentUser);
        $ret = $domainViewDataProvider->runSelection($domainViewDataProvider->getSelection()); // returns ['status' => true|false, 'message' => 'some message', 'minId' => leads.id, 'maxId' => leads.id, 'c_leads' => [] ]  => different arrays of records need in TWIG-Template

        if( false === $ret['status']) {
            return $this->json([
                'status' => $ret['status'],
                'message' => $ret['message'],
                'total_number_of_rows' => 0,
                'records' => [],
                'current_user' => $currentUser,
                'account' => $currentUser['company_name'] // old twig-template needs that separately
            ]);
        }

        return $this->json([
            'status' => $ret['status'],
            'message' => $ret['message'],
            'total_number_of_rows' => $totalNumberOfRows,
            'records' => $ret['records'],
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'], // old twig-template needs that separately
        ]);
    }

    /**
     * @return array
     * @author jsr
     */
    protected function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {

            $company_id = $this->apcuCacheService->get($this->apcuCacheService::SELECTED_COMPANY_ID_OF_USER.$user['email']);
            if(false !== $company_id) {
                $user['company_id'] = $company_id;
            }

            return $user;
        }
        return [];
    }

}
