<?php


namespace App\Controller;

use App\Entity\Company;
use App\Entity\Objectregister;
use App\Repository\AdministratesCompaniesRepository;
use App\Repository\AdministratesUsersRepository;
use App\Repository\AdministrationRepository;
use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\DatatypesRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\SeedPoolRepository;
use App\Repository\ServerConfigurationRepository;
use App\Repository\StandardFieldMappingRepository;
use App\Repository\UserMetaRepository;
use App\Repository\UsersRepository;
use App\Services\AppCacheService;
use App\Services\AuthenticationService;
use App\Services\CryptoService;
use App\Services\FeatureToggleService;
use App\Services\MsMailService;
use App\Services\ProjectServices;
use App\Services\RolesService;
use App\Services\SwiftMailerService;
use App\Services\UtilsService;
use App\Types\AccountStatusTypes;
use App\Types\IntegrationTypes;
use App\Types\StatusdefTypes;
use App\Types\UniqueObjectTypes;
use App\Types\UserMetaTypes;
use App\Types\UserStatusTypes;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AdministrationService;

/**
 *  * @Route("/admintool")
 * Class AdministrationController
 * @package App\Controller
 */
class AdministrationController extends AbstractController
{
    protected $authenticationService;
    protected $authenticationUtils;
    protected $administrationService;
    protected $usersRepository;
    protected $logger;
    protected $user;
    protected $userId;
    /**
     * @var ObjectRegisterRepository
     */
    private $objectRegisterRepository;
    /**
     * @var CompanyRepository
     */
    private $companyRepository;
    private $swiftMailerService;
    private $objectRegisterMetaRepository;
    private $integrationTypes;


    public function __construct(
        AuthenticationService        $authenticationService,
        AuthenticationUtils          $authenticationUtils,
        AdministrationService        $administrationService,
        UsersRepository              $usersRepository,
        CompanyRepository            $companyRepository,
        ObjectRegisterRepository     $objectRegisterRepository,
        SwiftMailerService           $swiftMailerService,
        LoggerInterface              $logger,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        IntegrationTypes             $integrationTypes,
        AppCacheService             $apcuCache
    )
    {
        $this->authenticationService = $authenticationService;
        $this->authenticationUtils = $authenticationUtils;
        $this->administrationService = $administrationService;
        $this->usersRepository = $usersRepository;
        $this->companyRepository = $companyRepository;
        $this->swiftMailerService = $swiftMailerService;
        $this->logger = $logger;
        $this->objectRegisterRepository = $objectRegisterRepository;
        $this->objectRegisterMetaRepository = $objectRegisterMetaRepository;
        $this->integrationTypes = $integrationTypes;
        $this->apcuCache = $apcuCache;
        // set userId
        $userEmail = $authenticationUtils->getLastUsername();
        $this->user = $authenticationService->getUserParametersFromCache($userEmail);
        $this->userId = $this->user['user_id'];
    }


    /**
     * @Route( "/super/useroverview", name="superadminUserOverview", methods={"GET"})
     * @param AdministratesUsersRepository $administratesUsersRepository
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return Response
     */
    public function getUserOverview(
        AdministratesUsersRepository $administratesUsersRepository,
        ObjectRegisterRepository     $objectRegisterRepository,
        FeatureToggleService         $featureToggleService
    ): Response
    {
        $userOverview = $administratesUsersRepository->getUserOverview();
        foreach ($userOverview as $key => $userOV) {
            $date = $objectRegisterRepository->getCompanyMarkedForDeletionDate($userOV['company_id']);
            $userOverview[$key]['markedfordeletion'] = (null === $date) ? '' : $date;
        }


        // show umlaute and ß in account overview
        foreach ($userOverview as $r => $records) {
            foreach ($records as $fieldname => $fieldvalue) {
                if (in_array($fieldname, ['account_name', 'username', 'email'])) {
                    $userOverview[$r][$fieldname] = html_entity_decode($fieldvalue);
                }
            }
        }
        $this->logger->info("userOverview", [print_r($userOverview, true), __METHOD__, __LINE__]);
        //init the message
        $successMessage = null;
        if($this->apcuCache->get('seed_pool_assign_message')){
            //Get the message
            $successMessage = $this->apcuCache->get('seed_pool_assign_message');
            //delete the message
            $this->apcuCache->del('seed_pool_assign_message');
        }


        return $this->render('/account/superadmin/useroverview.html.twig', [
            'current_user' => $this->user,
            'useroverview' => $userOverview,
            'page_css_id' => 'page-account-overview',
            'deletion_date' => date("Y-m-d", strtotime('+30 days')),
            'advertorial_deactivated' => $featureToggleService->getFeatureToggleStatus('Advertorial'),
            'successMessage' => $successMessage,
        ]);
    }


    /**
     * getCountAffectedUsers
     * returns the nunber of users, who data will also be deleted by this action
     * @Route( "/super/getcountaffectedusers", name="adminGetCountAffectedUsers", methods={"POST"})
     * @param Response $response
     * @return JsonResponse
     */
    public function getCountAffectedUsers(Request $request): JsonResponse
    {
        // $email = trim($request->get('email_address_to_delete'));
        $this->logger->info('CONTENT=' . $request->getContent(), [__METHOD__, __LINE__]);
        $this->logger->info('b64dec CONTENT=' . base64_decode($request->getContent()), [__METHOD__, __LINE__]);

        $data = json_decode(base64_decode($request->getContent()));
        $email = $data->email;
        $this->logger->info('EMAIL=' . $email, [__METHOD__, __LINE__]);

        if (empty($email) || $email !== filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->json(['status' => false, 'msg' => "please provide an email address. e" . __LINE__]);
        }

        $userToDelete = $this->usersRepository->getUserByEmail($email);
        // $this->logger->info('USER TO DEL = ' . print_r($userToDelete, true), [__METHOD__, __LINE__]);
        if (empty($userToDelete)) {
            // return $this->json(['status' => false, 'msg' => "please provide a correct email address. e" . __LINE__]); // confuse the user => don't tell the truth - jsr version
            return $this->json([
                'status' => false,
                'msg' => "the email address you entered does not exist. e" . __LINE__,
            ]); // tell the the truth - PO version
        }


        // CHECK IF USER IS PERMITTED
        $user = $this->getCurrentUser();
        if (empty($user) || empty($user["email"]) || (false === $user['is_super_admin'] && false === $user['is_master_admin'])) {
            return $this->json(['status' => false, 'msg' => "you're not permitted for this action. e" . __LINE__]);
        } else {
            $noOfUsersWhosDataIsDeleted = $this->administrationService->getCountUsersOfEmailsCompanyId($userToDelete->getEmail());
            $userIds = $this->administrationService->getUserIdsOfUsersCompany($userToDelete->getCompany()->getId());

            if (($noOfUsersWhosDataIsDeleted - 1) > 0) {
                $firstSentence = sprintf('This action will delete data of <b>%s</b> further users that belong to <b>%s</b> email\'s account! ',
                    ($noOfUsersWhosDataIsDeleted - 1), $userToDelete->getEmail());
            } else {
                $firstSentence = sprintf('The account belonging to email address <b>%s</b> is going be deleted. ',

                    $userToDelete->getEmail());
            }


            $companyName = '';
            try {
                $companyName = '"' . $userToDelete->getCompany()->getName() . '"'; // will fail for user zombie records (occur with shop mvp)
            } catch (Exception $e) {
                $this->logger->info('user record is zombie record',
                    ['userId=' . $userToDelete->getId(), __METHOD__, __LINE__]);
            }

            $msg = sprintf('%sIf you proceed all data %s of account <b>%s</b> will be deleted definitely!',
                $firstSentence,
                (($noOfUsersWhosDataIsDeleted - 1) > 0) ? 'and <b>all users</b>' : '', $companyName);
            return $this->json(['status' => true, 'msg' => $msg]);
        }
    }


    /**
     * @return array
     * @author jsr
     */
    protected function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {
            return $user;
        } else {
            return [];
        }
    }


    /**
     * deleteAccountByEmail
     * @Route( "/super/deleteaccountbyemail", name="adminDeleteAccountByEmail", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @internal  <b style="color:red">check also /config/packages/security.yaml</b>
     */
    public function deleteAccountByEmail(Request $request): JsonResponse
    {
        // $email = trim($request->get('email_address_to_delete'));
        $this->logger->info('CONTENT=' . $request->getContent(), [__METHOD__, __LINE__]);
        $this->logger->info('b64dec CONTENT=' . base64_decode($request->getContent()), [__METHOD__, __LINE__]);

        $data = json_decode(base64_decode($request->getContent()));
        $email = $data->email;
        $this->logger->info('EMAIL=' . $email, [__METHOD__, __LINE__]);

        if (empty($email) || $email !== filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->json(['status' => false, 'msg' => "please provide an email address. e" . __LINE__]);
        }

        $userToDelete = $this->usersRepository->getUserByEmail($email);
        if (empty($userToDelete)) {
            return $this->json(['status' => false, 'msg' => "email address not found. e" . __LINE__]);
        }

        $currentUser = $this->getCurrentUser();
        $this->logger->info('$currentUser', [$currentUser, __METHOD__, __LINE__]);

        if ((empty($currentUser) || empty($currentUser["email"]))) {
            return $this->json(['status' => false, 'msg' => "Seem's your session expired. Login again. e" . __LINE__]);
        }

        // CHECK IF USER IS PERMITTED
        // masterAdmin can only delete user accounts (along with that users company and  ALL data of ALL tables belonging to it) of companies managed by himself
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // master admin check REMOVED due to WEB-4891
        // ############# DO NOT DELETE COMMENTED CODE BELOW #############
        /*
        if($currentUser['is_master_admin'] ) {
            $doesUserManageCompany =  $this->administrationService->doesUserManageCompany($currentUser['user_id'], $userToDelete->getCompany()->getId());
            if(!$doesUserManageCompany) {
                return $this->json([
                    'status' => false,
                    'msg' => "The email you entered does not belong to your managed companies. You're not permitted for this action. e" . __LINE__
                ]);
            }

            $wouldDeleteOwnAccount =  ((int)$currentUser['company_id'] === (int)$userToDelete->getCompany()->getId()) ? true : false;
            if($wouldDeleteOwnAccount) {
                return $this->json([
                    'status' => false,
                    'msg' => "This action would delete your own Master-Admin account. You can delete invited users from this account, but not your own. Please contact a support Super-Admin to delete your account.  e" . __LINE__
                ]);
            }
        }
        */

        // only superAdmins and masterAdmins are allowed to proceed
        if (!($currentUser['is_super_admin'] || !$currentUser['is_master_admin'])) {
            return $this->json(['status' => false, 'msg' => "You're not permitted for this action. e" . __LINE__]);
        }


        $userToDeleteEmail = $userToDelete->getEmail();

        $redirect = ($currentUser["email"] === $userToDelete->getEmail()) ? $request->getBaseUrl() . '/logout' : '';

        $this->logger->info("RUNNING DELETE FOR $userToDeleteEmail", [__METHOD__, __LINE__]);

        $ret = $this->administrationService->deleteAccountByUserEmail($userToDeleteEmail);

        $this->logger->info("AFTER DELETE \$ret=", [$ret, __METHOD__, __LINE__]);

        if ($ret['ret'] === false) {
            $msg = (!empty($ret['msg']['denied'])) ? $ret['msg']['denied'] . ' e' . __LINE__ : 'account could not be deleted. e' . __LINE__;
        }

        return $this->json([
            'status' => $ret['ret'],
            'msg' => ($ret['ret']) ? "the account of {$userToDeleteEmail} has been deleted. i" . __LINE__ : $msg,
            'log' => $ret['msg'],
            'redirect' => $redirect,
        ]);
    }


    /**
     * TODO: Route, which is called by cronjob must be added
     * COMMENT: should work as delete account: delete all users along with that user of their company alias "account"
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function cronDeleteMarkedAccounts(Request $request)
    {
        // $email = trim($request->get('email_address_to_delete'));
        $this->logger->info('CONTENT=' . $request->getContent(), [__METHOD__, __LINE__]);
        $this->logger->info('b64dec CONTENT=' . base64_decode($request->getContent()), [__METHOD__, __LINE__]);

        $data = json_decode(base64_decode($request->getContent()));
        $email = $data->email;
        $this->logger->info('EMAIL=' . $email, [__METHOD__, __LINE__]);

        if (empty($email) || $email !== filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->json(['status' => false, 'msg' => "please provide an email address. e" . __LINE__]);
        }

        $userToDelete = $this->usersRepository->getUserByEmail($email);
        if (empty($userToDelete)) {
            return $this->json(['status' => false, 'msg' => "email address not found. e" . __LINE__]);
        }

        $currentUser = $this->getCurrentUser();
        $this->logger->info('$user', [$currentUser, __METHOD__, __LINE__]);

        if ((empty($currentUser) || empty($currentUser["email"]))) {
            return $this->json(['status' => false, 'msg' => "Seem's your session expired. Login again. e" . __LINE__]);
        }

        // CHECK IF USER IS PERMITTED
        // masterAdmin can only delete user accounts (along with that users company and  ALL data of ALL tables belonging to it) of companies managed by himself
        // -----------------------------------------------------------------------------------------------------------------------------------------------------
        // master admin check REMOVED due to WEB-4891
        // ############# DO NOT DELETE COMMENTED CODE BELOW #############
        /*
        if($currentUser['is_master_admin'] ) {
            $doesUserManageCompany =  $this->administrationService->doesUserManageCompany($currentUser['user_id'], $userToDelete->getCompany()->getId());
            if(!$doesUserManageCompany) {
                return $this->json([
                    'status' => false,
                    'msg' => "The email you entered does not belong to your managed companies. You're not permitted for this action. e" . __LINE__
                ]);
            }

            $wouldDeleteOwnAccount =  ((int)$currentUser['company_id'] === (int)$userToDelete->getCompany()->getId()) ? true : false;
            if($wouldDeleteOwnAccount) {
                return $this->json([
                    'status' => false,
                    'msg' => "This action would delete your own Master-Admin account. You can delete invited users from this account, but not your own. Please contact a support Super-Admin to delete your account.  e" . __LINE__
                ]);
            }
        }
        */

        // only superAdmins and masterAdmins are allowed to proceed
        if (!($currentUser['is_super_admin'] || !$currentUser['is_master_admin'])) {
            return $this->json(['status' => false, 'msg' => "You're not permitted for this action. e" . __LINE__]);
        }


        $userToDeleteEmail = $userToDelete->getEmail();

        $redirect = ($currentUser["email"] === $userToDelete->getEmail()) ? $request->getBaseUrl() . '/logout' : '';

        $this->logger->info("RUNNING DELETE FOR $email", [__METHOD__, __LINE__]);

        $ret = $this->administrationService->deleteAccountByUserEmail($userToDeleteEmail);

        if ($ret['ret'] === false) {
            $msg = (!empty($ret['msg']['denied'])) ? $ret['msg']['denied'] . ' e' . __LINE__ : 'your account could not be deleted. e' . __LINE__;
        }

        return $this->json([
            'status' => $ret['ret'],
            'msg' => ($ret['ret']) ? "your account has been deleted. i" . __LINE__ : $msg,
            'log' => $ret['msg'],
            'redirect' => $redirect,
        ]);
    }


    /**
     * @Route("/master/deleteaccount", name="master.deleteaccount", methods={"GET"})
     * @see      AdministrationController::ajaxMasterMarkForDeletion(), AdministrationController::ajaxMasterReactivateAccount()
     * @author   jsr
     */
    public function getTemplateMasterAdminDeleteAccount(ObjectRegisterRepository $objectRegisterRepository): Response
    {
        $companyObjectRegisterId = $objectRegisterRepository->getCompanyObjectRegisterId($this->user['company_id']);
        $companyMarkedForDeletionDate = $objectRegisterRepository->getCompanyMarkedForDeletionDate($this->user['company_id']);
        $date = date("Y-m-d", strtotime('+30 days'));
        return $this->render('/account/masteradmin/deleteaccount.html.twig', [
            'current_user' => $this->user,
            'marked_for_deletion_date' => $companyMarkedForDeletionDate,
            'deletion_date' => $date,
            'company_objectregister_id' => $companyObjectRegisterId,
        ]);
    }


    /**
     * @Route("/super/deleteaccount", name="super.deleteaccount", methods={"GET"})
     * @see
     * @author   jsr
     */
    public function getTemplateSuperAdminDeleteAccount(ObjectRegisterRepository $objectRegisterRepository): Response
    {
        $companyObjectRegisterId = $objectRegisterRepository->getCompanyObjectRegisterId($this->user['company_id']);

        return $this->render('/account/superadmin/deleteaccount.html.twig', [
            'current_user' => $this->user,
            'company_objectregister_id' => $companyObjectRegisterId,
        ]);
    }


    /**
     * set master admin account to "marked for deletion" in objectregister and add delete timestamp for cron job in object_register_meta
     * @Route( "/master/ajax/markfordeletion", name="masterMarkForDeletion", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @internal  <b style="color:red">check also /config/packages/security.yaml</b>
     * @see       AdministrationController::getTemplateMasterAdminDeleteAccount(), AdministrationController::getTemplateMasterAdminDeleteAccount()
     * @author    jsr
     */
    public function ajaxMasterMarkForDeletion(
        Request                  $request,
        ObjectRegisterRepository $objectRegisterRepository
    ): JsonResponse
    {
        // init
        $objectRegisterMetaId = null;
        $statusdefId = null;

        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        // early exit if token is invalid
        if (!$this->isCsrfTokenValid('masteradmin-deleteaccount', $data['token'])) {
            return $this->json([
                'status' => false,
                'msg' => 'your session seems to be expired, please login again. e' . __LINE__,
                'log' => 'your session seems to be expired, please login again. e' . __LINE__,
                'redirect' => '',
            ]);
        }

        $companyObjectRegisterId = $objectRegisterRepository->getCompanyObjectRegisterId($this->user['company_id']);
        $objectRegisterMetaId = $objectRegisterRepository->addCompanyDeletionDate($companyObjectRegisterId,
            $data['delete_at']);

        if (null !== $objectRegisterMetaId) {
            $statusdefId = $objectRegisterRepository->setCompanyObjectRegisterStatusToDeleted($companyObjectRegisterId);
        }

        if (null === $statusdefId || null === $objectRegisterMetaId) {
            $msg = 'your account could not be set to marked for deletion. e';
        } else {
            $msg = "your account will be deleted on {$data['delete_at']}. i";
        }

        $redirect = '';
        // $redirect = ($currentUser["email"] === $userToDelete->getEmail()) ? $request->getBaseUrl() . '/logout' : '';

        return $this->json([
            'status' => (null === $statusdefId) ? false : true,
            'msg' => $msg,
            'log' => $msg,
            'redirect' => $redirect,
        ]);
    }

    /**
     * set master admin account to "marked for deletion" in objectregister and add delete timestamp for cron job in object_register_meta
     * @Route( "/super/ajax/markfordeletion", name="super.markForDeletion", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @internal  <b style="color:red">check also /config/packages/security.yaml</b>
     * @see       AdministrationController::getTemplateMasterAdminDeleteAccount(), AdministrationController::getTemplateMasterAdminDeleteAccount()
     * @author    jsr
     */
    public function ajaxSuperMarkForDeletion(
        Request                  $request,
        ObjectRegisterRepository $objectRegisterRepository
    ): JsonResponse
    {
        // init
        $objectRegisterMetaId = null;
        $statusdefId = null;

        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        // early exit if token is invalid
        if (!$this->isCsrfTokenValid('superadmin-deleteaccount', $data['token'])) {
            return $this->json([
                'status' => false,
                'msg' => 'your session seems to be expired, please login again.',
                'line' => __LINE__,
                'method' => base64_encode(__METHOD__),
                'redirect' => '',
            ]);
        }

        // DONE: get the $data['company_objectregister_id'] of account from $data['email']
        $companyObjectRegisterId = $objectRegisterRepository->getCompanyObjectRegisterId(
            $this->usersRepository->getUserByEmail($data['email'])->getCompany()->getId()
        );
        $objectRegisterMetaId = $objectRegisterRepository->addCompanyDeletionDate($companyObjectRegisterId,
            $data['delete_at']);

        if (null !== $objectRegisterMetaId) {
            $statusdefId = $objectRegisterRepository->setCompanyObjectRegisterStatusToDeleted($companyObjectRegisterId);
        }

        if (null === $statusdefId || null === $objectRegisterMetaId) {
            $msg = "Account of {$data['email']} could not be set to marked for deletion.";
        } else {
            $msg = "Account of {$data['email']} will be deleted on {$data['delete_at']}";
            // start TODO: put that into a private method -> code review
            $userToDelete = $this->usersRepository->getUserByEmail($data['email']);
            $userCompanyToDelete = $userToDelete->getCompany();
            $userToNotifiy['first_name'] = $userToDelete->getFirstName();
            $userToNotifiy['last_name'] = $userToDelete->getLastName();
            $userToNotifiy['email'] = $userToDelete->getEmail();
            $usersCompany['account_no'] = $userCompanyToDelete->getAccountNo();
            $usersCompany['delete_date'] = $data['delete_at'];
            // end TODO
            $retSend = $this->swiftMailerService->sendAccountDeletionNotification($usersCompany, $userToNotifiy);
        }

        $redirect = '';
        // $redirect = ($currentUser["email"] === $userToDelete->getEmail()) ? $request->getBaseUrl() . '/logout' : '';

        return $this->json([
            'status' => (null === $statusdefId) ? false : true,
            'msg' => $msg . ', ' . $retSend['msg'],
            'redirect' => $redirect,
        ]);
    }


    /**
     * send acount activation reminder
     * @Route( "/super/ajax/accountactivationreminder", name="super.accountActivationReminder", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @internal  <b style="color:red">check also /config/packages/security.yaml</b>
     * @see       AdministrationController::getTemplateMasterAdminDeleteAccount(), AdministrationController::getTemplateMasterAdminDeleteAccount()
     * @author    jsr
     */
    public function ajaxSendAccountActivationReminder(
        Request                  $request,
        ObjectRegisterRepository $objectRegisterRepository
    ): JsonResponse
    {
        // init
        $objectRegisterMetaId = null;
        $statusdefId = null;

        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        // early exit if token is invalid
        if (!$this->isCsrfTokenValid('users-manage-account', $data['token'])) {
            return $this->json([
                'status' => false,
                'msg' => 'your session seems to be expired, please login again.',
                'line' => __LINE__,
                'method' => base64_encode(__METHOD__),
                'redirect' => '',
            ]);
        }


        $user = $this->usersRepository->getUserByEmail($data['email']);
        $userCompany = $user->getCompany();
        $userToNotifiy['first_name'] = $user->getFirstName();
        $userToNotifiy['last_name'] = $user->getLastName();
        $userToNotifiy['email'] = $user->getEmail();
        $company['account_no'] = $userCompany->getAccountNo();

        $retSend = $this->swiftMailerService->sendAccountActivationReminder($company, $userToNotifiy);

        $redirect = '';
        // $redirect = ($currentUser["email"] === $userToDelete->getEmail()) ? $request->getBaseUrl() . '/logout' : '';

        return $this->json([
            'status' => $retSend['ret'],
            'msg' => $retSend['msg'] . ' ' . implode(', ', $userToNotifiy),
            'redirect' => $redirect,
        ]);
    }


    /**
     * send acount activation reminder
     * @Route( "/super/ajax/usersetpasswordreminder", name="super.userSetPasswordReminder", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @internal  <b style="color:red">check also /config/packages/security.yaml</b>
     * @see       AdministrationController::getTemplateMasterAdminDeleteAccount(), AdministrationController::getTemplateMasterAdminDeleteAccount()
     * @author    jsr
     */
    public function ajaxSendUserSetPasswordReminder(
        Request                  $request,
        ObjectRegisterRepository $objectRegisterRepository,
        UserMetaRepository       $userMetaRepository,
        CryptoService            $cryptoService,
        AppCacheService         $apcuCacheService,
        UtilsService             $utilsService
    ): JsonResponse
    {
        // init
        $objectRegisterMetaId = null;
        $statusdefId = null;


        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        $this->logger->info('$data', [$data, __METHOD__, __LINE__]);
        $this->logger->info('$token', [$data['token'], __METHOD__, __LINE__]);
        // early exit if token is invalid
        // TODO: check why it somehow fails even if UI token === $data[ 'token' ]
//        if (!$this->isCsrfTokenValid('users-manage-account', $data[ 'token' ])) {
//            return $this->json([
//                'status' => false,
//                'msg' => 'your session seems to be expired, please login again.',
//                'line' => __LINE__,
//                'method' => base64_encode(__METHOD__),
//                'redirect' => '',
//            ]);
//        }

        $user = $this->usersRepository->getUserByEmail($data['email']);
        // reset account activation countdown
        $userEmailEncrypted = $cryptoService->simpleEncrypt($data['email']);
        $apcuCacheService->addOrOverwrite($apcuCacheService::USERACTIVATION . $userEmailEncrypted, $data['email'], $apcuCacheService::FIFTEEN_MINUTES);
        // $apcuCacheService->get($apcuCacheService::USERACTIVATION.$userEmailEncrypted);
//        $userCompany = $user->getCompany();
//		$userToNotifiy['first_name'] = $user->getFirstName();
//		$userToNotifiy['last_name'] = $user->getLastName();
//		$userToNotifiy['email'] = $user->getEmail();

        // check if registration comes from MVP or MIO, then set corresponding activation link
        $registrationOrigin = $userMetaRepository->getUserRegistrationOrigin($user->getId());

        switch ($registrationOrigin) {
            case UserMetaTypes::REGISTRATION_SOURCE_MIO:

                $activation_link = (stripos(strtolower($request->server->get('SERVER_PROTOCOL')),
                    'https')) ?: 'https://' . $request->server->get('HTTP_HOST') . '/doi/?p=' . base64_encode($userEmailEncrypted);
                break;
            case UserMetaTypes::REGISTRATION_SOURCE_AIO:
                $doi = $utilsService->encryptDataToNameValuePair([
                    'accountNo' => $user->getCompany()->getAccountNo(),
                    'email' => $user->getEmail()
                ]);
                $activation_link = $_ENV['MSAIO_URL'] . '/general/account/activate/?p=' . $doi;
                break;

            default: // use MIO activation link
                $activation_link = (stripos(strtolower($request->server->get('SERVER_PROTOCOL')),
                    'https')) ?: 'https://' . $request->server->get('HTTP_HOST') . '/doi/?p=' . base64_encode($userEmailEncrypted);
        }

//
//		$company['account_no'] = $userCompany->getAccountNo();
//		$company[''] =

        $retSend = $this->swiftMailerService->sendActivationLink($user->getEmail(), $user->getFirstName(), $user->getLastName(), $activation_link, $user->getCompany()->getAccountNo());

        $redirect = '';
        // $redirect = ($currentUser["email"] === $userToDelete->getEmail()) ? $request->getBaseUrl() . '/logout' : '';

        return $this->json([
            'status' => $retSend['ret'],
            'msg' => "User {$user->getEmail()} was informed to activate his MIO account.",
            'redirect' => $redirect,
        ]);
    }





// admin.manageCompanies


    /**
     * set  master admin account to active in objectregister and remove entry from object_register_meta
     * @Route( "/master/ajax/reactivateaccount", name="masterReactivateAccount", methods={"POST"})
     * @param Request $request
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return JsonResponse
     * @see      AdministrationController::ajaxMasterMarkForDeletion(), AdministrationController::ajaxMasterReactivateAccount()
     * @author   jsr
     */
    public function ajaxMasterReactivateAccount(
        Request                  $request,
        ObjectRegisterRepository $objectRegisterRepository
    ): JsonResponse
    {
        // init
        $objectRegisterMetaId = null;
        $statusdefId = null;
        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        // early exit if token is invalid
        if (!$this->isCsrfTokenValid('masteradmin-deleteaccount', $data['token'])) {
            return $this->json([
                'status' => false,
                'msg' => 'your session seems to be expired, please login again. e' . __LINE__,
                'log' => 'your session seems to be expired, please login again. e' . __LINE__,
                'redirect' => '',
            ]);
        }

        $objectRegisterMetaId = $objectRegisterRepository->removeCompanyDeletionDate($data['company_objectregister_id']);

        if (null !== $objectRegisterMetaId) {
            $accountStatusType = AccountStatusTypes::ACTIVE;
            $statusdefId = $objectRegisterRepository->setCompanyObjectRegisterStatusToAccountStatusType($data['company_objectregister_id'],
                $accountStatusType);
        }

        if (null === $statusdefId || null === $objectRegisterMetaId) {
            $msg = 'your account could not be reactivated. e' . __LINE__;
        } else {
            $msg = "your account was reactivated. i" . __LINE__;
        }

        $redirect = '';
        // $redirect = ($currentUser["email"] === $userToDelete->getEmail()) ? $request->getBaseUrl() . '/logout' : '';

        return $this->json([
            'status' => (null === $statusdefId) ? false : true,
            'msg' => $msg,
            'log' => $msg,
            'redirect' => $redirect,
        ]);
    }


    /**
     * super admin only. set account to active in objectregister and remove entry from object_register_meta
     * @Route( "/super/ajax/reactivateaccount", name="super.reactivateAccount", methods={"POST"})
     * @param Request $request
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return JsonResponse
     * @see      AdministrationController::ajaxMasterMarkForDeletion(), AdministrationController::ajaxMasterReactivateAccount()
     * @author   jsr
     */
    public function ajaxSuperReactivateAccount(
        Request                  $request,
        ObjectRegisterRepository $objectRegisterRepository
    ): JsonResponse
    {
        // init
        $objectRegisterMetaId = null;
        $statusdefId = null;
        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        // early exit if token is invalid
        if (!$this->isCsrfTokenValid('superadmin-deleteaccount', $data['token'])) {
            return $this->json([
                'status' => false,
                'msg' => 'your session seems to be expired, please login again.',
                'line' => __LINE__,
                'method' => base64_encode(__METHOD__),
                'redirect' => '',
            ]);
        }

        // TODO: get the $data['company_objectregister_id'] of account from $data['email']
        $objectRegisterMetaId = $objectRegisterRepository->removeCompanyDeletionDate(
            $this->usersRepository->getUserByEmail($data['email'])->getCompany()->getObjectregister()->getId()
        );

        if (null !== $objectRegisterMetaId) {
            $accountStatusType = AccountStatusTypes::ACTIVE;
            $statusdefId = $objectRegisterRepository->setCompanyObjectRegisterStatusToAccountStatusType($data['company_objectregister_id'],
                $accountStatusType);
        }

        if (null === $statusdefId || null === $objectRegisterMetaId) {
            $msg = "Account of {$data['email']} could not be reactivated.";
        } else {
            $msg = "Account of {$data['email']} was reactivated successfully.";
        }

        $redirect = '';
        // $redirect = ($currentUser["email"] === $userToDelete->getEmail()) ? $request->getBaseUrl() . '/logout' : '';

        return $this->json([
            'status' => (null === $statusdefId) ? false : true,
            'msg' => $msg,
            'line' => __LINE__,
            'method' => base64_encode(__METHOD__),
            'redirect' => $redirect,
        ]);
    }


    /**
     * @Route( "/master/managecompanies", name="master.managecompanies", methods={"GET"})
     */
    public function adminManageCompanies(
        AdministratesCompaniesRepository $administratesCompaniesRepository,
        CompanyRepository                $company,
        AuthenticationUtils              $authenticationUtils,
        AuthenticationService            $authenticationService
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $this->logger->info('USER=', [$user, __METHOD__, __LINE__]); // todo remove

        if ($user['is_admin']) { // user is simple admin
            $adminstratesCompanies[$user['company_id']] = $user['company_name'];
        } elseif ($user['is_master_admin']) {
            $adminstratesCompanies = $administratesCompaniesRepository->getManagedCompanies($user['user_id']);
        } elseif ($user['is_super_admin']) {
            $adminstratesCompanies = $administratesCompaniesRepository->getManagedCompanies();
        }

        $response['current_user'] = $user;
        $response['company'] = $company->getCompanyDetailById($user['company_id']);
        $response['adminstrates_companies'] = $adminstratesCompanies;
        return $this->render('/account/masteradmin/managecompanies.twig', $response);
    }


    /**
     * @Route( "/master/manageusers", name="master.manageusers", methods={"GET"})
     */
    public function adminManageUsers(
        AdministratesCompaniesRepository $administratesCompaniesRepository,
        AdministratesUsersRepository     $administratesUsersRepository,
        CompanyRepository                $company,
        AuthenticationUtils              $authenticationUtils,
        AuthenticationService            $authenticationService,
        LoggerInterface                  $logger
    )
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());

        if ($user['is_admin']) { // user is simple admin
            $adminstratesCompanies[] = ['id' => $user['company_id'], 'name' => $user['company_name']];
            $administratesUsers = $administratesUsersRepository->getAdministratedUsersForAdmin($user['company_id'],
                $user['user_id']);
        } elseif ($user['is_master_admin']) {
            $adminstratesCompanies = $administratesCompaniesRepository->getManagedCompanies($user['user_id']);
            $administratesUsers = $administratesUsersRepository->getAdministratedUsersForMasterAdmin($user['user_id']);
        } elseif ($user['is_super_admin']) {
            $adminstratesCompanies = $administratesCompaniesRepository->getManagedCompanies();
            $administratesUsers = $administratesUsersRepository->getAdministratedUsersForSuperAdmin($user['user_id']);
        }

        $this->logger->info('USER=', [$user, __METHOD__, __LINE__]); // todo remove
        $response['current_user'] = $user;
        $response['company'] = $company->getCompanyDetailById($user['company_id']);
        $response['adminstrates_companies'] = $adminstratesCompanies;
        $response['administrates_users'] = $administratesUsers;
        return $this->render('/account/masteradmin/manageusers.twig', $response);
    }


    /**
     * @param Request $request
     * @Route("/master/ajax/getusersbycompanyid", name="master.getusersbycompanyid", methods={"POST"})
     */
    public function ajaxGetUsersByCompanyId(Request $request): JsonResponse
    {
        $data = $request->getContent();
        $obj = json_decode(base64_decode($data));
        $selectedCompany = $obj->selected_company;
        $this->logger->info('selected_company, data', [$selectedCompany, $data, __METHOD__, __LINE__]);
        $content = $this->administrationService->getAdministratedUsersForJson($selectedCompany);
        $this->logger->info('selected_company $content = ', [$content, __METHOD__, __LINE__]);
        return $this->json([
            'status' => (!empty($content)) ? true : false,
            'msg' => '',
            'content' => (!empty($content)) ? $content : [],
        ]);
    }


    /**
     * @Route("/master/ajax/inviteuser", name="master.inviteuser", methods={"POST"})
     */
    public function ajaxInviteUser(Request $request, AdministrationService $administrationService): JsonResponse
    {
        $data = $request->getContent();
        $userData = $this->getUserFormData($data);

        if (!$this->isCsrfTokenValid('users-manage', $userData['token'])) {
            return $this->json([
                'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                'status' => false,
            ]);
        }

        $invitedUser = $administrationService->inviteUser(stripos(strtolower($request->server->get('SERVER_PROTOCOL')),
            'https') ? 'https://' : 'http://' . $request->server->get('HTTP_HOST') . '/invitation/doi/?p=',
            $userData['salutation'], $userData['first_name'],
            $userData['last_name'], $userData['email'], (int)base64_decode($userData['to_company']),
            $userData['role']);

        $this->logger->info('$invitedUser', [$invitedUser, __METHOD__, __LINE__]);

        return $this->json($invitedUser);
    }


    /**
     * @param string $requestContent
     * @return array
     */
    private function getUserFormData(string $requestContent): array
    {
        $userData = [];
        try {
            $b64 = utf8_encode(base64_decode($requestContent)); // use utf8_encode for chars like äöüÄÖÜß and so on
            $data = json_decode($b64, true, 200, JSON_OBJECT_AS_ARRAY);

            foreach ($data as $d) {
                $userData[$d['name']] = $d['value'];
            }
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage(), [__METHOD__, __LINE__]);
            return $userData;
        }
        return $userData;
    }


    /**
     * @Route("/master/ajax/updateuser", name="master.updateuser", methods={"POST"})
     */
    public function ajaxUpdateUser(Request $request, LoggerInterface $logger): JsonResponse
    {
        $userData = $this->getUserFormData($request->getContent());
        if (!$this->isCsrfTokenValid('users-manage-account', $userData['token'])) {
            return $this->json([
                'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                'status' => false,
            ]);
        }

        $content = $this->administrationService->updateUser($userData);
        $logger->info('CONTENT AFTER UPDATE USER = ', [$content, __METHOD__, __LINE__]);


        return (!empty($content)) ? $this->json([
            'status' => true,
            'msg' => 'User updated. i' . __LINE__,
            'content' => $content,
        ]) : $this->json(['status' => false, 'msg' => 'Failed to update user. e' . __LINE__, 'content' => []]);
    }

    /**
     * @Route("/master/ajax/toggle/advertorial", name="master.toggle.advertorial", methods={"POST"})
     */
    public function ajaxToggleAdvertorial(Request $request, LoggerInterface $logger, ObjectRegisterMetaRepository $objectRegisterMetaRepository, CompanyRepository $companyRepository): JsonResponse
    {
        $this->logger->info('ADVERTORIAL json dec', [print_r((array)json_decode(base64_decode($request->getContent())), true),  __METHOD__, __LINE__]);

        $userData = (array)json_decode(base64_decode($request->getContent()));

        $this->logger->info('token', [$userData['token'], __METHOD__, __LINE__]);

//        if (!$this->isCsrfTokenValid('users-manage', $userData['token']) ||
//            !$this->isCsrfTokenValid('users-manage-account', $userData['token'])) {
//                return $this->json([
//                'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
//                'status' => false,
//            ]);
//        }

        $objRegMetaAdvertorial = $objectRegisterMetaRepository->getObjectRegisterMetaDetails($userData['company_objectregister_id'], $objectRegisterMetaRepository::METAKEY_ADVERTORIAL_STATUS);
        $this->logger->info('$objRegMetaAdvertorial', [$objRegMetaAdvertorial, __METHOD__, __LINE__]);

        if(empty($objRegMetaAdvertorial)) {
            $status = $objectRegisterMetaRepository->insert($userData['company_objectregister_id'], $objectRegisterMetaRepository::METAKEY_ADVERTORIAL_STATUS, '1');
            $status = (null === $status) ? false : true;
            $this->logger->info('insert status', [$status, __METHOD__, __LINE__]);
        } else {
            $status = $objectRegisterMetaRepository->update(
                $objRegMetaAdvertorial['id'],
                $userData['company_objectregister_id'],
                $objectRegisterMetaRepository::METAKEY_ADVERTORIAL_STATUS,
                ((!$objRegMetaAdvertorial['meta_value']) === false) ? '0' : '1'
            );
            $this->logger->info('update status', [$status, __METHOD__, __LINE__]);
        }

        return $this->json([
            'status' => $status,
            'msg' => 'Advertorial access is switched!',
        ]);
    }


    /**
     * @Route("/master/ajax/blockuser", name="master.blockuser", methods={"POST"})
     */
    public function ajaxBlockUser(Request $request): JsonResponse
    {
        $currentUser = $this->getCurrentUser();

        $userData = $this->getUserFormData($request->getContent());
        $this->logger->info('BLOCK USER', [$userData, __METHOD__, __LINE__]);

        // check token either from manageusers.twig (users-manage) or from useroverview.html.twig (users-manage-account)
        if (!$this->isCsrfTokenValid('users-manage', $userData['token']) ||
            !$this->isCsrfTokenValid('users-manage-account', $userData['token'])) {
            return $this->json([
                'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                'status' => false,
            ]);
        }

        $userData['selected_user'] = (int)base64_decode($userData['selected_user']);

        $user = $this->usersRepository->blockUserById($userData['selected_user']);
        if (null != $user) {
            $this->logger->info('BLOCK USER', [
                (int)$currentUser['user_id'],
                (int)$userData['selected_user'],
                (int)$userData['selected_company'],
                __METHOD__,
                __LINE__,
            ]);
            $content = $this->administrationService->getAdministratedUsersSingleRecord(($currentUser['is_super_admin'] === true) ? -1 : (int)$currentUser['user_id'],
                (int)$userData['selected_user'], (int)$userData['selected_company']);
        }
        $this->logger->info('content', [$content, __METHOD__, __LINE__]);

        return (!empty($content)) ? $this->json([
            'status' => true,
            'msg' => 'User successfully blocked. i' . __LINE__,
            'content' => $content,
        ]) : $this->json(['status' => false, 'msg' => 'Failed to block user. e' . __LINE__, 'content' => []]);
    }

    /**
     * @Route("/super/ajax/blockuserbyadmin", name="super.blockuserbyadmin", methods={"POST"})
     * @param Request $request
     * @param UsersRepository $usersRepository
     * @return JsonResponse
     */
    public function blockUserByAdmin(Request $request, UsersRepository $usersRepository): JsonResponse
    {
        try {
            $payload = json_decode(base64_decode($request->getContent()), true, 512,
                JSON_THROW_ON_ERROR);
            if (empty($payload['email']) || empty($payload['token'])) {
                throw new RuntimeException('Invalid Request to Block User');
            }
            if (!$this->isCsrfTokenValid('users-manage-account', $payload['token'])) {
                throw new RuntimeException('Seems your session expired. Please refresh page.');
            }
            $user = $usersRepository->getUserByEmail($payload['email']);
            $this->logger->info('$user', [$payload['email'], $user->getId(), __FUNCTION__, __LINE__]);

            if (null === $user || false === $usersRepository->updateStatus((int)$user->getId(), UserStatusTypes::BLOCKBYADMIN)) {
                throw new RuntimeException('Failed to block user by admin');
            }
            return $this->json([
                'msg' => 'Successfully blocked the user with E-Mail ' . $user->getEmail(),
                'status' => true,
            ]);
        } catch (JsonException|RuntimeException|Exception $e) {
            return $this->json([
                'msg' => 'Error :' . $e->getMessage() . ' e' . __LINE__,
                'status' => false,
            ]);
        }
    }

    /**
     * @Route("/super/ajax/unblockbuseryadmin", name="super.unblockuserbyadmin", methods={"POST"})
     * @param Request $request
     * @param UsersRepository $usersRepository
     * @return JsonResponse
     */
    public function unblockUserByAdmin(Request $request, UsersRepository $usersRepository): JsonResponse
    {
        try {
            $payload = json_decode(base64_decode($request->getContent()), true, 512,
                JSON_THROW_ON_ERROR);
            if (empty($payload['email']) || empty($payload['token'])) {
                throw new RuntimeException('Invalid Request to Block User');
            }
            if (!$this->isCsrfTokenValid('users-manage-account', $payload['token'])) {
                throw new RuntimeException('Seems your session expired. Please refresh page.');
            }
            $user = $usersRepository->getUserByEmail($payload['email']);
            if ($user === null ||
                !$usersRepository->updateStatus((int)$user->getId(), UserStatusTypes::ACTIVE)) {
                throw new RuntimeException('Failed to unblock user by admin');
            }
            return $this->json([
                'msg' => 'Successfully blocked the user with E-Mail ' . $user->getEmail(),
                'status' => true,
            ]);
        } catch (JsonException|RuntimeException|Exception $e) {
            return $this->json([
                'msg' => 'Error :' . $e->getMessage() . __LINE__,
                'status' => false,
            ]);
        }
    }

    /**
     * @Route("/master/ajax/blockaccount", name="master.blockaccount", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxBlockAccount(Request $request): JsonResponse
    {
        $currentUser = $this->getCurrentUser();
        $c = $request->getContent();
        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        $this->logger->info('BLOCK ACCOUNT', [$data, __METHOD__, __LINE__]);

        // check token either from manageusers.twig (users-manage) or from useroverview.html.twig (users-manage-account)
        if (!$this->isCsrfTokenValid('masteradmin-blockaccount', $data['token'])) {
            return $this->json([
                'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                'status' => false,
            ]);
        }
        // get companyObjectRegisterId
        $companyObjectregisterId = $data['company_objectregister_id'];
        if (!$this->objectRegisterRepository->updateStatus($companyObjectregisterId,
            AccountStatusTypes::BLOCK)) {
            return $this->json([
                'msg' => 'Error: Failed to block Account',
                'status' => false,
            ]);
        }

        $companyDetails = $this->companyRepository->getCompanyDetailsByObjectRegisterId($companyObjectregisterId);

        if (empty($companyDetails)) {
            return $this->json([
                'msg' => 'Error: Failed to get CompanyDetails ',
                'status' => false,
            ]);
        }
        $usersIds = $this->usersRepository->getAllUserIdsByCompanyId($companyDetails['id']);

        // Change the status of all the Users to Block.
        foreach ($usersIds as $userId) {
            if (is_null($this->usersRepository->blockUserById($userId['id']))) {
                $this->logger->error('Failed to block User with Id ' . $userId);
                continue;
            }
        }

        return $this->json([
            'status' => true,
            'msg' => 'Account successfully blocked. i' . __LINE__,
        ]);
    }

    /**
     * @Route("/super/ajax/unblockaccount", name="super.unblockaccount", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxUnblockAccount(Request $request): JsonResponse
    {
        $currentUser = $this->getCurrentUser();
        $c = $request->getContent();
        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        $this->logger->info('UNBLOCK ACCOUNT', [$data, __METHOD__, __LINE__]);
        // check token either from manageusers.twig (users-manage) or from useroverview.html.twig (users-manage-account)
        if (!$this->isCsrfTokenValid('masteradmin-blockaccount', $data['token'])) {
            return $this->json([
                'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                'status' => false,
            ]);
        }
        // get companyObjectRegisterId
        $companyObjectregisterId = $data['company_objectregister_id'];
        if (!$this->objectRegisterRepository->updateStatus($companyObjectregisterId,
            AccountStatusTypes::ACTIVE)) {
            return $this->json([
                'msg' => 'Error: Failed to unblock Account',
                'status' => false,
            ]);
        }

        $companyDetails = $this->companyRepository->getCompanyDetailsByObjectRegisterId($companyObjectregisterId);

        if (empty($companyDetails)) {
            return $this->json([
                'msg' => 'Error: Failed to get CompanyDetails ',
                'status' => false,
            ]);
        }
        $usersIds = $this->usersRepository->getAllUserIdsByCompanyId($companyDetails['id']);
        // Change the status of all the Users to Block.
        foreach ($usersIds as $userId) {
            if (is_null($this->usersRepository->unblockUserById($userId['id']))) {
                $this->logger->error('Failed to unblock User with Id ' . $userId);
                continue;
            }
        }
        return $this->json([
            'status' => true,
            'msg' => 'Account successfully unblocked. i' . __LINE__,
        ]);

    }


    /**
     * @Route("/master/ajax/unblockuser", name="master.unblockuser", methods={"POST"})
     */
    public function ajaxUnBlockUser(Request $request): JsonResponse
    {
        $currentUser = $this->getCurrentUser();
        $userData = $this->getUserFormData($request->getContent());

        // check token either from manageusers.twig (users-manage) or from useroverview.html.twig (users-manage-account)
        if (!$this->isCsrfTokenValid('users-manage', $userData['token']) ||
            !$this->isCsrfTokenValid('users-manage-account', $userData['token'])) {
            return $this->json([
                'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                'status' => false,
            ]);
        }

        $userData['selected_user'] = (int)base64_decode($userData['selected_user']);

        $user = $this->usersRepository->unblockUserById($userData['selected_user']);
        if (null != $user) {
            $content = $this->administrationService->getAdministratedUsersSingleRecord(($currentUser['is_super_admin'] === true) ? -1 : (int)$currentUser['user_id'],
                (int)$userData['selected_user'], (int)$userData['selected_company']);
        }

        $this->logger->info('content', [$content, __METHOD__, __LINE__]);
        return (!empty($content)) ? $this->json([
            'status' => true,
            'msg' => 'User successfully unblocked. i' . __LINE__,
            'content' => $content,
        ]) : $this->json(['status' => false, 'msg' => 'Failed to unblock user. e' . __LINE__, 'content' => []]);
    }


    /**
     * called from /templates/account/superadmin/useroverview.html.twig
     * @Route("/super/ajax/unblockuser", name="super.unblockuser", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxUnblockUserFromUseroverview(Request $request): JsonResponse
    {
        $user = null;
        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        // early exit if token is invalid
        if (!$this->isCsrfTokenValid('users-manage-account', $data['token'])) {

            // return with error
            return $this->json([
                'status' => false,
                'msg' => 'your session seems to be expired, please login again.',
                'line' => __LINE__,
                'method' => base64_encode(__METHOD__),
                'redirect' => '',
            ]);
        }

        if (!empty($data['email'])) {
            $user = $this->usersRepository->unblockUserById(
                $this->usersRepository->getUserByEmail($data['email'])->getId()
            );

            if (null !== $user) {

                // return with success
                return $this->json([
                    'status' => true,
                    'msg' => "User {$data['email']} was unblocked.",
                    'line' => __LINE__,
                    'method' => base64_encode(__METHOD__),
                    'redirect' => '',
                ]);
            }
        }

        // return with error
        return $this->json([
            'status' => false,
            'msg' => "User {$data['email']} could not be unblocked.",
            'line' => __LINE__,
            'method' => base64_encode(__METHOD__),
            'redirect' => '',
        ]);
    }


    /**
     * called from /templates/account/superadmin/useroverview.html.twig
     * @Route("/super/ajax/blockuser", name="super.blockuser", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxBlockUserFromUseroverview(Request $request): JsonResponse
    {
        $user = null;
        $data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

        // early exit if token is invalid
        if (!$this->isCsrfTokenValid('users-manage-account', $data['token'])) {

            // return with error
            return $this->json([
                'status' => false,
                'msg' => 'your session seems to be expired, please login again.',
                'line' => __LINE__,
                'method' => base64_encode(__METHOD__),
                'redirect' => '',
            ]);
        }

        if (!empty($data['email'])) {
            $blocked = $this->usersRepository->blockUser($data['email']);
            if ($blocked) {

                // return with success
                return $this->json([
                    'status' => true,
                    'msg' => "User {$data['email']} was blocked.",
                    'line' => __LINE__,
                    'method' => base64_encode(__METHOD__),
                    'redirect' => '',
                ]);
            }
        }

        // return with error
        return $this->json([
            'status' => false,
            'msg' => "User {$data['email']} could not be blocked.",
            'line' => __LINE__,
            'method' => base64_encode(__METHOD__),
            'redirect' => '',
        ]);


    }


    /**
     * @Route("/super/ajax/sendreminder", name="super.sendreminder", methods={"POST"})
     */
    public function ajaxSendReminder(Request $request): JsonResponse
    {
        $sendEmailSuccess = false;

        $currentUser = $this->getCurrentUser();

        $payload = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);
        // check token from useroverview.html.twig (users-manage-account)
        if (!$this->isCsrfTokenValid('users-manage-account', $payload['token'])) {
            return $this->json([
                'msg' => 'Error: Seems your session has expired. Please refresh page.  e' . __LINE__,
                'status' => false,
            ]);
        }

        $reminderType = $payload['remindertype'];
        $user = $this->usersRepository->getUserByEmail($payload['email']);

        if (null !== $user) {
            // TODO: send email by msmail-call
            $sendEmailSuccess = true;
        } else {
            $sendEmailSuccess = false;
        }


        if ($sendEmailSuccess) {
            return $this->json([
                'status' => true,
                'msg' => "Reminder send to  {$payload['email']}",
                'line' => __LINE__,
                'method' => base64_encode(__METHOD__),
                'redirect' => '',
            ]);
        } else {
            return $this->json([
                'status' => true,
                'msg' => "Failed to send reminder to  {$payload['email']}",
                'line' => __LINE__,
                'method' => base64_encode(__METHOD__),
                'redirect' => '',
            ]);
        }


    }


    /**
     * @Route("/master/ajax/deleteuser", name="master.deleteuser", methods={"POST"})
     */
    public function ajaxDeleteUser(Request $request): Response
    {
        $userData = $this->getUserFormData($request->getContent());

        //  "email":"script.sql@web.de","role":"ADMIN","to_company":"Nzk=",
        if (!$this->isCsrfTokenValid('users-manage', $userData['token'])) {
            return $this->json([
                'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                'status' => false,
            ]);
        }
        $ret = $this->administrationService->deleteUserByIdAndCompanyInUsersAndAdministratesUsers($userData['email'],
            (int)base64_decode($userData['to_company']));
        $this->logger->info('$ret', [$ret, __METHOD__, __LINE__]);

        return $this->json($ret);
    }


    /**
     * @param CompanyRepository $company
     * @return JsonResponse
     * @Route( "/master/ajax/getcompanybyid", name="acc.ajaxGetCompanyById", methods={"POST"})
     * @internal called by managecompanies.twig
     */
    public function ajaxGetCompanyById(
        Request               $request,
        CompanyRepository     $companyRepository,
        AuthenticationUtils   $authenticationUtils,
        AuthenticationService $authenticationService,
        LoggerInterface       $logger
    ): JsonResponse
    {
        $data = json_decode(base64_decode($request->getContent()));
        $logger->info("DATA", [$data, __METHOD__, __LINE__]);
        $company_id = (int)trim($data->company_id);

        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (!empty($user)) {
            $content = $companyRepository->adminGetCompanyById($company_id);
            if (!empty($content)) {
                return $this->json(['status' => true, 'msg' => '', 'content' => $content]);
            }
        }
        return $this->json(['status' => false, 'msg' => 'Error e' . __LINE__, 'content' => []]);
    }


    /**
     * @param Request $request
     * @param CompanyRepository $companyRepository
     * @param AdministratesCompaniesRepository $administratesCompaniesRepository
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @throws MappingException
     * @throws ORMException
     * @throws OptimisticLockException
     * @internal WEB-3850
     * @author   jsr
     * @Route( "/master/ajax/addnewcompany", name="master.ajaxAddNewCompany", methods={"POST"})
     */
    public function ajaxAddNewCompany(
        Request                          $request,
        CompanyRepository                $companyRepository,
        AdministratesCompaniesRepository $administratesCompaniesRepository,
        AdministratesUsersRepository     $administratesUsersRepository,
        ProjectServices                  $projectServices,
        LoggerInterface                  $logger
    ): JsonResponse
    {
        $ipv4 = $request->getClientIp();
        $data = json_decode(utf8_encode(base64_decode($request->getContent())), true, 512, JSON_OBJECT_AS_ARRAY);
        $logger->info('DATA IN = ', [$data, __METHOD__, __LINE__]);

        // todo: rest of code goes to AdminstrationService
        $user = $this->getCurrentUser();

        foreach ($data as $d) {
            if ($d['name'] === 'token') {
                if (!$this->isCsrfTokenValid('company-manage', $d['value'])) {
                    return $this->json([
                        'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                        'status' => false,
                    ]);
                }
            } else {
                $company[$d['name']] = $d['value'];
                $logger->info('DATA=',
                    ['$d[\'name\']' => $d['name'], '$d[\'value\'' => $d['value'], __METHOD__, __LINE__]);
            }
        }
        $newCompany = $companyRepository->masterAdminCreateNewCompany($company);
        $this->logger->info('USER', [$user, __METHOD__, __LINE__]);
        $this->logger->info('$newCompany', [print_r(((array)$newCompany), true), __METHOD__, __LINE__]);

        if (!empty($newCompany)) {
            $administratesCompaniesRepository->addManagedCompany($user['user_id'], $newCompany->getId());
            $administratesUsersRepository->addRecord($user['user_id'], $user['user_id'], $newCompany->getId(),
                $user['user_roles']); // add yourself to be able to work on this company
            $projectServices->createProject($newCompany->getId(), $ipv4);
            return $this->json(['content' => $newCompany, 'msg' => "Success: Company added.", 'status' => true]);
        }

        return $this->json(['content' => $newCompany, 'msg' => "Error: Company not added.", 'status' => false]);
    }


    /**
     * @param Request $request
     * @param CompanyRepository $company
     * @param CampaignRepository $campaignRepository
     * @param AuthenticationService $authenticationService
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @internal WEB-3850
     * @author   jsr
     * @Route( "/master/ajax/updatecompany", name="acc.ajaxUpdateCompany", methods={"POST"})
     */
    public function ajaxUpdateCompany(
        Request               $request,
        CompanyRepository     $companyRepository,
        CampaignRepository    $campaignRepository,
        AuthenticationService $authenticationService,
        LoggerInterface       $logger
    ): JsonResponse
    {
        $company = json_decode(utf8_encode(base64_decode($request->getContent())), true, 512, JSON_OBJECT_AS_ARRAY);

        // todo: rest of code goes to AdminstrationService
        $user = $this->getCurrentUser();

        foreach ($company as $d) {
            if ($d['name'] === 'token') {
                if (!$this->isCsrfTokenValid('company-manage', $d['value'])) {
                    return $this->json([
                        'status' => false,
                        'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                        'content' => '',
                    ]);
                }
            } else {
                $company[$d['name']] = $d['value'];
                $logger->info('DATA=',
                    ['$d[\'name\']' => $d['name'], '$d[\'value\'' => $d['value'], __METHOD__, __LINE__]);
            }
        }

//        NOT HERE, SINCE NAME CANNOT BE CHANGED IN FORM: check if company has campaigns => do not allow to change company name if campaigns exist
//        if ($campaignRepository->hasCompanyIdCampaigns($company['company_id'])) {
//            return $this->json(['status' => false, 'msg' => "Company {$company['company_name']} has campaigns. change of company name not possible. e" . __LINE__, 'content' => $user]);
//        }

        $cmp['id'] = $company['company_id'];
        $cmp['address'] = filter_var($company['street'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp['city'] = filter_var($company['city'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp['company_name'] = filter_var($company['company_name'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp['country'] = filter_var($company['country'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp['delegated_domain'] = filter_var($company['delegated_domain'], FILTER_SANITIZE_URL);
        $cmp['phone'] = filter_var($company['phone'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp['plz'] = filter_var($company['postal_code'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp['url_option'] = filter_var($company['url_option'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp['vat'] = filter_var($company['vat'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        $cmp['website'] = filter_var($company['website'], FILTER_SANITIZE_URL);
        $cmp['updated_by'] = (int)$user['user_id'];

        $user['company_name'] = $company['company_name'];

        $status = $companyRepository->updateCompanyDetails($cmp);
        $updatedCompany = $companyRepository->adminGetCompanyById($company['company_id']);

        if ($status) {
            // update cache
            $current_user = $authenticationService->updateUserParametersInCache($user);
            return $this->json([
                'status' => $status,
                'msg' => 'Success: Company updated.',
                'content' => $updatedCompany,
            ]);
        } else {
            return $this->json([
                'status' => $status,
                'msg' => 'Error: Company update failed. e' . __LINE__,
                'content' => [],
            ]);
        }
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @Route( "/master/ajax/deletecompany", name="acc.ajaxDeleteCompany", methods={"POST"})
     */
    public function ajaxDeleteCompany(
        Request                          $request,
        AdministratesCompaniesRepository $administratesCompaniesRepository,
        AdministratesUsersRepository     $administratesUsersRepository,
        CompanyRepository                $companyRepository,
        LoggerInterface                  $logger
    ): JsonResponse
    {
        $user = $this->getCurrentUser();

        $company = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);
        foreach ($company as $d) {
            if ($d['name'] === 'token') {
                if (!$this->isCsrfTokenValid('company-manage', $d['value'])) {
                    return $this->json([
                        'status' => false,
                        'msg' => 'Error: Seems your session expired. Please refresh page.  e' . __LINE__,
                        'content' => '',
                    ]);
                }
            } else {
                $company[$d['name']] = $d['value'];
                $logger->info('DATA=',
                    ['$d[\'name\']' => $d['name'], '$d[\'value\'' => $d['value'], __METHOD__, __LINE__]);
            }
        }

        $company_id = $company['company_id'];
        $user_id = $user['user_id'];

        if ((int)$user['company_id'] == $company_id) {
            $numberOf = $administratesUsersRepository->getNoUsersCampaignsByCompanyId($company_id);
            if (!empty($numberOf) && ($numberOf['count_users'] > 0 || $numberOf['count_campaigns'] > 0)) {
                return $this->json([
                    'ret' => false,
                    'msg' => 'Deletion denied: You are signed on to this company. Please change the account or contact your support. To delete this company account, delete first all user and all campaigns.',
                ]);
            }
        }

        $this->logger->info('COMPANY_ID=' . $company_id, [__METHOD__, __LINE__]);

        $userManagesCompany = $administratesCompaniesRepository->doesUserManageCompany($user_id, $company_id);

        if (!$userManagesCompany && !$user['is_super_admin']) {
            return $this->json(['status' => false, 'msg' => "You are not administrator of this company. e" . __LINE__]);
        }

        // CHECK IF USER IS PERMITTED
        if (empty($user) || (!$user['is_super_admin'] && !$user['is_master_admin'])) {
            return $this->json(['status' => false, 'msg' => "you're not permitted for this action. e" . __LINE__]);
        } else {
            $this->logger->info("RUNNING DELETE FOR COMPANY #$company_id", [__METHOD__, __LINE__]);
            $company = $companyRepository->findOneBy(['id' => $company_id]);
            $companyName = $company->getName();

            $ret = $this->administrationService->deleteCompanyById($company_id);
            return $this->json([
                'status' => $ret['ret'],
                'msg' => "All data belonging to {$companyName} has been deleted. i" . __LINE__,
                'content' => $ret['msg'],
            ]);
        }
    }


    /**
     * @Route( "/super/emailserver", name="superadminEmailServer.get", methods={"GET"})
     * @param AuthenticationService $authenticationService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param IntegrationTypes $integrationTypes
     * @param AuthenticationUtils $authenticationUtils
     * @param LoggerInterface $logger
     * @return Response
     * @throws JsonException
     * @author Pradeep
     */
    public function getSuperAdminEmailServer(
        AuthenticationService        $authenticationService,
        ObjectRegisterRepository     $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        IntegrationTypes             $integrationTypes,
        AuthenticationUtils          $authenticationUtils,
        LoggerInterface              $logger
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $objRegId = $objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        // Get Email Configuration from Database
        if ($objRegId !== null) {
            $objRegMetaDetails = $objectRegisterMetaRepository->getJsonMetaValueAsArray($objRegId,
                UniqueObjectTypes::EMAILSERVER);
            $eMailIntegrationConfig = $objRegMetaDetails;
        }
        // When no email server configuration found
        if (empty($eMailIntegrationConfig)) {
            $eMailIntegrationConfig = $integrationTypes->getSuperAdminEmailServerConfig();
        }
        //$eMailIntegrationConfig['b64msmail'] = base64_encode($_ENV['MSMAIL_URL'] . '/email/test_cio_config');
        $eMailIntegrationConfig['b64msmail'] = base64_encode('/admintool/super/emailserver/test');
        return $this->render('/account/superadmin/emailserver.twig',
            ['current_user' => $user, 'emailserver' => $eMailIntegrationConfig, 'success' => '', 'error' => '']);
    }


    /**
     * @Route( "/super/emailserver/test", name="emailServerTest", methods={"POST"})
     * @param Request $request
     * @param LoggerInterface $logger
     * @return Response
     * @author aki
     * @internal Make call to ms-email to test the configuration form the superadmin page
     */
    public function testEmailServerConfig(
        Request                      $request,
        MsMailService                $msMailService,
        LoggerInterface              $logger
    ): Response
    {
        $emailConfigData = $request->getContent();
        //send the data to ms email and check the configuration
        $status = $msMailService->sendTestEmail($emailConfigData);
        return $this->json([$status]);
    }


    /**
     * @Route( "/super/emailserver", name="superadminEmailServer.post", methods={"POST"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param IntegrationTypes $integrationTypes
     * @param AuthenticationUtils $authenticationUtils
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function postSuperAdminEmailServer(
        Request                      $request,
        AuthenticationService        $authenticationService,
        ObjectRegisterRepository     $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        IntegrationTypes             $integrationTypes,
        AuthenticationUtils          $authenticationUtils,
        LoggerInterface              $logger
    ): Response
    {
        try {
            $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
            $companyId = $user['company_id'];
            // Verify the csrf token
            $objRegId = $objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
            //if null create objectregister entry for superadmin
            $objRegId = is_null($objRegId) ? $objectRegisterRepository->createInitialObjectRegisterEntry($companyId,
                UniqueObjectTypes::SUPERADMIN) : $objRegId;
            if (is_null($objRegId) || $objRegId <= 0) {
                throw new RuntimeException('Failed to create Object Register Id for SuperAdmin');
            }
            // Get the old settings
            $objRegMetaDetails = $objectRegisterMetaRepository->getObjectRegisterMetaDetails($objRegId,
                UniqueObjectTypes::EMAILSERVER);
            // Get the type for the admin email server config
            $eMailServerConfig = $integrationTypes->getSuperAdminEmailServerConfig();
            // fill the meat with the data from the UI
            $eMailServerConfig['relation']['superadmin_objectregister_id'] = $objRegId;
            $eMailServerConfig['relation']['superadmin_object_unique_id'] = $objectRegisterRepository->getUniqueObjectTypeIdByName(UniqueObjectTypes::SUPERADMIN);
            $eMailServerConfig['settings']['default_server']['smtp_server_domain'] = $request->get('smtp-server-domain');
            $eMailServerConfig['settings']['default_server']['sender_user_email'] = $request->get('sender-user-email');
            $eMailServerConfig['settings']['default_server']['sender_user_name'] = $request->get('sender-user-name');
            $eMailServerConfig['settings']['default_server']['sender_password'] = $request->get('sender-password');
            $eMailServerConfig['settings']['default_server']['port'] = $request->get('port');
            // Encode to json
            $eMailServerConfigJson = json_encode($eMailServerConfig, JSON_THROW_ON_ERROR);
            if (empty($objRegMetaDetails) || !isset($objRegMetaDetails['id']) || (int)$objRegMetaDetails['id'] <= 0) {
                //insert ObjectRegisterMeta
                if (is_null($objectRegisterMetaRepository->insert($objRegId, UniqueObjectTypes::EMAILSERVER,
                    $eMailServerConfigJson))) {
                    throw new RuntimeException('Failed to save eMIO APIKey to ObjectRegisterMeta');
                }
            } else {
                // Update ObjectRegisterMeta
                $objRegMetaId = $objRegMetaDetails['id'];
                if (!$objectRegisterMetaRepository->update($objRegMetaId, $objRegId, UniqueObjectTypes::EMAILSERVER,
                    $eMailServerConfigJson)) {
                    throw new RuntimeException('Failed to update eMIO APIKey to ObjectRegisterMeta');
                }
            }

            $mailContent = 'Successfully updated Email Server Configuration';
            /* HTML TEST */
            // $mailContentWithPlaceholder = file_get_contents('../../public/assets/email-templates/account-test-mailserver-configuration/successfully-updated-email-server-configuration.twig');
            // $mailContent = preg_replace('/###username###/', "{$user['first_name']}  {$user['last_name']}", $mailContentWithPlaceholder);

            $eMailServerConfig['b64msmail'] = base64_encode($_ENV['MSMAIL_URL'] . '/email/test_cio_config');


            return $this->render('account/superadmin/emailserver.twig', [
                'current_user' => $user,
                'emailserver' => $eMailServerConfig,
                'success' => $mailContent,
                'error' => '',
            ]);
        } catch (Exception $e) {
            $logger->error($e->getMessage());
            return $this->render('account/superadmin/emailserver.twig', [
                'current_user' => $user,
                'emailserver' => $eMailServerConfig,
                'success' => '',
                'error' => $e->getMessage(),
            ]);
        }
    }


    /**
     * @Route( "/super/emailinone/treaction/apikey", name="superadmineMailInOne.get", methods={"GET"})
     * @param AuthenticationService $authenticationService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param IntegrationTypes $integrationTypes
     * @param AuthenticationUtils $authenticationUtils
     * @param StandardFieldMappingRepository $standardFieldMappingRepository
     * @return Response
     * @author Pradeep
     */
    public function getSuperAdminEMailInOneSyncUserForNewsletter(
        AuthenticationService          $authenticationService,
        ObjectRegisterRepository       $objectRegisterRepository,
        ObjectRegisterMetaRepository   $objectRegisterMetaRepository,
        IntegrationTypes               $integrationTypes,
        AuthenticationUtils            $authenticationUtils,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        LoggerInterface                $logger
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $objRegId = $objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        $endPoint = $_ENV['MSMIO'] ?? "";
        if ($objRegId !== null) {
            $objRegMetaDetails = $objectRegisterMetaRepository->getJsonMetaValueAsArray($objRegId,
                UniqueObjectTypes::EMAILINONE);
            $eMIOConfig = $objRegMetaDetails;
        } else {
            $eMIOConfig = $integrationTypes->getSuperAdminEMailInOneConfig();
        }
        $eMIOMapping = $standardFieldMappingRepository->getEMIOFieldsForMapping(UniqueObjectTypes::EMAILINONE);

        return $this->render('/account/superadmin/emio.twig',
            [
                'current_user' => $user,
                'emailinone' => $eMIOConfig,
                'end_point' => $endPoint,
                'sd_fields' => $eMIOMapping,
                'success' => '',
                'error' => '',
            ]);
    }

    /**
     * @Route( "/super/emailinone/treaction/apikey", name="superadmineMailInOne.post", methods={"POST"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param IntegrationTypes $integrationTypes
     * @param AuthenticationUtils $authenticationUtils
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function postSuperAdminEMailInOneSyncUserForNewsletter(
        Request                      $request,
        AuthenticationService        $authenticationService,
        ObjectRegisterRepository     $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        IntegrationTypes             $integrationTypes,
        AuthenticationUtils          $authenticationUtils,
        LoggerInterface              $logger
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $companyId = $user['company_id'];
        $token = $request->get('token');
        $endPoint = $_ENV['MSMIO'] ?? "";
        if (!$this->isCsrfTokenValid('superadmin-emio', $token)) {
            $this->redirectToRoute('/');
        }
        // get Empty Json Structure
        $eMIOConfig = $integrationTypes->getSuperAdminEMailInOneConfig();
        try {
            $apikey = $request->get('apikey');
            $objRegId = $objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
            $objRegId = is_null($objRegId) ? $objectRegisterRepository->createInitialObjectRegisterEntry($companyId,
                UniqueObjectTypes::SUPERADMIN) : $objRegId;
            if (is_null($objRegId) || $objRegId <= 0) {
                throw new RuntimeException('Failed to create Object Register Id for SuperAdmin');
            }
            $objRegMetaDetails = $objectRegisterMetaRepository->getObjectRegisterMetaDetails($objRegId,
                UniqueObjectTypes::EMAILINONE);
            $eMIOConfig['relation']['superadmin_objectregister_id'] = $objRegId;
            $eMIOConfig['relation']['superadmin_object_unique_id'] = $objectRegisterRepository->getUniqueObjectTypeIdByName(UniqueObjectTypes::SUPERADMIN);
            $eMIOConfig['settings']['apikey'] = $apikey;
            $eMIOConfigJson = json_encode($eMIOConfig, JSON_THROW_ON_ERROR);
            if (empty($objRegMetaDetails)) {
                //insert ObjectRegisterMeta
                if (is_null($objectRegisterMetaRepository->insert($objRegId, UniqueObjectTypes::EMAILINONE,
                    $eMIOConfigJson))) {
                    throw new RuntimeException('Failed to save eMIO APIKey to ObjectRegisterMeta');
                }
            } else {
                // Update ObjectRegisterMeta
                $objRegMetaId = $objRegMetaDetails['id'];
                if (!$objectRegisterMetaRepository->update($objRegMetaId, $objRegId, UniqueObjectTypes::EMAILINONE,
                    $eMIOConfigJson)) {
                    throw new RuntimeException('Failed to update eMIO APIKey to ObjectRegisterMeta');
                }
            }

            return $this->render('account/superadmin/emio.twig', [
                'current_user' => $user,
                'emailinone' => $eMIOConfig,
                'end_point' => $endPoint,
                'success' => 'Successfully saved eMIO APIKey',
                'error' => '',
            ]);
        } catch (JsonException|Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->render('account/superadmin/emio.twig', [
                'current_user' => $user,
                'emailinone' => [],
                'end_point' => $endPoint,
                'success' => '',
                'error' => 'Failed to save eMIO APIKey',
            ]);
        }
    }

    /**
     * @Route( "/super/emailinone/fields/mapping", name="superadminemiomapping", methods={"GET","POST"})
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param StandardFieldMappingRepository $standardFieldMappingRepository
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function superAdminEMIOStandardFields(
        AuthenticationUtils            $authenticationUtils,
        AuthenticationService          $authenticationService,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        AdministrationService          $administrationService,
        MIOStandardFieldRepository     $mioStandardFieldRepository,
        DatatypesRepository            $datatypesRepository,
        LoggerInterface                $logger,
        Request                        $request
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $companyId = $user['company_id'];
        $msgSuccess = '';
        $msgError = '';
        $method = $request->getMethod();

        try {
            if ($method === 'POST') { // POST METHOD
                $token = $request->get('token');
                if (!$this->isCsrfTokenValid('emio-standardfields-mapping', $token)) { // CSRF TOKEN VALIDATION
                    $this->redirectToRoute('/');
                }
                $mapping = base64_decode($request->get('mapping'));
                $logger->info('Mapping ' . json_encode($mapping));
                if (empty($mapping)) {
                    throw new RuntimeException('Failed to insert or Update Mapping');
                }
                $fields = json_decode($mapping, true, 512, JSON_THROW_ON_ERROR);
                $system['id'] = $standardFieldMappingRepository->getSystemId(UniqueObjectTypes::EMAILINONE);
                if ($system['id'] <= 0) {
                    throw new RuntimeException('Failed to get SystemId');
                }
                // DELETE STANDARDFIELD MAPPING
                foreach ($fields['delete'] as $fieldId) {
                    if ($fieldId <= 0 || $standardFieldMapping = $standardFieldMappingRepository->delete($system['id'],
                            $fieldId)) {
                        $logger->alert('Failed to delete Mapping Field');
                        continue;
                    }
                }
                // INSERT OR UPDATE STANDARDFIELD MAPPING
                foreach ($fields['insert'] as $field) { // LOOP FOR EVERY STANDARD FIELD
                    if (empty($field) || empty($field['miostandardfield_id']) || empty($field['name'])) {
                        $logger->alert('Invalid StandardField provided for Mapping ' . json_encode($field,
                                JSON_THROW_ON_ERROR));
                        continue;
                    }
                    $standardFieldMapping = $standardFieldMappingRepository->getId((int)$system['id'],
                        (int)$field['miostandardfield_id']);
                    $logger->info('standardFieldMapping ' .
                        json_encode([
                            'stdFIeld' => $standardFieldMapping,
                            'field' => $field,
                        ], JSON_THROW_ON_ERROR
                        ));
                    if (empty($standardFieldMapping)) {
                        $logger->info('field ' . json_encode($field));
                        // INSERT TO STANDARD FIELD MAPPING TABLE
                        $mioField = $standardFieldMappingRepository->getSingleMIOStandardField($field['miostandardfield_id']);
                        $logger->info('$mioField ' . json_encode($mioField));
                        if ($field['name'] === 'Auto-Create') {
                            $eMIOField = $administrationService->generateEMIOFieldForMIOField($mioField['fieldname'],
                                $mioField['datatype']);
                        } else {
                            $eMIOField['name'] = $field['name'];
                            $eMIOField['datatype'] = $field['datatype'];
                        }
                        $logger->info('$eMIOField ' . json_encode($eMIOField));

                        if (!$standardFieldMappingRepository->insert($system['id'], $field['miostandardfield_id'],
                            $eMIOField['name'], $eMIOField['datatype'])) {
                            throw new RuntimeException('Failed to insert or Update Mapping');
                        }
                    } else {
                        // UPDATE STANDARD FIELD MAPPING TABLE.
                        if (!$standardFieldMappingRepository->update($standardFieldMapping['id'], $system['id'],
                            $field['miostandardfield_id'], $field['name'], $field['datatype'])) {
                            throw new RuntimeException('Failed to insert or Update Mapping');
                        }
                    }
                }
                $msgSuccess = 'Successfully Updated Mapping for eMIO';
            }
            $eMIOMapping = $standardFieldMappingRepository->getEMIOFieldsForMapping(UniqueObjectTypes::EMAILINONE);

            $mioFields['standard_fields'] = $mioStandardFieldRepository->getStandardFields();
            $mioFields['technical_fields'] = $mioStandardFieldRepository->getTechnicalFields();
            $mioFields['ecommerce_fields'] = $mioStandardFieldRepository->getECommerceFields();
            $mioDtypes = base64_encode(json_encode($datatypesRepository->getDatatypes()));
            $mioFields = base64_encode(json_encode($mioFields));
            return $this->render('account/superadmin/emio_standardfields_mapping.twig', [
                'current_user' => $user,
                'sd_fields' => $eMIOMapping,
                'mio_fields' => $mioFields,
                'mio_dtypes' => $mioDtypes,
                'success' => $msgSuccess,
                'error' => $msgError,
            ]);
        } catch (JsonException|RuntimeException|Exception$e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->render('account/superadmin/emio_standardfields_mapping.twig', [
                'current_user' => $user,
                'sd_fields' => '',
                'fields' => '',
                'success' => '',
                'error' => $e->getMessage(),
            ]);
        }


    }


    /**
     * @Route( "/super/hubspot/treaction/apikey", name="superadminhubspot", methods={"GET","POST"})
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param IntegrationTypes $integrationTypes
     * @param AuthenticationUtils $authenticationUtils
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function superAdminHBAPIkey(
        Request                      $request,
        AuthenticationService        $authenticationService,
        ObjectRegisterRepository     $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        IntegrationTypes             $integrationTypes,
        AuthenticationUtils          $authenticationUtils,
        LoggerInterface              $logger
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $companyId = $user['company_id'];
        try {
            $objRegId = $objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
            $logger->info('ObjectRegister Id ' . $objRegId);
            if ($objRegId !== null) { //GET SAVED SETTING FORM DB
                $hbConfig = $objectRegisterMetaRepository->getJsonMetaValueAsArray($objRegId, UniqueObjectTypes::HUBSPOT);
            } else { // GET EMPTY JSON STRUCTURE
                $hbConfig = $integrationTypes->getSuperAdminHubSpotConfig();
            }
            $logger->info('HBConfig ' . json_encode($hbConfig, JSON_THROW_ON_ERROR));
            if ($request->getMethod() === 'POST') {
                $token = $request->get('token');
                if (!$this->isCsrfTokenValid('superadmin-hb', $token)) {
                    $this->redirectToRoute('/');
                }
                // VALIDATE AND CREATE AN OBJECT REGISTER FOR SUPER ADMIN
                $objRegId = is_null($objRegId) ? $objectRegisterRepository->createInitialObjectRegisterEntry($companyId,
                    UniqueObjectTypes::SUPERADMIN) : $objRegId;
                if (is_null($objRegId) || $objRegId <= 0) {
                    throw new RuntimeException('Failed to create Object Register Id for SuperAdmin');
                }
                $objRegMetaDetails = $objectRegisterMetaRepository->getObjectRegisterMetaDetails($objRegId,
                    UniqueObjectTypes::HUBSPOT);
                $logger->info('ObjRegMetaDetails ' . json_encode($objRegMetaDetails, JSON_THROW_ON_ERROR));
                // UPDATE JSON STRUCTURE FOR HUBSPOT
                $hbConfig['relation']['superadmin_objectregister_id'] = $objRegId;
                $hbConfig['relation']['superadmin_object_unique_id'] =
                    $objectRegisterRepository->getUniqueObjectTypeIdByName(UniqueObjectTypes::SUPERADMIN);
                $hbConfig['settings']['apikey'] = $request->get('apikey');;
                $hbConfigJson = json_encode($hbConfig, JSON_THROW_ON_ERROR);
                $logger->info('Hubspot Configuration ' . $hbConfigJson);
                if (empty($objRegMetaDetails)) { // INSERT APIKEY TO DB
                    if (is_null($objectRegisterMetaRepository->insert($objRegId, UniqueObjectTypes::HUBSPOT,
                        $hbConfigJson))) {
                        throw new RuntimeException('Failed to save Hubspot APIKey to ObjectRegisterMeta');
                    }
                } else { // UPDATE APIKEY TO DB
                    $objRegMetaId = $objRegMetaDetails['id'];
                    if (!$objectRegisterMetaRepository->update($objRegMetaId, $objRegId, UniqueObjectTypes::HUBSPOT,
                        $hbConfigJson)) {
                        throw new RuntimeException('Failed to update Hubspot APIKey to ObjectRegisterMeta');
                    }
                }
            }
            return $this->render('account/superadmin/hb.twig', [
                'current_user' => $user,
                'hubspot' => $hbConfig,
                'success' => 'Successfully saved Hubspot APIKey',
                'error' => '',
                'endpoint' => $_ENV['MSHB_URL'],
            ]);
        } catch (JsonException|Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->render('account/superadmin/hb.twig', [
                'current_user' => $user,
                'hubspot' => [],
                'success' => '',
                'error' => $e->getMessage(),
                'endpoint' => $_ENV['MSHB_URL'],
            ]);
        }
    }


    /**
     * @Route( "/super/hubspot/fields/mapping", name="superadminehbmapping", methods={"GET","POST"})
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param StandardFieldMappingRepository $standardFieldMappingRepository
     * @param LoggerInterface $logger
     * @param Request $request
     * @return Response
     * @author Pradeep
     */
    public function superAdminHBStandardFields(
        AuthenticationUtils            $authenticationUtils,
        AuthenticationService          $authenticationService,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        ObjectRegisterRepository       $objectRegisterRepository,
        ObjectRegisterMetaRepository   $objectRegisterMetaRepository,
        LoggerInterface                $logger,
        Request                        $request
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $companyId = $user['company_id'];
        $msgSuccess = '';
        $msgError = '';
        $method = $request->getMethod();
        $objRegId = $objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        $hbConfig = $objectRegisterMetaRepository->getJsonMetaValueAsArray($objRegId, UniqueObjectTypes::HUBSPOT);
        try {
            if (empty($hbConfig)) {
                throw new RuntimeException('treaction Hubspot APIKey is Missing');
            }
            $apikey = $hbConfig['settings']['apikey'];

            if ($method === 'POST') { // POST METHOD
                $token = $request->get('token');
                if (!$this->isCsrfTokenValid('hb-standardfields-mapping', $token)) { // CSRF TOKEN VALIDATION
                    $this->redirectToRoute('/');
                }
                $mapping = base64_decode($request->get('mapping'));

                $logger->info('HB Mapping ' . json_encode($mapping, JSON_THROW_ON_ERROR));
                if (empty($mapping)) {
                    throw new RuntimeException('Failed to insert or Update Mapping');
                }
                $fields = json_decode($mapping, true, 512, JSON_THROW_ON_ERROR);
                $system['id'] = $standardFieldMappingRepository->getSystemId(UniqueObjectTypes::HUBSPOT);
                if ($system['id'] <= 0) {
                    throw new RuntimeException('Failed to get SystemId');
                }
                // DELETE STANDARDFIELD MAPPING
                foreach ($fields['delete'] as $fieldId) {
                    if ($fieldId <= 0 || $standardFieldMapping = $standardFieldMappingRepository->delete($system['id'],
                            $fieldId)) {
                        $logger->alert('Failed to delete Mapping Field');
                        continue;
                    }
                }
                // INSERT OR UPDATE STANDARDFIELD MAPPING
                foreach ($fields['insert'] as $field) { // LOOP FOR EVERY STANDARD FIELD
                    if (empty($field) || empty($field['miostandardfield_id']) || empty($field['name'])) {
                        $logger->alert('Invalid StandardField provided for Mapping ' . json_encode($field,
                                JSON_THROW_ON_ERROR));
                        continue;
                    }
                    $standardFieldMapping = $standardFieldMappingRepository->getId((int)$system['id'],
                        (int)$field['miostandardfield_id']);
                    $logger->info('standardFieldMapping ' . json_encode($standardFieldMapping, JSON_THROW_ON_ERROR));
                    if (empty($standardFieldMapping) && empty($standardFieldMapping['id'])) {
                        // INSERT TO STANDARDFIELD MAPPING TABLE
                        if (!$standardFieldMappingRepository->insert($system['id'], $field['miostandardfield_id'],
                            $field['name'], $field['datatype'])) {
                            throw new RuntimeException('Failed to insert or Update Mapping');
                        }
                    } else {
                        // UPDATE STANDARDFIELD MAPPING TABLE.
                        if (!$standardFieldMappingRepository->update($standardFieldMapping['id'], $system['id'],
                            $field['miostandardfield_id'], $field['name'], $field['datatype'])) {
                            throw new RuntimeException('Failed to insert or Update Mapping');
                        }
                    }
                }
                $msgSuccess = 'Successfully Updated Mapping for eMIO';
            }
            $hbMapping = $standardFieldMappingRepository->getEMIOFieldsForMapping(UniqueObjectTypes::HUBSPOT);

            return $this->render('account/superadmin/hb_standardfields_mapping.twig', [
                'current_user' => $user,
                'sd_fields' => $hbMapping,
                'hb_apikey' => $apikey,
                'success' => $msgSuccess,
                'error' => $msgError,
                'endpoint' => $_ENV['MSHB_URL'],
            ]);
        } catch (JsonException|RuntimeException|Exception$e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->render('account/superadmin/hb_standardfields_mapping.twig', [
                'current_user' => $user,
                'sd_fields' => '',
                'hb_apikey' => '',
                'success' => '',
                'error' => $e->getMessage(),
                'endpoint' => $_ENV['MSHB_URL'],
            ]);
        }

    }

    /**
     * @Route( "/super/inboxmonitor", name="superadminInboxMonitor.manage", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @author Aki
     */
    public function superAdminInboxMonitor(
        Request $request
    ): Response
    {
        //Get the user and object register id of super admin
        $user = $this->user;
        $superAdminObjectRegisterId = $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin();

        //init the required variables
        $config = [];
        $error = "";
        // Get Configuration from Database
        if ($superAdminObjectRegisterId !== null) {
            // Get the settings
            $inboxMonitorConfig = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails($superAdminObjectRegisterId,
                UniqueObjectTypes::INBOX_MONITOR_CONFIG);
        }

        if ($request->getMethod() === 'POST') { // POST METHOD

            //if null create objectregister entry for superadmin
            $superAdminObjectRegisterId = is_null($superAdminObjectRegisterId) ?
                $this->objectRegisterRepository->createInitialObjectRegisterEntry
                ($user['company_id'], UniqueObjectTypes::SUPERADMIN)
                : $superAdminObjectRegisterId;
            if (is_null($superAdminObjectRegisterId) || $superAdminObjectRegisterId <= 0) {
                $error = 'Failed to create Object Register Id for SuperAdmin';
            }

            // Get the type for the admin inbox monitor config
            $inboxMonitorConfigTemplate = $this->integrationTypes->getSuperAdminInboxMonitorConfig();

            // fill the meat with the data from the UI
            $inboxMonitorConfigTemplate['relation']['superadmin_objectregister_id'] = $superAdminObjectRegisterId;
            $inboxMonitorConfigTemplate['relation']['integration_object_unique_type_id'] =
                $this->objectRegisterRepository->getUniqueObjectTypeIdByName(UniqueObjectTypes::INBOX_MONITOR_CONFIG);
            $inboxMonitorConfigTemplate['settings']['user'] = $request->get('inbox-monitor-user');
            $inboxMonitorConfigTemplate['settings']['apikey'] = $request->get('inbox-monitor-apikey');
            $config = $inboxMonitorConfigTemplate['settings'];

            // Encode to json
            $inboxMonitorConfigTemplateJson = json_encode($inboxMonitorConfigTemplate, JSON_THROW_ON_ERROR);

            //Insert into DB
            if (empty($inboxMonitorConfig) || !isset($inboxMonitorConfig['id']) || (int)$inboxMonitorConfig['id'] <= 0) {
                //insert ObjectRegisterMeta
                if (is_null($this->objectRegisterMetaRepository->insert($superAdminObjectRegisterId,
                    UniqueObjectTypes::INBOX_MONITOR_CONFIG, $inboxMonitorConfigTemplateJson))) {
                    $error = 'Failed to insert data';
                }
            } else {
                // Update ObjectRegisterMeta
                $objRegMetaId = $inboxMonitorConfig['id'];
                if (!$this->objectRegisterMetaRepository->update($objRegMetaId, $superAdminObjectRegisterId,
                    UniqueObjectTypes::INBOX_MONITOR_CONFIG, $inboxMonitorConfigTemplateJson)) {
                    $error = 'Failed to update data';
                }
            }

        }
        //if not empty
        if(!empty($inboxMonitorConfig) && empty($config)){
            $config = json_decode($inboxMonitorConfig['meta_value'],
                true, 512, JSON_THROW_ON_ERROR)['settings'];
        }

        //return the rendered twig
        return $this->render('/account/superadmin/inbox_monitor.twig',
            ['current_user' => $user, 'config' => $config, 'success' => '', 'error' => $error]);
    }

    /**
     * @Route( "/super/ftp/test", name="superadmin_default_ftp_server_test", methods={"GET","POST"})
     * @param Request $request
     * @param ServerConfigurationRepository $serverConfigurationRepository
     * @param CompanyRepository $companyRepository
     * @param SeedPoolRepository $seedPoolRepository
     * @return Response
     * @throws JsonException
     * @author Aki
     * @deprecated
     */
    public function superAdminDefaultFTPTest(
        Request $request,
        ServerConfigurationRepository $serverConfigurationRepository,
        CompanyRepository $companyRepository,
        SeedPoolRepository $seedPoolRepository
    ): Response
    {

        //Super admin is an object with all the settings as its properties
        $superAdminObjectRegisterId = $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        //base details for parameters
        $currentUser = $this->getCurrentUser();

        //init the required variables
        $serverConfiguration = [];
        $ftpConfiguration = [];

        // Get Configuration from Database
        if ($superAdminObjectRegisterId !== null) {
            // Get the settings
            $config = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails($superAdminObjectRegisterId,
                UniqueObjectTypes::SUPER_ADMIN_DEFAULT_FTP);
        }

        if ($request->getMethod() === 'POST') { // POST METHOD

            //if null create objectregister entry for superadmin
            $superAdminObjectRegisterId = is_null($superAdminObjectRegisterId) ?
                $this->objectRegisterRepository->createInitialObjectRegisterEntry
                ($currentUser['company_id'], UniqueObjectTypes::SUPERADMIN)
                : $superAdminObjectRegisterId;
            if (is_null($superAdminObjectRegisterId) || $superAdminObjectRegisterId <= 0) {
                $error = 'Failed to create Object Register Id for SuperAdmin';
            }

            // Get the type for the admin inbox monitor config
            $inboxMonitorConfigTemplate = $this->integrationTypes->getSuperAdminInboxMonitorConfig();

            // fill the meat with the data from the UI
            $inboxMonitorConfigTemplate['relation']['superadmin_objectregister_id'] = $superAdminObjectRegisterId;
            $inboxMonitorConfigTemplate['relation']['integration_object_unique_type_id'] =
                $this->objectRegisterRepository->getUniqueObjectTypeIdByName(UniqueObjectTypes::INBOX_MONITOR_CONFIG);
            $inboxMonitorConfigTemplate['settings']['user'] = $request->get('inbox-monitor-user');
            $inboxMonitorConfigTemplate['settings']['apikey'] = $request->get('inbox-monitor-apikey');
            $config = $inboxMonitorConfigTemplate['settings'];

            // Encode to json
            $inboxMonitorConfigTemplateJson = json_encode($inboxMonitorConfigTemplate, JSON_THROW_ON_ERROR);

            //Insert into DB
            if (empty($inboxMonitorConfig) || !isset($inboxMonitorConfig['id']) || (int)$inboxMonitorConfig['id'] <= 0) {
                //insert ObjectRegisterMeta
                if (is_null($this->objectRegisterMetaRepository->insert($superAdminObjectRegisterId,
                    UniqueObjectTypes::INBOX_MONITOR_CONFIG, $inboxMonitorConfigTemplateJson))) {
                    $error = 'Failed to insert data';
                }
            } else {
                // Update ObjectRegisterMeta
                $objRegMetaId = $inboxMonitorConfig['id'];
                if (!$this->objectRegisterMetaRepository->update($objRegMetaId, $superAdminObjectRegisterId,
                    UniqueObjectTypes::INBOX_MONITOR_CONFIG, $inboxMonitorConfigTemplateJson)) {
                    $error = 'Failed to update data';
                }
            }
        }

        //if not empty
        if(!empty($config['serverConfiguration'])){
            $serverConfiguration = $config['serverConfiguration'];
            $ftpConfiguration = $config['ftpConfiguration'];
        }

        //return the rendered twig
        return $this->render('/account/superadmin/ftp_server.twig',
            [   'current_user' => $currentUser,
                'serverConfiguration' => $serverConfiguration,
                'ftpConfiguration' => $ftpConfiguration,
                'success' => '',
                'error' => ''
            ]);
    }

    /**
     * @Route( "/super/ftp", name="superadmin_default_ftp_server", methods={"GET","POST"})
     * @param Request $request
     * @param ServerConfigurationRepository $serverConfigurationRepository
     * @param CompanyRepository $companyRepository
     * @param SeedPoolRepository $seedPoolRepository
     * @return Response
     * @throws JsonException
     * @author Aki
     */
    public function superAdminDefaultFTP(
        Request $request,
        ServerConfigurationRepository $serverConfigurationRepository,
        CompanyRepository $companyRepository,
        SeedPoolRepository $seedPoolRepository
    ): Response
    {

        //Super admin is an object with all the settings as its properties
        $superAdminObjectRegisterId = $this->objectRegisterRepository->getObjectRegisterIdForSuperAdmin();
        //base details for parameters
        $currentUser = $this->getCurrentUser();

        //init the required variables
        $config = [];
        $success = '';
        $error = '';

        // Get Configuration from Database
        if ($superAdminObjectRegisterId !== null) {
            // Get the settings
            $SFTPConfig = $this->objectRegisterMetaRepository->getObjectRegisterMetaDetails($superAdminObjectRegisterId,
                UniqueObjectTypes::SUPER_ADMIN_DEFAULT_FTP);
        }

        if ($request->getMethod() === 'POST') { // POST METHOD

            //if null create objectregister entry for superadmin
            $superAdminObjectRegisterId = is_null($superAdminObjectRegisterId) ?
                $this->objectRegisterRepository->createInitialObjectRegisterEntry
                ($currentUser['company_id'], UniqueObjectTypes::SUPERADMIN)
                : $superAdminObjectRegisterId;
            if (is_null($superAdminObjectRegisterId) || $superAdminObjectRegisterId <= 0) {
                $error = 'Failed to create Object Register Id for SuperAdmin';
            }

            // Get the type for the admin inbox monitor config
            $SFTPConfigTemplate = $this->integrationTypes->getSuperSFTPConfig();

            // fill the meat with the data from the UI
            $SFTPConfigTemplate['relation']['superadmin_objectregister_id'] = $superAdminObjectRegisterId;
            $SFTPConfigTemplate['relation']['integration_object_unique_type_id'] =
                $this->objectRegisterRepository->getUniqueObjectTypeIdByName(UniqueObjectTypes::SUPER_ADMIN_DEFAULT_FTP);
            $SFTPConfigTemplate['settings']['ip_address'] = $request->get('ip_address');
            $SFTPConfigTemplate['settings']['sftp_user'] = $request->get('sftp_user');
            $SFTPConfigTemplate['settings']['sftp_password'] = $request->get('sftp_password');
            $config = $SFTPConfigTemplate['settings'];

            // Encode to json
            $SFTPConfigTemplateJson = json_encode($SFTPConfigTemplate, JSON_THROW_ON_ERROR);

            //Insert into DB
            if (empty($SFTPConfig) || !isset($SFTPConfig['id']) || (int)$SFTPConfig['id'] <= 0) {
                //insert ObjectRegisterMeta
                if (is_null($this->objectRegisterMetaRepository->insert($superAdminObjectRegisterId,
                    UniqueObjectTypes::SUPER_ADMIN_DEFAULT_FTP, $SFTPConfigTemplateJson))) {
                    $error = 'Failed to insert data';
                }
                $success = 'Super Admin SFTP configuration saved successfully ';
            } else {
                // Update ObjectRegisterMeta
                $objRegMetaId = $SFTPConfig['id'];
                if (!$this->objectRegisterMetaRepository->update($objRegMetaId, $superAdminObjectRegisterId,
                    UniqueObjectTypes::SUPER_ADMIN_DEFAULT_FTP, $SFTPConfigTemplateJson)) {
                    $error = 'Failed to update data';
                }
                $success = 'Super Admin SFTP configuration updated successfully ';
            }
        }

        //if not empty
        if(!empty($SFTPConfig) && empty($config)){
            $config = json_decode($SFTPConfig['meta_value'],
                true, 512, JSON_THROW_ON_ERROR)['settings'];
        }

        //return the rendered twig
        return $this->render('/account/superadmin/default_ftp_server.twig',
            [   'current_user' => $currentUser,
                'config' => $config,
                'success' => $success,
                'error' => $error
            ]);
    }




    /**
     * @Route( "/super/server/overview/{company_id}/{switch_to_status}", name="superadminServer.overview", methods={"GET"})
     * @param Request                       $request
     * @param ServerConfigurationRepository $serverConfigurationRepository
     * @return Response
     * @author JSR
     */
    public function superadminServerOverview(Request $request, ServerConfigurationRepository $serverConfigurationRepository, CompanyRepository $companyRepository):Response
    {
        $company_id = $request->get('company_id');
        $switchToStatus = $request->get('switch_to_status');
        $company = $companyRepository->getCompanyById($company_id);


        $serverConfiguration = $serverConfigurationRepository->getServerConfiguration($company_id);
        $serverFtpConfiguration = $serverConfigurationRepository->getServerSftpConfigurationByCompanyId($company_id);
        // NOT USED >>>>>
        // $serverEmailConfiguration = $serverConfigurationRepository->getServerEmailConfigurationByCompanyId($company_id);
        // <<<<< NOT USED
        $currentUser = $this->getCurrentUser();

        return $this->render(
            '/account/superadmin/server_overview.html.twig',
            [
                'company_id' => $company_id,
                'company_name' => $company->getName(),
                'current_user' => $currentUser,
                'serverConfiguration' =>$serverConfiguration,
                'ftpConfiguration' => $serverFtpConfiguration,
                'switchToStatus' => $switchToStatus
                // 'emailConfiguration' => $serverEmailConfiguration, // NOT USED
            ]);
    }


    /**
     * <b style="color:red">CAUTION:</b><br>for action = <b>edit</b> id is <b>server_configuration.id</b>,<br>for action = <b>new</b>, id is <b>company.id</b>
     * @Route( "/super/server/wizard/{action}/{id}", name="superadminServer.wizard", methods={"GET"})
     * @param Request                       $request
     * @param ServerConfigurationRepository $serverConfigurationRepository
     * @param SeedPoolRepository            $seedPoolRepository
     * @return Response
     * @author JSR
     */
    public function superadminServerWizard(Request $request, ServerConfigurationRepository $serverConfigurationRepository, SeedPoolRepository $seedPoolRepository, CompanyRepository $companyRepository):Response
    {
        $currentUser = $this->getCurrentUser();
        $action = $request->get('action');
        $serverStatusList = $serverConfigurationRepository->getStatusListForServerConfiguration();
        $serverHosting = $serverConfigurationRepository->getServerHosting();

        if($action === 'edit') { // action = {'edit' | 'new'}
            $server_id = $request->get('id');
            // there must be exactly one serverConfiguration-record. Otherwise, action=edit could not have been requested
            $serverConfiguration = $serverConfigurationRepository->getServerConfigurationByServerId($server_id);
            $seedpoolGroups = $seedPoolRepository->getServerWizardSeedPoolGroupsByCompanyId($serverConfiguration[0]['company_id']);
            $company = $companyRepository->getCompanyById($serverConfiguration[0]['company_id']);
            $serverFtpConfiguration = $serverConfigurationRepository->getServerSftpConfiguration($server_id);
            $parameters = [
                'mode' => 'edit',
                'serverHosting' => $serverHosting,
                'serverStatusList' => $serverStatusList,
                'company_id' => $company->getId(),
                'company_name' => $company->getName(),
                'current_user' => $currentUser,
                'seedpoolGroups' => $seedpoolGroups,
                'serverConfiguration' =>$serverConfiguration[0], // provide first and only record
                'ftpConfiguration' => $serverFtpConfiguration, // provide 0 ... n records
            ];
        } elseif($action=== 'new') {
            $company_id = $request->get('id');
            $company = $companyRepository->getCompanyById($company_id);
            $seedpoolGroups = $seedPoolRepository->getServerWizardSeedPoolGroupsByCompanyId($company_id);

            $parameters = [
                'mode' => 'new',
                'serverHosting' => $serverHosting,
                'serverStatusList' => $serverStatusList,
                'company_id' => $company_id,
                'company_name' => $company->getName(),
                'current_user' => $currentUser,
                'seedpoolGroups' => $seedpoolGroups,
                'serverConfiguration' => [],
                'ftpConfiguration' => [],
            ];
        } else {
            $this->logger->error("not existing route $action called in method", [__METHOD__, __LINE__]);
        }

        return $this->render(
            $view = '/account/superadmin/server_wizard.html.twig',
            $parameters
        );
    }


    /**
     * create or update a Server record modified or created by using ServerWizard
     * @Route( "/super/server/ajax/save", name="superadminServer.save", methods={"POST"})
     * @param Request $request
     * @param ServerConfigurationRepository $serverConfigurationRepository
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param StatusdefTypes $statusdefTypes
     * @return JsonResponse
     * @author JSR
     */
    public function ajaxSuperadminServerSave(Request $request, ServerConfigurationRepository $serverConfigurationRepository, ObjectRegisterRepository $objectRegisterRepository, StatusdefTypes $statusdefTypes):JsonResponse
    {
        $updated = false;
        $ftpUpSert = false;

        $serverConfigurationRepository->setCurrentUserId($this->getCurrentUser()['user_id']);

        // TODO: add token to twig and token check here, do field content validation
        $content = $request->getContent();
        $json = base64_decode($content);
        $server = json_decode($json, true, 10, JSON_OBJECT_AS_ARRAY);

        $this->logger->info('SAVE SERVER', [$server, __METHOD__, __LINE__]);

        // TODO: move this stuff to a service
        $serverHostingList = $serverConfigurationRepository->getServerHosting();

        // check if user entered a new Hosting Provider Name in SELECT2-select element of Server Wizard
        if(!ctype_digit($server['server_hosting_id'])) {
            // create new entry in server_hosting

            $serverHostingId = $serverConfigurationRepository->getServerHostingId($server['server_hosting_id']);

            if($serverHostingId === 0) { // if host_name does not exist
                $serverHostingId = $serverConfigurationRepository->createServerHosting($server['server_hosting_id']);
            }

            if(empty($serverHostingId)) { // $serverHostingId can be null or false
                $this->logger->error('could not create new entry in server_hosting', [__METHOD__, __LINE__]);
                // TODO: exit with error message
                return $this->json(['status' => false, 'msg' => 'could not create entry in server_hosting']);
            }
        } else {
            $serverHostingId = (int) $server['server_hosting_id'];
        }

        $this->logger->info('SERVER mode', [$server['mode'], __METHOD__, __LINE__]);

        if($server['mode'] === 'new') {
            $objectRegisterId = $objectRegisterRepository->createObjectRegisterEntryServer($server['internal_name'], $server['ip_address'], $server['default_domain']);
            $objectRegisterRepository->updateStatus($objectRegisterId, $statusdefTypes::DRAFT);
            $serverConfigurationId = $serverConfigurationRepository->insertIntoServerConfiguration(
                (int) $objectRegisterId,
                (int) $server['company_id'],
                (int) $server['seedpoolgroup_id'],
                (int) $serverHostingId,
                $server['ip_address'],
                $server['internal_name'],
                $server['description'],
                $server['default_domain'],
                $server['segment'],
                (int) $server['cloudflare'],
                null,
                $server['plesk_url'],
                $server['plesk_user'],
                $server['plesk_pw']
            );

        } elseif($server['mode'] === 'edit') {

            $serverConfigurationId = $server['id'];
            $updated = $serverConfigurationRepository->updateServerConfiguration(
                $server['id'],
                $server['seedpoolgroup_id'],
                $server['server_hosting_id'],
                $server['ip_address'],
                $server['internal_name'],
                $server['description'],
                $server['default_domain'],
                $server['segment'],
                $server['cloudflare'],
                null,
                $server['plesk_url'],
                $server['plesk_user'],
                $server['plesk_pw']
            );
        }

        if(!empty($server['ftp_records'])) {
            $ftpUpSert = $serverConfigurationRepository->insertOrUpdateSftpRecords($serverConfigurationId, $server['ftp_records']);
        }

        return $this->json(['status' =>true, 'msg' => 'configuration saved', '$data' => $server, 'updated' => $updated, 'ftpUpSert'=>$ftpUpSert]);
    }


    /**
     * delete a server by objectregister_id cascading to server_configuration
     * @Route( "/super/server/delete/{company_id}/{objectregister_id}", name="superadminServer.delete", methods={"GET"})
     * @param Request                       $request
     * @param ObjectRegisterRepository      $objectRegisterRepository
     * @return Response
     * @author JSR
     */
    public function superadminServerDelete(Request $request, ObjectRegisterRepository $objectRegisterRepository):Response
    {
        $company_id = $request->get('company_id');
        $objectregister_id = $request->get('objectregister_id');
        $objectRegisterRepository->delete($objectregister_id);

        return $this->redirect("/admintool/super/server/overview/$company_id/active");
    }


    /**
     * change Server Status
     * @Route( "/super/server/status/{status}/{company_id}/{objectregister_id}", name="superadminServer.status", methods={"GET"})
     * @param Request                       $request
     * @param ObjectRegisterRepository      $objectRegisterRepository
     * @return Response
     * @author JSR
     */
    public function superadminServerSetStatus(Request $request, ObjectRegisterRepository $objectRegisterRepository, StatusdefTypes $statusdefTypes):Response
    {
        $company_id = $request->get('company_id');
        $objectregister_id = $request->get('objectregister_id');
        $status = $request->get('status');
        $objectRegisterRepository->updateStatus((int) $objectregister_id, $status);
        return $this->redirect("/admintool/super/server/overview/$company_id/$status");
    }


    /**
     * * @Route( "/super/server/decode", name="superadminServer.decode", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function serverDecodePasswordOnDemand(Request $request, CryptoService $cryptoService):JsonResponse
    {
        $content = $request->getContent();
        $this->logger->info('$content', [$content,__METHOD__, __LINE__]);
        $password = $cryptoService->deCryptB64($content);
        $this->logger->info('$password', [$password,__METHOD__, __LINE__]);
        $status = ($content !== $password) ? true : false;
        return $this->json(['status' => $status, 'string' => base64_encode($password)]);
    }
}
