<?php

namespace App\Controller;

use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ContactRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\ObjectRegisterRepository;
use App\Services\AioServices;
use App\Services\AuthenticationService;
use App\Services\CampaignService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class CampaignIntegrationController
 * @package App\Controller
 * @Route( "/account")
 * @deprecated
 */
class CampaignIntegrationController extends AbstractController
{


    protected $campaignOrganizer; // NOWHERE USED EXCEPT IN CONSTRUCTOR
    protected $webhookOrganizer; // NOWHERE USED EXCEPT IN CONSTRUCTOR


    public function __construct()
    {
        $this->campaignOrganizer = $_ENV['CAMPAIGN_SERVICE'];
        $this->webhookOrganizer = $_ENV['WEBHOOK_SERVICE'];
    }

    /**
     * Route No. 56, Refactorname campaignintegration.fetchSplitTestFromTwig
     * @Route( "/campaign/{camp_id}/splittest/{view_type}", name="campInt.storesplittest", methods={"GET", "POST"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param AuthenticationService $authenticationService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignService $campaignService
     * @return Response
     * @throws \JsonException
     * @author Pradeep
     * @internal in case of post, split test configuration from frontend is saved
     */
    public function fetchSplitTestFromTwig(
        Request $request,
        CampaignRepository $campaignRepository,
        AuthenticationService $authenticationService,
        ObjectRegisterRepository $objectRegisterRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        AuthenticationUtils $authenticationUtils,
        CampaignService $campaignService
    ): Response
    {
        $camp_id = $request->get('camp_id');
        $view_type = $request->get('view_type');
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());

        if (empty($user)) {
            return $this->redirect('/');
        }
        $company = $user['company_name'];
        $campaign_details = $campaignRepository->get($camp_id);
        $pageSettings = $objectRegisterMetaRepository->getJsonMetaValueAsArray($campaign_details['objectregister_id'],
            ObjectRegisterMetaRepository::MetaKeyPage);
        $objectRegister = $objectRegisterRepository->getObjectregisterById($campaign_details['objectregister_id']);
        $version = $objectRegister->getVersion();
        if ($request->isMethod('post')) {
            $camp_params['status'] = $request->get('status');
            $camp_params['name'] = $request->get('name');
            $camp_params['total_variants'] = $request->get('total_variants');
            $camp_params['cookie_expiration'] = $request->get('cookie_expiration');
            $camp_params['sticky_config'] = $request->get('sticky_config');
            $camp_params['expiration_criteria'] = $request->get('expiration_criteria');
            $camp_params['expiration_value_visitors'] = $request->get('expiration_value_visitors');
            $camp_params['expiration_value_conversions'] = $request->get('expiration_value_conversions');
            $camp_params['expiration_value_date'] = $request->get('expiration_value_date');
            $camp_params['variants'] = $request->get('variants');
            $pageSettings['FlowControl']['Splittest'] = $camp_params;
            if ($objectRegisterMetaRepository->updateProjectSettings($campaign_details['objectregister_id'],json_encode($pageSettings))) {
                $campaignService->createSplittestFolder($this->campaignOrganizer,
                    $campaign_details['name'],
                    $company,
                    $version,
                    $camp_params['name']);
            }
        }
        $response['c_set'] = $pageSettings['FlowControl']['Splittest'];
        $response['c_set']['campaigns'] = $campaign_details;
        $response['c_set']['camp_id'] = $camp_id;
        $response['c_set']['view_type'] = $view_type;
        $response['current_user'] = $user;
        return $this->render('campaign_settings/splittest.twig', $response);
    }

    /**
     * @Route( "/pages/{camp_id}/pageflow/{view_type}", name="campInt.campaignIntegrationDataFlow", methods={"GET", "POST"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param AuthenticationService $authenticationService
     * @param CampaignService $campaignService
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @throws \JsonException
     * @author Pradeep || aki
     */
    public function campaignIntegrationDataFlow(
        Request $request,
        CampaignRepository $campaignRepository,
        AuthenticationService $authenticationService,
        CampaignService $campaignService,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        AuthenticationUtils $authenticationUtils
    ): Response
    {
        $campaign_id = $request->get('camp_id');
        $view_type = $request->get('view_type');
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            return $this->redirect('/');
        }
        $campaign_details = $campaignRepository->get($campaign_id);
        $pageSettings = $objectRegisterMetaRepository->getJsonMetaValueAsArray($campaign_details['objectregister_id'],
            ObjectRegisterMetaRepository::MetaKeyPage);
        $objectRegister = $objectRegisterRepository->getObjectregisterById($campaign_details['objectregister_id']);
        $version = $objectRegister->getVersion();
        if ($request->isMethod('post')) {
            // CSRF Token Validation
            if (!$this->isCsrfTokenValid('campaign-settings', $request->get('token'))) {
                return $this->redirect('/login/signin.twig');
            }
            $camp_params['success_page'] = $request->get('success_page');
            $camp_params['error_page'] = $request->get('error_page');
            $camp_params['redirect_page'] = $request->get('redirect_page');
            $camp_params['pages'] = $this->validatePageName($request->get('pages'));
            $pageSettings['FlowControl']['PageFlow'] = $camp_params;
            if ($objectRegisterMetaRepository->updateProjectSettings($campaign_details['objectregister_id'],json_encode($pageSettings))) {
                $campaignService->createFlowControlPagesInServer($this->campaignOrganizer,
                    $campaign_details['name'],
                    $user['company_name'],
                    $version,
                    $camp_params['pages']);
            }
        }
        $response['c_set'] = $pageSettings['FlowControl']['PageFlow'];
        $response['c_set']['campaign'] = $campaign_details;
        // Add survey page is survey is configured.
        if (isset($response['c_set']['campaign']['survey_id']) &&
            (int)$response['c_set']['campaign']['survey_id'] > 0 &&
            !in_array('survey.html', array_column($response['c_set']['pages']['custom'], 'name'), true)) {
            $response['c_set']['pages']['custom'][]['name'] = 'survey.html';
        }
        $response['c_set']['camp_id'] = $campaign_id;
        $response['c_set']['view_type'] = $view_type;
        $response['current_user'] = $user;
        return $this->render('campaign_settings/pageflow.twig', $response);
    }

    /**
     * Route No. 55, Refactorname campaignintegration.fetchCampaignSettingsForTwig
     * @Route( "/{useCase_type}/{camp_id}/settings/{cnf_tgt}/{view_type}", name="campInt.generateview", methods={"GET"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param CompanyRepository $companyRepository
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param LoggerInterface $logger
     * @return Response
     * @author pva || aki
     */
    public function getCampaignConfiguration(
        Request $request,
        CampaignService $campaignService,
        CompanyRepository $companyRepository,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        LoggerInterface $logger
    ): Response
    {
        $camp_id = $request->get('camp_id');
        $useCase_type = $request->get('useCase_type');
        $cnf_tgt = $request->get('cnf_tgt');
        $cnf_group = 'settings';
        $view_type = $request->get('view_type');
        if($useCase_type ==='webhook' or $useCase_type ==='webhooks'){
            $metaKey = ObjectRegisterMetaRepository::MetaKeyWebHook;
        }else{
            $metaKey = ObjectRegisterMetaRepository::MetaKeyPage;
        }
        $response = $campaignService->fetchCampaignSettings($camp_id,$cnf_group, $cnf_tgt,$metaKey);
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        //additional variables required for twig
        $response[ 'c_set' ][ 'menu_integration_status' ] = $companyRepository->getCompanyIntegrationMetaData($user[ 'company_id' ]);
        $response['c_set']['camp_id'] = $camp_id;
        $response['c_set']['view_type'] = $view_type;
        // Dynamically generate twig view.
        if($useCase_type ==='webhook' or $useCase_type ==='webhooks'){
            $twig_view = 'webhook_settings/' . $cnf_tgt . '.twig';
        }else{
            $twig_view = 'campaign_settings/' . $cnf_tgt . '.twig';
        }
        $response['current_user'] = $user;

        return $this->render($twig_view, $response);
    }

    /**
     * Route No. 55, Refactorname campaignintegration.storeCampaignSettingsFromTwig
     * @Route( "/{useCase_type}/{camp_id}/settings/{cnf_tgt}/{view_type}", name="campInt.storesettigns", methods={"POST"})
     * @param Request $request
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param LoggerInterface $logger
     * @return Response
     * @author pva
     */
    public function setCampaignConfiguration(
        Request $request,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        CampaignRepository $campaignRepository,
        CampaignService $campaignService,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService,
        LoggerInterface $logger
    ): Response
    {
        $token = $request->get('token');

        // CSRF Token Validation
        if (!$this->isCsrfTokenValid('campaign-settings', $token)) {
            return $this->redirect('/login/signin.twig');
        }
        // Get the request content
        $content = $request->getContent();
        $camp_id = $request->get('camp_id');
        $useCase_type = $request->get('useCase_type');
        $cnf_tgt = $request->get('cnf_tgt');
        $view_type = $request->get('view_type');
        $campaign_details = $campaignRepository->get($camp_id);
        if($useCase_type ==='webhook' or $useCase_type ==='webhooks'){
            $metaKey = ObjectRegisterMetaRepository::MetaKeyWebHook;
        }else{
            $metaKey = ObjectRegisterMetaRepository::MetaKeyPage;
        }
        $pageSettings = $objectRegisterMetaRepository->getJsonMetaValueAsArray($campaign_details['objectregister_id'],
            ObjectRegisterMetaRepository::MetaKeyPage);
        try {
            $camp_params = $campaignService->convertRequestToArray($content);
            $cnf_group = 'Settings';
            $cnf_tgt = ucfirst($cnf_tgt);
            $pageSettings[$cnf_group][$cnf_tgt] = $camp_params;
            // Check whether data is stored in DB or not.
            // Get the data if data is stored successfully.
            $status = $objectRegisterMetaRepository->updateProjectSettings($campaign_details['objectregister_id'],json_encode($pageSettings));
            if (is_bool($status) && $status) {
                $response = $campaignService->fetchCampaignSettings($camp_id,$cnf_group, $cnf_tgt,$metaKey);
                $response[ 'success' ] = 'Campaign settings are updated successfully';
            } else {
                $response[ 'error' ] = 'Something went wrong, Unable to update campaign settings';
            }

        } catch (\JsonException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        //$response[ 'c_set' ][ 'menu_integration_status' ] = $companyRepository->getCompanyIntegrationMetaData($user[ 'company_id' ]);
        $response['c_set']['camp_id'] = $camp_id;
        $response['c_set']['view_type'] = $view_type;

        // Dynamically generate twig view.
        if($useCase_type ==='webhooks' or $useCase_type === 'webhook'){
            $twig_view = 'webhook_settings/' . $cnf_tgt . '.twig';

        }else{
            $twig_view = 'campaign_settings/' . $cnf_tgt . '.twig';
        }
        $response['current_user'] = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        return $this->render($twig_view, $response);
    }

    /**
     * @param array $custom_pages
     * @return array
     * @author Pradeep
     */
    protected function validatePageName(array $custom_pages): array
    {
        $validated_pages['custom'] = [];
        if (!empty($custom_pages['custom'])) {
            foreach ($custom_pages['custom'] as $page) {
                // Check for empty array and empty page name
                if(empty($page) || !isset($page['name']) || empty($page['name'])) {
                    continue;
                }
                //if pageName does not contain .html extension
                if (strpos($page['name'], '.html') === false) {
                    if(strpos($page['name'], '.') !== false){
                        //this is to remove other extensions
                        $page['name'] = substr($page['name'], 0, strpos($page['name'], "."));
                    }
                    $page[ 'name' ] .= '.html';
                }
                if (!empty($page['name'])) {
                    $validated_pages['custom'][]['name'] = $page['name'];
                }
            }
        }

        return $validated_pages;
    }

}
