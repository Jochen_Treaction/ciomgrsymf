<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\AuthenticationService;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AppCacheService;
use Symfony\Component\HttpClient\HttpClient;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


/**
 * !!! TEMPORARY CONTROLLER !!!
 * provides temporary route to Clean Up Html - Minimum Viable Product
 * accessible only via direct route call - not provided in menu
 */
class CleanUpHtmlController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{

    private AuthenticationUtils $authenticationUtils;
    private AuthenticationService $authenticationService;
    private AppCacheService $apcuCacheService;

    public function __construct(AuthenticationUtils $authenticationUtils, AuthenticationService $authenticationService, AppCacheService $apcuCacheService)
    {
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->apcuCacheService = $apcuCacheService;
    }


    /**
     * @Route( "/mvp/cleanup/html", name="mvpCleanUpHtmlShow", methods={"GET"})
     */
    public function mvpCleanUpHtmlShow():Response
    {
        $currentUser = $this->getCurrentUser();

        $twigParams = [
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'], // old twig-template needs that separately
        ];
        return $this->render('/mvp/cleanUpHtml.twig', $twigParams);
    }


    /**
     * @Route( "/test/cleanup/html", name="testCleanUpHtmlShow", methods={"GET"})
     * @internal <b>CAUTION: <span style="color:red">Runs only on dev and testmio!!!</span></b>
     */
    public function testCleanUpHtml(Request $request, LoggerInterface $logger):Response
    {
        $currentUser = $this->getCurrentUser();

        $twigParams = [
            'current_user' => $currentUser,
            'account' => $currentUser['company_name'], // old twig-template needs that separately
        ];
        return $this->render('/mvp/testCleanUpHtml.twig', $twigParams);

    }



    /**
     * @Route( "/test/cleanup/html/testrun", name="testCleanUpHtmlRun", methods={"POST"})
     */
    public function testCleanUpHtmlRun(Request $request, LoggerInterface $logger, HttpClientInterface $httpClient):JsonResponse
    {

        $body = $request->getContent();

        $options = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json'
            ],
            'body' => $body,
        ];

        try {
            $response = $httpClient->request('POST', 'https://mshtmloptimiser.dev-api-in-one.net/advertorial/inlinecss/cleanup/html', $options); // TODO: remove hard coding in new code !!!!q
        } catch (TransportExceptionInterface $e) {
            return $this->json(['response' => ['success' => false, 'msg' => $e->getMessage(), 'cleanhtml' => '' ]]);
        }

        if($response->getStatusCode() === 200) {
            return $this->json($response->toArray());
        }

        return $this->json(['success' => false, 'msg' => 'oops ..., something went wrong' ]);
    }




    /**
     * @Route( "/mvp/cleanup/html/run", name="mvpCleanUpHtmlRun", methods={"POST"})
     * @internal <b>CAUTION: <span style="color:red">Runs only on dev and testmio!!!</span></b>
     */
    public function mvpCleanUpHtmlRun(Request $request, LoggerInterface $logger):Response
    {
        if ($_ENV['APP_ENV'] !== 'dev' && $_ENV['APP_ENV'] !== 'testmio') {
            $logger->error('EXECUTED ON WRONG SYSTEM', [$_ENV['APP_ENV'], __METHOD__, __LINE__] );
            return $this->json(['cleanhtml' => '']);
        }

        // $html = $request->getContent();
        // $logger->info('html', [$html, __METHOD__, __LINE__]);

        $json = $request->getContent();
        $jsonDecode = json_decode($json, true, 10, JSON_OBJECT_AS_ARRAY);
        $html = $jsonDecode['html'];
        $replaceTelNoAnkers = $jsonDecode['replaceTelNoAnkers'];

/*
        $comments = "/<!--.*?-->/mis";
        $scripts = '/<script>.*?<\/script>/mis';
        $classes = '/class\s*=\s*"[a-z0-9 _\-]*"/i';
        $microsoft_mso = '/mso\-[a-z0-9\-_]*\s*:\s*[0-9a-z\- _%!#]*[;"]{1}/i'; //  mso- in style
        $microsoft_ms ='/\-ms\-[a-z0-9\-_]*\s*:\s*[0-9a-z\- _%!#]*[;"]{1}/i'; // -ms in style
        $style = '/style=";/';
        $blank_semicolon = '/ ;/';
        $duplicate_semicolons = "/;{2,}/"; // from microsoft cleanup
        $spacelines = '/^\s+$/ims'; // before last one !!!
        $duplicate_crlf ='/\n{2,}/ims'; // as last one !!!


        $cleanhtml = preg_replace([$comments, $scripts, $classes, $microsoft_mso, $microsoft_ms, $style, $blank_semicolon, $duplicate_semicolons, $spacelines, $duplicate_crlf ],["", "", " ", ";", ";", ";", ";", ";", "", "\n"],$html);
*/
        // V3.0
        $comments = "/<!--.*?-->/mis";
        $scripts = '/<script>.*?<\/script>/mis';
        $classes = '/class\s*=\s*"[a-z0-9_ \-]*"/i';
        $microsoft_mso = '/mso\-[a-z0-9\-_]*\s*:\s*[0-9a-z\- _%!#]*;?/i'; //  mso- in style
        $microsoft_ms ='/\-ms\-[a-z0-9\-_]*\s*:\s*[0-9a-z\- _%!#]*;?/i'; // -ms in style
        $style = '/style=";>/'; // , ""
        $blank_semicolon = '/ ;/';
        $duplicate_semicolons = "/;{2,}/";
        $spacelines = '/^\s+$/ims';
        $duplicate_crlf ='/\n{2,}/ims';
        $title = '/<title.*?<\/title>/mi';
        $link_rel_icon = '/<link.*rel=[\'"]{1}icon[\'"]{1}.*>/';
        $href_tel_singleline = '/<a.*(href=[\'"]{1}((tel|mobil|handy|fon|telefon|festnetz|phone)?[\+ :\-\/0-9]+)[\'"]{1}).*>.*?<\/a>/mi';
        // --- as off 2022-04-28
        $head = '/.*<\/head>/ms';
        $headReplace = '<!DOCTYPE html><html><head><meta content="text/html; charset=utf-8" http-equiv="content-type"><meta content="text/html; charset=utf-8" http-equiv="content-type" style="box-sizing: border-box;"></head>';

        $lang='/lang="[a-z]{1,4}"/i';
        $langReplace='';

        $role='/role="[a-z0-9_\- ]+"/i';
        $roleReplace = '';

        $aria='/aria[\-]{0,1}[a-z]+="[\w\s]+"|aria="[\w\s]+"/i'; // keine Berücksichtigung von Sonderzeichen => No recognition of special characters: aria will stay!
        $ariaReplace = '';

        $betweenEndHeadStartBody='/<\/head>.*<body/is';
        $betweenEndHeadStartBodyReplace='</head><body';

        // do not change order within arrays !!!
        if($replaceTelNoAnkers) {
            $cleanhtml = preg_replace(
                [$title, $link_rel_icon, $href_tel_singleline, $comments, $scripts, $classes, $microsoft_mso, $microsoft_ms, $style, $blank_semicolon, $duplicate_semicolons, $spacelines, $duplicate_crlf, $head, $lang, $role, $aria, $betweenEndHeadStartBody],
                ['', '', '<div>$2</div>', "", "", " ", ";", ";", ">", ";", ";", "", "\n", $headReplace, $langReplace, $roleReplace, $ariaReplace, $betweenEndHeadStartBodyReplace],
                $html
            );
        } else {
            $cleanhtml = preg_replace(
                [$title, $link_rel_icon, $comments, $scripts, $classes, $microsoft_mso, $microsoft_ms, $style, $blank_semicolon, $duplicate_semicolons, $spacelines, $duplicate_crlf, $head, $lang, $role, $aria, $betweenEndHeadStartBody],
                ['', '', "", "", " ", ";", ";", ">", ";", ";", "", "\n", $headReplace, $langReplace, $roleReplace, $ariaReplace, $betweenEndHeadStartBodyReplace],
                $html
            );
        }

        return $this->json(['cleanhtml' => $cleanhtml]);
    }




    /**
     * @return array
     * @author jsr
     */
    protected function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {

            $company_id = $this->apcuCacheService->get($this->apcuCacheService::SELECTED_COMPANY_ID_OF_USER.$user['email']);
            if(false !== $company_id) {
                $user['company_id'] = $company_id;
            }

            return $user;
        }
        return [];
    }
}
