<?php

namespace App\Controller;

use App\Repository\CampaignRepository;
use App\Repository\ContactRepository;
use App\Repository\CustomfieldsRepository;
use App\Repository\ExternalDataQueueRepository;
use App\Repository\IntegrationsRepository;
use App\Repository\LeadSyncLogRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\ObjectRegisterRepository;
use App\Repository\StatusDefRepository;
use App\Repository\UsersRepository;
use App\Services\AioServices;
use App\Services\AuthenticationService;
use App\Services\IntegrationService;
use App\Services\UtilsService;
use App\Types\ContactDataTableViewTypes;
use App\Types\IntegrationTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Class LeadController
 * @package App\Controller
 * @author  pva
 * @Route( "/contact")
 */
class ContactController extends AbstractController
{

    protected $authenticationUtils;
    protected $authenticationService;
    protected $usersRepository;
    protected $contactRepository;
    protected $customFieldsRepository;
    protected $mioStandardFieldRepository;
    protected $contactDataTableViewTypes;
    protected $utilsService;
    /**
     * @var IntegrationTypes
     * @author Pradeep
     */
    private $integrationTypes;
    /**
     * @var LeadSyncLogRepository
     * @author Pradeep
     */
    private $leadSyncLogRepository;


    /**
     * ContactController constructor.
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param IntegrationTypes $integrationTypes
     * @param LeadSyncLogRepository $leadSyncLogRepository
     * @param AioServices $aioServices
     * @param UsersRepository $usersRepository
     * @param ContactRepository $contactRepository
     * @param CustomfieldsRepository $customFieldsRepository
     * @param MIOStandardFieldRepository $mioStandardFieldRepository
     * @param ContactDataTableViewTypes $contactDataTableViewTypes
     * @param UtilsService $utilsService
     * @author jsr
     */
    public function __construct(
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService,
        IntegrationTypes $integrationTypes,
        LeadSyncLogRepository $leadSyncLogRepository,
        AioServices $aioServices,
        UsersRepository $usersRepository,
        ContactRepository $contactRepository,
        CustomfieldsRepository $customFieldsRepository,
        MIOStandardFieldRepository $mioStandardFieldRepository,
        ContactDataTableViewTypes $contactDataTableViewTypes,
        UtilsService $utilsService
    ) {
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->usersRepository = $usersRepository;
        $this->contactRepository = $contactRepository;
        $this->customFieldsRepository = $customFieldsRepository;
        $this->mioStandardFieldRepository = $mioStandardFieldRepository;
        $this->contactDataTableViewTypes = $contactDataTableViewTypes;
        $this->utilsService = $utilsService;
    }


    /**
     * @Route( "/overview/dep", name="contact.ovewview", methods={"GET"})
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param UsersRepository $usersRepository
     * @param ContactRepository $contactRepository
     * @return Response
     * @author Pradeep
     * @deprecated
     */
    public function getContactOverview(
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        UsersRepository $usersRepository,
        ContactRepository $contactRepository,
        CustomfieldsRepository $customFieldsRepository,
        MIOStandardFieldRepository $mioStandardFieldRepository,
        ContactDataTableViewTypes $contactDataTableViewTypes,
        LoggerInterface $logger
    ): Response {
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);
        if (empty($user) || empty($userEmail)) {
            return $this->redirect('/');
        }
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }
        $currentUser = $this->getCurrentUser();

        $companyId = $currentUser[ 'company_id' ];
        // Get all contacts from DB (including duplicate EMails)
        $contacts = $contactRepository->getLeadsForContactOverview($companyId);
        // Get all supported custom fields for the company.
        $customFields = $customFieldsRepository->getProjectCustomfields($companyId, UniqueObjectTypes::CUSTOMFIELDS);
        // Removes the duplicate EMail and appends the custom fields.
        $contacts = $contactRepository->appendContactWithCustomFields($contacts, $customFields);
        // fetches the supported contact status
        $response[ 'c_status' ] = $contactRepository->getContactStatus();
        // assign Placeholder for the contacts to show in UserInterface.
        $contacts = $mioStandardFieldRepository->assignPlaceHoldersForLeadTwigView($contacts);
        // reorder all the fields as per the requirement to show in the UserInterface.
        $response[ 'c_leads' ] = $contactDataTableViewTypes->reOrderFieldsForDataTableView($contacts, $customFields);

        $response[ 'current_user' ] = $currentUser;
        $response[ 'account' ] = $user[ 'company_name' ];//append user name

        // For Contact Sync.
        $forwardResponse = $this->forward('App\Controller\LeadController::isAPIkeyChanged');
        if ($forwardResponse->getStatusCode() === 200) {
            $content = json_decode($forwardResponse->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $logger->info('sync Setting :' . json_encode($content, JSON_THROW_ON_ERROR));
            $response[ 'lead_sync' ] = !empty($content) ? $content : '';
        }

        return $this->render('leads/ovewview.twig', $response);
    }

    /**
     * @return array
     * @author jsr
     */
    protected function getCurrentUser(): array
    {
        $user = $this->authenticationService->getUserParametersFromCache($this->authenticationUtils->getLastUsername());
        if (!empty($user)) {
            return $user;
        }

        return [];
    }

    /**
     * Route No. 44, Refactorname lead.getallleads
     * $app->get('/contacts', 'LeadController:getAllLeads')->setName('leads.active');
     * @Route( "/history/dep", name="leads.active", methods={"GET"})
     * @param CampaignRepository $campaign
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param UsersRepository $usersRepository
     * @param ContactRepository $contactRepository
     * @param CustomfieldsRepository $customFieldsRepository
     * @param CampaignRepository $campaignRepository
     * @param MIOStandardFieldRepository $mioStandardFieldRepository
     * @param ContactDataTableViewTypes $contactDataTableViewTypes
     * @return Response
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author pva | jsr
     * @deprecated
     */
    public function getAllLeads(
        CampaignRepository $campaign,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        UsersRepository $usersRepository,
        ContactRepository $contactRepository,
        CustomfieldsRepository $customFieldsRepository,
        CampaignRepository $campaignRepository,
        MIOStandardFieldRepository $mioStandardFieldRepository,
        ContactDataTableViewTypes $contactDataTableViewTypes
    ): Response {
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);

        if (empty($user) || empty($userEmail)) {
            return $this->redirect('/');
        }
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }
        $response[ 'current_user' ] = $this->getCurrentUser();
        $response[ 'account' ] = $user[ 'company_name' ];//append user name
        $companyId = $response[ 'current_user' ][ 'company_id' ];

        // Get all contacts from DB (including duplicate EMails)
        $contacts = $contactRepository->getLeadsForContactHistory($companyId);
        // Get all supported custom fields for the company.
        $customFields = $customFieldsRepository->getProjectCustomfields($companyId, UniqueObjectTypes::CUSTOMFIELDS);
        // Removes the duplicate EMail and appends the custom fields.
        $contacts = $contactRepository->appendContactWithCustomFields($contacts, $customFields);
        // assign Placeholder for the contacts to show in UserInterface.
        $contacts = $mioStandardFieldRepository->assignPlaceHoldersForLeadTwigView($contacts);
        // reorder all the fields as per the requirement to show in the UserInterface.
        $response[ 'c_leads' ] = $contactDataTableViewTypes->reOrderFieldsForDataTableView($contacts, $customFields);
        $response[ 'c_status' ] = $contactRepository->getContactStatus();
        $response[ 'campaigns' ] = $campaignRepository->getCampaignForLeadView($companyId);
        return $this->render('leads/history.twig', $response);
    }

    /**
     * Route No. 44.1, Refactorname lead.getleadsjson
     * $app->get('/account/contacts', 'LeadController:getLeadsJson')->setName('leads.active_json');
     * @Route( "/json", name="leads.active_json", methods={"GET"})
     * @param ContactRepository $lead
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param LoggerInterface $logger
     * @return Response
     * @author pva
     * @deprecated
     */
    public function getLeadsJson(
        ContactRepository $lead,
        AuthenticationUtils $authenticationUtils,
        AuthenticationService $authenticationService,
        LoggerInterface $logger
    ): Response {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $response = new Response();
        try {
            $leads_for_cmp = $lead->getLeadsForDataTableView($user[ 'company_id' ]);
            $response->setContent(json_encode($leads_for_cmp, JSON_UNESCAPED_UNICODE));
            $response->headers->set('Content-Type', 'application/json');
        } catch (Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $response;
    }


    /**
     *
     * Route No. 46, Refactorname lead.getleadsbycampaignid
     * $app->get('/account/{acc_id}/campaign/{camp_id}/leads', 'LeadController:getLeadsByCampaignId')->setName('leads.LeadsByCampaignId');
     * #Route( "/account/campaign/{camp_id}/leads", name="leads.LeadsByCampaignId", methods={"GET"})
     * @Route( "/{camp_id}/contacts", name="leads.LeadsByCampaignId", methods={"GET"})
     * @param Request $request
     * @param ContactRepository $leadsRepository
     * @param ContactRepository $contactRepository
     * @param CustomfieldsRepository $customfieldsRepository
     * @param CampaignRepository $campaignRepository
     * @return Response
     * @throws NonUniqueResultException
     * @author pva
     */
    public function getLeadsByCampaignId(
        Request $request,
        ContactRepository $leadsRepository,
        ContactRepository $contactRepository,
        CustomfieldsRepository $customfieldsRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        CampaignRepository $campaignRepository,
        CustomfieldsRepository $customFieldsRepository,
        MIOStandardFieldRepository $mioStandardFieldRepository,
        ContactDataTableViewTypes $contactDataTableViewTypes
    ): Response {
        $response[ 'current_user' ] = $this->getCurrentUser();
        $companyId = $response[ 'current_user' ][ 'company_id' ];
        $cmpObjRegId = $request->get('camp_id');
        $objRegDetails = $objectRegisterRepository->getObjectregisterById($cmpObjRegId);
        if (is_null($objRegDetails)) {
            $response[ 'c_leads' ] = [];
        } else {
            $objectUniqueId = $objRegDetails->getObjectUniqueId();
            $contacts = $leadsRepository->getLeadsByCampaignObjectRegisterId($objectUniqueId);
            // Get all supported custom fields for the company.
            $customFields = $customFieldsRepository->getProjectCustomfields($companyId,
                UniqueObjectTypes::CUSTOMFIELDS);
            // Removes the duplicate EMail and appends the custom fields.
            $contacts = $contactRepository->appendContactWithCustomFields($contacts, $customFields);
            // assign Placeholder for the contacts to show in UserInterface.
            $contacts = $mioStandardFieldRepository->assignPlaceHoldersForLeadTwigView($contacts);
            // reorder all the fields as per the requirement to show in the UserInterface.
            $response[ 'c_leads' ] = $contactDataTableViewTypes->reOrderFieldsForDataTableView($contacts,
                $customFields);
        }

        $response[ 'c_status' ] = $contactRepository->getContactStatus();
        $response[ 'campaigns' ] = $campaignRepository->getCampaignForLeadView($companyId);
        //return $this->json($response);
        return $this->render('leads/leads_for_campaign.twig', $response);
    }


    /**
     * @Route("/{contact_id}/status/{view_type}/{contact_new_status}", name="leads.changeLeadStatus", methods={"GET"})
     * @param Request $request
     * @param ContactRepository $contactRepository
     * @param StatusDefRepository $statusDefRepository
     * @param AioServices $aioServices
     * @param IntegrationsRepository $integrationsRepository
     * @param LoggerInterface $logger
     * @return Response
     * @author Pradeep
     */
    public function changeLeadStatus(
        Request $request,
        ContactRepository $contactRepository,
        StatusDefRepository $statusDefRepository,
        AioServices $aioServices,
        IntegrationsRepository $integrationsRepository,
        LoggerInterface $logger
    ): Response {
        try {
            $response[ 'current_user' ] = $this->getCurrentUser();
            $companyId = $response[ 'current_user' ][ 'company_id' ];
            $contactId = $request->get('contact_id');
            $newStatus = $request->get('contact_new_status');
            //Get the view type
            $viewType = $request->get('view_type');
            if (!$statusDefRepository->isValidStatus($newStatus)) {
                $message = sprintf("invalid status provided %s", $newStatus);
                throw new RuntimeException($message);
            }

            $contact = $contactRepository->get($contactId);
            if (empty($contact) || empty($contact[ 'email' ])) {
                throw new RuntimeException('failed to find contact details');
            }

            // UPDATE LEAD STATUS TO DATABASE.
            if (!$contactRepository->changeLeadStatus($contact[ 'email' ], $newStatus)) {
                throw new RuntimeException('Failed to update Contact Status to ' . $newStatus);
            }
            // Get eMIO Details from Database
            $eMIOIntegrationConfig = $integrationsRepository->getIntegrationConfigForCompany($companyId,
                UniqueObjectTypes::EMAILINONE);

            // Update eMIO with unsubscribe or blacklist email.
            if (!empty($eMIOIntegrationConfig) && !empty($eMIOIntegrationConfig[ 'settings' ][ 'apikey' ])) {

                $eMIOAPIKey = $eMIOIntegrationConfig[ 'settings' ][ 'apikey' ];
                $blacklistId = $eMIOIntegrationConfig[ 'settings' ][ 'blacklist_id' ] ?? '';
                $blacklistFlaggedName = $eMIOIntegrationConfig[ 'settings' ][ 'blacklisting_flagged_name' ] ?? '';

                // Fraud or Inactive
                if (($newStatus === StatusDefRepository::FRAUD || $newStatus === StatusDefRepository::INACTIVE)
                    && !$aioServices->unsubscribeContactIneMIO($contact[ 'email' ], $eMIOAPIKey)) {
                    throw new RuntimeException('Failed to unsubscribe contact in eMIO');
                }
                // blacklist contact in eMIO.
                if (($newStatus === StatusDefRepository::STATUSDEF_BLACKLISTED) &&
                    !$aioServices->blacklistContactIneMIO($contact[ 'email' ], $eMIOAPIKey, $blacklistId,
                        $blacklistFlaggedName)) {
                    throw new RuntimeException('Failed to blacklist contact in eMIO');
                }
            }

            return $this->redirectToRoute('contact.viewType', ['viewType' => $viewType]);
        } catch (Exception|RuntimeException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->redirectToRoute('contact.viewType', ['viewType' => $viewType]);
        }
    }

    /**
     *
     * @Route( "/{lead_id}/deactivate", name="leads.deactivateLeadById", methods={"GET"})
     * @param Request $request
     * @param ContactRepository $leads
     * @return RedirectResponse
     * @author pva
     * @deprecated  by new service 'changeLeadStatus'
     */
    public function deactivateLeadById(Request $request, ContactRepository $leads): Response
    {
        $leadId = $request->get('lead_id');
        if (empty($leadId) || $leadId <= 0) {
            throw new RuntimeException('Invalid LeadId, Failed to detactivate Lead');
        }
        $inactive = StatusDefRepository::INACTIVE;
        $leads->changeLeadStatus($leadId, $inactive);

        return $this->redirectToRoute('leads.active');
    }


    /**
     *
     * Route No. 48, Refactorname lead.reactivateleadbyid
     * $app->get('/account/lead/{lead_id}/reactivate', 'LeadController:reactivateLeadById')->setName('leads.reactivateLeadById');
     * #Route( "/account/lead/{lead_id}/reactivate", name="leads.reactivateLeadById", methods={"GET"})
     * @Route( "/{lead_id}/reactivate", name="leads.reactivateLeadById", methods={"GET"})
     * @param Request $request
     * @param ContactRepository $leads
     * @return RedirectResponse
     * @author pva
     * @deprecated by new service 'changeLeadStatus'
     */
    public function reactivateLeadById(Request $request, ContactRepository $leads): Response
    {
        $lead_id = $request->get('lead_id');
        $lead_status = 'active';
        $status = $leads->changeLeadStatus($lead_id, $lead_status);

        return $this->redirectToRoute('leads.active');
    }


    /**
     *
     * Route No. 49, Refactorname lead.getdetailleadviewbyid
     * $app->get('/account/lead/{lead_id}/detail', 'LeadController:getLeadSingleView')->setName('leads.detailLeadViewById');
     * #Route( "/account/lead/{lead_id}/detail", name="leads.detailLeadViewById", methods={"GET"})
     * @Route( "/{lead_id}/detail", name="leads.detailLeadViewById", methods={"GET"})
     * @param Request $request
     * @param ContactRepository $leads
     * @param MIOStandardFieldRepository $mioStandardFieldRepository
     * @param LoggerInterface $logger
     * @return Response
     * @author pva | jsr
     */
    public function getLeadSingleView(
        Request $request,
        ContactRepository $leads,
        MIOStandardFieldRepository $mioStandardFieldRepository,
        LoggerInterface $logger
    ): Response {
        $response = [];
        try {
            $lead_id = $request->get('lead_id');
            if ($lead_id <= 0 || empty($lead_id)) {
                throw new RuntimeException('Invalid ContactId Provided');
            }
            $currentUser = $this->getCurrentUser();

            // Lead details from Database.
            $leadDetails = $leads->fetchDetailsForSingleLeadView($lead_id);
            $customFields = $leads->getLeadCustomFields($lead_id, UniqueObjectTypes::CUSTOMFIELDS);

            // Filter lead details according to category.
            $standardFields = $mioStandardFieldRepository->filterContactDetails(
                $leadDetails,
                MIOStandardFieldRepository::STANDARD_FIELDS
            );
            $eCommerceFields = $mioStandardFieldRepository->filterContactDetails(
                $leadDetails,
                MIOStandardFieldRepository::ECOMMERCE_FIELDS
            );
            $technicalFields = $mioStandardFieldRepository->filterContactDetails(
                $leadDetails,
                MIOStandardFieldRepository::TECHNICAL_FIELDS
            );

            if (isset($leadDetails[ 'objectregister_id' ])) {
                $leadDetails[ 'unique_object' ] =
                    $leads->getUniqueObjectForLead($leadDetails[ 'objectregister_id' ]);
            }

            // Get all the MIO Fields
            $mioFields = $mioStandardFieldRepository->getAll();

            return $this->render('leads/single_lead_view.twig',
                [
                    'lead' => $leadDetails,
                    'standard_fields' => $standardFields,
                    'custom_fields' => $customFields,
                    'ecommerce_fields' => $eCommerceFields,
                    'technical_fields' => $technicalFields,
                    'mio_fields' => $mioFields,
                    'current_user' => $currentUser,
                ]);

        } catch (Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->render('leads/single_lead_view.twig', [
                'lead' => [],
                'standard_fields' => [],
                'custom_fields' => [],
                'ecommerce_fields' => [],
                'technical_fields' => [],
                'unique_object' => '',
                'mio_fields' => [],
                'current_user' => [],
            ]);
        }
    }


    /**
     * @Route( "/sync/{integration_system}", defaults={"integration_system"="email_in_one"} ,
     *     methods={"POST"}, name="leads.sync")
     * @param Request $request
     * @param IntegrationTypes $integrationTypes
     * @param IntegrationsRepository $integrationsRepository
     * @param LeadSyncLogRepository $leadSyncLogRepository
     * @param ContactRepository $contactRepository
     * @param IntegrationService $integrationService
     * @param AioServices $aioServices
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @author Pradeep
     */
    public function syncLeads(
        Request $request,
        IntegrationTypes $integrationTypes,
        IntegrationsRepository $integrationsRepository,
        LeadSyncLogRepository $leadSyncLogRepository,
        ContactRepository $contactRepository,
        IntegrationService $integrationService,
        AioServices $aioServices,
        LoggerInterface $logger
    ): JsonResponse {

        try {
            // get the two parameters.
            // 1. sync_all -> synchronize all the contacts
            $syncAll = $request->get('sync_all');
            // 2. continue_sync -> synchronize from last sync.
            // when there is no last_sync present , sync_all.
            $continueSync = $request->get('continue_sync');

            $logger->info('integrationButton ' . json_encode([$syncAll, $continueSync]));
            //return $this->json('true');

            $response[ 'current_user' ] = $this->getCurrentUser();
            $companyId = $response[ 'current_user' ][ 'company_id' ];

            $integrationName = strtolower($request->get('integration_system'));

            // create all the custom fields for the EMIO
            if ($integrationName === UniqueObjectTypes::EMAILINONE) {
                $eMIOIntegrationConfig = $integrationsRepository->getIntegrationConfigForCompany($companyId,
                    UniqueObjectTypes::EMAILINONE);
                if (is_null($integrationService->autoCreateEMIOCustomField($eMIOIntegrationConfig))) {
                    throw new Exception('Invalid Integration, Try to reconfigure from integration menu');
                }
            }
            // fetch the settings given for the integration(hubspot, email-in-one.)
            $integration = $integrationsRepository->getSettings($integrationName, $companyId);
            if (empty($integration) || !isset($integration[ 'id' ], $integration[ 'settings' ])) {
                throw new Exception('Invalid Integration or ConfigurtionData is missing.');
            }

            if ($continueSync) {
                // fetch the minContactId and maxContactId
                $lastContactId = $leadSyncLogRepository->getLastContactId($companyId);
                $count = $contactRepository->getCountForSync($companyId, $lastContactId);
                $startContactId = $lastContactId;//$contactRepository->getStartContactIdForSync($companyId);

            } else {
                if ($syncAll) {
                    // Fetch the minContactId and maxContactId
                    $count = $contactRepository->getCountForSync($companyId);
                    $startContactId = $contactRepository->getStartContactIdForSync($companyId);
                }
            }

            $endContactId = $contactRepository->getEndContactIdForSync($companyId);
            $logger->info('sync : ' . json_encode(['integration' => $integration, 'count' => $count],
                    JSON_THROW_ON_ERROR));
            $t = $leadSyncLogRepository->insert((int)$integration[ 'id' ], $integration[ 'settings' ], $startContactId,
                $endContactId, $count);
            $logger->info('sync insert result : ' . json_encode(['result' => $t],
                    JSON_THROW_ON_ERROR));
            return $this->json(true);

        } catch (Exception $e) {
            return $this->json(['error' => $e->getMessage()]);
        }
    }

    public function isAPIkeyChanged(
        IntegrationsRepository $integrationsRepository,
        LeadSyncLogRepository $leadSyncLogRepository,
        ExternalDataQueueRepository $externalDataQueueRepository,
        LoggerInterface $logger
    ): JsonResponse {
        $response[ 'current_user' ] = $this->getCurrentUser();
        $companyId = $response[ 'current_user' ][ 'company_id' ];

        // check if there are external_queues with status new ?
        // if so return null.
        $unProcessedRecords = $externalDataQueueRepository->getNumberOfUnProcessedRecords($companyId);
        $logger->info('iSPIACHnagle:' . json_encode($unProcessedRecords));
        if ((int)$unProcessedRecords > 0) {
            return $this->json([
                'show_sync_button' => false,
                'is_apikey_changed' => false,
                'previous_sync_record' => false,
            ]);
        }

        $integrationName = 'email-in-one';
        $settings = $integrationsRepository->getSettings($integrationName, $companyId);
        if (empty($settings)) {
            return $this->json([
                'show_sync_button' => false,
                'is_integration_configured' => false,
                'is_apikey_changed' => false,
                'previous_sync_record'
                => false,
            ]);
        }
        $integrationId = $settings[ 'id' ];
        $integrationConfig = $settings[ 'settings' ];
        $leadSync = $leadSyncLogRepository->get((int)$integrationId);
        $logger->info('leadSync details ' . json_encode($leadSync));
        if (empty($leadSync) || !isset($leadSync[ 'settings' ])) {
            return $this->json([
                'show_sync_button' => true,
                'is_integration_configured' => true,
                'is_apikey_changed' => false,
                'previous_sync_record'
                => false,
            ]);
        }
        $leadSyncSettings = json_decode($leadSync[ 'settings' ], true, 512, JSON_THROW_ON_ERROR);
        $logger->info('$leadSyncSettings details ' . json_encode($leadSyncSettings));
        if (empty($leadSyncSettings)) {
            return $this->json([
                'cron-job-external-queue-running' => false,
                'is_apikey_changed' => 'error',
                'show_sync_button' => true,
                'is_integration_configured' => true,
                'previous_sync_record' => true,
                'error' => 'Failed to fetch previous synchronized config.',
            ]);
        }


        $status = $leadSyncLogRepository->isSettingsChanged($integrationConfig, $leadSyncSettings);

        return $this->json([
            'show_sync_button' => true,
            'cron-job-external-queue-running' => false,
            'is_integration_configured' => true,
            'is_apikey_changed' => $status,
            'previous_sync_record' =>
                true,
        ]);
    }


    /**
     * @Route( "/export/excel", name="contact.exportData", methods={"GET","POST"})
     * @return Response
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @author Aki
     * @deprecated
     * @internal  Example of exporting data in excel
     */
    public function exportData()
    {
        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->getCurrentUser();
        //basic user check
        if (empty($currentUser) || empty($userEmail)) {
            $message = "Invalid user";
            return $this->redirect('/');
        }
        //Check for DPA acceptance
        if (!$this->usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }
        //get the company id
        $companyId = $currentUser[ 'company_id' ];

        // Get all contacts from DB (including duplicate EMails)
        $contacts = $this->contactRepository->getLeadsForContactOverview($companyId);
        // Get all supported custom fields for the company.
        $customFields = $this->customFieldsRepository->getProjectCustomfields($companyId,
            UniqueObjectTypes::CUSTOMFIELDS);
        // Removes the duplicate EMail and appends the custom fields.
        $contacts = $this->contactRepository->appendContactWithCustomFields($contacts, $customFields);
        // assign Placeholder for the contacts to show in UserInterface.
        $contacts = $this->mioStandardFieldRepository->assignPlaceHoldersForLeadTwigView($contacts);
        // reorder all the fields as per the requirement to show in the UserInterface.
        $contactsJson = $this->contactDataTableViewTypes->reOrderFieldsForDataTableView($contacts, $customFields);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        //todo:Loop and fill cell values

        $sheet->setCellValue('A1', 'Hello World !');

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = $currentUser[ 'company_name' ] . '_Marketing-In-One.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);
        //return the file
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * @Route( "/export/json", name="contact.exportJson", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @author Aki
     * @deprecated
     * @internal This route returns all contacts in JSON
     */
    public function getContactListByPage(
        Request $request
    ): JsonResponse {
        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->getCurrentUser();
        $response = new JsonResponse();
        $pageNumber = $request->get('pageNumber');
        //basic user check
        if (empty($currentUser) || empty($userEmail)) {
            $message = "Invalid user";
            return $this->utilsService->setResponseWithMessage($response, $message, false);
        }
        //Check for DPA acceptance
        if (!$this->usersRepository->verifyDPAStatusForUser($userEmail)) {
            $message = "user DPA not accepted";
            return $this->utilsService->setResponseWithMessage($response, $message, false);
        }
        //get the company id
        $companyId = $currentUser[ 'company_id' ];
        //Get the count of the total number of records

        // Get all contacts from DB (including duplicate EMails)
        $contacts = $this->contactRepository->getLeadsForContactOverview($companyId);
        // Get all supported custom fields for the company.
        $customFields = $this->customFieldsRepository->getProjectCustomfields($companyId,
            UniqueObjectTypes::CUSTOMFIELDS);
        // Removes the duplicate EMail and appends the custom fields.
        $contacts = $this->contactRepository->appendContactWithCustomFields($contacts, $customFields);

        // assign Placeholder for the contacts to show in UserInterface.
        $contacts = $this->mioStandardFieldRepository->assignPlaceHoldersForLeadTwigView($contacts);
        // reorder all the fields as per the requirement to show in the UserInterface.
        $responseJson[ 'c_leads' ] = $this->contactDataTableViewTypes->reOrderFieldsForDataTableViewWithOutActions($contacts,
            $customFields);

        // For Contact Sync.
        $forwardResponse = $this->forward('App\Controller\ContactController::isAPIkeyChanged');
        if ($forwardResponse->getStatusCode() === 200) {
            $content = json_decode($forwardResponse->getContent(), true, 512, JSON_THROW_ON_ERROR);
            $responseJson[ 'lead_sync' ] = !empty($content) ? $content : '';
        }
        $message = "contacts for page " . $pageNumber;

        return $this->utilsService->setResponseWithMessage($response, $message, true, $responseJson[ 'c_leads' ]);
    }


    /**
     * @Route( "/{viewType}", name="contact.viewType", methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws JsonException
     * @author Aki
     * @internal This route returns twig template for overview of contacts
     */
    public function getContactView(
        LoggerInterface $logger,
        Request $request
    ): Response {
        $viewType = $request->get('viewType');
        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->getCurrentUser();
        //basic user check
        if (empty($currentUser) || empty($userEmail)) {
            return $this->redirect('/');
        }
        //Check for DPA acceptance
        if (!$this->usersRepository->verifyDPAStatusForUser($userEmail)) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }

        //get the company id
        $companyId = $currentUser[ 'company_id' ];
        // Get all supported custom fields for the company.
        $customFields = $this->customFieldsRepository->getProjectCustomfields($companyId,
            UniqueObjectTypes::CUSTOMFIELDS);

        $response[ 'current_user' ] = $currentUser;
        $response[ 'account' ] = $currentUser[ 'company_name' ];//append username
        $response[ 'standard_fields_for_view' ] = $this->contactDataTableViewTypes->dataTableFieldViewOrder();
        $response[ 'custom_fields_for_view' ] = $this->contactDataTableViewTypes->getFieldNameArray($customFields);
        if ($viewType === 'overview') {
            //This is for sync code
            $forwardResponse = $this->forward('App\Controller\ContactController::isAPIkeyChanged');
            $logger->info('forwardRespone', [$forwardResponse->getContent(), __METHOD__, __LINE__]);
            if ($forwardResponse->getStatusCode() === 200) {
                $content = json_decode($forwardResponse->getContent(), true, 512, JSON_THROW_ON_ERROR);
                $logger->info('sync Setting :' . json_encode($content, JSON_THROW_ON_ERROR));
                $response[ 'lead_sync' ] = !empty($content) ? $content : '';
            }
            return $this->render('leads/overview.twig', $response);
        }
        return $this->render('leads/history.twig', $response);

    }

}
