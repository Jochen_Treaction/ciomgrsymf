<?php

namespace App\Controller;

use App\Repository\CampaignRepository;
use App\Repository\CompanyRepository;
use App\Repository\ContactRepository;
use App\Repository\CustomfieldsRepository;
use App\Repository\MIOStandardFieldRepository;
use App\Repository\ObjectRegisterMetaRepository;
use App\Repository\StandardFieldMappingRepository;
use App\Repository\StatusDefRepository;
use App\Repository\UsersRepository;
use App\Repository\WorkflowRepository;
use App\Services\AppCacheService;
use App\Services\AuthenticationService;
use App\Services\CampaignService;
use App\Services\JotFormService;
use App\Services\ProcesshookService;
use App\Services\TwigGlobalsService;
use App\Types\MappingTypes;
use App\Types\PageWizardTypes;
use App\Types\UniqueObjectTypes;
use Doctrine\DBAL\Driver\Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use ZipArchive;
use App\Repository\ObjectRegisterRepository;


/**
 * @property AppCacheService       apcuCache
 * @property ProcesshookService    processhookService
 * @property CampaignService       campaignService
 * @property StatusDefRepository   statusdefRepository
 * @property AuthenticationUtils   authenticationUtils
 * @property AuthenticationService authenticationService
 * @property CampaignRepository    campaignRepository
 */
class CampaignController extends AbstractController
{

    protected $campaignOrganizer;
    protected $webhookOrganizer;
    protected $logger;


    public function __construct(
        LoggerInterface       $logger,
        AppCacheService      $apcuCache,
        ProcesshookService    $processhookService,
        CampaignService       $campaignService,
        StatusDefRepository   $statusDefRepository,
        AuthenticationUtils   $authenticationUtils,
        AuthenticationService $authenticationService,
        CampaignRepository    $campaignRepository
    )
    {
        $this->campaignOrganizer = $_ENV['CAMPAIGN_SERVICE'];
        $this->webhookOrganizer = $_ENV['WEBHOOK_SERVICE'];
        $this->logger = $logger;
        $this->apcuCache = $apcuCache;
        $this->processhookService = $processhookService;
        $this->campaignService = $campaignService;
        $this->statusdefRepository = $statusDefRepository;
        $this->authenticationUtils = $authenticationUtils;
        $this->authenticationService = $authenticationService;
        $this->campaignRepository = $campaignRepository;
    }


    /**
     *
     * Route No. 28,
     * @Route( "/account/pages/active", name="camp.active", methods={"GET"})
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param UsersRepository $usersRepository
     * @param ContactRepository $leadsRepository
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param LoggerInterface $logger
     * @return Response
     * @author aki | jsr
     */
    public function getCampaignsByStatusActive(
        AuthenticationUtils      $authenticationUtils,
        AuthenticationService    $authenticationService,
        CampaignRepository       $campaignRepository,
        CampaignService          $campaignService,
        UsersRepository          $usersRepository,
        ContactRepository        $leadsRepository,
        ObjectRegisterRepository $objectRegisterRepository,
        LoggerInterface          $logger
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $status = 'active';
        if (empty($user)) {
            return $this->redirect('/');
        }

        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($authenticationUtils->getLastUsername())) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }
        $campaigns = $objectRegisterRepository->getCampaignsWithStatus($user['company_id'], UniqueObjectTypes::PAGE, StatusDefRepository::ACTIVE);
        $campaignHasRecords = count($campaigns) !== 0;
        $camp_details = $campaignService->appendLinksForCampaigns($campaigns, 'pages', 'active');//campaign details with url
        //$camp_details are now $camp_details_with_conversions
        $camp_details_with_conversions = $campaignService->addConversionsValueToCampaigns($camp_details, ObjectRegisterMetaRepository::MetaKeyPage);
        $camp_details_with_conversions[0]['account'] = $user['company_name'];//append user name
        //this is for status messages
        $this->getMessageStatus('messageActive', $camp_details);
        return $this->render('campaign/campaigns.twig', [
            'campaigns' => $camp_details_with_conversions,
            'current_user' => $user,
            'campaignHasRecords' => $campaignHasRecords,
        ]);
    }


    private function getMessageStatus(string $key, array &$camp_details): void
    {
        $message = $this->apcuCache->get($key);
        if (!empty($message)) {
            if (!empty($message['error'])) {
                $camp_details[0]['error'] = $message['error'];
            }
            if (!empty($message['success'])) {
                $camp_details[0]['success'] = $message['success'];
            }
            $this->apcuCache->del($key);
        }
    }


    /**
     *
     * Route No. 30,
     * @Route( "/account/pages/draft", name="camp.draft", methods={"GET"})
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return Response
     * @author  aki | jsr
     */
    public function getCampaignsByStatusDraft(
        AuthenticationUtils      $authenticationUtils,
        AuthenticationService    $authenticationService,
        CampaignRepository       $campaignRepository,
        CampaignService          $campaignService,
        ObjectRegisterRepository $objectRegisterRepository
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $status = 'draft';
        $companyId = $user['company_id'];
        $campaigns = $objectRegisterRepository->getCampaignsWithStatus($companyId, UniqueObjectTypes::PAGE, StatusDefRepository::DRAFT);
        $campaignHasRecords = (count($campaigns) === 0) ? false : true;
        $camp_details = $campaignService->addObjectUniqueValueToCampaigns($campaigns);
        $camp_details = $campaignService->appendLinksForCampaigns($camp_details, 'pages', 'draft');
        $camp_details[0]['account'] = $user['company_name'];
        //this is for status messages
        $this->getMessageStatus('messageDraft', $camp_details);
        return $this->render('campaign/draftcampaigns.twig',
            ['campaigns' => $camp_details, 'current_user' => $user, 'campaignHasRecords' => $campaignHasRecords]);
    }


    /**
     *
     * Route No. 32,
     * $app->get('/account/pages/archive', 'CampaignController:getCampaignsByStatusArchive')->setName('camp.archive');
     * #Route( "/account/pages/archive", name="camp.archive", methods={"GET"})
     * @Route( "/account/pages/archive", name="camp.archive", methods={"GET"})
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param TwigGlobalsService $twigGlobalsService
     * @return Response
     * @author aki | jsr
     */
    public function getCampaignsByStatusArchive(
        AuthenticationUtils      $authenticationUtils,
        AuthenticationService    $authenticationService,
        CampaignRepository       $campaignRepository,
        CampaignService          $campaignService,
        ObjectRegisterRepository $objectRegisterRepository,
        TwigGlobalsService       $twigGlobalsService
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $status = 'archive';
        $companyId = $user['company_id'];
        $campaigns = $objectRegisterRepository->getCampaignsWithStatus($companyId, UniqueObjectTypes::PAGE, StatusDefRepository::ARCHIVED);
        $campaignHasRecords = (count($campaigns) === 0) ? false : true;
        $camp_details = $campaignService->appendLinksForCampaigns($campaigns, 'pages', 'archive');
        $camp_details[0]['account'] = $user['company_name'];
        //this is for status messages
        $this->getMessageStatus('messageArchive', $camp_details);
        return $this->render('campaign/archivecampaigns.twig',
            ['campaigns' => $camp_details, 'current_user' => $user, 'campaignHasRecords' => $campaignHasRecords]);
    }


    /**
     *
     * Route No. 34,
     * $app->get('/account/pages/{camp_id}/reactivate', 'CampaignController:getCampaignReactivateById')->setName('camp.reactivateById');
     * #Route( "/account/pages/{camp_id}/reactivate", name="camp.reactivateById", methods={"GET"})
     * @Route( "/account/pages/{camp_id}/reactivate", name="camp.reactivateById", methods={"GET"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param AuthenticationUtils $authenticationUtils ,
     * @param AuthenticationService $authenticationService
     * @param LoggerInterface $logger
     * @return Response
     * @deprecated
     * @author Aki | jsr
     * @deprecated
     */
    public function getCampaignReactivateById(
        Request               $request,
        CampaignRepository    $campaignRepository,
        CampaignService       $campaignService,
        AuthenticationUtils   $authenticationUtils,
        AuthenticationService $authenticationService,
        LoggerInterface       $logger
    ): Response
    {
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);

        if (empty($user)) {
            return $this->redirect('/');
        }
        $companyId = $user['company_id'];
        $camp_id = $request->attributes->get('camp_id');
        $company = str_replace(' ', '', $user['company_name']);
        $user_id = $user['user_id'];
        $useCase_type = 'campaign';
        if (!$campaignRepository->createAdditionalVersionsForCampaign($camp_id, $companyId, $company, $user_id, $useCase_type, $_ENV['CAMPAIGN_DOMAIN'])) {
            $logger->critical('Something went wrong, Could not create version for Campaign', [__METHOD__, __LINE__]);
            $this->apcuCache->addIfNotExists('messageDraft', ['error' => 'Something went wrong, Could not create version for Campaign'], 600);
            return $this->redirectToRoute('camp.draft');
        }
        // get Old campaign details
        $camp_details_old = $campaignRepository->getCampaignDetailsById($camp_id);
        if (empty($camp_details_old) || empty($camp_details_old['name']) || empty($camp_details_old['version'])) {
            $logger->critical('Unable to retrieve old campaign details', [__METHOD__, __LINE__]);
            $this->apcuCache->addIfNotExists('messageDraft', ['error' => 'Unable to retrieve old campaign details'], 600);
            return $this->redirectToRoute('camp.draft');
        }
        // get new campaign details
        $camp_details_new = $campaignRepository->getLatestCampaignInDraft($camp_details_old['name'], $companyId);
        if (empty($camp_details_new) || empty($camp_details_new['name']) || empty($camp_details_new['version']) || !$campaignRepository->copyCampaignParams($camp_details_old['id'],
                $camp_details_new['id']) || !$campaignRepository->copySurvey($camp_details_old['id'], $camp_details_new['id'])) {
            $logger->critical('Unable to retrieve old campaign details', [__METHOD__, __LINE__]);
            $this->apcuCache->addIfNotExists('messageDraft', ['error' => 'Unable to retrieve old campaign details'], 600);
            return $this->redirectToRoute('camp.draft');
        }
        // revise campaign in server
        if (!$campaignService->reviseCampaignInServer($this->campaignOrganizer, $camp_details_new['name'], (int)$camp_details_old['version'],
            (int)$camp_details_new['version'], $camp_details_new['id'], $companyId)) {
            $logger->critical('Unable to revise Campaign in Server', [__METHOD__, __LINE__]);
            $this->apcuCache->addIfNotExists('messageDraft', ['error' => 'Unable to revise Campaign in Server'], 600);
        }
        $this->apcuCache->addIfNotExists('messageDraft', ['success' => 'Campaign reactivated successfully'], 600);
        return $this->redirectToRoute('camp.draft');
    }


    /**
     *
     * Route No. 37,
     * $app->get('/page/create', 'CampaignController:getcreateCampaignById')->setName('camp.createById');
     * #Route( "/page/create", name="camp.create", methods={"GET"})
     * @Route( "/page/create", name="camp.create", methods={"GET"})
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignRepository $campaignRepository
     * @param ContactRepository $contactRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param PageWizardTypes $pageWizardTypes
     * @param CustomfieldsRepository $customfieldsRepository
     * @param MappingTypes $mappingType
     * @return Response
     * @author Aki | jsr
     */
    public function getCreateCampaignById(
        AuthenticationService        $authenticationService,
        AuthenticationUtils          $authenticationUtils,
        CampaignRepository           $campaignRepository,
        ContactRepository            $contactRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        PageWizardTypes              $pageWizardTypes,
        CustomfieldsRepository       $customfieldsRepository,
        MappingTypes                 $mappingType,
        StatusDefRepository          $statusDefRepository,
        MIOStandardFieldRepository   $mioStandardFieldRepository,
        LoggerInterface              $logger
    ): Response
    {
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);
        if (empty($user)) {
            return $this->redirect('/');
        }
        $params = [
            'user_id' => $user['user_id'],
            'company_id' => $user['company_id'],
            'account' => $user['company_name'],
            'company_name' => $user['company_account_no'],
        ];
        //append custom fields from project
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
        if (!empty($projectObjectRegisterId)) {
            $projectSettings = $objectRegisterMetaRepository->getJsonMetaValueAsArray($projectObjectRegisterId,
                ObjectRegisterMetaRepository::MetaKeyProject);
            $params['customFieldList'] = $customfieldsRepository->getProjectCustomfields($user['company_id'],
                UniqueObjectTypes::CUSTOMFIELDS);

            //$params[ 'customFieldList' ] = $projectSettings[ 'customfieldlist' ];
            $redirectPage = $projectSettings['advancedsettings']['redirect_after_registration'];
        }
        $params['ecommerce_fields'] = $mioStandardFieldRepository->getECommerceFields();
        $params['standard_fields'] = $mioStandardFieldRepository->getStandardFields();

        //default values required for pageflow and splittest
        $defaultPageSettings = $pageWizardTypes->getArrayForWizard();
        $params['page_flow'] = $defaultPageSettings['page_flow'];
        if (!empty($redirectPage)) {
            $params['page_flow']['redirect_page'] = $redirectPage;
        }
        //get defined processhooks
        try {
            $processhooks = $this->processhookService->fetchProcesshooks($statusDefRepository::DEFINED,
                $user['company_id']);
            $params['processhooks'] = $this->campaignService->formatProcesshooksForPageWizard($processhooks, null);
        } catch (Exception $e) {
            $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able fetch processhooks'], 600);
        }

        $params['splitTest'] = $defaultPageSettings['splitTest'];
        $params['mapping']['dataStructure'] = $mappingType->getArray();
        $this->logger->info('CreateCampaign ' . json_encode(['campaign' => $params]));
        return $this->render('campaign/createcampaign.twig', ['campaign' => $params, 'current_user' => $user]);
    }


    /**
     *
     * Route No. 38,
     * $app->post('/page/create', 'CampaignController:postCreateCampaignById');
     * #Route( "/page/create", name="camp.postCreate", methods={"POST"})
     * @Route( "/page/create", name="camp.postCreate", methods={"POST"})
     * @param Request $request
     * @param CampaignRepository $campaign_repository
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignService $campaignService
     * @param ObjectRegisterRepository $ObjectRegisterRepository ;
     * @return Response
     * @throws JsonException
     * @author Aravind
     * @internal 1. Creates new page/webhook
     *           2. Create files in client server (z.B:top-aktion)
     */
    public function postCreateCampaignById(
        Request                  $request,
        CampaignRepository       $campaign_repository,
        AuthenticationService    $authenticationService,
        AuthenticationUtils      $authenticationUtils,
        ObjectRegisterRepository $ObjectRegisterRepository,
        CampaignService          $campaignService
    ): Response
    {
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);
        $token = $request->get('token');
        if (empty($user) || !$this->isCsrfTokenValid('campaign-create', $token)) {
            return $this->redirect('/');
        }
        $campaign = $request->get('campaign');
        $campaign['company_id'] = $user['company_id'];
        $campaign['account'] = $user['company_name'];
        $campaign['user_id'] = $user['user_id'];
        // Create Campaign URL
        $campaign['url'] = $campaign_repository->createCampaignURL($campaign['settings']['name'], str_replace(' ', '', $user['company_account_no']), $_ENV['CAMPAIGN_DOMAIN']);
        //objectRegisterLayer entry
        $this->logger->info(__LINE__, [__METHOD__]);
        $ObjectRegisterId = $ObjectRegisterRepository->createInitialObjectRegisterEntryforCampaign($campaign['company_id'], 'page', 'draft');
        if (!empty($ObjectRegisterId)) {
            // Create Campaign return campaign id.dd
            $campaign['id'] = $campaign_repository->createPageInDB($ObjectRegisterId, $campaign['settings']['name'], $campaign['company_id'], $campaign['url'],
                $campaign['user_id']);
            //append fields from project
            $projectObjectRegisterId = (int)$campaign_repository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
            //store workflow settings
            if (!empty($campaign['processhooks']) && $campaign['processhooks'] !== "{}") {
                $campaign['processhookStatus'] = $this->processhookService->saveWorkflow($campaign['id'], $campaign['processhooks']);
                if (is_null($campaign['processhookStatus'])) {
                    $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Workflow could not be saved'], 600);
                    return $this->redirectToRoute('camp.draft');
                }
            }
            //format page with buttons
            if ($campaign['page_flow']['buttons']) {
                $campaign['page_flow'] = $this->campaignService->formatPageflowButtons($campaign['page_flow']);
            }

            // Store Settings in objectRegisterMeta
            if ($campaign['id'] <= 0 || !$campaignService->addCampaignSettings(ObjectRegisterMetaRepository::MetaKeyPage, $campaign, $ObjectRegisterId,
                    $projectObjectRegisterId)) {
                $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Default campaign settings not saved'], 600);
                return $this->redirectToRoute('camp.draft');
            }

            // Add customFields to objectRegister Meta.
            $campaignService->addCampaignCustomFields($campaign['company_id'], $ObjectRegisterId, $campaign['mapping']);
            // Client server related processes
            //create campaign in server
            $campaign['usecase_type'] = 'campaign';
            $isRequestCopy = $request->get('copy');
            if ($isRequestCopy) {
                //copy the campaign
                $oldCampaignName = $request->get('old_campaign_name');
                $oldCampaignVersion = $request->get('old_campaign_version');
                if (!$this->campaignService->copyCampaignInServer(
                    $_ENV['CAMPAIGN_SERVICE'],
                    $oldCampaignName,
                    $campaign['settings']['name'],
                    $oldCampaignVersion,
                    $campaign['id'],
                    $user['company_id'],
                    $user['company_account_no'],
                )) {
                    $this->logger->critical('Something went wrong, could not copy in Server', [__METHOD__, __LINE__]);
                    $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able to copy page in server please try again'], 600);
                    return $this->redirectToRoute('camp.draft');
                }
            }else{
                //create the new campaign
                if (!$campaignService->createCampaignInServer($this->campaignOrganizer, $campaign['settings']['name'], $campaign['id'], $campaign['company_id'],
                    $campaign['usecase_type'], str_replace(' ', '', $campaign['account']))) {
                    $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able to create page in server please try again'], 600);
                    return $this->redirectToRoute('camp.draft');
                }
            }
            //create additional flow control pages in server
            $additionalPages = $campaign['page_flow']['custom_pages'];
            if (sizeof($additionalPages) >= 2) {
                if (!$campaignService->createFlowControlPagesInServer($this->campaignOrganizer, $campaign['settings']['name'], $user['company_account_no'], 1,
                    $additionalPages)) {
                    $this->apcuCache->addIfNotExists('messageDraf',
                        ['error' => 'Could not able to create flow control additional pages in server please try again'], 600);
                    return $this->redirectToRoute('camp.draft');
                }
            }
            //create splittest pages in server
            $splitTest = array_key_exists('status', $campaign['splittest']);
            if ($splitTest) {
                $splittestStatus = $campaign['splittest']['status'];
                if ($splittestStatus === 'on') {
                    if (!$campaignService->createSplittestFolder($this->campaignOrganizer, $campaign['settings']['name'], $user['company_account_no'], 1,
                        $campaign['splittest']['name'])) {
                        $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able to create splittestPages in server please try again'], 600);
                        return $this->redirectToRoute('camp.draft');
                    }
                }
            }
            //Add campaign settings to the server
            if (!$this->campaignService->addOrUpdateSettingsInServer(
                $campaign['id'],
                $user['company_id'],
                $projectObjectRegisterId,
                $campaign['settings']['name'],
                $user['company_account_no'],
                $_ENV['CAMPAIGN_SERVICE']
            )) {
                $this->logger->critical('Something went wrong, could not create in Server', [__METHOD__, __LINE__]);
            }

            //create campaign in matomo
            $campaignService->addCampaignToMatomo($campaign['settings']['name'], $campaign['url'], $campaign['account']);
            $this->apcuCache->addIfNotExists('messageDraft', ['success' => 'Page created successfully'], 600);
            return $this->redirectToRoute('camp.draft');
        } else {
            $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able to create page because of object register'], 600);
            return $this->redirectToRoute('camp.draft');
        }
    }


    /**
     * TODO: not yet required, but campaign-organizer should also delete files and subdomain of campaigns which have status draft only, before they are deleted in the database
     * Route No. 39, Refactorname campaign.getcampaigndeletebyid
     * @Route( "/account/{useCase_type}/{camp_id}/delete", name="camp.deleteById", methods={"GET"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @return Response
     * @author pva
     */
    public function deleteCampaign(
        Request            $request,
        CampaignRepository $campaignRepository,
        CampaignService    $campaignService
    ): Response
    {
        $camp_id = $request->get('camp_id');
        $useCase_type = $request->get('useCase_type');
        $metaKey = "";
        if ($useCase_type == "pages") {
            $metaKey = ObjectRegisterMetaRepository::MetaKeyPage;
        } else {
            if ($useCase_type == "webhooks") {
                $metaKey = ObjectRegisterMetaRepository::MetaKeyWebHook;
            }
        }
        $status = $campaignService->deleteCampaign($camp_id, $metaKey);
        if (is_bool($status) || $status === 'true') {
            $this->apcuCache->addIfNotExists('messageDraft', ['success' => $useCase_type . ' deleted successfully'], 600);
        } else {
            $this->apcuCache->addIfNotExists('messageDraft', ['error' => $useCase_type . ' could not be deleted'], 600);
        }
        if ($useCase_type === 'webhooks') {
            return $this->redirectToRoute('webhook.draft');
        }
        return $this->redirectToRoute('camp.draft');
    }


    /**
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CompanyRepository $companyRepository
     * @param LoggerInterface $logger
     * @param CampaignService $campaignServices
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignService $campaignService
     * @return Response
     * @throws JsonException
     * @internal campaigns status can only be changed from draft to active, independent of running campaigns status
     * Route No. 42,
     * $app->get('/account/pages/{camp_id}/activate', 'CampaignController:getCampaignActivateById')->setName('camp.activateById');
     * #Route( "/account/pages/{camp_id}/activate", name="camp.activateById", methods={"GET"})
     * @Route( "/account/pages/{camp_id}/activate", name="camp.activateById", methods={"GET"})
     * @author   aki
     */
    public function setCampaignStatusToActive(
        Request               $request,
        CampaignRepository    $campaignRepository,
        CompanyRepository     $companyRepository,
        LoggerInterface       $logger,
        CampaignService       $campaignServices,
        AuthenticationService $authenticationService,
        AuthenticationUtils   $authenticationUtils,
        CampaignService       $campaignService
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            return $this->redirect('/');
        }

        $params = ['user_id' => $user['user_id'], 'company_id' => $user['company_id']];
        $camp_id = $request->attributes->get('camp_id');
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];

        if (!$campaignService->archivePreviousVersionsOfActiveCampaign($camp_id)) {
            $logger->critical('something wrong with Active version of page', [__METHOD__, __LINE__]);
        }

        $updateTo = 'active';
        $user_id = $params['user_id'];
        if (!$campaignRepository->updateCampaign($camp_id, StatusDefRepository::ACTIVE, $user_id)) {
            $logger->critical('Something went wrong, Could not activate the page', [__METHOD__, __LINE__]);
        }
        //todo:build survey when needed
        /*$camp_details = $campaignRepository->getCampaignDetailsById($camp_id);
        if ((int)$camp_details['survey_id'] > 0) {
            $survey_questions = $campaignRepository->fetchSurveyQuestions($camp_details['survey_id']);
            $mio_details = $companyRepository->getIntegrationsForCompany($params['company_id'], 'MailInOne');

            if (!empty($survey_questions) && !empty($mio_details)) {
                // call service to MSMIO to create custom fields.
                $status = $campaignServices->createSurveyAsMIOCustomFields($mio_details, $survey_questions);
                $this->logger->info('Campaign service ' . $status, [__METHOD__, __LINE__]);
            }
        }*/
        //add campaign settings to campaign in campaign server
        $campaignSettings = $campaignService->getCampaignSettings($camp_id, $projectObjectRegisterId, ObjectRegisterMetaRepository::MetaKeyPage);

        //add additional parameters for campaign settings
        $additionalSettings = $campaignService->addBasePayLoad($camp_id, $user['company_id']);
        $campaignSettings = array_merge($campaignSettings, $additionalSettings);
        $campaignSettings = json_encode($campaignSettings);
        $campaignName = $campaignRepository->fetchCampaignNameById($camp_id);
        $campaignVersion = $campaignRepository->getVersionFromCampaignId($camp_id);

        //This is calling Ms camp
        if (!$campaignServices->activateCampaignInServer($this->campaignOrganizer, $campaignName, (int)$campaignVersion, $user['company_account_no'],
            $campaignSettings)) {
            $logger->critical('Something went wrong, could not create in Server', [__METHOD__, __LINE__]);
        }
        $this->apcuCache->addIfNotExists('messageActive', ['success' => 'Page activated successfully'], 600);
        return $this->redirectToRoute('camp.active');
    }


    /**
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @internal independent of running campaigns (landing-page, "babies") status a campaign can be archived if the status is active (only)
     *           in this case no microservice has to be called .
     *           #This method move campaign from active to archive
     *           Route No. 43,
     *           $app->get('/account/pages/{camp_id}/archive', 'CampaignController:getCampaignArchivedById')->setName('camp.archiveById');
     *           #Route( "/account/pages/{camp_id}/archive", name="camp.archiveById", methods={"GET"})
     * @Route( "/account/{useCase_type}/{camp_id}/archive", name="camp.archiveById", methods={"GET"})
     * @author   aki
     */
    public function setCampaignStatusToArchive(
        Request               $request,
        CampaignRepository    $campaignRepository,
        CampaignService       $campaignService,
        AuthenticationService $authenticationService,
        AuthenticationUtils   $authenticationUtils
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());

        if (empty($user)) {
            return $this->redirect('/');
        }

        $camp_id = $request->attributes->get('camp_id');
        $useCase_type = $request->attributes->get('useCase_type');
        $updateTo = 'archived';
        $userId = $user['user_id'];
        //updating the campaign status
        $updateStatus = $campaignRepository->updateCampaign($camp_id, StatusDefRepository::ARCHIVED, $userId);
        //redirecting to archive page
        if (is_bool($updateStatus) && ($updateStatus === false)) {
            $this->apcuCache->addIfNotExists('messageArchive', ['error' => 'Campaign could not be Archived, something went wrong'], 600);
            return $this->redirectToRoute('camp.archive');
        }
        $this->apcuCache->addIfNotExists('messageArchive', ['success' => 'Campaign archived successfully'], 600);
        if ($useCase_type === 'pages') {
            return $this->redirectToRoute('camp.archive');
        } else {
            return $this->redirectToRoute('webhook.archive');
        }
    }


    /**
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @deprecated
     * @internal only revised campaigns can be set to draft, independent of running campaigns status.
     *           Status draft is assigned to every new created campaign.
     *           Route No. 44,
     *           $app->get('/account/pages/{camp_id}/move', 'CampaignController:moveCampaignToDraft')->setName('camp.move');
     *           #Route( "/account/pages/{camp_id}/move", name="camp.move", methods={"GET"})
     * @Route( "/account/pages/{camp_id}/move", name="camp.move", methods={"GET"})
     */
    public function setCampaignStatusToDraft(
        Request               $request,
        CampaignRepository    $campaignRepository,
        CampaignService       $campaignService,
        AuthenticationService $authenticationService,
        AuthenticationUtils   $authenticationUtils
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            return $this->redirect('/');
        }
        $camp_id = $request->attributes->get('camp_id');
        $updateTo = 'draft';
        $user_id = $user['user_id'];
        $updateStatus = $campaignRepository->updateCampaign($camp_id, StatusDefRepository::DRAFT, $user_id);

        if ((is_bool($updateStatus) && ($updateStatus === false))) {
            $this->apcuCache->addIfNotExists('messageDraft', ['error' => 'Campaign could not be Drafted, something went wrong'], 600);
            return $this->redirectToRoute('camp.draft');
        }
        $this->apcuCache->addIfNotExists('messageDraft', ['success' => 'Campaign is drafted successfully'], 600);
        return $this->redirectToRoute('camp.draft');
    }


    /**
     * $app->get('/account/pages/{camp_id}/copy', 'CampaignController:postCopyCampaign')->setName('camp.postCopyCampaign');
     * @Route( "/account/pages/{camp_id}/copy", name="camp.postCopyCampaign", methods={"GET","POST"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @author Pradeep && AKI
     * @internal campaign-organizer copies an existing campaign to new URL
     */
    public function CopyCampaign(
        Request                    $request,
        CampaignRepository         $campaignRepository,
        CampaignService            $campaignService,
        ObjectRegisterRepository   $objectRegisterRepository,
        AuthenticationService      $authenticationService,
        AuthenticationUtils        $authenticationUtils,
        MIOStandardFieldRepository $mioStandardFieldRepository,
        CustomfieldsRepository     $customfieldsRepository
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            return $this->redirect('/');
        }
        $campaign_id = $request->get('camp_id');
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
        $settings = $campaignService->fetchSettingsForCopyCampaign($campaign_id, $projectObjectRegisterId);
        if ($request->isMethod('post')) {
            $token = $request->get('token');
            $campaign = $request->get('campaign');
            $oldCampaignDetails = $campaignService->getLaterVersionAndNameOfCampaign($settings['objectRegisterId']);
            //create new page
            $response = $this->forward('App\Controller\CampaignController::postCreateCampaignById',
                ['campaign' => $campaign,
                    'token' => $token,
                    'copy' => true,
                    'old_campaign_name' => $oldCampaignDetails['name'],
                    'old_campaign_version' => $oldCampaignDetails['version'],
                ]);
            return $this->redirectToRoute('camp.draft');
        }

        $params = [
            'user_id' => $user['user_id'],
            'company_id' => $user['company_id'],
            'account' => $user['company_name'],
            'company_name' => $user['company_account_no'],
        ];
        //get defined processhooks
        try {
            $processhooks = $this->processhookService->fetchProcesshooks($this->statusdefRepository::DEFINED,
                $user['company_id']);
        } catch (Exception $e) {
            $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able fetch processhooks'], 600);
        }
        if (isset($settings['processhooks'])) {
            $settings['processhooks'] = $campaignService->formatProcesshooksForPageWizard($processhooks,
                $settings['processhooks']);
        }
        $settings['customFieldList'] = $customfieldsRepository->getProjectCustomfields(
            $user['company_id'], UniqueObjectTypes::CUSTOMFIELDS);
        $settings['ecommerce_fields'] = $mioStandardFieldRepository->getECommerceFields();
        $settings['standard_fields'] = $mioStandardFieldRepository->getStandardFields();
        $campaign_params = array_merge($params, $settings);
        return $this->render('campaign/createcampaign.twig',
            ['campaign' => $campaign_params, 'current_user' => $user]);

    }


    /**
     * @Route( "/getstandardfields", name="leads.getStandardFields", methods={"GET"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @return Response
     * @author Pradeep
     */
    public function getStandardFields(Request $request, CampaignRepository $campaignRepository): Response
    {
        $standard_fields = $campaignRepository->getStandardFields();
        $response = new Response();
        $response->setContent(json_encode($standard_fields, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE, 512));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route( "/getcustomfields", name="campaign.getCustomFields", methods={"GET"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @return Response
     * @author   Pradeep
     * @internal campaign_id has to be provided eg:'/getcustomfields?campaign_id=1010'
     */
    public function getCustomFields(Request $request, CampaignRepository $campaignRepository): Response
    {
        $campaign_id = ($request->get('campaign_id') > 0) ? $request->get('campaign_id') : 0;
        $custom_fields = $campaignRepository->getCustomFieldsForCampaign($campaign_id);
        $response = new Response();
        $response->setContent(json_encode($custom_fields, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE, 512));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }


    /**
     * @Route( "/account/campaigns/checkNameOfCampaign/{Check_Name}/{campaign_id}", name="campaign.check", methods={"GET"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @return Response
     * @author   Aki
     * @internal campaign_id has to be provided eg:'/checkNameOfCampaign?campaign_id=1010'
     */
    public function checkNameOfCampaign(Request $request, CampaignRepository $campaignRepository): Response
    {
        $return = ['status' => true, 'message' => 'Avaliable'];
        $response = new Response();
        $check_name = $request->attributes->get('Check_Name');
        $camp_id = $request->attributes->get('campaign_id');
        //check name for space and -
        if ((false !== strpos($check_name, '-'))) {
            $return = ['status' => false, 'message' => 'Campaign name should not contain -'];
        } elseif (($check_name === trim($check_name) && strpos($check_name, ' ') !== false)) {
            $return = ['status' => false, 'message' => 'Campaign name should not contain spaces'];
        }
        if ($campaignRepository->isCampaignNameAvailable($check_name, $camp_id)) {
            $return = ['status' => false, 'message' => 'Campaign name already used'];
        }
        return $response->setContent(json_encode($return));
    }


    /**
     *
     * @Route( "/account/webhooks/active", name="webhook.active", methods={"GET"})
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param CampaignService $campaignService
     * @param ContactRepository $leadsRepository
     * @param UsersRepository $usersRepository
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return Response
     * @author aki
     */
    public function getWebhooksByStatusActive(
        AuthenticationUtils      $authenticationUtils,
        AuthenticationService    $authenticationService,
        CampaignService          $campaignService,
        ContactRepository        $leadsRepository,
        UsersRepository          $usersRepository,
        ObjectRegisterRepository $objectRegisterRepository
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $status = 'active';
        //Check for DPA acceptance
        if (!$usersRepository->verifyDPAStatusForUser($authenticationUtils->getLastUsername())) {
            $error = ['error' => 'Please accept the DPA to proceed further'];
            return $this->redirectToRoute('acc_dpa_accept', $error);
        }

        if (empty($user)) {
            return $this->redirect('/');
        }
        $useCaseType = 'webhook';
        $campaigns = $objectRegisterRepository->getCampaignsWithStatus($user['company_id'], UniqueObjectTypes::WEBHOOK, StatusDefRepository::ACTIVE);
        $campaignHasRecords = count($campaigns) !== 0;
        $camp_details = $campaignService->addObjectUniqueValueToCampaigns($campaigns);
        $camp_details = $campaignService->appendLinksForCampaigns($camp_details, 'webhooks', null);//campaign details with url
        $camp_details_with_conversions = $campaignService->addConversionsValueToCampaigns($camp_details,
            ObjectRegisterMetaRepository::MetaKeyWebHook);//campaign details with conversion value
        $camp_details_with_conversions[0]['account'] = $user['company_name'];
        //this is for status messages
        $this->getMessageStatus('messageActive', $camp_details);
        return $this->render('webhook/activeWebhook.twig', [
            'campaigns' => $camp_details_with_conversions,
            'current_user' => $user,
            'campaignHasRecords' => $campaignHasRecords,
        ]);
    }


    /**
     *
     * $app->get('/account/webhooks/create', 'CampaignController:getcreateWebhookById')->setName('webhook.createById');
     * #Route( "/account/webhooks/create", name="webhook.create", methods={"GET"})
     * @Route( "/account/webhooks/create", name="webhook.create", methods={"GET"})
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignRepository $campaignRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param ContactRepository $contactRepository
     * @param CustomfieldsRepository $customfieldsRepository
     * @return Response
     * @author Aki
     */
    public function getCreateWebhookById(
        AuthenticationService        $authenticationService,
        AuthenticationUtils          $authenticationUtils,
        CampaignRepository           $campaignRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        ContactRepository            $contactRepository,
        CustomfieldsRepository       $customfieldsRepository,
        MappingTypes                 $mappingType,
        ProcesshookService           $processhookService,
        StatusdefRepository          $statusdefRepository,
        CampaignService              $campaignService,
        MIOStandardFieldRepository   $mioStandardFieldRepository
    ): Response
    {
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);

        if (empty($user)) {
            return $this->redirect('/');
        }
        $company_id = $user['company_id'];
        $params = [
            'user_id' => $user['user_id'],
            'company_id' => $user['company_id'],
            'account' => $user['company_name'],
            'company_name' => $user['company_account_no'],
        ];
        //append custom fields from project
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
        if (!empty($projectObjectRegisterId)) {
            $params['customFieldList'] = $customfieldsRepository->getProjectCustomfields($user['company_id'],
                UniqueObjectTypes::CUSTOMFIELDS);
        }

        $params['ecommerce_fields'] = $mioStandardFieldRepository->getECommerceFields();
        $params['technical_fields'] = $mioStandardFieldRepository->getTechnicalFields();
        $params['standard_fields'] = $mioStandardFieldRepository->getStandardFields();

        $params['mapping']['dataStructure'] = $mappingType->getArray();
        //get defined processhooks
        try {
            $processhooks = $processhookService->fetchProcesshooks($statusdefRepository::DEFINED, $company_id);
            $params['processhooks'] = $campaignService->formatProcesshooksForWizard($processhooks, null);
        } catch (Exception $e) {
            $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able fetch processhooks'], 600);
        }

        return $this->render('webhook/createWebhook.twig', ['campaign' => $params, 'current_user' => $user]);
    }


    /**
     *
     * $app->post('/account/webhooks/create', 'CampaignController:postCreateWebhookById');
     * #Route( "/account/webhooks/create", name="webhook.postCreate", methods={"POST"})
     * @Route( "/account/webhooks/create", name="webhook.postCreate", methods={"POST"})
     * @param Request $request
     * @param CampaignRepository $campaign_repository
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignService $campaignService
     * @param ObjectRegisterRepository $ObjectRegisterRepository
     * @return Response
     * @author Aki
     */
    public function postCreateWebhook(
        Request                  $request,
        CampaignRepository       $campaign_repository,
        AuthenticationService    $authenticationService,
        AuthenticationUtils      $authenticationUtils,
        CampaignService          $campaignService,
        ObjectRegisterRepository $ObjectRegisterRepository,
        ProcesshookService       $processhookService
    ): Response
    {
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);
        $token = $request->get('token');
        if (empty($user) || !$this->isCsrfTokenValid('campaign-create', $token)) {
            return $this->redirect('/');
        }
        $campaign = $request->get('campaign');
        $campaign['company_id'] = $user['company_id'];
        $campaign['account'] = $user['company_name'];
        $campaign['user_id'] = $user['user_id'];
        $campaign['version'] = 1;
        $campaign['comment'] = empty($campaign['comment']) ? '' : $campaign['comment'];
        // Create Campaign URL
        $campaign['url'] = $campaign_repository->createCampaignURL($campaign['settings']['name'], str_replace(' ', '', $user['company_account_no']), $_ENV['WEBHOOK_DOMAIN']);
        $ObjectRegisterId = $ObjectRegisterRepository->createInitialObjectRegisterEntryforCampaign($campaign['company_id'],
            ObjectRegisterMetaRepository::MetaKeyWebHook, 'draft');
        if (!empty($ObjectRegisterId)) {
            // Create Campaign return campaign id.dd
            $campaign['id'] = $campaign_repository->createPageInDB($ObjectRegisterId, $campaign['settings']['name'], $campaign['company_id'], $campaign['url'],
                $campaign['user_id']);
            //append fields from project
            $projectObjectRegisterId = (int)$campaign_repository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];

            //store workflow settings
            if (!empty($campaign['processhooks'])) {
                $campaign['processhookStatus'] = $processhookService->saveWorkflow($campaign['id'], $campaign['processhooks']);
                if (is_null($campaign['processhookStatus'])) {
                    $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Workflow could not be saved'], 600);
                    return $this->redirectToRoute('camp.draft');
                }
            }

            // Store Settings in objectRegisterMeta
            if ($campaign['id'] <= 0 || !$campaignService->addCampaignSettings(ObjectRegisterMetaRepository::MetaKeyWebHook, $campaign, $ObjectRegisterId,
                    $projectObjectRegisterId)) {
                $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Default campaign settings not saved'], 600);
                return $this->redirectToRoute('camp.draft');
            }

            // Add customFields to objectRegister Meta.
            try {
                $campaignService->addCampaignCustomFields($campaign['company_id'], $ObjectRegisterId, $campaign['mapping']);
            } catch (JsonException $e) {
                $this->logger->error("Custom fields not added");
            }

            //create campaign in server
            $campaign['usecase_type'] = 'webhook';
            if (!$campaignService->createCampaignInServer(
                $this->webhookOrganizer,
                $campaign['settings']['name'],
                $campaign['id'], $campaign['company_id'],
                $campaign['usecase_type'], str_replace(' ', '', $campaign['account'])))
            {
                $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able to create webhook in server please try again'], 600);
                return $this->redirectToRoute('webhook.draft');
            }
            $this->apcuCache->addIfNotExists('messageDraft', ['success' => 'Webhook created successfully'], 600);
            return $this->redirectToRoute('webhook.draft');
        } else {
            $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able to create page because of object register'], 600);
            return $this->redirectToRoute('webhook.draft');
        }
    }


    /**
     *
     * @Route( "/account/webhooks/draft", name="webhook.draft", methods={"GET"})
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @param CampaignService $campaignService
     * @return Response
     * @author  aki
     */
    public function getWebhooksByStatusDraft(
        AuthenticationUtils      $authenticationUtils,
        AuthenticationService    $authenticationService,
        ObjectRegisterRepository $objectRegisterRepository,
        CampaignService          $campaignService
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $status = 'draft';
        $companyId = $user['company_id'];
        $useCaseType = 'webhook';
        $campaigns = $objectRegisterRepository->getCampaignsWithStatus($companyId, UniqueObjectTypes::WEBHOOK, StatusDefRepository::DRAFT);
        $campaignHasRecords = (count($campaigns) === 0) ? false : true;
        $camp_details = $campaignService->addObjectUniqueValueToCampaigns($campaigns);
        $camp_details = $campaignService->appendLinksForCampaigns($camp_details, 'webhooks', null);
        $camp_details[0]['account'] = $user['company_name'];
        //this is for status messages
        $this->getMessageStatus('messageDraft', $camp_details);
        return $this->render('webhook/draftWebhook.twig', ['campaigns' => $camp_details, 'current_user' => $user, 'campaignHasRecords' => $campaignHasRecords]);
    }


    /**
     *
     * Route No. 32,
     * $app->get('/account/webhooks/archive', 'CampaignController:getCampaignsByStatusArchive')->setName('webhook.archive');
     * #Route( "/account/webhooks/archive", name="webhook.archive", methods={"GET"})
     * @Route( "/account/webhooks/archive", name="webhook.archive", methods={"GET"})
     * @param AuthenticationUtils $authenticationUtils
     * @param AuthenticationService $authenticationService
     * @param CampaignService $campaignService
     * @param ObjectRegisterRepository $objectRegisterRepository
     * @return Response
     * @author aki
     */
    public function getWebhookByStatusArchive(
        AuthenticationUtils      $authenticationUtils,
        AuthenticationService    $authenticationService,
        CampaignService          $campaignService,
        ObjectRegisterRepository $objectRegisterRepository
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $status = 'archive';
        $companyId = $user['company_id'];
        $useCaseType = 'webhook';
        $campaigns = $objectRegisterRepository->getCampaignsWithStatus($companyId, UniqueObjectTypes::WEBHOOK, StatusDefRepository::ARCHIVED);
        $campaignHasRecords = (count($campaigns) === 0) ? false : true;
        $camp_details = $campaignService->appendLinksForCampaigns($campaigns, 'webhooks', null);
        $camp_details[0]['account'] = $user['company_name'];
        //this is for status messages
        $this->getMessageStatus('messageArchive', $camp_details);
        return $this->render('webhook/archiveWebhook.twig',
            ['campaigns' => $camp_details, 'current_user' => $user, 'campaignHasRecords' => $campaignHasRecords]);
    }


    /**
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CompanyRepository $companyRepository
     * @param LoggerInterface $logger
     * @param CampaignService $campaignServices
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param CampaignService $campaignService
     * @return Response
     * @internal campaigns status can only be changed from draft to active, independent of running campaigns status
     * @Route( "/account/webhooks/{camp_id}/activate", name="webhook.activateById", methods={"GET"})
     * @author   aki
     */
    public function setWebhookStatusToActive(
        Request               $request,
        CampaignRepository    $campaignRepository,
        CompanyRepository     $companyRepository,
        LoggerInterface       $logger,
        CampaignService       $campaignServices,
        AuthenticationService $authenticationService,
        AuthenticationUtils   $authenticationUtils,
        CampaignService       $campaignService
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());

        if (empty($user)) {
            return $this->redirect('/');
        }

        $params = ['user_id' => $user['user_id'], 'company_id' => $user['company_id']];
        $company_name = str_replace(' ', '', $user['company_name']);
        $camp_id = $request->attributes->get('camp_id');
        if (!$campaignService->archivePreviousVersionsOfActiveCampaign($camp_id)) {
            $logger->critical('something wrong with Active version of campaign', [__METHOD__, __LINE__]);
        }

        $updateTo = 'active';
        $user_id = $params['user_id'];
        if (!$campaignRepository->updateCampaign($camp_id, StatusDefRepository::ACTIVE, $user_id)) {
            $logger->critical('Something went wrong, Could not activate the campaign', [__METHOD__, __LINE__]);
        }
        $camp_details = $campaignRepository->getCampaignDetailsById($camp_id);
        //add campaign settings to campaign in campaign server
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
        $campaignSettings = $campaignService->getCampaignSettings($camp_id, $projectObjectRegisterId, ObjectRegisterMetaRepository::MetaKeyWebHook);
        //add additional parameters for camapaign settings
        $additionalSettings = $campaignService->addBasePayLoad($camp_id, $user['company_id']);
        $campaignSettings = array_merge($campaignSettings, $additionalSettings);
        $campaignSettings = json_encode($campaignSettings);
        $campaignVersion = $campaignRepository->getVersionFromCampaignId($camp_id);
        //This is calling Ms camp
        if (!$campaignServices->activateCampaignInServer($this->webhookOrganizer, $camp_details['name'], (int)$campaignVersion, $user['company_account_no'],
            $campaignSettings)) {
            $logger->critical('Something went wrong, could not create in Server', [__METHOD__, __LINE__]);
        }

        $this->apcuCache->addIfNotExists('messageActive', ['success' => 'Campaign activated successfully'], 600);
        return $this->redirectToRoute('webhook.active');
    }


    /**
     *
     * @Route( "/account/{campaign_type}/{camp_id}/revise", name="campaign.revise", methods={"GET","POST"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param LoggerInterface $logger
     * @param AuthenticationService $authenticationService
     * @param CampaignService $campaignService
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param MappingTypes $mappingType
     * @param ContactRepository $contactRepository
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @throws JsonException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @internal This method revises page and webhooks - also called start wizard in UI but revises the page
     * and is different from start wizard in draft
     * @author aki
     */
    public function reviseById(
        Request $request,
        CampaignRepository $campaignRepository,
        LoggerInterface $logger,
        AuthenticationService $authenticationService,
        CampaignService $campaignService,
        CustomfieldsRepository $customfieldsRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        MappingTypes $mappingType,
        ContactRepository $contactRepository,
        AuthenticationUtils $authenticationUtils,
        ProcesshookService $processhookService,
        StatusDefRepository $statusdefRepository,
        WorkflowRepository $workflowRepository,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        MIOStandardFieldRepository $mioStandardFieldRepository,
        JotFormService $jotFormService
    ): Response {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            return $this->redirect('/');
        }
        $campaign_id = $request->get('camp_id');
        $campaignType = $request->get('campaign_type');
        $debug[ '$user' ] = $user;
        $debug[ '$campaign_id' ] = $campaign_id;
        $debug[ '$campaignType' ] = $campaignType;

        $settings = $campaignService->fetchSettingsForStartWizard($campaign_id, $campaignType);
        $debug[ '$settings' ] = $settings;
        $logger->info('revise : setting ' . json_encode($settings, true));
        //assign meta key as page or webhook
        if ($campaignType === "pages") {
            $metaKey = ObjectRegisterMetaRepository::MetaKeyPage;
            $organizer = $this->campaignOrganizer;
        } elseif ($campaignType === "webhooks") {
            $metaKey = ObjectRegisterMetaRepository::MetaKeyWebHook;
            $organizer = $this->webhookOrganizer;
        }

        $debug[ '$metaKey' ] = $metaKey;
        $debug[ '$organizer' ] = $organizer;
        //post request
        if ($request->isMethod('post')) {
            $campaign = $request->get('campaign');
            //append the campaign id
            $campaign[ 'id' ] = $campaign_id;
            $campaign[ 'company_id' ] = $user[ 'company_id' ];
            $campaign[ 'account' ] = $user[ 'company_name' ];
            $campaign[ 'user_id' ] = $user[ 'user_id' ];
            //add missing parameters
            $campaign = $campaignService->addMissingParametersToCampaign($campaign, $settings);

            $debug[ '$campaign' ] = $campaign;

            $logger->info('revise : campaign ' . json_encode($campaign, true));
            //format page with buttons
            if (isset($campaign[ 'page_flow' ][ 'buttons' ])) {
                $campaign[ 'page_flow' ] = $this->campaignService->formatPageflowButtons($campaign[ 'page_flow' ]);
            }
            $campaignSettings = json_encode($campaign);

            $debug[ '$campaignSettings' ] = $campaignSettings;
            //create new campaign in the DB
            $newCampaign = $campaignService->reviseCampaign($campaign_id, $campaignSettings, $metaKey, $settings);
            $debug[ '$newCampaign' ] = $newCampaign;
            if (empty($newCampaign)) {
                $logger->critical('Something went wrong, Could not create new version for' . $campaignType,
                    [__METHOD__, __LINE__]);
                $this->apcuCache->addIfNotExists('messageDraft',
                    ['error' => 'Something went wrong, Could not create new version for' . $campaignType], 600);
                if ($campaignType === "pages") {
                    return $this->redirectToRoute('camp.draft');
                } elseif ($campaignType === "webhooks") {
                    return $this->redirectToRoute('webhook.draft');
                }
            }

            // Add customFields to objectRegister Meta.
            $campaignService->addCampaignCustomFields($campaign[ 'company_id' ], $newCampaign[ 'ObjectRegisterId' ],
                $campaign[ 'mapping' ]);
            if (!$jotFormService->addJotFormConfig($campaign[ 'objectRegisterId' ],
                $newCampaign[ 'ObjectRegisterId' ])) {
                $this->logger->critical('Failed to update JotForm config', [__METHOD__, __LINE__]);
            }
            // revise campaign in server
            if (!$campaignService->reviseCampaignInServer(
                $this->campaignOrganizer,
                $newCampaign[ 'name' ],
                (int)$newCampaign[ 'oldVersion' ],
                (int)$newCampaign[ 'newVersion' ],
                $newCampaign[ 'CampaignId' ],
                $campaign[ 'company_id' ]
            )) {
                $logger->critical('Unable to revise ' . $campaignType . ' in Server', [__METHOD__, __LINE__]);
            }

            if ($campaignType === "pages") {
                //Server related calls ms-camp
                //Add campaign settings to the server
                $projectObjectRegisterId = (int)$this->campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
                if (!$this->campaignService->addOrUpdateSettingsInServer(
                    $newCampaign['CampaignId'],
                    $user['company_id'],
                    $projectObjectRegisterId,
                    $campaign['settings']['name'],
                    $user['company_account_no'],
                    $organizer
                )) {
                    $this->logger->critical('Something went wrong, could not create in Server', [__METHOD__, __LINE__]);
                }
                //check for difference in settings
                $newServices = $campaignService->checkNewSettings($campaign, $settings);
                if (isset($newServices)) {
                    //add pages
                    if (isset($newServices['newPages'])) {
                        if (!$campaignService->createFlowControlPagesInServer(
                            $this->campaignOrganizer,
                            $campaign['settings']['name'],
                            $user['company_account_no'],
                            (int)$newCampaign['newVersion'],
                            $newServices['newPages']
                        )) {
                            $this->apcuCache->addIfNotExists('messageDraf',
                                ['error' => 'Could not able to create flow control additional pages in server please try again'],
                                600);
                            return $this->redirectToRoute('camp.draft');
                        }
                    }
                    //add splittest
                    if (isset($newServices['difference_splittest']) && $newServices['difference_splittest'] == 'true') {
                        if (!$campaignService->createSplittestFolder($this->campaignOrganizer,
                            $campaign['settings']['name'],
                            $user['company_account_no'],
                            (int)$newCampaign['newVersion'],
                            $campaign['splittest']['name']
                        )) {
                            $this->apcuCache->addIfNotExists('messageDraf',
                                ['error' => 'Could not able to create splittestPages in server please try again'], 600);
                            return $this->redirectToRoute('camp.draft');
                        }
                    }

                }
                $this->apcuCache->addIfNotExists('messageDraft', ['success' => 'page revised successfully'], 600);  // jsr: what for?
                return $this->redirectToRoute('camp.draft');
            } elseif ($campaignType === "webhooks") {
                $this->apcuCache->addIfNotExists('messageDraft', ['success' => 'Webhook revised saved successfully'], 600);  // jsr: what for?
                return $this->redirectToRoute('webhook.draft');
            }
            //end of post request
        }
        //append additional fields from required for wizard
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user[ 'company_id' ])[ 'id' ];
        $debug[ '$projectObjectRegisterId' ] = $projectObjectRegisterId;
        if (!empty($projectObjectRegisterId)) {
            $settings['customFieldList'] = $customfieldsRepository->getProjectCustomfields($user['company_id'],
                UniqueObjectTypes::CUSTOMFIELDS);
            /*            $settings['customFieldList'] = $objectRegisterMetaRepository->getSavedSettings($projectObjectRegisterId,
                            ObjectRegisterMetaRepository::MetaKeyProject)['customfieldlist'];*/
        }
        $settings["company_name"] = $user["company_account_no"];

        if ($campaignType === "pages") {
            $settings['ecommerce_fields'] = $mioStandardFieldRepository->getECommerceFields();
            $settings['standard_fields'] = $mioStandardFieldRepository->getStandardFields();
        } elseif ($campaignType === "webhooks") {
            $settings['ecommerce_fields'] = $mioStandardFieldRepository->getECommerceFields();
            $settings['technical_fields'] = $mioStandardFieldRepository->getTechnicalFields();
            $settings['standard_fields'] = $mioStandardFieldRepository->getStandardFields();

            //automatically generated webhooks from MVP shop plugin
            //todo:fix the data structure in AIO
            if (gettype($settings['mapping']) === "string") {
                $settings = $campaignService->chageDataStructure($settings);
            }
        }
        $debug[ '$settings' ] = $settings;
        //get defined processhooks
        try {
            $processhooks = $processhookService->fetchProcesshooks($statusdefRepository::DEFINED, $user['company_id']);
        } catch (Exception $e) {
            $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able fetch processhooks'], 600);
        }
        $debug[ '$processhooks' ] = $processhooks;
        $settings[ 'mapping' ][ 'dataStructure' ] = $mappingType->getArray();
        $debug[ '$settings' ] = $settings;
        if ($campaignType === "pages") {
            if (isset($settings['processhooks'])) {
                $settings[ 'processhooks' ] = $campaignService->formatProcesshooksForPageWizard($processhooks,
                    $settings[ 'processhooks' ]);
            }
            return $this->render('campaign/createcampaign.twig', ['campaign' => $settings, 'current_user' => $user]);
        } elseif ($campaignType === "webhooks") {
            if (isset($settings[ 'processhooks' ])) {
                $settings[ 'processhooks' ] = $campaignService->formatProcesshooksForWizard($processhooks,
                    $settings[ 'processhooks' ]);
            }
            $debug[ '$settings' ] = $settings;
            return $this->render('webhook/createWebhook.twig', ['campaign' => $settings, 'current_user' => $user]);
        }
        $debug[ '$settings' ] = $settings;
        $this->logger->info('REVISEBYID:DEBUG', [$debug, __METHOD__, __LINE__]);
        return $this->redirectToRoute('camp.draft');
    }

    /**
     *
     * @Route( "/account/webhooks/{camp_id}/downloadCode", name="webhook.Download_code", methods={"GET", "POST"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CompanyRepository $companyRepository
     * @param CampaignService $campaignService
     * @param LoggerInterface $logger
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @throws JsonException
     * @author aki
     * @internal This controller generates code for webhooks
     */
    public function downloadCodeForWebhook(
        Request               $request,
        CampaignRepository    $campaignRepository,
        CompanyRepository     $companyRepository,
        CampaignService       $campaignService,
        LoggerInterface       $logger,
        AuthenticationService $authenticationService,
        AuthenticationUtils   $authenticationUtils
    ): Response
    {
        $userEmail = $authenticationUtils->getLastUsername();
        $user = $authenticationService->getUserParametersFromCache($userEmail);
        if (empty($user)) {
            return $this->redirect('/');
        }
        $companyAccountNumber = $user['company_id'];
        $campaignId = $request->attributes->get('camp_id');
        $campaign_name = $campaignRepository->getCampaignNameWithId($campaignId);
        if ($request->isMethod('post')) {
            //get the content
            $content = $request->getContent();
            $params = $campaignService->convertRequestToArray($content);
            $programingLanguage = $params['programingLanguage'];
            $templateData = $this->campaignService->generateDataForTemplate((int)$campaignId);
            $templateDataForHtml = $this->campaignService->formatTemplateData($templateData);
            if ($programingLanguage == 'JavaScript') {
                // Provide a name for your file with extension
                $filename = $campaign_name . '.js';
                // The dynamically created content of the file
                $fileContent = $this->renderView('webhook/download/javascript.twig', ['data' => $templateData]);
            } elseif ($programingLanguage == 'PHPCURL') {
                // Provide a name for your file with extension
                $filename = $campaign_name . '.php';
                // The dinamically created content of the file
                $fileContent = $this->renderView('webhook/download/php.twig', ['data' => $templateData]);
            } elseif ($programingLanguage == 'HTML/JavaScript') {
                //Create new Zip file and all the required files
                $projectDirectory = $this->getParameter('kernel.project_dir');
                $webhookFilesLocation = $projectDirectory . '/files/webhook';
                $zipFile = $campaign_name . ".zip";

                // Create recursive directory iterator
                $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($webhookFilesLocation),
                    RecursiveIteratorIterator::LEAVES_ONLY);
                //new zip file
                $zip = new ZipArchive;
                if ($zip->open($zipFile, ZipArchive::CREATE) === true) {
                    //Add all the library files
                    foreach ($files as $file) {
                        // Skip directories (they would be added automatically)
                        if (!$file->isDir()) {
                            // Get real and relative path for current file
                            $filePath = $file->getRealPath();
                            $relativePath = substr($filePath, strlen($webhookFilesLocation) + 1);

                            // Add current file to archive
                            $zip->addFile($filePath, $relativePath);
                        }
                        //Add html file
                        $renderedFormHtml = $this->renderView('webhook/download/html.twig',
                            ['data' => $templateDataForHtml]);
                        $zip->addFromString('index.html', $renderedFormHtml);
                        //Add js file
                        $renderedJs = $this->renderView('webhook/download/scriptForHtml.twig',
                            ['data' => $templateData]);
                        $zip->addFromString('script.js', $renderedJs);
                    }
                    // All files are added, so close the zip file.
                    $zip->close();
                }
                //adding headers for the file
                $response = new Response(file_get_contents($zipFile));
                $response->headers->set('Content-Type', 'application/zip');
                $response->headers->set('Content-Disposition', 'attachment;filename="' . $zipFile . '"');
                $response->headers->set('Content-length', filesize($zipFile));
                $response->headers->set('Content-Transfer-Encoding', 'binary');

                unlink($zipFile);
                return $response;
            } elseif ($programingLanguage == 'HTML/PHP') {


                $projectDirectory = $this->getParameter('kernel.project_dir');
                $logger->info('ProjectDirectory: ' . $projectDirectory);
                $webhookFilesLocation = $projectDirectory . '/files/webhook';
                $zipFile = $campaign_name . ".zip";

                // Create recursive directory iterator
                $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($webhookFilesLocation),
                    RecursiveIteratorIterator::LEAVES_ONLY);
                //new zip file
                $zip = new ZipArchive;
                if ($zip->open($zipFile, ZipArchive::CREATE) === true) {
                    //Add all the library files
                    foreach ($files as $file) {
                        // Skip directories (they would be added automatically)
                        if (!$file->isDir()) {
                            // Get real and relative path for current file
                            $filePath = $file->getRealPath();
                            $relativePath = substr($filePath, strlen($webhookFilesLocation) + 1);

                            // Add current file to archive
                            $zip->addFile($filePath, $relativePath);
                        }
                        $logger->info('Template Data ' . json_encode($templateDataForHtml));

                        //Add html file
                        $renderedFormHtml = $this->renderView(
                            'webhook/download/html-php/html.twig',
                            ['hdata' => $templateDataForHtml]
                        );
                        $zip->addFromString('index.html', $renderedFormHtml);

                        //Add js file
                        $renderedPHP = $this->renderView(
                            'webhook/download/html-php/php.twig',
                            ['pdata' => $templateData]);
                        $zip->addFromString('submit.php', $renderedPHP);
                    }
                    // All files are added, so close the zip file.
                    $zip->close();
                }
                //adding headers for the file
                $response = new Response(file_get_contents($zipFile));
                $response->headers->set('Content-Type', 'application/zip');
                $response->headers->set('Content-Disposition', 'attachment;filename="' . $zipFile . '"');
                $response->headers->set('Content-length', filesize($zipFile));
                $response->headers->set('Content-Transfer-Encoding', 'binary');

                unlink($zipFile);
                return $response;
            } else if ($programingLanguage === 'Json') {
                // Provide a name for your file with extension
                $filename = $campaign_name . '.json';
                // The dynamically created content of the file
                $fileContent = $this->renderView('webhook/download/json.twig', ['data' => $templateData]);
            }else if ($programingLanguage === 'Cookie Consent'){
                $filename = $campaign_name.'_cookie_consent' . '.html';
                // The dynamically created content of the file
                $fileContent = $this->renderView('webhook/download/cookieConsentTemplate.twig', ['data' => $templateData]);
            }
            else {
                $filename = $campaign_name . '.txt';
                // The dinamically created content of the file
                $fileContent = "Please select programing Language";
            }
            // Return a response with a specific content
            $response = new Response($fileContent);
            // Create the disposition of the file
            $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
            // Set the content disposition
            $response->headers->set('Content-Disposition', $disposition);
            // Dispatch request
            return $response;
        }
        $code_details = [];
        return $this->render('webhook/downloadCode.twig', ['campaigns' => $code_details, 'current_user' => $user]);
    }


    /**
     *
     * @Route( "page/createFlow", methods={"GET", "POST"})
     * @Route( "account/pages/{camp_id}/createFlow", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     * CampaignService $campaignService,
     * @author aki
     */

    public function createPageFlow2(
        Request               $request,
        AuthenticationUtils   $authenticationUtils,
        AuthenticationService $authenticationService
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $req_data = $request->get('json');
        $de_data = base64_decode($req_data);// Decode data
        $settings = json_decode($de_data, true, 512, JSON_OBJECT_AS_ARRAY);// Convert Json to Arr
        $included_template_content = $this->renderView('campaign/create/pageFlow.twig', ['c_set' => $settings, 'current_user' => $user]);

        return new Response($included_template_content);
    }


    /**
     * $app->get('account/{campaign_type}/{camp_id}/startwizard', 'CampaignController:startwizard')->setName('camp.startwizard');
     * @Route( "account/{campaign_type}/{camp_id}/startwizard", name="camp.startwizard", methods={"GET", "POST"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param CustomfieldsRepository $customfieldsRepository
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param ContactRepository $contactRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param ProcesshookService $processhookService
     * @param StatusDefRepository $statusdefRepository
     * @param StandardFieldMappingRepository $standardFieldMappingRepository
     * @param MappingTypes $mappingType
     * @param MIOStandardFieldRepository $mioStandardFieldRepository
     * @return Response
     * @throws Exception
     * @throws JsonException
     * @throws \Doctrine\DBAL\Exception
     * @author Aki
     * @internal Manages all starts wizards
     * todo: make separate routs for get and post and if possible for page and webhooks
     */
    public
    function startWizard(
        Request                        $request,
        CampaignRepository             $campaignRepository,
        CampaignService                $campaignService,
        CustomfieldsRepository         $customfieldsRepository,
        AuthenticationService          $authenticationService,
        AuthenticationUtils            $authenticationUtils,
        ContactRepository              $contactRepository,
        ObjectRegisterMetaRepository   $objectRegisterMetaRepository,
        ProcesshookService             $processhookService,
        StatusDefRepository            $statusdefRepository,
        StandardFieldMappingRepository $standardFieldMappingRepository,
        MappingTypes                   $mappingType,
        MIOStandardFieldRepository     $mioStandardFieldRepository
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $debug[ '$user' ] = $user;
        if (empty($user)) {
            return $this->redirect('/');
        }
        $campaign_id = $request->get('camp_id');
        $campaignType = $request->get('campaign_type');
        $settings = $campaignService->fetchSettingsForStartWizard($campaign_id, $campaignType);
        $debug[ '$campaign_id' ] = $campaign_id;
        $debug[ '$campaignType' ] = $campaignType;
        $debug[ '$settings' ] = $settings;
        //assign meta key as page or webhook
        if ($campaignType === "pages") {
            $metaKey = ObjectRegisterMetaRepository::MetaKeyPage;
            $organizer = $_ENV[ 'CAMPAIGN_SERVICE' ];
        } elseif ($campaignType === "webhooks") {
            $metaKey = ObjectRegisterMetaRepository::MetaKeyWebHook;
            $organizer = $_ENV[ 'WEBHOOK_SERVICE' ];
        }

        $debug[ '$metaKey' ] = $metaKey;
        $debug[ '$organizer' ] = $organizer;
        //post request
        if ($request->isMethod('post')) {
            $campaign = $request->get('campaign');
            //append the campaign id
            $campaign[ 'id' ] = $campaign_id;
            $campaign[ 'company_id' ] = $user[ 'company_id' ];
            $campaign[ 'account' ] = $user[ 'company_name' ];
            $campaign[ 'user_id' ] = $user[ 'user_id' ];
            //add missing parameters
            $campaign = $campaignService->addMissingParametersToCampaign($campaign, $settings);
            $debug[ '$campaign' ] = $campaign;
            //format page with buttons
            if (isset($campaign[ 'page_flow' ][ 'buttons' ])) {
                $campaign[ 'page_flow' ] = $this->campaignService->formatPageflowButtons($campaign[ 'page_flow' ]);
            }

            //update workflow
            if (isset($campaign[ 'processhooks' ])) {
                if (($settings[ 'processhooks' ] !== $campaign[ 'processhooks' ])) {

                    $updateStatus = $processhookService->updateWorkflowForStartWizard($campaign);
                    if (is_null($updateStatus)) {
                        $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Workflow could not be Updated'],
                            600);
                    }
                    $campaign[ 'processhookStatus' ] = $updateStatus;
                }
            }
            $debug[ '$campaign' ] = $campaign;
            //Store Settings in objectRegisterMeta
            if (!$campaignService->updateSettings($metaKey, $campaign, $settings['objectRegisterId'])) {
                $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Default campaign settings not saved'], 800);
                return $this->redirectToRoute('camp.draft');
            }
            // Add customFields to objectRegister Meta.
            $campaignService->addCampaignCustomFields($campaign['company_id'], $settings['objectRegisterId'], $campaign['mapping']);
            // add jotformfields while
            if ($campaignType === "pages") {
                //todo:check for difference in settings
                //todo:add if new page is added
                //todo:change splittest is configurations and pages
                //update settings to draft page
                $projectObjectRegisterId = (int)$this->campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
                if (!$this->campaignService->addOrUpdateSettingsInServer(
                    $campaign['id'],
                    $user['company_id'],
                    $projectObjectRegisterId,
                    $campaign['settings']['name'],
                    $user['company_account_no'],
                    $organizer
                )) {
                    $this->logger->critical('Something went wrong, could not create in Server', [__METHOD__, __LINE__]);
                }
                $this->apcuCache->addIfNotExists('mesageDraft', ['success' => 'Page settings saved successfully'], 600);
                return $this->redirectToRoute('camp.draft');
            } elseif ($campaignType === "webhooks") {
                $this->apcuCache->addIfNotExists('messageDraft', ['success' => 'Webhook settings saved successfully'], 600);
                return $this->redirectToRoute('webhook.draft');
            }
        }
        //append additional fields from required for wizard
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user[ 'company_id' ])[ 'id' ];
        $debug[ '$projectObjectRegisterId' ] = $projectObjectRegisterId;
        if (!empty($projectObjectRegisterId)) {
            $settings['customFieldList'] = $customfieldsRepository->getProjectCustomfields($user['company_id'],
                UniqueObjectTypes::CUSTOMFIELDS);
            // $objectRegisterMetaRepository->getSavedSettings($projectObjectRegisterId,
            // ObjectRegisterMetaRepository::MetaKeyProject)['customfieldlist'];
        }
        $settings[ "company_name" ] = $user[ "company_account_no" ];
        $debug[ '$settings' ] = $settings;
        if ($campaignType === "pages") {
            $settings['ecommerce_fields'] = $mioStandardFieldRepository->getECommerceFields();
            $settings['standard_fields'] = $mioStandardFieldRepository->getStandardFields();
        } elseif ($campaignType === "webhooks") {
            $settings['ecommerce_fields'] = $mioStandardFieldRepository->getECommerceFields();
            $settings['technical_fields'] = $mioStandardFieldRepository->getTechnicalFields();
            $settings['standard_fields'] = $mioStandardFieldRepository->getStandardFields();
            //automatically generated webhooks from MVP shop plugin
            //todo:fix the data structure in AIO
            if (gettype($settings['mapping']) === "string") {
                $settings = $campaignService->chageDataStructure($settings);
            }
        }
        $settings[ 'mapping' ][ 'dataStructure' ] = $mappingType->getArray();
        $debug[ '$settings' ] = $settings;
        //get defined processhooks
        try {
            $processhooks = $processhookService->fetchProcesshooks($statusdefRepository::DEFINED, $user['company_id']);
        } catch (Exception $e) {
            $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able fetch processhooks'], 600);
        }
        $debug[ '$processhooks' ] = $processhooks;

        if ($campaignType === "pages") {
            if (isset($settings['processhooks'])) {
                $settings['processhooks'] = $campaignService->formatProcesshooksForPageWizard($processhooks, $settings['processhooks']);
            }
            //render wizard for page
            return $this->render('campaign/createcampaign.twig', ['campaign' => $settings, 'current_user' => $user]);
        } elseif ($campaignType === "webhooks") {
            if (isset($settings[ 'processhooks' ])) {
                $settings[ 'processhooks' ] = $campaignService->formatProcesshooksForWizard($processhooks,
                    $settings[ 'processhooks' ]);
            }
            //render wizard with data for webhook
            return $this->render('webhook/createWebhook.twig', ['campaign' => $settings, 'current_user' => $user]);
        }
        $debug[ '$settings' ] = $settings;
        $this->logger->info('STARTWIZARD:DEBUG', [$debug, __METHOD__, __LINE__]);
        return $this->redirectToRoute('webhook.draft');
    }


    /**
     * @Route( "account/webhooks/{webhook_id}/copy", name="camp.copywebhook", methods={"GET", "POST"})
     * @param Request $request
     * @param CampaignRepository $campaignRepository
     * @param CampaignService $campaignService
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @param ContactRepository $contactRepository
     * @param ObjectRegisterMetaRepository $objectRegisterMetaRepository
     * @param ProcesshookService $processhookService
     * @param StatusDefRepository $statusdefRepository
     * @param MappingTypes $mappingType
     * @return Response
     * @throws JsonException
     * @throws \Doctrine\DBAL\Exception
     */
    public
    function CopyWebhook(
        Request                      $request,
        CampaignRepository           $campaignRepository,
        CampaignService              $campaignService,
        AuthenticationService        $authenticationService,
        AuthenticationUtils          $authenticationUtils,
        ContactRepository            $contactRepository,
        ObjectRegisterMetaRepository $objectRegisterMetaRepository,
        ProcesshookService           $processhookService,
        StatusDefRepository          $statusdefRepository,
        MappingTypes                 $mappingType,
        MIOStandardFieldRepository   $mioStandardFieldRepository
    ): Response
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        if (empty($user)) {
            return $this->redirect('/');
        }
        $campaign_id = $request->get('webhook_id');
        $campaignType = 'webhooks';
        $settings = $campaignService->fetchSettingsForStartWizard($campaign_id, $campaignType);
        //post request
        if ($request->isMethod('post')) {
            $token = $request->get('token');
            $campaign = $request->get('campaign');
            //create new webhook
            $response = $this->forward('App\Controller\CampaignController::postCreateWebhook', ['campaign' => $campaign, 'token' => $token]);
            return $this->redirectToRoute('webhook.draft');
        }
        //append additional fields from required for wizard
        $projectObjectRegisterId = $campaignRepository->getProjectObjectRegisterUOIDbyCompanyId($user['company_id'])['id'];
        if (!empty($projectObjectRegisterId)) {
            $settings['customFieldList'] = $objectRegisterMetaRepository->getJsonMetaValueAsArray($projectObjectRegisterId,
                ObjectRegisterMetaRepository::MetaKeyProject)['customfieldlist'];
        }
        $settings["company_name"] = $user["company_account_no"];
        //get fields
        $settings['ecommerce_fields'] = $mioStandardFieldRepository->getECommerceFields();
        $settings['technical_fields'] = $mioStandardFieldRepository->getTechnicalFields();
        $settings['standard_fields'] = $mioStandardFieldRepository->getStandardFields();
        //automatically generated webhooks from MVP shop plugin
        //todo:fix the data structure in AIO
        if (is_string($settings['mapping'])) {
            $settings = $campaignService->chageDataStructure($settings);
        }
        $settings['mapping']['dataStructure'] = $mappingType->getArray();
        //add default name
        $settings['settings']['name'] .= 'Copy';
        //get defined processhooks
        try {
            $processhooks = $processhookService->fetchProcesshooks($statusdefRepository::DEFINED, $user['company_id']);
        } catch (Exception $e) {
            $this->apcuCache->addIfNotExists('messageDraf', ['error' => 'Could not able fetch processhooks'], 600);
        }
        if (isset($settings['processhooks'])) {
            $settings['processhooks'] = $campaignService->formatProcesshooksForWizard($processhooks, $settings['processhooks']);
        }
        return $this->render('webhook/createWebhook.twig', ['campaign' => $settings, 'current_user' => $user]);
    }

    /**
     *
     * @Route( "page/download/{page_id}/{page_type}", name="page.download", methods={"GET"})
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\Exception|JsonException
     * @author aki
     * @internal This controller downloads code for webhooks
     */
    public function downloadPage(Request $request): Response
    {
        $pageType = $request->get('page_type');
        $pageId = $request->get('page_id');

        //get details for download
        $downloadPageDetails = $this->campaignService->getDetailsForPageDownload($pageType, (int)$pageId);
        $downloadDir = $this->getParameter('page_download_dir');

        //get page settings
        $pageSettings = $this->campaignService->getPageSettingsWithId((int)$pageId, ObjectRegisterMetaRepository::MetaKeyPage);

        //get the zip file from mscamp
        $zipFile = $this->campaignService->downloadPage($this->campaignOrganizer, $pageSettings, $downloadPageDetails, $downloadDir);

        if (!is_null($zipFile)) {
            // This should return the file to the browser as response
            $response = new BinaryFileResponse($zipFile);

            // To generate a file download, you need the mimetype of the file
            $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

            // Set the mimetype with the guesser or manually
            if ($mimeTypeGuesser->isGuesserSupported()) {
                // Guess the mimetype of the file according to the extension of the file
                $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($zipFile));
            }
            $fileName = basename($zipFile);
            // Set content disposition inline of the file
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $fileName);
            //delete the file
            @unlink($fileName);
        }
        return $response;
    }


    /**
     *
     * @Route( "page/upload/{page_id}/{page_type}", name="page.upload", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @throws \Doctrine\DBAL\Exception
     * @author aki
     * @deprecated
     */
    public function uploadPage(Request $request): JsonResponse
    {
        $pageId = $request->get('page_id');
        $uploadFile = $request->files->get('zipFile');
        if ($uploadFile->getClientOriginalExtension() !== "zip") {
            $stats['error'] = "Please upload a zip file";
        }

        $pageType = $request->get('page_type');
        //todo:move the file and send the location
        $uploadDir = $this->getParameter('page_upload_dir');
        //get details for download
        $uploadPageDetails = $this->campaignService->getDetailsForPageDownload($pageType, (int)$pageId);
        //upload locally
        $destination = $uploadDir . "/" . $uploadPageDetails['folder_name'];
        $uploadFile->move($destination, $uploadPageDetails['folder_name'] . "." . $uploadFile->getClientOriginalExtension());

        $status = $this->campaignService->uploadPageFiles($uploadPageDetails, $destination);
        //return the response
        return new JsonResponse($status);
    }

    /**
     *
     * @Route( "page/upload/htmlfieldanalysis", name="page.htmlfieldanalysis", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @author aki
     * @deprecated
     */
    public function uploadFormFieldsAnalysis(Request $request): JsonResponse
    {
        //get file form request
        $uploadedFile = $request->files->get('zipFile');
        // page_upload_dir is configured in services.yaml
        $uploadDir = $this->getParameter('page_upload_dir'); // ../files/upload_pages

        //get parameters form request
        $folderName = $request->get('folderName');
        $campaignId = $request->get('campaignId');


        //create the dir if not exists
        if (!file_exists($uploadDir)) {
            if (!mkdir($uploadDir, 0755, true) && !is_dir($uploadDir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $uploadDir));
            }
        }
        //get the zip file name
        $zipFIleName = $folderName . "." . $uploadedFile->getClientOriginalExtension();

        //move file to upload files
        $sourceFolder = $uploadDir . DIRECTORY_SEPARATOR . $folderName;
        $uploadedFile->move($sourceFolder, $zipFIleName);

        //unzip the file
        $unZipStatus = $this->campaignService->unZipFile($sourceFolder, $zipFIleName);
        if (!$unZipStatus) {
            return $this->json([
                'status' => false,
                'message' => 'failed to unzip the file',
            ]);
        }

        //make file analysis
        $fileAnalysisStatus = $this->campaignService->makeHtmlFieldAnalysis($sourceFolder, $campaignId);
        if (!$fileAnalysisStatus) {
            return $this->json([
                'status' => false,
                'message' => 'failed to clear the upload folder',
            ]);
        }

        //clear the upload_pages folder
        $folderClearStatus = $this->campaignService->deleteUploadFolder($sourceFolder);
        if (!$folderClearStatus) {
            return $this->json([
                'status' => false,
                'message' => 'failed to clear the upload folder',
            ]);
        }

        return $this->json([
            'status' => true,
            'message' => 'message according to change of the fields',
        ]);
    }

}
