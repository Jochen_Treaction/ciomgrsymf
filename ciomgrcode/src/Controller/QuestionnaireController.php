<?php


namespace App\Controller;


use App\Repository\StatusDefRepository;
use App\Services\AuthenticationService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class QuestionnaireController extends AbstractController
{
    /**
     * @var LoggerInterface
     */
    protected $logger;


    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    //todo: Replace dummy methods with functional

    /**
     * @Route("/questionnaire", name="questionnaire.overview", methods={"GET"}))
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function questionnaireoverview(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils
    )
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $twig = "questionnaire/questionnaireoverview.html.twig";
        return $this->render($twig, ['current_user' => $user]);
    }

    /**
     * @Route("/questionnaire/wizard", name="questionnaire.wizard", methods={"GET"}))
     * @param Request $request
     * @param AuthenticationService $authenticationService
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function questionnairewizard(
        Request $request,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils
    )
    {
        $user = $authenticationService->getUserParametersFromCache($authenticationUtils->getLastUsername());
        $twig = "questionnaire/questionnairewizard.html.twig";
        return $this->render($twig, ['current_user' => $user]);
    }


}