<?php

namespace App\Controller;

use App\Repository\SeedPoolRepository;
use App\Services\InboxMonitorService;
use App\Services\UtilsService;
use App\Types\UniqueObjectTypes;
use JsonException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Services\AuthenticationService;
use Psr\Log\LoggerInterface;

class SeedPoolController extends AbstractController
{


    private LoggerInterface $logger;
    private AuthenticationService $authenticationService;
    private AuthenticationUtils $authenticationUtils;
    private $userId;
    private $user;
    private UtilsService $utilsService;
    private InboxMonitorService $service;

    public function __construct(
        LoggerInterface $logger,
        AuthenticationService $authenticationService,
        AuthenticationUtils $authenticationUtils,
        UtilsService $utilsService,
        InboxMonitorService $inboxMonitorService,
        SeedPoolRepository $seedPoolRepository
    )
    {
        $this->logger = $logger;
        $this->authenticationService = $authenticationService;
        $this->authenticationUtils = $authenticationUtils;
        $this->utilsService = $utilsService;
        $this->service = $inboxMonitorService;
        $this->seedPoolRepository = $seedPoolRepository;
        // set userId
        $userEmail = $authenticationUtils->getLastUsername();
        $this->user = $authenticationService->getUserParametersFromCache($userEmail);
        $this->userId = $this->user['user_id'];

    }

    /**
     * @Route( "/seedpool/delete/{seed_pool_id}", name="seedpool.delete",  methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws JsonException
     * @internal Deletes seed pool with seed pool id
     * @author Aki
     */
    public function deleteSeedPool(
        Request $request
    ): JsonResponse {
        $userEmail = $this->authenticationUtils->getLastUsername();
        $currentUser = $this->user;
        $response = new JsonResponse();
        //basic user check
        if (empty($currentUser) || empty($userEmail)) {
            $message = "Invalid user";
            return $this->utilsService->setResponseWithMessage($response,$message,false);
        }
        //Get the params
        $seedPoolId = $request->get('seed_pool_id');
        //remove seedpool group if it is an orphaned record
        //todo: check for seedpool group is alone and if alone remove it if alone
        $status = $this->seedPoolRepository->deleteSeedPool((int)$seedPoolId);

        if(empty($status)){
            $message = "Something went wrong in deleting seed pool";
            return $this->utilsService->setResponseWithMessage($response,$message,false);
        }
        $message = 'success';
        return $this->utilsService->setResponseWithMessage($response,$message,true);
    }

}
