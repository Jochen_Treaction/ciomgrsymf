<?php


namespace App\Types;


class MessagesTypes
{
    private $type;
    private $typeDesc;
    public $uniqueObjectName;


    public function __construct()
    {
        $this->uniqueObjectName = 'mapping';

        $this->type = [
            'single_submit_error_text' => '',
            'countdown_expire_message' => '',
            'error_msg_color' => '',
            'error_msg_duplicate' => '',
            'error_msg_email' => '',
            'error_msg_telefon' => '',
            'error_msg_image_upload' => '',
            'error_msg_postalcode' => '',
            'error_msg_block_free_mailer' => '',
            'error_msg_address' => ' ',
            'error_msg_campaign_end_date' => '',
            'expiration_color' => '',
            'expiration_transparency' => ''
        ];
        //todo:make type description
        $this->typeDesc = [
            'single_submit_error_text' => [
                'ISO 639-1' => 'some text',
                'en' => 'submit unsuccesfull',
                'de' => 'Übertragung fehlgeschlagen',
                'es' => ''
            ],
            'countdown_expire_message' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'error_msg_Color' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'error_msg_duplicate' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'error_msg_email' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'error_msg_telefon' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'error_msg_image_upload' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'error_msg_postalcode' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'error_msg_block_free_mailer' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'error_msg_address' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'error_msg_campaign_end_date' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'expiration_color' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
            'expiration_transparency' => [
                'ISO 639-1' => '',
                'en' => '',
                'de' => '',
                'es' => ''
            ],
        ];

    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        return json_encode($this->type);
    }


    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getArrayWithDefaultValues():array
    {

        $default = $this->type;
        $default['single_submit_error_text'] = "You have already registred.";
        $default['countdown_expire_message'] = "Die Aktion ist beendet";
        $default['error_msg_color'] = "#FF0000";
        $default['error_msg_duplicate'] = "* Sie haben bereits teilgenommen";
        $default['error_msg_email'] = "* Bitte überprüfen Sie die E-Mail-Adresse";
        $default['error_msg_telefon'] = "* Bitte überprüfen Sie die Telefonnummer";
        $default['error_msg_image_upload'] = "* Bitte überprüfen Sie die Bildformat nur png, jpg sind erlaubt";
        $default['error_msg_postalcode'] = "* Bitte überprüfen Sie die Postleitzahl";
        $default['error_msg_block_free_mailer'] = "* Bitte nutzen Sie Ihre geschäftliche E-Mail-Adresse";
        $default['error_msg_address'] = "* Bitte überprüfen Sie die Adresse";
        $default['error_msg_campaign_end_date'] = "Die Aktion ist beendet";
        $default['expiration_color'] = "#FF0000";
        $default['expiration_transparency'] = "0.1";

        return $default;

    }

}