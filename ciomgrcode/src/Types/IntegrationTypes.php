<?php


namespace App\Types;


class IntegrationTypes
{

    // Integrations
    public const INTEGRATION_EMIO = 'eMail-In-One';
    public const INTEGRATION_EMAILSERVER = 'EmailServer';
    public const INTEGRATION_HUBSPOT = 'hubspot';
    public const INTEGRATION_DIGISTORE = 'digistore';
    public const INTEGRATION_WEBINARIS = 'webinaris';

    // Integration Types
    public const INTEGRATION_TYPE_ALL = 'all';
    public const INTEGRATION_TYPE_EMAIL = 'email';
    public const INTEGRATION_TYPE_CRM = 'crm';
    public const INTEGRATION_TYPE_ONLINE = 'online';


    public function __construct()
    {
        $this->type = [];
    }

    /**
     * @return string[]|null
     * @author Pradeep
     */
    public function getAllIntegrations(): ?array
    {
        return [
            self::INTEGRATION_EMIO => [
                'title' => 'eMail-In-One',
                'subTitle' => 'Newsletter,DOI,Trigger,Automation',
                'type' => [self::INTEGRATION_TYPE_EMAIL, self::INTEGRATION_TYPE_ALL],
                'releaseDate' => '26.05.2020',
                'lastUpdate' => '26.05.2020',
                'version' => '1.0',
            ],
            self::INTEGRATION_EMAILSERVER => [
                'title' => 'Email Server',
                'subTitle' => 'Newsletter,DOI,Trigger,Automation',
                'type' => [self::INTEGRATION_TYPE_EMAIL, self::INTEGRATION_TYPE_ALL],
                'releaseDate' => '26.05.2020',
                'lastUpdate' => '26.05.2020',
                'version' => '1.0',
            ],
            self::INTEGRATION_HUBSPOT => [
                'title' => 'Hubspot',
                'subTitle' => 'Newsletter,DOI,Trigger,Automation',
                'type' => [self::INTEGRATION_TYPE_CRM, self::INTEGRATION_TYPE_ALL],
                'releaseDate' => '26.05.2020',
                'lastUpdate' => '26.05.2020',
                'version' => '1.0',
            ],
            // Currently Not supported in this version of MIO.
            /*            self::INTEGRATION_DIGISTORE => [
                            'title' => 'Digistore',
                            'subTitle' => 'Save, organise leads in Digistore',
                            'type' => [self::INTEGRATION_TYPE_ONLINE, self::INTEGRATION_TYPE_ALL],
                            'releaseDate' => '26.05.2020',
                            'lastUpdate' => '26.05.2020',
                            'version' => '1.0',
                        ],
                        self::INTEGRATION_WEBINARIS => [
                            'title' => 'Webinaris',
                            'subTitle' => 'Save, organise leads in Webinaris',
                            'type' => [self::INTEGRATION_TYPE_ONLINE, self::INTEGRATION_TYPE_ALL],
                            'releaseDate' => '26.05.2020',
                            'lastUpdate' => '26.05.2020',
                            'version' => '1.0',
                        ],*/
        ];
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getIntegrationTypeEmail(): ?array
    {

    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getIntegrationTypeCRM(): ?array
    {

    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getIntegrationTypeOnlineTools(): ?array
    {

    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getEmailServerConfig(): ?array
    {
        return [
            'overview' => [
                'title' => 'Email Server',
                'subTitle' => 'Newsletter,DOI,Trigger,Automation',
                'type' => [IntegrationTypes::INTEGRATION_TYPE_EMAIL, IntegrationTypes::INTEGRATION_TYPE_ALL],
                'releaseDate' => '26.05.2020',
                'lastUpdate' => '26.05.2020',
                'version' => '1.0',
            ],
            'relation' => [
                'company_id' => '',
                'integration_object_unique_id' => '',
                'integration_objectregister_id' => '',
                'integration_name' => IntegrationTypes::INTEGRATION_EMAILSERVER,
                'integration_type' => [
                    IntegrationTypes::INTEGRATION_TYPE_EMAIL,
                    IntegrationTypes::INTEGRATION_TYPE_ALL,
                ],
                'integration_object_register_meta_id' => '',
            ],
            'settings' => [
                'server_type' => [
                    'custom_server',
                    'default_server',
                ],
                'default_server' => [
                    'smtp_server_domain' => 'default.com',
                    'sender_user_email' => 'test@default.com',
                    'sender_password' => 'default',
                    'port' => '21',
                ],
                'custom_server' => [
                    'smtp_server_domain' => '',
                    'sender_user_email' => '',
                    'sender_password' => '',
                    'port' => '',
                ],
            ],
        ];
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getHubspotConfig(): ?array
    {
        return [
            'overview' => [
                'title' => 'Hubspot',
                'subTitle' => 'Newsletter,DOI,Trigger,Automation',
                'type' => [self::INTEGRATION_TYPE_CRM, self::INTEGRATION_TYPE_ALL],
                'releaseDate' => '26.05.2020',
                'lastUpdate' => '26.05.2020',
                'version' => '1.0',
            ],
            'relation' => [
                'company_id' => '',
                'integration_object_unique_id' => '',
                'integration_objectregister_id' => '',
                'integration_object_register_meta_id' => '',
                'integration_name' => IntegrationTypes::INTEGRATION_HUBSPOT,
                'integration_type' => [IntegrationTypes::INTEGRATION_TYPE_CRM, IntegrationTypes::INTEGRATION_TYPE_ALL],
            ],
            'settings' => [
                'apikey' => '',
                'owner_id' => '',
                'mapping' => '',
            ],
        ];
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getDigistoreConfig(): ?array
    {
        return [
            'overview' => [
                'title' => 'Digistore',
                'subTitle' => 'Save, organise leads in Digistore',
                'type' => [self::INTEGRATION_TYPE_ONLINE, self::INTEGRATION_TYPE_ALL],
                'releaseDate' => '26.05.2020',
                'lastUpdate' => '26.05.2020',
                'version' => '1.0',
            ],
            'relation' => [
                'company_id' => '',
                'integration_object_unique_id' => '',
                'integration_objectregister_id' => '',
                'integration_object_register_meta_id' => '',
                'integration_name' => IntegrationTypes::INTEGRATION_DIGISTORE,
                'integration_type' => [
                    IntegrationTypes::INTEGRATION_TYPE_ONLINE,
                    IntegrationTypes::INTEGRATION_TYPE_ALL,
                ],
            ],
            'settings' => [
                'apikey' => '',
            ],
        ];
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getWebinarisConfig(): ?array
    {
        return [
            'overview' => [
                'title' => 'Webinaris',
                'subTitle' => 'Save, organise leads in Webinaris',
                'type' => [self::INTEGRATION_TYPE_ONLINE, self::INTEGRATION_TYPE_ALL],
                'releaseDate' => '26.05.2020',
                'lastUpdate' => '26.05.2020',
                'version' => '1.0',
            ],
            'relation' => [
                'company_id' => '',
                'integration_object_unique_id' => '',
                'integration_objectregister_id' => '',
                'integration_object_register_meta_id' => '',
                'integration_name' => IntegrationTypes::INTEGRATION_WEBINARIS,
                'integration_type' => [
                    IntegrationTypes::INTEGRATION_TYPE_ONLINE,
                    IntegrationTypes::INTEGRATION_TYPE_ALL,
                ],
            ],
            'settings' => [
                'user_id' => '',
                'password' => '',
            ],
        ];
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getEMailInOneConfig(): ?array
    {
        return [
            'overview' => [
                'title' => 'eMail-In-One',
                'subTitle' => 'Newsletter,DOI,Trigger,Automation',
                'type' => [IntegrationTypes::INTEGRATION_TYPE_EMAIL, IntegrationTypes::INTEGRATION_TYPE_ALL],
                'releaseDate' => '26.05.2020',
                'lastUpdate' => '26.05.2020',
                'version' => '1.0',
            ],
            'relation' => [
                'company_id' => '',
                'integration_object_unique_id' => '',
                'integration_objectregister_id' => '',
                'integration_name' => IntegrationTypes::INTEGRATION_EMIO,
                'integration_type' => [
                    IntegrationTypes::INTEGRATION_TYPE_EMAIL,
                    IntegrationTypes::INTEGRATION_TYPE_ALL,
                ],
            ],
            'settings' => [
                'apikey' => '',
                'doi_mailing' => '',
                'blacklist_id' => '',
                'blacklisting_flagged_name' => '',
                'reactivate_unsubscibers' => '',
                'mapping' => '',
            ],
        ];
    }

    /**
     * @return string[]
     * @author Pradeep
     */
    public function getSuperAdminEMailInOneConfig(): array
    {
        return [
            'overview' => [
                'title' => 'eMail-In-One',
            ],
            'relation' => [
                'superadmin_object_unique_id' => '',
                'superadmin_objectregister_id' => '',
                'superadmin_objectregister_meta_id' => '',
            ],
            'settings' => [
                'apikey' => '',
            ],
        ];
    }

    /**
     * @return string[]
     * @author Pradeep
     */
    public function getSuperAdminHubSpotConfig(): array
    {
        return [
            'overview' => [
                'title' => 'Hubspot',
            ],
            'relation' => [
                'superadmin_object_unique_id' => '',
                'superadmin_objectregister_id' => '',
                'superadmin_objectregister_meta_id' => '',
            ],
            'settings' => [
                'apikey' => '',
            ],
        ];
    }

    /**
     * @return array|null
     * @author Pradeep
     */
    public function getSuperAdminEmailServerConfig(): ?array
    {
        return [
            'overview' => [
                'title' => 'Email Server',
            ],
            'relation' => [
                'integration_object_unique_id' => '',
                'integration_objectregister_id' => '',
            ],
            'settings' => [
                'server_type' => [
                    'default_server',
                ],
                'default_server' => [
                    'smtp_server_domain' => '',
                    'sender_user_name' => '',
                    'sender_user_email' => '',
                    'sender_password' => '',
                    'port' => '',
                ],
            ],
        ];
    }

    /**
     * @return array|null
     * @author Aki
     */
    public function getSuperAdminInboxMonitorConfig(): ?array
    {
        return [
            'overview' => [
                'title' => 'Inbox Monitor',
            ],
            'relation' => [
                'integration_object_unique_type_id' => '',
                'superadmin_objectregister_id' => '',
            ],
            'settings' => [
                'user' => '',
                'apikey' => '',
            ],
        ];
    }


    /**
     * @return array|null
     * @author Aki
     */
    public function getSuperSFTPConfig(): ?array
    {
        return [
            'overview' => [
                'title' => 'SFTP config',
            ],
            'relation' => [
                'integration_object_unique_type_id' => '',
                'superadmin_objectregister_id' => '',
            ],
            'settings' => [
                'ip_address' => '',
                'sftp_user' => '',
                'sftp_password' => '',
            ],
        ];
    }

}
