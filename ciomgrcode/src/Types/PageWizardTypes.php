<?php


namespace App\Types;


class PageWizardTypes
{
    private $type;
    private $typeDesc;
    public $uniqueObjectName;
    private array $mappingFieldType;


    public function __construct()
    {
        //todo: change this to enum from php 8.1
        $this->uniqueObjectName = 'pages';

        $this->type = [
            "page_flow" => [
                "success_page" => "",
                "error_page" => "",
                "redirect_page" => "",
                "redirect_time" => "",
                "custom_pages" => [
                    [
                        "name" => "index.html",
                        "buttons" => [
                            [
                                "name" => "submit"
                            ],
                        ]
                    ],
                ],
            ],
            "splittest" => [
                "status" => "",
                "name" => "",
                "total_variants" => "",
                "cookie_expiration" => "",
                "sticky_config" => "",
                "expiration_criteria" => "",
                "expiration_value_visitors" => "",
                "expiration_value_visitor" => "",
                "expiration_value_date" => "",
                "variants" => []
            ],
            "settings" => [
                "name" => "",
                "start_date" => "",
                "end_date" => "",
                "description" => "",
            ],
            "mapping" => [
                "mappingTable" => ""
            ],
            "processhooks" => "",
            'additionalSettings' =>
                [
                    'objectregister_id' => '',
                    'accountNumber' => '',
                    'apiKey' => '',
                    'CampaignName' => '',
                    'api-in-one' => '',
                ],
            'tracker' =>
                [
                    'goolge_apikey' => '',
                    'facebook_pixel' => '',
                    'webgains_id' => '',
                    'outbrain_id' => '',
                    'bool_outbrain_track_after_30_seconds' => '',
                    'matomo_auth_token' => ''
                ],
        ];
        //todo:make type description
        $this->typeDesc = [];
        $this->mappingFieldType = [
            "fieldName" => "",
            "required" => "No",
            "datatype" => "Text",
            "regex" => ""
        ];

    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        return json_encode($this->type);
    }


    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getArrayWithDefaultValues(): array
    {
        $default = $this->type;
        $default['pageflow']['success_page'] = 'success.html';
        $default['pageflow']['error_page'] = 'error.html';
        $default['pageflow']['redirect_page'] = 'https://treaction.net/';
        $default['pageFlowDefaultSettings']['custom_pages'][0]['name'] = 'index.html';
        $default['splittest']['status'] = NULL;
        $default['splittest']['total_variants'] = 'Please select';
        $default['splittest']['sticky_config'] = NULL;
        $default['splittest']['expiration_criteria'] = '90';
        $default['splittest']['variants'] = NULL;
        return $default;
    }

    /**
     *
     */
    public function getDatastructure()
    {

    }

    /**
     * @return array
     */
    public function getArrayForWizard(): array
    {
        $default = [];
        $default['page_flow']['campaign']['url'] = '';
        $default['page_flow']['success_page'] = 'success.html';
        $default['page_flow']['error_page'] = 'error.html';
        $default['page_flow']['redirect_page'] = 'https://treaction.net/';
        $default['page_flow']['custom_pages'][0]['name'] = 'index.html';
        $default['page_flow']['custom_pages'][0]['buttons'][0]['name'] = 'Submit';
        $default['splitTest']['status'] = NULL;
        $default['splitTest']['name'] = '';
        $default['splitTest']['total_variants'] = 'Please select';
        $default['splitTest']['cookie_expiration'] = '90';
        $default['splitTest']['sticky_config'] = NULL;
        $default['splitTest']['expiration_criteria'] = NULL;
        $default['splitTest']['expiration_value_visitors'] = '';
        $default['splitTest']['expiration_value_conversions'] = '';
        $default['splitTest']['expiration_value_date'] = '';
        $default['splitTest']['variants'] = NULL;
        return $default;
    }

    public function getMappingHtmlFieldType(): array
    {
        return $this->mappingFieldType;
    }



}
