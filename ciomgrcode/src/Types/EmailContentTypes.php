<?php


namespace App\Types;

/**
 * MIO, AIO, MSMAIL
 * Class EmailContentTypes
 * @package App\Types
 */
class EmailContentTypes
{
	public  $accountFinishSetupNotification = 'account-finish-setup-notification';
	public  $accountReactivationNotification = 'account-reactivation-notification'; // TODO
	public  $accountDeletionNotification = 'account-deletion-notification'; // TODO
	public  $accountRegistrationNotification = 'account-registration-notification'; // okay
	public  $accountSendActivationlink = 'account-send-activationlink'; // okay
	public  $accountSendActivationReminder = 'account-send-activation-reminder'; // TODO
	public  $accountSendPasswordResetLink = 'account-send-password-reset-link';
	public  $accountTestMailserverNotification = 'account-test-mailserver-notification'; // TODO
	public  $accountVerifyApikey = 'account-verify-apikey'; // okay
	public  $aioAccountSendActivationlink = 'aio-account-send-activationlink'; // TODO
	public  $aioSendLeadNotification = 'aio-send-lead-notification'; // TODO


	public function __construct()

	{
	}


	/**
	 * @return array
	 */
	public function getEmailContentTypesArray():array
	{
		$emailContentTypesArray = [];
		foreach ($this as $k => $v) {
			$emailContentTypesArray[$k] = $v;
		}
		return $emailContentTypesArray;
	}

	public function getPropertyName(string $emailContentType): ?string
	{
		foreach ($this as $k => $v) {
			if( $v === $emailContentType) return $k;
		}
		return null;
	}
}
