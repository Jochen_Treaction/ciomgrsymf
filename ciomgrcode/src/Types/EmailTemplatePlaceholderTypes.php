<?php


namespace App\Types;

use Exception;


/**
 * QUICK & DIRTY -> must be replaced by TWIG Templates TODO: reimplement, since concept is bad ...
 * Class EmailTemplatePlaceholderTypes
 * @package App\Types
 */
class EmailTemplatePlaceholderTypes
{

	const PLACEHOLDER_ACTIVATIONLINK = "###ACTIVATIONLINK###";
	const PLACEHOLDER_FIRSTNAME = "###FIRSTNAME###";
	const PLACEHOLDER_LASTNAME = "###LASTNAME###";
	const PLACEHOLDER_IMGURL = "###IMGURL###";
	const PLACEHOLDER_ACCOUNTNO = "###ACCOUNTNO###";
	const PLACEHOLDER_APIKEY = "###APIKEY###";
	const PLACEHOLDER_EMAIL = "###EMAIL###";


	private static $ACTIVATIONLINK_PATH = '/assets/email-templates/account-send-activationlink';
	private static $ACTIVATIONLINK_FILENAME = 'mio-email-template-account-activation.twig';

	private static $VERIFY_APIKEY_PATH = '/assets/email-templates/account-verify-apikey';
	private static $VERIFY_APIKEY_FILENAME = 'mio-email-template-verfiy-api-key.twig';

	private static $REGISTRATION_NOTIFICATION_PATH = '/assets/email-templates/account-registration-notification';
	private static $REGISTRATION_NOTIFICATION_FILENAME = 'mio-email-template-registration-notification.twig';







	/**
	 * @return string    string: e.g. https://dev-campaign-in-one.net
	 */
	public static function getBaseUrl(): string
	{
		$protocol = explode('/', $_SERVER['SERVER_PROTOCOL'])[0];

		return $protocol . '://' . $_SERVER['SERVER_NAME'];
	}


	/**
	 * @return string root path to activation link email template e.g. '/mypath/subfolder'
	 */
	public static function getActivationLinkPath(): string
	{
		try {
			$path = $_SERVER['DOCUMENT_ROOT'] . self::$ACTIVATIONLINK_PATH;
		} catch (Exception $e) {
			$path = 'exception not found';
		}
		return (null !== $path) ? $path : 'not found';
	}


	/**
	 * @return string string of activation link filename e.g. 'mytemplate.html'
	 */
	public static function getActivationlinkHtmlFilename(): string
	{
		return self::$ACTIVATIONLINK_FILENAME;
	}


	/**
	 * @return string root path to activation link email template e.g. 'https://dev-campaign-in-one.net/activationlink'
	 */
	public static function getActivationLinkUrl(): string
	{
		return self::getBaseUrl() . self::$ACTIVATIONLINK_PATH;
	}


	/**
	 * @return string root path to activation link email template e.g. 'https://dev-campaign-in-one.net/activationlink/img'
	 */
	public static function getActivationLinkImgUrl(): string
	{
		return self::getBaseUrl() . self::$ACTIVATIONLINK_PATH . '/img';
	}




	/**
	 * @return string[] string[] of all activation link placeholders
	 */
	public static function getActivationLinkPlaceholders(): array
	{
		return [
			self::PLACEHOLDER_ACTIVATIONLINK,
			self::PLACEHOLDER_FIRSTNAME,
			self::PLACEHOLDER_LASTNAME,
			self::PLACEHOLDER_IMGURL,
			self::PLACEHOLDER_ACCOUNTNO,
		];
	}



	/**
	 * @return string root path to verify apikey email template e.g. '/mypath/subfolder'
	 */
	public static function getVerifyApikeyPath(): string
	{
		try {
			$path = $_SERVER['DOCUMENT_ROOT'] . self::$VERIFY_APIKEY_PATH;
		} catch (Exception $e) {
			$path = 'exception not found';
		}
		return (null !== $path) ? $path : 'not found';
	}



	/**
	 * @return string string of verify apikey filename e.g. 'mytemplate.html'
	 */
	public static function getVerifyApikeyHtmlFilename(): string
	{
		return self::$VERIFY_APIKEY_FILENAME;
	}


	/**
	 * @return string root path to verfiy apikey email template e.g. 'https://dev-campaign-in-one.net/activationlink'
	 */
	public static function getVerifyApikeyLinkUrl(): string
	{
		return self::getBaseUrl() . self::$VERIFY_APIKEY_PATH;
	}


	/**
	 * @return string root path to activation link email template e.g. 'https://dev-campaign-in-one.net/activationlink/img'
	 */
	public static function getVerifyApikeyImgUrl(): string
	{
		return self::getBaseUrl() . self::$VERIFY_APIKEY_PATH . '/img';
	}





	/**
	 * @return string[] string[]  of all verify apikey placeholders
	 */
	public static function getVerifyApikeyPlaceholders():array
	{
		return array_merge([self::PLACEHOLDER_APIKEY], self::getActivationLinkPlaceholders());
	}



	/**
	 * @return string[]
	 */
	public static function getRegistrationNotificationPlaceholders(): array
	{
		return [
			self::PLACEHOLDER_FIRSTNAME,
			self::PLACEHOLDER_IMGURL,
			self::PLACEHOLDER_LASTNAME,
			self::PLACEHOLDER_EMAIL,
			self::PLACEHOLDER_ACCOUNTNO,
		];
	}



	/**
	 * @return string string of registration notification filename e.g. 'mytemplate.html'
	 */
	public static function getRegistrationNotificationHtmlFilename(): string
	{
		return self::$REGISTRATION_NOTIFICATION_FILENAME;
	}


	/**
	 * @return string root path to registration notification email template e.g. 'https://dev-campaign-in-one.net/activationlink'
	 */
	public static function getRegistrationNotificationLinkUrl(): string
	{
		return self::getBaseUrl() . self::$REGISTRATION_NOTIFICATION_PATH;
	}


	/**
	 * @return string root path to registration notification email image url
	 */
	public static function getRegistrationNotificationImgUrl(): string
	{
		return self::getBaseUrl() . self::$REGISTRATION_NOTIFICATION_PATH . '/img';
	}

	/**
	 * @return string root path to registration notification email template e.g. '/mypath/subfolder'
	 */
	public static function getRegistrationNotificationPath(): string
	{
		try {
			$path = $_SERVER['DOCUMENT_ROOT'] . self::$REGISTRATION_NOTIFICATION_PATH;
		} catch (Exception $e) {
			$path = 'exception not found';
		}
		return (null !== $path) ? $path : 'not found';
	}


}
