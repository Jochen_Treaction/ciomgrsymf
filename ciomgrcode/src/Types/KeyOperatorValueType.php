<?php

namespace App\Types;

class KeyOperatorValueType
{
    private string $tableFieldName;//b.z leads.email
    private string $operator;
    private string $value;

    public function __construct()
    {
        $this->tableFieldName = '';
        $this->operator = '';
        $this->value = '';
    }


    /**
     * @return string|null
     */
    public function getTableFieldName(): ?string
    {
        return $this->tableFieldName;
    }


    /**
     * @param string|null $tableFieldName
     */
    public function setTableFieldName(?string $tableFieldName): self
    {
        $this->tableFieldName = $tableFieldName;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getOperator(): ?string
    {
        return $this->operator;
    }


    /**
     * @param string|null $operator
     */
    public function setOperator(?string $operator): self
    {
        $this->operator = $operator;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }


    /**
     * @param string|null $value
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;
        return $this;
    }


    /**
     * magic invoke initialisation
     */
    public function __invoke()
    {
        // TODO: Implement __invoke() method.
        $this->tableFieldName = '';
        $this->operator = '';
        $this->value = '';
    }

}
