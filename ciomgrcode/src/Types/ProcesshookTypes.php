<?php


namespace App\Types;


class ProcesshookTypes
{

    public const SEND_LEAD_NOTIFICATION = 'send_lead_notification';
    public const START_MARKETING_AUTOMATION = 'start_marketing_automation';
    public const SEND_DOI_EMAIL = 'send_double_opt-In_mail';
    public const CALL_CONTACT_EVENT = 'call_contact_event';
    public const SEND_DOI_EMAIL_OR_CALL_CONTACT_EVENT = 'send_double_opt_in_mail_or_call_contact_event';
    public const SET_SMART_TAG = 'set_smart_tag';
    public const REMOVE_SMART_TAG = 'remove_smart_tag';
    public const SHOW_POPUP = 'show_popup';

    // TODO: add all neccessary const for all corresponding ProcesshookTypes of table unique_object_type. Values must be in lower case + snake_case, keys are upper case + SNAKE_CASE! Replace/refactor corresponding occurances in the code!
	// e.g. const SEND_LEAD_NOTIFICATION {"name":"","list_of_recipients":"","subject-line":""} ===>
	//
	public const SEND_LEAD_NOTIFICATION_PROP = ['NAME' => 'name', 'LIST_OF_RECIPIENTS' => 'list_of_recipients' , 'SUBJECT_LINE' => 'subject_line'];
	public const DEFAULT_PROCESSHOOK_SEND_LEAD_NOTIFICATION = 'Neuer Lead';
	//
	// use in code example:
	// 		use App\Types\ProcesshookTypes as PhType;
	//		...
	//   	$newLeadNotification = [
	// 			PhType::SEND_LEAD_NOTIFICATION_PROP['NAME'] => 'myName',
	// 			PhType::SEND_LEAD_NOTIFICATION_PROP['LIST_OF_RECIPIENTS'] => 'email@web.de',
	// 			PhType::SEND_LEAD_NOTIFICATION_PROP['SUBJECT_LINE'] => 'my subject line'
	//		];

	public function __construct()
    {
    }
}
