<?php


namespace App\Types;


class MessageParameterTypes
{
	/**
	 * @var string
	 */
	public  $firstName = '';

	/**
	 * @var string
	 */
	public  $lastName = '';

	/**
	 * @var string
	 */
	public  $email = '';

	/**
	 * @var string
	 */
	public  $apiKey = '';

	/**
	 * @var string
	 */
	public  $accountNo = '';

	/**
	 * @var string
	 */
	public  $activationLink = '';

	/**
	 * @var string
	 */
	public  $customContent = '';


	public function __construct()
	{
	}


	public function getMessageParameterTypesArray():array
	{
		return (array) $this;
	}
}
