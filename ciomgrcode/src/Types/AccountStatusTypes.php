<?php


namespace App\Types;


class AccountStatusTypes
{
    /**
     * @const NEW = "new" nach Registrierung
     */
    public const NEW = "new";

    /**
     * @const ACTIVE = "active" nach Aktivierungslink
     */
    public const ACTIVE = "active";

    /**
     * @const CONFIRMED = "confirmed" nach DPA Bestätigung oder nachdem master admin account reaktiviert hat
     */
    public const CONFIRMED = "confirmed";

    /**
     * @const MARKEDFORDELETION = "markedfordeletion" nachdem master admin account zum Löschen freigegeben hat (30 Tage bis cron job account löscht)
     */
    public const MARKEDFORDELETION = "markedfordeletion";

    /**
     * @const BLOCK = "blocked" Block complete account of the user and User cannot login even after reseting the password.
     */
    public const BLOCK = "blocked";

    /**
     * @return string[] all possible status types of App\Types\AccountStatusTypes
     * @auther jsr
     */
    public static function getTypes(): array
    {
        return [
            self::NEW,
            self::CONFIRMED,
            self::ACTIVE,
            self::MARKEDFORDELETION,
        ];
    }


    /**
     * @param string $accountStatusType
     * @return bool
     * @author jsr
     */
    public static function isCorrectType(string $accountStatusType): bool
    {
        return in_array($accountStatusType, self::getTypes());
    }
}
