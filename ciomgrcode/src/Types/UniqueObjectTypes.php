<?php


namespace App\Types;


class UniqueObjectTypes
{
    public const ACCOUNT = "account";
    public const ADVERTORIAL = 'advertorial';
    public const CUSTOMER = "customer";
    public const CUSTOMFIELDS = "customfields";
    public const DIGISTORE = 'digistore';
    public const EMAILINONE = 'email-in-one';
    public const EMAILSERVER = 'emailserver';
    public const HUBSPOT = 'hubspot';
    public const INBOX_MONITOR_CONFIG = 'inbox_monitor_user_config';
    public const PAGE = "page";
    public const PROCESSHOOK = 'processhook';
    public const PROJECT = "project";
    public const SEEDPOOL = 'seed_pool';
    public const STANDALONE_SURVEY = "standalone survey";
    public const STANDARDCUSTOMFIELDS = "standardcustomfields";
    public const SUPERADMIN = 'superadmin';
    public const SURVEY = "survey";
    public const UNIQUE_OBJECT_NAME = "unique_object_name";
    public const WEBHOOK = "webhook";
    public const WEBINARIS = 'webinaris';
    public const WORKFLOW = 'workflow';
    public const INBOX_MONITOR = 'inbox_monitor';
    public const DOMAIN = 'domain';
    public const IP = 'ip';
    public const SERVER = 'server';
    public const LANGUAGE_LOCALIZATION = 'language_localization';
    public const SUPER_ADMIN_DEFAULT_FTP = 'superadmin_default_ftp';


    /**
     * @return string[] all possible status types of App\Types\UniqueObjectTypes
     * @auther jsr
     */
    public static function getTypes(): array
    {
        return [
            self::ACCOUNT,
            self::ADVERTORIAL,
            self::CUSTOMER,
            self::CUSTOMFIELDS,
            self::DIGISTORE,
            self::EMAILINONE,
            self::EMAILSERVER,
            self::HUBSPOT,
            self::INBOX_MONITOR_CONFIG,
            self::PAGE,
            self::PROCESSHOOK,
            self::PROJECT,
            self::SEEDPOOL,
            self::STANDALONE_SURVEY,
            self::STANDARDCUSTOMFIELDS,
            self::SUPERADMIN,
            self::SURVEY,
            self::UNIQUE_OBJECT_NAME,
            self::WEBHOOK,
            self::WEBINARIS,
            self::WORKFLOW,
            self::DOMAIN,
            self::IP,
            self::SERVER,
        // TODO: add rest
        ];
    }


    /**
     * @param string $uniqueObjectType
     * @return bool
     * @author jsr
     */
    public static function isCorrectType(string $uniqueObjectType): bool
    {
        return in_array($uniqueObjectType, self::getTypes());
    }

}

