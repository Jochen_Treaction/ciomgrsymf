<?php

namespace App\Types;

/**
 * ConfigurationTargetTypes
 * @package App\Interfaces
 * @author  jsr
 */
class ConfigurationTargetTypes
{
    // public const WEBHOOK_DEFAULT = "webhookDefault";
    public const WEBHOOK = "webhook";
    // public const PAGE_DEFAULT = "pageDefault";
    public const PAGE = "page";
    public const PROJECT_DEFAULT = "projectDefault"; // TODO @jsr change => must select from unique_object_meta, remove from here
    // NEW:
    // public const PROJECT_DEFAULT = "type_default_settings"; // TODO @jsr change => must select from unique_object_meta
    public const PROJECT = "project";
}
