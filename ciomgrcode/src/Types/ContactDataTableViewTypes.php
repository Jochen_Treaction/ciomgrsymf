<?php

namespace App\Types;

class ContactDataTableViewTypes
{
    /**
     * ReOrder the contact fields for the service 'contact.overview'.
     * Determines contact order from 'dataTableFieldViewOrder'.
     * @param array $leads
     * @param array $customFields
     * @return array
     * @author Pradeep
     */
    public function reOrderFieldsForDataTableView(array $leads, array $customFields):array
    {
        $reOrderedFieldsForLead = [];
        $dTableViewFields = $this->dataTableFieldViewOrder();
        foreach ($leads as $lead) {
            //standard fields
            foreach ($dTableViewFields as $field) {
                if (array_key_exists($field, $lead)) {
                    $reOrderedFields[ $field ] = $lead[ $field ];
                }
            }
            $field = "";
            //custom fields
            foreach ($customFields as $field) {
                if (!isset($field[ 'fieldname' ]) || empty($field[ 'fieldname' ])) {
                    continue;
                }
                if (array_key_exists($field[ 'fieldname' ], $lead)) {
                    $reOrderedFields[ $field[ 'fieldname' ] ] = $lead[ $field[ 'fieldname' ] ];
                }
            }
            //add actions at the end
            $reOrderedFields['Actions'] = $lead['Actions'];

            $reOrderedFieldsForLead [] = $reOrderedFields;
        }
        return $reOrderedFieldsForLead;
    }

    /**
     * @param array $leads
     * @param array $customFields
     * @return array
     */
    public function reOrderFieldsForDataTableViewWithOutActions(array $leads, array $customFields):array
    {
        $reOrderedFieldsForLead = [];
        $dTableViewFields = $this->dataTableFieldViewOrder();
        foreach ($leads as $lead) {
            //standard fields
            foreach ($dTableViewFields as $field) {
                if (array_key_exists($field, $lead)) {
                    $reOrderedFields[ $field ] = $lead[ $field ];
                }
            }
            $field = "";
            //custom fields
            foreach ($customFields as $field) {
                if (!isset($field[ 'fieldname' ]) || empty($field[ 'fieldname' ])) {
                    continue;
                }
                if (array_key_exists($field[ 'fieldname' ], $lead)) {
                    $reOrderedFields[ $field[ 'fieldname' ] ] = $lead[ $field[ 'fieldname' ] ];
                }
            }

            $reOrderedFieldsForLead [] = $reOrderedFields;
        }
        return $reOrderedFieldsForLead;
    }

    /**
     * @internal - Use miostandardfields database table and coloum html_placeholder 'values' hear
     * Helper function for 'reOrderFieldsForDataTableView'
     * Determines the order in which fields in the datatable should show.
     * i,e called from service "contact.overview"
     * @return string[]
     * @author Pradeep
     */
    public function dataTableFieldViewOrder(): array
    {
        return [
            // updated 00
            'updated',
            // status 01
            'Status',
            // Permission 02
            'Permission',
            // Reference 03
            'Reference',
            // Email 04
            'Email',
            // FirstName 05
            'FirstName',
            // LastName 06
            'LastName',
            // PostalCode 07
            'PostalCode',



            ////// Technical Fields ////////
            // AffiliateID 08
            'AffiliateID',
            // Sub AffiliateID 09
            'AffiliateSubID',
            // Referrer ID 10
            'ReferrerID', // Missing in the lead and miostandardfield tables.
            // TargetGroup 11
            'TargetGroup',
            /*            'DeviceType',
                        'DeviceOS',
                        'DeviceOSVersion',
                        'DeviceBrowser',
                        'DeviceBrowserVersion',*/
            // last sync 12
            'LastSync',

            ///// eCommerce Fields ////////
            // LastOrderDate 13
            'LastOrderDate',
            // LastOrderNo 14
            'LastOrderNo',
            // eCommerce Starttags (Category-Tree) 15
            'eCommerceTags',
            // ProductAttributes (SmartList) 16
            'ProductAttributes',
            // LifeTimeTotalNetValue
            'TotalOrderNetValue',      //17
            'LastYearOrderNetValue',   //18
            'LastOrderNetValue', // 19
            'FirstOrderDate', //20
            'SubShops', //21
            'ShopUrl', //22
            'Group', //23
            'TotalNumberOfOrders', //24
            ///// Permission ///////
            // FinalURL (with Parameter) 25
            'FinalURL',
            // URL (without Parameter) 26
            'URL',
            // SOIIP (OptinIP) 27
            'SOIIP',
            // SOITimeStamp (OptinTimeStamp) 28
            'SOITimestamp',
            // DOIIP 29
            'DOIIP',
            // DOITimeStamp 30
            'DOITimestamp',
            ///// CustomFields
        ];
    }

    /**
     * @internal This method generates the field name array for contact view from the array we get from DB
     * @param array $mioFields
     * @return array
     * @author Aki
     */
    public function getFieldNameArray(array $mioFields):array
    {
        $fieldNamesArray = [];
        foreach ($mioFields as $field) {
            $fieldName = $field['fieldname'];
            $fieldNamesArray[] = $fieldName;
        }
        return $fieldNamesArray;
    }
}