<?php


namespace App\Types;


class UserMetaTypes
{
	const REGISTRATION_FIRSTSTEP = 'Registration_FirstStep';
	const REGISTRATION_SECONDSTEP = 'Registration_SecondStep';
	const REGISTRATION_THIRDSTEP = 'Registration_ThirdStep';
	const REGISTRATION_SOURCE_MIO = 'MIO';
	const REGISTRATION_SOURCE_AIO = 'AIO';
}
