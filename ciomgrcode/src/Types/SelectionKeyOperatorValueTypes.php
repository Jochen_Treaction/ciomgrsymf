<?php

namespace App\Types;

use App\Types\KeyOperatorValueType;

class SelectionKeyOperatorValueTypes
{

    private array $fieldNamesOpsValues;
    /**
     * @var string[] <pre>['=', '<', '>', '<=', '>=', 'IN', 'NOT IN', 'LIKE', 'MATCHES']</pre>
     */
    private array $operators;

    public function __construct()
    {
        $this->fieldNamesOpsValues = [];
        $this->operators = ['=', '<', '>', '<=', '>=', 'IN', 'NOT IN', 'LIKE', 'MATCHES'];
    }


    /**
     * @return array string[] <pre>['=', '<', '>', '<=', '>=', 'IN', 'NOT IN', 'LIKE', 'MATCHES']</pre>
     */
    public function getPossibleOperators():array
    {
        return $this->operators;
    }

    /**
     * @param string $tableFieldName
     * @param string $operator <pre>['=', '<', '>', '<=', '>=', 'IN', 'NOT IN', 'LIKE', 'MATCHES']</pre>
     * @param string $selectionValue
     */
    public function addKeyOpValue(string $tableFieldName, string $operator, string $selectionValue):self
    {
        $keyOperatorValue = new KeyOperatorValueType();
        $keyOperatorValue
            ->setTableFieldName($tableFieldName)
            ->setOperator($operator)
            ->setValue($selectionValue);
        $this->fieldNamesOpsValues[$tableFieldName] = $keyOperatorValue;
        return $this;
    }


    /**
     * @param string $tableFieldName
     * @return \App\Types\KeyOperatorValueType|null
     */
    public function getOperator(string $tableFieldName):?string
    {

        $return = null;

        foreach ($this->fieldNamesOpsValues as $fieldName  => $keyOperatorValue) {
            if($tableFieldName === $fieldName){
                $return = $keyOperatorValue->getOperator();
            }
        }

        return $return;

    }


    /**
     * @param string $tableFieldName
     * @return string|null
     */
    public function getSelectionValue(string $tableFieldName):?string
    {

        $return = null;

        foreach ($this->fieldNamesOpsValues as $fieldName  => $keyOperatorValue) {
            if($tableFieldName === $fieldName)
                $return = $keyOperatorValue->getValue();
        }

        return $return;
        // here is some error ...
        // return (array_key_exists($tableFieldName, $this->fieldNamesOpsValues[$tableFieldName])) ?  $this->fieldNamesOpsValues[$tableFieldName]->getValue() : null;

    }



    public function getAllSelectionSettings():array
    {
        $return = [];
        // $keyOperatorValue = new KeyOperatorValueType();
        // $keyOperatorValue->getValue()

        foreach ($this->fieldNamesOpsValues as $fieldName  => $keyOperatorValue) {
            $return[$fieldName] = [$keyOperatorValue->getTableFieldName(), $keyOperatorValue->getOperator(), $keyOperatorValue->getValue()];
        }

        return $return;
    }


    /**
     * @return array <pre>App\Types\KeyOperatorValueType[]</pre>
     */
    public function getAllKeysOpValues():array
    {
        return $this->fieldNamesOpsValues;
    }

}
