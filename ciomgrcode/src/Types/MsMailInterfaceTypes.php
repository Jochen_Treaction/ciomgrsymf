<?php


namespace App\Types;


use App\Services\MsMailService;

/**
 * MIO, AOI, MSMAIL
 * Class MsMailInterfaceTypes
 * @package App\Types
 */
class MsMailInterfaceTypes
{
	public $ccRecipientEmailList = '';
	public $messageBody = "string, mandatory";
	public $messageSubject = "string, mandatory";
	public $recipientEmailList = '';
	public $recipientIsBcc = false;
	public $senderEmailAddress = "string, optional";
	public $senderFullname = "string, optional";
	public $senderPassword = "string, optional, encrypted";
	public $smtpPort = 25;
	public $smtpServer = "string, optional";

	private $configured;
	private $msMail;


	public function __construct(MsMailService $msMail)
	{
		$this->configured = false;
		$this->msMail = $msMail;

		// get mailInterface properties from MSMAIL ()
		$mailParameters = $msMail->getMailParameters();

		// set own class properties by the ones obtained from MSMAIL
		$this->init($mailParameters);
	}


	/**
	 * @return array [ccRecipientEmailList,messageBody,messageSubject,recipientEmailList,recipientIsBcc,senderEmailAddress,senderFullname,senderPassword,smtpPort,smtpServer]<br>with basic configuration
	 */
	public function getInitialConfigArray(): array
	{
		return [
			$this->ccRecipientEmailList => '',
			$this->messageBody => '',
			$this->messageSubject => 'E-Mail from Marketing-In-One',
			$this->recipientEmailList => '',
			$this->recipientIsBcc => false,
			$this->senderEmailAddress => '',
			$this->senderFullname => 'Marketing-In-One',
			$this->senderPassword => '',
			$this->smtpPort => '',
			$this->smtpServer => '',
		];
	}


	/**
	 * @return MsMailInterfaceTypes with  basic configuration
	 */
	public function getInitialConfigObject(): MsMailInterfaceTypes
	{
		$obj = clone $this;
		$obj->ccRecipientEmailList = '';
		$obj->messageBody = '';
		$obj->messageSubject = 'E-Mail from Marketing-In-One';
		$obj->recipientEmailList = '';
		$obj->recipientIsBcc = false;
		$obj->senderEmailAddress = '';
		$obj->senderFullname = 'Marketing-In-One';
		$obj->senderPassword = '';
		$obj->smtpPort = '';
		$obj->smtpServer = '';
		return $obj;
	}


	/**
	 * @param array $myconfig
	 * @return $this
	 */
	public function setConfiguration(array $myconfig): MsMailInterfaceTypes
	{
		foreach ($myconfig as $k => $v) {
			$this->{$k} = $v;
		}

		$this->configured = true;
		return $this;
	}


	/**
	 * @return $this|null
	 */
	public function getConfiguration(): ?MsMailInterfaceTypes
	{
		if ($this->configured) {
			return $this;
		} else {
			return null;
		}
	}


	/**
	 * @return array
	 */
	public function getConfigurationArray(): array
	{
		if ($this->configured) {
			$config = [];
			foreach (get_object_vars($this) as $varname => $value) {
				$rp = new \ReflectionProperty($this,$varname);
				if($rp->isPublic()) {
					$config[$varname] = $this->{$varname};
				}
			}
			return $config;
		} else {
			return [];
		}
	}


	private function init(array $mailParameters = null)
	{
		if (!empty($mailParameters['mailInterface'])) {
			foreach ($mailParameters['mailInterface'] as $type => $value) {
				$this->{$type} = $value;
			}
			$this->configured = true;
		} else {
			$this->configured = false;
		}
	}
}
