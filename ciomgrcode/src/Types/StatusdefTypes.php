<?php


namespace App\Types;


class StatusdefTypes
{
    public const ACTIVE = 'active';
    public const ARCHIVED = 'archived';
    public const AVAILABLE = 'available';
    public const DRAFT = 'draft';
    public const BLACKLISTED = 'blacklisted';
    public const BLOCKED = 'blocked';
    public const CONFIRMED = 'confirmed';
    public const DEFINED = 'defined';
    public const DELETED = 'deleted';
    public const DONE = 'done';
    public const FAILED = 'failed';
    public const FRAUD = 'fraud';
    public const INACTIVE = 'inactive';
    public const INSTALLED = 'installed';
    public const MARKEDFORDELETION = 'markedfordeletion';
    public const NEW = 'new';
    public const PREPARED = 'prepared';
    public const REGISTERED = 'registered';
    public const REMOVED = 'removed';
    public const UNSUBSCRIBED = 'unsubscribed';
    public const WORKING = 'working';

    public function __construct()
    {
    }


}
