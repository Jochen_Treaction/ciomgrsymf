<?php

namespace App\Types;

use Psr\Log\LoggerInterface;

class LocalizationLanguageWizardTypes
{
    public const OBJ_REG_META_KEY = 'localized-lang-optin-text';

    private const DFLT_EC_OPT_TEXT = 'Ich bin damit einverstanden, dass der $Imprint$ meine E-Mail-Adresse verwendet,um mir in Zukunft per E-Mail interessante Angebote für Produkte, die zu den erworbenen Produkten ähnlich sind, zukommen zu lassen. Ich kann dieser Verwendung jederzeit widersprechen, etwa durch eine Entfernung des Häkchens,aber auch durch eine E-Mail an $DPP-MailTo$. Kosten (außer den Übermittlungskosten nach den Basistarifen) werden für einen derartigen Widerspruch nicht erhoben .Weitere Informationen finden Sie in der $DPP$';
    private const DFLT_NL_OPT_TEXT = 'Ich stimme den $DPP$ zu';
    public const DFLT_LANG = 'German';

    /**
     * @var array[]
     * @author Pradeep
     */
    private array $wizardPayload;

    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->__invoke();
    }


    /**
     * payload config which is ubtained from language wizard.
     * @author Pradeep
     */
    public function __invoke()
    {
        $this->wizardPayload = [
            'lang_id' => '',
            'local_lang_id' => '',
            'name' => '',
            'objectregister_id' => '',
            'company_id' => '',
            'iso_code' => '',
            'description' => '',
            'is_default' => '',
            'user_id' => '',
            'gtc' => [
                'vrbl' => '',
                'nchrtxt' => '',
                'url' => '',
            ],
            'mprnt' => [
                'vrbl' => '',
                'nchrtxt' => '',
                'url' => '',
            ],
            'dpp' => [
                'vrbl' => '',
                'nchrtxt' => '',
                'url' => '',
                'mail_vrbl' => '',
                'mail' => '',
            ],
            'optn_txt' => [
                'nwslttr' => '',
                'ecmrce' => '',
            ],
        ];
    }

    /**
     * builds the json body with the values from localization of languages wizard.
     * - json body is stored inside objectregister meta.
     * @param int $userId
     * @param int $companyId
     * @param int $objRegId
     * @param bool $langLocalId
     * @param array $language
     * @param array $localLangValue
     * @return string
     * @author Pradeep
     */
    public function buildJsonBody(
        int $userId,
        int $companyId,
        int $objRegId,
        int $langLocalId,
        array $language,
        array $localLangValue
    ): string {
        $body = $this->wizardPayload;
        $body[ 'lang_id' ] = (int)$language[ 'id' ];
        $body[ 'user_id' ] = $userId;
        $body[ 'local_lang_id' ] = $langLocalId;
        $body[ 'lang_name' ] = trim($language[ 'name' ]);
        $body[ 'objectregister_id' ] = $objRegId;
        $body[ 'description' ] = trim($localLangValue[ 'description' ]) ?? '';
        $body[ 'company_id' ] = $companyId;
        $body[ 'iso_code' ] = trim($language[ 'iso_code' ]);
        $body[ 'is_default' ] = $this->isLangDefault($localLangValue);
        $body[ 'gtc' ] = [
            'vrbl' => trim($localLangValue[ 'gtc' ][ 'vrbl' ]),
            'nchrtxt' => trim($localLangValue[ 'gtc' ][ 'nchrtxt' ]),
            'url' => trim($localLangValue[ 'gtc' ][ 'url' ]),
            'gtc_url' =>
                $this->buildGTCVariable(
                    trim($localLangValue[ 'gtc' ][ 'nchrtxt' ]),
                    trim($localLangValue[ 'gtc' ][ 'url' ])
                ),
        ];
        $body[ 'mprnt' ] = [
            'vrbl' => trim($localLangValue[ 'mprnt' ][ 'vrbl' ]),
            'nchrtxt' => trim($localLangValue[ 'mprnt' ][ 'nchrtxt' ]),
            'url' => trim($localLangValue[ 'mprnt' ][ 'url' ]),
            'mprnt_url' =>
                $this->buildImprint(
                    trim($localLangValue[ 'mprnt' ][ 'nchrtxt' ]),
                    trim($localLangValue[ 'mprnt' ][ 'url' ])
                ),
        ];
        $body[ 'dpp' ] = [
            'vrbl' => trim($localLangValue[ 'dpp' ][ 'vrbl' ]),
            'nchrtxt' => trim($localLangValue[ 'dpp' ][ 'nchrtxt' ]),
            'url' => trim($localLangValue[ 'dpp' ][ 'url' ]),
            'dpp_url' => $this->buildDPPUrl(
                trim($localLangValue[ 'dpp' ][ 'nchrtxt' ]),
                trim($localLangValue[ 'dpp' ][ 'url' ])
            ),
            'mail_vrbl' => trim($localLangValue[ 'dpp' ][ 'mail_vrbl' ]),
            'mail' => trim($localLangValue[ 'dpp' ][ 'mail' ]),
            'dpp_mail' => $this->buildEMailAnchorTag(
                trim($localLangValue[ 'dpp' ][ 'mail' ])
            ),
        ];
        $body[ 'optn_txt' ] = [
            'nwslttr' => trim($localLangValue[ 'optn_txt' ][ 'nwslttr' ]),
            'ecmrce' => trim($localLangValue[ 'optn_txt' ][ 'ecmrce' ]),
        ];
        $body[ 'translated_optn_txt' ] = [
            'nwslttr' => $this->newslttrOptTxt(
                $body[ 'optn_txt' ][ 'nwslttr' ],
                $body[ 'dpp' ][ 'vrbl' ],
                $body[ 'dpp' ][ 'dpp_url' ]
            ),
            'ecmrce' => $this->ecOptTxt(
                $body[ 'gtc' ],
                $body[ 'mprnt' ],
                $body[ 'dpp' ],
                $body[ 'optn_txt' ][ 'ecmrce' ]
            ),
        ];

        return $this->toJson($body);
    }

    /**
     * parses the Payload from wizard (localization language wizard) verifies whether
     * the value of 'is_default' is set or not .
     * @param array $wizardValues
     * @return bool
     * @author Pradeep
     */
    public function isLangDefault(array $wizardValues): bool
    {
        if (empty($wizardValues) ||
            !isset($wizardValues[ 'is_default' ]) ||
            $wizardValues[ 'is_default' ] !== 'on') {
            return false;
        }
        return true;
    }

    /**
     * Build anchor tag and Url for GTC variable.
     * @param string $anchorText
     * @param string $url
     * @return string
     * @author Pradeep
     */
    private function buildGTCVariable(string $anchorText, string $url): string
    {
        return $this->buildURLWithAnchorText($anchorText, $url);
    }

    /**
     * Helper function for forming the Url and anchor tag for HTML.
     * @param string $anchorText
     * @param string $url
     * @return string
     * @author Pradeep
     */
    private function buildURLWithAnchorText(string $anchorText, string $url): string
    {
        return '<a href="' . $url . '" target="_blank" >' . $anchorText . '</a>';
    }

    /**
     * Build anchor tag and Url for Imprint variable.
     * @param string $anchorText
     * @param string $url
     * @return string
     * @author Pradeep
     */
    private function buildImprint(string $anchorText, string $url): string
    {
        return $this->buildURLWithAnchorText($anchorText, $url);
    }

    /**
     * Build anchor tag and Url for DPP variable.
     * @param string $anchorText
     * @param string $url
     * @return string
     * @author Pradeep
     */
    private function buildDPPUrl(string $anchorText, string $url): string
    {
        return $this->buildURLWithAnchorText($anchorText, $url);
    }


    /**
     * Build anchor tag for the eMail
     * @param string $eMail
     * @return string
     * @author Pradeep
     */
    private function buildEMailAnchorTag(string $eMail): string
    {
        return '<a href = "mailto: ' . $eMail . '">' . $eMail . '</a>';
    }


    /**
     * @param string $optinText
     * @param string $dppVariable
     * @param string $dppValue
     * @return array|string|string[]
     * @author Pradeep
     */
    private function newslttrOptTxt(string $optinText, string $dppVariable, string $dppValue)
    {
        return $this->searchAdReplaceTxt($dppVariable, $dppValue, $optinText);
    }

    /**
     * searchAdReplaceTxt uses php Str_replace function to search and replace the text
     * @param string $variable
     * @param string $replaceValue
     * @param string $text
     * @return array|string|string[]
     * @author Pradeep
     */
    private function searchAdReplaceTxt(string $variable, string $replaceValue, string $text)
    {
        return str_replace($variable, $replaceValue, $text);
    }

    /**
     * - search and replace each and every constant, which are defined in the wizard.
     * @param array $gtc
     * @param array $imprint
     * @param array $dpp
     * @param string $ecOptText
     * @return array|string|string[]
     * @author Pradeep
     */
    private function ecOptTxt(array $gtc, array $imprint, array $dpp, string $ecOptText)
    {

        // search and replace the value of gtc_Url configured in wizard.
        $t = $this->searchAdReplaceTxt($gtc[ 'vrbl' ], $gtc[ 'gtc_url' ], $ecOptText);
        // search and replace the value of imprint_url configured in wizard
        $t2 = $this->searchAdReplaceTxt($imprint[ 'vrbl' ], $imprint[ 'mprnt_url' ], $t);
        // search and replace the value of dpp_url configured in wizard.
        $t3 = $this->searchAdReplaceTxt($dpp[ 'vrbl' ], $dpp[ 'dpp_url' ], $t2);
        // search and replace the value of E-Mail with configured in wizard.
        return $this->searchAdReplaceTxt($dpp[ 'mail_vrbl' ], $dpp[ 'dpp_mail' ], $t3);
    }

    /**
     * Helper function to convert array to json.
     *
     * @param array $arr
     * @return string
     * @author Pradeep
     */
    private function toJson(array $arr): string
    {
        try {
            if (empty($arr)) {
                return '';
            }
            return json_encode($arr, JSON_THROW_ON_ERROR);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return '';
        }
    }

    /**
     * Helper function to build config parameters for default Language.
     * - This is called when accepting DPA.
     * @param int $companyId
     * @param int $userId
     * @param string $companyName
     * @param string $gtcUrl
     * @param string $dppUrl
     * @param string $imprintUrl
     * @param string $email
     * @param array $langDetails
     * @return array
     * @author Pradeep
     */
    public function buldCnfgFrDfltLang(
        int $companyId,
        int $userId,
        string $companyName,
        string $gtcUrl,
        string $dppUrl,
        string $imprintUrl,
        string $email,
        array $langDetails
    ): array {
        $defaultConfig[ 'lang_id' ] = $langDetails[ 'id' ];
        $defaultConfig[ 'local_lang_id' ] = 0;
        $defaultConfig[ 'name' ] = $langDetails[ 'name' ];
        $defaultConfig[ 'objectregister_id' ] = 0;
        $defaultConfig[ 'lang_name' ] = $langDetails[ 'name' ];
        $defaultConfig[ 'company_id' ] = $companyId;
        $defaultConfig[ 'iso_code' ] = $langDetails[ 'iso_code' ];
        $defaultConfig[ 'description' ] = 'Default language created while creating account.';
        $defaultConfig[ 'is_default' ] = "on";
        $defaultConfig[ 'user_id' ] = $userId;
        $defaultConfig[ 'gtc' ] = [
            'vrbl' => '$GTC$',
            'nchrtxt' => 'AGB',
            'url' => $gtcUrl,
        ];
        $defaultConfig[ 'mprnt' ] = [
            'vrbl' => '$Imprint$',
            'nchrtxt' => $companyName,
            'url' => $imprintUrl,
        ];
        $defaultConfig[ 'dpp' ] = [
            'vrbl' => '$DPP$',
            'nchrtxt' => 'Datenschutzerklärung',
            'url' => $dppUrl,
            'mail_vrbl' => '$DPP-MailTo$',
            'mail' => $email,
        ];
        $defaultConfig[ 'optn_txt' ] = [
            'nwslttr' => self::DFLT_NL_OPT_TEXT,
            'ecmrce' => self::DFLT_EC_OPT_TEXT,
        ];
        return $defaultConfig;
    }

}