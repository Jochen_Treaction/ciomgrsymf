<?php

namespace App\Types;

class ContactPermissionTypes
{

    public const PERMISSION_NONE = 1;
    public const PERMISSION_SINGLE_OPT_IN = 2;
    public const PERMISSION_CONFIRMED_OPT_IN = 3;
    public const PERMISSION_DOUBLE_OPT_IN = 4;
    public const PERMISSION_DOUBLE_OPT_IN_SINGLE_USER_TRACKING = 5;

    /**
     * @return int[]
     * @author Pradeep
     */
    public function getSupportedContactPermission(): array
    {
        return [
            self::PERMISSION_NONE,
            self::PERMISSION_SINGLE_OPT_IN,
            self::PERMISSION_CONFIRMED_OPT_IN,
            self::PERMISSION_DOUBLE_OPT_IN,
            self::PERMISSION_DOUBLE_OPT_IN_SINGLE_USER_TRACKING,
        ];
    }

    /**
     * @param int $permission
     * @return string
     * @author Pradeep
     */
    public function getPermission(int $permission)
    {
        if ($permission === self::PERMISSION_NONE) {
            return 'No Permission';
        }
        if ($permission === self::PERMISSION_SINGLE_OPT_IN) {
            return 'Single Opt-In';
        }
        if ($permission === self::PERMISSION_CONFIRMED_OPT_IN) {
            return 'Confirmed Opt-In';
        }
        if ($permission === self::PERMISSION_DOUBLE_OPT_IN) {
            return 'Double Opt-In';
        }
        if ($permission === self::PERMISSION_DOUBLE_OPT_IN_SINGLE_USER_TRACKING) {
            return 'Double Opt-In Single User Tracking';
        }
        return 'No Permission';
    }

    /**
     * Helper function for 'parseStandardFields'
     * Checks if the permission is DoubleOptIn or SingleOptin or None .
     * @param int $permission
     * @return bool
     * @author Pradeep
     */
    public function hasDoubleOptInPermision(int $permission): bool
    {
        if ($permission === (int)self::PERMISSION_DOUBLE_OPT_IN_SINGLE_USER_TRACKING) {
            return true;
        }
        if ($permission === (int)self::PERMISSION_DOUBLE_OPT_IN) {
            return true;
        }
        return false;
    }
}