<?php


namespace App\Types;

//@internal- This types is for mapping UI and database names for the seed_pool-type
class SeedPoolTypes
{
    /**
     * @const MONITORING = "monitoring" nach monitoring
     */
    public const MONITORING = "monitoring";

    /**
     * @const TESTING = "testing" nach testing
     */
    public const TESTING = "testing";

    /**
     * @const MONITORINGANDTESTING = "monitoring&testing" both monitoring and testing
     */
    public const MONITORINGANDTESTING = "monitoring&testing";

    /**
     * @const MONITORING_UI = "Monitoring" nach monitoring
     */
    public const MONITORING_UI = "Monitoring";

    /**
     * @const TESTING_UI = "Testing" nach testing
     */
    public const TESTING_UI = "Testing";

    /**
     * @const MONITORINGANDTESTING_UI = "Monitoring & Testing" both monitoring and testing
     */
    public const MONITORINGANDTESTING_UI = "Monitoring & Testing";

    /**
     * @const seed pool type mapping
     */
    const SEED_POOL_TYPE_MAPPING_DB_TO_UI = [
        self::MONITORING => self::MONITORING_UI,
        self::TESTING=> self::TESTING_UI,
        self::MONITORINGANDTESTING=> self::MONITORINGANDTESTING_UI
    ];

    /**
     * @const seed pool type mapping
     */
    const SEED_POOL_TYPE_MAPPING_UI_TO_BD = [
        self::MONITORING_UI => self::MONITORING,
        self::TESTING_UI => self::TESTING,
        self::MONITORINGANDTESTING_UI => self::MONITORINGANDTESTING
    ];



    /**
     * @return array[]
     * @internal all possible seed pool types for UI
     * @auther aki
     */
    public static function getUITypes(): array
    {
        return [
            self::MONITORING_UI,
            self::TESTING_UI,
            self::MONITORINGANDTESTING_UI
        ];
    }

    /**
     * @return array[]
     * @internal  All possible seed pool types for database
     * @auther aki
     */
    public static function getDataBaseTypes(): array
    {
        return [
            self::MONITORING,
            self::TESTING,
            self::MONITORINGANDTESTING
        ];
    }

    /**
     * @return array[]
     * @internal mapping UI types to database types
     * @auther aki
     */
    public static function getDataBaseTypesMapping(): array
    {
        return self::SEED_POOL_TYPE_MAPPING_UI_TO_BD;
    }


    /**
     * @return array[]
     * @internal mapping database types to UI types
     * @auther aki
     */
    public static function getUITypesMapping(): array
    {
        return self::SEED_POOL_TYPE_MAPPING_DB_TO_UI;
    }


    /**
     * @param string $type
     * @return string
     * @internal converts UI name to database name
     */
    public static function convertUITypesToDataBaseTypes(string $type): string
    {
        return self::SEED_POOL_TYPE_MAPPING_UI_TO_BD[$type];
    }

    /**
     * @param string $type
     * @return string
     * @internal converts Database name to UI name
     */
    public static function convertDatabaseTypesToUITypes(string $type): string
    {
        return self::SEED_POOL_TYPE_MAPPING_DB_TO_UI[$type];
    }

}
