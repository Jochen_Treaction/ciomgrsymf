<?php


namespace App\Types;

/**
 * Class UserStatusTypes
 * @package App\Types
 * @see Database mio.users.status
 */
class UserStatusTypes
{
    public const NEW = "new";
    public const CONFIRMED = "confirmed";
    public const ACTIVE = "active";
    public const BLOCKED = "blocked";
    public const INACTIVE = "inactive";
    public const BLOCKBYADMIN = 'blockedbyadmin';

    /**
     * @return string[] all possible status type strings of App\Types\UserStatusTypes ['new', 'confirmed', 'active' ,'blocked', 'inactive']
     * @auther jsr
     */
    public static function getTypes(): array
    {
        return [
            self::NEW,
            self::CONFIRMED,
            self::ACTIVE,
            self::BLOCKED,
			self::INACTIVE,
        ];
    }


	/**
	 * @return string json string containing all status type  strings of \App\UserStatusTypes "['new', 'confirmed', 'active' ,'blocked', 'inactive']"
	 */
    public static function getJsonTypes():string
	{
		return json_encode(self::getTypes());
	}


    /**
     * @param string UserStatusType
     * @return bool
     * @author jsr
     */
    public static function isCorrectType(string $userStatusType): bool
    {
        return in_array($userStatusType, self::getTypes());
    }

}

