<?php


namespace App\Types;


use JsonException;

class FraudSettingsTypes
{
    public const ACC_FRAUD_SETTINGS = 'fraud_settings';


    //Todo : Add Relation (for objectregister Id, CompanyId) and settings to type array.
    public function __construct()
    {
        $this->type = [
            "maxLeadsPerDomain" => 100, // WEB-6025 Increase Fraudsettings value from 3 to 100
            "maxLeadsPerIp" => 100, // WEB-6025 Increase Fraudsettings value from 3 to 100
            "urlToReplaceLinks" => "",
            "geoBlocking" => "false",
            "urlToHidePage" => "",
            "useIntermediatePage" => "",
            "scrambleUrl" => "false",
        ];
    }

    public function getDefaultFraudSettingsForAccount(): array
    {
        return $this->type;
    }

    public function getDefaultFraudSettingsForAccountAsJson(): string
    {
        try {
            return json_encode($this->type, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            return '';
        }
    }

    public function getDefaultFraudSettingsForCampaign(): array
    {
        return [];
    }
}