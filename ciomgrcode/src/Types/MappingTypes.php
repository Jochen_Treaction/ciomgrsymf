<?php


namespace App\Types;


class MappingTypes
{
    private $type;
    private $typeDesc;
    public $uniqueObjectName;


    public function __construct()
    {
        $this->uniqueObjectName = 'mapping';

        $this->type = [
            "contact" => [
                "standard" => [
                        "fieldName"=>"",
                        "required"=> "",
                        "datatype"=> "",
                        "regex"=> "",
                    ],
                "custom" =>[
                    "fieldName"=>"",
                    "required"=> "",
                    "datatype"=> "",
                    "regex"=> "",
                ],
            ]
        ];
        //todo:make type description
        $this->typeDesc = [];

    }

    /**
     * @return string
     */
    public function getJson(): string
    {
        return json_encode($this->type);
    }


    /**
     * @return array
     */
    public function getArray(): array
    {
        return $this->type;
    }

}