<?php

namespace App\Types;


class AdvertorialImageTypes
{
    private int $advertorials_id;
    private string $advertorialSurrogateId;
    private string $original_url;
    private string $url;
    private string $original_image_name;
    private string $image_hash_name;
    private string $image_content;
    private ?string $mime_type;
    private int $scan_line_number;
    private bool $scan_status;
    private string $scan_status_reason;
    private ?string $transfer_status;


    public function __construct()
    {
        //$this->transfer_status = "<span class='bad'>no status provided</span>";
    }


    /**
     * @return int
     */
    public function getAdvertorialSurrogateId(): string
    {
        return $this->advertorialSurrogateId;
    }

    /**
     * @param int $advertorialSurrogateId
     */
    public function setAdvertorialSurrogateId(string $advertorialSurrogateId): self
    {
        $this->advertorialSurrogateId = $advertorialSurrogateId;
        return $this;
    }


    /**
     * @return int
     */
    public function getAdvertorialsId(): int
    {
        return $this->advertorials_id;
    }


    /**
     * @param int $advertorials_id
     */
    public function setAdvertorialsId(int $advertorials_id): self
    {
        $this->advertorials_id = $advertorials_id;
        return $this;
    }


    /**
     * @return string
     */
    public function getOriginalUrl(): string
    {
        return $this->original_url;
    }


    /**
     * @param string $original_url
     */
    public function setOriginalUrl(string $original_url): self
    {
        $this->original_url = $original_url;
        return $this;
    }


    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }


    /**
     * @param string $url
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }


    /**
     * @return string
     */
    public function getOriginalImageName(): string
    {
        return $this->original_image_name;
    }


    /**
     * @param string $original_image_name
     */
    public function setOriginalImageName(string $original_image_name): self
    {
        $this->original_image_name = $original_image_name;
        return $this;
    }


    /**
     * @return string
     */
    public function getImageHashName(): string
    {
        return $this->image_hash_name;
    }


    /**
     * @param string $image_hash_name
     */
    public function setImageHashName(string $image_hash_name): self
    {
        $this->image_hash_name = $image_hash_name;
        return $this;
    }


    /**
     * @return string
     */
    public function getImageContent(): string
    {
        return $this->image_content;
    }


    /**
     * @param string $image_content
     */
    public function setImageContent(string $image_content): self
    {
        $this->image_content = $image_content;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mime_type;
    }


    /**
     * @param string|null $mime_type
     */
    public function setMimeType(?string $mime_type): self
    {
        $this->mime_type = $mime_type;
        return $this;
    }


    /**
     * @return int
     */
    public function getScanLineNumber(): int
    {
        return $this->scan_line_number;
    }


    /**
     * @param int $scan_line_number
     */
    public function setScanLineNumber(int $scan_line_number): self
    {
        $this->scan_line_number = $scan_line_number;
        return $this;
    }


    /**
     * @return bool
     */
    public function isScanStatus(): bool
    {
        return $this->scan_status;
    }


    /**
     * @param bool $scan_status
     */
    public function setScanStatus(bool $scan_status): self
    {
        $this->scan_status = $scan_status;
        return $this;
    }


    /**
     * @return string
     */
    public function getScanStatusReason(): string
    {
        return $this->scan_status_reason;
    }


    /**
     * @param string $scan_status_reason
     */
    public function setScanStatusReason(string $scan_status_reason): self
    {
        $this->scan_status_reason = $scan_status_reason;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getTransferStatus(): ?string
    {
        return $this->transfer_status;
    }


    /**
     * @param string|null $transfer_status
     */
    public function setTransferStatus(?string $transfer_status): self
    {
        $this->transfer_status = $transfer_status;
        return $this;
    }


    /**
     * @return array of AdvertorialImageTypes properties with values
     */
    public function toArray():array
    {
        file_put_contents(__DIR__.'/../../var/log/imgLog.log', date('d.m.Y H:i:s')."\n", FILE_APPEND );
        $array = [];
        foreach ($this as $k => $v) {
            file_put_contents(__DIR__.'/../../var/log/imgLog.log', "$k : $v\n", FILE_APPEND);
            $array[$k] = $v;
        }
        return $array;
    }

}
