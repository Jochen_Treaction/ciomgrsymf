<?php


namespace App\Types;


use JsonException;
use Monolog\Logger;

class WebhookWizardTypes
{
    private $type;

    public function __construct()
    {
        $this->type = [

            "relation" => [
                "company_name" => '',
                "company_id" => '',
                "project_id" => '',
            ],

            "settings" => [
                "name" => [
                    "phpdatatype" => "string",
                    "pattern" => "/^[a-zA-Z0-9_]+$/",
                    "value" => "",
                    "filterValidate" => "",
                    "filterSanitize" => "FILTER_SANITIZE_STRING",
                ],
                "description" => [
                    "phpdatatype" => "string",
                    "pattern" => "",
                    "value" => "",
                    "filterValidate" => "",
                    "filterSanitize" => "FILTER_SANITIZE_STRING",
                ],
                "beginDate" => [
                    "phpdatatype" => "string",
                    "pattern" => "",
                    "value" => "",
                    "filterValidate" => "",
                    "filterSanitize" => "",
                ],
                "expireDate" => [
                    "phpdatatype" => "string",
                    "pattern" => "",
                    "value" => "",
                    "filterValidate" => "",
                    "filterSanitize" => "",
                ],
            ],

            "mapping" => [
                'newsletter' => [
                    [
                        "marketing-in-one" => "Contact.email",
                        "html_form" => "Contact_email",
                        "data_type" => "Email",
                    ],
                    [
                        "marketing-in-one" => "Contact.firstname",
                        "html_form" => "Contact_firstname",
                        "data_type" => "Text",
                    ],
                    [
                        "marketing-in-one" => "Contact.lastname",
                        "html_form" => "Contact_lastname",
                        "data_type" => "Text",
                    ],
                ],
                'eCommerce' => [
                    [
                        "marketing-in-one" => "Contact.email",
                        "html_form" => "Contact_email",
                        "data_type" => "Email",
                    ],
                    [
                        "marketing-in-one" => "Contact.firstname",
                        "html_form" => "Contact_firstname",
                        "data_type" => "Text",
                    ],
                    [
                        "marketing-in-one" => "Contact.lastname",
                        "html_form" => "Contact_last_name",
                        "data_type" => "Text",
                    ],
                    [
                        "marketing-in-one" => "Contact.street",
                        "html_form" => "Contact_street",
                        "data_type" => "Text",
                    ],
                    [
                        "marketing-in-one" => "Contact.house_number",
                        "html_form" => "Contact_house_number",
                        "data_type" => "Text",
                    ],
                    [
                        "marketing-in-one" => "Contact.postal_code",
                        "html_form" => "Contact_postal_code",
                        "data_type" => "Text",
                    ],
                    [
                        "marketing-in-one" => "Contact.city",
                        "html_form" => "Contact_city",
                        "data_type" => "Text",
                    ],
                    [
                        "marketing-in-one" => "Contact.country",
                        "html_form" => "Contact_country",
                        "data_type" => "Text",
                    ],
                    [
                        "marketing-in-one" => "Contact.phone",
                        "html_form" => "Contact_phone",
                        "data_type" => "Phone",
                    ],
                    [
                        "marketing-in-one" => "Contact.permission",
                        "html_form" => "Contact_permission",
                        "data_type" => "Integer",
                    ],
                    [
                        "marketing-in-one" => "Contact.last_order_date",
                        "html_form" => "Contact_last_order_date",
                        "data_type" => "Date",
                    ],
                    [
                        "marketing-in-one" => "Contact.smart_tags",
                        "html_form" => "Contact_smart_tags",
                        "data_type" => "List",
                    ],
                    [
                        "marketing-in-one" => "Contact.total_order_net_value",
                        "html_form" => "Contact_total_order_net_value",
                        "data_type" => "Decimal",
                    ],
                ],
            ],
        ];
    }

    /**
     * @param string $companyName
     * @param int $companyId
     * @param string $projectId
     * @return bool
     * @author Pradeep
     */
    public function initialize(string $companyName, int $companyId, string $projectId): bool
    {
        if (empty($companyName) || $companyId <= 0 || empty($projectId)) {
            return false;
        }
        $this->type[ 'relation' ][ 'company_name' ] = $companyName;
        $this->type[ 'relation' ][ 'company_id' ] = $companyId;
        $this->type[ 'relation' ][ 'project_id' ] = $projectId;
        return true;
    }

    /**
     * @param string $webhookName
     * @return array
     * @author Pradeep
     */
    public function getDefaultConfigForeCommerceWebhook(string $webhookName): ?array
    {
        try {
            $eCommerce [ 'relation' ] = $this->getRelation();
            $eCommerce [ 'settings' ] = $this->setSettings($webhookName);
            $eCommerce[ 'mapping' ] = base64_encode(json_encode($this->getMappingForeCommerceWebhook(),
                JSON_THROW_ON_ERROR));
            return $eCommerce;
        } catch (JsonException $e) {
            return null;
        }

    }

    private function getRelation(): ?array
    {
        return $this->type[ 'relation' ];
    }

    /**
     * @param string $webhookName
     * @param string $description
     * @param string $beginDate
     * @param string $expireDate
     * @return array|null
     * @author Pradeep
     */
    private function setSettings(
        string $webhookName,
        string $description = '',
        string $beginDate = '',
        string $expireDate = ''
    ): array {
        // TODO :: Update Filter validation with filter_var,  sanitization and pattern
        /*        $this->type[ 'settings' ][ 'name' ][ 'value' ] = preg_match($this->type[ 'settings' ][ 'name' ][ 'pattern' ],
                    $webhookName) ? $webhookName : '';*/
        $this->type[ 'settings' ][ 'name' ][ 'value' ] = $webhookName;
        $this->type[ 'settings' ][ 'description' ][ 'value' ] = $description;
        $this->type[ 'settings' ][ 'beginDate' ][ 'value' ] = $beginDate;
        $this->type[ 'settings' ][ 'expireDate' ][ 'value' ] = $expireDate;
        return $this->type[ 'settings' ];
    }

    /**
     * @return array
     * @author Pradeep
     */
    private function getMappingForeCommerceWebhook(): array
    {
        return $this->type[ 'mapping' ][ 'eCommerce' ];
    }

    /**
     * @param string $webhookName
     * @return array|null
     * @author Pradeep
     */
    public function getDefaultConfigForNewsLetterWebhook(string $webhookName): ?array
    {
        try {
            $newsLetter[ 'relation' ] = $this->getRelation();
            $newsLetter[ 'settings' ] = $this->setSettings($webhookName);
            $newsLetter[ 'mapping' ] = base64_encode(json_encode($this->getMappingForNewsLetterWebhook(),
                JSON_THROW_ON_ERROR));
            return $newsLetter;
        } catch (JsonException $e) {
            var_dump($e->getMessage());
            return null;
        }
    }

    /**
     * @return array
     * @author Pradeep
     */
    private function getMappingForNewsLetterWebhook(): array
    {
        return $this->type[ 'mapping' ][ 'newsletter' ];
    }

    /**
     * @param string $pattern
     * @param string $value
     * @return bool
     * @author Pradeep
     */
    public function validateWithPattern(string $pattern, string $value): bool
    {
        $isValid = false;
        if (empty($pattern) || empty($value)) {
            return $isValid;
        }
        if (preg_match($pattern, $value)) {
            $isValid = true;
        }
        return $isValid;
    }
}