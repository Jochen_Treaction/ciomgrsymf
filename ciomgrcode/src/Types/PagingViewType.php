<?php

namespace App\Types;

class PagingViewType
{
    private string $pagingView;

    public function __construct()
    {
        $this->pagingView = '';
    }


    /**
     * @return string|null
     */
    public function getPagingView(): ?string
    {
        return $this->pagingView;
    }


    /**
     * @param string|null $pagingView
     */
    public function setPagingView(?string $pagingView): void
    {
        $this->pagingView = $pagingView;
    }


}
