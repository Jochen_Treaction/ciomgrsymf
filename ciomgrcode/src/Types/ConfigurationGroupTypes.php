<?php

namespace App\Types;

/**
 * Class ConfigurationGroupTypes
 * @author jsr
 */
class ConfigurationGroupTypes
{
    public const PROJECTS = 'projects';
    public const PAGES = 'pages';
    public const WEBHOOKS = 'webhooks';
}
