<?php

namespace App\Interfaces;

use App\Entity\Leads;

interface LeadsInterface
{
    public const LastName = 'lastname';
    public const FirstName = 'firstname';
    public const Email = 'email';
    public const Salutation = 'salutation';
    public const ReferenceNo = 'reference_no';
    public const AccountName = 'account_name';
    public const Street = 'street';
    public const HouseNo = 'house_number';
    public const PostalCode = 'postal_code';
    public const City = 'city';
    public const Country = 'country';
    public const Phone = 'phone';
    public const URL = 'url';
    public const Company = 'company';
    public const TrafficSource = 'traffic_source';
    public const UTMParameters = 'utm';
    public const TargetGroup = 'target_group';
    public const AffiliateID = 'affiliate_id';
    public const AffiliateSubID = 'affiliate_sub_id';
    public const IP = 'ip';
    public const Permission = 'permission';
    public const LastOrderNo = 'last_order_no';
    public const LastOrderDate = 'last_order_date';
    public const TotalOrderNetValue = 'total_order_net_value';
    public const LastYearOrderNetValue = 'last_year_order_net_value';
    public const LastOrderNetValue = 'last_order_net_value';
    public const SmartTags = 'smart_tags';
    public const ReferrerID = 'referrer_id';
    public const FirstOrderDate = 'first_order_date';

    public function getStandardFields(): array;

    public function getStandardFieldsFromDB(): array;

    public function getStandardFieldsForWebhook(): array;

}