# Example Naming Conventions for TWIG Templates
#### - CIO Manager Project -
### TWIG Template Names
    create.twig
    destroy.twig
    edit.twig
    index.twig
    show.twig
    store.twig
    update.twig 

    Verb	    URI	                    Typical Method Name         Route Name
    GET         /example	                index()	                example_index
    (GET         /example/create	        create()	        example_create)
    POST	    /example	                store()	                example_store
    GET	    /example/{photo}	        show()	                example_show
    GET	    /example/{photo}/edit	edit()	                example_edit
    PUT/PATCH   /example/{photo}	        update()	        example_update
    DELETE	    /example/{photo}             destroy()	        example_destroy
