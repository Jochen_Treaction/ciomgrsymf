
// Class definition

var KTDatatableHtmlTableCampaigns = function () {
    // Private functions

    // demo initializer
    var CampaignDT = function () {

        var datatable = $('#dashbd-campaigns').DataTable({


            // DOM Layout settings
            dom: `<'row'<'col-sm-12'tr>>
                  <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

            pageLength: 10,

            lengthMenu: [10],

            sortable: true,

            pagination: true,

            language: {
                'lengthMenu': 'Display _MENU_',
            },

            // Order settings
            order: [[0, 'asc']],

            // columns definition
            columns: [
                {
                    title: '#',
                    sortable: 'asc',
                   // width: 30,
                   // type: 'number',
                    selector: false,
                    textAlign: 'center',
                }, {
                    //field: 'Name',
                    //title: 'Campaign Name',
                  //  width: 100,
                    //type: 'string',
                }, {
                    //field: 'URL ',
                    //title: 'URL',
                    //width: 150,
                   // type: 'string',
                }, {
     /*               field: 'Status',
                    title: 'status',*/
                    width: 30,
					// callback function support for column rendering
                    render: function (data, type, full, meta) {
                        var status = {
                            active: {'title': 'active', 'class': 'kt-badge--success'},
                            inactive: {'title': 'inactive', 'class': ' kt-badge--danger'},

                        };

                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
                    },
                }
            ],
        });
    };

    return {
        // Public functions
        init: function () {
            // init dmeo
            CampaignDT();
        },
    };
}();

jQuery(document).ready(function () {
    KTDatatableHtmlTableCampaigns.init();
});
