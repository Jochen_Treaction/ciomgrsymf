$(document).ready(function () {

    // get json data 
    var contacts = $('#chart-leads').data("isTest");
    var f_camps = formatData(contacts,"graph_1");

    // draw Google charts based on json data
    google.charts.load('current', { packages: ['corechart', 'bar'] });
    google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Contacts');


        data.addRows(
            f_camps,
        );

        var options = {
            title: 'Contacts generated from past 6 months in all Pages',
            colors: ['#7abe73'],
            hAxis: {
                title: 'Month',
            },
            vAxis: {
                title: 'Contact Count'
            }

        };

        var chart = new google.visualization.ColumnChart(
            document.getElementById('chart-leads'));

        chart.draw(data, options);
    }


    // PIE Chart
    var leads_per_camp = $('#chart-leads-per-camp').data("isTest");
    var f_leads_per_camp = formatData(leads_per_camp, "graph_2");

    google.charts.setOnLoadCallback(drawPieChart);
    function drawPieChart() {
        var data = google.visualization.arrayToDataTable(f_leads_per_camp);

        var options = {
            title: 'Contacts generated for each Landing Page',
           // is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart-leads-per-camp'));

        google.visualization.events.addListener(chart, 'error', function (googleError) {
            google.visualization.errors.removeError(googleError.id);
        });

        chart.draw(data, options);
    }


    /** 
     * formatData
     *  Formats the json data to array which is acceptable to Google graph API
     * @param {*} json_data 
     * @param {*} graph 
     * 
     * @return array  
     */
    function formatData(json_data, graph)  { 
        var formatData = [];
        var i = 0;
        // Validation check
        if(json_data.length == 0 || graph.trim() ==""){
            return formatData
        }
        // Parse each element in json data
        while (i < json_data.length) {
            var l_val;
            var r_val;
            // format style for graph 1
            if(graph == "graph_1"){
                l_val = json_data[i].create_month;
                r_val = parseInt(json_data[i].cnt);
            }else if(graph == "graph_2"){
                // Format style for graph 2
                if(i == 0){
                    formatData.push(['Campaigns', 'leads']);
                }
                l_val = json_data[i].campaign_name;
                r_val = parseInt(json_data[i].leads);
            }
            formatData.push([l_val, r_val])
            i++;
        }

        return formatData;
    }
});