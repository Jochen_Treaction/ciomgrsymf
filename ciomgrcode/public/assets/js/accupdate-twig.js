$(document).ready(function () {

    // check if vaild domain was entered
    $('input#website').on('blur', function checkDomain(){
        var $domain = $(this).val();
        var $domainExists = false;

        let isDomain = new RegExp( "^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)[A-Za-z]{2,6}$" );
        $patternMatched = isDomain.test($domain);
        $urlMatched = Boolean(new URL('https://' + $domain));

        if(!$patternMatched || !$urlMatched) {
            $(this).css('border-color', 'red').val('').focus();
            $('button#saveaccount').attr('disabled', true);
        } else {
            $(this).css('border-color', '#e2e5ec');
            $('button#saveaccount').removeAttr('disabled');
        }
    });


    var x_deleteTheData = false;
    var old_company_name = $('input[name="company_name"]').data('oldcompanyname');
    console.log('old_company_name =' + old_company_name);


    jQuery('button#checkdelaccount').on('click', function () {
        // Todo: feddisch implementieren WEB-3737!

        var email = $('input#delbyemail').val();
        var route = atob($(this).data('route'));
        var routeproceed = atob($(this).data('routeproceed'));


        if (email === '' || email === undefined) {
            alert('Please enter an email address');
            return false;
        }


        console.log("email  = " + email);
        console.log("route  = " + route);

        var send_data = btoa(JSON.stringify({'email': email}));
        var x = false;

        var settings = {
            url: route,
            method: "POST",
            timeout: 0,
            headers: {
                'Content-Type': "application/x-www-form-urlencoded"
            },
            data: send_data,
            success: function (data, x) {

                if (data.status === true) {
                    console.log('status=' + data.status);
                    if (data.msg !== '' || data.msg !== undefined) {
                        console.log('msg=' + data.msg);
                        x_deleteTheData = confirm(data.msg + ' Are you sure that you want to proceed?');
                        console.log("x in return confirm ist " + x_deleteTheData);
                    }
                } else {
                    alert(data.msg);
                }
            }
        };


        $.ajax(settings)
            .done(function (data) {
                console.log('DONE');
                if (x_deleteTheData === true) {
                    console.log("running deleteTheData ...");
                    var ret = deleteTheData(send_data, routeproceed);
                    console.log(ret);
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                $('div#msg_delaccbyemail').text('The email address you entered does not exist.').css('color', 'red').css('display', 'initial').fadeOut(5000);
                console.log("request failed");
                console.debug('d:' + data.toString());
            });
    });

/* listener deactivated due to requirement WEB-4088 */
/*    $('input[name="company_name"]').on('blur', function () {

        var input_company_name = $(this);

        var divmsg = $('div#company_msg');
        var company_name = $(this).val();
        var route = atob($(this).data('route'));
        var action = $(this).data('action');

        console.log(old_company_name, company_name, route);

        // exit if name is not changed
        if(old_company_name === company_name) {
            return;
        }

        var settings = {
            url: route,
            method: "POST",
            timeout: 0,
            headers: {
                'Content-Type': "application/x-www-form-urlencoded"
            },
            data: btoa(JSON.stringify({company_name : company_name, old_company_name : old_company_name, action : action})),
            success: function (data) {
                if (data.status === true) {
                    input_company_name.val(data.company_name);
                    divmsg.text(data.msg).css('color', 'green').fadeIn(1000).fadeOut(20000);
                } else {
                    input_company_name.val(old_company_name);
                    divmsg.text(data.msg).css('color', 'red').fadeIn(1000).fadeOut(20000);
                }
            }
        };

        $.ajax(settings)
            .done(function (data) {
                console.log("request to " + route + " successful");
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log("request to " + route + " failed");
            });

        // on error
        // $('div#company_msg').text(msg).css('color', 'red').fadeOut(10000);
        // on success
        // $('div#company_msg').text(msg).css('color', 'green').fadeOut(10000);
    });*/


    function deleteTheData(sendData, route) {
        console.log('deleteTheData', sendData, route);

        var settings = {
            url: route,
            method: "POST",
            timeout: 0,
            headers: {
                'Content-Type': "application/x-www-form-urlencoded"
            },
            data: sendData,
            success: function (data) {
                /*
                                'status' => $ret['status'],
                                'msg' => "the account of {$userToDeleteEmail} has been deleted. i" . __LINE__,
                                'log' => $ret['data'],
                                'redirect' => $redirect,
                 */

                if (data.status === true) {

                    console.log('status=' + data.status);
                    console.log('msg=' + data.msg);

                    if (data.msg !== '' || data.msg !== undefined) {
                        console.log('msg=' + data.msg);
                        var moreInfo = '';
                        alert(data.msg);

                        // // todo: fix the for loop, since it does not work
                        // data.log.forEach(function (item, index) {
                        //     console.log(index + ' -> ' + item + ' records deleted');
                        //     moreInfo += 'del ' + index + ' -> ' + item + ' records, ';
                        // });
                        //
                        // if (moreInfo !== '' || moreInfo !== undefined) {
                        //     alert('delete log: ' + moreInfo);
                        // }

                        if (data.redirect !== '' && data.redirect !== undefined) {
                            window.location = data.redirect;
                        }
                    }
                } else {
                    if(data.msg !== '') {
                        alert(data.msg);
                    } else {
                        alert('Either user does not exist or you have lost your session.');
                    }
                }
            }
        };

        $.ajax(settings)
            .done(function (data) {
                console.log('DONE');
                console.debug(data);
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log("request failed");
                console.debug('d:' + data.toString());
            });

        return 'done!';
    }
    $('#company_name_recomedations').hide();
    $('#company_name').change(function(e) {
        let name = $('#company_name').val();
        let formJson = JSON.stringify([{'name': 'accountName', 'value': name}]);
        let formbtoa = btoa(formJson);
        $.ajax ( {
            url: apiInOne+"/general/accountname/validate",
            type: 'POST',
            async: false,
            header: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                'Access-Control-Max-Age': '86400'
            },
            timeout: 0,
            data: {data: formbtoa},
            success: function (data) {
                var response = (data);
                if(response.status) {
                    $('#company_msg').empty().append('<p>'+response.message+'</p>').show();
                }else {
                    $('#company_msg').empty().append('<p>'+response.message+'</p>').show();
                    $('#company_name').hide();
                    if(response.response) {
                        $('#company_name_recomedations').empty().append('<option class="form-control">Available Company Names</option>').show();
                        let reco = response.response;
                        $.each(reco, function(i, val) {
                            if(i <=2) {
                                $('#company_name_recomedations').append('<option class="form-control" value='+val+'>' +val+ '</option>');
                            }
                        })
                    }
                }
            },
            error: function(data) {
                console.log('error', data);
            }
        } );

        $('select#company_name_recomedations').change(function(e) {
            let val = $('select#company_name_recomedations').val();
            $('#company_name').empty().val(val).show();
            $('select#company_name_recomedations').hide();
            $('#company_msg').empty().hide();
        });

    });

});
