/**
 *
 * @internal This document handles contact table
 * @global mioStandardFields,mioCustomFields,applicationUrl, viewType {overview || history} , viewName = 'contact'
 * @author Aki
 */

// on page load functions
$(document).ready(function () {
    //new table div template
     emptyTableTemplate = contactTable.html();
    //Init date range picker
    initDateRangePicker();
    //@internal - get initial data for page 1 and adds table
    drawTable(1);
})

//variables for storing meta data
let leadStatus; //        ( 'active' | 'inactive'| 'fraud' | 'blacklist' )
let emptyTableTemplate;//  Empty table Html - used as template for changing page numbers
let emptyPaginationBarTemplate;//  Empty pagination Html - used as template for changing page numbers
let recordsPerPage = 10 ;// Number of records per page
let totalNumberOfRecords;
let totalNumberOfPages;
let currentPage;
let userName;
//get all available fields - mio standard fields and mio custom fields are from js of overview.twig
const allAvailableFieldsForView = generateOrderForTableCol(mioStandardFields.concat(mioCustomFields));
//html div
const contactTable = $('#contactViewTable');



/**
 * @internal - get data for contact view. Generates - {Table,Pagination_bar}. The parameter - daterange is dynamically taken
 * @param {int}pageNumber data collected from UI
 * @param {null/array}contactData data collected from UI
 * @param {null/string}searchString
 * @param {null/string}contactStatus
 * @returns {void}
 */
function drawTable(pageNumber,contactData,searchString,contactStatus) {
    //get contact data
    if(!contactData)
        contactData = getData(pageNumber,searchString,contactStatus)
    //Generate and append html  with contact_data
    generateHtmlAndAppendToTable(contactData);
    if(contactData.length > 0 ){
        //Generate and  append pagination list
        generatePaginationList(pageNumber);
        currentPage = pageNumber;
    }
}

/**
 * @internal - get data form API
 * @param {int}pageNumber
 * @param {string}searchString
 * @param {string}contactStatus
 * @returns {array}
 */
function getData(pageNumber,searchString,contactStatus){
    //Get the payload
    let payloadData = getPayloadData(searchString,contactStatus);
    let contactData
    //Get the data form the server side
    $.ajax({
        url: "/paging/contact/" + pageNumber,
        type: 'POST',
        async: false,
        data: payloadData,
        success: function (data) {
            if (data.status === true) {
                contactData = data.c_leads;
                totalNumberOfRecords = data.total_number_of_rows;
                userName = data.current_user.company_name;
            } else {
                //show message on table
                $('#contactViewTable').html('No contacts found , Please refresh the page or search new settings');
                //$('#contactViewTable').html('data.message');
                contactData = "";
            }
        }
    });
    return contactData;
}

/**
 * @internal - Makes the page button look active
 * @returns {void}
 */
function makeCurrentPageActive(pageNumber){
    //Make current page active
    $('#page_'+pageNumber).addClass('active');
    //init the pagination bar
    initPaginationBar();
}

/**
 * @internal - Generates pagination bar
 * todo: Handle all the page follow up
 * @returns {void}
 */
function generatePaginationList(pageNumber){
    const paginationUl = $('#paginationUl');
    //Hold the pagination bar
    const paginationBar = $('#pagination_bar');
    if(paginationUl.find('li').length > 0){
        //if already present reset the pagination bar
        paginationBar.html(emptyPaginationBarTemplate);
    }else{
        //Save the template
        emptyPaginationBarTemplate = paginationBar.html();
    }
    //Fill the template
    totalNumberOfPages = Math.trunc(totalNumberOfRecords/recordsPerPage)+1;

    //add numbers
    if(totalNumberOfPages <= 8){
        for(let n = 1 ; n <= totalNumberOfPages; ++n){
            addPageListToTable(n);
        }
    }else{
        //changing pattern
        if(pageNumber > 4 && pageNumber < totalNumberOfPages-2){
            //Generate first 10 number and last two number and dots in between
            for(let n = 1 ; n <= 3 ; ++n){
                addPageListToTable(n);
            }
            //add dots
            addPageListToTable('.');
            //add page number nearby elements
            addPageListToTable(pageNumber-1);
            addPageListToTable(pageNumber);
            addPageListToTable(pageNumber+1);
            //add dots
            addPageListToTable('.');
            addPageListToTable(totalNumberOfPages-2);
            addPageListToTable(totalNumberOfPages-1);
            addPageListToTable(totalNumberOfPages);

        }else{
            //Generate first 10 number and last two number and dots in between
            for(let n = 1 ; n <= 5 ; ++n){
                addPageListToTable(n);
            }
            //add dots
            addPageListToTable('.');
            //add last three pages in view
            addPageListToTable(totalNumberOfPages-2);
            addPageListToTable(totalNumberOfPages-1);
            addPageListToTable(totalNumberOfPages);
        }
    }

    //un-hide the div
    $('#pagination_nav_bar').removeAttr('hidden');
    $('#pagination_nav_bar').show();//test
    //make current page active
    makeCurrentPageActive(pageNumber);
}


/**
 * @internal - Generates and appends html for pagination
 * @internal - Add next button , dots, previous button
 * @returns {void}
 */
function addPageListToTable(number){
    const paginationUl = $('#paginationUl');
    if(number === 1){
        //add previous button
        paginationUl.append($('#previous_template').html());
    }
    let pageHtml
    if(number !== '.'){
         pageHtml = `<li class="page-item pagination-bar" id="page_` + number + `"><a class="page-link" href="#" >` + number + `</a></li>`
    }else{
        pageHtml = '  .  .  .  .  .  '
    }
    paginationUl.append(pageHtml);
    if(number === totalNumberOfPages ){
        //add Next button ----
        paginationUl.append($('#next_template').html())
    }
}



/**
 * @internal - Generates payload for the request
 * @param {string}searchText search text string
 * @param {string}contactStatus status name ( 'active' | 'inactive'| 'fraud' | 'blacklist' )
 * @returns {string}
 */
function getPayloadData(searchText ='',contactStatus =''){
    return JSON.stringify({
        "viewname":viewName,
        "contact_search":searchText,
        "filter_by_status":contactStatus,
        "daterange":getDateRangeFromDatePicker(),
        "records_per_page":recordsPerPage
    })
}

/**
 * @internal - Generates daterange as per payload requirements ( startDate#endDate )
 * @returns {string}
 */
function getDateRangeFromDatePicker(){
    const dateRangePicker = $('#contactFilterByDate');
    let startDate = dateRangePicker.data('daterangepicker').startDate.format('YYYY-MM-DD');
    let endDate = dateRangePicker.data('daterangepicker').endDate.format('YYYY-MM-DD');
    return startDate+"#"+endDate;
}

/**
 * @internal - Generates html and appends to table - thead and tbody
 * @param {array}contactData data form the pagination API
 * @returns {void}
 */
function generateHtmlAndAppendToTable(contactData) {
    //Html templates and divs
    const contactTableHeadRow = $("#contactViewTable > thead > tr");
    const contactTableBody = $("#contactViewTable > tbody");
    console.log(allAvailableFieldsForView);
    //add head to table
    $.each(allAvailableFieldsForView, function (key, value) {
        //get field type
        let fieldType = getFieldType(value);
        //check for hidden field
        let hiddenValue = getHiddenType(value);
        //thead template
        let theadTemplate = `<th ` + hiddenValue + ` id='` + value + `' class="` + fieldType + `"><b>` + value + `</b></th>`;
        contactTableHeadRow.append(theadTemplate);
    });

    //Add body to table - As the order is defined in backend just append the data
    $.each(contactData, function (key, value) {
        //take table row template
        const tbodyRow = $('#body_row_template');

        //for single row
        $.each(value, function (index, field) {
            let rowHtml = generateTbodyRowHtml(index, field)
            //get field type
            let fieldType = getFieldType(index);
            //check for hidden field
            let hiddenValue = getHiddenType(index);
            //template
            let theadTemplate = `<td ` + hiddenValue + ` class="` + index + ` ` + fieldType + `">` + rowHtml + `</td>`;
            tbodyRow.append(theadTemplate);
        });
        //dump html to table body
        let tr = `<tr> ` + tbodyRow.html() + `</tr>`
        contactTableBody.append(tr)
        //empty tbody row for loop use/next value
        tbodyRow.html('');
    });
}

/**
 * @internal - Return field type - makes fields visible and hidden on initial load
 * @param {string} field mio data field
 * @return null/string
 */
function getHiddenType(field) {
    //@internal - All available visible fields for view
    let visibleFields = [
        "updated", "Status", "Permission", "Reference", "Email", "FirstName", "LastName", "PostalCode", "Actions"
    ];
    if (visibleFields.includes(field)) {
        return ''
    }
    return 'hidden';
}

/**
 * @internal - Return field type - Has field mapping
 * @param {string} field mio data field
 */
function getFieldType(field) {
    //field Mapping - index of fields in mioStandardFields
    let permissionFieldColumns = [ 25, 26, 27, 28, 29,30];
    let technicalFieldColumns = [8, 9, 10, 11 ,12];
    let eCommerceFieldColumns = [13, 14, 15, 16, 17, 18, 19,20,21,22,23,24];

    if (mioCustomFields.includes(field)) {
        return 'custom_fields';
    }
    if (mioStandardFields.includes(field)) {
        let fieldIndex = mioStandardFields.indexOf(field);
        if (permissionFieldColumns.includes(fieldIndex)) {
            return 'permission_fields'
        }
        if (technicalFieldColumns.includes(fieldIndex)) {
            return 'technical_fields'
        }
        if (eCommerceFieldColumns.includes(fieldIndex)) {
            return 'eCommerce_fields'
        }
        return 'standard_fields'
    }
}

/**
 * @internal - Generates html for table_body  with different styling like actions, permission number and status
 * @param {string} fieldName data form the pagination API
 * @param {string} fieldValue data form the pagination API
 * @returns {string} html string
 */
function generateTbodyRowHtml(fieldName, fieldValue) {
    //variable for status
    let status = {
        active: {'title': 'active', 'class': 'kt-badge--success'},
        inactive: {'title': 'inactive', 'class': ' kt-badge--danger'},
        fraud: {'title': 'fraud', 'class': ' kt-badge--warning'},
        blacklisted: {'title': 'blacklisted', 'class': ' kt-badge--danger'},
    }
    //show correct permission name instead of int
    let PERMISSION_NONE = 1
    let PERMISSION_SINGLE_OPT_IN = 2
    let PERMISSION_CONFIRMED_OPT_IN = 3
    let PERMISSION_DOUBLE_OPT_IN = 4
    let PERMISSION_DOUBLE_OPT_IN_SINGLE_USER_TRACKING = 5

    if (fieldName === 'Status') {
        leadStatus = fieldValue;
        return '<span class="kt-badge ' + status[fieldValue].class + ' kt-badge--inline kt-badge--pill">' + status[fieldValue].title + '</span>'
    }
    if (fieldName === 'Permission') {
        let permission = parseInt(fieldValue)
        if (permission === PERMISSION_NONE) { // 1
            return 'No Permission'
        }
        if (permission === PERMISSION_SINGLE_OPT_IN) { // 2
            return 'Single Opt-In'
        }
        if (permission === PERMISSION_CONFIRMED_OPT_IN) { // 3
            return 'Confirmed Opt-In'
        }
        if (permission === PERMISSION_DOUBLE_OPT_IN) { // 4
            return 'Double Opt-In'
        }
        if (permission === PERMISSION_DOUBLE_OPT_IN_SINGLE_USER_TRACKING) { // 5
            return 'Double Opt-In Single User Tracking'
        }
        return 'No Permission'
    }
    if (fieldName === 'Actions') {

        //holding html div in jquery
        const dropdownTd = $('#tbody_dropdown_template');
        const dropdownMenu = $('#tbody_dropdown_template .dropdown-menu');

        //urls
        let leadViewUrl = applicationURL + '/contact/' + fieldValue +'/detail';
        let activationUrl = applicationURL + '/contact/' + fieldValue + '/status/' + viewType +'/active';
        let deactivationUrl = applicationURL + '/contact/' + fieldValue +'/status/' + viewType +'/inactive';
        let makeFraudUrl = applicationURL + '/contact/' + fieldValue + '/status/' + viewType +'/fraud';
        let blacklistUrl = applicationURL + '/contact/' + fieldValue + '/status/' + viewType +'/blacklisted';

        //Back up the template
        let templateBackup = dropdownTd.html();
        //Add <a> tags to the template
        let aTagDetails = `<a class="dropdown-item" id="export_lead_detail" href=` + leadViewUrl + `><i
                      class="la la-search-plus"></i> Show details</a>`
        dropdownMenu.append(aTagDetails);
        if (leadStatus && leadStatus === "active") {
            //html for a tags if the status is active
            let aTags = `<a class="dropdown-item" href=` + deactivationUrl + `>
                <i class="la la-user-times"></i>Set to inactive</a>
            <a class="dropdown-item" href=` + makeFraudUrl + `>
                <i class="la la-user-secret"></i>Mark as fraud</a>
            <a class="dropdown-item" href=` + blacklistUrl + `>
                <i class="la la-times-circle-o"></i>Put on blacklist</a>`
            //append the <a> tags html to template
            dropdownMenu.append(aTags);
        } else {
            //html for a tags if the status is active
            let aTags = `<a class="dropdown-item" href=` + activationUrl + `>
                <i class="la la-user-times"></i>Set Contact as active</a>`
            //append the <a> tags html to template
            dropdownMenu.append(aTags);
        }

        //rest process for the html template
        //store html in string
        let html = dropdownTd.html();
        //reset the template
        dropdownTd.html(templateBackup);
        //return the string
        return html;
    }
    if(!fieldValue) fieldValue = "";
    //if no styling return the normal text field
    return fieldValue;
}


/**
 * @internal date range picker library init the input value
 */
function initDateRangePicker() {
    let start = moment().subtract(7, 'days');
    let end = moment();
    //default range 7 days
    function cb(start, end) {
        $('#contactFilterByDate span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    // Date Range Picker
    $('#contactFilterByDate').daterangepicker({
        timePicker: false,
        autoApply: false,
        autoUpdateInput: true,
        alwaysShowCalendars: false,
        startDate: start,
        endDate: end,

        locale: {
            format: 'Y-MM-D',
            cancelLabel: 'Clear'
        },

        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    },cb);
}

/**
 * @internal - get data form API
 * @param {string}dataRoute
 * @param {null/string}searchString
 * @param {null/string}contactStatus
 * @returns {array}
 */
function getAllContacts(dataRoute){

    let contactData
    //Get the data form the server side
    $.ajax({
        url: applicationURL+dataRoute,
        type: 'GET',
        async: false,
        success: function (data) {
            if (data.status === true) {
                contactData = data.data;
            } else {
                //show message on table
                contactData = [];
            }
        }
    });
    return contactData;
}
/**
 * @internal - order for the columns of the table
 * @param {array}allAvailableFields
 * @returns {array}
 */
function generateOrderForTableCol(allAvailableFields){
    //WEB-5869 Always actions should be at the end
    let length = allAvailableFields.length;
    allAvailableFields.splice(length+1,0,"Actions");
    return allAvailableFields;
}

//@internal - ***** jquery event based functions ******

/**
 * @internal - On change event show and hide the additional fields
 */
$('#checkbox :checkbox').change(function(){
    // $(this) - contains a reference to the checkbox
    if (this.checked) {
        let id = $(this).attr("id");
        //show the fields
        $('.'+id).removeAttr('hidden');
        $('.'+id).show();
    } else {
        let id = $(this).attr("id");
        //hide the fields
        $('.'+id).hide();
    }
});

/**
 * @internal - On pagination bar click
 */
function initPaginationBar(){
    //event runner stops multiple events running
    let eventRunning;
    let pageNumber;
    $('.page-item').click(function () {
        if(!eventRunning){
            eventRunning = true;
            let page = $(this).attr('id').replace("page_","");
            if(page === 'next' && currentPage !== totalNumberOfPages){
                pageNumber = Number(currentPage)+1;
            }else if(page === 'previous' && currentPage !== 1){
                pageNumber = Number(currentPage)-1;
            }else{
                pageNumber = Number(page);
            }
            //prevent same requests
            if(!$(this).hasClass('active') && Number.isInteger(pageNumber)){
                //Get the values for base
                let searchString = $('#contactSearch').val();
                let contactStatus = $('#filter_by_status option:selected').val();
                let contactData = getData(pageNumber,searchString,contactStatus);
                //Reset the table
                contactTable.html(emptyTableTemplate);
                $('#pagination_bar').html(emptyPaginationBarTemplate);
                //draw the table
                drawTable(pageNumber,contactData,searchString,contactStatus);
            }
            eventRunning = false;
        }
    })
}

/**
 * @internal - Get data button in UI click function - Gets data and show in table
 */
$('#get_data').click(function () {
    //Get the values for base
    let searchString = $('#contactSearch').val();
    let contactStatus = $('#filter_by_status option:selected').val();
    //get contact data
    let contactData = getData(1,searchString,contactStatus);
    //Reset the table and pagiantion bar
    contactTable.html(emptyTableTemplate);
    $('#pagination_bar').html(emptyPaginationBarTemplate);
    //draw the table
    drawTable(1,contactData,searchString,contactStatus);
})

/**
 * @internal - Download excel file
 */
$('#export_data').click(function () {
    //Event runner stops multiple clicks until the job is processed
    let eventRunning;
    if(!eventRunning){
        eventRunning = true;
        //event runner stops multiple events running
        let dataRoute = $(this).attr('data-route');
        let contactData = getAllContacts(dataRoute);
        let options = {
            header: mioStandardFields.concat(mioCustomFields),
            nullError : false,
            cellDates : false
        };
        //new workbook
        let workbook = XLSX.utils.json_to_sheet(contactData,options);
        //create new work sheet
        let wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, workbook, "MIO");
        XLSX.writeFile(wb,userName+'-Marketing-In-One.xlsx');
        eventRunning = false;
    }
})


//***** todo events ******

/**
 * @internal - search by filter ( 'active' | 'inactive'| 'fraud' | 'blacklist' )
 * todo
 */
$('#filter_by_status').click(function () {

})


/**
 * @internal - search by date
 * todo : change in data picker field
 */


/**
 * @internal - search by string triggered by - min 3 letters and event 'enter pressed'
 * todo : min 3 letters and enter
 */










