class MioFieldList {
    constructor() {
        this.uniqueList = {};
        this.record = {
            selectId : '',
            fieldName : ''
        }
    }


    /**
     * needed if user changes contact event selection
     */
    init() {
        this.uniqueList = {};
    }

    /**
     *
     * @param selectBoxId
     * @param record
     * @returns {boolean}
     */
    addRecord(selectBoxId, record) {
        if(!this.isFieldInList(record.selectId)) {
            this.uniqueList[selectBoxId] = record;
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @returns {*|{selectId: string, fieldName: string}}
     */
    newRecord() {
        return Object.assign({}, this.record);
    }

    /**
     *
     * @param selectId
     * @returns {boolean}
     */
    isFieldInList(selectId) {
        var $return = false;
        $.each( this.uniqueList, function(i,v) {
            // console.log(i, v.selectId, selectId, (v.selectId == selectId) ? 'JA':'NEIN')
            if( v.selectId == selectId) {
                $return = true;
                return false;
            }
        })
        return $return;
    }
}

/**
 *
 * @type {MioFieldList}
 */
const $objMioUniqueFieldList = new MioFieldList();

/**
 * attached to all select.selectMioField elements
 * @see MioFieldList
 * @see $objMioUniqueFieldList
 * @param ele
 */
function checkDuplicates(ele) {

    var $currentSelectId = $(ele).find(':selected').attr('id');
    console.log('IN', $objMioUniqueFieldList.uniqueList, '$currentSelectId='+$currentSelectId)

    $.each($('select.selectMioField'), function (i, v) {
        $(v).css('border-color', 'lightgrey');
        $record = $objMioUniqueFieldList.newRecord();
        $record.selectId = $(v).find(':selected').attr('id');
        $record.fieldName = $(v).find(':selected').text();

        $selectBoxId = $(v).attr('id');


        if (!$objMioUniqueFieldList.isFieldInList($record.selectId) && (0 != $record.selectId)) {
            $objMioUniqueFieldList.addRecord($selectBoxId, $record);
            $('button#saveContactEventConfig').removeAttr('disabled');
        } else {
            if (0 != $record.selectId && $record.selectId == $currentSelectId) {
                $(v).css('border-color', 'red');
                $('button#saveContactEventConfig').attr('disabled', true);
            }
        }

        if('initial' == $record.selectId) {
            $('button#saveContactEventConfig').attr('disabled', true);
        }
    });
    //console.log('OUT', $objMioUniqueFieldList.uniqueList)
}

$(document).ready(function() {
    class ObjectRegisterMeta {
        constructor() {

            this.fieldMap = {
                ce_field_id: "",
                ce_field_name: "",
                mio_field_id: "",
                mio_field_name: "",
                mio_sourcetable: ""
            };

            this.settings = {
                name: "",
                doi_mailing_key:"",
                doi_mailing_name:"",
                contact_event_id: "",
                contact_event_name: "",
                field_mappings: []
            }; // settings

        }// constructor


        /**
         *
         * @returns {*|{field_mappings: *[], contact_event_id: string, name: string, contact_event_name: string}}
         */
        getSetting() {
            return this.settings;
        }

        /**
         *
         * @returns {*|{ce_field_name: string, mio_field_id: string, mio_sourcetable: string, mio_field_name: string, ce_field_id: string}}
         */
        getFieldMap() {
            return Object.assign({}, this.fieldMap);
        }

        /**
         *
         * @param fieldMap
         */
        setFieldMap(fieldMap) {
            this.settings.field_mappings.push ( fieldMap );
        }
    }//class

    var preDefinedContactEvent = JSON.parse ( $ ( 'data#contactEventFromDB' ).val () );
    var preDefinedMapping = JSON.parse ( $ ( 'data#mappingFromDB' ).val () );

    // CE Fields template used
    var template = $ ( 'template#miofieldlisttemplate' );
    var templateContent = template.toArray ()[0].innerHTML
    template.html ( '' );

    /**
     *
     *  @type {ObjectRegisterMeta}
     */
    const $orm = new ObjectRegisterMeta ();


    // init select2
    $ ( 'select.selectnr2' ).select2 ( {
        placeholder: "Select a Contact Event",
        allowClear: false
    } );

    // for updating the contactevent.
    // after getting the contactevent value form DB, ContactEvent has to be preselected.
    if ( preDefinedContactEvent !== null ) {
        $('select.selectnr2').val(preDefinedContactEvent).trigger('change')
        // Generate Mapping table based on selected Contact Event
        generateMappingTable ( preDefinedContactEvent );
        // pre-select MIO-fields for each CE fields.
        if ( preDefinedMapping !== null ) {
            for (let field of preDefinedMapping) {
                $ ( 'select#mfl_' + field['ce_field_id'] ).val ( field['mio_field_id'] );
            }
        }
        $ ( 'button#saveContactEventConfig' ).removeAttr ( 'disabled' );
    }

    // {# generate table for field mappings on TAB #2  #}
    $ ( 'select.selectnr2' ).on ( 'change', function () {
        $objMioUniqueFieldList.init ();
        $ ( 'div#tableblock' ).css ( 'display', 'none' );
        //  $('tbody#mappingtablecontent').html('');
        var ceId = $ ( this ).find ( ':selected' ).val ();
        generateMappingTable ( ceId );
    } )

    /**
     * Function generates mapping table for the defiend contact event from eMIO.
     * @param ceId
     */
    function generateMappingTable(ceId) {
        /*        var template = $('template#miofieldlisttemplate');
                console.log('template', template);
                var templateContent = template.toArray()[0].innerHTML
                console.log('template content: ', templateContent);
                template.html('');*/

        if ( ceId === 0 || ceId === null ) {
            return;
        }

        var contactEventAttributes = JSON.parse ( $ ( 'data#contactEventAttributes' ).val () );
        //console.log ( contactEventAttributes[ceId] );

        var tableRows = '';

        $.each ( contactEventAttributes[ceId], function (index, val) {
            newContent = templateContent.replace ( '"miofieldlist"', '"mfl_' + index + '" data-ce_fieldid="' + index + '" data-ce_fieldname="' + val + '" ' );
            console.log ( 'newContent', newContent );
            tableRows += "" +
                "<tr id='id_" + index + "'>" +
                "   <td id='tdce_" + index + "'  data-ceid='" + index + "'>" + val + "</td>" +
                "   <td id='tdmio_" + index + "' data-maptoid='" + index + "'>" + newContent + "</td>" +
                "</tr>";
        });

        $('tbody#mappingtablecontent').html(tableRows);
        $('div#tableblock').css('display', 'initial');
    }

    // check for duplicate processhook names
    $('input#processhookname').on('blur', function () {
        var phName = $(this).val();
        var namesInUse = JSON.parse( $('data#phNames').val());
        var myself = this;
        var $found = false;

        //console.log('namesInUse',namesInUse)

        if (namesInUse === null) {
            return;
        } else {
            $.each(namesInUse, function (i, usedName) {
                if (usedName.processhook_name === phName) {
                    $found = true;
                    return;
                }
            })
        }

        if($found) {
            $(this).css('border-color', 'red').focus().val('').attr('placeholder', phName +' is already in use! Please chose another name.');
            $('button#next').attr('disabled', true);
        } else {
            $(this).css('border-color', 'inherit').attr('placeholder','Processhook Name');
            $('button#next').removeAttr('disabled');
        }
    });


    // simulates php empty function
    function empty($value) {
        if (null === $value || undefined === $value || '' === $value || 0 === $value) {
            return true;
        } else {
            return false;
        }
    }

    // save processhook config
    $('button#saveContactEventConfig').on('click', function () {
        var $exit = false;

        var $route = atob($(this).data('route'));
        var $redirect = $(this).data('redirect');
        var $baseUrl = $(this).data('baseurl').slice(0,-1);
        $processhookname = $('input#processhookname').val();

        if (empty($processhookname)) {
            $('input#processhookname').focus();
            return;
        }

        $contact_event_id = $('select#contactevents :selected').val();

        if (empty($contact_event_id)) {
            $('select#contactevents').focus();
            return;
        }

        if(processhookType === "Send Double Opt In Mail Or Call Contact Event"){
            $orm.settings.doi_mailing_key = $('select#doiSelect :selected').val();
            $orm.settings.doi_mailing_name = $('select#doiSelect :selected').text();
        }

        $contact_event_name = $('select#contactevents :selected').text(); // is set when $contact_event_id is set

        $orm.settings.name = $processhookname;
        $orm.settings.contact_event_id = $contact_event_id;
        $orm.settings.contact_event_name = $contact_event_name;

        $.each($('select.selectMioField'), function (i, v) {
            $fm = $orm.getFieldMap();

            if ($(v).find(':selected').val() == 0) {
                $(v).focus(); // jump to missing field mapping
                $exit = true;
                return;
            }
            if ($exit) return;

            $fm.ce_field_id = $(v).data('ce_fieldid');
            $fm.ce_field_name = $(v).data('ce_fieldname');
            $fm.mio_field_id = $(v).find(':selected').val();
            $fm.mio_field_name = $(v).find(':selected').data('fieldname');
            $fm.mio_sourcetable = $(v).find(':selected').data('sourcetable');

            $orm.setFieldMap($fm);

            //console.log(i, $(v).data('ce_fieldid'), $(v).data('ce_fieldname'), $(v).find(':selected').val(), $(v).find(':selected').data('sourcetable'), $(v).find(':selected').data('fieldname'));

        }); // each

        if ($exit) {
            return;
        } else {

            $.post(
                $route,
                {
                    processhook: 'call_contact_event',
                    token: $('input#token').val(),
                    settings: $orm.getSetting()
                },
                function (data, status) {
                    //console.log("saveContactEvent");
                    //console.log('data', data);

                    if (status == 'success' && data.status == true) {

                        let redirect = $baseUrl + $redirect.replace('XXXX', data.processhook_id);
                        //console.log('redirect', redirect);

                        if (data.msg !== '' || data.msg !== undefined) {
                            //console.log(data.msg);
                        }
                        $('i#iconSave').removeClass('fa-save').addClass('fa-thumbs-up');
                        $('div#msg').text(data.msg ).css('display', 'inherit').fadeOut(4000);

                        setTimeout(function() {
                            window.location.href=redirect; // simulate mouse click, do not redirect
                            // parent.history.back();
                        }, 1000);

                    } else {
                        $('i#iconSave').removeClass('fa-save').addClass('fa-bug');
                        //console.log(data.msg);
                        $('div#msg').text(data.msg).css('display', 'inherit').fadeOut(8000);
                    }
                });
        }//if

    });// $('button#saveContactEventConfig').on('click', function ()

});//document ready
