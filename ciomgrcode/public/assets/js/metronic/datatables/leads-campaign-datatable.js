"use strict";
var KTDatatablesLeadCampaign = function () {

    var initTableLeadCampaign = function () {
        var lead_status = ''
    
        let PERMISSION_NONE = 1
        let PERMISSION_SINGLE_OPT_IN = 2
        let PERMISSION_CONFIRMED_OPT_IN = 3
        let PERMISSION_DOUBLE_OPT_IN = 4
        let PERMISSION_DOUBLE_OPT_IN_SINGLE_USER_TRACKING = 5
    
        // Helper function to generate URL Using Ids
        function getURLForOperation (ids, operation) {
        
            var url = ''
            var urlObj = window.location
        
            if (( ids.length === 0 ) || ( operation.trim() == '' ) || $.isEmptyObject(urlObj)) {
                return url
            }
        
            var host = urlObj.host
            var protocol = urlObj.protocol
            var create_url = protocol + '//' + host + '/account/' + ids[0] + '/lead/' + ids[2] + '/' + operation
            if (isValidURL(create_url)) {
                url = create_url
            }
            return url
        }



        // Validates if the String is URL or not
        function isValidURL(string) {
            let res;
            res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
            if (res == null)
                return false;
            else
                return true;
        }



        let table = $('#leads_for_campaign');
        let id = 0;
        // begin first table
        table.DataTable({
            responsive: true,
            // // DOM Layout settings
            dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

            select: {
                style: 'multi'
            },
    
            // lengthMenu: [5, 10, 25, 50],
            paging: true,
            bInfo: false,
            pageLenght: 10,
            ordering: true,
            language: {
                'lengthMenu': 'Display _MENU_',
            },
    
            // Order settings
            order: [[0, 'desc']],
    
            /*            headerCallback: function (thead, data, start, end, display) {
							thead.getElementsByTagName('th')[0].innerHTML = `
								<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
									<input type="checkbox" value="" class="m-group-checkable">
									<span></span>
								</label>`;
						},*/
            columnDefs: [
                /*                {
									targets: 0,
									width: '30px',
									autoWidth: false,
									className: 'dt-right record-id',
									orderable: true,
									render: function (data, type, full, meta) {
										return `
											 <label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
												 <input type="checkbox" value="` + data + `" class="m-checkable" id="record-id">
												 <span></span>
											 </label>`;
									},
								},*/
                /////////// Begin : Base Parameters /////////////
                {
                    // Updated
                    targets: 0,
                    // title: 'Updated',
                    className: 'noClick align-middle data-no-filter',
                    visible: true
                },
                {
                    // Status
                    targets: 1,
                    className: 'noClick align-middle data-no-filter',
                    render: function (data, type, full, meta) {
                        var status = {
                            active: { 'title': 'active', 'class': 'kt-badge--success' },
                            inactive: { 'title': 'inactive', 'class': ' kt-badge--danger' },
                            fraud: { 'title': 'fraud', 'class': ' kt-badge--warning' },
                            blacklisted: { 'title': 'blacklisted', 'class': ' kt-badge--danger' },
                        }
            
                        if (typeof status[data] === 'undefined') {
                            return data
                        }
                        lead_status = status[data].title
                        return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>'
                    },
                    visible: true,
                },
                {
                    // Permission
                    targets: 2,
                    className: 'noClick align-middle data-no-filter',
                    render: function (data) {
                        let permission = parseInt(data)
                        if (permission === PERMISSION_NONE) {
                            return 'No Permission'
                        }
                        if (permission === PERMISSION_SINGLE_OPT_IN) {
                            return 'Single Opt-In'
                        }
                        if (permission === PERMISSION_CONFIRMED_OPT_IN) {
                            return 'Confirmed Opt-In'
                        }
                        if (permission === PERMISSION_DOUBLE_OPT_IN) {
                            return 'Double Opt-In'
                        }
                        if (permission === PERMISSION_DOUBLE_OPT_IN_SINGLE_USER_TRACKING) {
                            return 'Double Opt-In Single User Tracking'
                        }
                        return 'NONE'
                    },
                    visible: true,
                },
                {
                    // Reference
                    targets: 3,
                    className: 'noClick align-middle data-no-filter',
                    visible: true
                },
                {
                    // Email
                    targets: 4,
                    className: 'noClick align-middle data-no-filter',
                    visible: true
                },
                {
                    // FirstName
                    targets: 5,
                    className: 'noClick align-middle data-no-filter',
                    visible: true
                },
                {
                    // LastName
                    targets: 6,
                    className: 'noClick align-middle data-no-filter',
                    visible: true
                },
                {
                    // PostalCode
                    targets: 7,
                    className: 'noClick align-middle data-no-filter',
                    visible: true
                },
                {
                    // Actions
                    targets: 8,
                    className: ' align-middle data-no-filter',
                    visible: true
                },
                /////////// END : Base Parameters /////////////
                /////////// START : TECHNICAL PARAMETERS ///////////
    
                {
                    // AffiliateID
                    targets: 9,
                    className: 'noClick align-middle data-tech-fields',
                    visible: false
                },
                {
                    // AffiliateSubID
                    targets: 10,
                    className: 'noClick align-middle data-tech-fields',
                    visible: false
                },
                {
                    // Referrer ID
                    targets: 11,
                    className: 'noClick align-middle data-tech-fields',
                    visible: false
                },
                {
                    // Target Group
                    targets: 12,
                    className: 'noClick align-middle data-tech-fields',
                    visible: false
                },
                /////////// END : TECHNICAL PARAMETERS ///////////
    
                /////////// START : ECOMMERCE PARAMETERS ///////////
                {
                    // LastOrderDate
                    targets: 13,
                    className: 'noClick align-middle data-eco-fields',
                    visible: false
                },
                {
                    // LastOrderNo
                    targets: 14,
                    className: 'noClick align-middle data-eco-fields',
                    visible: false
                },
                {
                    // eCommerceTags
                    targets: 15,
                    className: 'noClick align-middle data-eco-fields',
                    visible: false
                },
                {
                    // ProductAttributes
                    targets: 16,
                    className: 'noClick align-middle data-eco-fields',
                    visible: false
                },
                {
                    // TotalOrderNetValue
                    targets: 17,
                    className: 'noClick align-middle data-eco-fields',
                    visible: false
                },
                {
                    // LastYearOrderNetValue
                    targets: 18,
                    className: 'noClick align-middle data-eco-fields',
                    visible: false
                },
                {
                    // LastOrderNetValue
                    targets: 19,
                    className: 'noClick align-middle data-eco-fields',
                    visible: false
                },
                {
                    // FirstOrderDate
                    targets: 20,
                    className: 'noClick align-middle data-eco-fields',
                    visible: false
                },
                /////////// END : ECOMMERCE PARAMETERS ///////////
    
                /////////// START : PERMISSION PARAMETERS ///////////
    
                {
                    // FinalURL
                    targets: 21,
                    className: 'noClick align-middle data-permission-fields',
                    visible: false
                },
                {
                    // URL
                    targets: 22,
                    className: 'noClick align-middle data-permission-fields',
                    visible: false
                },
                {
                    // SOITimestamp
                    targets: 23,
                    className: 'noClick align-middle data-permission-fields',
                    visible: false
                },
                {
                    // SOIIP
                    targets: 24,
                    className: 'noClick align-middle data-permission-fields',
                    visible: false
                },
                {
                    // DOIIP
                    targets: 25,
                    className: 'noClick align-middle data-permission-fields',
                    visible: false
                },
                {
                    // DOITimestamp
                    targets: 26,
                    className: 'noClick align-middle data-permission-fields',
                    visible: false
                },
    
                /////////// END : PERMISSION PARAMETERS ///////////
            ],
        });


        table.on('change', '.kt-group-checkable', function () {
            var set = $(this).closest('table').find('td:first-child .kt-checkable');
            var checked = $(this).is(':checked');

            $(set).each(function () {
                if (checked) {
                    $(this).prop('checked', true);
                    $(this).closest('tr').addClass('active');

                } else {
                    $(this).prop('checked', false);
                    $(this).closest('tr').removeClass('active');
                }
            });
        });

        table.on('change', 'tbody tr .kt-checkbox', function () {
            $(this).parents('tr').toggleClass('active');
            $(this).trigger("click");

        });

        table.on('click', 'tbody tr td', function () {
            var tr = $(this);
            if (tr.hasClass("noClick")) {
                return false;
            }
        });

    };

    return {

        //main function to initiate the module
        init: function () {
            initTableLeadCampaign();
        },

    };


}();

jQuery(document).ready(function () {
    KTDatatablesLeadCampaign.init()
    
    var table = $('#leads_for_campaign').DataTable()
    let rows = table.rows(['#duplicate'])
    let arr_cols = table.columns()[0]
    let default_cols = 26
    // let action_col = 0 // Number of action columns
    let len_cols = arr_cols.length
    console.log('length of cols', len_cols)
    let customFieldColumns = []
    
    let chkBoxPer = $('#cb-permission-fields')
    let permissionFieldColumns = [21, 22, 23, 24, 25, 26]
    
    let chkBoxTech = $('#cb-technical-fields')
    let technicalFieldColumns = [9, 10, 11, 12]
    
    let chkBoxECom = $('#cb-ecommerce-fields')
    let eCommerceFieldColumns = [13, 14, 15, 16, 17, 18, 19, 20]
    
    let chkBoxCustom = $('input#cb-custom-fields')
    
    // Calculate the custom fields.
    if (len_cols > default_cols) {
        let diff_cols = len_cols - default_cols
        
        console.log('difference columns', diff_cols)
        for (let i = default_cols + 1; i < len_cols; i++) {
            console.log('i', i)
            customFieldColumns.push(i)
        }
    }
    // Hide Custom field columns.
    table.columns(customFieldColumns).visible(false)

    //Search Lead Table
    $('#lead-search').keyup(function (e) {
        var value = $(this).val();
        // Search value only on 'Enter' is pressed
        if (e.keyCode == 13 && value.length >= 3) {
            table
                .search(value)
                .draw();
        } else {
            table
                .search('')
                .draw();
        }
    });

    // Filter By Campaign
    $('#filter_by_campaign').change(function (e) {
        var value = $(this).children("option:selected").val();
        console.log('filterByCampaign', value)
        var regex = '\\b' + value + '\\b'
        table
            .columns(3)
            .search(regex, true, false)
            .draw()
    });
    
    // Custom field Checkbox
    $('input[name="cb-custom-fields"]').on('change', function () {
    
        chkBoxTech.prop('checked', false)
        chkBoxPer.prop('checked', false)
        chkBoxECom.prop('checked', false)
    
        if ($(this).is('.ac-sel')) {
            chkBoxCustom.removeClass('ac-sel')
            changeFieldsVisibility(customFieldColumns, false)
        } else {
            chkBoxCustom.addClass('ac-sel')
            changeFieldsVisibility(customFieldColumns, true)
        }
    })
    
    // Technical Field Checkbox
    $('input[name="cb-technical-fields"]').on('change', function () {
    
        $('#cb-ecommerce-fields').prop('checked', false)
        $('#cb-custom-fields').prop('checked', false)
        $('#cb-permission-fields').prop('checked', false)
        if (chkBoxTech.is('.ac-sel')) {
            chkBoxTech.removeClass('ac-sel')
            changeFieldsVisibility(chkBoxTech, false)
        } else {
            chkBoxTech.addClass('ac-sel')
            changeFieldsVisibility(chkBoxTech, true)
        }
        // table.columns.adjust().draw()
    })
    
    // eCommerce Fields Checkbox
    $('input[name="cb-ecommerce-fields"]').on('change', function () {
    
        $('#cb-technical-fields').prop('checked', false)
        $('#cb-custom-fields').prop('checked', false)
        $('#cb-permission-fields').prop('checked', false)
        if (chkBoxECom.is('.ac-sel')) {
            chkBoxECom.removeClass('ac-sel')
            changeFieldsVisibility(chkBoxECom, false)
        } else {
            chkBoxECom.addClass('ac-sel')
            changeFieldsVisibility(chkBoxECom, true)
        }
        //table.columns.adjust().draw()
    });
    
    // Permission Fields Checkbox
    $('input[name="cb-permission-fields"]').on('change', function () {
        
        $('#cb-ecommerce-fields').prop('checked', false)
        $('#cb-custom-fields').prop('checked', false)
        $('#cb-technical-fields').prop('checked', false)
        if (chkBoxPer.is('.ac-sel')) {
            chkBoxPer.removeClass('ac-sel')
            changeFieldsVisibility(chkBoxPer, false)
        } else {
            chkBoxPer.addClass('ac-sel')
            changeFieldsVisibility(chkBoxPer, true)
        }
    })
    
    function changeFieldsVisibility (chkBox, visability) {
        let visibleColumns = []
        let hiddenColumns = []
        
        if (chkBox === chkBoxPer) {
            $.each([chkBoxTech, chkBoxECom, chkBoxCustom], function (i, v) {
                v.removeClass('ac-sel')
            })
            visibleColumns = permissionFieldColumns
            hiddenColumns = $.merge([], eCommerceFieldColumns)
            hiddenColumns = $.merge(hiddenColumns, technicalFieldColumns)
            hiddenColumns = $.merge(hiddenColumns, customFieldColumns)
        } else if (chkBox === chkBoxECom) {
            
            $.each([chkBoxTech, chkBoxPer, chkBoxCustom], function (i, v) {
                v.removeClass('ac-sel')
            })
            visibleColumns = eCommerceFieldColumns
            hiddenColumns = $.merge([], permissionFieldColumns)
            hiddenColumns = $.merge(hiddenColumns, technicalFieldColumns)
            hiddenColumns = $.merge(hiddenColumns, customFieldColumns)
        } else if (chkBox === chkBoxTech) {
            
            $.each([chkBoxECom, chkBoxPer, chkBoxCustom], function (i, v) {
                v.removeClass('ac-sel')
            })
            visibleColumns = technicalFieldColumns
            hiddenColumns = $.merge([], eCommerceFieldColumns)
            hiddenColumns = $.merge(hiddenColumns, permissionFieldColumns)
            hiddenColumns = $.merge(hiddenColumns, customFieldColumns)
        } else if (chkBox === chkBoxCustom) {
            
            $.each([chkBoxECom, chkBoxPer, chkBoxTech], function (i, v) {
                v.removeClass('ac-sel')
            })
            
            visibleColumns = customFieldColumns
            hiddenColumns = $.merge([], eCommerceFieldColumns)
            hiddenColumns = $.merge(hiddenColumns, technicalFieldColumns)
            hiddenColumns = $.merge(hiddenColumns, permissionFieldColumns)
        }
        
        table.columns(hiddenColumns).visible(false)
        table.columns(visibleColumns).visible(visability)
        table.draw()
        
        // table.columns.adjust ().draw ();
    }
    
    // Filter By Status
    $('#filter_by_status').change(function (e) {
        var value = $(this).children('option:selected').val()
        var regex = '\\b' + value + '\\b'
        table
            .columns(1)
            .search(regex, true, false)
            .draw()
        
    })
    // Date Range Picker
    $('input[name="daterange"]').daterangepicker({
        timePicker: false,
        autoApply: false,
        autoUpdateInput: true,
        alwaysShowCalendars: false,

        locale: {
            format: 'Y-MM-D',
            cancelLabel: 'Clear'
        },

        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }

    });

    // Search Datatable with selected Date range
    $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
        var d_range = picker.startDate.format('Y-MM-D') + ' - ' + picker.endDate.format('Y-MM-D')
        $(this).val(d_range);
        var d_arr = getDiffDates(d_range);
        var searchString = '(' + d_arr.join('|') + ')';
        table.columns(0).search(searchString, true).draw(true)
    });

    // Reset the Date range filter
    $('input[name="daterange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        table
            .search('')
            .draw();
    });


    // Get all the dates between Min and Max Date.
    function getDiffDates(daterange) {
        var dates = [];
        var split_range = daterange.split(" - ");
        var from_date = new Date(split_range[0]);
        var to_date = new Date(split_range[1]);
        var current_date = from_date;

        while (current_date <= to_date) {
            var date = new Date(current_date);
            var m = ( date.getMonth () + 1 ).toString ().replace ( /(^.$)/, "0$1" );
            var y = date.getFullYear ();
            var d = ( date.getDate () < 10 ? '0' : '' ) + date.getDate ()
            var f_date = y + '-' + m + '-' + d;
            dates.push ( ( f_date ) );
            current_date.setDate ( current_date.getDate () + 1 );
        }
        return dates;
    }

    // Used while exporting Leads to Excel
    // remove Actions column form Printed Excel sheet.
    let colId = 0;
    let colArray = [];
    while (colId < len_cols) {
        if ( colId !== 15 ) {
            colArray.push ( colId );
        }
        colId++;
    }

    // Appends Export Button to datatable.
    var buttons = new $.fn.dataTable.Buttons ( table, {
        buttons: [
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: colArray,
                }
            }
        ]
    }).container().appendTo($('#export-table'));

    // Hide the Custom fields by default.
/*    $( '#show-cus-fields' ).trigger( "click" );*/

});
