/**
 *
 * @internal This document handles external data queue UI i.e table and pagination
 * @global path(i.e route) = paging.externaldataqueue, viewType = shopdataqueue
 * @author Aki
 */

// on page load functions
$(document).ready(function () {
    //new table div template
    emptyTableTemplate = queueTable.html();
    //@internal - get initial data for page 1 and adds table
    drawTable(1);
    //Make all active
    $('#all').addClass('active');
})

//variables for storing meta data
let emptyTableTemplate;//  Empty table Html - used as template for changing page numbers
let emptyPaginationBarTemplate;//  Empty pagination Html - used as template for changing page numbers
let recordsPerPage = 10;// Number of records per page
let totalNumberOfRecords;
let totalNumberOfPages;
let currentPage;
let userName;
let queueStatus = 'all';
let webhookSelectName = 'all'

//html div
const queueTable = $('#ShopDataQueueTable');


/**
 * @internal - get data for contact view. Generates - {Table,Pagination_bar}. The parameter - daterange is dynamically taken
 * @param {int}pageNumber data collected from UI
 * @param {null/array}data data collected from API/ route
 * @param {null/string}webhookName
 * @param {null/string}queueStatus ( 'new' | 'working'| 'failed' | 'done' )
 * @returns {void}
 */
function drawTable(pageNumber,data,webhookName,queueStatus) {
    //get contact data
    if(!data)
        data = getData(pageNumber,webhookName,queueStatus)

    if(data.length >= 1 ){
        //Generate and append html  with data
        generateHtmlAndAppendToTable(formatDataForQueueView(data));
        //Generate and  append pagination list
        generatePaginationList(pageNumber);
        currentPage = pageNumber;
    }else{
        queueTable.html('No records found , Please refresh the page or try other search')
    }
}

/**
 * @internal - formate - creates the order in data
 * @param {array}data
 * @returns {array}
 */
function formatDataForQueueView(data){
    let formattedData = [];
    $.each(data, function(index, value){
        //map is used because of order of the fields is very important
        let tempObj = new Map();
        tempObj.set('Update',value.updated);
        tempObj.set('Webhook',value.webhook);
        tempObj.set('Webhook_ID',value.webhook_id);
        tempObj.set('Status',value.status);
        tempObj.set('Start',value.start);
        tempObj.set('End',value.end);
        tempObj.set('Actions',value.id);

        formattedData.push(tempObj);
    });
    return formattedData;
}


/**
 * @internal - get data form API
 * @param {int}pageNumber
 * @param {string}webhookName
 * @param {string}queueStatus ( 'new' | 'working'| 'failed' | 'done' )
 * @returns {array}
 */
function getData(pageNumber,webhookName,queueStatus){
    //Get the payload
    let payloadData = getPayloadData(webhookName,queueStatus);
    let queueData
    //Get the data form the server side
    $.ajax({
        url: path + pageNumber,
        type: 'POST',
        async: false,
        data: payloadData,
        success: function (data) {
            if (data.status === true) {
                queueData = data.records;
                totalNumberOfRecords = data.total_number_of_rows;
                userName = data.current_user.company_name;
            } else {
                //show message on table
                $('#contactViewTable').html('No records found , Please refresh the page or try new settings');
                //$('#contactViewTable').html('data.message');
                queueData = [];
            }
        }
    });
    return queueData;
}


/**
 * @internal - Generates html and appends to table
 * @param {array}data data form the pagination API
 * @returns {void}
 * todo: Need to change this as sven design
 */
function generateHtmlAndAppendToTable(data) {
    //Html templates and divs
    const contactTableHeadRow = $("#ShopDataQueueTable > thead > tr");
    const contactTableBody = $("#ShopDataQueueTable > tbody");

    //add head to table
    $.each(allAvailableFieldsForView, function (key, value) {
        //thead template
        let theadTemplate = `<th id='` + value + `'><b>` + value + `</b></th>`;
        contactTableHeadRow.append(theadTemplate);
    });
    //Add body to table - As the order is defined in backend just append the data
    $.each(data, function (key, value) {
        //take table row template
        const tbodyRow = $('#body_row_template');
        for (let [index, field] of value) {
            let rowHtml = generateTbodyRowHtml(index, field);
            //template
            let theadTemplate = `<td  class="` + index + `">` + rowHtml + `</td>`;
            tbodyRow.append(theadTemplate);
        }
        //dump html to table body
        let tr = `<tr> ` + tbodyRow.html() + `</tr>`
        contactTableBody.append(tr)
        //empty tbody row for loop use/next value
        tbodyRow.html('');
    });
}


/**
 * @internal - Generates payload for the request
 * @param {string}webhookName search text string
 * @param {string}queueStatus status name ( 'new' | 'working'| 'failed' | 'done' )
 * @returns {string}
 */
function getPayloadData(webhookName ='all',queueStatus ='all'){
    return JSON.stringify({
        "viewname":viewName,
        "webhookname":webhookName,
        "statusqueue":queueStatus,
        "records_per_page":recordsPerPage
    })
}


/**
 * @internal - Generates html for table_body  with different styling like actions and status
 * @param {string} fieldName data form the pagination API
 * @param {string} fieldValue data form the pagination API
 * @returns {string} html string
 */
function generateTbodyRowHtml(fieldName, fieldValue) {

    //variable for status
    let status = {
        new: {'title': 'new', 'class': ' kt-badge--secondary'},
        working: {'title': 'working', 'class': ' kt-badge--warning'},
        failed: {'title': 'failed', 'class': ' kt-badge--danger'},
        done: {'title': 'done', 'class': ' kt-badge--success'},
    }
    if(fieldName === 'Start' || fieldName ===  'End'){
        if(!fieldValue){
            return  '';
        }
    }

    if (fieldName === 'Status') {
        return '<span class="kt-badge ' + status[fieldValue].class + ' kt-badge--inline kt-badge--pill">' + status[fieldValue].title + '</span>'
    }
    if (fieldName === 'Actions') {
        //holding html div in jquery
        const dropdownTd = $('#tbody_dropdown_template');
        const dropdownMenu = $('#tbody_dropdown_template .dropdown-menu');
        //urls
        let queueDownloadUrl = '/webhook/queue/download/' + fieldValue ;
        //Back up the template
        let templateBackup = dropdownTd.html();
        //Add <a> tags to the template
        let aTagDetails = `<a class="dropdown-item" id="download_queue" href=` + queueDownloadUrl + `><i
                      class="la la-download"></i> Download</a>`
        dropdownMenu.append(aTagDetails);
        //rest process for the html template
        //store html in string
        let html = dropdownTd.html();
        //reset the template
        dropdownTd.html(templateBackup);
        //return the string
        return html;
    }
    //if no styling return the normal text field
    return fieldValue;
}


/**
 * @internal - Generates pagination bar
 * todo: Handle all the page follow up
 * @returns {void}
 */
function generatePaginationList(pageNumber){
    const paginationUl = $('#paginationUl');
    //Hold the pagination bar
    const paginationBar = $('#pagination_bar');
    if(paginationUl.find('li').length > 0){
        //if already present reset the pagination bar
        paginationBar.html(emptyPaginationBarTemplate);
    }else{
        //Save the template
        emptyPaginationBarTemplate = paginationBar.html();
    }
    //Calculate number of pages
    if(totalNumberOfRecords % recordsPerPage === 0){
        totalNumberOfPages = Math.trunc(totalNumberOfRecords / recordsPerPage)
    }else{
        //Fill the template
        totalNumberOfPages = Math.trunc(totalNumberOfRecords / recordsPerPage) + 1;
    }

    //add numbers
    if(totalNumberOfPages <= 8){
        for(let n = 1 ; n <= totalNumberOfPages; ++n){
            addPageListToTable(n);
        }
    }else{
        //changing pattern
        if(pageNumber > 4 && pageNumber < totalNumberOfPages-2){
            //Generate first 10 number and last two number and dots in between
            for(let n = 1 ; n <= 3 ; ++n){
                addPageListToTable(n);
            }
            //add dots
            addPageListToTable('.');
            //add page number nearby elements
            addPageListToTable(pageNumber-1);
            addPageListToTable(pageNumber);
            addPageListToTable(pageNumber+1);
            //add dots
            addPageListToTable('.');
            addPageListToTable(totalNumberOfPages-2);
            addPageListToTable(totalNumberOfPages-1);
            addPageListToTable(totalNumberOfPages);

        }else{
            //Generate first 10 number and last two number and dots in between
            for(let n = 1 ; n <= 5 ; ++n){
                addPageListToTable(n);
            }
            //add dots
            addPageListToTable('.');
            //add last three pages in view
            addPageListToTable(totalNumberOfPages-2);
            addPageListToTable(totalNumberOfPages-1);
            addPageListToTable(totalNumberOfPages);
        }
    }

    //un-hide the div
    $('#pagination_nav_bar').removeAttr('hidden');
    $('#pagination_nav_bar').show();//test
    //make current page active
    makeCurrentPageActive(pageNumber);
}


/**
 * @internal - Generates and appends html for pagination
 * @internal - Add next button , dots, previous button
 * @returns {void}
 */
function addPageListToTable(number){
    const paginationUl = $('#paginationUl');
    if(number === 1){
        //add previous button
        paginationUl.append($('#previous_template').html());
    }
    let pageHtml
    if(number !== '.'){
        pageHtml = `<li class="page-item pagination-bar" id="page_` + number + `"><a class="page-link" href="#" >` + number + `</a></li>`
    }else{
        pageHtml = '  .  .  .  .  .  '
    }
    paginationUl.append(pageHtml);
    if(number === totalNumberOfPages ){
        //add Next button ----
        paginationUl.append($('#next_template').html())
    }
}

/**
 * @internal - Makes the page button look active
 * @returns {void}
 */
function makeCurrentPageActive(pageNumber){
    //Make current page active
    $('#page_'+pageNumber).addClass('active');
    //init the pagination bar
    initPaginationBar();
}

/**
 * @internal - On pagination bar click
 */
function initPaginationBar(){
    //event runner stops multiple events running
    let eventRunning;
    let pageNumber;
    $('.page-item').click(function () {
        if(!eventRunning){
            eventRunning = true;
            let page = $(this).attr('id').replace("page_","");
            if(page === 'next' && currentPage !== totalNumberOfPages){
                pageNumber = Number(currentPage)+1;
            }else if(page === 'previous' && currentPage !== 1){
                pageNumber = Number(currentPage)-1;
            }else{
                pageNumber = Number(page);
            }
            //prevent same requests
            if(!$(this).hasClass('active') && Number.isInteger(pageNumber)){
                //Get the values for base
                let contactData = getData(pageNumber,webhookSelectName,queueStatus);
                //Reset the table
                queueTable.html(emptyTableTemplate);
                $('#pagination_bar').html(emptyPaginationBarTemplate);
                //draw the table
                drawTable(pageNumber,contactData,webhookSelectName,queueStatus);
            }
            eventRunning = false;
        }
    })
}

//@internal - ***** jquery event based functions ******

/**
 * @internal - On select of webhook
 */
$('#webhookSelect').on('change', function() {
    webhookSelectName = $(this).find('option:selected').val();
    let queueStatus = $('#queue_status').children('.active').attr('id');
    //Get the values for base
    let data = getData(1,webhookSelectName,queueStatus);
    //Reset the table and pagination bar
    queueTable.html(emptyTableTemplate);
    $('#pagination_bar').html(emptyPaginationBarTemplate);
    //draw the table
    drawTable(1,data,webhookSelectName,queueStatus);
});


/**
 * @internal - On click of status or change of status
 */
$('.queueStatus').on('click', function() {
    if(queueStatus !==  $(this).attr('id')){
        queueStatus  = $(this).attr('id');
        //Get the values for base
        let data = getData(1,webhookSelectName,queueStatus);
        //Reset the table and pagination bar
        queueTable.html(emptyTableTemplate);
        $('#pagination_bar').html(emptyPaginationBarTemplate);
        //draw the table
        drawTable(1,data,webhookSelectName,queueStatus);
    }
});