// for prod:
// console.log = function() {}


class UsersRepository {
    constructor() {
        this.repo = [];
    }

    add(item) {
        this.repo[item.id] = item;
        return this.repo[item.id];
    }

    get(id) {
        var res = {};
        Object.entries(this.repo).forEach(([i, v]) => {
            if (v.id == id) {
                res = this.repo[i];
            }
        });
        return res;
    }

    getList() {
        return this.repo;
    }

    del(id) {
        const index = this.repo.indexOf(id);
        if (index > -1) {
            this.repo.splice(index, 1);
        }
    }

    getByInvitedUserId(id) {
        var res = {};
        Object.entries(this.repo).forEach(([i, v]) => {
            if (v.invited_user_id == id) {
                res = this.repo[i];
            }
        });
        return res;
    }

    getUserListByCompanyId(id) {
        var list = [];
        Object.entries(this.repo).forEach(([i, v]) => {
            if (v.to_company_id == id) {
                list.push(this.repo[i]);
            }
        });
        return list;
    };

}

var usersStorage = new UsersRepository();

$(document).ready(function () {

    if (administrates_users !== undefined) {
        usersStorage.add(atob(administrates_users));
    }

    function getAction() {
        return $('select#manageusers_action').val();
    }

    function setButton(btnText) {
        $('button#processuser').text(btnText);
    }

    function clearForm() {
        var inputList = $('form#manageusers').find("input").not("input[type='hidden']");
        inputList.each(function (i, input) {
            $(input).val('');
        })
        $('select#role').val('');
        $('select#to_company').val('');
    }

    function mapFormData(formSelector) {
        form = $(formSelector);
        let formData = form.serializeArray();

        // if you want to use own form field names/id (e.g. to transfer data with another ajax call to your database),  add the predefined input/select/checkbox/radio names to data-mapto="<treaction predefined field>"
        // e.g. predefined input:
        //          <input id="postal_code" name="postal_code" placeholder="*Postal Code" type="text" value="">  <!-- "postal_code" is defined by campaign configuration and must be submitted as postal_code to campaign-system -->
        // if you want to use PLZ instead of postal_code input would look like that:
        //          <input id="PLZ" name="PLZ" data-mapto="postal_code" placeholder="*Postleitzahl" type="text" value="">
        // (customer fields can changed to type "hidden", but then must have attribute value set to your custom value. E.g. <input type="hidden" name="<treaction predefined field>" value="<your custom value>")
        console.log('mapping', formData);
        $.each(formData, function (index, item) {
            let mapto = $(formSelector + ' ' + 'input[name="' + item.name + '"]').data('mapto');
            item.name = (mapto !== undefined) ? mapto : item.name;
        })
        let formJson = JSON.stringify(formData);
        console.log('formJson', formJson);
        let formbtoa = btoa(formJson);
        console.log('formbtoa', formbtoa);
        return formbtoa;
    }

    // this can be improved by adding corresponding classes to all div-blocks
    $('select#manageusers_action').on('change', function () {

        var action = getAction();
        var buttonClear = $('button#clearusersform');
        var selectedUserSelectBox = $('div#selected_user_block');
        var selectedCompanySelectBox = $('div#selected_company_block');
        var toCompanySelectBox = $('div#to_company_block');
        var inviteUserInputs = $('div.invite_user');
        var roleInput = $('div#role_block');
        var defineDetailsText = $('div#define_details');

        switch (action) {
            case 'invite_user':
                clearForm();
                selectedUserSelectBox.css('display', 'none');
                selectedCompanySelectBox.css('display', 'none');
                toCompanySelectBox.css('display', 'block ruby');
                inviteUserInputs.css('display', 'block ruby');
                roleInput.css('display', 'block ruby');
                defineDetailsText.css('display', 'block');
                buttonClear.css('display', 'initial');
                setButton('Invite User');
                $('div#notmyself').css('display', 'initial');
                $('button#processuser').prop("disabled",false);
                break;
            case 'edit_user':
                selectedUserSelectBox.css('display', 'block ruby');
                selectedCompanySelectBox.css('display', 'block ruby');
                toCompanySelectBox.css('display', 'block ruby');
                inviteUserInputs.css('display', 'block ruby');
                roleInput.css('display', 'block ruby');
                defineDetailsText.css('display', 'block');
                buttonClear.css('display', 'none');
                setButton('Update User');
                break;
            case 'change_role':
                selectedUserSelectBox.css('display', 'block ruby');
                selectedCompanySelectBox.css('display', 'block ruby');
                toCompanySelectBox.css('display', 'none');
                inviteUserInputs.css('display', 'none');
                roleInput.css('display', 'block ruby');
                defineDetailsText.css('display', 'block');
                buttonClear.css('display', 'none');
                setButton('Change Role');
                break;
            case 'block_user':
                selectedUserSelectBox.css('display', 'block ruby');
                selectedCompanySelectBox.css('display', 'block ruby');
                toCompanySelectBox.css('display', 'none');
                inviteUserInputs.css('display', 'none');
                roleInput.css('display', 'none');
                defineDetailsText.css('display', 'none');
                buttonClear.css('display', 'none');
                setButton('Block User');
                break;
            case 'unblock_user':
                selectedUserSelectBox.css('display', 'block ruby');
                selectedCompanySelectBox.css('display', 'block ruby');
                toCompanySelectBox.css('display', 'none');
                inviteUserInputs.css('display', 'none');
                roleInput.css('display', 'none');
                defineDetailsText.css('display', 'none');
                buttonClear.css('display', 'none');
                setButton('Unblock User');
                break;
            case 'delete_user':
                selectedUserSelectBox.css('display', 'block ruby');
                selectedCompanySelectBox.css('display', 'block ruby');
                toCompanySelectBox.css('display', 'none');
                inviteUserInputs.css('display', 'none');
                roleInput.css('display', 'none');
                defineDetailsText.css('display', 'none');
                buttonClear.css('display', 'none');
                setButton('Delete User');
                break;
            case 'change_company_membership':
                selectedUserSelectBox.css('display', 'block ruby');
                selectedCompanySelectBox.css('display', 'block ruby');
                toCompanySelectBox.css('display', 'block ruby');
                inviteUserInputs.css('display', 'none');
                roleInput.css('display', 'none');
                defineDetailsText.css('display', 'block');
                buttonClear.css('display', 'none');
                setButton('Change Company Membership');
                break;

        }


        /*        var button = $("button#processcompany");

                if (action === 'del_company') {
                    $('button#clearform').css('display', 'none');
                    $("div#company_list").css('display', 'block ruby');
                    $("div#add_company_name").css('display', 'none');
                    $('div#mainformdata').css('display', 'none');
                    $('div#deletecompanyhint').css('display', 'initial');
                    button.text('Delete Company');
                } else {
                    $('div#mainformdata').css('display', 'initial');
                    $('div#deletecompanyhint').css('display', 'none');
                    $('button#clearform').css('display', 'initial');
                }

                if (action === 'update_company') {
                    // show select box
                    $("div#company_list").css('display', 'block ruby');
                    // $("div#add_company_name").css('display', 'none');
                    setUsersCompanyToForm();
                    button.text('Update Company');
                }

                if (action === 'add_company') {
                    // clear form
                    console.log('none', $(this).val());
                    $("div#company_list").css('display', 'none');
                    // $("div#add_company_name").css('display', 'block ruby');
                    button.text('Add Company');
                    clearForm();
                }*/
    })


    $('select#selected_company').on('change', function getUsersBelongingToCompany() {
        clearForm();

        var route = atob($(this).data('route'));
        var selected_company = $(this).find(':selected').val();
        var action = $('select#manageusers_action').find(':selected').val();

        var storageList = usersStorage.getUserListByCompanyId(selected_company);
        console.log('storageList', storageList);

        // get user list from storage and return without ajax call
        if (storageList.length > 0) {

            var count = 0
            $('select#selected_user').empty();
            $.each(storageList, function (i, v) {
                if (count === 0) {
                    var baseoption = new Option('Select User', -1);
                    $('select#selected_user').append($(baseoption)).prop('selected');

                    // $('input#salutation').val(v.salutation);
                    // $('input#first_name').val(v.invited_user_first_name);
                    // $('input#last_name').val(v.invited_user_last_name);
                    // $('input#email').val(v.invited_user_email);
                }
                count++;

                var option = new Option(v.invited_user_email, btoa(v.invited_user_id));
                $('select#selected_user').append($(option));
                $(option).attr('data-adminstratedby', btoa(v.user_id))
                    .attr('data-company_id', btoa(v.to_company_id))
                    .attr('data-roles', btoa(v.invited_user_roles));

            });
            return;
        }

        var settings = {
            url: route,
            method: "POST",
            timeout: 0,
            headers: { // 'Content-Type: application/json; charset=utf-8'
                'Content-Type': "text/html; charset=utf-8"
            },
            data: btoa(JSON.stringify({'selected_company': selected_company})),
            success: function (data) {
                if (data.status === true) {
                    console.log('content', data.content);
                    if (data.content !== '' && data.content !== undefined) {

                        /// first delete all options of select
                        $('select#selected_user').empty();
                        /*
                                                <select id="selected_user" class="form-control" name="selected_user">
                                                        {% for admin_user in administrates_users %}
                                                            <option class="select-option" value="{{ admin_user.invited_user_id }}" data-company_id="{{ admin_user.to_company_id }}" data-roles="{{ admin_user.with_roles|base64encode }}">{{ admin_user.invited_user_email }}</option>
                                                        {% endfor %}
                                                </select>

                                                    id: "41-106"
                        ​                            invited_user_company_name: "dev-campaign-in-one"
                                                    invited_user_email: "test20@dev-campaign-in-one.net"
                                                    invited_user_failed_logins: "0"
                                                    invited_user_first_name: "Donald"
                                                    invited_user_id: "106"
                                                    invited_user_last_name: "Duck"
                                                    invited_user_roles: "[\"ROLE_USER\"]"
                                                    invited_user_status: "active"
                                                    to_company_id: "41"
                                                    user_id: "82"
                        */
                        // may be an array => loop and add to usersStorage
                        var count_i = 0;

                        $.each(data.content, function (i, v) {
                            if (count_i === 0) {
                                var baseoption = new Option('Select User', -1);
                                $('select#selected_user').append($(baseoption)).prop('selected');

                                //$('input#salutation').val(v.salutation);
                                //$('input#first_name').val(v.invited_user_first_name);
                                //$('input#last_name').val(v.invited_user_last_name);
                                //$('input#email').val(v.invited_user_email);
                            }
                            count_i++;
                            v.id = i;
                            var key = usersStorage.add(v);
                            var option = new Option(v.invited_user_email, btoa(v.invited_user_id));
                            $('select#selected_user').append($(option));
                            $(option).attr('data-adminstratedby', btoa(v.user_id))
                                .attr('data-company_id', btoa(v.to_company_id))
                                .attr('data-roles', btoa(v.invited_user_roles));
                            // <option class="select-option" value="MQ==" data-adminstratedby="ODI=" data-company_id="MQ==" data-roles="WyJST0xFX1VTRVIiLCAiUk9MRV9TVVBFUl9BRE1JTiJd">pradeep.veera@treaction.de (Admin-Test18 Achtzehn) </option>
                        });
                    } else {
                        // show empty users list
                        $('select#selected_user').empty();
                    }
                } else {
                    $('select#selected_user').empty();
                    console.log('no data ...', route);
                }
            }
        };

        $.ajax(settings)
            .done(function (data) {
                console.log('DONE');
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log("request failed", textStatus, errorThrown);
            });

    })


    $('select#selected_user').on('change', function fillForm() {
        var option = $(this).find(':selected');
        var invited_user_id = option.val();
        var action = getAction();

        var currentUser = atob($(this).data('cu'));

        if( action !== 'invite_user' && currentUser === option.text()) {
            $('div#notmyself').css('display', 'none');
            $('button#processuser').prop("disabled",true);
        } else {
            $('div#notmyself').css('display', 'initial');
            $('button#processuser').prop("disabled",false);
        }

        var v = usersStorage.getByInvitedUserId(atob(invited_user_id));

        if (invited_user_id === -1) {
            return; // header option
        } else if(v !== undefined && v !== '' && v !== null)  {
            console.log('invited_user_id', atob(invited_user_id), 'company', v.invited_user_company_name);
            console.log('v', v);
            $('input#salutation').val(v.invited_user_salutation);
            $('input#first_name').val(v.invited_user_first_name);
            $('input#last_name').val(v.invited_user_last_name);
            $('input#email').val(v.invited_user_email);
            $('select#to_company').val(btoa(v.to_company_id));
            var role = v.invited_user_roles;
            console.log('role', role);

            if (role.search('SUPER') > -1) {
                $('select#role').val('SUPER')
            } else if(role.search('MASTER') > -1) {
                $('select#role').val('MASTER');
            } else if(role.search('ADMIN') > -1) {
                $('select#role').val('ADMIN');
            }  else {
                $('select#role').val('USER');
            }


            /*          id: "41-106"
                        invited_user_company_name: "dev-campaign-in-one"
                        invited_user_email: "test20@dev-campaign-in-one.net"
                        invited_user_failed_logins: "0"
                        invited_user_first_name: "Donald"
                        invited_user_id: "106"
                        invited_user_last_name: "Duck"
                        invited_user_roles: "[\"ROLE_USER\"]"
                        invited_user_status: "active"
                        to_company_id: "41"
                        user_id: "82"
            */
        }

        // else get user from ajax

    })

    /**
     * BUTTON: process the action
     */
    $('button#processuser').on('click', function processuser() {
        var msgdiv = $('div#manage_user_msg');
        var action = getAction();
        var route = atob($(this).data('route_' + action));

        var company_id = $('select#selected_company').find(":selected").val();
        var user_id = $('select#selected_user').find(":selected").val();

        console.log(route, action, user_id, company_id);
        if (action === 'edit_user' || action === 'block_user' || action === 'unblock_user') {
            console.log('action', action, 'delete', company_id + '-' + atob(user_id));
            usersStorage.del(company_id + '-' + atob(user_id)); // force new ajax request
            console.log('usersStorage after delete', usersStorage.getList());
        }

        var settings = {
            url: route,
            method: "POST",
            timeout: 0,
            // headers: { // 'Content-Type: application/json; charset=utf-8'
            //     'Content-Type': "text/html; charset=utf-8"
            // },
            data: mapFormData('form#manageusers'),
            success: function (data) {

                if (data.msg !== '' || data.msg !== undefined) {
                    if (data.status === true) {
                        msgdiv.text(data.msg).css('color', 'green').fadeIn(100).fadeOut(10000);
                        console.log('content', data.content);

                        if (action === 'delete_user') {
                            msgdiv.text(data.msg).css('color', 'green').fadeIn(100).fadeOut(10000);
                            usersStorage.del(company_id + '-' + user_id);
                        }

                        if (action === 'block_user') {
                            msgdiv.text(data.msg).css('color', 'green').fadeIn(100).fadeOut(10000);
                        }

                        if (action === 'unblock_user') {
                            msgdiv.text(data.msg).css('color', 'green').fadeIn(100).fadeOut(10000);
                        }

                        console.log('data.content', data.content);

                        // all actions including edit_user, but except delete_user
                        if (data.content !== '' && data.content !== undefined && action !== 'delete_user') {
                            console.log('userStorage before', usersStorage.getList())
                            //console.log('add to userStorage for action', action,'data.content', data.content);
                            $.each(data.content, function (i, v) {
                                v.id = i;
                                var key = usersStorage.add(v);
                            });
                            console.log('userStorage after', usersStorage.getList())
                            msgdiv.text(data.msg).css('color', 'green').fadeIn(100).fadeOut(10000);

                        }
                    } else {
                        msgdiv.text(data.msg).css('color', 'red').fadeIn(100).fadeOut(10000);
                    }
                }
            }
        };

        $.ajax(settings)
            .done(function (data) {
                console.log('DONE');
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log("request failed", textStatus, errorThrown);
            });


        function addItemsToStorage(content) {
            // deprecated
        }

    });


    $('button#clearusersform').on('click', function fClearForm() {
        clearForm()
    });


})
