class CompanyRepository {
    constructor() {
        this.company = [];
    }

    add(company) {
        this.company[company.id] = company;
    }

    get(id) {
        return (this.company[id] !== undefined) ? this.company[id] : null;
    }

    getList() {
        return this.company;
    }

    del(id) {
        const index = this.company.indexOf(id);
        if (index > -1) {
            this.company.splice(index, 1);
        }
    }
}

var companyStorage = new CompanyRepository();
companyStorage.add(users_company);


$(document).ready(function () {


    function getAction() {
        return $('select#managecompany_action').val();
    }


    function mapFormData(formSelector) {
        form = $(formSelector);
        let formData = form.serializeArray();

        // if you want to use own form field names/id (e.g. to transfer data with another ajax call to your database),  add the predefined input/select/checkbox/radio names to data-mapto="<treaction predefined field>"
        // e.g. predefined input:
        //          <input id="postal_code" name="postal_code" placeholder="*Postal Code" type="text" value="">  <!-- "postal_code" is defined by campaign configuration and must be submitted as postal_code to campaign-system -->
        // if you want to use PLZ instead of postal_code input would look like that:
        //          <input id="PLZ" name="PLZ" data-mapto="postal_code" placeholder="*Postleitzahl" type="text" value="">
        // (customer fields can changed to type "hidden", but then must have attribute value set to your custom value. E.g. <input type="hidden" name="<treaction predefined field>" value="<your custom value>")
        console.log('mapping', formData);
        $.each(formData, function (index, item) {
            let mapto = $(formSelector + ' ' + 'input[name="' + item.name + '"]').data('mapto');
            item.name = (mapto !== undefined) ? mapto : item.name;
        })
        let formJson = JSON.stringify(formData);
        let formbtoa = btoa(formJson);
        return formbtoa;
    }

    $('button#clearform').on('click', function fClearForm() {
        clearForm()
    });

    function clearForm() {
        var inputList = $('form#form_add_company').find("input").not("input[type='hidden']");
        console.log(inputList);
        inputList.each(function (i, input) {
            $(input).val('');
        })
        //todo: reset select box
    }


    function setUsersCompanyToForm() {
        companyStorage.add(users_company);
        $.each(users_company, function (inputname, value) {
            if (inputname === 'name') {
                inputname = 'company_name';
            }
            let ele = $('form#form_add_company').find('[name="' + inputname + '"]');

            if (ele.length > 0) {
                ele.val(value);
            }
        })
    }


    /**
     * action: add company
     * check if company name is free to use
     * listener turned off due to requirement of WEB-4088
     */
/*    $('input[name="company_name"]').on('blur', function () {

        var input_company_name = $(this);
        var company_name = $(this).val();
        var old_company_name = $(this).data('oldcompanyname');

        var action = getAction();
        var route = atob($(this).data('route'));
        var divmsg = $('div#company_msg');

        console.log(old_company_name, company_name, action, route);

        if (old_company_name === company_name) {
            return;
        }

        var settings = {
            url: route,
            method: "POST",
            timeout: 0,
            headers: {
                'Content-Type': "application/x-www-form-urlencoded"
            },
            data: btoa(JSON.stringify({
                company_name: company_name,
                old_company_name: old_company_name,
                action: action
            })),
            success: function (data) {
                if (data.status === true) {
                    input_company_name.val(data.company_name);
                    divmsg.text(data.msg).css('color', 'green').fadeIn(1000).fadeOut(20000);
                } else {
                    input_company_name.val(old_company_name);
                    divmsg.text(data.msg).css('color', 'red').fadeIn(1000).fadeOut(20000);
                }
            }
        };

        $.ajax(settings)
            .done(function (data) {
                console.log("request to " + route + " successful");
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log("request to " + route + " failed");
            });

        // on error
        // $('div#company_msg').text(msg).css('color', 'red').fadeOut(10000);
        // on success
        // $('div#company_msg').text(msg).css('color', 'green').fadeOut(10000);
    });*/

    /**
     * Choose Action: switch form parts on and off
     */
    $('select#managecompany_action').on('change', function () {

        var action = getAction();
        var button = $("button#processcompany");

        var menu_userlogincompany = $('span#menu_userlogincompany').data('id');
        var company_id = $('select#selected_company').find(":selected").val();

        var goingToDeleteOwnCompany = (menu_userlogincompany == company_id) ? true : false;

        console.log(menu_userlogincompany, company_id, goingToDeleteOwnCompany);

        if(goingToDeleteOwnCompany) {
            $('div#deletelogincompanyhint').css('display', 'block ruby');
        } else {
            $('div#deletelogincompanyhint').css('display', 'none');
        }


        if (action === 'del_company') {
            $('button#clearform').css('display', 'none');
            $("div#company_list").css('display', 'block ruby');
            $("div#add_company_name").css('display', 'none');
            $('div#mainformdata').css('display', 'none');
            $('div#deletecompanyhint').css('display', 'initial');
            button.text('Delete Company');
        } else {
            $('div#mainformdata').css('display', 'initial');
            $('div#deletecompanyhint').css('display', 'none');
            $('div#deletelogincompanyhint').css('display', 'none');
            $('button#clearform').css('display', 'initial');
        }

        if (action === 'update_company') {
            // show select box
            $("div#show_only_on_edit").css('display', 'block ruby');
            $("div#company_list").css('display', 'block ruby');
            $('div#add_company_name').css('display', 'block ruby');
            setUsersCompanyToForm();
            button.text('Update Company');
        } else {
            $("div#show_only_on_edit").css('display', 'none');
            $("div#add_company_name").css('display', 'none');
        }

        if (action === 'add_company') {
            // clear form
            console.log('none', $(this).val());
            $("div#company_list").css('display', 'none');
            $('div#add_company_name').css('display', 'block ruby');
            button.text('Add Company');
            clearForm();
        }
        // else {
        //     $('div#add_company_name').css('display', 'none');
        // }
    })


    /**
     * Choose Company: SELECT BOX
     */
    $('select#selected_company').on('change', function () {

        var action = getAction();
        var input_company_name = $(this);
        var company_id = $(this).val();
        var route = atob($(this).data('route'));
        var divmsg = $('div#company_action_msg');
        var oldcompanyname = $(this).find(":selected").text();
        $('input#company_name').attr('data-oldcompanyname', oldcompanyname);
        $('input#company_id').val(company_id);
        $('input#company_identifier').val(company_id).attr('title', company_id);

        var menu_userlogincompany = $('span#menu_userlogincompany').data('id');
        var goingToDeleteOwnCompany = (menu_userlogincompany == company_id) ? true : false;

        console.log(menu_userlogincompany, company_id, goingToDeleteOwnCompany);

        if(action === 'del_company' && goingToDeleteOwnCompany) {
            $('div#deletelogincompanyhint').css('display', 'block ruby');
        } else {
            $('div#deletelogincompanyhint').css('display', 'none');
        }

        console.log(company_id, route); // todo: remove
        comp = companyStorage.get(company_id);

        if (comp !== null) {
            console.log('from chache ;)')
            users_company = comp;
            setUsersCompanyToForm();
            return;
        }

        var settings = {
            url: route,
            method: "POST",
            timeout: 0,
            headers: {
                'Content-Type': "application/x-www-form-urlencoded"
            },
            data: btoa(JSON.stringify({company_id: company_id})),
            success: function (data) {
                if (data.status === true) {
                    console.log('content', data.content);
                    users_company = data.content;
                    setUsersCompanyToForm();
                } else {
                    divmsg.text(data.msg).css('color', 'red').fadeIn(1000).fadeOut(20000);
                }
            }
        };

        $.ajax(settings)
            .done(function (data) {
                console.log("request to " + route + " successful");
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log("request to " + route + " failed");
            });
    });


    /**
     * BUTTON: process the action
     */
    $('button#processcompany').on('click', function processcompany() {
        var msgdiv = $('div#add_company_msg');
        var action = getAction();
        var route = atob($(this).data('route_' + action));
        var logoutUrl = atob($(this).data('route_logout'));
        var company_id = $('select#selected_company').find(":selected").val();
        var company_name = $('select#selected_company').find(":selected").text();

        if(action === 'update_company' ) {
            var old_li = $('li#' + 'ACC__' + company_name.replace(/\s/g, ''));
        }


        var menu_userlogincompany = $('span#menu_userlogincompany').data('id');
        var logoutAfterAction = (menu_userlogincompany == company_id) ? true : false;
        console.log('in ' + action, menu_userlogincompany, company_id, logoutAfterAction);
        console.log(route, action);

        var settings = {
            url: route,
            method: "POST",
            timeout: 0,
            headers: {
                'Content-Type': "application/x-www-form-urlencoded"
            },
            data: mapFormData('form#form_add_company'),
            success: function (data) {

                if (data.msg !== '' || data.msg !== undefined) {
                    if (data.status === true) {
                        msgdiv.text(data.msg).css('color', 'green').fadeIn(100).fadeOut(10000);
                        console.log('content', data.content);
                        // perhaps add company data content to object by id
                        // add option to company SELECT BOX

                        if (action === 'add_company') {
                            var option = new Option(data.content.name, data.content.id);
                            data.content.company_name = data.content.name; // set company_name
                            $('select#selected_company').append($(option));
                            companyStorage.add(data.content);

                            // increase company counter
                            no_companies = Number($('label#labelcountcompanies').text().split(' ')[0]) + 1;
                            console.log('no_companies', no_companies); // todo: remove
                            if (no_companies < 0) {
                                no_companies = 0;
                            }
                            text_companies = (no_companies > 1) ? "companies" : "company";
                            $('label#labelcountcompanies').text(no_companies + ' ' + text_companies);

                            // add company to main menu
                            let li_id = 'ACC__'+data.content.name.replace(/\s/g, '');
                            let new_company_id = data.content.id;
                            let add_li =
                                '<li id="'+ li_id +'" class="kt-nav__item">' +
                                    '<a class="kt-nav__link active" href="http://ciomgrsymf.test/account/'+data.content.id+'/switch" value="'+data.content.id+'" active="">' +
                                    '<span class="kt-nav__link-text">'  + data.content.name + '</span>' +
                                '</a></li>';

                            $("ul#company_select_dropdown").append(add_li);
                        }

                        if (action === 'update_company') {
                            $('select#selected_company').find(":selected").text(data.content.company_name); // check company_name or name
                            $('input#company_name').attr('data-oldcompanyname', data.content.company_name); // check company_name or name
                            companyStorage.add(data.content);

                            // in main menu change assigned company name to users login company if the name was changed
                            if( data.content.id == $('span#menu_userlogincompany').data('id') && data.content.company_name != $('span#menu_userlogincompany').text().split(',')[0].trim()) {
                                $('span#menu_userlogincompany').text(data.content.company_name+', ');
                                $('button#company_dropdown_button').text(data.content.company_name);
                            }

                            // change the company name in dropdown if the name was changed
                            // find
                            if(data.content.company_name != company_name) {
                                let li_id = 'ACC__'+data.content.company_name.replace(/\s/g, '');
                                let new_li =
                                    '<li id="'+ li_id +'" class="kt-nav__item">' +
                                    '<a class="kt-nav__link active" href="http://ciomgrsymf.test/account/'+data.content.id+'/switch" value="'+data.content.id+'" active="">' +
                                    '<span class="kt-nav__link-text">'  + data.content.company_name + '</span>' +
                                    '</a></li>';

                                console.log('remove', old_li );
                                old_li.remove();
                                console.log('prepend', new_li );
                                $("ul#company_select_dropdown").prepend(new_li);
                            }

                        }

                        if (action === 'del_company') {
                            companyStorage.del(company_id);
                            var option = new Option(company_name, company_id);
                            $('select#selected_company option[value="' + company_id + '"]').remove();
                            // remove company from main menu
                            $('li#ACC__'+company_name.replace(/\s/g, '')).remove(); // delete company from main menu
                            no_companies = Number($('label#labelcountcompanies').text().split(' ')[0]) - 1;
                            if (no_companies < 0) {
                                no_companies = 0;
                            }
                            text_companies = (no_companies > 1) ? "companies" : "company";
                            $('label#labelcountcompanies').text(no_companies + ' ' + text_companies);

                            if(logoutAfterAction) {
                                alert("You'll be logged out now");
                                window.open(logoutUrl, '_self');
                            }

                        }
                    } else {
                        msgdiv.text(data.msg).css('color', 'red').fadeIn(100).fadeOut(10000);
                    }
                }
            }
        };

        $.ajax(settings)
            .done(function (data) {
                console.log('DONE');
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log("request failed", textStatus, errorThrown);
            });

    });

    $(window).on('beforeunload', function(event) {
        $('select#managecompany_action').val('add_company');
        clearForm();
    });

});
