$ ( 'div#alert-apikey-valid' ).hide ();
$ ( 'div#alert-apikey-invalid' ).hide ();
$ ( 'button#retry-connection' ).hide ();
$ ( 'button#cio-select-next' ).hide ();


// prefill the existing mapping to table.
const $tableID = $ ( '#table' );
const $BTN = $ ( "#finalSubmit" );
const $EXPORT = $ ( '#export' );
let endpoint = $ ( 'form#kt_form' ).data ( 'endpoint' );

// campaignInOneParams has to be MIO customfields from the backend.
let mio_customfields = $tableID.data ( 'isTest' );
let hb_customfields_mapping_from_db = $tableID.data ( 'mapping-from-db' );


/////////////////////////////////////////// Start of APIKey Validation & CustomFields /////////////////////////////////
// Verify eMIO apikey on Change
let is_valid_apikey = false;
let hb_customfields = [];
let input_apikey = $ ( 'input[name="apikey"]' );

// Validate APIKey automatically if the apikey is loaded from database.
/*
if ( input_apikey.val ().length > 4 ) {
    ajaxRequestToValidateAPIKey ( input_apikey.val () );
    ajaxRequestToGetCustomFields ( input_apikey.val () );
}
*/

// Any change in apikey triggers the click for test apikey button.
input_apikey.blur ( function () {
    $ ( 'button#cio-select-next' ).hide ();
    // $ ( 'button.test-apikey-connection' ).trigger ( 'click' );
} )

// verify eMIO apikey on click TestConnection
$ ( 'button.test-apikey-connection' ).on ( 'click', function () {
    let input_apikey_value = input_apikey.val ();
    ajaxRequestToValidateAPIKey ( input_apikey_value );
    ajaxRequestToGetCustomFields ( input_apikey_value );
} );

/**
 * ajax call to hb microserver to get list of customfields
 * @param apikey_value
 */
function ajaxRequestToGetCustomFields(apikey) {
    let payload = btoa ( JSON.stringify ( {'apikey': apikey} ) );
    let settings_get_hb_customfields = {
        url: endpoint + '/contact/properties/get',
        method: 'POST',
        async: 'true',
        timeout: 0,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: payload,
        success: function (data) {
            console.log ( 'hb customfields', data );
            if ( data.status === true ) {
                hb_customfields = data;
            }
            console.log ( data.message );
        },
        error: function (data) {
            console.log ( 'error', data );
        }
    };
    console.log ( 'settingsFromDB', settings_get_hb_customfields );

    // ajax call to hb customfields
    $.ajax ( settings_get_hb_customfields )
        .done ( function () {
            // prepopulate the intial customfields
            miofromDb = getMIOCustomFieldsFromMapping ();
            console.log ( 'miofromDb', miofromDb );
            if ( mio_customfields ) {
                let mio_customfields_arr = $.parseJSON ( mio_customfields );
                $.each ( mio_customfields_arr, function (index, value) {
                    // DONT ALLOW IF MIO CUSTOMFIELD NAME IS EMPTY, EVEN THOUGH IT HAS OBJECT_REG_ID
                    if ( value['fieldname'].trim () === '' ) {
                        return;
                    }
                    let fieldname = value['fieldname'];
                    console.log ( 'Fieldname', fieldname );
                    console.log ( jQuery.inArray ( fieldname, miofromDb ) === -1 );
                    if ( jQuery.inArray ( fieldname, miofromDb ) === -1 ) {
                        let mioSelect = helperFunctionForSelectField ( mio_customfields_arr, fieldname );
                        let hbCustomFields = generateSelectForHBCustomFields ( hb_customfields );
                        const newTrTest = `
                            <tr class="">
                                <td class="pt-3-half pt-3-half select2 CIO" contenteditable="true">` + mioSelect + `</td>
                                <td class="pt-3-half select2 hb-custom-fields" contenteditable="true">` + hbCustomFields + `</td>
                     
                                <td><span class="table-remove">
                                    <i style="color:#ff0000;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                </td>
                            </tr>
                            `;
                        //appending html to
                        $ ( 'tbody' ).append ( newTrTest );
                    }
                } );
            }

            console.log ( 'done' );
        } );
}

function getMIOCustomFieldsFromMapping() {
    let mioCustomfields = [];
    if ( hb_customfields_mapping_from_db ) {
        mapping_from_db = JSON.parse ( atob ( hb_customfields_mapping_from_db ) );
        $.each ( mapping_from_db, function (db_index, db_value) {
            mioCustomfields.push ( db_value['mio-custom-field'] );
        } );
    }
    return mioCustomfields;
}

/**
 * Ajax Call to hb microserver to validate apikey.
 * @param apikey
 * @returns {boolean}
 */
function ajaxRequestToValidateAPIKey(apikey) {
    let input_apikey_value = apikey;
    // Check for number of length of APIKey before sending to validate
    if ( input_apikey_value.length <= 5 ) {
        input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
        $ ( 'div#alert-apikey-valid' ).hide ();
        $ ( 'div#alert-apikey-invalid' ).show ();
        $ ( 'button#test-connection' ).hide ();
        $ ( 'button#retry-connection' ).show ();
        $ ( 'button#cio-select-next' ).hide ();
        return false;
    }

    var settings_verify_apikey = {
        url: endpoint + '/apikey/check',
        method: 'POST',
        timeout: 0,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: {
            apikey: input_apikey_value
        },
        beforeSend: function (xhr) {
            input_apikey.removeClass ( 'is-invalid' ).removeClass ( 'is-valid' );
        },
        success: function (data) {
            if ( data === 'true' ) {
                input_apikey.removeClass ( 'is-invalid' ).addClass ( 'is-valid' );
                $ ( 'div#alert-apikey-invalid' ).hide ();
                $ ( 'div#alert-apikey-valid' ).show ();
                $ ( 'button#test-connection' ).show ();
                $ ( 'button#retry-connection' ).hide ();
                $ ( 'button#cio-select-next' ).show ();
            } else {
                input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
                $ ( 'div#alert-apikey-valid' ).hide ();
                $ ( 'div#alert-apikey-invalid' ).show ();
                $ ( 'button#test-connection' ).hide ();
                $ ( 'button#retry-connection' ).show ();
            }
        },
        error: function (data) {
            input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
            $ ( 'div#alert-apikey-valid' ).hide ();
            $ ( 'div#alert-apikey-invalid' ).show ();
            $ ( 'button#updathbapikey' ).attr ( 'disabled', true );
        }
    };

    // ajax call to verify apikey
    $.ajax ( settings_verify_apikey )
        .done ( function () {
            console.log ( 'Validate APIKey Completed' );
        } );
}

//////////////////////////////////////////// End of APIKey Validation ////////////////////////////////////////


// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;
//$BTN.on ( "click", () => {
$BTN.click ( function () {

    const $rows = $tableID.find ( 'tr:not(:hidden)' );
    const headers = [];
    const data = [];

    // Get the headers (add special header logic here)
    $ ( $rows.shift () ).find ( 'th:not(:empty)' ).each ( function () {
        headers.push ( $ ( this ).text ().replace ( /\s+/g, '-' ).toLowerCase () );
    } );

    // Turn all existing rows into a loopable array
    $rows.each ( function () {
        const $td = $ ( this ).find ( 'td' );
        const h = {};
        console.log ( 'headers', headers );
        // Use the headers from earlier to name our hash keys
        headers.forEach ( (header, i) => {
            i = 1;
            switch (header) {
                case 'marketing-in-one-custom-field':
                    // Fetch the Standard Fields
                    let CIOText = $ ( '.CIO', this ).find ( 'option:selected' ).text ();
                    let CIOValue = $ ( '.CIO', this ).find ( 'option:selected' ).val ();
                    h['mio-custom-field'] = CIOText;
                    // Fetch the Custom Fields
                    if ( CIOText.length === 0 ) {
                        let CIOCustomText = $ ( this ).find ( 'td.mio_customfield_frm_db' ).text ();
                        let valSplit = CIOCustomText.split ( '.' );
                        if ( valSplit.length >= 2 && valSplit[1].length !== 0 ) {
                            CIOValue = 'Contact_' + valSplit[1];
                        }
                        h['mio-custom-field'] = CIOCustomText;
                    }
                    h['mio-custom-field-id'] = CIOValue;
                    break;
                case "husbpot-custom-field":
                    let hb_custom_field = $ ( ".hb-select", this ).find ( 'option:selected' ).text ();
                    if ( hb_custom_field.length === 0 ) {
                        hb_custom_field = $ ( this ).find ( 'td.hb_customfield_frm_db' ).text ();
                    }
                    h['hb-custom-field'] = hb_custom_field;
                    break;
            }
            if ( h[header] === '' ) {
                h[header] = $td.eq ( i ).text ();
            }
        } );

        data.push ( h );
    } );
    value = JSON.stringify ( data );
    encryptedValue = btoa ( value );
    $ ( '#hb-mapping' ).attr ( 'value', encryptedValue );
    $ ( '#second' ).prop ( 'disabled', true );
    $ ( '#submit' ).click ();
    // Output the result
    $EXPORT.text ( JSON.stringify ( data ) );
} );


//////////////////////////////////////////// Start of eMIO CustomFields Mapping ////////////////////////////////////////


function helperFunctionForSelectField(fieldList, selectedField) {
    let innerHTML = ""
    let select = ''
    $.each ( fieldList, function (key, value) {
        if ( value['fieldname'].trim () === '' ) {
            return;
        }
        let label = value['fieldname'];
        let id = value['id'];
        if ( label === selectedField ) {
            select = `<option value="` + id + `" selected>` + label + `</option>`;
        } else {
            select = `<option value="` + id + `">` + label + `</option>`;
        }
        innerHTML += select;
    } );
    let selectInnerHTML = ` <select  class="cio-select form-control form-control-md" style="opacity:1">
                          ` + innerHTML + `
                        </select> `;
    return selectInnerHTML;
}

if ( hb_customfields_mapping_from_db ) {
    mapping_from_db = JSON.parse ( atob ( hb_customfields_mapping_from_db ) );
    $.each ( mapping_from_db, function (index, value) {
        let hb_customfield = value['hb-custom-field'];
        let mio_customfield = value['mio-custom-field'];
        const newTrTest = `
        <tr class="">
            <td class="pt-3-half mio_customfield_frm_db" contenteditable="true">` + mio_customfield + `</td>
            <td class="pt-3-half hb_customfield_frm_db" contenteditable="true">` + hb_customfield + `</td>
            <td><span class="table-remove">
                <i style="color:#ff0000;cursor: pointer;" class="fas fa-minus-circle"></i></span>
            </td>
        </tr>

        `;
        //appending html to
        $ ( 'tbody' ).append ( newTrTest );
    } );
}

function getMIOSelectedOption() {
    let mio_selected_customfield;
    $ ( 'tbody > tr > td.CIO > select.cio-select' ).change ( function (e) {
        mio_selected_customfield = $ ( "option:selected", this )[0].innerHTML;
        return mio_selected_customfield;
    } );
    return '';
}

function geteMIOSelectedOption() {
    $ ( 'tbody > tr > td.hb-custom-fields > select' ).change ( function (e) {
        hb_selected_customfield = $ ( "option:selected", this )[0].innerHTML;
        return hb_selected_customfield;
    } );
    return '';
}

let tmp = '';
// make a copy of params used for appending the standard field.
let campaignParamsBackup = Object.assign ( tmp, mio_customfields );

$ ( '.table-add' ).on ( 'click', 'a', () => {
    let newTr = generateHTML ();
    $ ( 'tbody' ).append ( newTr );
} );

$tableID.on ( 'click', '.table-remove', function () {
    let tr = ( $ ( this ).parents ( 'tr' ) );
    let cio_select_value = tr.find ( 'td.CIO >select.cio-select option:selected' ).text ();
    // Add standard field to global campaignInOneParams  for latter usage.
    mio_customfields[cio_select_value] = campaignParamsBackup[cio_select_value];
    $ ( this ).parents ( 'tr' ).detach ();
} );

$tableID.on ( 'click', '.table-up', function () {
    const $row = $ ( this ).parents ( 'tr' );
    if ( $row.index () === 1 ) {
        return;
    }
    $row.prev ().before ( $row.get ( 0 ) );
} );

$tableID.on ( 'click', '.table-down', function () {
    const $row = $ ( this ).parents ( 'tr' );
    $row.next ().after ( $row.get ( 0 ) );
} );


//This is to Generate html for select  to mapping table
function generateSelect(array) {
    let innerHTML = "";
    //get mapping array from database but for now hard-coded
    let json_parsed = $.parseJSON ( array );
    $.each ( json_parsed, function (key, value) {
        if ( value.fieldname.trim () === '' ) {
            return; // IGNORE EMPTY CUSTOMFIELDS NAMES
        }
        let label = value.fieldname;
        let id = value.id;
        let select = `<option value="` + id + `">` + label + `</option>`;
        innerHTML += select;
    } );
    let HTML = ` <select  class="cio-select form-control form-control-md" style="opacity:1">
                          ` + innerHTML + `
                        </select> `;
    return HTML;
}

function getTableLength() {
    return $ ( '.table tr' ).length;
}


// Generates table row for each pressed '+' sign.
function generateHTML() {
    // remove the selected option from CIO dropdown list
    removeCIOSelectedOptions ();

    let i = getTableLength ();
    let mioCustomFields = generateSelect ( mio_customfields );
    let hbCustomFields = generateSelectForHBCustomFields ( hb_customfields );
    return `
          <tr class="defaultTableValue" id="` + i + `">
              <td class="pt-3-half select2 CIO">` + mioCustomFields + `</td>
              <td class="pt-3-half select2 hb-custom-fields">` + hbCustomFields + `</td>
              <td valign="middle">
                  <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
              </td>
          </tr>`;
}

/**
 *  remove the selected fields from cio select options
 */
function removeCIOSelectedOptions() {
    const rows = $tableID.find ( 'tr:not(:hidden) >td >select.cio-select option:selected' );
    let i = 0;
    // Currently remove the used standard param from the list.
    $.each ( rows, function (key, value) {
        delete mio_customfields[value.value];
    } );
}

/**
 *  This functions generate HTML for datatypes
 */
function generateSelectForHBCustomFields(array) {
    let innerHTML = '';
    innerHTML += `
                  <optgroup label="To create customfield in Hubspot">
                    <option value='hb_autocreate_customfield' selected>Auto-Create</option>
                  </optgroup>`;
    innerHTML += `<optgroup label="Current list of cusstomfields from Hubspot">`;
    if ( array['content'].length > 0 ) {
        $.each ( array['content'], function (key, value) {
            let select = `<option value="` + value['name'] + `">` + value['label'] + `</option>`;
            innerHTML += select;
        } );
    }
    innerHTML += `</optgroup>`;
    return ` <select  class="form-control form-control-md hb-select" style="opacity:1">
                         ` + innerHTML + `
              </select> `;
}
