$('div#alert-apikey-valid').hide()
$('div#alert-apikey-invalid').hide()
$('button#retry-connection').hide()
$('button#cio-select-next').hide()

// Todo :
// prefill the existing mapping to table.
const $tableID = $('#table')
const $BTN = $('#finalSubmit')
const $EXPORT = $('#export')

// Const Used to parse the array.
const STANDARD_FIELDS = 'standard_fields'
const customFields = 'custom_fields'
const ECOMMERCE_FIELDS = 'ecommerce_fields'
const TECHNICAL_FIELDS = 'technical_fields'

// campaignInOneParams has to be MIO customfields from the backend.
let mio_customfields = $tableID.data('isTest')
let mio_standard_fields = $tableID.data('isMio')

//console.log ( 'MIOStandardFields', mio_standard_fields );
let emio_customfields_mapping_from_db = $tableID.data('mapping-from-db')
let emio_standard_fields = ''
let mio_fields = ''
let mio_dtypes = ''
/////////////////////////////////////////// Start of APIKey Validation & CustomFields /////////////////////////////////
// Verify eMIO apikey on Change
let mio_standard_fields_del_list = []

ajaxRequestToGetStandardFields()

/**
 * ajax call to emio microserver to get list of customfields
 * @param apikey_value
 */
function ajaxRequestToGetStandardFields () {
    let settings_get_emio_standard_fields = {
        url: msMio + '/standardfields/get',
        method: 'POST',
        async: 'true',
        timeout: 0,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        success: function (data) {
            emio_standard_fields = data
        },
        error: function (data) {
            console.log ( 'error', data );
        }
    };

    // ajax call to emio StandardFields
    $.ajax ( settings_get_emio_standard_fields )
        .done ( function () {
            console.log ( 'Successfully fetched eMIO StandardFields', emio_standard_fields );
        } );
}

function getMIOCustomFieldsFromMapping() {
    let mioCustomfields = [];
    if (emio_customfields_mapping_from_db) {
        mapping_from_db = JSON.parse(atob(emio_customfields_mapping_from_db))
        $.each(mapping_from_db, function (db_index, db_value) {
            mioCustomfields.push(db_value['marketing-in-one-custom-field'])
        })
    }
    return mioCustomfields
}

//////////////////////////////////////////// Start of eMIO CustomFields Mapping ////////////////////////////////////////

/**
 * Gets the MIO fields from table data parameter.
 * @returns {string}
 */
function getMIOFields () {
    if (mio_fields.length === 0) {
        let mio_fields_from_table_data = $tableID.data('mio-fields')
        console.log(mio_fields_from_table_data)
        mio_fields = $.parseJSON(atob(mio_fields_from_table_data))
    }
    return mio_fields
}

/**
 * Gets MIO Datatypes from table data parameter.
 * @returns {*}
 */
function getMIODTypes () {
    if (mio_dtypes.length === 0) {
        let mio_dtypes_from_table_data = $tableID.data('mio-dtypes')
        console.log(mio_dtypes_from_table_data)
        mio_dtypes = $.parseJSON(atob(mio_dtypes_from_table_data))
    }
    return mio_dtypes
}

let tmp = ''
// make a copy of params used for appending the standard field.
let campaignParamsBackup = Object.assign(tmp, mio_customfields)

$('.table-add').on('click', 'a', () => {
    let newTr = generateTableRow()
    $('tbody').append(newTr)
    
    // Update the datatype for MIO automatically.
    $ ( 'select#sel-mio-sdfield' ).change ( function () {
        let sdfldVal = $ ( this ).val ();
        let sdfld_dtype = $ ( this ).find ( ':selected' ).data ( 'dtype' );
        let mio_dtype = $ ( this ).closest ( 'tr' ).find ( 'td.mio-dtype > select.sel-mio-dtype' )
        mio_dtype.val ( sdfld_dtype ).change ().prop ( 'selected', true );
    } );
    
    // Update the datatype of EMIO automatically
    $ ( 'select.sel-emio-sdfield' ).change ( function () {
        let sdfldVal = $ ( this ).val ();
        let sdfld_dtype = $ ( this ).find ( ':selected' ).data ( 'dtype' );
        let parents = $ ( this ).parents ();
        let emio_dtype = $ ( this ).closest ( 'tr' ).find ( 'td.emio-dtype > select.sel-emio-dtype' );
        emio_dtype.val ( sdfld_dtype ).change ().prop ( 'selected', true );
    } );
} );

$tableID.on ( 'click', '.table-remove', function () {
    let tr = ( $ ( this ).parents ( 'tr' ) );
    /*    let cio_select_value = tr.find ( 'td.CIO >select.cio-select option:selected' ).text ();
        // Add standard field to global campaignInOneParams  for latter usage.
        mio_customfields[cio_select_value] = campaignParamsBackup[cio_select_value];*/
    let field_id = $ ( this ).closest ( 'tr' ).find ( 'td.mio-sdfield > select#sel-mio-sdfield' ).val ()
    mio_standard_fields_del_list.push ( field_id );
    $ ( this ).parents ( 'tr' ).detach ();
} );

$tableID.on ( 'click', '.table-up', function () {
    const $row = $ ( this ).parents ( 'tr' );
    if ( $row.index () === 1 ) {
        return;
    }
    $row.prev ().before ( $row.get ( 0 ) );
} );

$tableID.on ( 'click', '.table-down', function () {
    const $row = $ ( this ).parents ( 'tr' );
    $row.next ().after ( $row.get ( 0 ) );
} );


// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.on ( 'click', () => {

    const $rows = $tableID.find ( 'tr:not(:hidden)' );
    const headers = [];
    const data = [];
    const insert = [];

    // Get the headers (add special header logic here)
    $ ( $rows.shift () ).find ( 'th:not(:empty)' ).each ( function () {
        headers.push ( $ ( this ).text ().replace ( /\s+/g, '-' ).toLowerCase () );
    } );

    // Turn all existing rows into a loopable array
    $rows.each ( function () {
        const $td = $ ( this ).find ( 'td' );
        const h = {};
        console.log ( 'headers', headers );
        // Use the headers from earlier to name our hash keys
        headers.forEach ( (header, i) => {
            i = 1;
            switch (header) {
                case 'mio-field':
                    // fill the name of the mio field
                    h['miostandardfield_id'] = $.trim ( $ ( '.mio-sdfield', this ).find ( 'option:selected' ).val () );
                    break;
                case 'mio-datatype':
                    h['mio_datatype'] = $.trim ( $ ( '.mio-dtype', this ).find ( 'option:selected' ).text () );
                    break;
                case 'emio-field':
                    h['name'] = $.trim ( $ ( '.emio-sdfield', this ).find ( 'option:selected' ).text () );
                    break;
                case 'emio-datatype':
                    h['datatype'] = $.trim ( $ ( '.emio-dtype', this ).find ( 'option:selected' ).text () );
                    break;
            }
            if ( h[header] === '' ) {
                h[header] = $td.eq ( i ).text ();
            }
        } );
        insert.push ( h )
    } );

    value = JSON.stringify ( {'insert': insert, 'delete': mio_standard_fields_del_list} );
    encryptedValue = btoa ( value );
    $ ( '#emio-mapping' ).attr ( 'value', encryptedValue );
    $ ( '#second' ).prop ( 'disabled', true );
    $ ( '#submit' ).click ();
    $EXPORT.text ( JSON.stringify ( data ) );
} );

//This is to Generate html for select  to mapping table
function getTableCellForMIOColumn () {
    let innerHTML = ''
    
    let fields = getMIOFields()
    console.log('MIOFields ', fields)
    
    $.each(fields, function (categoryName, categoryValues) {
        //console.log('CategoryName', categoryName)
        let optGroup = categoryName
        let optGroupHtml = ''
        let select = ''
        $.each(categoryValues, function (key, value) {
            let option = ''
            let id = $.trim(value.id)
            console.log('Option id', id)
            let fieldName = value.fieldname
            let htmlLabel = value.htmllabel
            let fieldBelongsTo = value.fieldtype
            let datatypeId = value.datatypes_id
            let t = isMIOFieldSelected(fieldName)
            console.log('isMIOFieldSelected', t)
            if (isMIOFieldSelected(id)) {
                option = `<option
                            value="` + id + `"
                            data-category="` + fieldBelongsTo + `"
                            data-field="` + fieldName + `"
                            data-dtype="` + datatypeId + `"
                            data-selected = true
                            disabled
                            >` + htmlLabel + `</option>`
            } else {
                option = `<option
                            value="` + id + `"
                            data-category="` + fieldBelongsTo + `"
                            data-field="` + fieldName + `"
                            data-dtype="` + datatypeId + `"
                            data-selected = false
                            >` + htmlLabel + `</option>`
            }
            select += option
            //console.log('select', select)
        })
        if (optGroup === ECOMMERCE_FIELDS) {
            optGroupHtml = `<optgroup label="eCommerce Fields">
                ` + select + `
            </optgroup>`
        } else if (optGroup === STANDARD_FIELDS) {
            optGroupHtml = `<optgroup label="Standard Fields">
                ` + select + `
            </optgroup>`
        } else if (optGroup === TECHNICAL_FIELDS) {
            optGroupHtml = `<optgroup label="Technical Fields">
                ` + select + `
            </optgroup>`
        }
        //console.log ('optGroupHtml', optGroupHtml)
        innerHTML += `<option value="" data-selected = false disabled selected>Select MIOField</option>`
        innerHTML += optGroupHtml
    })
    
    let htmlSelect = `<select class="form-control form-control-md sel-mio-sdfield" id="sel-mio-sdfield" style="opacity:1">` + innerHTML + `</select> `
    console.log('htmlSelect', htmlSelect)
    
    return htmlSelect
}


//This is to Generate html for select  to mapping table
function getTableCellForMIODatatype () {
    let innerHTML = 'Text'
    let dTypes = getMIODTypes()
    
    $.each(dTypes, function (key, value) {
        let label = value.name
        let id = value.id
        let select = `<option value="` + id + `" data-name="` + label + `">` + label + `</option>`
        innerHTML += select
    })
    let HTML = ` <select  class="sel-mio-dtype form-control form-control-md" style="opacity:1" disabled>
                          ` + innerHTML + `
                        </select> `
    return HTML
}

//This is to Generate html for select  to mapping table
function getTableCellForEMIOColumn (array) {
    let innerHTML = ''
    console.log('getTableCellForEMIOColumn', array)
    //get mapping array from database but for now hard-coded
    $.each(array, function (key, value) {
        let label = key
        let dtype = value.dataType
        let tooltip = value.comment
        let select = `<option value="` + label + `" title = "` + tooltip + `" data-dtype="` + dtype + `">` + label + `</option>`
        innerHTML += select
    })
    let HTML = ` <select  class="sel-emio-sdfield form-control form-control-md" style="opacity:1">
                    <optgroup label="CustomField">
                        <option value="Auto-Create" title = "Automatically create a custom field" data-dtype="string">Auto-Create</option>
                    </optgroup>
                    <optgroup label="Mail-In-One StandardFields">` + innerHTML + `</optgroup>    
                  </select> `;
    return HTML;
}

function getTableCellForEMIODatatype (array) {
    let innerHTML = ''
    //get mapping array from database but for now hard-coded
    $.each(array, function (key, value) {
        let label = value.dataType
        let select = `<option value="` + label + `">` + label + `</option>`
        innerHTML += select
    })
    let HTML = ` <select  class="sel-emio-dtype form-control form-control-md" style="opacity:1" disabled>
                          ` + innerHTML + `
                        </select> `
    return HTML;
}

function getTableLength() {
    return $ ( '.table tr' ).length;
}


// Generates table row for each pressed '+' sign.
function generateTableRow () {
    // remove the selected option from CIO dropdown list
    // removeCIOSelectedOptions ();
    
    let i = getTableLength()
    
    // 1st Table cell
    let mio_sel_sdfields = getTableCellForMIOColumn()
    // 2nd Table cell
    let mio_sel_sdfields_dtype = getTableCellForMIODatatype()
    // 3rd Table Cell
    let emio_sel_sdfields = getTableCellForEMIOColumn(emio_standard_fields)
    // 4th Table Cell.
    let emio_sel_sdfields_dtype = getTableCellForEMIODatatype(emio_standard_fields)
    
    return `
          <tr class="defaultTableValue" id="` + i + `">
              <td class="mio-sdfield">` + mio_sel_sdfields + `</td>
              <td class="mio-dtype">` + mio_sel_sdfields_dtype + `</td>
              <td class="emio-sdfield">` + emio_sel_sdfields + `</td>
              <td class="emio-dtype">` + emio_sel_sdfields_dtype + `</td>
              <td>
                    <span class="table-remove"><i style="color:#ff0000;cursor: pointer;padding-top:15px" class="fas fa-minus-circle"></i></span>
              </td>
          </tr>`
}

/**
 * Checks if the MIO field is already mapped / selected
 * @param fieldId
 * @returns {boolean}
 */
function isMIOFieldSelected (fieldId) {
    let isPresent = false
    let rows = $tableID.find('tr:not(:hidden) >td.mio-sdfield >select.sel-mio-sdfield option:selected')
    $.each(rows, function (key, value) {
        let selectedFieldId = $.trim(value.value)
        if ($.trim(fieldId) === selectedFieldId) {
            isPresent = true
            return false
        }
    })
    return isPresent
}

/**
 *  remove the selected fields from cio select options
 */
function removeCIOSelectedOptions() {
    const rows = $tableID.find ( 'tr:not(:hidden) >td >select.cio-select option:selected' );
    let i = 0;
    // Currently remove the used standard param from the list.
    $.each ( rows, function (key, value) {
        delete mio_customfields[value.value];
    } );
}

/**
 *  This functions generate HTML for datatypes
 */
function generateSelectForeMIOCustomFields(array) {
    let innerHTML = "";
    innerHTML += `
                  <optgroup label="To create customfield in eMIO">
                    <option value='emio_autocreate_customfield' selected>Auto-Create</option>
                  </optgroup>`;
    innerHTML += `<optgroup label="Current list of cusstomfields from eMIO">`;
    $.each ( array, function (key, value) {
        let select = `<option value="` + key + `">` + key + `</option>`;
        innerHTML += select;
    } );
    innerHTML += `</optgroup>`;
    return ` <select  class="form-control form-control-md emio-select" style="opacity:1">
                          ` + innerHTML + `
                        </select> `;
}
