$ ( 'div#alert-apikey-valid' ).hide ();
$ ( 'div#alert-apikey-invalid' ).hide ();
$ ( 'button#retry-connection' ).hide ();
//$ ( 'button#finalSubmit' ).hide();

/////////////////////////////////////////// Start of APIKey Validation & CustomFields /////////////////////////////////
let user_field = $ ( 'input[name="user-id"]' );
let pass_field = $ ( 'input[name="password"]' );
let endpoint = $ ( 'form#kt_form' ).data ( 'endpoint' );
// verify eMIO apikey on click TestConnection
$ ( 'button.test-apikey-connection' ).on ( 'click', function () {
    let user = user_field.val ();
    let password = pass_field.val ();
    ajaxValidateWebinarisAccount ( user, password );
} );


/**
 * Ajax Call to hb microserver to validate apikey.
 * @param apikey
 * @returns {boolean}
 */
function ajaxValidateWebinarisAccount(user, password) {
    // Check for number of length of APIKey before sending to validate
    if ( user.trim () === '' || password.trim () === '' ) {
        user_field.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
        $ ( 'div#alert-apikey-valid' ).hide ();
        $ ( 'div#alert-apikey-invalid' ).show ();
        $ ( 'button#test-connection' ).hide ();
        $ ( 'button#retry-connection' ).show ();
        $ ( 'button#finalSubmit' ).hide ();
        return false;
    }
    let payload = btoa ( JSON.stringify ( {'user': user, 'password': password} ) );
    var verify_account = {
        url: endpoint + '/getWebinarisDates',
        method: 'POST',
        timeout: 0,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: payload,
        beforeSend: function (xhr) {
            user_field.removeClass ( 'is-invalid' ).removeClass ( 'is-valid' );
        },
        success: function (data) {
            if ( data.status === true ) {
                user_field.removeClass ( 'is-invalid' ).addClass ( 'is-valid' );
                $ ( 'div#alert-apikey-invalid' ).hide ();
                $ ( 'div#alert-apikey-valid' ).show ();
                $ ( 'button#test-connection' ).show ();
                $ ( 'button#retry-connection' ).hide ();
                $ ( 'button#finalSubmit' ).show ();
            } else {
                user_field.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
                $ ( 'div#alert-apikey-valid' ).hide ();
                $ ( 'div#alert-apikey-invalid' ).show ();
                $ ( 'button#test-connection' ).hide ();
                $ ( 'button#retry-connection' ).show ();
            }
        },
        error: function (data) {
            user_field.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
            $ ( 'div#alert-apikey-valid' ).hide ();
            $ ( 'div#alert-apikey-invalid' ).show ();
            $ ( 'button#updathbapikey' ).attr ( 'disabled', true );
        }
    };

    // ajax call to verify apikey
    $.ajax ( verify_account )
        .done ( function () {
            console.log ( 'Validate APIKey Completed' );
        } );
}

//////////////////////////////////////////// End of APIKey Validation ////////////////////////////////////////

