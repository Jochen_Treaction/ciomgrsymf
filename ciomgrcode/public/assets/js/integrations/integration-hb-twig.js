$ ( 'div#alert-apikey-valid' ).hide ();
$ ( 'div#alert-apikey-invalid' ).hide ();
$ ( 'button#retry-connection' ).hide ();
$ ( 'button#cio-select-next' ).hide ();


// prefill the existing mapping to table.
const $tableID = $ ( '#table' );
const $BTN = $ ( "#finalSubmit" );
const $EXPORT = $ ( '#export' );
let endpoint = $ ( 'form#kt_form' ).data ( 'endpoint' );


/////////////////////////////////////////// Start of APIKey Validation & CustomFields /////////////////////////////////
// Verify eMIO apikey on Change
let is_valid_apikey = false;
let hb_customfields = [];
let input_apikey = $ ( 'input[name="apikey"]' );


// Any change in apikey triggers the click for test apikey button.
input_apikey.blur ( function () {
    $ ( 'button#cio-select-next' ).hide ();
} )

// verify eMIO apikey on click TestConnection
$ ( 'button.test-apikey-connection' ).on ( 'click', function () {
    let input_apikey_value = input_apikey.val ();
    ajaxRequestToValidateAPIKey ( input_apikey_value );
} );

/**
 * Ajax Call to hb microserver to validate apikey.
 * @param apikey
 * @returns {boolean}
 */
function ajaxRequestToValidateAPIKey(apikey) {
    let input_apikey_value = apikey;
    // Check for number of length of APIKey before sending to validate
    if ( input_apikey_value.length <= 5 ) {
        input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
        $ ( 'div#alert-apikey-valid' ).hide ();
        $ ( 'div#alert-apikey-invalid' ).show ();
        $ ( 'button#test-connection' ).hide ();
        $ ( 'button#retry-connection' ).show ();
        $ ( 'button#cio-select-next' ).hide ();
        return false;
    }

    var settings_verify_apikey = {
        url: endpoint + '/apikey/check',
        method: 'POST',
        timeout: 0,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: {
            apikey: input_apikey_value
        },
        beforeSend: function (xhr) {
            input_apikey.removeClass ( 'is-invalid' ).removeClass ( 'is-valid' );
        },
        success: function (data) {
            if ( data === 'true' ) {
                input_apikey.removeClass ( 'is-invalid' ).addClass ( 'is-valid' );
                $ ( 'div#alert-apikey-invalid' ).hide ();
                $ ( 'div#alert-apikey-valid' ).show ();
                $ ( 'button#test-connection' ).show ();
                $ ( 'button#retry-connection' ).hide ();
                $ ( 'button#cio-select-next' ).show ();
            } else {
                input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
                $ ( 'div#alert-apikey-valid' ).hide ();
                $ ( 'div#alert-apikey-invalid' ).show ();
                $ ( 'button#test-connection' ).hide ();
                $ ( 'button#retry-connection' ).show ();
            }
        },
        error: function (data) {
            input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
            $ ( 'div#alert-apikey-valid' ).hide ();
            $ ( 'div#alert-apikey-invalid' ).show ();
            $ ( 'button#updathbapikey' ).attr ( 'disabled', true );
        }
    };

    // ajax call to verify apikey
    $.ajax ( settings_verify_apikey )
        .done ( function () {
            console.log ( 'Validate APIKey Completed' );
        } );
}

//////////////////////////////////////////// End of APIKey Validation ////////////////////////////////////////


