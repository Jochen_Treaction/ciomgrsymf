$ ( 'div#alert-apikey-valid' ).hide ();
$ ( 'div#alert-apikey-invalid' ).hide ();
$ ( 'button#retry-connection' ).hide ();
$ ( 'button#finalSubmit' ).hide ();

/////////////////////////////////////////// Start of APIKey Validation & CustomFields /////////////////////////////////
// Verify eMIO apikey on Change
let input_apikey = $ ( 'input[name="apikey"]' );
let endpoint = $ ( 'form#kt_form' ).data ( 'endpoint' );

// Any change in apikey triggers the click for test apikey button.
input_apikey.blur ( function () {
    $ ( 'button#cio-select-next' ).hide ();
} )

// verify eMIO apikey on click TestConnection
$ ( 'button.test-apikey-connection' ).on ( 'click', function () {
    let apikey = input_apikey.val ();
    ajaxRequestToValidateAPIKey ( apikey );
} );


/**
 * Ajax Call to hb microserver to validate apikey.
 * @param apikey
 * @returns {boolean}
 */
function ajaxRequestToValidateAPIKey(apikey) {
    // Check for number of length of APIKey before sending to validate
    if ( apikey.length <= 5 ) {
        input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
        $ ( 'div#alert-apikey-valid' ).hide ();
        $ ( 'div#alert-apikey-invalid' ).show ();
        $ ( 'button#test-connection' ).hide ();
        $ ( 'button#retry-connection' ).show ();
        $ ( 'button#finalSubmit' ).hide ();
        return false;
    }
    let payload = btoa ( JSON.stringify ( {'apikey': apikey} ) );
    var settings_verify_apikey = {
        url: endpoint + '/apikey/check',
        method: 'POST',
        timeout: 0,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: payload,
        beforeSend: function (xhr) {
            input_apikey.removeClass ( 'is-invalid' ).removeClass ( 'is-valid' );
        },
        success: function (data) {
            if ( data.status === true ) {
                input_apikey.removeClass ( 'is-invalid' ).addClass ( 'is-valid' );
                $ ( 'div#alert-apikey-invalid' ).hide ();
                $ ( 'div#alert-apikey-valid' ).show ();
                $ ( 'button#test-connection' ).show ();
                $ ( 'button#retry-connection' ).hide ();
                $ ( 'button#finalSubmit' ).show ();
            } else {
                input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
                $ ( 'div#alert-apikey-valid' ).hide ();
                $ ( 'div#alert-apikey-invalid' ).show ();
                $ ( 'button#test-connection' ).hide ();
                $ ( 'button#retry-connection' ).show ();
            }
        },
        error: function (data) {
            input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
            $ ( 'div#alert-apikey-valid' ).hide ();
            $ ( 'div#alert-apikey-invalid' ).show ();
            $ ( 'button#updathbapikey' ).attr ( 'disabled', true );
        }
    };

    // ajax call to verify apikey
    $.ajax ( settings_verify_apikey )
        .done ( function () {
            console.log ( 'Validate APIKey Completed' );
        } );
}

//////////////////////////////////////////// End of APIKey Validation ////////////////////////////////////////

