$ ( 'div#alert-apikey-valid' ).hide ();
$ ( 'div#alert-apikey-invalid' ).hide ();
$ ( 'button#retry-connection' ).hide ();
$ ( 'button#cio-select-next' ).hide ();


// prefill the existing mapping to table.
const $tableID = $ ( '#table' );
const $BTN = $ ( "#finalSubmit" );
const $EXPORT = $ ( '#export' );
// campaignInOneParams has to be MIO customfields from the backend.
let mio_customfields = $tableID.data ( "isTest" );
let mio_standard_fields = $tableID.data ( 'isMio' );
let end_point = $ ( 'form#kt_form' ).data ( 'endpoint' );
let apikey = $ ( 'form#kt_form' ).data ( 'apikey' );
let emio_customfields_mapping_from_db = $tableID.data ( "mapping-from-db" );


/////////////////////////////////////////// Start of APIKey Validation & CustomFields /////////////////////////////////
// Verify eMIO apikey on Change
let mio_standard_fields_del_list = [];
let input_apikey = $ ( 'input[name="apikey"]' );
let hb_standard_fields = ''; // HubSpot Standard Fields.

ajaxRequestToGetStandardFields ( input_apikey.val () );

/**
 * ajax call to emio microserver to get list of customfields
 * @param apikey_value
 */
function ajaxRequestToGetStandardFields(apikey_value) {
    let body = btoa ( '{"apikey":"' + apikey + '"}' );
    console.log ( body );
    let settings_get_emio_standard_fields = {
        url: end_point + "/contact/properties/get",
        method: "POST",
        async: "true",
        timeout: 0,
        data: body,
        headers: {
            'Content-Type': "application/x-www-form-urlencoded",
        },
        success: function (data) {
            hb_standard_fields = data;
        },
        error: function (data) {
            console.log ( 'error', data );
        }
    };

    // ajax call to emio StandardFields
    $.ajax ( settings_get_emio_standard_fields )
        .done ( function () {
            console.log ( 'Successfully fetched eMIO StandardFields', hb_standard_fields );
        } );
}

console.log ( 'HB Properties', hb_standard_fields )

function getMIOCustomFieldsFromMapping() {
    let mioCustomfields = [];
    if ( emio_customfields_mapping_from_db ) {
        mapping_from_db = JSON.parse ( atob ( emio_customfields_mapping_from_db ) );
        $.each ( mapping_from_db, function (db_index, db_value) {
            mioCustomfields.push ( db_value['marketing-in-one-custom-field'] );
        } );
    }
    return mioCustomfields;
}

//////////////////////////////////////////// Start of eMIO CustomFields Mapping ////////////////////////////////////////

let tmp = '';
// make a copy of params used for appending the standard field.
let campaignParamsBackup = Object.assign ( tmp, mio_customfields );

$ ( '.table-add' ).on ( 'click', 'a', () => {
    let newTr = generateHTML ();
    $ ( 'tbody' ).append ( newTr );

    $ ( 'select#sel-mio-sdfield' ).change ( function () {
        let sdfldVal = $ ( this ).val ();
        let sdfld_dtype = $ ( this ).find ( ':selected' ).data ( 'dtype' );
        let mio_dtype = $ ( this ).closest ( 'tr' ).find ( 'td.mio-dtype > select.sel-mio-dtype' )
        mio_dtype.val ( sdfld_dtype ).change ().prop ( 'selected', true );
    } );

    $ ( 'select.sel-hb-sdfield' ).change ( function () {
        let sdfldVal = $ ( this ).val ();
        let sdfld_dtype = $ ( this ).find ( ':selected' ).data ( 'dtype' );
        let parents = $ ( this ).parents ();
        let hb_dtype = $ ( this ).closest ( 'tr' ).find ( 'td.hb-dtype > select.sel-hb-dtype' );
        hb_dtype.val ( sdfld_dtype ).change ().prop ( 'selected', true );
    } );
} );


$tableID.on ( 'click', '.table-remove', function () {
    let tr = ( $ ( this ).parents ( 'tr' ) );
    /*    let cio_select_value = tr.find ( 'td.CIO >select.cio-select option:selected' ).text ();
        // Add standard field to global campaignInOneParams  for latter usage.
        mio_customfields[cio_select_value] = campaignParamsBackup[cio_select_value];*/
    let field_id = $ ( this ).closest ( 'tr' ).find ( 'td.mio-sdfield > select#sel-mio-sdfield' ).val ()
    mio_standard_fields_del_list.push ( field_id );
    $ ( this ).parents ( 'tr' ).detach ();
} );

$tableID.on ( 'click', '.table-up', function () {
    const $row = $ ( this ).parents ( 'tr' );
    if ( $row.index () === 1 ) {
        return;
    }
    $row.prev ().before ( $row.get ( 0 ) );
} );

$tableID.on ( 'click', '.table-down', function () {
    const $row = $ ( this ).parents ( 'tr' );
    $row.next ().after ( $row.get ( 0 ) );
} );


// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.on ( 'click', () => {

    const $rows = $tableID.find ( 'tr:not(:hidden)' );
    const headers = [];
    const data = [];
    const insert = [];

    // Get the headers (add special header logic here)
    $ ( $rows.shift () ).find ( 'th:not(:empty)' ).each ( function () {
        headers.push ( $ ( this ).text ().replace ( /\s+/g, '-' ).toLowerCase () );
    } );

    // Turn all existing rows into a loopable array
    $rows.each ( function () {
        const $td = $ ( this ).find ( 'td' );
        const h = {};
        console.log ( 'headers', headers );
        // Use the headers from earlier to name our hash keys
        headers.forEach ( (header, i) => {
            i = 1;
            switch (header) {
                case 'mio-field':
                    // fill the name of the mio field
                    h['miostandardfield_id'] = $.trim ( $ ( '.mio-sdfield', this ).find ( 'option:selected' ).val () );
                    break;
                case 'mio-datatype':
                    h['mio_datatype'] = $.trim ( $ ( '.mio-dtype', this ).find ( 'option:selected' ).text () );
                    break;
                case 'hb-field':
                    h['name'] = $.trim ( $ ( '.hb-sdfield', this ).find ( 'option:selected' ).text () );
                    break;
                case 'hb-datatype':
                    h['datatype'] = $.trim ( $ ( '.hb-dtype', this ).find ( 'option:selected' ).text () );
                    break;
            }
            if ( h[header] === '' ) {
                h[header] = $td.eq ( i ).text ();
            }
        } );
        insert.push ( h )
    } );

    value = JSON.stringify ( {'insert': insert, 'delete': mio_standard_fields_del_list} );
    encryptedValue = btoa ( value );
    $ ( '#hb-mapping' ).attr ( 'value', encryptedValue );
    $ ( '#second' ).prop ( 'disabled', true );
    $ ( '#submit' ).click ();
    $EXPORT.text ( JSON.stringify ( data ) );
} );

//This is to Generate html for select  to mapping table
function generateMIOSdFieldSelect(array) {
    let innerHTML = "";
    //get mapping array from database but for now hard-coded
    let json_parsed = $.parseJSON ( array );
    $.each ( json_parsed, function (key, value) {
        let label = value.htmllabel;
        let id = value.id;
        let field = value.fieldname
        let dtype = value.datatypes_id;
        let select = `<option value="` + id + `" data-field="` + field + `" data-dtype="` + dtype + `">` + label + `</option>`;
        innerHTML += select;
    } );
    let HTML = ` <select  class="form-control form-control-md" id="sel-mio-sdfield" style="opacity:1">
                          ` + innerHTML + `
                        </select> `;
    return HTML;
}


//This is to Generate html for select  to mapping table
function generateMIODTypeSelect(array) {
    let innerHTML = "";
    //get mapping array from database but for now hard-coded
    let json_parsed = $.parseJSON ( array );
    $.each ( json_parsed, function (key, value) {
        let label = value.datatype;
        let id = value.datatypes_id;
        let field_id = value.id;
        let select = `<option value="` + id + `" data-field="` + field_id + `">` + label + `</option>`;
        innerHTML += select;
    } );
    let HTML = ` <select  class="sel-mio-dtype form-control form-control-md" style="opacity:1" disabled>
                          ` + innerHTML + `
                        </select> `;
    return HTML;
}

//This is to Generate html for select  to mapping table
function generateHBSdFieldSelect(array) {
    let innerHTML = "";
    //get mapping array from database but for now hard-coded
    $.each ( array['content'], function (key, value) {
        let label = value.name;
        let dtype = value.type;
        let tooltip = value.description;
        let select = `<option value="` + label + `" title = "` + tooltip + `" data-dtype="` + dtype + `">` + label + `</option>`;
        innerHTML += select;
    } );

    let HTML = ` <select  class="sel-hb-sdfield form-control form-control-md" style="opacity:1">
                    <optgroup label="CustomField">
                        <option value="Auto-Create" title = "Automatically create a custom field" data-dtype="string">Auto-Create</option>
                    </optgroup>
                    <optgroup label="HubSpot StandardFields">` + innerHTML + `</optgroup>    
                  </select> `;
    return HTML;
}

function generateHBDTypeSelect(array) {
    let innerHTML = "";
    //get mapping array from database but for now hard-coded
    $.each ( array['content'], function (key, value) {
        let label = value.type;
        let select = `<option value="` + label + `">` + label + `</option>`;
        innerHTML += select;
    } );
    let HTML = ` <select  class="sel-hb-dtype form-control form-control-md" style="opacity:1" disabled>
                          ` + innerHTML + `
                        </select> `;
    return HTML;
}

function getTableLength() {
    return $ ( '.table tr' ).length;
}


// Generates table row for each pressed '+' sign.
function generateHTML() {
    // remove the selected option from CIO dropdown list
    // removeCIOSelectedOptions ();
    let i = getTableLength ();

    let mio_sel_sdfields = generateMIOSdFieldSelect ( mio_standard_fields );
    let mio_sel_sdfields_dtype = generateMIODTypeSelect ( mio_standard_fields );
    let hb_sel_sdfields = generateHBSdFieldSelect ( hb_standard_fields );
    let hb_sel_sdfields_dtype = generateHBDTypeSelect ( hb_standard_fields );
    return `
          <tr class="defaultTableValue" id="` + i + `">
              <td class="mio-sdfield">` + mio_sel_sdfields + `</td>
              <td class="mio-dtype">` + mio_sel_sdfields_dtype + `</td>
              <td class="hb-sdfield">` + hb_sel_sdfields + `</td>
              <td class="hb-dtype">` + hb_sel_sdfields_dtype + `</td>
              <td>
                    <span class="table-remove"><i style="color:#ff0000;cursor: pointer;padding-top:15px" class="fas fa-minus-circle"></i></span>
              </td>
          </tr>`;
}


/**
 *  remove the selected fields from cio select options
 */
function removeCIOSelectedOptions() {
    const rows = $tableID.find ( 'tr:not(:hidden) >td >select.cio-select option:selected' );
    let i = 0;
    // Currently remove the used standard param from the list.
    $.each ( rows, function (key, value) {
        delete mio_customfields[value.value];
    } );
}

/**
 *  This functions generate HTML for datatypes
 */
function generateSelectForeMIOCustomFields(array) {
    let innerHTML = "";
    innerHTML += `
                  <optgroup label="To create customfield in eMIO">
                    <option value='emio_autocreate_customfield' selected>Auto-Create</option>
                  </optgroup>`;
    innerHTML += `<optgroup label="Current list of cusstomfields from eMIO">`;
    $.each ( array, function (key, value) {
        let select = `<option value="` + key + `">` + key + `</option>`;
        innerHTML += select;
    } );
    innerHTML += `</optgroup>`;
    return ` <select  class="form-control form-control-md emio-select" style="opacity:1">
                          ` + innerHTML + `
                        </select> `;
}
