$ ( 'div#alert-apikey-valid' ).hide ();
$ ( 'div#alert-apikey-invalid' ).hide ();
$ ( 'button#retry-connection' ).hide ();
$ ( 'button#cio-select-next' ).hide ();


// prefill the existing mapping to table.
const $tableID = $ ( '#table' );
const $BTN = $ ( "#finalSubmit" );
const $EXPORT = $('#export')
let endpoint = $('form#kt_form').data('endpoint')
let doi_mailing_from_db = $('select#emio-mailing').data('mailing')
let blacklisting_from_db = $('select#blacklist').data('blacklist')

// campaignInOneParams has to be MIO customfields from the backend.
let mio_customfields = $tableID.data('isTest')
let emio_customfields_mapping_from_db = $tableID.data('mapping-from-db')
let mio_std_cus_fields = $tableID.data('sd-cus-fields')

console.log('mio_std_cus_fields', mio_std_cus_fields)

//console.log ("emio_customfields_mapping_from_db", emio_customfields_mapping_from_db);

/////////////////////////////////////////// Start of APIKey Validation & CustomFields /////////////////////////////////
// Verify eMIO apikey on Change
let is_valid_apikey = false
let emio_customfields = []
let input_apikey = $('input[name="apikey"]')

// Hide Page next button untill APIKey is validated and fetch all the configuration from msmio service.
$('button#cio-select-next').hide()
$('select#emio-mailing').hide() // HIDE THE MAILING LIST UNTIL IT IS LOADED.

function ajaxRequestToGetMailingList(apikey_value) {
    let settings_get_emio_mailing_list = {
        url: endpoint + '/getMailingsDoiKeys',
        method: 'GET',
        async: 'true',
        timeout: 0,
        headers: {
            'Content-Type': "application/x-www-form-urlencoded",
        },
        data: {
            apikey: apikey_value
        },
        success: function (data) {
            console.log ( 'Sussess to get Mailing List', data );
            emio_mailing_list = data;
        },
        error: function (data) {
            console.log ( 'error', data );
        }
    };

    $.ajax ( settings_get_emio_mailing_list )
        .done ( function () {
            let innerHTML = ''
            if ( $.trim ( doi_mailing_from_db ) === '' ) {
                innerHTML += `<option value="0" selected>Not Defined</option>`;
            } else {
                innerHTML += `<option value="0">Not Defined</option>`;
            }
            if ( $.trim ( emio_mailing_list.doiKeyList ) !== '' ) {
                $.each ( emio_mailing_list.doiKeyList, function (i, e) {
                    let doikey = e.doiKey
                    let doi_mailing_name = e.name
                    if ( $.trim ( doi_mailing_from_db ) !== '' && doi_mailing_from_db === doikey ) {
                        innerHTML += `<option value="` + doikey + `" selected>` + doi_mailing_name + `</option>`;
                    } else {
                        innerHTML += `<option value="` + doikey + `">` + doi_mailing_name + `</option>`;
                    }
                } );
            }
            $ ( 'div#doi-mailing-key-loading' ).hide ();
            $ ( 'select#emio-mailing' ).append ( innerHTML ).show ();
        } );

}

function ajaxRequestToGetBlacklistList(apikey_value) {
    let settings_get_emio_blacklist_list = {
        url: endpoint + '/blacklist/get',
        method: 'GET',
        async: 'true',
        timeout: 0,
        headers: {
            'Content-Type': "application/x-www-form-urlencoded",
        },
        data: {
            apikey: apikey_value
        },
        success: function (data) {
            console.log ( 'Sussess to get Mailing List', data );
            emio_blacklist_list = data;
        },
        error: function (data) {
            console.log ( 'error', data );
        }
    };

    $.ajax ( settings_get_emio_blacklist_list )
        .done ( function () {
            let innerHTML = ''
            if ( $.trim ( blacklisting_from_db ) === '' ) {
                innerHTML += `<option value="0" selected>Not Defined</option>`;
            } else {
                innerHTML += `<option value="0">Not Defined</option>`;
            }
            $.each ( emio_blacklist_list, function (i, e) {
                let id = parseInt ( e.id );
                let name = e.name
                if ( $.trim ( blacklisting_from_db ) !== '' && blacklisting_from_db === id ) {
                    innerHTML += `<option value="` + id + `" selected>` + name + `</option>`;
                } else {
                    innerHTML += `<option value="` + id + `">` + name + `</option>`;
                }
            } );
            $ ( 'div#blacklist-key-loading' ).hide ();
            $ ( 'select#blacklist' ).append ( innerHTML ).show ();
        } );

}

// verify eMIO apikey on click TestConnection
$ ( "button.test-apikey-connection" ).on ( 'click', function () {

    let input_apikey_value = input_apikey.val ();
    ajaxRequestToValidateAPIKey ( input_apikey_value );
    ajaxRequestToGetCustomFields ( input_apikey_value );
    ajaxRequestToGetMailingList ( input_apikey_value );
    ajaxRequestToGetBlacklistList ( input_apikey_value );
    $ ( 'button#cio-select-next' ).show ();
} );

/**
 * ajax call to emio microserver to get list of customfields
 * @param apikey_value
 */
function ajaxRequestToGetCustomFields(apikey_value) {
    let settings_get_emio_customfields = {
        url: endpoint + '/customfields/get',
        method: 'POST',
        async: 'true',
        timeout: 0,
        headers: {
            'Content-Type': "application/x-www-form-urlencoded",
        },
        data: {
            apikey: apikey_value
        },
        success: function (data) {
            console.log ( 'Sussess to get Customfields', data );
            emio_customfields = data;
        },
        error: function (data) {
            console.log ( 'error', data );
        }
    };

    // ajax call to emio customfields
    $.ajax ( settings_get_emio_customfields )
        .done ( function () {
            // prepopulate the intial customfields
            miofromDb = getMIOCustomFieldsFromMapping()
            console.log('miofromDb', miofromDb)
            if ( mio_customfields ) {
                let mio_customfields_arr = $.parseJSON ( mio_customfields );
                console.log('mio_customfields_arr', mio_customfields_arr)
                $.each(mio_customfields_arr, function (index, value) {
                    let fieldname = value['fieldname']
                    if (!fieldname.trim()) {
                        return
                    }
                    if (jQuery.inArray(fieldname, miofromDb) === -1) {
                        let mioSelect = helperFunctionForSelectField(mio_customfields_arr, fieldname)
                        let emioCustomFields = generateSelectForeMIOCustomFields(emio_customfields)
                        let requiredHtml = `
                         <select class="form-control form-control-md fieldTypeSelect" style="opacity:1">
                            <option value="true">Yes</option>
                            <option value="false">No</option>
                        </select> `;
                        const newTrTest = `
                            <tr class="">
                                <td class="pt-3-half pt-3-half select2 CIO" contenteditable="true">` + mioSelect + `</td>
                                <td class="pt-3-half select2 emio-custom-fields" contenteditable="true">` + emioCustomFields + `</td>
                     
                                <td><span class="table-remove">
                                    <i style="color:#ff0000;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                </td>
                            </tr>
                            `;
                        //appending html to
                        $ ( 'tbody' ).append ( newTrTest );
                    }
                } );
            }

            console.log ( 'done' );
        } );
}

function getMIOCustomFieldsFromMapping() {
    let mioCustomfields = [];
    if ( emio_customfields_mapping_from_db ) {
        mapping_from_db = JSON.parse ( atob ( emio_customfields_mapping_from_db ) );
        $.each ( mapping_from_db, function (db_index, db_value) {
            mioCustomfields.push ( db_value['marketing-in-one-custom-field'] );
        } );
    }
    return mioCustomfields;
}

/**
 * Ajax Call to emio microserver to validate apikey.
 * @param apikey
 * @returns {boolean}
 */
function ajaxRequestToValidateAPIKey(apikey) {
    let input_apikey_value = apikey;
    // Check for number of length of APIKey before sending to validate
    if ( input_apikey_value.length <= 5 ) {
        input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
        $ ( 'div#alert-apikey-valid' ).hide ();
        $ ( 'div#alert-apikey-invalid' ).show ();
        $ ( 'button#test-connection' ).hide ();
        $ ( 'button#retry-connection' ).show ();
        $ ( 'button#cio-select-next' ).hide ();
        return false;
    }

    var settings_verify_apikey = {
        url: endpoint + "/can_i_post", // TODO: remove hardcode url
        method: "POST",
        timeout: 0,
        headers: {
            'Content-Type': "application/x-www-form-urlencoded",
        },
        data: {
            apikey: input_apikey_value
        },
        beforeSend: function (xhr) {
            input_apikey.removeClass ( 'is-invalid' ).removeClass ( 'is-valid' );
        },
        success: function (data) {
            if ( data.post === true ) {
                input_apikey.removeClass ( 'is-invalid' ).addClass ( 'is-valid' );
                $ ( 'div#alert-apikey-invalid' ).hide ();
                $ ( 'div#alert-apikey-valid' ).show ();
                $ ( 'button#test-connection' ).show ();
                $ ( 'button#retry-connection' ).hide ();
                $ ( 'button#cio-select-next' ).show ();
            } else {
                input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
                $ ( 'div#alert-apikey-valid' ).hide ();
                $ ( 'div#alert-apikey-invalid' ).show ();
                $ ( 'button#test-connection' ).hide ();
                $ ( 'button#retry-connection' ).show ();
            }
        },
        error: function (data) {
            input_apikey.removeClass ( 'is-valid' ).addClass ( 'is-invalid' );
            $ ( 'div#alert-apikey-valid' ).hide ();
            $ ( 'div#alert-apikey-invalid' ).show ();
            $ ( 'button#updatemioapikey' ).attr ( 'disabled', true );
        }
    };

    // ajax call to verify apikey
    $.ajax ( settings_verify_apikey )
        .done ( function () {
            console.log ( 'done' );
        } );
}

//////////////////////////////////////////// End of APIKey Validation ////////////////////////////////////////


//////////////////////////////////////////// Start of eMIO CustomFields Mapping ////////////////////////////////////////


function helperFunctionForSelectField(fieldList, selectedField) {
    let innerHTML = ''
    let select = ''
    console.log('fieldList', fieldList)
    console.log('selectedField', selectedField)
    $.each(fieldList, function (key, value) {
        let label = getHtmlLabelForField(value['fieldname'])
        if (!label.trim()) {
            return
        }
        let id = value['id']
        if (value['fieldname'] === selectedField) {
            select = `<option value="` + id + `" data-htmllabel="` + label + `" data-value="` + value['fieldname'] + `" selected>` + label + `</option>`
        } else {
            select = `<option value="` + id + `" data-htmllabel="` + label + `" data-value="` + value['fieldname'] + `" >` + label + `</option>`
        }
        innerHTML += select
    })
    let selectInnerHTML = ` <select  class="cio-select form-control form-control-md" style="opacity:1">
                          ` + innerHTML + `
                        </select> `
    return selectInnerHTML
}

/**
 * @param formFieldName
 * @returns {string}
 * @internal fetches the HTML label for the given formField.
 */
function getHtmlLabelForField (formFieldName) {
    let htmlLabel = ''
    standardCustomFields = JSON.parse(mio_std_cus_fields)
    $.each(standardCustomFields, function (key, value) {
        if (value.name === formFieldName) {
            htmlLabel = key
            return false
        }
    })
    
    if (htmlLabel === '') {
        htmlLabel = 'Contact.' + formFieldName
    }
    return htmlLabel
}

if (emio_customfields_mapping_from_db) {
    mapping_from_db = JSON.parse(atob(emio_customfields_mapping_from_db))
    console.log('mapping_from_db', mapping_from_db)
    $.each(mapping_from_db, function (index, value) {
        if ($.trim(value['marketing-in-one-custom-field']) === '') {
            return
        }
        let emio_customfield = value['email-in-one-custom-field']
        let mio_customfield = getHtmlLabelForField(value['marketing-in-one-custom-field'])
        let action = value['action']
        const newTrTest = `
        <tr class="">
            <td class="pt-3-half mio_customfield_frm_db"
                contenteditable="true"
                data-id="` + value['marketing-in-one-custom-field-id'] + `"
                data-htmllabel="` + mio_customfield + `"
                data-value="` + value['marketing-in-one-custom-field'] + `" >` + mio_customfield + `</td>
            <td class="pt-3-half emio_customfield_frm_db" contenteditable="true">` + emio_customfield + `</td>
            <td><span class="table-remove">
                <i style="color:#ff0000;cursor: pointer;" class="fas fa-minus-circle"></i></span>
            </td>
        </tr>
        `;
        //appending html to
        $ ( 'tbody' ).append ( newTrTest );
    } );
}

function getMIOSelectedOption() {
    let mio_selected_customfield;
    $ ( 'tbody > tr > td.CIO > select.cio-select' ).change ( function (e) {
        mio_selected_customfield = $ ( "option:selected", this )[0].innerHTML;
        return mio_selected_customfield;
    } );
    return '';
}

function geteMIOSelectedOption() {
    $ ( 'tbody > tr > td.emio-custom-fields > select' ).change ( function (e) {
        emio_selected_customfield = $ ( "option:selected", this )[0].innerHTML;
        return emio_selected_customfield;
    } );
    return '';
}

let tmp = '';
// make a copy of params used for appending the standard field.
let campaignParamsBackup = Object.assign ( tmp, mio_customfields );

$ ( '.table-add' ).on ( 'click', 'a', () => {
    let newTr = generateHTML ();
    $ ( 'tbody' ).append ( newTr );
} );

$tableID.on ( 'click', '.table-remove', function () {
    let tr = ( $ ( this ).parents ( 'tr' ) );
    let cio_select_value = tr.find ( 'td.CIO >select.cio-select option:selected' ).text ();
    // Add standard field to global campaignInOneParams  for latter usage.
    mio_customfields[cio_select_value] = campaignParamsBackup[cio_select_value];
    $ ( this ).parents ( 'tr' ).detach ();
} );

$tableID.on ( 'click', '.table-up', function () {
    const $row = $ ( this ).parents ( 'tr' );
    if ( $row.index () === 1 ) {
        return;
    }
    $row.prev ().before ( $row.get ( 0 ) );
} );

$tableID.on ( 'click', '.table-down', function () {
    const $row = $ ( this ).parents ( 'tr' );
    $row.next ().after ( $row.get ( 0 ) );
} );

// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.on ( 'click', () => {

    const $rows = $tableID.find ( 'tr:not(:hidden)' );
    const headers = [];
    const data = [];

    // Get the headers (add special header logic here)
    $ ( $rows.shift () ).find ( 'th:not(:empty)' ).each ( function () {
        console.log ( 'text', $ ( this ).text () )
        headers.push ( $.trim ( $ ( this ).text () ).replace ( /\s+/g, '-' ).toLowerCase () );
    } );

    // Turn all existing rows into a loopable array
    $rows.each ( function () {
        const $td = $ ( this ).find ( 'td' );
        const h = {};
        console.log ( 'headers', headers );
        // Use the headers from earlier to name our hash keys
        headers.forEach ( (header, i) => {
            i = 1;
            switch (header) {
                case 'marketing-in-one-custom-field':
                    h[header] = $('.CIO', this).find('option:selected').data('value')
                    h['html_label'] = $('.CIO', this).find(':selected').data('htmllabel')
                    h['marketing-in-one-custom-field-id'] = $('.CIO', this).find('option:selected').val()
                    if (h[header] === undefined) {
                        h[header] = $(this).find('td.mio_customfield_frm_db').data('value')
                        h['html_label'] = $(this).find('td.mio_customfield_frm_db').data('htmllabel')
                        h['marketing-in-one-custom-field-id'] = $(this).find('td.mio_customfield_frm_db').data('id')
                    }
        
                    // Fetch the Standard Fields
                    // let CIOText = $ ( ".CIO", this ).find ( 'option:selected' ).text ();
                    // let CIOValue = $ ( ".CIO", this ).find ( 'option:selected' ).val ();
                    // h[header] = CIOText;
                    // Fetch the Custom Fields
                    /*                    if ( CIOText.length === 0 ) {
											let CIOCustomText = $ ( this ).find ( 'td.mio_customfield_frm_db' ).text ();
											let valSplit = CIOCustomText.split ( '.' );
											if ( valSplit.length >= 2 && valSplit[1].length !== 0 ) {
												CIOValue = 'Contact_' + valSplit[1];
											}
											h[header] = CIOCustomText;
										}*/
        
                    break
                case 'email-in-one-custom-field':
                    let emio_custom_field = $('.emio-select', this).find('option:selected').text()
                    if (emio_custom_field.length === 0) {
                        emio_custom_field = $(this).find('td.emio_customfield_frm_db').text()
                    }
                    h[header] = emio_custom_field
                    break
            }
            if ( h[header] === "" ) {
                h[header] = $td.eq ( i ).text ();
            }
        } );

        data.push ( h );
    } );

    value = JSON.stringify ( data );
    encryptedValue = btoa ( value );
    $ ( "#emio-mapping" ).attr ( 'value', encryptedValue );
    $ ( "#second" ).prop ( 'disabled', true );
    $ ( "#submit" ).click ();
    // Output the result
    $EXPORT.text ( JSON.stringify ( data ) );
} );

//This is to Generate html for select  to mapping table
function generateSelect(array) {
    let innerHTML = "";
    //get mapping array from database but for now hard-coded
    let json_parsed = $.parseJSON ( array );
    $.each ( json_parsed, function (key, value) {
        let label = getHtmlLabelForField(value.fieldname)
        if ( !label.trim () ) {
            return;
        }
        let id = value.id
        let select = `<option value="` + id + `" data-id ="` + id + `" data-value="` + value.fieldname + `"  data-htmllabel="` + label + `">` + label + `</option>`
        innerHTML += select;
    } );
    let HTML = ` <select  class="cio-select form-control form-control-md" style="opacity:1">
                          ` + innerHTML + `
                        </select> `;
    return HTML;
}

function getTableLength() {
    return $ ( '.table tr' ).length;
}


// Generates table row for each pressed '+' sign.
function generateHTML() {
    // remove the selected option from CIO dropdown list
    removeCIOSelectedOptions ();
    console.log ( "emio_customfields", emio_customfields );
    let i = getTableLength ();
    let mioCustomFields = generateSelect ( mio_customfields );
    let emioCustomFields = generateSelectForeMIOCustomFields ( emio_customfields );
    return `
          <tr class="defaultTableValue" id="` + i + `">
              <td class="pt-3-half select2 CIO">` + mioCustomFields + `</td>
              <td class="pt-3-half select2 emio-custom-fields">` + emioCustomFields + `</td>
              <td valign="middle">
                  <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
              </td>
          </tr>`;
}

/**
 *  remove the selected fields from cio select options
 */
function removeCIOSelectedOptions() {
    const rows = $tableID.find ( 'tr:not(:hidden) >td >select.cio-select option:selected' );
    let i = 0;
    // Currently remove the used standard param from the list.
    $.each ( rows, function (key, value) {
        delete mio_customfields[value.value];
    } );
}

/**
 *  This functions generate HTML for datatypes
 */
function generateSelectForeMIOCustomFields(array) {
    let innerHTML = "";
    innerHTML += `
                  <optgroup label="To create customfield in eMIO">
                    <option value='emio_autocreate_customfield' selected>Auto-Create</option>
                  </optgroup>`;
    innerHTML += `<optgroup label="Current list of cusstomfields from eMIO">`;
    $.each ( array, function (key, value) {
        let select = `<option value="` + key + `">` + key + `</option>`;
        innerHTML += select;
    } );
    innerHTML += `</optgroup>`;
    return ` <select  class="form-control form-control-md emio-select" style="opacity:1">
                          ` + innerHTML + `
                        </select> `;
}
