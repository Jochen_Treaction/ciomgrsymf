//on view load
$(function () {
    //create mapping table if mapping exits
    if (settings['mapping']['mappingTable']) {
        generateMappingTable(settings['mapping']['mappingTable']);
    }
    //add Email to table by default if create
    if (window.location.href.indexOf('/create') > 0) {
        let emailTr = generateEmailTr();
        $('tbody').append(emailTr);
    }
    //processhook drag and drop
    $("#allFacets, #userFacets").sortable({
        connectWith: "ul",
        placeholder: "placeholder",
        delay: 150
    })
        .disableSelection()
        .dblclick(function (e) {
            var item = e.target;
            if (e.currentTarget.id === 'allFacets') {
                //move from all to user
                $(item).fadeOut('fast', function () {
                    $(item).appendTo($('#userFacets')).fadeIn('slow');
                });
            } else {
                //move from user to all
                $(item).fadeOut('fast', function () {
                    $(item).appendTo($('#allFacets')).fadeIn('slow');
                });
            }
        });
});

//mapping table logic
const $tableID = $('#table');
const $BTN = $("#cio-select-next");
const $EXPORT = $('#export');
let campaignInOneParams = $('#table').data("isTest");

let tmp = '';
// make a copy of params used for appending the standard field.
let campaignParamsBackup = Object.assign(tmp, campaignInOneParams);

$('.table-add').on('click', 'a', () => {
    let newTr = generateHTML ();
    $ ( 'tbody' ).append ( newTr );


    // Update datatype for new created Table Row
    let v = $ ( 'tbody tr:last td.CIO select :selected' ).val ()
    let ftype = $ ( 'tbody tr:last td.CIO select :selected' ).data ( 'dtype' );
    if ( ftype !== 'Text' ) {
        $ ( 'tbody tr:last td.DataType' ).html ( ftype );
    }

    //this is for adding custom params
    $ ( '.fieldTypeSelect' ).on ( 'change', function () {
        let selectedValue = this.value;
        let row = $ ( this ).closest ( "tr" );
        let fieldtypeClass = row.find ( '.CIO' );
        if ( selectedValue === 'custom' ) {
            let cioSelect = fieldtypeClass.empty ();
            cioSelect.append ( `<input class="form-control cioCustom" value="Contact." type="text" placeholder="CIO Custom Field">` );
        } else if ( selectedValue === 'standard' ) {
            let cioSelect = fieldtypeClass.empty ();
            let CIOStandatdFields = generateSelect ( campaignInOneParams );
            cioSelect.append ( CIOStandatdFields );
        }
    } );

    $ ( '.cio-select' ).on ( 'change', function () {
        let selectedValue = this.value;
        let sdfld_dtype = $ ( this ).find ( ':selected' ).data ( 'dtype' );
        let dtype = $ ( this ).closest ( 'tr' ).find ( 'td.DataType' );
        dtype.text ( sdfld_dtype );
    } );


});

$tableID.on('click', '.table-remove', function () {
    let tr = ($(this).parents('tr'));
    let cio_select_value = tr.find('td.CIO >select.cio-select option:selected').text();
    //add field to campaign in one params
    if (campaignParamsBackup[cio_select_value]) {
        campaignInOneParams[cio_select_value] = campaignParamsBackup[cio_select_value];
    }
    $(this).parents('tr').detach();
});

$tableID.on('click', '.table-up', function () {
    const $row = $(this).parents('tr');
    if ($row.index() === 1) {
        return;
    }
    $row.prev().before($row.get(0));
});

$tableID.on('click', '.table-down', function () {
    const $row = $(this).parents('tr');
    $row.next().after($row.get(0));
});

// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.on('click', () => {
    let mappingTableVlaue = $("#urlID").attr('value');
    if (typeof mappingTableVlaue === "undefined" || mappingTableVlaue === 'W10=') {
        const $rows = $tableID.find('tr:not(:hidden)');
        const headers = [];
        const data = [];

        // Get the headers (add special header logic here)
        $($rows.shift()).find('th:not(:empty)').each(function () {
            headers.push($(this).text().toLowerCase());
        });

        // Turn all existing rows into a loopable array
        $rows.each(function () {
            const $td = $(this).find('td');
            const h = {};

            // Use the headers from earlier to name our hash keys
            headers.forEach((header, i) => {
                i = 1;
                switch (header) {
                    case "marketing-in-one":
                        let CIOText
                        // Fetch the Standard Fields
                        CIOText = $('.CIO', this).find('option:selected').val()
                        h['html_label'] = $('.CIO', this).find(':selected').data('htmllabel')
                        // Fetch the Custom Fields
                        if (typeof CIOText === 'undefined') {
                            //CIOText = $(".CIO", this).text();
                            CIOText = $('.CIO', this).data('value')
                        }
                        h[header] = CIOText;
                        //h['html_form'] = CIOText;
                        break;
                    case "data type":
                        let dataType = $(".DataType", this).find('option:selected').val();
                        if (typeof dataType == 'undefined') {
                            dataType = $(".DataType", this).text();
                        }
                        h["data_type"] = dataType;
                        break;
                    case "required":
                        let requiredText = $(".required", this).find('option:selected').val();
                        if (typeof requiredText == 'undefined') {
                            requiredText = $(".required", this).text();
                        }
                        h[header] = requiredText;
                        break;
                    /*default:
                        h[header] = "";*/

                }
                if (h[header] === "") {
                    h[header] = $td.eq(i).text();
                }
            });

            data.push(h);
        });
        let structuredData = structureMappingDataToHTMLTable(data);
        let value = JSON.stringify(structuredData);
        let encryptedValue = btoa(value);
        $("#urlID").attr('value', encryptedValue);
        $("#second").prop('disabled', true);
        $("#submit").click();
        // Output the result
        $EXPORT.text(JSON.stringify(data));
    }
});

function getCategoryFields (categoryName) {
    if (settings[categoryName]) {
        return settings[categoryName]
    }
    return [];
}

/*function generateSelect(array) {
    
    let customFieldOptions = ''
    let standardFieldOptions = ''
    let standardCustomFieldOptions = ''
    let trackingFieldOptions = ''
    let sdField = []
    let standardCustomFields = getCategoryFields('standard_custom_fields')
    let trackingFields = getCategoryFields('tracking_fields')
    let standardFieldList = getStandardAndTechnicalFieldsAsArr()

    
    $.each(standardCustomFields, function (key, value) {
        let label = value.htmllabel
        let field_value = value.fieldname
        let field_dtype = value.datatype
        
        let select = `<option value="` + field_value + `" data-dtype="` + field_dtype + `"  data-htmllabel="` + label + `">` + label + `</option>`
        standardCustomFieldOptions += select
    })
    
    $.each(trackingFields, function (key, value) {
        let label = value.htmllabel
        let field_value = value.fieldname
        let field_dtype = value.datatype
        
        let select = `<option value="` + field_value + `" data-dtype="` + field_dtype + `"  data-htmllabel="` + label + `">` + label + `</option>`
        trackingFieldOptions += select
    })
    
    $.each(standardFieldList, function (sd_key, sd_value) {
        sdField.push(sd_value['name'])
    })
    //get mapping array from database but for now hard-coded
    $.each(array, function (key, value) {
        if (key !== 'campaign_id') {
            let label = key
            let field_value = ''
            let field_dtype = ''
            if (typeof value === 'object' && $.trim(value.name) !== '') {
                field_value = value.name
                field_dtype = value.datatype
                
                if (sdField.includes(field_value)) {
                    let select = `<option value="` + field_value + `" data-dtype="` + field_dtype + `"  data-htmllabel="` + label + `">` + label + `</option>`
                    standardFieldOptions += select
                } else {
                    let select = `<option value="` + field_value + `" data-dtype="` + field_dtype + `"  data-htmllabel="` + label + `">` + label + `</option>`
                    customFieldOptions += select
                }
            }
            
        }
    })
    
    let HTML = ` <select  class="cio-select form-control form-control-md" style="opacity:1">
                    <optgroup label="Standard Fields">
                          ` + standardFieldOptions + `
                    </optgroup>
                    <optgroup label="Tracking Fields">
                          ` + trackingFieldOptions + `
                    </optgroup>
                    <optgroup label="Custom Fields">
                          ` + customFieldOptions + `
                          ` + standardCustomFieldOptions + `
                    </optgroup>
                  </select> `
    return HTML;
}*/

function getDataType(array) {
    let dtype = '';
    $.each ( array, function (key, value) {
        dtype = value.datatype;
        return false;
    } );
    return dtype;
}

function getTableLength() {
    return $('.table tr').length;
}

/**
 * Generates table row for Mapping MIO and HtmlFields.
 * @returns {string}
 */
function generateHTML() {
    // remove the selected option from CIO dropdown list
    removeCIOSelectedOptions();
    
    let i = getTableLength();
    let CIOSelect;
    let dataTypeHtml = 'Date';
    
    CIOSelect = generateSelectForMIOColumn();
    
    let requiredHtml = 'No';
    
    return `
                <tr class="defaultTableValue" id="` + i + `">
                    <td class="pt-3-half select2 CIO">` + CIOSelect + `</td>
                    <td class="pt-3-half select2 DataType" style="position:relative;font-weight: bold">` + dataTypeHtml + `</td>
                    <td class="pt-3-half select2 required" style="position:relative;font-weight: bold">` + requiredHtml + `</td>
                    <td valign="middle">
                        <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                    </td>
                </tr>`;
}

/**
 * Generates the HTML select options dynamically
 * Options contain -> standardfields, eCommerceFields, customfields,
 * @param array
 * @returns {string}
 */
function generateSelectForMIOColumn () {
    let eComFldOptions = ''
    let sdFldOptions = ''
    let cusFldOptions = ''
    let techFldOptions = ''
    let standardFields = getCategoryFields('standard_fields');
    let technicalFields = getCategoryFields('technical_fields');
    let customFields = settings['customFieldList'];

    let eCommerceFields = settings['ecommerce_fields'];
    
    // option fields for standard fields.
    $.each(standardFields, function (key, value) {
        
        let sdFldOption = `<option value="` + value['fieldname'] + `" data-dtype="` + value['dtype_label'] + `"  data-htmllabel="` + value['htmllabel'] + `">` + value['htmllabel'] + `</option>`
        sdFldOptions += sdFldOption
    })
    // option fields for standard fields.
    $.each(technicalFields, function (key, value) {
        
        let techFldOption = `<option value="` + value['fieldname'] + `" data-dtype="` + value['dtype_label'] + `"  data-htmllabel="` + value['htmllabel'] + `">` + value['htmllabel'] + `</option>`
        techFldOptions += techFldOption
    })
    // option fields for custom fields
    $.each(customFields, function (key, value) {
        
        if (value['fieldname'] !== '') {
            
            let field_value = value['fieldname']
            let dtype_label = value['datatypes_name']
            let field_htmllabel = 'Contact.' + field_value
            
            let cusFldOption = `<option value="` + field_value + `" data-dtype="` + dtype_label + `"  data-htmllabel="` + field_htmllabel + `">` + field_htmllabel + `</option>`
            cusFldOptions += cusFldOption
        }
    })
    // option fields for ecommerce fields.
    $.each(eCommerceFields, function (key, value) {
        
        let eComFldOption = `<option value="` + value['fieldname'] + `" data-dtype="` + value['dtype_label'] + `"  data-htmllabel="` + value['htmllabel'] + `">` + value['htmllabel'] + `</option>`
        eComFldOptions += eComFldOption
    })
    
    let HTML = ` <select  class="cio-select form-control form-control-md" style="opacity:1">
                    <optgroup label="Standard Fields">
                          ` + sdFldOptions + `
                    </optgroup>
                    <optgroup label="eCommerce Fields">
                            ` + eComFldOptions + `
                    </optgroup>
                    <optgroup label="Technical Fields">
                            ` + techFldOptions + `
                    </optgroup>
                    <optgroup label="Custom Fields">
                            ` + cusFldOptions + `
                    </optgroup>
                  </select> `
    return HTML
    
}

/**
 *  remove the selected fields from cio select options
 */
function removeCIOSelectedOptions () {
    const rows = $tableID.find('tr:not(:hidden) >td >select.cio-select option:selected')
    let i = 0
    // Currently remove the used standard param from the list.
    $.each(rows, function (key, value) {
        delete campaignInOneParams[value.text];
    });
}

/**
 *  This functions generate HTML for datatypes
 */
function generateSelectForDataType(array) {
    let innerHTML = "";

    //get mapping array from database but for now hard-coded
    $.each(array, function (key, value) {
        let select = `<option value="` + value + `">` + value + `</option>`;
        innerHTML += select;
    });

    let HTML = ` <select disabled="disabled"  class="form-control form-control-md" style="opacity:1">
                          ` + innerHTML + `
                        </select> `;
    return HTML;

}

function generateCustomFieldsList() {
    let customFields = settings['customFieldList'];
    let customFieldName = {};
    $.each(customFields, function (index, value) {
        let fieldName = value['fieldname'];
        let field_dtype = value['datatypes_name'];
        customFieldName["Contact." + fieldName] = {'name': fieldName, 'datatype': field_dtype};
    });
    return customFieldName;
}

/**
 * Returns HTML Label for the fieldnames for mapping MIO and HTML fields.
 * fieldNames is for 'standard','eCommerce' or 'custom'.
 * @param formFieldName
 * @returns {string}
 */
function getHtmlLabelForField (formFieldName) {
    let htmlLabel = ''
    if ($.trim(formFieldName) <= 0) {
        return ''
    }
    
    if (formFieldName === 'Contact.Email') {
        formFieldName = 'email'
    }
    
    let standardCustomFields = getCategoryFields('ecommerce_fields')
    let technicalFields = getCategoryFields('technical_fields')
    let standardFields = getCategoryFields('standard_fields')
    let customFields = getCategoryFields('customFieldList')
    
    // check if the field is present in standardCustomFields.
    $.each(standardCustomFields, function (key, value) {
        if (htmlLabel !== '') {
            return false
        }
        if (value.fieldname === formFieldName) {
            htmlLabel = value.htmllabel
            return false
        }
    })
    
    // check if the field is present in standardCustomFields.
    $.each(technicalFields, function (key, value) {
        if (htmlLabel !== '') {
            return false
        }
        if (value.fieldname === formFieldName) {
            htmlLabel = value.htmllabel
            return false
        }
    })
    
    $.each(standardFields, function (key, value) {
        if (htmlLabel !== '') {
            return false
        }
        if (value.fieldname === formFieldName) {
            htmlLabel = value.htmllabel
            return false
        }
    })
    
    $.each(customFields, function (key, value) {
        if (htmlLabel !== '') {
            return false
        }
        let searchValue = value.fieldname.toLowerCase()
        if (searchValue === formFieldName.toLowerCase()) {
            htmlLabel = 'Contact.' + value.fieldname
            return false
        }
    })
    
    return htmlLabel
}

/**
 * Helper function for generating the Mapping Table for MIO and HTML Fields.
 * @param structure
 * @returns fieldName of type string
 *
 */
function parseFieldName (structure) {
    let fieldName = ''
    let keys = Object.keys(structure)
    $.each(keys, function (key, value) {
        if ($.inArray(value, ['required', 'datatype', 'regex']) === -1) {
            fieldName = value
            return false
        }
    })
    return fieldName
}

/**
 * Generates the Mapping tab, by considering the mappingValue from DB,
 * @param {string} mappingValue
 * @author AKi
 * @comment takes mappings string and appends mapping table
 */
function generateMappingTable (mappingValue) {
    //decode mapping
    let mapping = JSON.parse(atob(mappingValue))['contact'];
    $.each(mapping, function (index, element) {
        $.each(element, function (key, value) {
            let fieldValue = (value['field_name'] !== undefined) ? value['field_name'] : parseFieldName(value);
            let CIO = getHtmlLabelForField(fieldValue);
            let dataType = value['datatype'];
            let required =  (value['required']) ? "yes" : "No";
            //todo:use html template to store html templates rather than using strings
            const newTrTest = `
        <tr class="">
                                        <td class="pt-3-half CIO" contenteditable="false" data-value=` + fieldValue + ` >` + CIO + `</td>
                                        <td class="pt-3-half DataType" contenteditable="false"> ` + dataType + `</td>
                                        <td class="pt-3-half required"  contenteditable="false">` + required + `</td>
                                        <td>
                                          <span class="table-remove"><i style="color:#ff0000;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                        </td>
                                    </tr>

        `;
            //appending html to
            $('tbody').append(newTrTest);
        });
    });
}

/**
 * @param (void) getStandardAndTechnicalFieldsAsArr
 * @author AKi
 * @comment generates standard field list from input data
 */
function getStandardAndTechnicalFieldsAsArr () {
    let list = []
    let standardFields = getCategoryFields('standard_fields')
    let technicalFields = getCategoryFields('technical_fields')
    let ecommerceFields = getCategoryFields('ecommerce_fields')
    $.each(standardFields, function (key, value) {
        list.push(value['fieldname'])
    })
    $.each(technicalFields, function (key, value) {
        list.push(value['fieldname'])
    })
    $.each(ecommerceFields, function (key, value) {
        list.push(value['fieldname'])
    })
    return list
}

/**
 * @param {json} data
 * @author AKi
 * @comment takes mappings string and appends mapping table
 */
function structureMappingDataToHTMLTable (data) {
    if (data.length === 0) {
        return data;
    }
    let structuredMappingData = {};
    let standard = [];
    let custom = [];
    let sdFieldsAsArr = generateStandardFieldList();
    data = appendAdditionalFieldsForClient(data);

    $.each(data, function (key, value) {
        let field = '';
        if(value['field_name']){
             field = value['field_name'];
        }
        //new parameter marketing-in-one for plugin generated webhooks
        if(value['marketing-in-one']){
            field = value['field_name'];
        }
        if (sdFieldsAsArr.includes(field)) {
            //todo:take the structure dynamically form mapping data type
            standard.push(value);
        } else {
            custom.push(value);
        }
    });
    structuredMappingData["contact"] = {standard, custom};
    return structuredMappingData;
}

//checks for the campaign name
if (campaignName === "undefined" || $(location).attr('href').includes('copy')) {
    jQuery('input#name1').on('mouseout change blur', function () {
        let myurl = jQuery(this).data('urlprefix') + jQuery(this).val() + '/' + jQuery(this).data('getparams');
        let settings = {
            url: myurl,
            method: "GET",
            timeout: 0,
            headers: {
                'Content-Type': "application/x-www-form-urlencoded"
            }
        };
        $.ajax(settings).done(function (response) {
            let check = JSON.parse(response);
            if (check.status === false) {
                $('input#name1').removeClass('is-valid').addClass('is-invalid');
                let message = check['message'];
                let html = `<font color="red">` + message + `</font>`;
                $('#campaignName').html(html);
            } else {
                $('input#name1').removeClass('is-invalid').addClass('is-valid');
                let message = check['message'];
                let html = `<font color="green">` + message + `</font>`;
                $('#campaignName').html(html);
                $BTN.prop('disabled', false);
            }
        });

    });
} else {
    $('.btn-success').text("Update settings");
    $BTN.prop('disabled', false);
    jQuery('input#name1').prop('readonly', true);
    jQuery('input#name1').on('mouseout change blur', function () {
        let message = "Page name can not be changed";
        let html = `<font color="green">` + message + `</font>`;
        $('#campaignName').html(html);
    });
}
//clear data functionality
$("#clearDate").click(function () {
    $('#kt_datetimepicker_3').val('').datepicker('update');
});
$("#clearDate2").click(function () {
    $('#kt_datetimepicker_2').val('').datepicker('update');
});
//when facebook and google inputs are typed they are automatically on
$('#googleAnalyticsInput').keyup(function () {
    $('#googleAnalyticsInputChecked').prop("checked", true);
});
$('#facebookAnalyticsInput').keyup(function () {
    $('#facebookAnalyticsInputChecked').prop("checked", true);
});


/**
 *@return Html text for email row
 * @author AKi
 * @comment makes email tr for mapping table
 */
function generateEmailTr() {
    //todo:inject this fields from backend
    let emailField = "Contact.Email";
    let dataTypeHtml = "Email";
    let requiredHtml = 'Yes'
    let fieldValue = 'email'
    delete campaignInOneParams["Contact.Email"];

    return `
                <tr class="defaultTableValue" id="` + 0 + `">
                    <td class="pt-3-half select2 CIO" data-value=` + fieldValue + `>` + emailField + `</td>
                    <td class="pt-3-half select2 DataType" style="position:relative;font-weight: bold">` + dataTypeHtml + `</td>
                    <td class="pt-3-half select2 required" style="position:relative;font-weight: bold">` + requiredHtml + `</td>
                    <td valign="middle">
                        <span class="table-remove-email"><i style="cursor: pointer;" class="fas fa-minus-circle"></i></span>
                    </td>
                </tr>`;
}

//processhooks to workflow
$('#finalSubmit').on('click', () => {
    let processhook=new Array();
    $('#userFacets li').each(function (i) {
        let processhookType = $(this)[0].getAttribute('processhookType');
        let processhookName = $(this)[0].getAttribute('processhookName');
        let userProcesshookId = $(this)[0].getAttribute('userProcesshookId');
        let type = {
            'processhookType': processhookType,
            'processhookName': processhookName,
            'userProcesshookId' : userProcesshookId
        }
        processhook.push(type);
    });
    if(processhook.length !== 0){
        processhook = {
            "index.html":{
                "data_received":processhook
            }
        }
        let value = JSON.stringify(processhook);
        $("#selectedProcesshooks").attr('value', value);
    }
});

/**
 * @param {string} string
 * @comment some handy function, act as replacement ucfirst in php
 */
function capitalizeFirstLetter(string) {
    return string[0].toUpperCase() + string.slice(1);
}

/**
 * @param {string} string
 * @comment some handy function, act as replacement Icfirst in php
 */
function lowerizeFirstLetter(string) {
    return string[0].toLowerCase() + string.slice(1);
}

/**
 * @param {array} data
 * @return {array}
 * @internal Append the fields required by the client(baby) to handle form in index.html
 * @author Aki
 */
function appendAdditionalFieldsForClient(data) {
    //init the structured data
    let structuredData = [];
    //All the marketing-in-one fields
    let marketingInOneFields = [];

    if(settings.hasOwnProperty('customFieldList') && settings['customFieldList']){
        marketingInOneFields = settings['customFieldList'].concat(settings['standard_fields'],
            settings['ecommerce_fields']);
    }else{
        marketingInOneFields = settings['standard_fields'].concat(settings['ecommerce_fields']);
    }

    console.log('marketingInOneFields',marketingInOneFields); //[object,object]

    // Add the additional fields required to selected fields
    $.each(data, function (key, value) {
        $.each(marketingInOneFields, function (index, fieldData) {
            if(value['marketing-in-one'] === fieldData['fieldname']){
                //data field is the single instance object of the data
                let dataField = {};
                //required fields for mapping table in MIO
                dataField['field_name'] = fieldData['fieldname'];
                dataField['marketing-in-one'] = fieldData['fieldname'];
                dataField[fieldData['fieldname']] = "";
                //additional fields required to handle client
                if(fieldData.hasOwnProperty('fieldtype')){
                    //if fieldtype property exits this field belongs to standard fields or ecommerce field
                    dataField['datatype'] =fieldData['dtype_label'];
                    //Email field is always required , All other standard fields are not required
                    dataField['required'] = (fieldData['fieldname'] === 'email') ?  1 : 0 ;
                    //Other HTML required fields
                    dataField['html_label'] = fieldData['dtype_label'];
                    dataField['html_placeholder'] = fieldData['html_placeholder'];
                    dataField['html_hidden_field'] = parseInt(fieldData['hidden_field']);//convert to int
                    dataField['field_type'] = fieldData['fieldtype'];
                    dataField['php_type'] = fieldData['dtype_name'];
                }else{
                    //This field belongs to custom field
                    dataField['datatype'] =fieldData['datatypes_name'];
                    dataField['required'] = parseInt(fieldData['required']);//convert to int
                    dataField['html_label'] = fieldData['fieldname'];
                    dataField['html_placeholder'] = fieldData['fieldname'];
                    dataField['html_hidden_field'] = 0;//always custom fields are shown in client UI
                    dataField['field_type'] = 'custom';
                    dataField['php_type'] = fieldData['php_type'];
                }
                dataField['regex'] = fieldData['regex'];
                //push the instance to structured data
                structuredData.push(dataField);
            }
        });
    });
    //return the structured data
    return structuredData;
}

/**
 * @author AKi
 * @comment generates standard field list from input data
 */
function generateStandardFieldList () {
    let list = [];
    let standardFields = getCategoryFields('standard_fields');
    let ecommerceFields = getCategoryFields('ecommerce_fields');
    let technicalFields = getCategoryFields('technical_fields');
    $.each(standardFields, function (key, value) {
        list.push(value['fieldname']);
    });
    $.each(ecommerceFields, function (key, value) {
        list.push(value['fieldname']);
    });
    $.each(technicalFields, function (key, value) {
        list.push(value['fieldname']);
    });
    return list;
}

