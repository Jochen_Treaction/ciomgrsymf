//Mapping table functionality
const $tableID = $('#table');
const $BTN = $("#cio-select-next");
const $EXPORT = $('#export');
const $pageFlowWizard = $('#pageFlow');
let campaignInOneParams = $('#table').data("isTest");
//process global variables
let workflows = {};
let buttonsForProcesshooks = {};

const processhookDropdownMenu = $('#processhookConfigureDropdownMenu');
const processhookBuilder = $('#buttonProcesshookBuilder');
const definedProcesshooksDiv = $('#definedProcesshooks');
const activeProcesshooksDiv = $('#activeProcesshooks');
let button;


// processhook variables configuration
if (settings['processhooks']['defined'] && !jQuery.isEmptyObject(settings['processhooks']['defined'] )) {
    workflows = settings['processhooks']['defined'];
}

if (settings['page_flow']['buttons']) {
    buttonsForProcesshooks = JSON.parse(settings['page_flow']['buttons']);
    addInitialButtonsForProcesshookConfigurator(buttonsForProcesshooks);
}

//deleting unwanted keys(new requirement)
let tmp = '';
// make a copy of params used for appending the standard field.
let campaignParamsBackup = Object.assign(tmp, campaignInOneParams);

//########################### Begin: Mapping MIO and HTML Wizard #########################//
//view onload functions
$(function () {
    //create mapping table if mapping exits
    if (settings['mapping']['mappingTable']) {
        generateMappingTable(settings['mapping']['mappingTable']);
    }
    $('.splittestTable').hide();
    //add Email to table by default for new page in /create ; or wizard steps /revise, /copy gets ignored
    if (window.location.href.indexOf('/create') > 0) {
        let emailTr = generateEmailTr();
        $('tbody').append(emailTr);
    }
    //processhook drag and drop
    let definedProcesshooks, activeProcesshooks, processhook;
    $("#definedProcesshooks, #activeProcesshooks").sortable({
        connectWith: "ul",
        placeholder: "placeholder",
        delay: 150,
        start: function (event, ui) {
            processhook = ui.item;
            activeProcesshooks = definedProcesshooks = ui.item.parent();
        },
        stop: function (event, ui) {
            configureWorkflow(processhook.text());
        }
    })
        .disableSelection()
        .dblclick(function (e) {
            var item = e.target;
            if (e.currentTarget.id === 'definedProcesshooks') {
                //move from all to user
                $(item).fadeOut('fast', function () {
                    $(item).appendTo($('#activeProcesshooks')).fadeIn('slow');
                });
            } else {
                //move from user to all
                $(item).fadeOut('fast', function () {
                    $(item).appendTo($('#definedProcesshooks')).fadeIn('slow');
                });
            }
        });

});

$('.table-add').on('click', 'a', () => {
    let newTr = generateHTML();
    $('tbody').append(newTr);

    /*
        // Update datatype for new created Table Row
        let v = $('tbody tr:last td.CIO select :selected').val()
        let ftype = $('tbody tr:last td.CIO select :selected').data('dtype');
        if (ftype !== 'Text') {
            $('tbody tr:last td.DataType').html(ftype);
        }


        //this is for adding custom params - No longer need this but can use in future
        $('.fieldTypeSelect').on('change', function () {
            let selectedValue = this.value;
            let row = $(this).closest("tr");
            let fieldtypeClass = row.find('.CIO');
            if (selectedValue === 'custom') {
                let cioSelect = fieldtypeClass.empty();
                cioSelect.append(`<input class="form-control cioCustom" value="Contact." type="text" placeholder="CIO Custom Field">`);
            } else if (selectedValue === 'standard') {
                let cioSelect = fieldtypeClass.empty();
                let CIOStandatdFields = generateSelect(campaignInOneParams);
                cioSelect.append(CIOStandatdFields);
            }
        });

        */

    //change data type according to selected value in MIO field
    $('.cio-select').on('change', function () {
        let sdfld_dtype = $(this).find(':selected').data('dtype');
        let dtype = $(this).closest('tr').find('td.DataType');
        dtype.text(sdfld_dtype);
    });

});


//configuration mapping on row remove
$tableID.on('click', '.table-remove', function () {
    let tr = ($(this).parents('tr'));
    let cio_select_value = tr.find('td.CIO >select.cio-select option:selected').text();
    //add field to campaign in one params
    if (campaignParamsBackup[cio_select_value]) {
        campaignInOneParams[cio_select_value] = campaignParamsBackup[cio_select_value];
    }
    $(this).parents('tr').detach();
});



// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;
//fill the mapping table
$BTN.on('click', () => {
    let mappingTableVlaue = $("#urlID").attr('value');
    if (typeof mappingTableVlaue === "undefined" || mappingTableVlaue === 'W10=') {//check if mapping table is filled
        const $rows = $tableID.find('tr:not(:hidden)');
        const headers = [];
        const data = [];

        // Get the headers (add special header logic here)
        $($rows.shift()).find('th:not(:empty)').each(function () {
            headers.push($(this).text().toLowerCase());
        });

        // Turn all existing rows into a loopable array
        $rows.each(function () {
            const $td = $(this).find('td');
            const h = {};

            // Use the headers from earlier to name our hash keys
            headers.forEach((header, i) => {
                i = 1;

                switch (header) {
                    case "marketing-in-one":
                        let CIOText;
                        // Fetch the Standard Fields
                        CIOText = $('.CIO', this).find('option:selected').val();
                        h['html_label'] = $('.CIO', this).find(':selected').data('htmllabel');
                        // Fetch the Custom Fields
                        if (typeof CIOText === 'undefined') {
                            CIOText = $('.CIO', this).data('value');
                        }
                        h[header] = CIOText;
                        break;

                    case "data type":
                        let dataType = $(".DataType", this).find('option:selected').val();
                        if (typeof dataType === 'undefined') {
                            dataType = $(".DataType", this).text();
                            dataType = dataType.replace(/\s+/g, "");
                        }
                        h["data_type"] = dataType;
                        break;

                    case "required":
                        let requiredText = $(".required", this).find('option:selected').val();
                        if (typeof requiredText === 'undefined') {
                            requiredText = $(".required", this).text();
                            if (requiredText === "Yes") {
                                requiredText = "true";
                            }
                        }
                        h[header] = requiredText;
                        break;

                }
                if (h[header] === '') {
                    h[header] = $td.eq(i).text();
                }
            });
            data.push(h);
        });

        console.log(data);

        let structuredData = structureMappingData(data);
        value = JSON.stringify(structuredData);
        encryptedValue = btoa(value);
        $('#urlID').attr('value', encryptedValue);
        $('#second').prop('disabled', true);
    }
});

/**
 * Helper function for getting fields inside "settings" array
 * @param categoryName
 * @returns {*[]|*}
 */
function getCategoryFields (categoryName) {
    if (settings[categoryName]) {
        return settings[categoryName];
    }
    return [];
}

/**
 * Generates the HTML select options dynamically
 * Options contain -> standardfields, eCommerceFields, customfields,
 * @param array
 * @returns {string}
 */
function generateSelect (array) {

    let customFieldOptions = ''
    let standardFieldOptions = ''
    let standardCustomFieldOptions = ''
    //let trackingFieldOptions = '';
    let sdField = []
    let standardCustomFields = getCategoryFields('ecommerce_fields')
    //let trackingFields = getCategoryFields('tracking_fields');
    let standardFieldList = getCategoryFields('standard_fields')//generateStandardFieldList();

    $.each(standardCustomFields, function (key, value) {
        let label = value.htmllabel
        let field_value = value.fieldname
        let field_dtype = value.datatype

        let select = `<option value="` + field_value + `" data-dtype="` + field_dtype + `"  data-htmllabel="` + label + `">` + label + `</option>`
        standardCustomFieldOptions += select
    })

    $.each(standardFieldList, function (sd_key, sd_value) {
        sdField.push(sd_value['name']);
    });
    //get mapping array from database but for now hard-coded
    $.each(array, function (key, value) {
        if (key !== 'campaign_id') {
            let label = key;
            let field_value = '';
            let field_dtype = '';
            if (typeof value === 'object' && $.trim(value.name) !== '') {
                field_value = value.name;
                field_dtype = value.datatype;

                if (sdField.includes(field_value)) {
                    let select = `<option value="` + field_value + `" data-dtype="` + field_dtype + `"  data-htmllabel="` + label + `">` + label + `</option>`;
                    standardFieldOptions += select;
                } else {
                    let select = `<option value="` + field_value + `" data-dtype="` + field_dtype + `"  data-htmllabel="` + label + `">` + label + `</option>`;
                    customFieldOptions += select;
                }
            }

        }
    });

    let HTML = ` <select  class="cio-select form-control form-control-md" style="opacity:1">
                    <optgroup label="Standard Fields">
                          ` + standardFieldOptions + `
                    </optgroup>
                    <optgroup label="Custom Fields">
                            ` + standardCustomFieldOptions + `
                            ` + customFieldOptions + `
                          
                    </optgroup>
                  </select> `;
    return HTML;
}

/**
 * Helper function returns the lenght of the table.
 * @returns {jQuery}
 */
function getTableLength () {
    return $('.table tr').length;
}

/**
 * Generates the HTML select options dynamically
 * Options contain -> standardfields, eCommerceFields, customfields,
 * @param array
 * @returns {string}
 */
function generateSelectForMIOColumn () {
    let eComFldOptions = '';
    let sdFldOptions = '';
    let cusFldOptions = '';
    let standardFields = settings['standard_fields'];
    let customFields = settings['customFieldList'];
    let eCommerceFields = settings['ecommerce_fields'];

    // option fields for standard fields.
    $.each(standardFields, function (key, value) {

        let sdFldOption = `<option value="` + value['fieldname'] + `" data-dtype="` + value['dtype_label'] + `"  data-htmllabel="` + value['htmllabel'] + `">` + value['htmllabel'] + `</option>`;
        sdFldOptions += sdFldOption;
    });
    // option fields for custom fields
    $.each(customFields, function (key, value) {

        if (value['fieldname'] !== '') {

            let field_value = value['fieldname'];
            let dtype_label = value['datatypes_name'];
            let field_htmllabel = 'Contact.' + field_value;

            let cusFldOption = `<option value="` + field_value + `" data-dtype="` + dtype_label + `"  data-htmllabel="` + field_htmllabel + `">` + field_htmllabel + `</option>`;
            cusFldOptions += cusFldOption;
        }
    });
    // option fields for ecommerce fields.
    $.each(eCommerceFields, function (key, value) {

        let eComFldOption = `<option value="` + value['fieldname'] + `" data-dtype="` + value['dtype_label'] + `"  data-htmllabel="` + value['htmllabel'] + `">` + value['htmllabel'] + `</option>`;
        eComFldOptions += eComFldOption;
    });

    let HTML = ` <select  class="cio-select form-control form-control-md" style="opacity:1">
                    <optgroup label="Standard Fields">
                          ` + sdFldOptions + `
                    </optgroup>
                    <optgroup label="eCommerce Fields">
                            ` + eComFldOptions + `
                    </optgroup>
                    <optgroup label="Custom Fields">
                            ` + cusFldOptions + `
                    </optgroup>
                  </select> `;
    return HTML;

}

/**
 * Generates table row for Mapping MIO and HtmlFields.
 * @returns {string}
 */
function generateHTML() {
    // remove the selected option from CIO dropdown list
    removeCIOSelectedOptions();

    let i = getTableLength();
    let CIOSelect;
    let dataTypeHtml = 'Date';


    CIOSelect = generateSelectForMIOColumn();

    let requiredHtml = 'No';

    return `
                <tr class="defaultTableValue" id="` + i + `">
                    <td class="pt-3-half select2 CIO">` + CIOSelect + `</td>
                    <td class="pt-3-half select2 DataType" style="position:relative;font-weight: bold">` + dataTypeHtml + `</td>
                    <td class="pt-3-half select2 required" style="position:relative;font-weight: bold">` + requiredHtml + `</td>
                    <td valign="middle">
                        <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                    </td>
                </tr>`;
}

/**
 * @param array
 * @returns {string}
 * @depreacted method is deprecated
 */
function getDataType(array) {
    let dtype = '';
    $.each(array, function (key, value) {
        dtype = value.datatype;
        return false;
    });
    return dtype;
}

/**
 *  remove the selected fields from cio select options
 */
function removeCIOSelectedOptions () {
    const rows = $tableID.find('tr:not(:hidden) >td >select.cio-select option:selected')
    let i = 0;
    // Currently remove the used standard param from the list.
    $.each(rows, function (key, value) {
        delete campaignInOneParams[value.text];
    });
}

function generateCustomFieldsList () {
    let customFields = settings['customFieldList'];
    let customFieldName = {};
    $.each(customFields, function (index, value) {
        let fieldName = value['fieldname'];
        let field_dtype = value['datatypes_name'];
        customFieldName['Contact.' + fieldName] = { 'name': fieldName, 'datatype': field_dtype };
    });
    return customFieldName;
}

/**
 * Returns HTML Label for the fieldnames for mapping MIO and HTML fields.
 * fieldNames is for 'standard','eCommerce' or 'custom'.
 * @param formFieldName
 * @returns {string}
 */
function getHtmlLabelForField (formFieldName) {
    let htmlLabel = '';
    if ($.trim(formFieldName) <= 0) {
        return '';
    }

    if (formFieldName === 'Contact.Email') {
        formFieldName = 'email';
    }

    let standardCustomFields = getCategoryFields('ecommerce_fields');
    //let trackingFields = getCategoryFields('tracking_fields');
    let standardFields = getCategoryFields('standard_fields');
    let customFields = getCategoryFields('customFieldList');


    // check if the field is present in standardCustomFields.
    $.each(standardCustomFields, function (key, value) {
        if (htmlLabel !== '') {
            return false;
        }
        if (value.fieldname === formFieldName) {
            htmlLabel = value.htmllabel;
            return false;
        }
    });

    $.each(standardFields, function (key, value) {
        if (htmlLabel !== '') {
            return false;
        }
        if (value.fieldname === formFieldName) {
            htmlLabel = value.htmllabel;
            return false;
        }
    });

    $.each(customFields, function (key, value) {
        if (htmlLabel !== '') {
            return false;
        }
        let searchValue = value.fieldname.toLowerCase();
        if (searchValue === formFieldName.toLowerCase()) {
            htmlLabel = 'Contact.' + value.fieldname;
            return false;
        }
    });

    return htmlLabel;
}

/**
 * Helper function for generating the Mapping Table for MIO and HTML Fields.
 * @param structure
 * @returns fieldName of type string
 * @deprecated
 */
function parseFieldName (structure) {
    let fieldName = '';
    let keys = Object.keys(structure);
    $.each(keys, function (key, value) {
        if ($.inArray(value, ['required','datatype', 'regex']) === -1) {
            fieldName = value;
            return false;
        }
    });
    return fieldName;
}

/**
 * Generates the Mapping tab, by considering the mappingValue from DB,
 * @param {string} mappingValue
 * @author AKi
 * @comment takes mappings string and appends mapping table
 */
function generateMappingTable (mappingValue) {
    //decode mapping
    mapping = JSON.parse(atob(mappingValue))['contact'];
    $.each(mapping, function (index, element) {
        $.each(element, function (key, value) {
            let fieldValue = (value['field_name'] !== undefined) ? value['field_name'] : parseFieldName(value);
            let CIO = getHtmlLabelForField(fieldValue);
            let dataType = value['datatype'];
            let required =  (value['required']) ? "yes" : "No";
            //todo:use html template to store html templates rather than using strings
            const newTrTest = `
        <tr class="">
                                        <td class="pt-3-half CIO" contenteditable="false" data-value=` + fieldValue + ` >` + CIO + `</td>
                                        <td class="pt-3-half DataType" contenteditable="false"> ` + dataType + `</td>
                                        <td class="pt-3-half required"  contenteditable="false">` + required + `</td>
                                        <td>
                                          <span class="table-remove"><i style="color:#ff0000;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                        </td>
                                    </tr>

        `;
            //appending html to
            $('tbody').append(newTrTest);
        });
    });
}

//########################### End: Mapping MIO and HTML Wizard #########################//

//Initial event
$BTN.prop('disabled', true);
//checks for the campaign name

if (campaignName === 'undefined' || $(location).attr('href').includes('copy')) {
    jQuery('input#name1').on('mouseout change blur', function () {
        let myurl = jQuery(this).data('urlprefix') + jQuery(this).val() + '/' + jQuery(this).data('getparams');
        let settings = {
            url: myurl,
            method: "GET",
            timeout: 0,
            headers: {
                'Content-Type': "application/x-www-form-urlencoded"
            }
        };
        $.ajax(settings).done(function (response) {
            let check = JSON.parse(response);
            if (check.status === false) {
                $('input#name1').removeClass('is-valid').addClass('is-invalid');
                let message = check['message'];
                let html = `<font color="red">` + message + `</font>`;
                $('#campaignName').html(html);
            } else {
                $('input#name1').removeClass('is-invalid').addClass('is-valid');
                let message = check['message'];
                let html = `<font color="green">` + message + `</font>`;
                $('#campaignName').html(html);
                $BTN.prop('disabled', false);
            }
        });

        //generate New Pageflow
        let pageName = jQuery(this).val();
        let pageFlowFormattedSettings = formatePageSettings(pageFlowSettings, pageName);
        flowPage(pageFlowFormattedSettings);//build page flow wizard
    });
} else {
    $('.btn-success').text("Update settings");
    $BTN.prop('disabled', false);
    let pageFlowFormattedSettings = formatePageSettings(pageFlowSettings, campaignName);
    flowPage(pageFlowFormattedSettings);//build page flow wizard
    jQuery('input#name1').prop('readonly', true);
    jQuery('input#name1').on('mouseout change blur', function () {
        let message = "Page name can not be changed";
        let html = `<font color="green">` + message + `</font>`;
        $('#campaignName').html(html);
    });
}

$pageFlowWizard.on('click', '.trash-main', function () {
    let wrapper = $(this).closest('li');
    wrapper.remove();
});

//clear date

$("#clearDate").click(function () {
    $('#kt_datetimepicker_3').val('').datepicker('update');
});
$("#clearDate2").click(function () {
    $('#kt_datetimepicker_2').val('').datepicker('update');
});


//SplitTest code
// For expiration criteria == date
$('#menu-flowcontrol').addClass("show");
$('#set-splittest').addClass("kt-widget__item--active");
$("#datetimepicker").datetimepicker({format: 'y-m-d h:i'});


//Split test activation status.
if ($('.show-splittest-settings').prop("checked") === true) {
    $(".settings-splittest").show();
    $(".child-row-splitest").show();
} else {
    $(".settings-splittest").hide();
    $(".child-row-splitest").hide();
}

$(".show-splittest-settings").click(function () {
    $(".settings-splittest").toggle();
});

$(".show-splittest").click(function () {
    $(".child-row-splitest").toggle("slow", function () {
    });
});

//SP expiration criteria
$("#button-visitors").click(function () {
    $("#label-button-visitors").toggleClass('active');
    $("#label-button-conversions").removeClass('active');
    $("#label-button-date").removeClass('active');
    $("#value-conversions").removeClass('show');
    $("#value-date").removeClass('show');
});

$("#button-conversions").click(function () {
    $("#label-button-conversions").toggleClass('active');
    $("#label-button-visitors").removeClass('active');
    $("#label-button-date").removeClass('active');
    $("#value-visitors").removeClass('show');
    $("#value-date").removeClass('show');
});

$("#button-date").click(function () {
    $("#label-button-date").toggleClass('active');
    $("#label-button-conversions").removeClass('active');
    $("#label-button-visitors").removeClass('active');
    $("#value-visitors").removeClass('show');
    $("#value-conversions").removeClass('show');
});

//Dynamically generate tr based on count on variants.
$('select#total_variants').on('change', function () {

    let diff;
    const select_variant_count = $(this).val();
    let tr = '';

    const current_rows = $("table tbody#split-with-variants tr").length;

    if (current_rows > 0) {
        if (select_variant_count > current_rows) {
            tr = getVariantTableRows(current_rows + 1, select_variant_count);
            // append new rows to table.
            $("table tbody#split-with-variants").append(tr);
        } else {
            diff = select_variant_count - current_rows;
            $("table tbody#split-with-variants tr").slice(diff).remove();
        }
    } else {
        // remove existing rows.
        $("table tbody#split-variant > tr").remove();
        tr = getVariantTableRows(1, select_variant_count);
        // append new rows to table.
        $("table tbody#split-variant").append(tr);
    }
    $('.splittestTable').show();
});

// Generate table rows based on given count.
function getVariantTableRows(row_index, max_row_count) {
    let tr;
    let variant_id;
    let variant_name;
    for (let i = row_index; i <= max_row_count; i++) {

        variant_id = 'campaign[splittest][variants][' + i + '][variant_id]';
        variant_name = 'campaign[splittest][variants][' + i + '][variant_name]';

        tr += '<tr>' +
            '<th class="align-middle" scope="row">\n' +
            '<input type="text" class="form-control"\n' +
            ' name="' + variant_id + '"\n' +
            ' autocomplete="off"\n' +
            ' value="' + i + '" readonly>\n' +
            ' </th>\n' +
            ' <td class="align-middle">\n' +
            ' <input type="text" class="form-control"\n' +
            ' name="' + variant_name + '"\n' +
            ' autocomplete="off"\n' +
            ' placeholder="Please Name Splittest Variant"\n' +
            ' value="">\n' +
            ' </td>' +
            '</tr>';
    }
    return tr;
}

//Generate page flow with data
function flowPage(data) {
    let flowControlSettings = btoa(JSON.stringify(data));
    $('#pageFlow').html('');
    $.ajax({
        type: 'POST',
        url: "createFlow",
        data: {json: flowControlSettings},
        success: function (response) {
            $('#pageFlow').html(response);
        }
    });
    return true;
}


/**
 * @param {*} data
 * @param {jQuery|string} pageName
 * @author Aki
 */
function formatePageSettings(data, pageName) {
    let campaignUrl = 'https://treaction.marketing-in-one.net/' + companyName + '/' + pageName;
    data['campaign']['url'] = campaignUrl;
    return data;
}

function addPage() {
    const flowControlPages = $('#flowControlPages');
    let pageName = $('input.pagetext').val();
    let flowLength = flowControlPages.children().length;
    let html = $('#pageFlowList1').html()
        .toString()
        .replaceAll("_1", ("_" + (flowLength + 1)))
        .replaceAll("#1", ("#" + (flowLength + 1)))
        .replaceAll("index.html", (pageName + ".html"))
        .replaceAll("campaign[page_flow][custom_pages][1][name]", "campaign[page_flow][custom_pages][" + (flowLength + 1) + "][name]");
    let appendHtml = `
    <li style="list-style-type: none;" id="pageFlowList` + (flowLength + 1) + `">
    ` + html + `
    </li> 
      `;
    flowControlPages.append(appendHtml);//append page to list
    let addedPageId = "pageFlowList" + (flowLength + 1);
    const addedPage = flowControlPages.find('#' + addedPageId);
    const pageButtonDropdown = addedPage.find('.page_buttons_dropdown');
    //remove all the list
    pageButtonDropdown.find('a.custom-page-buttons').remove();
    $('input.pagetext').val('');//make value empty
    sortable();//init the sortable for page flow
}

function sortable() {
    // flow control drag library
    var group = $("div.sortableElements").sortable({
        group: 'limited_drop_targets',
        isValidTarget: function ($item, container) {
            if ($item.is(".highlight"))
                return true;
            else
                return $item.parent("ol")[0] == container.el[0];
        },
        onDrop: function ($item, container, _super) {
            $('#serialize_output').text(
                group.sortable("serialize").get().join("\n"));
            _super($item, container);
        },
        serialize: function (parent, children, isContainer) {
            return isContainer ? children.join() : parent.text();
        },
        tolerance: 6,
        distance: 10
    });
}


/**
 *
 * @param field
 * @returns {string}
 * @internal Searches the database field value for HTML field value.
 * eg : for given field "email " searches for "Contact.Email" inside settings["standard_fields]"
 */

/*
function getHtmlLabelForField (field) {
    let htmlLabel = ''
    let standardFields = settings['standard_fields']
    $.each(standardFields, function (index, element) {
        console.log('field', field)
        if (field === Object.values(element)[0]) {
            console.log('Found Index', index)
            htmlLabel = index
            return false
        }
    })
    return htmlLabel
}
*/

/**
 * @param (void) generateStandardFieldList
 * @author AKi
 * @comment generates standard field list from input data
 */
function generateStandardFieldList () {
    let list = [];
    let standardFields = getCategoryFields('standard_fields');
    let ecommerceFields = getCategoryFields('ecommerce_fields');
    $.each(standardFields, function (key, value) {
        list.push(value['fieldname']);
    });
    $.each(ecommerceFields, function (key, value) {
        list.push(value['fieldname']);
    });
    return list;
}


/**
 * @param {json} data
 * @return {{}}
 * @author AKi
 * @comment takes mappings string and structures the data
 */
function structureMappingData(data) {
    if (data.length === 0) {
        return data;
    }
    let structuredMappingData = {};
    let standard = [];
    let custom = [];
    let sdFieldsAsArr = generateStandardFieldList();
    data = appendAdditionalFieldsForClient(data);

    $.each(data, function (key, value) {
        let field = value['field_name'];
        if (sdFieldsAsArr.includes(field)) {
            //todo:take the structure dynamically form mapping data type
            standard.push(value);
        } else {
            custom.push(value);
        }
    });
    structuredMappingData["contact"] = {standard, custom};
    return structuredMappingData;
}

/**
 *@return Html text for email row
 * @author AKi
 * @comment makes email tr for mapping table
 */
function generateEmailTr() {
    //todo:inject this fields from backend
    let emailField = "Contact.Email";
    let dataTypeHtml = "Email";
    let requiredHtml = 'Yes';
    let fieldValue = 'email';
    //remove recursion for of the Email
    delete campaignInOneParams["Contact.Email"];

    return `
                <tr class="defaultTableValue" id="` + 0 + `">
                    <td class="pt-3-half select2 CIO" data-value=` + fieldValue + `>` + emailField + `</td>
                    <td class="pt-3-half select2 DataType" style="position:relative;font-weight: bold">` + dataTypeHtml + `</td>
                    <td class="pt-3-half select2 required" style="position:relative;font-weight: bold">` + requiredHtml + `</td>
                    <td valign="middle">
                        <span class="table-remove-email"><i style="cursor: pointer;" class="fas fa-minus-circle"></i></span>
                    </td>
                </tr>`;
}

/**
 * @event on click final submit
 * @author AKi
 * @comment add values to hidden fields
 * @todo:replace this with ajax calls
 */
$('#finalSubmit').on('click', () => {
    let value = JSON.stringify(workflows);

    let buttonsValue = JSON.stringify(buttonsForProcesshooks);
    //add workflow
    $("#selectedProcesshooks").attr('value', value);
    //add buttons
    $("#processhook_buttons").attr('value', buttonsValue);
});


/**
 * @event on click page dropdown elements (only for view to take data)
 * @author AKi
 * @comment custom buttons for pages
 */
$pageFlowWizard.on('click', '.page_buttons_dropdown a', function () {
    let text = $(this).text();
    const buttonWithText = $(this).parents('.processhook_buttons_dropdown').find('.processhook_configure_button');
    const customButtonText = $(this).parents('.processhook_buttons_dropdown').find('.processhook_add_button');
    if (text === "Add Custom Button") {
        buttonWithText.hide("slide", {direction: "up"}, 150);
        customButtonText.removeAttr('hidden');
        customButtonText.show("slide", {direction: "down"}, 150);
    } else {
        customButtonText.hide("slide", {direction: "up"}, 150);
        buttonWithText.hide("slide", {direction: "up"}, 150);
        buttonWithText.removeAttr('hidden');
        buttonWithText.find('.button_name').text('[Button]' + text);
        buttonWithText.find('input').val(text);
        buttonWithText.show("slide", {direction: "down"}, 150);
    }
});

/**
 * @param {string} newButton
 * @internal checks for duplicate buttons
 * @author AKi
 * @comment add button to page
 */
function checkDuplicateButtons(newButton) {
    if(newButton === 'Submit'){
        return false;
    }
    for (const [key, value] of Object.entries(buttonsForProcesshooks)) {
        if (value.includes(newButton)) {
            return false;
        }
    }
    return true;
}

/**
 * @event on click add new button
 * @author AKi
 * @comment add button to page
 */
$pageFlowWizard.on('click', '.addButton', function () {
    const inputField = $(this).parents('.pageFlowButton').find('input[type=text]');
    let newButton = inputField.val();
    const dropdownMenu = $(this).parents('.processhook_buttons_dropdown').find('.page_buttons_dropdown');
    const customButtonText = $(this).parents('.processhook_buttons_dropdown').find('.processhook_add_button');
    const page = $(this).parents('.form-row').find('input.PageinputValue');
    let pageName = page.val();
    if (newButton && checkDuplicateButtons(newButton)) {
        inputField.css({"border-color": "black", "outline": "none", "box-shadow": "none"});
        //append additional menu
        dropdownMenu.prepend('<a class="dropdown-item custom-page-buttons" id="' + newButton + '" role="button">' + newButton + '</a>');
        processhookDropdownMenu.append('<a class="dropdown-item" id="' + newButton + '" role="button">[' + pageName + ']' + newButton + '</a>')
        //empty input field
        inputField.val('');
        customButtonText.hide("slide", {direction: "up"}, 150);
        if (buttonsForProcesshooks[pageName]) {
            buttonsForProcesshooks[pageName].push(newButton);
        } else {
            buttonsForProcesshooks[pageName] = [newButton];
        }
        inputField.css({"border-color": "black", "outline": "none", "box-shadow": "none"});
    } else {
        inputField.css({"border-color": "red", "outline": "none", "box-shadow": "none"});
        inputField.effect("shake");
    }
});


/**
 * @event on click change the name of the button
 * @author AKi
 * @comment change the name of the button
 */
$pageFlowWizard.on('click', '.changeButton', function () {
    const inputField = $(this).parents('.pageFlowButton').find('input[type=text]');
    let newButtonName = inputField.val();
    const changeMenu = $(this).parents('.processhook_buttons_dropdown').find('.processhook_configure_button');
    const dropdownMenu = $(this).parents('.processhook_buttons_dropdown').find('.page_buttons_dropdown');
    let oldButtonName = changeMenu.find('.button_name').text().replace("[Button]", "");
    const page = $(this).parents('.form-row').find('input.PageinputValue');
    let pageName = page.val();
    if (newButtonName) {
        //find the element and remove it
        dropdownMenu.find('a#' + oldButtonName).remove();
        processhookDropdownMenu.find('a#' + oldButtonName).remove();
        //add new element
        dropdownMenu.prepend('<a class="dropdown-item custom-page-buttons" id="' + newButtonName + '" role="button">' + newButtonName + '</a>');
        processhookDropdownMenu.append('<a class="dropdown-item" id="' + newButtonName + '" role="button">[' + pageName + ']' + newButtonName + '</a>')
        changeMenu.hide("slide", {direction: "up"}, 150);
        if (buttonsForProcesshooks[pageName]) {
            buttonsForProcesshooks[pageName] = removeElementFromArray(buttonsForProcesshooks[pageName], oldButtonName);
            buttonsForProcesshooks[pageName].push(newButtonName);
        } else {
            buttonsForProcesshooks[pageName] = [newButtonName];
        }
    } else {
        inputField.css({"border-color": "red", "outline": "none", "box-shadow": "none"});
        inputField.effect("shake");
    }
});


/**
 * @event on click delete the button
 * @author AKi
 * @comment Delete the button
 */
$pageFlowWizard.on('click', '.deleteButton', function () {
    const changeMenu = $(this).parents('.processhook_buttons_dropdown').find('.processhook_configure_button');
    const dropdownMenu = $(this).parents('.processhook_buttons_dropdown').find('.page_buttons_dropdown');
    let buttonName = changeMenu.find('.button_name').text().replace("[Button]", "");
    const page = $(this).parents('.form-row').find('input.PageinputValue');
    let pageName = page.val();
    if (buttonName === "Submit") {
        $(this).effects("shake");
    } else {
        //find the element and remove it
        dropdownMenu.find('a#' + buttonName).remove();
        processhookDropdownMenu.find('a#' + buttonName).remove();
        //close animation
        changeMenu.hide("slide", {direction: "up"}, 150);
        buttonsForProcesshooks[pageName] = removeElementFromArray(buttonsForProcesshooks[pageName], buttonName);
        //remove all workflow actions configured with this button
        delete workflows[pageName][buttonName];
    }
});

/**
 * @param {string} element
 * @param {array} array
 * @author AKi
 * @comment removes element from the array
 */
function removeElementFromArray(array, element) {
    const index = array.indexOf(element);
    if (index > -1) {
        array.splice(index, 1);
    }
    return array;
}

/**
 * @event on click
 * @author AKi
 * @comment generates and appends the view for the processhook configurator
 */
processhookDropdownMenu.on('click', 'a', function () {
    processhookBuilder.hide("slide", {direction: "down"}, 150);
    //remove all the old processhooks
    definedProcesshooksDiv.find('.process-hook').remove();
    activeProcesshooksDiv.find('.process-hook').remove();
    //get button info
    button = $(this).text();
    let pageName = button.substring(
        button.lastIndexOf("[") + 1,
        button.lastIndexOf("]")
    );
    let buttonName = button.replace('[' + pageName + ']', "");
    let textButtonName = "<strong>" + pageName.replace('.html', "") + "." + buttonName + "</strong>";
    processhookBuilder.find('.buttonName').html(textButtonName);
    let availableProcesshooks = settings['processhooks']['available'];
    //generate configurator for workflow object
    if (workflows[pageName] && workflows[pageName][buttonName]) {
        let definedProcesshooksIds = [];
        let definedProcesshooks = workflows[pageName][buttonName];
        $.each(definedProcesshooks, function (index, processhook) {
            definedProcesshooksIds.push(processhook['userProcesshookId']);
        });
        $.each(availableProcesshooks, function (index, processhook) {
            let processhookId = processhook['user_processhook_id'];
            if (definedProcesshooksIds.includes(processhookId)) {
                let listItem = generateListItemForProcesshookView(processhook);
                activeProcesshooksDiv.append(listItem);
            } else {
                let listItem = generateListItemForProcesshookView(processhook);
                definedProcesshooksDiv.append(listItem);
            }
        });
    } else {
        $.each(availableProcesshooks, function (key, processhook) {
            let listItem = generateListItemForProcesshookView(processhook);
            definedProcesshooksDiv.append(listItem);
        });
    }
    processhookBuilder.removeAttr('hidden');
    processhookBuilder.show("slide", {direction: "up"}, 150);
});


/**
 * @param {array} processhook
 * @author AKi
 * @comment generates list item for processhook configurator
 */
function generateListItemForProcesshookView(processhook) {
    let viewName = processhook['view_name'];
    let name = processhook['name'];
    let userProcesshookId = processhook['user_processhook_id'];
    return '<li class="process-hook" ' +
        'processhookType="' + viewName + '"  ' +
        'processhookName="' + name + '" ' +
        'userProcesshookId="' + userProcesshookId + '">' +
        '<strong>[' + viewName + ']</strong><br>' + name + '</li>';
}

/**
 * @param {string} text
 * @author AKi
 * @comment configures workflow object
 */
function configureWorkflow(text) {
    //extract page name
    let pageName = button.substring(
        button.lastIndexOf("[") + 1,
        button.lastIndexOf("]")
    );
    let buttonName = button.replace('[' + pageName + ']', "");
    let processhooks = [];
    $('#activeProcesshooks li').each(function () {
        let processhookType = $(this)[0].getAttribute('processhookType');
        let processhookName = $(this)[0].getAttribute('processhookName');
        let userProcesshookId = $(this)[0].getAttribute('userProcesshookId');
        let type = {
            'processhookType': processhookType,
            'processhookName': processhookName,
            'userProcesshookId': userProcesshookId
        };
        processhooks.push(type);
    });
    //update or create workflow for button
    if (typeof (workflows[pageName]) !== 'undefined') {
        workflows[pageName][buttonName] = processhooks;
    } else {
        workflows[pageName] = {
            [buttonName]: processhooks
        };
    }

}

/**
 * @param {array} buttonsForProcesshooks
 * @author AKi
 * @comment initial list for processhook configurator
 */
function addInitialButtonsForProcesshookConfigurator(buttonsForProcesshooks) {
    $.each(buttonsForProcesshooks, function (pageName, buttons) {
        $.each(buttons, function (index, button) {
            processhookDropdownMenu.append('<a class="dropdown-item" id="' + button + '" role="button">[' + pageName + ']' + button + '</a>');
        });
    });

}

/**
 * @param {string} string
 * @comment some handy function, act as replacement ucfirst in php
 */
function capitalizeFirstLetter(string) {
    return string[0].toUpperCase() + string.slice(1);
}

/**
 * @param {string} string
 * @comment some handy function, act as replacement Icfirst in php
 */
function lowerizeFirstLetter(string) {
    return string[0].toLowerCase() + string.slice(1);
}


/**
 * @param {array} data
 * @return {array}
 * @internal Append the fields required by the client(baby) to handle form in index.html
 * @author Aki
 */
function appendAdditionalFieldsForClient(data) {
    //init the structured data
    let structuredData = [];
    //All the marketing-in-one fields
    let marketingInOneFields = settings['customFieldList'].concat(settings['standard_fields'],
                                                                    settings['ecommerce_fields']);


    // Add the additional fields required to selected fields
    $.each(data, function (key, value) {
        $.each(marketingInOneFields, function (index, fieldData) {
            if(value['marketing-in-one'] === fieldData['fieldname']){
                //data field is the single instance object of the data
                let dataField = {};
                //required fields for mapping table in MIO
                dataField['field_name'] = fieldData['fieldname'];
                dataField['marketing-in-one'] = fieldData['fieldname'];
                dataField[fieldData['fieldname']] = "";
                //additional fields required to handle client
                if(fieldData.hasOwnProperty('fieldtype')){
                    //if fieldtype property exits this field belongs to standard fields or ecommerce field
                    dataField['datatype'] =fieldData['dtype_label'];
                    //Email field is always required , All other standard fields are not required
                    dataField['required'] = (fieldData['fieldname'] === 'email') ?  1 : 0 ;
                    //Other HTML required fields
                    dataField['html_label'] = fieldData['dtype_label'];
                    dataField['html_placeholder'] = fieldData['html_placeholder'];
                    dataField['html_hidden_field'] = parseInt(fieldData['hidden_field']);//convert to int
                    dataField['field_type'] = fieldData['fieldtype'];
                    dataField['php_type'] = fieldData['dtype_name'];
                }else{
                    //This field belongs to custom field
                    dataField['datatype'] =fieldData['datatypes_name'];
                    dataField['required'] = parseInt(fieldData['required']);//convert to int
                    dataField['html_label'] = fieldData['fieldname'];
                    dataField['html_placeholder'] = fieldData['fieldname'];
                    dataField['html_hidden_field'] = 0;//always custom fields are shown in client UI
                    dataField['field_type'] = 'custom';
                    dataField['php_type'] = fieldData['php_type'];
                }
                dataField['regex'] = fieldData['regex'];
                //push the instance to structured data
                structuredData.push(dataField);
            }
        });
    });
    //return the structured data
    return structuredData;
}

