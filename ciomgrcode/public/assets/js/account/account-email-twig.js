jQuery('button#email_twig_test').on( 'click', function(){
    // Todo: feddisch implementieren WEB-3737!
    $(this).css({
        'background-color' : 'green'
    });
    $(this).text('Wait');
    var email = {
        'smtpServer': $('input#email_host').val(),
        'smtpPort': $('input#email_port').val(),
        'senderEmailAddress': $('input#email_user').val(),
        'senderPassword': $('input#email_pw').val(),
        'recipientEmailList': $('input#email_receiver').val(),
        'messageSubject': 'SMTP configuration test-email by Marketing-In-one -Manager',
        'firstname': $('div#username').data('firstname'),
        'lastname': $('div#username').data('lastname'),
        // 'messageBody': 'Ihre Konfiguration funktioniert!',
        'messageBody': '<!DOCTYPE html>\n' +
            '<html lang="de">\n' +
            '<head>\n' +
            '    <meta charset="UTF-8">\n' +
            '    <title>Email Server Configuration Successfull</title>\n' +
            '</head>\n' +
            '<body>\n' +
            '<h3><b>Success!</b></h3>\n' +
            '<p>This email was sent from the email server just configured.</p>\n' +
            '</body>\n' +
            '</html>'
    };

    // console.log("email object =");
    // console.debug(email);
    // console.log('url=' + $('input#email_receiver').data('checktarget'));
    // console.log('url=' + atob($('input#email_receiver').data('checktarget')));

    $send_data = btoa(JSON.stringify(email));

    var settings = {
        url: atob($('input#email_receiver').data('checktarget')),
        method: "POST",
        timeout: 0,
        headers: {
            'Content-Type': "application/x-www-form-urlencoded"
        },
        data: $send_data,
        beforeSend: function (xhr) {
            $('i#test_indicator').removeClass('text-secondary').removeClass('text-danger').addClass('text-warning');
        },
        success: function (data) {
            //console.log(data);
            if (data[0].status === true) {
                console.log('success => data:');
                console.debug(data);
                $('i#test_indicator').removeClass('text-warning').addClass('text-success');
                $('div#email_smtp_msg_success').show();
                $('button#email_twig_test').attr('disabled', true);
            } else {
                console.log('failed => data:');
                console.debug(data);
                $('i#test_indicator').removeClass('text-warning').addClass('text-danger');
                $('button#email_twig_test').attr('disabled', false);
                if (data.message !== undefined && data.message !== '') {
                    $('div#email_smtp_msg_error').show();
                }
            }
        }
    };

    $.ajax(settings)
        .done(function (data) {
            console.log('DONE');
            console.debug(data);

        })
        .fail(function (data, textStatus, errorThrown) {
            console.log("request failed");
            console.debug('d:'+data.toString());
            console.debug('s:'+textStatus);
            $(this).text('Done');
            $('i#test_indicator').removeClass('text-warning').addClass('text-danger');
            $('div#email_smtp_msg').text('').text('Test failed. Check configuration or SMTP Server').removeClass('hidden').fadeOut(6000).addClass('hidden');
            $('button#email_twig_test').attr('disabled', false);
        });
    $(this).text('Done');

});
