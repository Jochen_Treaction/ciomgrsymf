/**
 *
 * @internal This document handles inbox monitor user config tests
 * @author Aki
 */

//Global variables
const errorDiv = $ ( 'div#msg_error' );
const successDiv = $ ( 'div#msg_success' );

// on page load functions
$(document).ready(function () {
    //Hide the message
    errorDiv.hide ();
    successDiv.hide ();
});

/**
 * @internal - get data form API
 * @param {any}payload
 * @return void
 */
function testConnection( payload){
    //Get the data form mosento
    $.ajax({
        "url": "/inboxmonitor/apicheck",
        "method": "POST",
        "async":"false",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": payload,
        success: function (data) {
            let seedPoolData = JSON.parse(data).data;
            console.log(seedPoolData);
            if (seedPoolData.success === true) {
                errorDiv.hide();
                successDiv.show();
            } else {
                //show message on table
                let errorMessage = seedPoolData.error_message;
                successDiv.hide();
                errorDiv.text('We were not able to establish a connection. Error: '+ errorMessage);
                errorDiv.show();
            }
        },
        error:function (){
            successDiv.hide();
            errorDiv.text('We were not able to establish a connection. Error: Communication Error MIO  ');
            errorDiv.show();
        }
    });
}

/**
 * @internal - test the config
 */
$('#config_test').click(function () {
    //Event runner stops multiple clicks until the job is processed
    let eventRunning;
    if(!eventRunning){
        eventRunning = true;
        //event runner stops multiple events running
        let form = new FormData();
        form.append("apikey", $('#inbox-monitor-apikey').val());
        form.append("user_id", $('#inbox-monitor-user').val());
        //test the connection
        testConnection(form);
        //reset the event
        eventRunning = false;
    }
});

