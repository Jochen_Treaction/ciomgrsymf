/**
 *
 * @internal This document handles mapping table for the seed pool assignment
 * @author Aki
 */
//Global variables
const $BTN = $('#saveData');
const $Table = $('#mappingTable');
const seedPoolsTypes = seedPoolData['allAvailableSeedPoolTypes'];
let seedPools = seedPoolData['allAvailableSeedPools'];
let seedPoolsGroups = seedPoolData['allAvailableSeedPoolGroups'];

// on page load functions
$(document).ready(function () {
    let seedPoolMappingForAccount = seedPoolData['seedPoolMappingForAccount'];
    //Generate mapping table
    generateMappingTable(seedPoolMappingForAccount);
});

/**
 * Generates the Mapping table,
 * @param {array} seedPoolMappingForAccount
 * @comment takes mapping array and generates and appends mapping table
 */
function generateMappingTable(seedPoolMappingForAccount) {

    $.each(seedPoolMappingForAccount, function (key, value) {
        let seedPoolName = value['name'];
        let group = value['group'];
        let type = value['type'];
        let id = value['id'];
        //todo:use html template to store html templates rather than using strings
        const newTrTest = `
        <tr class="">
                                        <td class="pt-3-half name" contenteditable="false" data-value-id=` + id + ` >` + seedPoolName + `</td>
                                        <td class="pt-3-half type" contenteditable="false"> ` + type + `</td>
                                        <td class="pt-3-half group"  contenteditable="false">` + group + `</td>
                                        <td>
                                          <span class="table-remove"><i style="color:#ff0000;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                        </td>
                                    </tr>

        `;
        //appending html to
        $('tbody').append(newTrTest);
    });
}

/**
 * @internal Helper function returns the length of the table.
 * @returns {jQuery}
 */
function getTableLength() {
    return $('.table tr').length;
}

/**
 * @internal Generates table row for Mapping MIO and HtmlFields.
 * @returns {string}
 */
function generateHTML() {

    //Get the length of the table
    let i = getTableLength();
    //generate select statements
    let seedPoolSelectHtml = generateSelectHtmlForSeedPool();
    let seedPoolTypeSelectHtml = generateSelectHtmlForType();
    let seedPoolGroupSelectHtml = generateSelectHtmlForGroup();


    return `
                <tr class="defaultTableValue tableBodyRow" id="table_row_` + i + `">
                    <td class="pt-3-half name ">` + seedPoolSelectHtml + `</td>
                    <td class="pt-3-half type " style="position:relative;font-weight: bold">` + seedPoolTypeSelectHtml + `</td>
                    <td class="pt-3-half group " style="position:relative;font-weight: bold">` + seedPoolGroupSelectHtml + `</td>
                    <td valign="middle">
                        <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                    </td>
                </tr>`;
}

/**
 * @internal Generates select html for seed-pool
 * @returns html{string}
 */
function generateSelectHtmlForSeedPool() {
    let optionHtml = '';
    // option fields for standard fields.
    $.each(seedPools, function (key, value) {
        let htmlOption = `<option value="` + value['name'] + `" data-seedPoolId="` + value['id'] + `" data-seedPoolKey="` + key + `" >` + value['name'] + `</option>`;
        optionHtml += htmlOption;
    });

    let HTML = ` <select  class="seed-pool-name form-control form-control-md" style="opacity:1">
                   ` + optionHtml + `
                  </select> `;
    return HTML;

}

/**
 * @internal Generates select html for seed-pool-type
 * @returns html{string}
 */
function generateSelectHtmlForType() {
    let optionHtml = '';
    // option fields for standard fields.
    $.each(seedPoolsTypes, function (key, value) {
        let htmlOption = `<option value="` + value + `"  >` + value + `</option>`;
        optionHtml += htmlOption;
    });

    let HTML = ` <select  class="seed-pool-type form-control form-control-md" style="opacity:1">
                   ` + optionHtml + `
                  </select> `;
    return HTML;

}

/**
 * @internal Generates select html for seed-pool-group
 * @returns html{string}
 */
function generateSelectHtmlForGroup() {
    let optionHtml = '';
    // option fields for standard fields.
    $.each(seedPoolsGroups, function (key, value) {
        let htmlOption = `<option value="` + value['name'] + `" data-seedPoolId="` + value['id'] + `" >` + value['name'] + `</option>`;
        optionHtml += htmlOption;
    });

    let HTML = ` <select  class="seed-pool-group form-control form-control-md" style="opacity:1;">
                   ` + optionHtml + `
                  </select> `;
    return HTML;

}

/**
 * @internal delete the seed pool form the seedPools Object thus next row will not have the selected seed pool
 * @returns void
 */
function deleteSeedPool() {
    //Get the data form mapping table
    const $rows = $Table.find('tr:not(:hidden)').toArray();
    if($rows.length-1 > 0){
        let $row = $rows[$rows.length-1];
        //Get the selected value
        let seedPoolKey = $('.name', $row).find('option:selected').attr('data-seedPoolKey');
        delete seedPools[seedPoolKey];
    }
}

/**
 * @param {string} seedPoolName
 * @param {string} seedPoolId
 * @param {string} seedPoolKey
 * @internal add the seed pool form to seedPools Object thus next row will  have the option to select the seed pool
 * @returns void
 */
function addSeedPool(seedPoolName,seedPoolId,seedPoolKey) {
    seedPools[seedPoolKey] = {
        'id': seedPoolId,
        'name': seedPoolName
    };
}

function initSelect2(){
    $(".seed-pool-group").select2({
        tags: true
    });
    $('.select2-selection__arrow').hide();
}


/**
 * @internal - get data form the UI table
 * @return array
 */
function getDataFromTable() {
    //Get the data form mapping table
    const $rows = $Table.find('tr:not(:hidden)').toArray();
    const headers = [];
    const data = [];

    // Get the headers (add special header logic here)
    $($rows.shift()).find('th:not(:empty)').each(function () {
        headers.push($(this).text().toLowerCase());
    });

    // Turn all existing rows into a loopable array
    $rows.forEach(function (row) {
        const h = {};

        // Use the headers from earlier to name our hash keys
        headers.forEach((header, i) => {
            i = 1;

            switch (header) {
                case "seed pool":
                    //Get the selected value
                    let seedPoolVal = $('.name', row).find('option:selected').val();
                    //get already defined value
                    if (typeof seedPoolVal === 'undefined') {
                        seedPoolVal = $('.name', row).text();
                        h['seed_pool_id'] = $('.name', row).attr('data-value-id');
                    }
                    h["seed_pool"] = seedPoolVal;
                    break;

                case "type":
                    let dataType = $(".type", row).find('option:selected').val();
                    if (typeof dataType === 'undefined') {
                        dataType = $(".type", row).text();
                    }
                    h["type"] = dataType;
                    break;

                case "group":
                    let requiredText = $(".group", row).find('option:selected').val();
                    if (typeof requiredText === 'undefined') {
                        requiredText = $(".group", row).text();
                    }
                    h["group"] = requiredText;
                    break;

            }

        });
        data.push(h);
    });
    return data;
}

/**
 * @internal - get data form the UI table
 * @param {string} seedPoolId
 * @return boolean
 */
function deleteSeedPoolInDatabase(seedPoolId){
    let form = new FormData();
    form.append("seed_pool_id", seedPoolId);
    //send delete request to backend
    $.ajax({
        url: path + seedPoolId,
        type: 'GET',
        async: false,
        success: function (data) {
            if (data.status === true) {
                return true
            } else {
                return false
            }
        }
    });
    return false;
}

/*
* ********************   Jquery Events  *******************************************************************************
*/

/**
 * @internal - on click of the save button - save the mapping data as base64 encoded string
 */
$BTN.on('click', () => {
    let mappingData = getDataFromTable();
    //append the mapping data to form
    $('#seedPoolMappingTable').attr('value', JSON.stringify(mappingData));

});

/**
 * @internal - on click of plus button- Generate the html for table row and append it
 */
$('.table-add').on('click', 'a', () => {
    //Generate the html
    let newTr = generateHTML();
    //append html to table body
    $('tbody').append(newTr);
    //init select2
    initSelect2();
    //delete seed  pool
    deleteSeedPool();

});

/**
 * @internal - on click of minus button- remove the table row
 */
$Table.on('click', '.table-remove', function () {
    const tr = ($(this).parents('tr'));
    //Get the parameters from the removed row
    let seedPoolSelect = tr.find('td.name > select.seed-pool-name option:selected');
    let seedPoolName = seedPoolSelect.val();
    let seedPoolId = seedPoolSelect.attr('data-seedPoolId');
    let seedPoolKey = seedPoolSelect.attr('data-seedPoolKey');
    if(!seedPoolName){
         seedPoolSelect = tr.find('.name');
         seedPoolName = seedPoolSelect.text();
         seedPoolId = seedPoolSelect.attr('data-value-id');
    }
    //If seed pool id exists delete the seed pool from the database
    if(seedPoolId && !seedPoolId.includes('#')){
        deleteSeedPoolInDatabase(seedPoolId);
    }
    if(seedPoolKey){
        //add the seed pool
        addSeedPool(seedPoolName,seedPoolId,seedPoolKey);
    }
    //remove the row from the table
    $(this).parents('tr').detach();
});


