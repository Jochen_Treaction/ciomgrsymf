/**
 *
 * @internal This document handles inbox monitor user config tests
 * @author Aki
 */

//Global variables
const errorDiv = $ ( 'div#msg_error' );
const successDiv = $ ( 'div#msg_success' );

// on page load functions
$(document).ready(function () {
    //Hide the message
    errorDiv.hide ();
    successDiv.hide ();
});

/**
 * @internal - get data form API
 * @param {any}payload
 * @return void
 */
function testConnection( payload){
    //Get the data form mosento
    $.ajax({
        "url": "/advertorial/ftpcheck",
        "method": "POST",
        "async":"false",
        "timeout": 0,
        "processData": false,
        "mimeType": "multipart/form-data",
        "contentType": false,
        "data": payload,
        success: function (data) {
            let statusMessage = JSON.parse(data).data;
            console.log(statusMessage);
            if (statusMessage.success === true) {
                errorDiv.hide();
                successDiv.show();
            } else {
                //show message on table
                let errorMessage = statusMessage.error_message;
                successDiv.hide();
                errorDiv.text('We were not able to establish a connection. Error: '+ errorMessage);
                errorDiv.show();
            }
        },
        error:function (){
            successDiv.hide();
            errorDiv.text('We were not able to establish a connection. Error: Communication Error MIO  ');
            errorDiv.show();
        }
    });
}

/* ****************** Jquery Event based functions *********************** */

/**
 * @internal - test the config
 */
$('#config_test').click(function () {
    //Event runner stops multiple clicks until the job is processed
    let eventRunning;
    if(!eventRunning){
        eventRunning = true;
        //event runner stops multiple events running
        let form = new FormData();
        form.append("ip_address", $('#ip_address').val());
        form.append("sftp_user", $('#sftp_user').val());
        form.append("sftp_password", $('#sftp_password').val());
        //test the connection
        testConnection(form);
        //reset the event
        eventRunning = false;
    }
});

//toggle to show the password
$(".toggle-password").click(function() {
    $(this).toggleClass("fa-eye fa-eye-slash");
    let input = $($(this).attr("toggle"));
    if (input.attr("type") === "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});