$ ( ".child-row" ).hide ();
$ ( ".showhr" ).click ( function () {
    $ ( ".child-row-splitest" ).hide ();
    let portlet_body = $ ( this ).closest ( '.kt-portlet__body' );
        portlet_body.find ( ".child-row" ).toggle ( "slow", function () {
    } );
} );


$ ( ".child-row-splitest" ).hide ();
$ ( ".show-splittest" ).click ( function () {
    let wrapper = $ ( this ).closest ( '.campaign-list-wrapper' );
    wrapper.find ( ".child-row-splitest" ).toggle ( "slow", function () {
    } );
} );

////// Fraud settings for createcampaign.twig and fraudsettings.twig //////

var fraud_level = ( $ ( 'input[name=level]:checked' ).val () );
// Default fraud level
if ( fraud_level === 'low' ) {
    changeFraudLevelLow();
} else if ( fraud_level === 'medium' ) {
    changeFraudLevelMedium();
} else if ( fraud_level === 'high' ) {
    changeFraudLevelHigh();
}

// Set fraud level low
$('input#fraud-level-low').on('change',  function () {
console.log("fraud level low");
    changeFraudLevelLow();
});

// set fraud level medium
$('input#fraud-level-medium').on('change',  function () {
    changeFraudLevelMedium();
});

//set fraud level high
$('input#fraud-level-high').on('change',  function () {
    changeFraudLevelHigh();
});

// Change fraud level to Low
function changeFraudLevelLow()
{
    if($('#fraud-level-low').prop("checked", true)){

        unsetFraudLevelSetting();
        setFraudLevelLow();
    }else {
        unsetFraudLevelSetting();
    }
}

// Change Fraud Level to Medium
function changeFraudLevelMedium()
{
    if($('#fraud-level-medium').prop("checked", true)){
        unsetFraudLevelSetting();
        setFraudLevelMedium();
    }else {
        unsetFraudLevelSetting();
    }
}

// Change fraud Level to High.
function changeFraudLevelHigh()
{
    if($('#fraud-level-high').prop("checked", true)){
        unsetFraudLevelSetting();
        setFraudLevelHigh();
    }else {
        unsetFraudLevelSetting();
    }
}

// helper function fraud level low
function setFraudLevelLow()
{
    console.log('low fraud level settings');
    $('input#max_leads_per_ip').val("10");
    $('input#max_leads_per_domain').val("10");
    $('input#chk_white_listed_domains').prop('checked', false);
}
// helper function fraud level medium
function setFraudLevelMedium()
{
    $('input#max_leads_per_ip').val("5");
    $('input#max_leads_per_domain').val("5");
    $('input#cut_copy_paste').prop('checked', true);
    $('input#unique_lead_registration').prop('checked', true);
    $('input#chk_white_listed_domains').prop('checked', true);
}
// helper function fraud level high
function setFraudLevelHigh()
{
    $('input#max_leads_per_ip').val("3");
    $('input#max_leads_per_domain').val("3");
    $('input#cut_copy_paste').prop('checked', true);
    $('input#unique_lead_registration').prop('checked', true);
    $('input#chk_white_listed_domains').prop('checked', true);
}
// helper function remove fraud level setting.
function unsetFraudLevelSetting()
{
    $('input#max_leads_per_ip').val("");
    $('input#max_leads_per_domain').val("");
    $('input#cut_copy_paste').prop('checked', false);
    $('input#unique_lead_registration').prop('checked', false);
    $('input#chk_white_listed_domains').prop('checked', false);
}

////// end of Fraudsettings  //////