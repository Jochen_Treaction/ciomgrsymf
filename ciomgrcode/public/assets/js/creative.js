/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    // Add custom buttons
    var dataTableButtons = '<div class="dataTables_buttons hidden-sm-down actions">' +
            //'<span class="actions__item zmdi zmdi-print" data-table-action="print" />' +
            '<span class="actions__item zmdi zmdi-fullscreen" data-table-action="fullscreen" />' +
            '<div class="dropdown actions__item">' +
            '<i data-toggle="dropdown" class="zmdi zmdi-download" />' +
            '<ul class="dropdown-menu dropdown-menu-right">' +
            '<a href="" class="dropdown-item" data-table-action="excel">Excel (.xlsx)</a>' +
            '<a href="" class="dropdown-item" data-table-action="csv">CSV (.csv)</a>' +
            '</ul>' +
            '</div>' +
            '</div>';

    var lead_table = $('#lead-table').DataTable({
        autoWidth: false,
        responsive: true,
        lengthMenu: [[10, 20, 50, 100, -1], ['10 Rows', '20 Rows', '50 Rows', '100 Rows', 'Everything']], //Length select
        language: {
            searchPlaceholder: "Search for records..." // Search placeholder
        },
        dom: 'Blfrtip',

        buttons: [// Data table buttons for export and print
            {
                extend: 'excelHtml5',
                title: 'Export Data'
            },
            {
                extend: 'csvHtml5',
                title: 'Export Data'
            }
        ],
//        scrollY:        '200vh',
//        scrollCollapse: true,        

        initComplete: function () {
            $(this).closest('.dataTables_wrapper').prepend(dataTableButtons); // Add custom button (fullscreen, print and export)
        }
    });

    $('#campaign-table').DataTable({
        autoWidth: false,
        responsive: true,
        lengthMenu: [[5, 10, 15, -1], ['5 Rows', '10 Rows', '15 Rows', 'Everything']], //Length select
        language: {
            searchPlaceholder: "Search for records..." // Search placeholder
        },
        dom: 'Blfrtip',
        buttons: [// Data table buttons for export and print
            {
                extend: 'excelHtml5',
                title: 'Export Data'
            },
            {
                extend: 'csvHtml5',
                title: 'Export Data'
            },
            {
                extend: 'print',
                title: 'Material Admin'
            }
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                    );

                            column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                        });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });

    // Data table button actions
    $('body').on('click', '[data-table-action]', function (e) {
        e.preventDefault();

        var exportFormat = $(this).data('table-action');

        if (exportFormat === 'excel') {
            $(this).closest('.dataTables_wrapper').find('.buttons-excel').trigger('click');
        }
        if (exportFormat === 'csv') {
            $(this).closest('.dataTables_wrapper').find('.buttons-csv').trigger('click');
        }
//        if (exportFormat === 'print') {
//            $(this).closest('.dataTables_wrapper').find('.buttons-print').trigger('click');
//        }
        if (exportFormat === 'fullscreen') {
            var parentCard = $(this).closest('.card');

            if (parentCard.hasClass('card--fullscreen')) {
                parentCard.removeClass('card--fullscreen');
                $('body').removeClass('data-table-toggled');
            } else {
                parentCard.addClass('card--fullscreen')
                $('body').addClass('data-table-toggled');
            }
        }
    });



    $('#select_campaign').on('keyup', function () {
        lead_table
                .search(this.value)
                .draw();
    });


    $('input[name="datetimes"]').daterangepicker({
        timePicker: false,
        autoApply: false,
        autoUpdateInput: false,

        locale: {
            format: 'Y-MM-D',
            cancelLabel: 'Clear'
        }
    });

    $('select.select2').select2({
        dropdownAutoWidth: true,
        width: '150px',
    });

    /*******************************************************************************
     * 
     * Jquery for Lead tracking 
     * 
     *******************************************************************************/
    /*
     * @jQuery
     * Clear the date field from the button
     */
    $('button.cancelBtn.btn.btn-sm.btn-default').click(function () {

        var current_url = $(location).attr('href');
        var url_params = current_url.split("/");
        // Base URL
        var base_url = url_params[0] + "//" + url_params[2] + "/account/" + url_values(current_url)['account'];

        // Check for campaign id 
        if ($.inArray("campaigns", url_params) != "-1") {
            base_url += "/campaigns/" + url_values(current_url)['campaigns'];
        }
        // Check for status of leads
        if ($.inArray("status", url_params) != "-1") {
            base_url += "/status/" + url_values(current_url)['status'];
        }
        // Dont append the filtered dates
        base_url += "/leads";
        // redirect to the base url
        $(location).attr('href', base_url);
    });

    /*
     * Filter the All the leads by given date range and other filter data
     */
    $('button.applyBtn.btn.btn-sm.btn-primary').click(function () {

        var range = $("span.drp-selected").html();
        range = range.split(' - ');
        var from_date = $.trim(range[0]);  // Get From date
        var to_date = $.trim(range[1]);    // Get To date
        var current_url = $(location).attr('href');
        var url_params = current_url.split("/");
        // Base URL
        var base_url = url_params[0] + "//" + url_params[2] + "/account/" + url_values(current_url)['account'];
        // Check for campaign id 
        if ($.inArray("campaigns", url_params) != "-1") {
            base_url += "/campaigns/" + url_values(current_url)['campaigns'];
        }
        // Check for status of leads
        if ($.inArray("status", url_params) != "-1") {
            base_url += "/status/" + url_values(current_url)['status'];
        }
        // Append the filtered dates
        base_url += "/from/" + from_date + "/till/" + to_date + "/leads";
        // redirect to the base url
        $(location).attr('href', base_url);
    });


    $('#select-lead-campaign').on('select2:select', function (e) {

        var campaign = e.params.data;
        var current_url = $(location).attr('href');
        var url_params = current_url.split("/");
        // Base URL
        var base_url = url_params[0] + "//" + url_params[2] + "/account/" + url_values(current_url)['account'];
        // Check for campaign id 
        if (($.inArray("from", url_params) != "-1") && ($.inArray("till", url_params) != "-1")) {
            base_url += "/from/" + url_values(current_url)['from'] + "/till/" + url_values(current_url)['till'];
        }
        // Check for status of leads
        if ($.inArray("status", url_params) != "-1") {
            base_url += "/status/" + url_values(current_url)['status'];
        }
        // Append the filtered dates
        base_url += "/campaigns/" + campaign.id + "/leads";

        // redirect to the base url
        $(location).attr('href', base_url);
    });


    $('#select-lead-status').on('select2:select', function (e) {

        var lead = e.params.data;
        var current_url = $(location).attr('href');
        var url_params = current_url.split("/");
        // Base URL
        var base_url = url_params[0] + "//" + url_params[2] + "/account/" + url_values(current_url)['account'];
        // Check for campaign id 
        if (($.inArray("from", url_params) != "-1") && ($.inArray("till", url_params) != "-1")) {
            base_url += "/from/" + url_values(current_url)['from'] + "/till/" + url_values(current_url)['till'];
        }
        // Check for status of leads
        if ($.inArray("campaigns", url_params) != "-1") {
            base_url += "/campaigns/" + url_values(current_url)['campaigns'];
        }
        // Append the filtered dates
        base_url += "/status/" + lead.id + "/leads";

        // redirect to the base url
        $(location).attr('href', base_url);
    });

    /*******************************************************************************
     * 
     * General Jquery Functions
     * 
     *******************************************************************************/
    /**
     * @Function : url_values
     *
     * @param $params : Current URL
     *        
     * @return Array with account_no, From and till date, campaign_id and status;
     */
    var url_values = function (url) {

        var url_array = {};
        if (url.length > 0) {
            var url_params = url.split("/");
            url_array['account'] = $.inArray("account", url_params) == "-1" ? "" : url_params[$.inArray("account", url_params) + 1];
            url_array['from'] = $.inArray("from", url_params) == "-1" ? "" : url_params[$.inArray("from", url_params) + 1];
            url_array['till'] = $.inArray("till", url_params) == "-1" ? "" : url_params[$.inArray("till", url_params) + 1];
            url_array['campaigns'] = $.inArray("campaigns", url_params) == "-1" ? "" : url_params[$.inArray("campaigns", url_params) + 1];
            url_array['status'] = $.inArray("status", url_params) == "-1" ? "" : url_params[$.inArray("status", url_params) + 1];
        }

        return url_array;
    }

    /**
     * @Function : url_index
     *
     * @param $params : Current URL
     *        
     * @return Array of index value for account_no, From and till date, campaign_id and status;
     */
    var url_index = function (url) {

        var url_array = {};
        if (url.length > 0) {
            var url_params = url.split("/");
            url_array['account'] = $.inArray("account", url_params) == "-1" ? " " : $.inArray("account", url_params) + 1;
            url_array['from'] = $.inArray("from", url_params) == "-1" ? " " : $.inArray("from", url_params) + 1;
            url_array['till'] = $.inArray("till", url_params) == "-1" ? " " : $.inArray("till", url_params) + 1;
            url_array['campaigns'] = $.inArray("campaigns", url_params) == "-1" ? " " : $.inArray("campaigns", url_params) + 1;
            url_array['status'] = $.inArray("status", url_params) == "-1" ? " " : $.inArray("status", url_params) + 1;
        }

        return url_array;
    }

    /*
     * @type jQuery
     * 
     * Get the filtered dates from the URL and print on button
     * 
     */
    var url = $(location).attr('href');

    /*
     * @type jQuery
     * Get the filtered dates from the URL and print on button
     */
    if ((url_values(url)['from'].length > 0) && (url_values(url)['till'].length > 0)) {
        var range = url_values(url)['from'] + " - " + url_values(url)['from'];
        $('#datetimes').val(range);
    } else {
        $('#datetimes').val('Filter By Date');
    }

    /*
     * @type jQuery
     * 
     * Get the Campaign Name from the URL
     * 
     */
    if (url_values(url)['campaigns'].length > 0) {
        var campaign = url_values(url)['campaigns'];
        $('#select-lead-campaign').val(campaign).trigger('change');
    } else {
        $('#select-lead-campaign').val('0').trigger('change');
    }

    /*
     * @type jQuery
     * 
     * Get the Lead status from the URL
     * 
     */
    if (url_values(url)['status'].length > 0) {
        var lead_status = url_values(url)['status'];
        $('#select-lead-status').val(lead_status).trigger('change');
    } else {
        $('#select-lead-status').val('0').trigger('change');
    }

    $('#campaign-table tbody').on( 'click', 'tr', function () {
        console.log( table.row( this ).data() );
    } );    

    /*
     * @type jQuery
     * 
     * Change the status for account in Super Admin
     * 
     */   
    $('div#super-admin-div a').each(function(e)
    {
        if($(this).attr('value') == url_values(url)['account']){
            $(this).addClass("active-dropdown");
        }
    });
    
    
    
   // Convert Form Elements to Js Object    
    (function ($) {
        $.fn.getForm2obj = function () {
            var _ = {}, _t = this;
            this.c = function (k, v) {
                eval("c = typeof " + k + ";");
                if (c == 'undefined')
                    _t.b(k, v);
            }
            this.b = function (k, v, a = 0) {
                if (a)
                    eval(k + ".push(" + v + ");");
                else
                    eval(k + "=" + v + ";");
            };
            $.map(this.serializeArray(), function (n) {
                if (n.name.indexOf('[') > -1) {
                    var keys = n.name.match(/[a-zA-Z0-9_]+|(?=\[\])/g), le = Object.keys(keys).length, tmp = '_';
                    $.map(keys, function (key, i) {
                        if (key == '') {
                            eval("ale = Object.keys(" + tmp + ").length;");
                            if (!ale)
                                _t.b(tmp, '[]');
                            if (le == (i + 1))
                                _t.b(tmp, "'" + n['value'] + "'", 1);
                            else
                                _t.b(tmp += "[" + ale + "]", '{}');
                        } else {
                            _t.c(tmp += "['" + key + "']", '{}');
                            if (le == (i + 1))
                                _t.b(tmp, "'" + n['value'] + "'");
                        }
                    });
                } else
                    _t.b("_['" + n['name'] + "']", "'" + n['value'] + "'");
            });
            return _;
        }
    })(jQuery);    
    
});
  