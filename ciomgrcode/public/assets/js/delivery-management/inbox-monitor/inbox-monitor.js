/**
 *
 * @internal This document handles all the js for inbox monitor view
 * @global viewname = 'inboxmonitor', path = '/paging/inboxmonitor/page_number' : They are configured in inbox_monitor.twig
 * @author Aki
 */


/*
@internal : on page load functions
 */
$(document).ready(function () {
    //init select2 for domains
    initSelect2();
    //store empty table template in a variable used for redraw of the table when page number is changed
    emptyTableTemplate = inboxMonitorViewTable.html();
    //@internal - get initial data for page 1 and adds table
    drawTable(1);
    createInboxQuotaCircles('.round');
});

class CurrentFilter {
    constructor() {
        this.currentFilter = {
            page_index : 1,
            inbox_search : '',
            seed_pool_group_id : 'all',
            seed_pool_name : 'all',
            domain : 'all',
            spam_reason : 'all',
            seed_pool_type : 'all'
        };
        this.indirectClick = false;
    }

    getCurrentFilter() {
        return this.currentFilter;
    }


    setFilter($field, $val) {
        this.currentFilter[$field] = $val;
    }

    getFilter($field) {
        return this.currentFilter[$field];
    }

    setIndirectClick($bool) {
        this.indirectClick = $bool;
    }

    getIndirectClick() {
        return this.indirectClick;
    }
}
const currentFilter = new CurrentFilter();

//variables for storing meta data
let emptyTableTemplate;//  Empty table Html - used as template for changing page numbers
let emptyPaginationBarTemplate;//  Empty pagination Html - used as template for changing page numbers
let recordsPerPage = 10;// Number of records per page
let totalNumberOfRecords;
let totalNumberOfPages;
let currentPage;
let userName;
//todo: on change update the variables
let seedPoolName = 'all';
let seedPoolGroupId = 'all';
let doamin = 'all';
let spamReason = 'all';
let searchString = ''; //todo:jsr append the search string to this variable
let type = 'all';

//html div
const inboxMonitorViewTable = $('#inboxMonitorTable');


/**
 * @internal - get data for inbox monitor view. Generates - {Table,Pagination_bar}
 * @param {int}pageNumber data collected from UI
 * @param {null/array}data Payload for request
 * @param {null/string}seedPooGroupId Seed Pool Group ID
 * @param {null/string}seedPoolName
 * @param {null/string}domain
 * @param {null/string}spamReason
 * @param {null/string}type ( 'Monitoring' | 'Testing'| 'Monitoring/Testing' | 'All' )
 * @param {null/string}seed_pool_group_id
 * @param {null/string}searchString
 * @returns {void}
 */
function drawTable(pageNumber,
                   data,
                   seedPooGroupId,
                   seedPoolName,
                   domain,
                   spamReason,
                   type,

                   searchString) {
    //get contact data
    if (!data)
        data = getData(pageNumber, seedPooGroupId, seedPoolName, domain, spamReason, type, searchString)
        console.log('drawTable data', data);

    if (data.length >= 1) {
        //Generate and append html  with data
        generateHtmlAndAppendToTable(formatDataForQueueView(data));
        //Generate and  append pagination list
        generatePaginationList(pageNumber);
        currentPage = pageNumber;

    } else {
        inboxMonitorViewTable.html('No records found , Please refresh the page or try other search')
    }

    createInboxQuotaCircles('.round');
    currentFilter.setFilter('page_index', pageNumber);
}

/**
 * @internal - format data - creates the order in data || order according to the table heads
 * @param {array}data
 * @returns {array}
 */
function formatDataForQueueView(data) {
    console.log(data);
    let formattedData = [];
    $.each(data, function (index, value) {
        //map is used because of order of the fields is very important
        let tempObj = new Map();
        tempObj.set('Received', value.received);
        tempObj.set('Name', value.name);
        tempObj.set('SeedPoolGroupName', value.SeedPoolGroupName);
        tempObj.set('SeedPool', value.SeedPool);
        tempObj.set('SeedPool-Type', value.SeedPoolType);
        tempObj.set('SenderAlias', value.sender_alias);
        tempObj.set('SenderAddress', value.sender_address);
        tempObj.set('ReceivedEmails', value.received_emails);
        tempObj.set('InboxEmails', value.inbox_emails);
        tempObj.set('SpamEmails', value.spam_emails);
        tempObj.set('SpamReason', value.spam_reason);
        tempObj.set('InboxQuota', calculateInboxQuota(value.received_emails, value.inbox_emails));
        tempObj.set('Actions', value.id);
        formattedData.push(tempObj);
    });
    return formattedData;
}


/**
 * @internal - Generates pagination bar
 * todo: Handle all the page follow up
 * @returns {void}
 */
function generatePaginationList(pageNumber) {
    const paginationUl = $('#paginationUl');
    //Hold the pagination bar
    const paginationBar = $('#pagination_bar');
    if (paginationUl.find('li').length > 0) {
        //if already present reset the pagination bar
        paginationBar.html(emptyPaginationBarTemplate);
    } else {
        //Save the template
        emptyPaginationBarTemplate = paginationBar.html();
    }
    //Calculate number of pages
    if(totalNumberOfRecords % recordsPerPage === 0){
        totalNumberOfPages = Math.trunc(totalNumberOfRecords / recordsPerPage)
    }else{
        //Fill the template
        totalNumberOfPages = Math.trunc(totalNumberOfRecords / recordsPerPage) + 1;
    }

    //add numbers
    if (totalNumberOfPages <= 8) {
        for (let n = 1; n <= totalNumberOfPages; ++n) {
            addPageListToTable(n);
        }
    } else {
        //changing pattern
        if (pageNumber > 4 && pageNumber < totalNumberOfPages - 2) {
            //Generate first 10 number and last two number and dots in between
            for (let n = 1; n <= 3; ++n) {
                addPageListToTable(n);
            }
            //add dots
            addPageListToTable('.');
            //add page number nearby elements
            addPageListToTable(pageNumber - 1);
            addPageListToTable(pageNumber);
            addPageListToTable(pageNumber + 1);
            //add dots
            addPageListToTable('.');
            addPageListToTable(totalNumberOfPages - 2);
            addPageListToTable(totalNumberOfPages - 1);
            addPageListToTable(totalNumberOfPages);

        } else {
            //Generate first 10 number and last two number and dots in between
            for (let n = 1; n <= 5; ++n) {
                addPageListToTable(n);
            }
            //add dots
            addPageListToTable('.');
            //add last three pages in view
            addPageListToTable(totalNumberOfPages - 2);
            addPageListToTable(totalNumberOfPages - 1);
            addPageListToTable(totalNumberOfPages);
        }
    }

    //un-hide the div
    $('#pagination_nav_bar').removeAttr('hidden');
    $('#pagination_nav_bar').show();//test
    //make current page active
    makeCurrentPageActive(pageNumber);
 }


/**
 * @internal - Generates and appends html for pagination
 * @internal - Add next button , dots, previous button
 * @returns {void}
 */
function addPageListToTable(number) {
    const paginationUl = $('#paginationUl');
    if (number === 1) {
        //add previous button
        paginationUl.append($('#previous_template').html());
    }
    let pageHtml
    if (number !== '.') {
        pageHtml = `<li class="page-item pagination-bar" id="page_` + number + `"><div class="page-link" style="cursor: pointer">` + number + `</div></li>`
    } else {
        pageHtml = '  .  .  .  .  .  '
    }
    paginationUl.append(pageHtml);
    if (number === totalNumberOfPages) {
        //add Next button ----
        paginationUl.append($('#next_template').html())
    }
}

/**
 * @internal - Makes the page button look active
 * @returns {void}
 */
function makeCurrentPageActive(pageNumber) {
    //Make current page active
    $('#page_' + pageNumber).addClass('active');
    //init the pagination bar
    currentFilter.setFilter('page_index', pageNumber)
    initPaginationBar();
}

/**
 * @internal - On pagination bar click
 */
function initPaginationBar() {
    //event runner stops multiple events running
    let eventRunning;
    let pageNumber;
    $('.page-item').click(function () {
        if (!eventRunning) {
            eventRunning = true;
            let page = $(this).attr('id').replace("page_", "");
            if (page === 'next' && currentPage !== totalNumberOfPages) {
                pageNumber = Number(currentPage) + 1;
            } else if (page === 'previous' && currentPage !== 1) {
                pageNumber = Number(currentPage) - 1;
            } else {
                pageNumber = Number(page);
            }
            currentFilter.setFilter('page_index',pageNumber);
            //prevent same requests
            if (!$(this).hasClass('active') && Number.isInteger(pageNumber)) {
                //Get the values for base
                let inboxData = getData(pageNumber, seedPoolGroupId, seedPoolName, doamin, spamReason, type, searchString);
                //Reset the table
                inboxMonitorViewTable.html(emptyTableTemplate);
                $('#pagination_bar').html(emptyPaginationBarTemplate);
                //draw the table
                drawTable(pageNumber, inboxData);
            }
            eventRunning = false;
        }
    })
}



/**
 * @internal - Calculate the inbox quota
 * @param {int}receivedEmails
 * @param {int}inboxEmails
 * @returns {int}
 */
function calculateInboxQuota(receivedEmails, inboxEmails) {
    return ((inboxEmails / receivedEmails) * 100);
}

/**
 * @internal - Generates html and appends to table
 * @param {array}data data form the pagination API
 * @returns {void}
 * todo: Need to change this as sven design
 */
function generateHtmlAndAppendToTable(data) {
    //Html templates and divs
    const contactTableHeadRow = $("#inboxMonitorTable > thead > tr");
    const contactTableBody = $("#inboxMonitorTable > tbody");

    //add head to table
    $.each(allAvailableFieldsForView, function (key, value) {
        //thead template
        let theadTemplate = `<th id="` + value + `"><b>` + value + `</b></th>`;
        contactTableHeadRow.append(theadTemplate);
    });
    //Add body to table - As the order is defined in backend just append the data
    $.each(data, function (key, value) {
        //take table row template
        const tbodyRow = $('#body_row_template');
        for (let [index, field] of value) {
            let rowHtml = generateTbodyRowHtml(index, field);
            //template
            let theadTemplate = `<td  class="alignmiddle ` + index + `">` + rowHtml + `</td>`;
            tbodyRow.append(theadTemplate);
        }
        //dump html to table body
        let tr = `<tr> ` + tbodyRow.html() + `</tr>`
        contactTableBody.append(tr)
        //empty tbody row for loop use/next value
        tbodyRow.html('');
    });
}

/**
 * @internal - Generates html for table_body  with different styling like actions and status
 * @param {string} fieldName data form the pagination API
 * @param {string} fieldValue data form the pagination API
  * @returns {string} html string
 */
function generateTbodyRowHtml(fieldName, fieldValue) {

    //variable for status
    //todo:Make circular graph for inbox quota
    if(fieldName==='inboxMonitorId') {
        return;
    }

    if (fieldName === 'Actions') {
        //holding html div in jquery
        const dropdownTd = $('#tbody_dropdown_template');
        const dropdownMenu = $('#tbody_dropdown_template .dropdown-menu');
        //urls
        let showInboxMonitorDetailedView = '/inboxmonitor/details'; //todo: add id and change the controller
        //Back up the template
        let templateBackup = dropdownTd.html();
        //Add <a> tags to the template

        let aTagDetails = '<a data-inboxmonitorid="'+ fieldValue +'" class="dropdown-item" href="' + showInboxMonitorDetailedView + '/'+ fieldValue + '"><i class="la la-search-plus"></i>Show details</a>';

        dropdownMenu.append(aTagDetails);
        //rest process for the html template
        //store html in string
        let html = dropdownTd.html();
        //reset the template
        dropdownTd.html(templateBackup);
        //return the string
        return html;
    }

    // https://webdevtrick.com/circular-progress-bar-css/
    if(fieldName === 'InboxQuota') {
        console.log('InboxQuota fieldValue', fieldValue);
        $html =
            '<section id="circleBar">\n' +
            '       <div class="container">\n' +
            '           <div class="row">\n' +
            '               <div class="col-md-3"> \n' +
            '                   <div class="round"\n' +
            '                       data-value="'+fieldValue/100.0+'"\n' +
            //'                       data-value="'+(37/100.0)+'"\n' +  // TEST
            '                       data-size="50"\n' +
            '                       data-thickness="3">\n' +
            '                       <strong class="innerCirclePct"></strong>\n'+
            '                   </div>\n' +
            '               </div>\n' +
            '           </div>\n' +
            '   </div>\n' +
            '</section>';
        return $html;
    }

    //if no styling return the normal text field
    return fieldValue;
}


/**
 * @internal - get data form API
 * @param {int}pageNumber
 * @param {null|string}seed_pool_group_id
 * @param {null/string}seedPoolName
 * @param {null/string}domain
 * @param {null/string}spamReason
 * @param {null/string}seed_pool_type ( 'Monitoring' | 'Testing'| 'Monitoring/Testing' | 'all' )
 * @param {null/string}searchString
 * @returns {array}
 */
function getData(pageNumber,
                 seedPoolGroupId = 'all',
                 seedPoolName='all',
                 domain='all',
                 spamReason='all',
                 seed_pool_type='all' ,
                 searchString='') {
    //Get the payload
    // if(pageNumber === undefined || pageNumber === '' || pageNumber === 0 ) {
    //     pageNumber = 1;
    // }
    let payloadData = getPayloadData(seedPoolGroupId, seedPoolName, domain, spamReason, seed_pool_type, searchString);

    let inboxData
    //Get the data form the server side
    $.ajax({
        url: path + pageNumber,
        type: 'POST',
        async: false,
        data: payloadData,
        success: function (data) {
            if (data.status === true) {
                inboxData = data.records;
                console.log('inboxData', inboxData);
                totalNumberOfRecords = data.total_number_of_rows;
                userName = data.current_user.company_name;
            } else {
                //show message on table
                $('#inboxMonitorTable').html('No records found , Please refresh the page or try new settings');
                //$('#contactViewTable').html('data.message');
                inboxData = [];
            }
        }
    });
    return inboxData;
}


/**
 * @internal - Generates payload for the request
 * @param {null/string}seedPoolName
 * @param {null/string}domain
 * @param {null/string}spamReason
 * @param {null/string}type ( 'Monitoring' | 'Testing'| 'Monitoring/Testing' | 'All' )
 * @param {null/string}searchString
 * @returns {string}
 */
function getPayloadData(
    seedPoolGroupId = null,
    seedPoolName = null,
    domain = null,
    spamReason = null,
    seed_pool_type = null,
    searchString = null
) {
    return JSON.stringify({
        "viewname": viewName,
        "seed_pool_group_id" : currentFilter.getFilter('seed_pool_group_id'),
        "seed_pool_name": currentFilter.getFilter('seed_pool_name'),
        "domain": currentFilter.getFilter('domain'),
        "spam_reason": currentFilter.getFilter('spam_reason'),
        "seed_pool_type": currentFilter.getFilter('seed_pool_type'),
        "search_string": currentFilter.getFilter('inbox_search'),
        "records_per_page": recordsPerPage
    })
}

/**
 * @internal - helper function to inti select2 for domains dropdown
 * @return void
 */
function initSelect2() {
    $(".domains-dropdown").select2({
        tags: false
    });
    $('.select2-selection__arrow').hide();
}

//@internal - ***** jquery event based functions ******

// PRESS SYNC BUTTON
$('#inbox-monitor-sync-button').on('click', function () {
    //Start animation
    $('img#runsync').css('display', 'inline');
    //call sync method
    let path = $(this).data('route')
    $.ajax({
        url: path,
        type: 'GET',
        async: false,
        success: function (data) {
            if (data.status === true) {
                //todo: show that sync is complete
            } else {
                //todo: show error
            }
        }
    });

    //stop animation
    $('img#runsync').css('display', 'none');
    window.local.reload();
});




// FILTER LISTENER on domainname
$('select#domainName').on('change', function changeDomainName() {
    currentFilter.setFilter('domain', $(this).val());
    console.log('$currentFilterValue domainName', $(this).val());
});


$('.filter').on('change, change.select2', function changeFilter() { // ,, change.select2
    currentFilter.setIndirectClick(false);
    var $this = $(this);
    var $filter = $this.data('filter');
    console.log('FILTER ON ' + $filter, 'id', $this.attr('id'), 'val', $this.val(), 'element', $this.get(0).nodeName);

    switch ($filter) {
        case 'fulltextsearch':
            currentFilter.setFilter('inbox_search',$this.val());
            break;
        case 'seedpoolgroupname':
            currentFilter.setFilter('seed_pool_group_id',$this.val());
            currentFilter.setFilter('seed_pool_name','all');
            $('select#SeedPoolName').val('all');
            break;
        case 'seedpoolname':
            currentFilter.setFilter('seed_pool_name', $this.val());
            var $seedPoolGroupId = $('select#SeedPoolName :selected').data('seedpoolgroupid');
            console.log('$seedPoolGroupId', $seedPoolGroupId);
            $('select#SeedPoolGroupName').val($seedPoolGroupId);
            currentFilter.setFilter('seed_pool_group_id',$seedPoolGroupId);
            break;
        case 'domainname':
            currentFilter.setFilter('domain', $this.val());
            // console.log($filter, $currentFilterValue);
            break;
        case 'spamreason':
            currentFilter.setFilter('spam_reason', $this.val());
            break;
        case 'seedpooltype':
            currentFilter.setFilter('seed_pool_type', $('input.filter[name="seedpooltype"]:checked').val());
            break;
        default:
            console.log('nothing found');
            break;
    }

    console.log('currentFilter.getCurrentFilter', currentFilter.getCurrentFilter());
});


/**
 * startFilter
 */
$('div#startFilter').on('click', function startFilter() {
    currentFilter.setFilter('page_index', 1);
    let inboxData = getData(
        currentFilter.getFilter('page_index'),
        currentFilter.getFilter('seed_pool_group_id'),
        currentFilter.getFilter('seed_pool_name'),
        currentFilter.getFilter('domain'),
        currentFilter.getFilter('spam_reason'),
        currentFilter.getFilter('seed_pool_type'),
        currentFilter.getFilter('inbox_search')
    );

    //Reset the table
    inboxMonitorViewTable.html(emptyTableTemplate);
    $('#pagination_bar').html(emptyPaginationBarTemplate);
    //draw the table
    drawTable(currentFilter.getFilter('page_index'), inboxData);
    createInboxQuotaCircles('.round');
})

/**
 * reset all filters
 */
$('div#resetFilter').on('click', function resetAllFilters() {
    $('.filter').each(function (i, obj) {

        currentFilter.setFilter('page_index', 1);
        var $resetFilter = $(obj).data('filter');
        console.log('each loop $resetFilter', $resetFilter, obj);
        var $seedpooltypeProcessed = false;

        switch ($resetFilter) {
            case 'fulltextsearch':
                $(obj).val('');
                currentFilter.setFilter('inbox_search', '');
                break;
            case 'seedpoolgroupname':
                $(obj).val('all');
                currentFilter.setFilter('seed_pool_group_id', 'all');
                break;
            case 'seedpoolname':
                $(obj).val('all');
                currentFilter.setFilter('seed_pool_name', 'all');
                break;
            case 'domainname':
                $('select#domainName').val('all').trigger('change');
                currentFilter.setFilter('domain', 'all');
                break;
            case 'spamreason':
                $(obj).val('all');
                currentFilter.setFilter('spam_reason', 'all');
                break;
            case 'seedpooltype':
                if( !$seedpooltypeProcessed) { // do that only once and not for each radio-input
                    $('input#radioAll').prop('checked', true).parent().addClass('active');
                    $('input#radioMonitoring').prop('checked', false).parent().removeClass('active');
                    $('input#radioTesting').prop('checked', false).parent().removeClass('active');
                    $('input#radioMonitoringTesting').prop('checked', false).parent().removeClass('active');
                    currentFilter.setFilter('seed_pool_type', 'all');
                    $seedpooltypeProcessed = true;
                }
                break;
            default:
                console.log('do nothing');
                break;
        }//switch
    })// each

    $('div#startFilter').trigger('click');

});// resetAllFilters


/**
 *
 * @param el
 * @see https://webdevtrick.com/circular-progress-bar-css/
 */
function createInboxQuotaCircles(el){
    $(el).circleProgress({fill: {color: '#7abe73'}})
        .on('circle-animation-progress', function(event, progress, stepValue){

            $(this).find('strong').text(((String(stepValue.toFixed(2)).substr(0))*100) + '%');

        });
};


// ENABLE BUTTON POPOVER
$('.cio-popover').popover({
    container: 'body',
    trigger: 'hover'
})

