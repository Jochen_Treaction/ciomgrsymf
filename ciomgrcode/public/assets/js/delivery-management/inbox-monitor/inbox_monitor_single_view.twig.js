$(document).ready(function () {
    createInboxQuotaCircles('.round');
    function createInboxQuotaCircles(el) {
        $(el).circleProgress({fill: {color: '#7abe73'}})
            .on('circle-animation-progress', function (event, progress, stepValue) {
                console.log(progress, stepValue);
                // $(this).find('strong').text(String(stepValue.toFixed(2)).substr(0) + '%');
                $(this).find('strong').text(((String(stepValue.toFixed(2)).substr(0))*100) + '%');
            });
    };


    var $spam_reason_explanation = $('div#spam_reason_explanation').data('spam_reason_explanation');
    $('div#'+$spam_reason_explanation).css('display', 'initial');

});
