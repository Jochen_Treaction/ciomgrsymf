$(document).ready(function () {



    $('#orderCioApiKey').click(function () {
        let account_no = $('#orderCioApiKey').data("isTest");
        if(parseInt(account_no) <= 0) {
            return ;
        }
        let ajax_data = ( {'name': 'accountNo', 'value': account_no} );
        let ajax_en_data = btoa ( JSON.stringify ( ajax_data ) );

        $.ajax( {
            url: apiInOne+"/general/account/apikey/create",
            type: 'POST',
            async: false,
            timeout: 0,
            header: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE',
                'Access-Control-Allow-Headers': 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
                'Access-Control-Max-Age': '86400'
            },
            data: {data: ajax_en_data},
            success: function(data) {
                let response = (data);
                if(response.status) {
                    // refresh the page to fetch the apikey from Backend Database
                    location.reload(true);
                    //$('#cioapikeyconfirm').removeClass("kt-hide");
                }else {
                    $('#cioapikeyerror').removeClass("kt-hide");
                }
            }
        })
    });

});