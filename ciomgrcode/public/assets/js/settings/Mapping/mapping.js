//table
const $tableID = $('#table');
const $BTN = $(".update");
const $EXPORT = $('#export');
const newTr = generateHTML();
let data;

//add button in table functionality
$('.table-add').on('click', 'a', () => {
    //this adds
    $('tbody').append(newTr);
    addSelect2();

    //this for jquery type and select
    function addSelect2() {
        $(function () {
            $.widget("custom.combobox", {
                _create: function () {
                    this.wrapper = $("<span>")
                        .addClass("custom-combobox")
                        .insertAfter(this.element);

                    this.element.hide();
                    this._createAutocomplete();
                    this._createShowAllButton();
                },

                _createAutocomplete: function () {
                    var selected = this.element.children(":selected"),
                        value = selected.val() ? selected.text() : "";

                    this.input = $("<input>")
                        .appendTo(this.wrapper)
                        .val(value)
                        .attr("title", "")
                        .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
                        .autocomplete({
                            delay: 0,
                            minLength: 0,
                            source: $.proxy(this, "_source")
                        })
                        .tooltip({
                            classes: {
                                "ui-tooltip": "ui-state-highlight"
                            }
                        });

                    this._on(this.input, {
                        autocompleteselect: function (event, ui) {
                            ui.item.option.selected = true;
                            this._trigger("select", event, {
                                item: ui.item.option
                            });
                        },

                        autocompletechange: "_removeIfInvalid"
                    });
                },

                _createShowAllButton: function () {
                    var input = this.input,
                        wasOpen = false;

                    $("<a>")
                        .attr("tabIndex", -1)
                        .attr("title", "Show All Items")
                        .tooltip()
                        .appendTo(this.wrapper)
                        .button({
                            icons: {
                                primary: "ui-icon-triangle-1-s"
                            },
                            text: false
                        })
                        .removeClass("ui-corner-all")
                        .addClass("custom-combobox-toggle ui-corner-right")
                        .on("mousedown", function () {
                            wasOpen = input.autocomplete("widget").is(":visible");
                        })
                        .on("click", function () {
                            input.trigger("focus");

                            // Close if already visible
                            if (wasOpen) {
                                return;
                            }

                            // Pass empty string as value to search for, displaying all results
                            input.autocomplete("search", "");
                        });
                },

                _source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response(this.element.children("option").map(function () {
                        var text = $(this).text();
                        if (this.value && (!request.term || matcher.test(text)))
                            return {
                                label: text,
                                value: text,
                                option: this
                            };
                    }));
                },

                _removeIfInvalid: function (event, ui) {

                    // Selected an item, nothing to do
                    if (ui.item) {
                        return;
                    }

                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                        valueLowerCase = value.toLowerCase(),
                        valid = false;
                    this.element.children("option").each(function () {
                        if ($(this).text().toLowerCase() === valueLowerCase) {
                            this.selected = valid = true;
                            return false;
                        }
                    });

                    // Found a match, nothing to do
                    if (valid) {
                        return;
                    }

                    // Remove invalid value
                    this.input
                        .val("")
                        .attr("title", value + " didn't match any item")
                        .tooltip("open");
                    this.element.val("");
                    this._delay(function () {
                        this.input.tooltip("close").attr("title", "");
                    }, 2500);
                    this.input.autocomplete("instance").term = "";
                },

                _destroy: function () {
                    this.wrapper.remove();
                    this.element.show();
                }
            });

            $(".combobox").combobox();
            $("#toggle").on("click", function () {
                $("#combobox").toggle();
            });
        });
    };

    //this is for adding custom params
    $('.fieldTypeSelect').on('change', function () {
        let selectedValue = this.value;
        let row = $(this).closest("tr");
        let fieldtypeClass = row.find('.cio');
        if (selectedValue === 'custom') {
            let cioSelect = fieldtypeClass.empty();
            cioSelect.append(`<input class="form-control cioCustom" value="Contact." type="text" placeholder="CIO Custom Field">`);
        } else if (selectedValue === 'standard') {
            let cioSelect = fieldtypeClass.empty ();
            let CIOStandardFields = generateCIOSelect ( standardFields );
            cioSelect.append ( CIOStandardFields );
            addSelect2 ();
        }
    });


});
//remove button functionality
$tableID.on('click', '.table-remove', function () {

    $(this).parents('tr').detach();
});

$tableID.on('click', '.table-up', function () {

    const $row = $(this).parents('tr');

    if ($row.index() === 1) {
        return;
    }

    $row.prev().before($row.get(0));
});

$tableID.on('click', '.table-down', function () {

    const $row = $(this).parents('tr');
    $row.next().after($row.get(0));
});

// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;
//collect data form the table encrypt the data and map it to the mapping table field in the form
$BTN.on('click', () => {
    $(".justShow").remove();
    const $rows = $tableID.find('tr:not(:hidden)');
    const headers = [];
    const data = [];

    // Get the headers (add special header logic here)
    $($rows.shift()).find('th:not(:empty)').each(function () {
        headers.push($(this).text().toLowerCase());
    });

    // Turn all existing rows into a loop able array
    $rows.each(function () {
        const $td = $(this).find('td');
        const h = {};

        // Use the headers from earlier to name our hash keys
        headers.forEach((header, i) => {
            switch (header) {
                case "data type":
                    h[header] = $(".dataType", this).find('option:selected').text();
                    break;
                case "html form":
                    let text = ( $td.eq ( i ).text () );
                    if (text.length < 2) {
                        let textCio = $(".cio", this).text();
                        if (textCio.length > 20) {
                            text = ( $ ( ".cio", this ).find ( 'option:selected' ).val () );
                        } else {
                            CIOCusField = $(this).find('td.cio > input').val();
                            let valSplit = CIOCusField.split('.');
                            if(valSplit[1].length !== 0) {
                                text = 'Contact_' + valSplit[1];
                            }
                        }
                    }
                    h[header] = text;
                    break;
                case "marketing-in-one":
                    let CIOtext = $td.eq(i).text();
                    if (CIOtext.length > 20) {
                        CIOtext = $(".cio", this).find('option:selected').text();
                    }else if(CIOtext.length === 0){
                        CIOtext = $(this).find('td.cio > input').val();
                    }
                    h[header] = CIOtext;
                    break;
                case "field type":
                    h[header] = $(".FT", this).find('option:selected').text();
                    break;
                default:
                    h[header] = "";

            }
            if (h[header] === "") {
                h[header] = $td.eq(i).text();
            }
        });
        data.push(h);
    });

    // replace German character and other special characters
    function formatString(original_string) {
        temp_str = original_string.replace ( /ö/g, 'oe' );
        temp_str = temp_str.replace ( /ä/g, 'ae' );
        temp_str = temp_str.replace ( /ü/g, 'oe' );
        temp_str = temp_str.replace ( /ß/g, 'ss' );
        temp_str = temp_str.replace ( /Ü/g, 'Ue' );
        temp_str = temp_str.replace ( /Ä/g, 'Ae' );
        temp_str = temp_str.replace ( /Ö/g, 'Oe' );
        temp_str = temp_str.replace ( / /g, '_' );
        temp_str = temp_str.replace ( '&', '-' );
        temp_str = temp_str.replace ( '/', '-' );
        temp_str = temp_str.replace ( '.', '.' );
        /*        temp_str = temp_str.replace(/[_\W]+/g, "");*/
        formated_string = temp_str.replace ( /-/g, '' );
        return formated_string;
    }
    let value = JSON.stringify(data);
    let encryptedValue = btoa(value);
    $("#urlID").attr('value', encryptedValue);
    $("#second").prop('disabled', true);
    $("#submit").click();
    setTimeout(function () {
        $("#int-mapping").click();
    }, 1)
})

//Take mapping values from database and generate table
//appending Mail-In-One values
let mailInOneValues =[];
if(mailInOne) {
    mailInOneValues = JSON.parse ( atob ( mailInOne ) );
}

if (mapping) {
    mapping = JSON.parse(atob(mapping));
} else {
    mapping = [];
}
console.log ( mapping );

$.each(mapping, function (index, value) {
    let CIO = value["marketing-in-one"];
    let HtmlForm = value["html form"];
    //if empty HTML form value will be CIO value
    if (HtmlForm.length === 0) {
        HtmlForm = CIO;
    }
    let MIO;
    if (mailInOneValues) {
        MIO = appendMIOValues(mailInOneValues, CIO)
    } else {
        MIO = checkValues(value["email-in-one"]);
    }
    ;
    let Hubspot = checkValues(value["hubspot"]);
    let EmailForwarding = checkValues(value["email forwarding"]);
    let fieldType = checkValues(value["field type"]);
    let customFields = checkValues(value['custom fields']);
    let dataType = checkValues(value['data type']);

    const newTrTest = `
        <tr class="">
                                        <td class="pt-3-half cio" contenteditable="true">` + CIO + `</td>
                                        <td class="pt-3-half" contenteditable="true">` + HtmlForm + `</td>
                                        <td class="pt-3-half" contenteditable="flase">` + MIO + `</td>
                                        <td class="pt-3-half" contenteditable="false">` + Hubspot + `</td>
                                        <td class="pt-3-half" contenteditable="false">` + EmailForwarding + `</td>
                                        <td class="pt-3-half" contenteditable="false">` + customFields + `</td>
                                        <td class="pt-3-half dataType noEdit" contenteditable="true"> ` + dataType + `</td>
                                        <td class="pt-3-half" contenteditable="false">` + fieldType + `</td>
                                        <td>
                                          <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                        </td>
                                    </tr>

        `;
    //appending html to
    $('tbody').append(newTrTest);

});
//This are questions to fill in the table
//adding survey fields to the table
if (survey) {
    let surveyQuestions = [];
    $.each(survey, function (key, value) {
        let questionJson = value['question'];
        let text = questionJson['text'];
        let type = questionJson['type'];
        let options = questionJson[type];
        options = options['options'];
        let answers = [];
        $.each(options, function (k, val) {
            let option = val['value'];
            if (typeof option == 'undefined') {
                let min = val['min'];
                let max = val['max'];
                let step = val['step'];
                option = 'Min:' + min + ',Max' + max + ',Step' + step;
            } else if (option === '') {
                option = "This is text box";
            }
            //make it as array
            answers.push(option);
        });
        let surveyQuestion = {[text]: [answers]};
        surveyQuestions.push(surveyQuestion);
    });
    $.each(surveyQuestions, function (i, value) {
        let number = i + 1;
        questionTitle = (Object.keys(value)).toString();
        questionAnswers = (Object.values(value)).toString();
        const newTrTest = `
        <tr class="justShow">
                                        <td data-html="true" data-toggle="popover" title="` + questionTitle + `" data-content="` + questionAnswers + `" class="pt-3-half cio-popover hover` + number + `" contenteditable="false">Survey_Question` + number + `</td>
                                        <td class="pt-3-half" contenteditable="false"></td>
                                        <td class="pt-3-half" contenteditable="false"></td>
                                        <td class="pt-3-half" contenteditable="false"></td>
                                        <td class="pt-3-half" contenteditable="false"></td>
                                        <td class="pt-3-half" contenteditable="false"></td>
                                        <td class="pt-3-half" contenteditable="false"></td>
                                        <td class="pt-3-half" contenteditable="false">Custom</td>
                                        <td>
                                          <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                        </td>
                                    </tr>

        `;
        //appending html to
        $('tbody').append(newTrTest);
    });
}

/*let questions = "{{ c_set.questions is defined ? c_set.questions:'' }}";
let question = questions.replace(/&quot;/ig,'"');
question = JSON.parse(question);*/


//generate HTML function -generates HTML for the table row

function generateHTML() {

    let dataTypeSelectHtml = ` <select>
                            <option value="standard">Text</option>
                            <option value="custom">Decimal</option>
                            <option value="custom">Integer</option>
                            <option value="custom">Date</option>
                            <option value="custom">DateTime</option>
                            <option value="custom">Email</option>
                            <option value="custom">URL</option>
                            <option value="custom">Phone</option>
                        </select> `
    let cioParamsArray = standardFields;
    /*let cioParamsArray = ['',
        'email',
        'salutation',
        'first_name',
        'last_name',
        'firma',
        'street',
        'postal_code',
        'city',
        'country',
        'phone',
        'account',
        'url',
        'final_url',
        'trafficsource',
        'utm_parameters',
        'campaign',
        'split_version',
        'lead_reference',
        'targetgroup',
        'affiliateID',
        'affiliateSubID',
        'status',
        'ip',
        'device_type',
        'device_browser',
        'device_os',
        'device_os_version',
        'device_browser_version'
    ];*/
    let CioHtml = generateCIOSelect(cioParamsArray);
    let fieldType = `
                         <select class="form-control form-control-md fieldTypeSelect" style="opacity:1">
                            <option value="standard">Standard</option>
                            <option value="custom">Custom</option>
                        </select> `

    return `<tr class="">
                                        <td class="pt-3-half cio" contenteditable="true"> ` + CioHtml + ` </td>
                                        <td class="pt-3-half" contenteditable="true"> </td>
                                        <td class="pt-3-half" contenteditable="false"> </td>
                                        <td class="pt-3-half noEdit" contenteditable="false"> </td>
                                        <td class="pt-3-half noEdit" contenteditable="false"> </td>
                                        <td class="pt-3-half" contenteditable="true"> </td>
                                        <td class="pt-3-half dataType">` + dataTypeSelectHtml + `</td>
                                        <td class="pt-3-half noEdit FT">` + fieldType + `</td>
                                        <td>
                                          <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                        </td>
                                    </tr>`;
}

//this functions check the values and remove undefined and make them empty
function checkValues(x) {
    if (x) {
        return x;
    } else {
        return '';
    }
};

//Generate select options for CIO
function generateCIOSelect(array) {
    let innerHTML = "";
    //get mapping array from database but for now hard-coded
    console.log ( array )
    $.each ( array, function (key, value) {
        let select = `<option value="` + value + `">` + key + `</option>`;
        innerHTML += select;
    } )
    /*    array.forEach(function (value) {
            let select = `<option value="` + value + `">` + value + `</option>`;
            innerHTML += select;
        });*/
    let EmptyHtml = `<option value=""> </option>`//this is for empty field
    innerHTML = innerHTML + EmptyHtml;
    let HTML = ` <select class ="combobox">
                          ` + innerHTML + `
                        </select> `;
    return HTML;
}

//this checks the CIO value and MIO mapping

function appendMIOValues(mailInOneValues, CIO) {
    let Return = '';
    $.each(mailInOneValues, function (key, value) {
        if (value['marketing-in-one'] === CIO) {
            Return = value['email-in-one'];
        }
    });
    return Return;
}

