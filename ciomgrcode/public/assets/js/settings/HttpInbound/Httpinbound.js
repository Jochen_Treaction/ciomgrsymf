//Table
const $tableID = $('#table');
const $BTN = $(".update");
const $EXPORT = $('#export');
const newTr = generateHTML();

$('.table-add').on('click', 'a', () => {
    $('tbody').append(newTr);
});

$tableID.on('click', '.table-remove', function () {

    $(this).parents('tr').detach();
});

$tableID.on('click', '.table-up', function () {

    const $row = $(this).parents('tr');

    if ($row.index() === 1) {
        return;
    }

    $row.prev().before($row.get(0));
});

$tableID.on('click', '.table-down', function () {

    const $row = $(this).parents('tr');
    $row.next().after($row.get(0));
});

// A few jQuery helpers for exporting only
jQuery.fn.pop = [].pop;
jQuery.fn.shift = [].shift;

$BTN.on('click', () => {

    const $rows = $tableID.find('tr:not(:hidden)');
    const headers = [];
    const data = [];

    // Get the headers (add special header logic here)
    $($rows.shift()).find('th:not(:empty)').each(function () {

        headers.push($(this).text().toLowerCase());
    });

    // Turn all existing rows into a loopable array
    $rows.each(function () {
        const $td = $(this).find('td');
        const h = {};

        // Use the headers from earlier to name our hash keys
        headers.forEach((header, i) => {
            switch (header) {
                case "marketing-in-one":
                    h[header] = $(".cio", this).find('option:selected').text();
                    break;
                default:
                    h[header] = "";

            }
            if (h[header] === "") {
                h[header] = $td.eq(i).text();
            }
        });

        data.push(h);
    });

    value = JSON.stringify(data);
    encryptedValue = btoa(value);
    $("#urlID").attr('value', encryptedValue);
    $("#second").prop('disabled', true);
    $("#submit").click();
    // Output the result
    $EXPORT.text(JSON.stringify(data));
});
// Generate table columns when page loads
mapping = JSON.parse(atob(mapping))
$.each(mapping, function (index, value) {
    let CIO = value["marketing-in-one"];
    let constant = value["default value"];
    let urlParameter = value["url parameter"];
    const newTrTest = `
        <tr class="">
                                        <td class="pt-3-half" contenteditable="true">` + urlParameter + `</td>
                                        <td class="pt-3-half" contenteditable="true">` + constant + `</td>
                                        <td class="pt-3-half " contenteditable="true">` + CIO  + `</td>
                                        <td>
                                          <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                        </td>
                                    </tr>

        `;
    //appending html to
    $('tbody').append(newTrTest);
});
//generate HTML function -generates HTML for the table row
function generateHTML() {
    let CIOSelect = generateCIOSelect(campaignInOneParams);
    return `<tr class="defaultTableValue">
                                        <td class="pt-3-half" ></td>
                                        <td class="pt-3-half" ></td>
                                        <td class="pt-3-half cio">` + CIOSelect + `</td>
                                        <td>
                                          <span class="table-remove"><i style="color:red;cursor: pointer;" class="fas fa-minus-circle"></i></span>
                                        </td>
                                      </tr>`;
}

function generateCIOSelect(array) {
    let innerHTML = "";
    //get mapping array from database but for now hard-coded
    array.forEach(function (value) {
        let select = `<option value="` + value + `">` + value + `</option>`;
        innerHTML += select;

    });
    let EmptyHtml = `<option value=""> </option>`//this is for empty field
    innerHTML = innerHTML + EmptyHtml;
    let HTML = ` <select>
                          ` + innerHTML + `
                        </select> `;
    return HTML;
}

