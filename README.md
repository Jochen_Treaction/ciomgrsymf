# README ciomgrsymf #


### What is this repository for? ###

# CIO Manager Symfony - Version 2.0.3.0

## How do I get set up?
#### HOMESTEAD
    ip: "192.168.10.10"
    memory: 2048
    cpus: 2
    provider: virtualbox
    
    \# authorize: ~/.ssh/id_rsa.pub
    
    \# keys:
    \#    - ~/.ssh/id_rsa

    # change this to your installation path!!!!
    folders:
        - map: ~/Documents/php-codes/ciomgrsymf/ciomgrcode
          to: /home/vagrant/code
          
        - map: ~/Documents/php-codes/ciomgrsymf/ciomgrcode/exchange
          to: /home/vagrant/exchange

    sites:

        - map: ciomgrsymf.test
          to: /home/vagrant/code/public

    databases:

    - homestead

    backup: true
    
    features:
        - mariadb: false
        - ohmyzsh: false
        - webdriver: false
---    

## Database Configuration in vagrant ssh
##### first copy sqlscripts/cio.zip  to your exchange folder and unzip it

\> vagrant ssh

$ cd exchange

$ mysql

mysql> use homestead;

mysql> \\. cionew.sql

mysql> \\. user_ciomgr.sql


  
## EDIT YOUR .env
    
##### PRECONDITION: user_ciomgr.sql must have been excecuted => user ciomgr is alive ! 
~~DATABASE_URL=mysql://ciomgr:@192.168.2.1x5:3306/cio?serverVersion=5.7~~
    DATABASE_URL=mysql://ciomgr:"2%26yko3%3BZs5%5D8N%3EM%3A%7D"@192.168.2.145:3306/cio?serverVersion=5.7

## EDIT YOUR Windows\\System32\\drivers\\hosts
    
    192.168.10.10 ciomgrsymf.test

## EDIT YOUR .gitignore
   
    /.env.local
  
    /.env.local.php
    
    /.env.*.local
    
    /public/bundles/
    
    /var/
    
    /vendor/
    
    /Homestead/
    
    /exchange/

## EDIT YOUR doctrine.yaml
    got to \ciomgrcode\config\packages\doctrine.yaml
    see "add following lines (start)" in example below and add this two lines to enable enum handling
    ---  doctrine.yaml ---
    dbal:
        url: '%env(resolve:DATABASE_URL)%'

        # IMPORTANT: You MUST configure your db driver and server version,
        # either here or in the DATABASE_URL env var (see .env file)
        #driver: 'mysql'
        #server_version: '5.7'

        # Only needed for MySQL (ignored otherwise)
        charset: utf8mb4
        default_table_options:
            collate: utf8mb4_unicode_ci
            
        #add following lines (start):    
        mapping_types:
            enum:       string
        #add following lines (end).
        ...
    ---  doctrine.yaml ---        
